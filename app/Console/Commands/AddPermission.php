<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Permission;

class AddPermission extends Command
{
    protected $signature = 'permission:add {--permission_code=} {--permission_name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add permission commande';

    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
    	$permission_code = $this->option('permission_code');
    	$permission_name = $this->option('permission_name');
    	if(!$permission_code OR !$permission_name){
    		$this->error('Missing params');
    		$this->info('Signature: '.$this->signature);
    		return false;
    	}
    	if(Permission::where('lookup_code',$permission_code)->count()){
    		$this->error('Permission Already Exists');
    		return false;
    	}
    	$permission = new Permission;
    	$permission->active = 'Y';
    	$permission->lookup_code = $permission_code;
    	$permission->permission_name = $permission_name;
    	$permission->save();
        //$this->info('Display info');
        //$this->error('Display error');
        $this->line('Permission added with success');
        return true;
    }
}

