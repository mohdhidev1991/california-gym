<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ReaCacheHelper;

class Cache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:flush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command flush all cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //flush_cache();
        flush_cache();
        $this->info("Flush all cache with success");
        return;
    }
}
