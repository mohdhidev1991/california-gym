<?php

namespace App\Console\Commands;

use App\Helpers\HijriDateHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class CloseCourseSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course_session:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close all course session';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $attributes = ['session_status_id'=>Session_status_Closed_session];
        $hdate = HijriDateHelper::getHdateByFormat(date('Ymd'));
        $time = date('H:i');
        $sql = DB::table('course_session')
                ->where('session_status_id','=',Session_status_Current_session)
                ->where(function($query) use ($hdate){
                    $query->where('session_hdate','<',$hdate);
                })
                ->orWhere(function($query) use ($hdate,$time){
                    $query->where('session_hdate','=',$hdate);
                    $query->where('session_end_time','<=',$time);
                })->update($attributes);

        $this->info('The courses session were closed successfully!');
    }
}
