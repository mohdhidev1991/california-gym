<?php

namespace App\Console\Commands;
use App\Helpers\HijriDateHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class GetDateInfos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'infos:date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get date infos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $info = HijriDateHelper::to_hijri(date('Ymd'),'all');
        Redis::set('date:'.date('Ymd'),json_encode($info));
        $this->info('The date info is added in cache with success!');
    }
}
