<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Helpers\HijriDateHelper;

class MissedCourseSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course_session:missed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $attributes = ['session_status_id'=>Session_status_Missed_session];
        $hdate = HijriDateHelper::getHdateByFormat(date('Ymd'));
        $time = date('H:i');
        DB::table('course_session')
            ->where('session_status_id','=',Session_status_Coming_session)
            ->where(function($query) use ($hdate){
                $query->where('session_hdate','<',$hdate);
            })
            ->orWhere(function($query) use ($hdate,$time){
                $query->where('session_hdate','=',$hdate);
                $query->where('session_end_time','<=',$time);
            })->update($attributes);
        $this->info('The courses session were missed successfully!');
    }
}
