<?php

namespace App\Console;


use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\CloseCourseSession::class,
        \App\Console\Commands\MissedCourseSession::class,
        \App\Console\Commands\GetDateInfos::class,
        \App\Console\Commands\AddPermission::class,
        \App\Console\Commands\Cache::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('course_session:close')->everyFiveMinutes();
        //$schedule->command('course_session:missed')->everyFiveMinutes();
        //$schedule->command('infos:date')->dailyAt('00:00');
        //$schedule->command('queue:work')->everyMinute();
    }
}
