<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AlertUserByAttendanceEvent extends Event
{
    use SerializesModels;
    public $course_session;
    public $students_session;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($course_session,$students_session)
    {
        $this->course_session = $course_session;
        $this->students_session = $students_session;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
