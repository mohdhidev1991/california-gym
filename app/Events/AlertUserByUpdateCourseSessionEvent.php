<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Sly\NotificationPusher\PushManager;

class AlertUserByUpdateCourseSessionEvent extends Event
{
    use SerializesModels;
    public $course_session;
    public $alert_type_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($course_session,$alert_type_id)
    {
        $this->course_session = $course_session;
        $this->alert_type_id = $alert_type_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
