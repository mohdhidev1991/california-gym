<?php

namespace App\Events;

use App\Events\Event;
use App\Models\AlertUser;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AlertUserEvents extends Event
{
    use SerializesModels;
    public $devices;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($devices)
    {
        $this->devices = $devices;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
