<?php
namespace App\Helpers;
use Excel;

class ExcelHelper
{
    public static function export_model($data, $model = 'data', $rows = [])
    {
    	$Filename = $model.'-'.date('Y-m-d--H\hm');
    	
        Excel::create($Filename, function($excel) use($data, $model, $rows) {
		    $excel->sheet($model, function($sheet) use($data, $model, $rows) {
		    	$data_to_export = [];
		    	foreach($data as $item){
		    		$fields = [];
		    		foreach ($rows as $key_row => $row){
			    		if( isset($item[$key_row]) ){
			    			if(is_array($item[$key_row])){
			    				$fields[$row] = implode("\n",$item[$key_row]);
			    			}else{
			    				$fields[$row] = $item[$key_row];
			    			}
			    		}else{
			    			$fields[$row] = null;
			    		}
			    	}
		    		$data_to_export[] = $fields;
		    	}
		        $sheet->fromArray($data_to_export);
		    });
		})->export('xls');
		exit();
    }

}