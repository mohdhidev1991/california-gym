<?php
/**
 * Created by PhpStorm.
 * User: Aziz Trabelsi
 * Date: 7/2/2016
 * Time: 9:59 PM
 */

namespace App\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Logger
{
    public static function queries($user)
    {
        DB::connection()->enableQueryLog();

        DB::listen(
            function ($sql) use ($user) {

                // $sql is an object with the properties:
                //  sql: The query
                //  bindings: the sql query variables
                //  time: The execution time for the query
                //  connectionName: The name of the connection

                // To save the executed queries to file:
                // Process the sql and the bindings:
                foreach ($sql->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else {
                        if (is_string($binding)) {
                            $sql->bindings[$i] = "'$binding'";
                        }
                    }
                }

                // Insert bindings into query
                $query = '[ SQL ]             : [ '.str_replace(array('%', '?'), array('%%', '%s'), $sql->sql).' ]';
                $time  = '[ TIME ]            : [ '.$sql->time.'ms ]'."\n";
                $query = vsprintf($query, $sql->bindings);
                // Save the query to file
                if($user)
                    $logFile = fopen(storage_path('logs/' . date('Y-m-d').'__UserID_'.$user->id.'.log'), 'a+');
                else
                    $logFile = fopen(storage_path('logs/' . date('Y-m-d').'__guest.log'), 'a+');

                fwrite($logFile,  $query . "\n" . $time . "\n");
                fclose($logFile);
            }
        );
    }
    public static function http($user)
    {
        $request = Request();
        $param = $request->all();

        if(method_exists($request->route(),'parameters')):
            if(!$param)
                $param = $request->route()->parameters();

            $str = "\n\n".'[ '.date('Y-m-d H:i:s') . ' ] '."\n";
            $str .= '[ HTTP ]            : [ ' . $request->route()->uri() . ' ]'."\n";
            $str .= '[ ACTION ]          : [ ' . $request->route()->getAction()['uses']. ' ]'."\n";
            $str .= '[ METHOD ]          : '.json_encode($request->route()->getMethods())."\n";
            $str .= '[ PARAMETERS ]      : '.json_encode($param)."\n";
            if($user)
                $logFile = fopen(storage_path('logs/' . date('Y-m-d').'__UserID_'.$user->id.'.log'), 'a+');
            else
                $logFile = fopen(storage_path('logs/' . date('Y-m-d').'__guest.log'), 'a+');

        else:
            $str = "\n\n".'[ '.date('Y-m-d H:i:s') . ' ] '."\n";
            $str .= '[ Commande ]            : [ @aziz please code it ]'."\n";
            $logFile = fopen(storage_path('logs/' . date('Y-m-d').'__ssh.log'), 'a+');
        endif;

        fwrite($logFile, $str . "\n");
        fclose($logFile);

    }
}