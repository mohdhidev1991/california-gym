<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 27/02/16
 * Time: 07:13 م
 */
class MfkHelper
{
    public static function mkfDecode($mfk,$table=null,$field=null){
        if(!$mfk OR empty($mfk)) return [];

        $items =  explode(',',trim($mfk, ","));
        $i = 0;
        foreach($items as $item){
            if( is_numeric($item) ) $item = (int)$item;
            $items[$i] = $item;
            $i++;
        }
        return $items;
    }
    /* This is added because the first is not good alias but used in many code cources */
    public static function mfkDecode($mfk,$table=null,$field=null){
        return self::mkfDecode($mfk,$table,$field);
    }

    public static function mfkEncode(array $data)
    {
        return ','.implode(',',$data).',';
    }


    public static function mfkIdsDecode($data)
    {
        if(empty($data)) return [];
        return array_values(array_filter(array_map('intval', explode(',', $data))));
    }

    public static function unsetMfkElement($element,$data)
    {
        foreach ($data as $k=>$v){
            if($v == $element)
                unset($data[$k]);
        }

        $str = MfkHelper::mfkEncode($data);
        return $str;
    }
}