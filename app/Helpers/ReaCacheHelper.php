<?php

function set_cache($key, $data){
	return ReaCacheHelper::set($key, $data);
}

function get_cache($key){
	return ReaCacheHelper::get($key);
}

function delete_cache($key){
	return ReaCacheHelper::del($key);
}
function unset_cache($key){
	return ReaCacheHelper::del($key);
}
function flush_cache(){
	return ReaCacheHelper::flush();
}


class ReaCacheHelper
{
	function __construct()
	{

	}

	public static function set($key,$data)
	{
		$redis = Illuminate\Support\Facades\Redis::connection();
		if( is_array($data) OR is_object($data) ){
			$data = json_encode($data);
		}
        $redis->set($key,$data);

        return true;
	}


	public static function get($key)
	{
		$redis = Illuminate\Support\Facades\Redis::connection();
		$data = $redis->get($key);

		$data_json = json_decode($data);
		

		if (json_last_error() != JSON_ERROR_NONE) return $data;
		
        return $data_json;
	}


	public static function del($key)
	{
		$redis = Illuminate\Support\Facades\Redis::connection();
		$redis->del($key);
	}

	public static function flush()
	{
		$redis = Illuminate\Support\Facades\Redis::connection();
		$keys = $redis->keys("*");
        $count = 0;
        foreach ($keys as $key) {
            $redis->del($key);
            $count++;
        }      
        return $count;
	}
}


