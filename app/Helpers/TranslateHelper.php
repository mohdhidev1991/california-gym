<?php

function translate($module, $string, $params = [], $lang = null){
	return TranslateHelper::translate($module, $string, $params, $lang);
}


class TranslateHelper
{
	function __construct()
	{

	}
	public static function translate($module, $string, $params = [], $lang = null)
	{
		return \App\Libraries\Factories\TranslateFactory::translate($module, $string, $params, $lang);
	}
}


