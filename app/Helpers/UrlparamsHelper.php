<?php


function decode_url_params($params){
	$params = explode(',', $params);
	$parametres = [];
	foreach ($params as $param){
		$param = explode('=',$param);
		if(isset($param[0]) AND isset($param[1])){
			$key = urldecode($param[0]);
			$value = urldecode($param[1]);
			$parametres[$key] = $value;
		}
		if(isset($param[0]) AND !isset($param[1])){
			$key = urldecode($param[0]);
			$parametres[$key] = true;
		}
	}
	return $parametres;
}