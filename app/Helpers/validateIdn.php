<?php
/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 16/03/16
 * Time: 04:47 م
 */

namespace App\Helpers;


class validateIdn {

    var $stype = [
            'ar' => [
                1 => "هوية وطنية سعودية",
                2 => "رقم إقامة لغير السعوديين"
            ],
            'en'=>[
                1 => "Saudi National ID",
                2 => "Non-Saudi Residnent ID (Iqama)"
            ]
        ];

    var $hl = "ar";
    
    function validnateSAID($hl = "ar"){
        $this->hl = $hl;
    }
    
    function scheck($idn){
        return $this->stype[$this->hl][$this->check($idn)];
    }

    static function check($idn){
        $idn = trim($idn);

        if(!is_numeric($idn)) return false;

        if(strlen($idn) !== 10) return false;


        

        $type = substr ( $idn, 0, 1 );
        if($type != 2 && $type != 1 ) return false;


        $sum=0;
        for( $i = 0 ; $i<10 ; $i++ ) {
            //echo "  $idn <b>"."</b> -";
            if ( $i % 2 == 0){
                $ZFOdd = str_pad ( ( substr($idn, $i, 1) * 2 ), 2, "0", STR_PAD_LEFT );
                $sum += substr ( $ZFOdd, 0, 1 ) + substr ( $ZFOdd, 1, 1 );
            }else{
                $sum += substr ( $idn, $i, 1 );
            }
        }
        

        //echo $sum;
        return $sum%10 ? false : $type;
    }
}

