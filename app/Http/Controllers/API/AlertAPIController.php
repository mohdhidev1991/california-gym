<?php namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Libraries\Factories\AppFactory;
use App\Libraries\Factories\ReaUserFactory;
use App\Libraries\Repositories\AlertRepository;
use App\Models\Alert;
use App\Models\AlertUser;
use App\Models\ReaUser;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Input;
use Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;



class AlertAPIController extends AppBaseController
{
	/** @var  AlertRepository */
	private $alertRepository;
	private $lang_id = null;
	private $school_id = null;
	private $group_num = null;


	function __construct(AlertRepository $alertRepo)
	{
		parent::__construct();
		$this->alertRepository = $alertRepo;
	}


	public function getReceivers(Request $request)
	{
		$alert_receivers = config('lookup.alert_receivers');
		$receivers = [];
		foreach($alert_receivers as $key => $receiver){
			$receivers[] = ['lookup_code' => $key, 'receiver_name' => $receiver];
		}
		return $this->sendResponse(['alert_receivers' => $receivers], Lang::get('alerts.success.get_alert_receivers'), true);
	}


	function prepare_teacher_data(){

      $data_rea_user = get_cache('rea_user:'.$this->user->id);
      

      if(
        !isset($data_rea_user->current_school)
        OR !isset($data_rea_user->current_school->school_employee_id)
        OR !isset($data_rea_user->current_school->group_num)
        OR !isset($data_rea_user->current_school->school_id)
      ){
        return false;
      }

      $this->lang_id = $this->user->lang_id;
      $this->lang = $data_rea_user->lang;
      $this->school_employee_id = (isset($data_rea_user->current_school->school_employee_id)) ? $data_rea_user->current_school->school_employee_id : null;
      $this->group_num = (isset($data_rea_user->current_school->group_num)) ? $data_rea_user->current_school->group_num : null;
      $this->school_id = (isset($data_rea_user->current_school->school_id)) ? $data_rea_user->current_school->school_id : null;
      $this->session_status = config('lookup.session_status');

      return true;
   }
	private function validateAlertAttribute($alert){

		if(!isset($alert['year']) || empty($alert['year']))
			return $this->sendResponse(null, 'alert.errors.alert_year_is_required', false);	
		
		if(!isset($alert['hday_num']) || empty($alert['hday_num']))
			return $this->sendResponse(null, 'alert.errors.alert_hday_num_is_required', false);	
		
		if(!isset($alert['alert_type_id']) || empty($alert['alert_type_id']))
			return $this->sendResponse(null, 'alert.errors.alert_type_id_is_required', false);	
		
		if(!isset($alert['alert_cat_id']) || empty($alert['alert_cat_id']))
			return $this->sendResponse(null, 'alert.errors.alert_type_id_is_required', false);	
		
		if(!isset($alert['alert_type_name_ar']) || empty($alert['alert_type_name_ar']))
			return $this->sendResponse(null, 'alert.errors.alert_type_name_ar_is_required', false);	
		
		if(!isset($alert['alert_type_name_en']) || empty($alert['alert_type_name_en']))
			return $this->sendResponse(null, 'alert.errors.alert_type_name_en_is_required', false);	
		
		if(!isset($alert['alert_type_desc_ar']) || empty($alert['alert_type_desc_ar']))
			return $this->sendResponse(null, 'alert.errors.alert_type_desc_ar_is_required', false);			
		
		if(!isset($alert['alert_type_desc_en']) || empty($alert['alert_type_desc_en']))
			return $this->sendResponse(null, 'alert.errors.alert_type_desc_en_is_required', false);			
			
		return true;
	}

	private function requestValidation(Request $request){
		
		$alert_receivers = config('lookup.alert_receivers');
		$alert_receivers = collect($alert_receivers)->keys()->all();
		
		extract($request->all());
		
		if(!isset($receiver))
			return $this->sendResponse(null, 'alert.errors.receiver_is_required', false);
		
		if(!in_array($receiver,$alert_receivers))
			return $this->sendResponse(null, 'alert.errors.receiver_not_defined', false);
		
		if(!isset($alert))
			return $this->sendResponse(null, 'alert.errors.alert_is_required', false);


		return true;

	}


	public function sendByReceiver(Request $request)
	{
		$requestValidation = $this->requestValidation($request);
		
		if( $requestValidation != true)
			return $this->requestValidation($request);

		extract($request->all());

		if(isset($alert)){
			$alert['hday_num'] = AppFactory::get_current_hday_num();
			$alert['year'] = ReaUserFactory::get_current_school_year();
		}

		if (!$this->validateAlertAttribute($alert))
			return $this->validateAlertAttribute($alert);

		switch($receiver){
			case Receiver_Enum_Student :

				if(!isset($student_id) || empty($student_id))
					return $this->sendResponse(null, 'alert.errors.student_id_is_required', false);

				$devices = ReaUser::getByStudentId($student_id);
				break;

			case Receiver_Enum_LevelClass :

				if(!isset($level_class_id) || empty($level_class_id))
					return $this->sendResponse(null, 'alert.errors.level_class_id_is_required', false);

				if(!isset($school_id) || empty($school_id))
					return $this->sendResponse(null, 'alert.errors.school_id_is_required', false);

				$devices = ReaUser::getByLevelClassId($level_class_id,$school_id,$alert['year']);
				break;

			case Receiver_Enum_LevelClassAndSymbol :

				if(!isset($level_class_id) || empty($level_class_id))
					return $this->sendResponse(null, 'alert.errors.level_class_id_is_required', false);

				if(!isset($school_id) || empty($school_id))
					return $this->sendResponse(null, 'alert.errors.school_id_is_required', false);

				if(!isset($symbol) || empty($symbol))
					return $this->sendResponse(null, 'alert.errors.symbol_is_required', false);

				$devices = ReaUser::getByLevelClassIdAndSymbol($level_class_id,$symbol,$school_id,$alert['year']);
				break;

			case Receiver_Enum_School :
				if(!isset($school_id) || empty($school_id))
					return $this->sendResponse(null, 'alert.errors.school_id_is_required', false);

				$devices = ReaUser::getBySchoolIdAndYear($school_id,$alert['year']);
				break;

			case Receiver_Enum_Teacher :
				if(!isset($school_id) || empty($school_id))
					return $this->sendResponse(null, 'alert.errors.school_id_is_required', false);

				if(!isset($teacher_id) || empty($teacher_id))
					return $this->sendResponse(null, 'alert.errors.teacher_id_is_required', false);

				$devices = ReaUser::getByTeacherId($teacher_id,$school_id,$alert['year']);
				break;

			case Receiver_Enum_TeacherAndLevelClass :
				if(!isset($level_class_id) || empty($level_class_id))
					return $this->sendResponse(null, 'alert.errors.level_class_id_is_required', false);

				if(!isset($school_id) || empty($school_id))
					return $this->sendResponse(null, 'alert.errors.school_id_is_required', false);

				if(!isset($teacher_id) || empty($teacher_id))
					return $this->sendResponse(null, 'alert.errors.teacher_id_is_required', false);

				$devices = ReaUser::getByTeacherIdAndLevelClassId($teacher_id,$level_class_id,$school_id,$alert['year']);
				break;
		}

		if(count($devices)==0)
			return $this->sendResponse(null,['alert.errors.emptyResults']);

		$devices = $this->reformatDevices($devices,$alert);
		$response = Alert::saveNotification($devices);
		
		if ($response == true){
			return $this->sendResponse(null,'alert.success.process_with_success');
		}else
			return $response;
	}

	private function getAlertDesc($student_name,$desc,$course_session_name=null)
	{
		$str = str_replace('{{$student_name}}',$student_name,$desc);
		$str = str_replace('{{$course_session}}',$course_session_name,$str);
		return $str;
	}
	
	
	private function getAndroidMessage($title,$desc){
		$to = "/notifications";
		$notification = [
			'to' => $to,
			'data' => [
				'title' => $title,
				'message'	=> $desc
			]
		];
		return $notification;
	}
	
	
	private static function getIPhoneMessage($title,$desc,$student_name){
		$notification = [];
		return $notification;
	}
	
	
	private function reformatDevices($devices,$alert){
		
		foreach ($devices as $key => $device){
			$devices[$key]['alert_type_id'] = $alert['alert_type_id'];
			$devices[$key]['alert_type_name_ar'] = $alert['alert_name_ar'];
			$devices[$key]['alert_type_name_en'] = $alert['alert_name_en'];
			$devices[$key]['alert_type_desc_ar'] = $this->getAlertDesc($device['student_name'],$alert['alert_desc_ar']);
			$devices[$key]['alert_type_desc_en'] = $this->getAlertDesc($device['student_name'],$alert['alert_desc_en']);
			$devices[$key]['alert_cat_id'] = $alert['alert_cat_id'];
			$devices[$key]['year'] = $alert['year'];
			$devices[$key]['hday_num'] = $alert['hday_num'];
		}
		$devices = collect($devices)->groupBy('student_id')->toArray();
		return $devices;
	}


	public function getMyAlerts(){
		$alerts = AlertUser::select(
			DB::raw('CONCAT(rea_user.firstname," ",rea_user.f_firstname," ",rea_user.lastname) AS owner_name'),
			DB::raw('CONCAT(student.firstname," ",student.f_firstname," ",student.lastname) AS student_name'),
			'receiver_enum',
			'alert.group_num',
			'alert.school_id',
			'alert.owner_id',
			'alert.year',
			'alert.hday_num',
			'alert.alert_time',
			'alert.alert_num',
			'alert.alert_name_ar',
			'alert.alert_name_en',
			'alert.alert_text_ar',
			'alert.alert_text_en',
			'alert.alert_cat_id',
			DB::raw('hday.hday_date AS alert_hdate'),
			'alert_user.student_id',
			'alert_user.alert_read',
			'alert_user.alert_read_hdate',
			'alert_user.alert_read_time'
		)
			->join('alert',function($join){
				$join->on('alert.group_num','=','alert_user.group_num');
				$join->on('alert.school_id','=','alert_user.school_id');
				$join->on('alert.owner_id','=','alert_user.owner_id');
				$join->on('alert.year','=','alert_user.year');
				$join->on('alert.hday_num','=','alert_user.hday_num');
				$join->on('alert.alert_time','=','alert_user.alert_time');
				$join->on('alert.alert_num','=','alert_user.alert_num');
			})
			->join('hday',function($join){
				$join->on('hday.hday_num','=','alert.hday_num');
				$join->on('hday.school_year_id','=',DB::raw(ReaUserFactory::get_current_school_year_id()));
			})
			->join('rea_user','rea_user.id','=','alert.owner_id')
			->join('student','student.id','=','alert_user.student_id')
			->where('alert.group_num',ReaUserFactory::get_group_num())
			->where('alert.school_id',ReaUserFactory::get_current_school_id())
			->where('alert.year',ReaUserFactory::get_current_school_year())
			->where('alert_user.parent_id',$this->user->id)
			->orderBy('alert.hday_num','desc')
			->orderBy('alert.alert_time','desc')
			->take(10)
			->get();

		$total = count($alerts);
		if($total > 0)
			return $this->sendResponse($alerts,['alerts.success.getMyAlertsWithSuccess'],true,$total);
		else
			return $this->sendResponse(null,['alerts.errors.EmptygetMyAlerts'],true,$total);

	}

	public function setAlertReaded(Request $request)
	{
		if(!isset($request->owner_id) || empty($request->owner_id)){
			return $this->sendResponse(null,['alert.errors.ownerIdIsRequired'],false);
		}

		if(!isset($request->alert_time) || empty($request->alert_time)){
			return $this->sendResponse(null,['alert.errors.alertTimeIsRequired'],false);
		}

		if(!isset($request->student_id) || empty($request->student_id)){
			return $this->sendResponse(null,['alert.errors.studentIdIsRequired'],false);
		}
		if(!isset($request->alert_num) || empty($request->alert_num)){
			return $this->sendResponse(null,['alert.errors.alertNumIsRequired'],false);
		}
		if(!isset($request->hday_num) || empty($request->hday_num)){
			return $this->sendResponse(null,['alert.errors.hdayNumIsRequired'],false);
		}
		if(!isset($request->year) || empty($request->year)){
			return $this->sendResponse(null,['alert.errors.yearIsRequired'],false);
		}
		$alert = [
			'alert_read' => "Y",
			'alert_read_hdate' => AppFactory::get_current_hdate(),
			'alert_read_time' => date('His')
		];
		$query = AlertUser::where('group_num',ReaUserFactory::get_group_num())
				->where('parent_id',$this->user->id);

		if(!isset($request->all) || (isset($request->all) && boolval($request->all)==false) )
			$query->where('hday_num',$request->hday_num)
				->where('year',$request->year)
				->where('owner_id',$request->owner_id)
				->where('alert_time',$request->alert_time)
				->where('alert_num',$request->alert_num);

		if(!isset($request->all) || (isset($request->all) && boolval($request->all)==false) )
			$query->where('student_id',$request->student_id);


		$update = $query->update($alert);

		if($update)
			return $this->sendResponse(null,['alert.success.updateAlertWithSuccess']);
		else
			return $this->sendResponse(null,['alert.errors.updateAlertfailed'],false);
	}
}
