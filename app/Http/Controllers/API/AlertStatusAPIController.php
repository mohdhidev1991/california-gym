<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AlertStatusRepository;
use App\Models\AlertStatus;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class AlertStatusAPIController extends AppBaseController
{
	/** @var  AlertStatusRepository */
	private $alertStatusRepository;

	function __construct(AlertStatusRepository $alertStatusRepo)
	{
        parent::__construct();
		$this->alertStatusRepository = $alertStatusRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = AlertStatus::select([
            'alert_status.*'
        ]);

        if(isset($alert_status_id)){
            $query->where('alert_status.id',$alert_status_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('alert_status.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('alert_status.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['alert_statuss' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }

    function filter_item_after_get(&$item){
    }




    public function save(Request $request)
    {
        $data = $request->get('alert_status');

        if(!$data){
            return $this->sendResponse(null, Lang::get('alert_status.errors.empty_alert_status_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $alert_status = new AlertStatus;
        }else{
            $alert_status = AlertStatus::find($data['id']);
        }


        if(!$alert_status){
            return $this->sendResponse(null, Lang::get('alert_status.errors.alert_status_not_existe'), false);
        }

        if(isset($data['active'])) $alert_status->active = $data['active'];
        if(isset($data['lookup_code'])) $alert_status->lookup_code = $data['lookup_code'];
        if(isset($data['alert_status_name_ar'])) $alert_status->alert_status_name_ar = $data['alert_status_name_ar'];
        if(isset($data['alert_status_name_en'])) $alert_status->alert_status_name_en = $data['alert_status_name_en'];
        $alert_status->save();

        return $this->sendResponse($alert_status, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $alert_status_id = $request->get('alert_status_id');

        if(!$alert_status_id){
            return $this->sendResponse(null, Lang::get('alert_status.errors.empty_alert_status_id'), false);
        }

        $destroy = AlertStatus::destroy($alert_status_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('alert_status.errors.alert_status_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
