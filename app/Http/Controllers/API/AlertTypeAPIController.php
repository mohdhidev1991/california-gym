<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AlertTypeRepository;
use App\Models\AlertType;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class AlertTypeAPIController extends AppBaseController
{
	/** @var  AlertTypeRepository */
	private $alertTypeRepository;

	function __construct(AlertTypeRepository $alertTypeRepo)
	{
		$this->alertTypeRepository = $alertTypeRepo;
	}

    public function get($params = null)
    {
        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }

        

        $query = AlertType::select([
            'alert_type.*'
        ]);

        
        if(isset($alert_type_id)){
            $query->where('alert_type.id',$alert_type_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('alert_type.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }


        $query->orderBy('alert_type.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }

        $count = $query->count();
        $return = ['alert_types' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }




    public function save(Request $request)
    {
        $data = $request->get('alert_type');

        if(!$data){
            return $this->sendResponse(null, Lang::get('alert_type.errors.empty_alert_type_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $alert_type = new AlertType;
        }else{
            $alert_type = AlertType::find($data['id']);
        }


        if(!$alert_type){
            return $this->sendResponse(null, Lang::get('alert_type.errors.alert_type_not_existe'), false);
        }

        if(isset($data['active'])) $alert_type->active = $data['active'];
        if(isset($data['lookup_code'])) $alert_type->lookup_code = $data['lookup_code'];
        if(isset($data['alert_type_name_ar'])) $alert_type->alert_type_name_ar = $data['alert_type_name_ar'];
        if(isset($data['alert_type_name_en'])) $alert_type->alert_type_name_en = $data['alert_type_name_en'];
        $alert_type->save();

        return $this->sendResponse($alert_type, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $alert_type_id = $request->get('alert_type_id');

        if(!$alert_type_id){
            return $this->sendResponse(null, Lang::get('alert_type.errors.empty_alert_type_id'), false);
        }

        $destroy = AlertType::destroy($alert_type_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('alert_type.errors.alert_type_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
