<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AlertUserRepository;
use App\Models\AlertUser;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class AlertUserAPIController extends AppBaseController
{
	/** @var  AlertUserRepository */
	private $alertUserRepository;

	function __construct(AlertUserRepository $alertUserRepo)
	{
		$this->alertUserRepository = $alertUserRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = AlertUser::select([
            'alert_user.*'
        ]);

        if(isset($alert_user_id)){
            $query->where('alert_user.id',$alert_user_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('alert_user.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('alert_user.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        $count = $query->count();
        return $this->sendResponse($result, [Lang::get('global.success.results')], true,$count);
    }




    public function save(Request $request)
    {
        $data = $request->get('alert_user');

        if(!$data){
            return $this->sendResponse(null, Lang::get('alert_user.errors.empty_alert_user_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $alert_user = new AlertUser;
        }else{
            $alert_user = AlertUser::find($data['id']);
        }


        if(!$alert_user){
            return $this->sendResponse(null, Lang::get('alert_user.errors.alert_user_not_existe'), false);
        }

        if(isset($data['active'])) $alert_user->active = $data['active'];
        if(isset($data['lookup_code'])) $alert_user->lookup_code = $data['lookup_code'];
        if(isset($data['alert_user_name'])) $alert_user->alert_user_name = $data['alert_user_name'];
        $alert_user->save();

        return $this->sendResponse($alert_user, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $alert_user_id = $request->get('alert_user_id');

        if(!$alert_user_id){
            return $this->sendResponse(null, Lang::get('alert_user.errors.empty_alert_user_id'), false);
        }

        $destroy = AlertUser::destroy($alert_user_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('alert_user.errors.alert_user_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
