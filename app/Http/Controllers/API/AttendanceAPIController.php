<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AttendanceRepository;
use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class AttendanceAPIController extends AppBaseController
{
	/** @var  AttendanceRepository */
	private $attendanceStatusRepository;

	function __construct(AttendanceRepository $attendanceStatusRepo)
	{
		parent::__construct();
		$this->attendanceStatusRepository = $attendanceStatusRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = Attendance::select([
            'attendance.*'
        ]);

        if(isset($attendance_id)){
            $query->where('attendance.id',$attendance_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('attendance.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('attendance.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        $count = $query->count();
        $return = ['attendances' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }




    public function save(Request $request)
    {
        $data = $request->get('attendance');

        if(!$data){
            return $this->sendResponse(null, Lang::get('attendance.errors.empty_attendance_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $attendance = new Attendance;
        }else{
            $attendance = Attendance::find($data['id']);
        }


        if(!$attendance){
            return $this->sendResponse(null, Lang::get('attendance.errors.attendance_not_existe'), false);
        }

        if(isset($data['active'])) $attendance->active = $data['active'];
        if(isset($data['att_hdate'])) $attendance->att_hdate = $data['att_hdate'];
        if(isset($data['att_time'])) $attendance->att_time = $data['att_time'];
        if(isset($data['attendance_type_id'])) $attendance->attendance_type_id = $data['attendance_type_id'];
        if(isset($data['rea_user_id'])) $attendance->rea_user_id = $data['rea_user_id'];
        if(isset($data['rejected'])) $attendance->rejected = $data['rejected'];
        if(isset($data['att_comments'])) $attendance->att_comments = $data['att_comments'];
        $attendance->save();

        return $this->sendResponse($attendance, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $attendance_id = $request->get('attendance_id');

        if(!$attendance_id){
            return $this->sendResponse(null, Lang::get('attendance.errors.empty_attendance_id'), false);
        }

        $destroy = Attendance::destroy($attendance_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('attendance.errors.attendance_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
