<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AttendanceStatusRepository;
use App\Models\AttendanceStatus;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class AttendanceStatusAPIController extends AppBaseController
{
	/** @var  AttendanceStatusRepository */
	private $attendanceStatusRepository;

	function __construct(AttendanceStatusRepository $attendanceStatusRepo)
	{
        parent::__construct();
		$this->attendanceStatusRepository = $attendanceStatusRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = AttendanceStatus::select([
            'attendance_status.*'
        ]);

        if(isset($attendance_status_id)){
            $query->where('attendance_status.id',$attendance_status_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('attendance_status.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('attendance_status.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['attendance_statuss' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }

    function filter_item_after_get(&$item){
    }




    public function save(Request $request)
    {
        $data = $request->get('attendance_status');

        if(!$data){
            return $this->sendResponse(null, Lang::get('attendance_status.errors.empty_attendance_status_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $attendance_status = new AttendanceStatus;
        }else{
            $attendance_status = AttendanceStatus::find($data['id']);
        }


        if(!$attendance_status){
            return $this->sendResponse(null, Lang::get('attendance_status.errors.attendance_status_not_existe'), false);
        }

        if(isset($data['active'])) $attendance_status->active = $data['active'];
        if(isset($data['lookup_code'])) $attendance_status->lookup_code = $data['lookup_code'];
        if(isset($data['attendance_status_name_ar'])) $attendance_status->attendance_status_name_ar = $data['attendance_status_name_ar'];
        if(isset($data['attendance_status_name_en'])) $attendance_status->attendance_status_name_en = $data['attendance_status_name_en'];
        $attendance_status->save();

        return $this->sendResponse($attendance_status, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $attendance_status_id = $request->get('attendance_status_id');

        if(!$attendance_status_id){
            return $this->sendResponse(null, Lang::get('attendance_status.errors.empty_attendance_status_id'), false);
        }

        $destroy = AttendanceStatus::destroy($attendance_status_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('attendance_status.errors.attendance_status_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
