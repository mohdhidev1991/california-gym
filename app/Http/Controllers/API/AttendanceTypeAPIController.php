<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AttendanceTypeRepository;
use App\Models\AttendanceType;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class AttendanceTypeAPIController extends AppBaseController
{
	/** @var  AttendanceTypeRepository */
	private $attendanceTypeRepository;

	function __construct(AttendanceTypeRepository $attendanceTypeRepo)
	{
        parent::__construct();
		$this->attendanceTypeRepository = $attendanceTypeRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = AttendanceType::select([
            'attendance_type.*'
        ]);

        if(isset($attendance_type_id)){
            $query->where('attendance_type.id',$attendance_type_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('attendance_type.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('attendance_type.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['attendance_types' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }


    function filter_item_after_get(&$item){
    }



    public function save(Request $request)
    {
        $data = $request->get('attendance_type');

        if(!$data){
            return $this->sendResponse(null, Lang::get('attendance_type.errors.empty_attendance_type_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $attendance_type = new AttendanceType;
        }else{
            $attendance_type = AttendanceType::find($data['id']);
        }


        if(!$attendance_type){
            return $this->sendResponse(null, Lang::get('attendance_type.errors.attendance_type_not_existe'), false);
        }

        if(isset($data['active'])) $attendance_type->active = $data['active'];
        if(isset($data['lookup_code'])) $attendance_type->lookup_code = $data['lookup_code'];
        if(isset($data['attendance_type_name_ar'])) $attendance_type->attendance_type_name_ar = $data['attendance_type_name_ar'];
        if(isset($data['attendance_type_name_en'])) $attendance_type->attendance_type_name_en = $data['attendance_type_name_en'];
        $attendance_type->save();

        return $this->sendResponse($attendance_type, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $attendance_type_id = $request->get('attendance_type_id');

        if(!$attendance_type_id){
            return $this->sendResponse(null, Lang::get('attendance_type.errors.empty_attendance_type_id'), false);
        }

        $destroy = AttendanceType::destroy($attendance_type_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('attendance_type.errors.attendance_type_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
