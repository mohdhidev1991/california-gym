<?php

namespace App\Http\Controllers\API\Auth;

use App\Helpers\HijriDateHelper;
use App\Models\ReaUser;
use Illuminate\Http\Request;
use League\OAuth1\Client\Server\Server;
use App\Http\Requests;
use Illuminate\Support\Facades\Lang;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\Auth\CreateToken;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Socialite;
use App\Providers\AuthServiceProvider;
use Auth;
use Hash;
use App\Libraries\Repositories\ReaUserRepository;
use Illuminate\Support\Facades\DB;
use ReaCacheHelper;
use Laravel\Socialite\Two\FacebookProvider;
use Curl\Curl;
use Abraham\TwitterOAuth\TwitterOAuth;


class AuthAPIController extends AppBaseController
{

    const PHONE_RE = '/(05)\d{8}/';
    private $reaUserRepository;



    public function __construct(ReaUserRepository $reaUserRepository)
    {
        parent::__construct();
        $this->reaUserRepository = $reaUserRepository;
    }




    public function me(Request $request)
    {
        return JWTAuth::parseToken()->authenticate();
    }




    public function authenticate(CreateToken $request)
    {
        $this->middleware('api.auth', ['except' => ['authenticate']]);

        $lang_short_name = $request->get('lang');
        $identifier = $this->decodeCredentialIdentifier($request->get('login'));

        if(!$identifier) {
            $result['status'] = false;
            $result['lang'] = $lang_short_name;
            $result['message'] = 'login should be email or mobile number';
            return response()->json($result, 400);
        }

        $credentials[$identifier]   = $request->get('login');
        $credentials['password']    = $request->get('password');
        $credentials['sim_card_sn'] = $request->get('sim_card_sn');

        // this will remove any null value on the credentials array.
        $credentials = array_filter($credentials);

        try {
            if(!$token = $this->createToken($credentials)) {
                return ['message' => 'auth.invalid_credentials','status'=>false];
            }
        } catch (JWTException $e) {
            return ['message' => 'auth.could_not_create_token', 'status' => false];
        }

        $rea_user = JWTAuth::authenticate($token);
        
        $data_rea_user['token'] = $token;
        $data_rea_user += $this->reaUserRepository->authentification_data($rea_user, $request->get('lang'));
        return $this->sendResponse($data_rea_user,'auth.authentication_with_success');
    }




    protected function decodeCredentialIdentifier($identifier)
    {
        $emailValidator = Validator::make(compact('identifier'), ['identifier' => 'email']);

        if($emailValidator->passes()) {
            return 'email';
        }

        $phoneValidator = Validator::make(compact('identifier'), ['identifier' => 'regex:'.static::PHONE_RE]);

        if($phoneValidator->passes()) {
            return 'mobile';
        }

        return null;

    }




    protected function createToken($credentials)
    {

        if(array_has($credentials, 'sim_card_sn')){
            $token = $this->createTokenFromSimCardSN($credentials);
        }else{
            $token = $this->createTokenFromUsernamePassword($credentials);
        }
        return $token;
    }




    protected function createTokenFromSimCardSN($credentials)
    {

        $user = ReaUser::where('sim_card_sn', $credentials['sim_card_sn'])->first();
        if($user){
            return JWTAuth::fromUser($user);
        }else{
            return null;
        }
    }



    protected function createTokenFromReaUserID($rea_user_id)
    {

        $user = ReaUser::where('id', $rea_user_id)->first();
        if($user){
            return JWTAuth::fromUser($user);
        }else{
            return null;
        }
    }




    protected function createTokenFromUsernamePassword($credentials)
    {
        return JWTAuth::attempt($credentials);
    }


    public function validateToken()
    {
        // Our routes file should have already authenticated this token, so we just return success here
        return self::response()->array(['status' => true])->statusCode(200);
    }




    public function LinkSimCard(Request $request,$sim_card_sn){
        $user = $this->me($request);
        ReaUser::updateSimCardSerialNumber($user->id,$sim_card_sn);
        return [
            'status' => true
        ];
    }




    public function register(UserRequest $request)
    {
        $newUser = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ];
        $user = User::create($newUser);
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }






    public function weblogin(Request $request) {

        $data_request = $request->get('rea_user');

        if( !isset($data_request['username']) OR !isset($data_request['pwd']) ){
            return $this->sendResponse(null, ['User.PleaseFillInAllRequiredFields'], false);
        }

        $rea_user = ReaUser::where('username', $data_request['username'] )
        ->where('active','Y')
        ->first();

        if ($rea_user)
        {   
            if (Hash::check($data_request['pwd'], $rea_user->pwd)){
                $remember = $request->get('rea_user.remember');
                auth()->login($rea_user,$remember);
                $authentification_data = $this->reaUserRepository->authentification_data($rea_user);
                unset_cache('failed_auth:'.$rea_user->id);
                unset_cache('rea_user:'.$rea_user->id);
                return $this->sendResponse($authentification_data, ['User.SuccessLogin']);
            }else{
                $failed = (int)get_cache('failed_auth:'.$rea_user->id);
                $failed++;
                set_cache('failed_auth:'.$rea_user->id,$failed);
                if($failed>2){
                    $rea_user = ReaUser::find($rea_user->id);
                    $rea_user->active = "N";
                    $rea_user->save();
                }
                return $this->sendResponse($failed, ['User.InvalidUserLoginOrPassword'], false);
            }
        }else{
            return $this->sendResponse(null, ['User.InvalidUserLoginOrPassword'], false);
        }
    }



    public function webregister(Request $request, ReaUserRepository $reaUserRepo)
    {

        $data_request = $request->get('rea_user');
        $v = Validator::make($data_request, [
            'email' => 'required|email|unique:rea_user,email',
            'mobile' => 'required|numeric|unique:rea_user,mobile',
            'pwd' => 'required|min:6|max:25',
        ]);
        

        if ( $v->fails() )
        {   
            return $this->sendResponse(['success' => false, 'errors' => $v->messages()], Lang::get('rea_user.error.invlide_data'), true);
        }
        else{
            $rea_user = new ReaUser();
            $rea_user->active = "Y";
            $rea_user->email = $data_request['email'];
            $rea_user->mobile = $data_request['mobile'];
            $rea_user->pwd = bcrypt($data_request['pwd']);
            $rea_user->save();
            DB::table('module_rea_user')->insert(
                ['rea_user_id' => $rea_user->id, 'module_id' => config('lookup.rea_user_module_id')]
            );
            auth()->login($rea_user);
            $this->reaUserRepo = $reaUserRepo;
            $this->reaUserRepo->send_activation_email($rea_user->id);
            return $this->sendResponse(['success' => true], Lang::get('rea_user.success.success_login'), true);
        }
    }

    public function send_remember_password(Request $request, ReaUserRepository $reaUserRepo)
    {
        
        $data = $request->get('rea_user');
        if( !isset($data['email']) ){
            return $this->sendResponse(['success' => false, 'errors' => ['Form.PleaseFillInAllRequiredFields'] ], Lang::get('rea_user.errors.email_not_exists'));
        }

        $rea_user = ReaUser::where('email',$data['email'])->first();
        if(!$rea_user){
            return $this->sendResponse(['success' => false, 'errors' => ['User.TheEmailAddressYouEnteredNotExists'] ], Lang::get('rea_user.errors.email_not_exists'));
        }

        $reaUserRepo->send_remember_password($rea_user->id);
        
        return $this->sendResponse(['success' => true], Lang::get('global.success.send'), true);
    }




    public function fb_redirect()
    {
        return Socialite::driver('facebook')->redirect();   
        
    }


    public function auth_by_fb_token($token, Request $request)
    {
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $url = "https://graph.facebook.com/v2.6/me?access_token=".$token."&fields=name,first_name,last_name,email,verified";
        $curl->get($url);
        $curl->close();

        if ($curl->error) {
            return $this->sendResponse(['success' => false,], Lang::get('auth.errors.bad_fb_token'), true);
        }
        $social_user = json_decode($curl->response);
        $providerUser = [
            'id' => $social_user->id,
            'email' => $social_user->email,
        ];
        if(isset($social_user->first_name)){
            $providerUser['firstname'] = ucfirst($social_user->first_name);
        }
        if(isset($social_user->last_name)){
            $providerUser['lastname'] = ucfirst($social_user->last_name);
        }

        $rea_user = AuthServiceProvider::FbCreateOrGetUser($providerUser);
        $token = $this->createTokenFromReaUserID($rea_user->id);
        try {
            if(!$token = $this->createTokenFromReaUserID($rea_user->id)) {
                return ['message' => 'auth.errors.could_not_create_token', 'status' => false];
            }
        } catch (JWTException $e) {
            return ['message' => 'auth.errors.could_not_create_token', 'status' => false];
        }
        $data_rea_user['token'] = $token;
        $data_rea_user += $this->reaUserRepository->authentification_data($rea_user, $request->get('lang'));
        ReaCacheHelper::set('rea_user:'.$rea_user->id,$data_rea_user);
        return $this->sendResponse($data_rea_user,'auth.success.facebook_authentification');
    }





    


    public function fb_callback()
    {
        $social_user = Socialite::driver('facebook')->fields(['first_name', 'last_name', 'email', 'id'])->user();

        if(!$social_user){
            return $this->sendResponse(['success' => false], Lang::get('auth.errors.invalide_social_authentification'), true);
        }
        $providerUser = [
            'id' => $social_user->getId(),
            'email' => $social_user->getEmail(),
        ];
        if(isset($social_user->user['first_name'])){
            $providerUser['firstname'] = ucfirst($social_user->user['first_name']);
        }
        if(isset($social_user->user['last_name'])){
            $providerUser['lastname'] = ucfirst($social_user->user['last_name']);
        }
        $rea_user = AuthServiceProvider::FbCreateOrGetUser($providerUser);
        auth()->login($rea_user);
        return redirect()->to('/');
    }




    public function google_redirect()
    {
        return Socialite::driver('google')->redirect();   
    }


    public function google_callback()
    {
        $social_user = Socialite::driver('google')->user();
        if(!$social_user){
            return $this->sendResponse(['success' => false], Lang::get('auth.errors.invalide_social_authentification'), true);
        }

        $providerUser = [
            'id' => $social_user->getId(),
            'email' => $social_user->getEmail(),
            //'firstname' => $social_user->getName(),
        ];
        if(isset($social_user->user['name']['givenName'])){
            $providerUser['firstname'] = ucfirst($social_user->user['name']['givenName']);
        }
        if(isset($social_user->user['name']['familyName'])){
            $providerUser['lastname'] = ucfirst($social_user->user['name']['familyName']);
        }

        $rea_user = AuthServiceProvider::GoogleCreateOrGetUser($providerUser);
        auth()->login($rea_user);
        return redirect()->to('/');
    }


    public function auth_by_google_token($token, Request $request)
    {

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Accept', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$token);
        $url = "https://www.googleapis.com/plus/v1/people/me?";
        $curl->get($url);
        $curl->close();
        if ($curl->error) {
            return $this->sendResponse(['success' => false,], Lang::get('auth.errors.bad_google_token'), true);
        }
        $social_user = json_decode($curl->response);
        $providerUser = [
            'id' => $social_user->id,
            'email' => $social_user->emails[0]->value,
        ];
        if(isset($social_user->name->givenName)){
            $providerUser['firstname'] = ucfirst($social_user->name->givenName);
        }
        if(isset($social_user->name->familyName)){
            $providerUser['lastname'] = ucfirst($social_user->name->familyName);
        }

        $rea_user = AuthServiceProvider::GoogleCreateOrGetUser($providerUser);
        $token = $this->createTokenFromReaUserID($rea_user->id);
        try {
            if(!$token = $this->createTokenFromReaUserID($rea_user->id)) {
                return ['message' => 'auth.errors.could_not_create_token', 'status' => false];
            }
        } catch (JWTException $e) {
            return ['message' => 'auth.errors.could_not_create_token', 'status' => false];
        }
        $data_rea_user['token'] = $token;
        $data_rea_user += $this->reaUserRepository->authentification_data($rea_user, $request->get('lang'));
        ReaCacheHelper::set('rea_user:'.$rea_user->id,$data_rea_user);
        return $this->sendResponse($data_rea_user,'auth.success.google_authentification');
    }




    public function twitter_redirect()
    {
        return Socialite::driver('twitter')->redirect();   
    }


    public function twitter_callback()
    {
        $social_user = Socialite::driver('twitter')->user();

        if(!$social_user){
            return $this->sendResponse(['success' => false], Lang::get('auth.errors.invalide_social_authentification'), true);
        }
        $providerUser = [
            'id' => $social_user->getId(),
            'firstname' => $social_user->getName(),
            //'email' => $social_user->getEmail(),
        ];
        $rea_user = AuthServiceProvider::TwitterCreateOrGetUser($providerUser);
        auth()->login($rea_user);
        return redirect()->to('/');
    }

    public function auth_by_twitter_token($token, $secret,Request $request)
    {

        $connection = $this->getConnectionWithAccessToken($token, $secret);
        $twitter_request = $connection->get("account/verify_credentials");

        if (isset($twitter_request->errors)) {
            return $this->sendResponse(['success' => false,], Lang::get('auth.errors.bad_twitter_token_or_secret'), true);
        }

        $social_user = $twitter_request;
        if (!isset($social_user->id)) {
            return $this->sendResponse(['success' => false,], Lang::get('auth.errors.bad_twitter_token_or_secret'), true);
        }
        $providerUser = [
            'id' => $social_user->id,
        ];
        if(isset($social_user->screen_name)){
            $providerUser['firstname'] = $social_user->screen_name;
        }
        $rea_user = AuthServiceProvider::TwitterCreateOrGetUser($providerUser);
        $token = $this->createTokenFromReaUserID($rea_user->id);
        try {
            if(!$token = $this->createTokenFromReaUserID($rea_user->id)) {
                return ['message' => 'auth.errors.could_not_create_token', 'status' => false];
            }
        } catch (JWTException $e) {
            return ['message' => 'auth.errors.could_not_create_token', 'status' => false];
        }
        $data_rea_user['token'] = $token;
        $data_rea_user += $this->reaUserRepository->authentification_data($rea_user, $request->get('lang'));
        ReaCacheHelper::set('rea_user:'.$rea_user->id,$data_rea_user);
        return $this->sendResponse($data_rea_user,'auth.success.twitter_authentification');
    }

    public function getConnectionWithAccessToken($oauth_token, $oauth_token_secret) {
      $connection = new TwitterOAuth(config('services.twitter.client_id'), config('services.twitter.client_secret'), $oauth_token, $oauth_token_secret);
      return $connection;
    }
     
    

}
