<?php

namespace App\Http\Controllers\API;

use App\Helpers\MfkHelper;
use App\Models\Language;
use App\Models\ReaUser;

use App\Models\School;
use App\Models\SchoolJob;
use Dingo\Api\Http\Response\Format\Json;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redis;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\Auth\CreateToken;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use App\Helpers\HijriDateHelper;
use Socialite;
use App\Providers\AuthServiceProvider;
use Auth;
use Hash;
use App\Libraries\Repositories\ReaUserRepository;
use Illuminate\Support\Facades\DB;

class AuthAPIController extends AppBaseController
{

    const PHONE_RE = '/(05)\d{8}/';

    public function __construct()
    {
        parent::__construct();
    }

    public function me(Request $request)
    {
        return JWTAuth::parseToken()->authenticate();
    }

    public function authenticate(CreateToken $request)
    {
        $this->middleware('api.auth', ['except' => ['authenticate']]);

        $lang_short_name = $request->get('lang');
        $identifier = $this->decodeCredentialIdentifier($request->get('login'));

        if(!$identifier) {
            $result['status'] = false;
            $result['lang'] = $lang_short_name;
            $result['message'] = 'login should be email or mobile number';
            return response()->json($result, 400);
        }

        $credentials[$identifier]   = $request->get('login');
        $credentials['password']    = $request->get('password');
        $credentials['sim_card_sn'] = $request->get('sim_card_sn');

        // this will remove any null value on the credentials array.
        $credentials = array_filter($credentials);

        try {
            if(!$token = $this->createToken($credentials)) {
                return ['message' => 'auth.invalid_credentials','status'=>false];
            }
        } catch (JWTException $e) {
            return ['message' => 'auth.could_not_create_token', 'status' => false];
        }

        return $this->prepareResponse($token,$request->get('lang'));
    }

    protected function decodeCredentialIdentifier($identifier)
    {
        $emailValidator = Validator::make(compact('identifier'), ['identifier' => 'email']);

        if($emailValidator->passes()) {
            return 'email';
        }

        $phoneValidator = Validator::make(compact('identifier'), ['identifier' => 'regex:'.static::PHONE_RE]);

        if($phoneValidator->passes()) {
            return 'mobile';
        }

        return null;
    }

    protected function createToken($credentials)
    {

        if(array_has($credentials, 'sim_card_sn')){
            $token = $this->createTokenFromSimCardSN($credentials);
        }else{
            $token = $this->createTokenFromUsernamePassword($credentials);
        }
        return $token;
    }

    protected function createTokenFromSimCardSN($credentials)
    {

        $user = ReaUser::where('sim_card_sn', $credentials['sim_card_sn'])->first();
        if($user){
            return JWTAuth::fromUser($user);
        }else{
            return null;
        }
    }

    protected function createTokenFromUsernamePassword($credentials)
    {
        return JWTAuth::attempt($credentials);
    }

    protected function prepareResponse($token,$lang_short_name)
    {
        $user = JWTAuth::authenticate($token);
        // IF USER DEFAULT LANGUAGE IS NULL
        if($user->lang_id == 0){
            // FIND THE LANGUAGE DEMANDED IN THE LANGUAGE TABLE
            $lang_demanded = Language::getByLookupCode($lang_short_name);
            // IF LANGUAGE DEMANDED IS NOT EXIST IN LANGUAGE TABLE, RETURN THE DEFAULT LANGUAGE SYSTEM
            if(!$lang_demanded)
                $lang = Language::getByLookupCode(config('app.locale'));
            // IF LANGUAGE DEMAMDED IS EXIST IN LANGUAGE TABLE, RETURN THEM
            else
                $lang = $lang_demanded;

            // UPDATE THE USER DEFAULT LANGUAGE WITH THE DEFAULT SYSTEM LANGUAGE OR THE LANGUAGE DEMANDED BY USER
            ReaUser::updateLang($user->id,$lang['id']);//
        }
        // RETURN THE USER DEFAULT LANGUAGE SHORT NAME
        else{
            $lang = Language::getById($user->lang_id);
        }

        $items = School::getSchoolsByUserId($user->id,$lang->lookup_code);
        $schools = $items->each(function ($item)
        {
            $job_title_ids =MfkHelper::mfkIdsDecode($item['school_job_mfk']);
            $job_title = SchoolJob::select('school_job_name_ar','school_job_name_en')
                                    ->whereIn('id',$job_title_ids)->get();
            $item['job_title'] = $job_title;
            unset($item['school_job_mfk']);
        });
        $redis = Redis::connection();

        $result =  [
            'token'         => $token,
            'status'        => true,
            'schools'       => $schools,
            'firstname'     => $user->firstname,
            'f_firstname'   => $user->f_firstname,
            'lastname'      => $user->lastname,
            'lang'          => $lang->lookup_code,
            'hdate'         => HijriDateHelper::getHdateByFormat(date('Ymd'),'hdate'),
            'hdate_long'    => HijriDateHelper::getHdateByFormat(date('Ymd'),'hdate_long'),
            'sim_card_sn'   => $user->sim_card_sn,
        ];
        $storage = $result;
        unset($storage['token']);
        unset($storage['status']);
        $current_school = $schools[0];

        // GET ALL PERMISSIONS OF THE CURRENT USER
        $permissions = DB::table('employee_bf')
                            ->select('bfunction_id')
                            ->where('school_employee_id','=',$user->id)
                            ->groupBy('bfunction_id')
                            ->pluck('bfunction_id');

        $redis->set('user:'.$user->id.':current_school',json_encode($current_school));
        $redis->set('user:'.$user->id.':permissions',json_encode($permissions));

        return $this->sendResponse($result,'auth.authentication_with_success');
    }

    public function validaeToken()
    {
        // Our routes file should have already authenticated this token, so we just return success here
        return self::response()->array(['status' => true])->statusCode(200);
    }

    public function LinkSimCard(Request $request,$sim_card_sn){
        $user = $this->me($request);
        ReaUser::updateSimCardSerialNumber($user->id,$sim_card_sn);
        return [
            'status' => true
        ];
    }

    public function register(UserRequest $request)
    {
        $newUser = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ];
        $user = User::create($newUser);
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }




    public function weblogin(Request $request)
    {


        $data_request = $request->get('rea_user');

        $rea_user = ReaUser::where('email', $data_request['email'] )
        //->where('pwd', bcrypt($data_request['pwd']) )
        ->first();

        if ($rea_user)
        {
            if (Hash::check($data_request['pwd'], $rea_user->pwd)){
                unset_cache('rea_user:'.$rea_user->id);
                $remember = $request->get('rea_user.remember');
                auth()->login($rea_user,$remember);
                return $this->sendResponse(null, ['User.SuccessLogin']);
            }else{
                return $this->sendResponse(null, ['User.FailedPassword'], false);
            }

        }else{
            return $this->sendResponse(null, ['User.FailedLogin'], false);
        }
    }



    public function webregister(Request $request, ReaUserRepository $reaUserRepo)
    {


        $data_request = $request->get('rea_user');


        $v = Validator::make($data_request, [
            'email' => 'required|email|unique:rea_user,email',
            'mobile' => 'required|numeric|unique:rea_user,mobile',
            'pwd' => 'required|min:6|max:25',
        ]);


        if ( $v->fails() )
        {
            return $this->sendResponse(['success' => false, 'errors' => $v->messages()], Lang::get('rea_user.error.invlide_data'), true);
        }
        else{
            $rea_user = new ReaUser();
            $rea_user->active = "Y";
            $rea_user->email = $data_request['email'];
            $rea_user->mobile = $data_request['mobile'];
            $rea_user->pwd = bcrypt($data_request['pwd']);
            $rea_user->save();
            auth()->login($rea_user);
            $this->reaUserRepo = $reaUserRepo;
            $this->reaUserRepo->send_activation_email($rea_user->id);
            return $this->sendResponse(['success' => true], Lang::get('rea_user.success.success_login'), true);
        }
    }






    public function send_remember_password(Request $request, ReaUserRepository $reaUserRepo)
    {

        $data = $request->get('rea_user');

        $rea_user = ReaUser::where('email',$data['email'])->first();
        if(!$rea_user){
            return $this->sendResponse([], Lang::get('rea_user.errors.email_not_exists'), false);
        }

        $reaUserRepo->send_remember_password($rea_user->id);

        return $this->sendResponse(['success' => true], Lang::get('global.success.send'), true);
    }




    public function fb_redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function fb_callback()
    {
        $rea_user = AuthServiceProvider::FbCreateOrGetUser(Socialite::driver('facebook')->user());
        auth()->login($rea_user);
        return redirect()->to('/');
    }




    public function google_redirect()
    {
        return Socialite::driver('google')->redirect();
    }


    public function google_callback()
    {
        $rea_user = AuthServiceProvider::GoogleCreateOrGetUser(Socialite::driver('google')->user());
        auth()->login($rea_user);
        return redirect()->to('/');
    }


    public function twitter_redirect()
    {
        return Socialite::driver('twitter')->redirect();
    }


    public function twitter_callback()
    {
        $rea_user = AuthServiceProvider::TwitterCreateOrGetUser(Socialite::driver('twitter')->user());
        auth()->login($rea_user);
        return redirect()->to('/');
    }


}
