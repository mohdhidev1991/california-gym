<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CandidateStatusRepository;
use App\Models\CandidateStatus;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class CandidateStatusAPIController extends AppBaseController
{
	/** @var  CandidateStatusRepository */
	private $candidateStatusRepository;

	function __construct(CandidateStatusRepository $candidateStatusRepo)
	{
        parent::__construct();
		$this->candidateStatusRepository = $candidateStatusRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = CandidateStatus::select([
            'candidate_status.*'
        ]);

        if(isset($candidate_status_id)){
            $query->where('candidate_status.id',$candidate_status_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('candidate_status.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('candidate_status.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['candidate_statuss' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }

    function filter_item_after_get(&$item){
    }




    public function save(Request $request)
    {
        $data = $request->get('candidate_status');

        if(!$data){
            return $this->sendResponse(null, Lang::get('candidate_status.errors.empty_candidate_status_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $candidate_status = new CandidateStatus;
        }else{
            $candidate_status = CandidateStatus::find($data['id']);
        }


        if(!$candidate_status){
            return $this->sendResponse(null, Lang::get('candidate_status.errors.candidate_status_not_existe'), false);
        }

        if(isset($data['active'])) $candidate_status->active = $data['active'];
        if(isset($data['lookup_code'])) $candidate_status->lookup_code = $data['lookup_code'];
        if(isset($data['candidate_status_name_ar'])) $candidate_status->candidate_status_name_ar = $data['candidate_status_name_ar'];
        if(isset($data['candidate_status_name_en'])) $candidate_status->candidate_status_name_en = $data['candidate_status_name_en'];
        $candidate_status->save();

        return $this->sendResponse($candidate_status, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $candidate_status_id = $request->get('candidate_status_id');

        if(!$candidate_status_id){
            return $this->sendResponse(null, Lang::get('candidate_status.errors.empty_candidate_status_id'), false);
        }

        $destroy = CandidateStatus::destroy($candidate_status_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('candidate_status.errors.candidate_status_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
