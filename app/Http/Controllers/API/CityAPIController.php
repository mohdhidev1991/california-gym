<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CityRepository;
use App\Models\City;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class CityAPIController extends AppBaseController
{
	/** @var  CityRepository */
	private $cityRepository;

	function __construct(CityRepository $cityRepo)
	{
		parent::__construct();
		$this->cityRepository = $cityRepo;
	}



	/**
	* Crud methods
	**/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$query = City::select([
			'city.*'
		]);

		if(isset($city_id)){
			$query->where('city.id',$city_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where('city.active',"Y");
		}

		if( isset($country) AND ($country=='local') ){
			$query->where('city.country_id',config('lookup.local_country_id'));
		}
		if( isset($country_id) ){
			$query->where('city.country_id',$country_id);
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('city.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		
	}







	public function save(Request $request)
	{
		$data = $request->get('city');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$city = new City;
		}else{
			$city = City::find($data['id']);	
		}

		
		if(!$city){
			return $this->sendResponse(null, ['City.InvalideCity'], false);
		}

		if(isset($data['active'])) $city->active = $data['active'];
		if(isset($data['city_name_ar'])) $city->city_name_ar = $data['city_name_ar'];
		if(isset($data['city_name_en'])) $city->city_name_en = $data['city_name_en'];
		if(isset($data['country_id'])) $city->country_id = (int)$data['country_id'];

		$city->save();

		return $this->sendResponse($city->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$city_id = $request->get('city_id');

		if(!$city_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = City::destroy($city_id);
		if(!$destroy){
			return $this->sendResponse(null, ['City.InvalideCity'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}

}
