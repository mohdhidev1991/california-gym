<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ClassCourseExamRepository;
use App\Models\ClassCourseExam;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class ClassCourseExamAPIController extends AppBaseController
{
	/** @var  ClassCourseExamRepository */
	private $classCourseExamRepository;

	function __construct(ClassCourseExamRepository $classCourseExamRepo)
	{
        parent::__construct();
		$this->classCourseExamRepository = $classCourseExamRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = ClassCourseExam::select([
            'class_course_exam.*'
        ]);

        if(isset($class_course_exam_id)){
            $query->where('class_course_exam.id',$class_course_exam_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('class_course_exam.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('class_course_exam.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['class_course_exams' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }

    function filter_item_after_get(&$item){
    }




    public function save(Request $request)
    {
        $data = $request->get('class_course_exam');

        if(!$data){
            return $this->sendResponse(null, Lang::get('class_course_exam.errors.empty_class_course_exam_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $class_course_exam = new ClassCourseExam;
        }else{
            $class_course_exam = ClassCourseExam::find($data['id']);
        }


        if(!$class_course_exam){
            return $this->sendResponse(null, Lang::get('class_course_exam.errors.class_course_exam_not_existe'), false);
        }

        if(isset($data['active'])) $class_course_exam->active = $data['active'];
        if(isset($data['class_course_exam_name_ar'])) $class_course_exam->class_course_exam_name_ar = $data['class_course_exam_name_ar'];
        if(isset($data['class_course_exam_name_en'])) $class_course_exam->class_course_exam_name_en = $data['class_course_exam_name_en'];
        $class_course_exam->save();

        return $this->sendResponse($class_course_exam, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $class_course_exam_id = $request->get('class_course_exam_id');

        if(!$class_course_exam_id){
            return $this->sendResponse(null, Lang::get('class_course_exam.errors.empty_class_course_exam_id'), false);
        }

        $destroy = ClassCourseExam::destroy($class_course_exam_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('class_course_exam.errors.class_course_exam_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
