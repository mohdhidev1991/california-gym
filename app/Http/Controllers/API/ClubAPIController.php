<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ClubRepository;
use App\Models\Club;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClubAPIController extends AppBaseController
{
	/** @var  ClubRepository */
	private $clubRepository;

	function __construct(ClubRepository $clubRepo)
	{
		parent::__construct();
		$this->clubRepository = $clubRepo;
	}


	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}


		$model = new Club();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}
		$query = Club::select(
			$model_fields
		);
		$query->where($model->getTable().'.active','<>',"D");

		if(isset($club_id)){
			$query->where($model->getTable().'.id',$club_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		$params = \Request::__get('params');
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		if( isset($join_obj) AND !empty($join_obj) ){
			$join_obj = explode('!', $join_obj);
		}else{
			$join_obj = [];
		}


		if( in_array('room',$join_obj) or in_array('all',$join_obj) ){
			$model_room = new Room();
			$item->rooms = Room::select($model_room->getFillable())->where('club_id',$item->id)->where('active','Y')->get();
		}
	}

	public function save(Request $request)
	{
		$data = $request->get('club');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$club = new Club;
		}else{
			$club = Club::find($data['id']);
		}

		if(!$club){
			return $this->sendResponse(null, ['Club.InvalidClub'], false);
		}

		$model = new Club;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $club->{$item} = $data[$item];
			if( empty($data[$item]) ) $club->{$item} = null;
		}
		$club->save();

		return $this->sendResponse($club->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$club_id = $request->get('club_id');

		if(!$club_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$club = Club::find($club_id);
		if(!$club){
			return $this->sendResponse(null, ['Club.InvalideClub'], false);
		}
		$club->active = 'D';
		$club->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


	public function get_clubs_and_rooms(Request $request) {
		$query = Club::select(
			['id','club_name']
		);
		$query->where('active','Y');
		$query->where('id','<>',12);
		$query->where('id','<>',16);
		$query->orderBy('club_name', 'ASC');
		$clubs = $query->get();
		foreach ($clubs as &$club) {
			$query = Room::select(
				['id','room_name']
			);
			$query->orderBy('room_name', 'ASC');
			$query->where('active','Y');
			$query->where('club_id',$club->id);
			$club->rooms = $query->get();
		}

		return $this->sendResponse($clubs, ['Global.DataDeletedWithSuccess'], true);
	}

	public function get_clubs_and_rooms_platinum(Request $request) {
		$query = Club::select(
			['id','club_name']
		);
		$query->where('active','Y');
		$query->where('id','=',12);
		$query->orderBy('club_name', 'ASC');
		$clubs = $query->get();
		foreach ($clubs as &$club) {
			$query = Room::select(
				['id','room_name']
			);
			$query->orderBy('room_name', 'ASC');
			$query->where('active','Y');
			$query->where('club_id',$club->id);
			$club->rooms = $query->get();
		}

		return $this->sendResponse($clubs, ['Global.DataDeletedWithSuccess'], true);
	}




	public function get_club_with_rooms($club_id, Request $request) {
		$query = Club::select(
			['id','club_name']
		);
		$query->where('active','Y');
		$query->where('id',$club_id);
		$query->orderBy('club_name', 'ASC');
		$clubs = $query->get();
		foreach ($clubs as &$club) {
			$query = Room::select(
				['id','room_name']
			);
			$query->orderBy('room_name', 'ASC');
			$query->where('active','Y');
			$query->where('club_id',$club->id);
			$club->rooms = $query->get();
		}

		return $this->sendResponse($clubs, ['Global.DataDeletedWithSuccess'], true);
	}


}
