<?php namespace App\Http\Controllers\API;
use App\Http\Requests;
use App\Libraries\Repositories\CoachRepository;
use App\Models\Coach;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Helpers\MfkHelper;
use Carbon\Carbon;
use DB;
use Excel;


class CoachAPIController extends AppBaseController
{
	/** @var  CoachRepository */
	private $coachRepository;

	function __construct(CoachRepository $coachRepo)
	{
		parent::__construct();
		$this->coachRepository = $coachRepo;
	}


	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

        $model = new Coach();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}
		$query = Coach::select(
			$model_fields
		);
		
		$query->where($model->table.'.active',"<>","D");

	    if(isset($coach_id)){
            $query->where($model->getTable().'.id',$coach_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if(isset($course_id)){
			$query->where($model->getTable().'.courses_mfk',"LIKE","%,".$course_id.",%");
		}

		if(isset($exclude_coach_id)){
			$query->where($model->getTable().'.id',"<>",$exclude_coach_id);
		}

		if(isset($club_id)){
			$query->where($model->getTable().'.clubs_mfk',"LIKE","%,".$club_id.",%");
		}

		
       if(isset($date_disponibility) AND isset($time_disponibility) AND isset($course_id)){

			$date_disponibility = Carbon::createFromFormat('Y-m-d',$date_disponibility);
			$time_disponibility = Carbon::createFromFormat('H:i',$time_disponibility);
			$course = Course::find($course_id);
			if(!$course){
				return $this->sendResponse(null, ['Course.InvalidCourse'], false);
			}
			$course_ending_time = clone $time_disponibility;
			$course_ending_time = $course_ending_time->addMinutes($course->during);

			//$query->where($model->getTable().'.morning_starting_hour_'.$date_disponibility->format('N'),"LIKE","%,".$date_disponibility->format('N').",%");
			$query->whereRaw(
				"
				(
					(".$model->getTable().".morning_starting_hour_".$date_disponibility->format('N')." <= '".$time_disponibility->format('H:i')."'
					AND ".$model->getTable().".morning_ending_hour_".$date_disponibility->format('N')." >= '".$time_disponibility->format('H:i')."'
					AND ".$model->getTable().".morning_starting_hour_".$date_disponibility->format('N')." <= '".$course_ending_time->format('H:i')."'
					AND ".$model->getTable().".morning_ending_hour_".$date_disponibility->format('N')." >= '".$course_ending_time->format('H:i')."'
					)
					OR
					(".$model->getTable().".afternoon_starting_hour_".$date_disponibility->format('N')." <= '".$time_disponibility->format('H:i')."'
					AND ".$model->getTable().".afternoon_ending_hour_".$date_disponibility->format('N')." >= '".$time_disponibility->format('H:i')."'
					AND ".$model->getTable().".afternoon_starting_hour_".$date_disponibility->format('N')." <= '".$course_ending_time->format('H:i')."'
					AND ".$model->getTable().".afternoon_ending_hour_".$date_disponibility->format('N')." >= '".$course_ending_time->format('H:i')."'
					)
					OR
					(".$model->getTable().".morning_starting_hour_".$date_disponibility->format('N')." <= '".$time_disponibility->format('H:i')."'
					AND ".$model->getTable().".afternoon_ending_hour_".$date_disponibility->format('N')." >= '".$time_disponibility->format('H:i')."'
					AND ".$model->getTable().".morning_starting_hour_".$date_disponibility->format('N')." <= '".$course_ending_time->format('H:i')."'
					AND ".$model->getTable().".afternoon_ending_hour_".$date_disponibility->format('N')." >= '".$course_ending_time->format('H:i')."'
					AND ".$model->getTable().".morning_ending_hour_".$date_disponibility->format('N')." = ".$model->getTable().".afternoon_starting_hour_".$date_disponibility->format('N')."
					)
				)
				"
			);


			
			if(!isset($course_session_id)){
				$course_session_id = "NULL";
			}
			
			$coach_reserved = DB::select(

				DB::raw("SELECT cs.coach_id , cs.secondary_coach_id , cs.triple_coach_id , cs.quad_coach_id , cs.five_coach_id FROM course_session as cs
				WHERE 1
				AND cs.id<>:course_session_id
				AND cs.course_session_year=:course_session_year
				AND cs.course_session_month=:course_session_month
				AND cs.course_session_day=:course_session_day
				AND (
					(
						cs.course_start_time <= :course_start_time1
						AND cs.course_end_time > :course_start_time2
					)
					OR (
						cs.course_start_time < :course_end_time1
						AND cs.course_end_time >= :course_end_time2
					)
				)
				AND cs.active IN ('Y','I')
				/*GROUP BY cs.coach_id*/
				"),
				[
					'course_session_id'=>$course_session_id,
					'course_session_year'=>$date_disponibility->format('Y'),
					'course_session_year'=>$date_disponibility->format('Y'),
					'course_session_month'=>$date_disponibility->format('m'),
					'course_session_day'=>$date_disponibility->format('d'),
					'course_start_time1'=>$time_disponibility->format('H:i'),
					'course_start_time2'=>$time_disponibility->format('H:i'),
					'course_end_time1'=> $course_ending_time->format('H:i'),
					'course_end_time2'=> $course_ending_time->format('H:i'),
				]
			);
			
			/*
			$coach_reserved = collect($coach_reserved)->map(function($x){ return (int)$x->coach_id; })->toArray();
			$query->whereNotIn($model->getTable().'.id',$coach_reserved);
			*/

			$coach_reserved = collect($coach_reserved)->map(function($x){ return $x ;})->toArray();
			if(!$coach_reserved){
				$query->whereNotIn($model->getTable().'.id',$coach_reserved);
			}else
			if($coach_reserved[0]->coach_id != null && $coach_reserved[0]->secondary_coach_id == null && $coach_reserved[0]->triple_coach_id == null && $coach_reserved[0]->quad_coach_id == null && $coach_reserved[0]->five_coach_id == null){
				$query->whereRaw(
					"
					(
						(".$model->getTable().'.id'." != '".$coach_reserved[0]->coach_id."'
						
						)
						
					)
					"
				);
				}else 
			
			if($coach_reserved[0]->coach_id != null && $coach_reserved[0]->secondary_coach_id != null && $coach_reserved[0]->triple_coach_id == null && $coach_reserved[0]->quad_coach_id == null && $coach_reserved[0]->five_coach_id == null){
			$query->whereRaw(
				"
				(
					(".$model->getTable().'.id'." != '".$coach_reserved[0]->coach_id."'
					AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->secondary_coach_id."'
					
					)
					
				)
				"
			);
			}else
			
			if($coach_reserved[0]->coach_id != null && $coach_reserved[0]->secondary_coach_id != null && $coach_reserved[0]->triple_coach_id != null && $coach_reserved[0]->quad_coach_id == null && $coach_reserved[0]->five_coach_id == null){
				$query->whereRaw(
					"
					(
						(".$model->getTable().'.id'." != '".$coach_reserved[0]->coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->secondary_coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->triple_coach_id."'
					
						)
						
					)
					"
				);
			}else

			if($coach_reserved[0]->coach_id != null && $coach_reserved[0]->secondary_coach_id != null && $coach_reserved[0]->triple_coach_id != null && $coach_reserved[0]->quad_coach_id != null && $coach_reserved[0]->five_coach_id == null){
				$query->whereRaw(
					"
					(
						(".$model->getTable().'.id'." != '".$coach_reserved[0]->coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->secondary_coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->triple_coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->quad_coach_id."'
			            
						)
						
					)
					"
				);
			}else
      
			if($coach_reserved[0]->coach_id != null && $coach_reserved[0]->secondary_coach_id != null && $coach_reserved[0]->triple_coach_id != null && $coach_reserved[0]->quad_coach_id != null && $coach_reserved[0]->five_coach_id != null){
				$query->whereRaw(
					"
					(
						(".$model->getTable().'.id'." != '".$coach_reserved[0]->coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->secondary_coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->triple_coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->quad_coach_id."'
						AND ".$model->getTable().'.id'." != '".$coach_reserved[0]->five_coach_id."'
						)
						
					)
					"
				);
			}
			 
			 
			
			
		}
		
	
		if($selectConcept != 0 && $inputName != ''){


			$query->whereRaw(
				"
				(
					(".$model->getTable().'.firstname'." LIKE '%{$inputName}%'
					OR ".$model->getTable().'.lastname'." LIKE '%{$inputName}%'
					)
					AND
					(".$model->getTable().'.courses_mfk'." LIKE '%,{$selectConcept},%'
					)
					
				)
				"
			);
			

		}else if($selectConcept == 0 && $inputName != ''){

			$query->whereRaw(
				"
				(
					(".$model->getTable().'.firstname'." LIKE '%{$inputName}%'
					OR ".$model->getTable().'.lastname'." LIKE '%{$inputName}%'
					)
					
				)
				"
			);

			
	    }else if($selectConcept != 0 && $inputName == ''){

			$query->whereRaw(
				"
				(
					(".$model->getTable().'.courses_mfk'." LIKE '%,{$selectConcept},%'
					)
					
				)
				"
			);
			
	    }

		if(isset($limit)){
			$query->take($limit);
		}else{
			$limit = 100;
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();


		if( isset($export) ){
			
			  
			  $model = 'get_coachs';
			  
			  $Filename = $model;
			  Excel::create($Filename, function($excel) use($result, $model) {
				$excel->sheet($model, function($sheet) use($result) {
					$sheet->fromArray($result);
				});
			})->export('xls');
			exit();
		}
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}
	
	public function getAll()
	{
          $data = DB::select(DB::raw("
		  SELECT *
		  FROM coach as ch
		  WHERE active = 'Y'
		  ORDER BY ch.firstname asc
		  ")
		  );

		  if(!$data){
			return $this->sendResponse(null,['Global.EmptyResults'], false);
		  }
		  return $this->sendResponse($data,['Global.GetDataWithSuccess'], true);
	}


	public function get_coachs($cours_id,$club_id)
	{
		  if ((int)$cours_id != 0){
		  $data = DB::select(DB::raw("
		  SELECT *
		  FROM coach as ch
		  WHERE active = 'Y'
		  AND courses_mfk LIKE '%,{$cours_id},%'
		  AND clubs_mfk LIKE '%,{$club_id},%'
		  ")
		  );
		  }else{
			$data = DB::select(DB::raw("
			SELECT *
			FROM coach as ch
			WHERE active = 'Y'
			AND clubs_mfk LIKE '%,{$club_id},%'
			")
			);
         }



		  if(!$data){
			return $this->sendResponse(null,['Global.EmptyResults'], false);
		  }
		  return $this->sendResponse($data,['Global.GetDataWithSuccess'], true);
	}


	public function get_liste_coachs($primary_coach_id, $secondary_coach_id, $triple_coach_id , $quad_coach_id , $five_coach_id ,Request $request)
	{
		
		

		if((int)$primary_coach_id != 0 && (int)$secondary_coach_id == 0 && (int)$triple_coach_id == 0 && (int)$quad_coach_id == 0 && (int)$five_coach_id == 0 ){
		   
			$params_query[':primary_coach_id'] = (int)$primary_coach_id;
			
			
			$data = DB::select(DB::raw("
			SELECT *
			FROM coach as ch
			WHERE active = 'Y'
			AND ch.id = :primary_coach_id
			"),$params_query
			);

		}else if((int)$primary_coach_id != 0 && (int)$secondary_coach_id != 0 && (int)$triple_coach_id == 0 && (int)$quad_coach_id == 0 && (int)$five_coach_id == 0){
		   
			$params_query[':primary_coach_id'] = (int)$primary_coach_id;
			$params_query[':secondary_coach_id'] = (int)$secondary_coach_id;

			
			
			$data = DB::select(DB::raw("
			SELECT *
			FROM coach as ch
			WHERE active = 'Y'
			AND (ch.id = :primary_coach_id OR ch.id = :secondary_coach_id)
			"),$params_query
			);

		}else if((int)$primary_coach_id != 0 && (int)$secondary_coach_id != 0 && (int)$triple_coach_id != 0 && (int)$quad_coach_id == 0 && (int)$five_coach_id == 0){
		   
			$params_query[':primary_coach_id'] = (int)$primary_coach_id;
			$params_query[':secondary_coach_id'] = (int)$secondary_coach_id;
			$params_query[':triple_coach_id'] = (int)$triple_coach_id;
			
			
			$data = DB::select(DB::raw("
			SELECT *
			FROM coach as ch
			WHERE active = 'Y'
			AND (ch.id = :primary_coach_id OR ch.id = :secondary_coach_id OR ch.id = :triple_coach_id )
			"),$params_query
			);

		}else if((int)$primary_coach_id != 0 && (int)$secondary_coach_id != 0 && (int)$triple_coach_id != 0 && (int)$quad_coach_id != 0 && (int)$five_coach_id == 0){
		   
			$params_query[':primary_coach_id'] = (int)$primary_coach_id;
			$params_query[':secondary_coach_id'] = (int)$secondary_coach_id;
			$params_query[':triple_coach_id'] = (int)$triple_coach_id;
			$params_query[':quad_coach_id'] = (int)$quad_coach_id;
			
			
			$data = DB::select(DB::raw("
			SELECT *
			FROM coach as ch
			WHERE active = 'Y'
			AND (ch.id = :primary_coach_id OR ch.id = :secondary_coach_id OR ch.id = :triple_coach_id OR ch.id = :quad_coach_id )
			"),$params_query
			);

		}else if((int)$primary_coach_id != 0 && (int)$secondary_coach_id != 0 && (int)$triple_coach_id != 0 && (int)$quad_coach_id != 0 && (int)$five_coach_id != 0){
		   
			$params_query[':primary_coach_id'] = (int)$primary_coach_id;
			$params_query[':secondary_coach_id'] = (int)$secondary_coach_id;
			$params_query[':triple_coach_id'] = (int)$triple_coach_id;
			$params_query[':quad_coach_id'] = (int)$quad_coach_id;
			$params_query[':five_coach_id'] = (int)$five_coach_id;

			$data = DB::select(DB::raw("
			SELECT *
			FROM coach as ch
			WHERE active = 'Y'
			AND (ch.id = :primary_coach_id OR ch.id = :secondary_coach_id OR ch.id = :triple_coach_id OR ch.id = :quad_coach_id OR ch.id = :five_coach_id )
			"),$params_query
			);

		}

		



		  if(!$data){
			return $this->sendResponse(null,['Global.EmptyResults'], false);
		  }
		  return $this->sendResponse($data,['Global.GetDataWithSuccess'], true);
	}
	

	public function search($params = null)
    {

	if($params){
      $parametres = decode_url_params($params);
    }
    $params_query = [];
     
	$extra_join_query = null;
	$search_name_coach = $parametres['search_name_coach'];
	$course = $parametres['course'];
	
	$params_query[':search_name_coach'] = $search_name_coach;
	$params_query[':course'] = $course;
	
	$data = DB::select(DB::raw("
    SELECT ch.id , ch.active , ch.firstname , ch.lastname
	FROM coach as ch 
	WHERE (ch.firstname	LIKE '%{$search_name_coach}%' OR ch.lastname LIKE '%{$search_name_coach}%')
	AND (ch.courses_mfk LIKE '%,{$course},%')
	AND (ch.active != 'D')
	ORDER BY ch.id DESC
    ")
    );
	
	
	if( isset($export) ){
			
		$model = 'get_coachs';
		$Filename = $model;
		Excel::create($Filename, function($excel) use($result, $model) {
		  $excel->sheet($model, function($sheet) use($result) {
			  $sheet->fromArray($result);
		  });
	  })->export('xls');
	  exit();
	
	}
	

    if(!$data){
      return $this->sendResponse(null, ['Global.EmptyResults'], false);
    }
    return $this->sendResponse($data, ['Global.GetDataWithSuccess'], true);
	
    }


	function filter_item_after_get(&$item){
		$item->courses = MfkHelper::mfkIdsDecode($item->courses_mfk);
		$item->clubs = MfkHelper::mfkIdsDecode($item->clubs_mfk);
		$item->disponibilities = MfkHelper::mfkIdsDecode($item->disponibility);
	}



	public function save(Request $request)
	{
		$data = $request->get('coach');
		
		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
		
		if( isset($data['new']) AND $data['new']==true ){
			$coach = new Coach;
		}else{
			$coach = Coach::find($data['id']);
		}
		
		if(!$coach){
			return $this->sendResponse(null, ['Coach.InvalidCoach'], false);
		}

		if(
			isset($data['user_id'])
			AND Coach::where('id','<>',$coach->id)->where('user_id',$data['user_id'])->count()
		){
			return $this->sendResponse(null, ['Coach.UserAccountAlreadyUsedByAnotherCoach'], false);
		}

		$model = new Coach;
		$model_fields = $model->getFillable();
		
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $coach->{$item} = $data[$item];
			if( empty($data[$item]) ) $coach->{$item} = null;
		}
		$coach->save();

		return $this->sendResponse($coach->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$coach_id = $request->get('coach_id');

		if(!$coach_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$coach = Coach::find($coach_id);
		if(!$coach){
			return $this->sendResponse(null, ['Coach.InvalideCoach'], false);
		}
		$coach->active = 'D';
		$coach->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
