<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Helpers\MfkHelper;
use App\Libraries\Repositories\CountryRepository;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class CountryAPIController extends AppBaseController
{
	/** @var  CountryRepository */
	private $countryRepository;

	function __construct(CountryRepository $countryRepo)
	{
		parent::__construct();
		$this->countryRepository = $countryRepo;
	}




	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 06/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Country::select([
			'country.*'
		]);

		if(isset($country_id)){
			$query->where('country.id',$country_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('country.active',"Y");
		}
		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('country.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get($item){
		if( isset($item->we_days_mfk) AND !empty($item->we_days_mfk) ){
			$item->we_days = MfkHelper::mfkIdsDecode($item->we_days_mfk);
		}else{
			$item->we_days = [];
		}

		if( isset($item->sa_idn_type_mfk) AND !empty($item->sa_idn_type_mfk) ){
			$item->sa_idn_types = MfkHelper::mfkIdsDecode($item->sa_idn_type_mfk);
		}else{
			$item->sa_idn_types = [];
		}
	}





	public function save(Request $request)
	{
		$data = $request->get('country');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$country = new Country;
		}else{
			$country = Country::find($data['id']);	
		}

		
		if(!$country){
			return $this->sendResponse(null, ['Country.InvalideCountry'], false);
		}

		if(isset($data['active'])) $country->active = $data['active'];
		if(isset($data['country_name_ar'])) $country->country_name_ar = $data['country_name_ar'];
		if(isset($data['country_name_en'])) $country->country_name_en = $data['country_name_en'];
		if(isset($data['time_offset'])) $country->time_offset = $data['time_offset'];
		if(isset($data['maintenance_end_time'])) $country->maintenance_end_time = $data['maintenance_end_time'];
		if(isset($data['maintenance_start_time'])) $country->maintenance_start_time = $data['maintenance_start_time'];
		if(isset($data['date_system_id'])) $country->date_system_id = (int)$data['date_system_id'];
		if(isset($data['abrev'])) $country->abrev = $data['abrev'];
		if(isset($data['nationalty_name_ar'])) $country->nationalty_name_ar = $data['nationalty_name_ar'];
		if(isset($data['nationalty_name_en'])) $country->nationalty_name_en = $data['nationalty_name_en'];
		if(isset($data['we_days_mfk'])) $country->we_days_mfk = $data['we_days_mfk'];
		if(isset($data['sa_idn_type_mfk'])) $country->sa_idn_type_mfk = $data['sa_idn_type_mfk'];
		
		$country->save();

		return $this->sendResponse($country->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$country_id = $request->get('country_id');

		if(!$country_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Country::destroy($country_id);
		if(!$destroy){
			return $this->sendResponse(null, ['Country.InvalideCountry'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
