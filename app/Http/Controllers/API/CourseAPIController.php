<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseRepository;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Helpers\MfkHelper;
use DB;

class CourseAPIController extends AppBaseController
{
	/** @var  CourseRepository */
	private $courseRepository;

	function __construct(CourseRepository $courseRepo)
	{
		parent::__construct();
		$this->courseRepository = $courseRepo;
	}


	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

        
		$model = new Course;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}
		$query = Course::select(
			$model_fields
		);
		$query->where($model->getTable().'.active','<>',"D");

		if(isset($course_id)){
			$query->where($model->getTable().'.id',$course_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}


		if( isset($room_id) ){
			$query->where($model->getTable().'.rooms_mfk','LIKE',"%,".(int)$room_id.",%");
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
        

		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');
        $result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	public function get_cours($room_id)
	{ 
		 
		  $data = DB::select(DB::raw("
		  SELECT *
		  FROM course as cs
		  WHERE active = 'Y'
		  AND rooms_mfk LIKE '%,{$room_id},%'
		  ORDER BY cs.course_name asc
		  ")
		  );

		  if(!$data){
			return $this->sendResponse(null,['Global.EmptyResults'], false);
		  }
		  return $this->sendResponse($data,['Global.GetDataWithSuccess'], true);
	}
	


	public function filter_item_after_get(&$item){
		$params = \Request::__get('params');
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		if( isset($join_obj) AND !empty($join_obj) ){
			$join_obj = explode('!', $join_obj);
		}else{
			$join_obj = [];
		}

		$item->course_fors = MfkHelper::mfkIdsDecode($item->course_for_mfk);
		$item->rooms = MfkHelper::mfkIdsDecode($item->rooms_mfk);

	}



	public function save(Request $request)
	{
		$data = $request->get('course');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$course = new Course;
		}else{
			$course = Course::find($data['id']);
		}

		if(!$course){
			return $this->sendResponse(null, ['Course.InvalidCourse'], false);
		}
		$model = new Course;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $course->{$item} = $data[$item];
			if( empty($data[$item]) ) $course->{$item} = null;
		}
		$course->save();

		return $this->sendResponse($course->id, ['Form.DataSavedWithSuccess'], true);
	}


	public function delete(Request $request)
	{
		$course_id = $request->get('course_id');

		if(!$course_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$course = Course::find($course_id);
		if(!$course){
			return $this->sendResponse(null, ['Course.InvalideCourse'], false);
		}
		$course->active = 'D';
		$course->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
