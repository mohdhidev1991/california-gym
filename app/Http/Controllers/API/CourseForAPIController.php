<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseForRepository;
use App\Models\CourseFor;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CourseForAPIController extends AppBaseController
{
	/** @var  CourseForRepository */
	private $course_forRepository;

	function __construct(CourseForRepository $course_forRepo)
	{
		parent::__construct();
		$this->course_forRepository = $course_forRepo;
	}




	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CourseFor();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}
		$query = CourseFor::select(
			$model_fields
		);

		if(isset($course_for_id)){
			$query->where($model->getTable().'.id',$course_for_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		$query->where($model->getTable().'.active','<>',"D");
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		$params = \Request::__get('params');
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		if( isset($join_obj) AND !empty($join_obj) ){
			$join_obj = explode('!', $join_obj);
		}else{
			$join_obj = [];
		}


		if( in_array('room',$join_obj) or in_array('all',$join_obj) ){
			$model_room = new Room();
			$item->rooms = Room::select($model_room->getFillable())->where('course_for_id',$item->id)->where('active','Y')->get();
		}
	}

	public function save(Request $request)
	{
		$data = $request->get('course_for');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$course_for = new CourseFor;
		}else{
			$course_for = CourseFor::find($data['id']);
		}

		if(!$course_for){
			return $this->sendResponse(null, ['CourseFor.InvalidCourseFor'], false);
		}

		$model = new CourseFor;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $course_for->{$item} = $data[$item];
			if( empty($data[$item]) ) $course_for->{$item} = null;
		}
		$course_for->save();

		return $this->sendResponse($course_for->id, ['Form.DataSavedWithSuccess'], true);
	}


	public function delete(Request $request)
	{
		$course_for_id = $request->get('course_for_id');

		if(!$course_for_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$course_for = CourseFor::find($course_for_id);
		if(!$course_for){
			return $this->sendResponse(null, ['CourseFor.InvalideCourseFor'], false);
		}
		$course_for->active = 'D';
		$course_for->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
