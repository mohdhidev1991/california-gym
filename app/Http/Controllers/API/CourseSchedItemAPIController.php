<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseSchedItemRepository;
use App\Models\CourseSchedItem;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CourseSchedItemAPIController extends AppBaseController
{
	/** @var  CourseSchedItemRepository */
	private $courseSchedItemRepository;

	function __construct(CourseSchedItemRepository $courseSchedItemRepo)
	{
		$this->courseSchedItemRepository = $courseSchedItemRepo;
	}





	/**
	*	Created by Achraf
	*	Created at 07/03/2016
	*	Function key: BF264 / Q0010
	*/
	public function GetItems(Request $request)
	{

		$school_year_id = $request->get('school_year_id',null);
		$level_class_id = $request->get('level_class_id',null);
		$symbol_id = $request->get('symbol_id',null);

		$result = CourseSchedItem::select('wday_id','id','session_order','session_start_time','session_end_time','course_id')
			->where('school_year_id',$school_year_id)
			->where('level_class_id',$level_class_id)
			->where('symbol',$symbol_id)->get();

		$planning = array();
		$i = 1;
		foreach ($result as $item) {
			$planning[$item->wday_id][$item->session_order] = array(
				'id' => $item->id,
				'session_start_time' => $item->session_start_time,
				'session_end_time' => $item->session_end_time,
				'course_id' => $item->course_id,
				);
		}

		if( !$result->count() ){
			return $this->sendResponse(null, Lang::get('course_sched_item.errors.empty_course_sched_item'), false);	
		}

		return $this->sendResponse($planning, Lang::get('course_sched_item.success.valide_course_sched_item'), true);
	}



	/**
	*	Created by Achraf
	*	Created at 08/03/2016
	*	Updated at 05/05/2016
	*	Function key: BF264 / Q0011
	*/
	public function GetDefaultWeekTemplate()
	{

		$result = DB::table('week_template')
		->select('id','week_template_name','day1_template_id','day2_template_id','day3_template_id','day4_template_id','day5_template_id','day6_template_id','day7_template_id')
		->where(function($query){
                 $query->whereNull('school_id');
                 $query->orWhere('school_id', 1);
        	})
		->where(function($query){
                 $query->whereNull('level_class_id');
                 $query->orWhere('level_class_id', 1);
        	})
		->where('active','=','Y')
		->get();

		return $this->sendResponse($result, Lang::get('course_sched_item.success.valide_default_week_template'), true);	
	}



	/**
	*	Created by Achraf
	*	Created at 08/03/2016
	*	Function key: BF264 / Q0012
	*/
	public function CreatFromWeekTemplate(Request $request)
	{
		if( !$request->get('week_template_id')
			OR !$request->get('school_year_id')
			OR !$request->get('level_class_id')
			OR !$request->get('school_class_id')
		){
			return $this->sendResponse(null, 'course_sched_item.errors.empty_params', false);
		}


		$week_template = DB::table('week_template')->where('id',$request->get('week_template_id') )->first();

		for ($wday=1; $wday <= 7; $wday++){
			$num_day_name = "day{$wday}_template_id";
			$user_id = Auth::user()->id;
			$query = "insert into course_sched_item
					(school_year_id,level_class_id, symbol,wday_id,session_order,
					 session_start_time,session_end_time,course_id, 
					 active, version, created_by, created_at) 
					select :school_year_id,:level_class_id,:school_class_id,:wday_{$wday},
					session_order,session_start_time,session_end_time, 
					null, 'Y', 0, :user_id, now() 
					from day_template_item 
					where day_template_id = :day_template_id_{$wday}
					";
			$params_query = [
				":school_year_id" => $request->get('school_year_id'),
				":level_class_id" => $request->get('level_class_id'),
				":school_class_id" => $request->get('school_class_id'),
				":wday_{$wday}" => $wday,
				":user_id" => $user_id,
				":day_template_id_{$wday}" => $week_template->{$num_day_name},
			];
			DB::insert( $query, $params_query );
		}

		
		return $this->sendResponse($query, Lang::get('course_sched_item.success.valide_default_week_template'), true);
	}




	/**
	*	Created by Achraf
	*	Created at 08/03/2016
	*	Function key: used in BF264 / Save Items table
	*/
	public function SaveItems(Request $request)
	{
		$items = $request->get('course_sched_item');

		if( empty($items) ){
			return $this->sendResponse(null, 'course_sched_item.errors.empty_params', false);
		}

		foreach($items as $id => $course_id){
				if(empty($course_id)) $course_id = null;
				CourseSchedItem::where( 'id' ,$id )->update(['course_id' => $course_id ]);
		}
		return $this->sendResponse(null, Lang::get('course_sched_item.success.valide_default_week_template'), true);
	}













	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = CourseSchedItem::select([
			'course_sched_item.*'
		]);

		if(isset($course_sched_item_id)){
			$query->where('course_sched_item.id',$course_sched_item_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('course_sched_item.active',"Y");
		}

		if( isset($school_year_id) ){
			$query->where('school_year_id',$school_year_id);
		}
		if( isset($level_class_id) ){
			$query->where('level_class_id',$level_class_id);
		}
		if( isset($symbol) ){
			$query->where('symbol',$symbol);
		}
		

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('course_sched_item.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach ($result as &$item) {
			$item->symbol = (int)$item->symbol;
			$item->session_order = (int)$item->session_order;
			$item->wday_id = (int)$item->wday_id;
		}
		$count = $query->count();
		$return = ['course_sched_items' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}



	public function save(Request $request)
	{
		$data = $request->get('course_sched_item');

		if(!$data){
			return $this->sendResponse(null, Lang::get('course_sched_item.errors.empty_course_sched_item_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$course_sched_item = new CourseSchedItem;
		}else{
			$course_sched_item = CourseSchedItem::find($data['id']);	
		}

		
		if(!$course_sched_item){
			return $this->sendResponse(null, Lang::get('course_sched_item.errors.course_sched_item_not_existe'), false);
		}

		$course_sched_item->save();

		return $this->sendResponse($course_sched_item, Lang::get('global.success.save'), true);
	}



	public function multisave(Request $request)
	{
		$course_sched_items = $request->get('course_sched_items');

		if(!$course_sched_items){
			return $this->sendResponse(null, Lang::get('course_sched_item.errors.empty_course_sched_items_data'), false);
			
		}
		foreach ($course_sched_items as $item){
			if(!isset($item['id'])){
				return $this->sendResponse(null, Lang::get('course_sched_item.errors.course_sched_item_not_exists'), false);
			}
			$course_sched_item = CourseSchedItem::find($item['id']);
			
			if(!$course_sched_item){
				return $this->sendResponse(null, Lang::get('course_sched_item.errors.course_sched_item_not_exists2'), false);
			}
			if( isset($item['course_id']) ){
				$course_sched_item->course_id = (int)$item['course_id'];
				//var_dump($course_sched_item->course_id);
				//exit();
			}
			$course_sched_item->save();
		}
		return $this->sendResponse(null, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$course_sched_item_id = $request->get('course_sched_item_id');

		if(!$course_sched_item_id){
			return $this->sendResponse(null, Lang::get('course_sched_item.errors.empty_course_sched_item_id'), false);
		}

		$destroy = CourseSchedItem::destroy($course_sched_item_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('course_sched_item.errors.course_sched_item_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}




}
