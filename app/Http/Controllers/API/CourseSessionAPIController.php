<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseSessionRepository;
use App\Models\CourseSession;
use App\Models\CourseSessionFin;
use App\Models\ReaUser;
use App\Models\Coach;
use App\Models\Club;
use App\Models\Room;
use App\Models\Course;
use App\Models\Holiday;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use DB;
use Carbon\Carbon;
use Excel;

class CourseSessionAPIController extends AppBaseController
{
  /** @var  CourseSessionRepository */
  private $course_sessionRepository;

  function __construct(CourseSessionRepository $course_sessionRepo)
  {
    parent::__construct();
    $this->courseSessionRepository = $course_sessionRepo;
  }

  public function get($params = null)
  {
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }
    $model = new CourseSession();
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }
    $query = CourseSession::select(
      $model_fields
    );
    $query->AddSelect(DB::raw("(TIME_FORMAT(TIMEDIFF(".$model->getTable().".course_end_time,".$model->getTable().".course_start_time),'%h,%i')*60) as during"));
    if( isset($actives) ){
      $actives = explode('!', $actives);
      $query->whereIn($model->getTable().'.active',$actives);
    }elseif( !isset($active) OR ($active!='all') ){
      $query->where($model->getTable().'.active',"Y");
    }
    
    if( isset($limit) ){
    $query->take($limit);
    }
    
    
    if(isset($page) AND isset($limit) ){
      $skip = ($page-1)*$limit;
      $query->skip($skip);
    }

    $query->orderBy($model->getTable().'.id', 'ASC');

    if( isset($join) AND !empty($join) ){
      $join = explode('!', $join);
    }else{
      $join = [];
    }

    if( in_array('coach',$join) or in_array('all',$join) ){
      $coach_model = new Coach;
      $query->leftJoin($coach_model->getTable(),$coach_model->getTable().'.id','=',$model->getTable().'.coach_id');
      $query->AddSelect([$coach_model->getTable().'.firstname']);
    }


    if(isset($club_id) AND isset($room_id) ){
      $query->where($model->getTable().'.club_id',(int)$club_id);
      $query->where($model->getTable().'.room_id',(int)$room_id);
    }else{
      return $this->sendResponse(null, ['Global.EmptyData'], false);
    }

    if(isset($week_num) AND isset($week_year)){
      $query->where($model->getTable().'.week_num',(int)$week_num);
      $query->where($model->getTable().'.week_year',(int)$week_year);
    }elseif(isset($course_session_year) AND isset($course_session_month) AND isset($course_session_day)){
      $query->where($model->getTable().'.course_session_year',(int)$course_session_year);
      $query->where($model->getTable().'.course_session_month',(int)$course_session_month);
      $query->where($model->getTable().'.course_session_day',(int)$course_session_day);
    }else{
      return $this->sendResponse(null, ['Global.EmptyData'], false);
    }

    $result = $query->get();
    
    if(!$result){
      return $this->sendResponse(null, ['Global.EmptyResults'], false);
    }
    
    
    foreach($result as $item){
      $this->filter_item_after_get($item);
    }
    
    $total = $query->count();
    $attributes = [
      'cost_per_club' => (isset($actives))? $this->get_cost_per_club($week_year, $week_num, $club_id, ['Y','I']): $this->get_cost_per_club($week_year, $week_num, $club_id, ['Y']),
      'cost_per_room' => (isset($actives))? $this->get_cost_per_club_and_room($week_year, $week_num, $club_id, $room_id, ['Y','I']) : $this->get_cost_per_club_and_room($week_year, $week_num, $club_id, $room_id, ['Y']),
    ];
    
    return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total, $attributes);
  }


  public function get_sessions_cours($club_id, $room_id, $cours_id , $coach_id , $week_num , $week_year ,Request $request) {
    
    $params_query[':club_id'] = $club_id;
    $params_query[':week_num'] = $week_num;
    $params_query[':week_year'] = $week_year;
    
    if($room_id == 0 && $cours_id == 0 && $coach_id == 0){

        $data = DB::select(DB::raw("
        SELECT cs.id , c.course_name as title , r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id 
        FROM course_session as cs , course as c , coach as ch , room as r
        WHERE cs.active = 'Y' 
        AND cs.course_id=c.id
        AND cs.coach_id=ch.id
        AND cs.club_id=:club_id
        AND cs.room_id=r.id
        AND cs.week_num=:week_num
        AND cs.week_year=:week_year
        ORDER BY cs.id ASC
        "),$params_query
        );

    }else if($room_id == 0 && $cours_id != 0 && $coach_id == 0){
      $params_query[':cours_id'] = $cours_id;

      $data = DB::select(DB::raw("
      SELECT cs.id , c.course_name as title ,  r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , course as c , coach as ch , room as r
      WHERE cs.active = 'Y' 
      AND cs.course_id=c.id
      AND cs.coach_id=ch.id
      AND cs.course_id=:cours_id
      AND cs.club_id=:club_id 
      AND cs.room_id=r.id  
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );

    }else if($room_id == 0 && $cours_id == 0 && $coach_id != 0){
      $params_query[':coach_id'] = $coach_id;

      $data = DB::select(DB::raw("
      SELECT cs.id , c.course_name as title , r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , course as c , coach as ch , room as r
      WHERE cs.active = 'Y' 
      AND cs.course_id=c.id
      AND cs.coach_id=ch.id
      AND cs.coach_id=:coach_id
      AND cs.club_id=:club_id
      AND cs.room_id=r.id  
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );

    }else if($room_id == 0 && $cours_id != 0 && $coach_id != 0){

      $params_query[':coach_id'] = $coach_id;
      $params_query[':cours_id'] = $cours_id;

      $data = DB::select(DB::raw("
      SELECT cs.id , c.course_name as title , r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , course as c , coach as ch , room as r
      WHERE cs.active = 'Y' 
      AND cs.course_id=c.id
      AND cs.coach_id=ch.id
      AND cs.coach_id=:coach_id
      AND cs.course_id=:cours_id
      AND cs.club_id=:club_id
      AND cs.room_id=r.id
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );

    
    }else if($room_id != 0 && $cours_id == 0 && $coach_id == 0){
        $params_query[':room_id'] = $room_id;
        $data = DB::select(DB::raw("
        SELECT cs.id , c.course_name as title , r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id 
        FROM course_session as cs , course as c , coach as ch , room as r
        WHERE cs.active = 'Y' 
        AND cs.course_id=c.id
        AND cs.coach_id=ch.id
        AND cs.club_id=:club_id
        AND cs.room_id=r.id
        AND cs.room_id=:room_id
        AND cs.week_num=:week_num
        AND cs.week_year=:week_year
        ORDER BY cs.id ASC
        "),$params_query
        );

    }else if($room_id != 0  && $cours_id != 0 && $coach_id == 0){
      
      $params_query[':room_id'] = $room_id;
      $params_query[':cours_id'] = $cours_id;
      
      $data = DB::select(DB::raw("
      SELECT cs.id , c.course_name as title , r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , course as c , coach as ch , room as r
      WHERE cs.active = 'Y' 
      AND cs.course_id=c.id
      AND cs.coach_id=ch.id
      AND cs.course_id=:cours_id
      AND cs.club_id=:club_id  
      AND cs.room_id=r.id
      AND cs.room_id=:room_id   
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );

    }else if($room_id != 0 && $cours_id == 0 && $coach_id != 0){
      
      $params_query[':room_id'] = $room_id;
      $params_query[':coach_id'] = $coach_id;

      $data = DB::select(DB::raw("
      SELECT cs.id , c.course_name as title , r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , course as c , coach as ch  , room as r
      WHERE cs.active = 'Y' 
      AND cs.course_id=c.id
      AND cs.coach_id=ch.id
      AND cs.coach_id=:coach_id
      AND cs.club_id=:club_id
      AND cs.room_id=r.id
      AND cs.room_id=:room_id
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );

    }else{
      
      $params_query[':room_id'] = $room_id;
      $params_query[':coach_id'] = $coach_id;
      $params_query[':cours_id'] = $cours_id;

      $data = DB::select(DB::raw("
      SELECT cs.id , c.course_name as title , r.room_name as room , c.color as color , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , course as c , coach as ch , room as r
      WHERE cs.active = 'Y' 
      AND cs.course_id=c.id
      AND cs.coach_id=ch.id
      AND cs.coach_id=:coach_id
      AND cs.course_id=:cours_id
      AND cs.club_id=:club_id
      AND cs.room_id=r.id
      AND cs.room_id=:room_id
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );

    }
    
    return $this->sendResponse($data,['Global.DataDeletedWithSuccess'], true);
  }

  public function get_sessions_cours_autres($club_id, $room_id , $coach_id , $week_num , $week_year ,Request $request) {
    
    $params_query[':club_id'] = $club_id;
    $params_query[':week_num'] = $week_num;
    $params_query[':week_year'] = $week_year;
    if($room_id == 0 && $coach_id == 0){
        $data = DB::select(DB::raw("
        SELECT cs.id , cs.description_course as title ,  r.room_name as room , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
        FROM course_session as cs , coach as ch , room as r
        WHERE cs.active = 'Y' 
        AND cs.type_course= 'Autre'
        AND cs.coach_id=ch.id
        AND cs.club_id=:club_id
        AND cs.room_id=r.id
        AND cs.week_num=:week_num
        AND cs.week_year=:week_year
        ORDER BY cs.id ASC
        "),$params_query
        );
    }else if($room_id == 0 && $coach_id != 0){
      $params_query[':coach_id'] = $coach_id;
      $data = DB::select(DB::raw("
      SELECT cs.id , cs.description_course as title , r.room_name as room , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , coach as ch , room as r
      WHERE cs.active = 'Y'
      AND cs.type_course= 'Autre'
      AND cs.coach_id=ch.id
      AND cs.coach_id=:coach_id
      AND cs.club_id=:club_id
      AND cs.room_id=r.id
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );
    }else if($room_id != 0 && $coach_id == 0){
      $params_query[':room_id'] = $room_id;
      $data = DB::select(DB::raw("
      SELECT cs.id , cs.description_course as title , r.room_name as room , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , coach as ch , room as r
      WHERE cs.active = 'Y' 
      AND cs.type_course= 'Autre'
      AND cs.coach_id=ch.id
      AND cs.club_id=:club_id
      AND cs.room_id=r.id
      AND cs.room_id=:room_id
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );
    }else{
      $params_query[':coach_id'] = $coach_id;
      $params_query[':room_id'] = $room_id;
      $data = DB::select(DB::raw("
      SELECT cs.id , cs.description_course as title , r.room_name as room , ch.firstname as firstname , ch.lastname as lastname , cs.course_session_date as date , cs.course_start_time as start , cs.course_end_time as end , cs.primary_coach_id , cs.secondary_coach_id , cs.triple_coach_id ,  cs.quad_coach_id , cs.five_coach_id
      FROM course_session as cs , coach as ch , room as r
      WHERE cs.active = 'Y'
      AND cs.type_course= 'Autre'
      AND cs.coach_id=ch.id
      AND cs.coach_id=:coach_id
      AND cs.club_id=:club_id
      AND cs.room_id=r.id
      AND cs.room_id=:room_id
      AND cs.week_num=:week_num
      AND cs.week_year=:week_year
      ORDER BY cs.id ASC
      "),$params_query
      );
    }
    return $this->sendResponse($data, ['Global.DataDeletedWithSuccess'], true);
  }

  
  public function getbycoach($params = null)
  {
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }

    $model = new CourseSession();
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }
    $query = CourseSession::select(
      $model_fields
    );
    $query->AddSelect(DB::raw("(TIME_FORMAT(TIMEDIFF(".$model->getTable().".course_end_time,".$model->getTable().".course_start_time),'%h,%i')*60) as during"));

    
    
    
    if( isset($actives) ){
      $actives = explode('!', $actives);
      $query->whereIn($model->getTable().'.active',$actives);
    }elseif( !isset($active) OR ($active!='all') ){
      $query->where($model->getTable().'.active',"Y");
    }
    
    if( isset($limit) ){
    $query->take($limit);
    }
    
    if( isset($page) AND isset($limit) ){
      $skip = ($page-1)*$limit;
      $query->skip($skip);
    }

    $query->orderBy($model->getTable().'.id', 'ASC');

    if( isset($join) AND !empty($join) ){
      $join = explode('!', $join);
    }else{
      $join = [];
    }

    if( in_array('coach',$join) or in_array('all',$join) ){
      $coach_model = new Coach;
      $query->leftJoin($coach_model->getTable(),$coach_model->getTable().'.id','=',$model->getTable().'.coach_id');
      $query->AddSelect([$coach_model->getTable().'.firstname']);
    }

    if($club_id != 'null' AND isset($coach_id) ){
      $query->where($model->getTable().'.club_id',(int)$club_id);
      $query->where($model->getTable().'.primary_coach_id',(int)$coach_id);
    }else{
      $query->where($model->getTable().'.primary_coach_id',(int)$coach_id);
    }

    if(isset($week_num) AND isset($week_year)){
      $query->where($model->getTable().'.week_num',(int)$week_num);
      $query->where($model->getTable().'.week_year',(int)$week_year);
    }elseif(isset($course_session_year) AND isset($course_session_month) AND isset($course_session_day)){
      $query->where($model->getTable().'.course_session_year',(int)$course_session_year);
      $query->where($model->getTable().'.course_session_month',(int)$course_session_month);
      $query->where($model->getTable().'.course_session_day',(int)$course_session_day);
    }else{
      return $this->sendResponse(null, ['Global.EmptyData'], false);
    }

    $result = $query->get();
    
    if(!$result){
      return $this->sendResponse(null, ['Global.EmptyResults'], false);
    }
    

    
    foreach($result as $item){
      $this->filter_item_after_get($item);
    }
    
    $total = $query->count();
    
    $attributes=[
    ];
    
    return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total, $attributes);
  
  }


  public function getCoursesAllRoomsHome($params = null)
  {
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }
    
    $model = new CourseSession();
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }
    
    $query = CourseSession::select(
      $model_fields
    );
    
    $query->AddSelect(DB::raw("(TIME_FORMAT(TIMEDIFF(".$model->getTable().".course_end_time,".$model->getTable().".course_start_time),'%h,%i')*60) as during"));
    
    if( isset($actives) ){
      $actives = explode('!', $actives);
      $query->whereIn($model->getTable().'.active',$actives);
    }elseif( !isset($active) OR ($active!='all') ){
      $query->where($model->getTable().'.active',"Y");
    }
    
    if( isset($limit) ){
    $query->take($limit);
    }
    
    if( isset($page) AND isset($limit) ){
      $skip = ($page-1)*$limit;
      $query->skip($skip);
    }

    $query->orderBy($model->getTable().'.id', 'ASC');


    if( isset($join) AND !empty($join) ){
      $join = explode('!', $join);
    }else{
      $join = [];
    }

    if( in_array('coach',$join) or in_array('all',$join) ){
      $coach_model = new Coach;
      $query->leftJoin($coach_model->getTable(),$coach_model->getTable().'.id','=',$model->getTable().'.coach_id');
      $query->AddSelect([$coach_model->getTable().'.firstname']);
    }


    if(isset($club_id) AND isset($room_id) ){
      $query->where($model->getTable().'.club_id',(int)$club_id);
      $query->where($model->getTable().'.room_id',(int)$room_id);
    }else{
      return $this->sendResponse(null, ['Global.EmptyData'], false);
    }
    $today = $date_planing_home; 
    if(isset($week_num) AND isset($week_year)){
      $query->where($model->getTable().'.course_session_date',$today);
    }else{
      return $this->sendResponse(null, ['Global.EmptyData'], false);
    }

    $result = $query->get();
    if(!$result){
      return $this->sendResponse(null, ['Global.EmptyResults'], false);
    }
  
    foreach($result as $item){
      $this->filter_item_after_get($item);
    }


    $total = $query->count();
    $attributes = [
      'cost_per_club' => (isset($actives))? $this->get_cost_per_club($week_year, $week_num, $club_id, ['Y','I']): $this->get_cost_per_club($week_year, $week_num, $club_id, ['Y']),
      'cost_per_room' => (isset($actives))? $this->get_cost_per_club_and_room($week_year, $week_num, $club_id, $room_id, ['Y','I']) : $this->get_cost_per_club_and_room($week_year, $week_num, $club_id, $room_id, ['Y']),
    ];
    
    return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total, $attributes);
  }

  function add_remplissage_item_after_get(&$item){

    $id = $item->ID;
    $result = CourseSessionFin::select('*')->where('coach_session_id', (int)$id)->first();
      if($result){
        if($result->check_validate_remp == 'on'){
          $item->remplissage = '1' ;
        }else{
          $item->remplissage = '' ;
        }
        
      }else{
        $item->remplissage = '' ;
      }
      
      $during = $item->Nbr_heure; 
      if($during >= 45){
        $item->Nbr_heure = '1' ;
      }else{
        $item->Nbr_heure = '1' ;
      }


      
     if($item->Jour < 21){
      $item->MoisPaie = $item->Mois;
     }else if($item->Mois == 12){
      $item->MoisPaie = $item->Mois;
     }
     else{
      $item->MoisPaie = ($item->Mois)+1 ;
     }

      if (setlocale(LC_TIME, 'fr_FR') == '') {
        setlocale(LC_TIME, 'FRA');  //correction problème pour windows
        $format_jour = '%#d';
        } else {
        $format_jour = '%e';
      }
      $date = $item->Date;
      
      $dateLettre = strftime("%A $format_jour %B %Y", strtotime($date));
      $findme = ' ';
      $jourLettre = substr($dateLettre, 0 , strpos($dateLettre, $findme));
      if($jourLettre == 'Monday') {
        $jourLettre = 'lundi';
      } 
      if($jourLettre == 'Tuesday') {
        $jourLettre = 'mardi';
      }
      if($jourLettre == 'Wednesday') {
        $jourLettre = 'mercredi';
      }
      if($jourLettre == 'Thursday') {
        $jourLettre = 'jeudi';
      }
      if($jourLettre == 'Friday') {
        $jourLettre = 'vendredi';
      }
      if($jourLettre == 'Saturday') {
        $jourLettre = 'samedi';
      }
      if($jourLettre == 'Sunday') {
        $jourLettre = 'dimanche';
      }
      $item->Jour = $jourLettre;

      $date_modifie = preg_replace('#([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2})#',"$3/$2/$1", $date);
      $item->Date = $date_modifie;
  
  }

  

  function filter_item_after_get(&$item){

    $params = \Request::__get('params');
    $item->primary_coach_id = (int)$item->primary_coach_id;
    $item->secondary_coach_id = (int)$item->secondary_coach_id;
    $item->coach_id = (int)$item->primary_coach_id;
    $hours_today = date('H:i:s');
    $time_item = $item->course_end_time ;
    $time_item = $time_item.":00";

    $id = $item->id ;
    $result = CourseSessionFin::select('*')->where('coach_session_id', (int)$id)->first();
    if($result){
      $state = $result->state ;
      if($state == 'replace'){
        $item->replace_coach = true;
        $item->set_nbr_participants = $result->nbr_paticipants ;
      }else{
        $item->replace_coach = false;
        $item->set_nbr_participants = $result->nbr_paticipants ;
      }
    }

    if($hours_today > $time_item){
      $item->notif_time_set_nbr_participants = 'skip' ;
      $id = $item->id ;
      $result = CourseSessionFin::select('*')->where('coach_session_id', (int)$id)->first();
      if($result){
      $state = $result->state ;
      if ($state == 'participe'){
        $item->set_nbr_participants = $result->nbr_paticipants ;
        $item->check_validate_remplissage = $result->check_validate_remp;
      }

      if ($state == 'annuler' || $state == 'validerann'){
        $item->color_cancel_cours = '#ddd' ;
        $item->set_nbr_participants = $result->nbr_paticipants ;
      }

      
      }else{
        $item->set_nbr_participants = '' ;
      }
    }else{

      $h1=$time_item;
      $h2=$hours_today;
      $tab=explode(":", $h1); 
      $tab2=explode(":", $h2); 
      $h=$tab[0]; 
      $m=$tab[1]; 
      $s=$tab[2]; 
      $h2=$tab2[0]; 
      $m2=$tab2[1]; 
      $s2=$tab2[2];  
     
      if ($h2>$h) { 
      $h=$h+24; 
      }  
      if ($m2>$m) { 
      $m=$m+60; 
      $h2++; 
      } 
      if ($s2>$s) { 
      $s=$s+60; 
      $m2++; 
      } 
      
      $ht=$h-$h2; 
      $mt=$m-$m2; 
      $st=$s-$s2; 
      if (strlen($ht)==1) { 
      $ht="0".$ht; 
      }  
      if (strlen($mt)==1) { 
      $mt="0".$mt; 
      }  
      if (strlen($st)==1) { 
      $st="0".$st; 
      }
      
      $time_result = $ht.":".$mt;
      if($time_result > "00:10"){
      $item->notif_time_set_nbr_participants = $time_result;
      
      $id = $item->id ;
      $result = CourseSessionFin::select('*')->where('coach_session_id', (int)$id)->first();
      if($result){
      $state = $result->state ;
      
      if ($state == 'participe'){
        $item->set_nbr_participants = $result->nbr_paticipants ;
        $item->check_validate_remplissage = $result->check_validate_remp;
      }
      
      if ($state == 'annuler' || $state == 'validerann'){
        $item->color_cancel_cours = '#ddd' ;
      }
    
      }else{
        $item->set_nbr_participants = '' ;
      }
      }else{

      $id = $item->id ;
      $result = CourseSessionFin::select('*')->where('coach_session_id', (int)$id)->first();
      if($result){
      $state = $result->state ;

      if ($state == 'participe'){
        $item->notif_time_set_nbr_participants = "skip"; 
        $item->set_nbr_participants = $result->nbr_paticipants ; 
        $item->check_validate_remplissage = $result->check_validate_remp;
      }
      
      if ($state == 'annuler' || $state == 'validerann'){
        $item->course->color = '#ddd' ;
      }

      }else{
        $item->notif_time_set_nbr_participants = "notif_active"; 
        $item->set_nbr_participants = 0 ;
      }
      
      }
    
    }
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }


    if(isset($join_obj) AND !empty($join_obj) ){
      $join_obj = explode('!', $join_obj);
    }else{
      $join_obj = [];
    }


    if( in_array('coach',$join_obj) or in_array('all',$join_obj) ){
      $model_coach = new Coach();
      $item->coach = ($item->coach_id)? Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
    }

    if( in_array('club',$join_obj) or in_array('all',$join_obj) ){
      $model_club = new Club();
      $item->club = ($item->club_id)? Club::select($model_club->getFillable())->where('active','Y')->find($item->club_id) : null;
    }

    if( in_array('coach',$join_obj) or in_array('all',$join_obj) ){
      $model_room = new Room();
      $item->room = ($item->room_id)? Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
    }
    if( in_array('course',$join_obj) or in_array('all',$join_obj) ){
      $model_course = new Course();
      $item->course = ($item->course_id)? Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;
    }
  
  }


  public function get_cost_per_club_and_room($week_year, $week_num, $club_id, $room_id, $active = ['Y'])
  {
    $where_active = "(";
    $k_ = 1;
    foreach($active as $i_){
      $where_active .= "'".$i_."'";
      if($k_<count($active)) $where_active .= ",";
      $k_++;
    }
    $where_active .= ")";
    $result = DB::select("
      SELECT sum(cs.cost) as cost FROM course_session as cs
      WHERE 1
      AND cs.club_id = :club_id
      AND cs.week_num = :week_num
      AND cs.room_id = :room_id
      AND cs.week_year = :week_year
      AND cs.active IN {$where_active}
    ",
      ['week_year' => $week_year, 'week_num' => $week_num, 'club_id' => $club_id, 'room_id' => $room_id]
    );
    if(!$result OR !isset($result[0]->cost)) return 0;
    return ((int)$result[0]->cost!=$result[0]->cost)? number_format($result[0]->cost, 3, ',', '') : (int)$result[0]->cost;
  }




  public function get_cost_per_club($week_year, $week_num, $club_id, $active = ['Y'])
  {
    $where_active = "(";
    $k_ = 1;
    foreach($active as $i_){
      $where_active .= "'".$i_."'";
      if($k_<count($active)) $where_active .= ",";
      $k_++;
    }
    $where_active .= ")";
    $result = DB::select("
      SELECT sum(cs.cost) as cost FROM course_session as cs
      WHERE 1
      AND cs.club_id = :club_id
      AND cs.week_num = :week_num
      AND cs.week_year = :week_year
      AND cs.active IN {$where_active}
    ",
      ['week_year' => $week_year, 'week_num' => $week_num, 'club_id' => $club_id]
    );
    if(!$result OR !isset($result[0]->cost)) return 0;
    return ((int)$result[0]->cost!=$result[0]->cost)? number_format($result[0]->cost, 3, ',', '') : (int)$result[0]->cost;
  }


  public function add_course_session(Request $request)
  {
    
    $data = $request->get('course_session');
    $force_validation = $request->get('force_validation');
    
    if(!$data){
    return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $coach = Coach::find($data['primary_coach_id']);

    if($data['type_course'] == 'Cours'){
    $course = Course::find($data['course_id']);
    }else{
    $course = $data['description_course'] ;
    }

    $room = Room::find($data['room_id']);
    $club = Club::find($data['club_id']);
    if(!$coach || !$course || !$room || !$club){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $course_session_date = Carbon::createFromFormat('Y-m-d',$data['course_session_date']);
    
    if($data['type_course'] == 'Cours'){
    $course_start_time = $data['course_start_time'];
    $course_end_time = Carbon::createFromFormat('Y-m-d H:i',$data['course_session_date'].' '.$data['course_start_time'])->addMinutes($course->during)->format('H:i');
    }else{
    $course_start_time = $data['time_others_begin'];
    $course_end_time = $data['time_others_stop'];
    }
    
    Carbon::setWeekStartsAt(Carbon::MONDAY);
    $new_date = Carbon::createFromFormat('Y-m-d', $data['course_session_date']);
    $clone_new_date = clone $new_date;
    $base_week_number = $clone_new_date->startOfWeek()->add(new \DateInterval('P6D'))->format('W');
    $base_year_number = $clone_new_date->startOfWeek()->add(new \DateInterval('P6D'))->format('Y');

    if(Holiday::where('date',$course_session_date->format('Y-m-d'))->where('active','Y')->count()){
      return $this->sendResponse(null, ['Planning.TheSelectedDateIsAHoliday'], false);
    }

    

    $opening_hour_field = 'opening_hour_'.$course_session_date->format('N');
    $opening_hour = $club->{$opening_hour_field};
    $closing_hour_field = 'closing_hour_'.$course_session_date->format('N');
    $closing_hour = $club->{$closing_hour_field};

    

    if(
      !$closing_hour
      OR !$opening_hour
      OR $opening_hour>$course_start_time
      OR $opening_hour>$course_end_time
      OR $closing_hour<$course_start_time
      OR $closing_hour<$course_end_time
    ){
      return $this->sendResponse(null, ['Planning.TheRoomIsClosedDuringTheCourseSessionHours'], false);
      
    }
    
    
    
     
     if(
      CourseSession::select('*')
      ->whereIn('active', ['I','Y'])
      ->where('club_id', (int)$club->id)
      ->where('room_id', (int)$room->id)
      ->where('course_session_year', (int)$course_session_date->format('Y'))
      ->where('course_session_month', (int)$course_session_date->format('m'))
      ->where('course_session_day', (int)$course_session_date->format('d'))
      ->where( 'course_start_time', '<=',$course_start_time)
      ->where( 'course_end_time', '>',$course_start_time)
      ->count()
      OR
      CourseSession::select('*')
      ->whereIn('active', ['I','Y'])
      ->where('club_id', (int)$club->id)
      ->where('room_id', (int)$room->id)
      ->where('course_session_year', (int)$course_session_date->format('Y'))
      ->where('course_session_month', (int)$course_session_date->format('m'))
      ->where('course_session_day', (int)$course_session_date->format('d'))
      ->where( 'course_start_time', '<',$course_end_time)
      ->where( 'course_end_time', '>=',$course_end_time)
      ->count()
    ){
      return $this->sendResponse(null, ['Planning.TheRoomAlreadyReservedAtThisDateAndTime'], false);
    }
    
   
    if($data['type_course'] == 'Cours'){
    
    if(
      !$force_validation
      AND
      ($couch_week_during_hours = CourseSession::select(DB::raw("SUM(FORMAT((TIME_FORMAT(TIMEDIFF(course_end_time,course_start_time),'%h,%i')*60),0)) as during_hours"))
      ->whereIn('active', ['I','Y'])
      ->where('coach_id', (int)$coach->id)
      ->where('week_num', (int)$base_week_number)
      ->where('week_year', (int)$base_year_number)
      ->first()) AND $couch_week_during_hours->during_hours AND (int)($couch_week_during_hours->during_hours+$course->during) > (int)($coach->week_hours_max*60)
    ){
      return $this->sendResponse(null, ['Planning.TheCoachHasExceededHisMaximumWeeklySchedule'], false, null, ['need_force_validation' => true]);
    }

    }
    
    if($data['type_course'] == 'Cours'){

    if(
      !$force_validation
      AND
      $coach->day_hours_max
      AND ($couch_count_courses_day = CourseSession::select("*")
      ->whereIn('active', ['I','Y'])
      ->where('coach_id', (int)$coach->id)
      ->where('course_session_year', (int)$course_session_date->format('Y'))
      ->where('course_session_month', (int)$course_session_date->format('m'))
      ->where('course_session_day', (int)$course_session_date->format('d'))
      ->count())
      AND (int)($couch_count_courses_day) > (int)($coach->day_hours_max)
    ){
      return $this->sendResponse(null, ['Planning.TheCoachHasExceededHisMaximumNumberOfCoursesPerDay'], false, null, ['need_force_validation' => true]);
    }

    }
    
    if($data['type_course'] == 'Cours'){
    if(
      !$force_validation
      AND
      $course->max_courses
      AND ($couch_count_courses_current_day = CourseSession::select("*")
      ->whereIn('active', ['I','Y'])
      ->where('coach_id', (int)$coach->id)
      ->where('course_id', (int)$course->id)
      ->where('course_session_year', (int)$course_session_date->format('Y'))
      ->where('course_session_month', (int)$course_session_date->format('m'))
      ->where('course_session_day', (int)$course_session_date->format('d'))
      ->count())
      AND (int)($couch_count_courses_current_day) > (int)($course->max_courses)
    ){
      return $this->sendResponse($course, ['Planning.TheCoachHasExceededHisMaximumNumberOfCoursesPerDayForThisCourse'], false, null, ['need_force_validation' => true]);
    }
    }

    
    $startOfWeek_date = clone $course_session_date;
    $startOfWeek_date->startOfWeek();
    $course_session = new CourseSession;
    $course_session->active = 'I';

    if($data['type_course'] == 'Cours'){
    $course_session->course_id = $data['course_id'];
    }


    $course_session->room_id = $data['room_id'];
    $course_session->club_id = $data['club_id'];
    
    $course_session->coach_id = (isset($data['primary_coach_id']) && !empty($data['primary_coach_id'])) ? $data['primary_coach_id'] : null;
    $course_session->primary_coach_id = (isset($data['primary_coach_id']) && !empty($data['primary_coach_id'])) ? $data['primary_coach_id'] : null;
    $course_session->secondary_coach_id = (isset($data['secondary_coach_id']) && !empty($data['secondary_coach_id'])) ? $data['secondary_coach_id'] : null;
    $course_session->triple_coach_id = (isset($data['triple_coach_id']) && !empty($data['triple_coach_id'])) ? $data['triple_coach_id'] : null;
    $course_session->quad_coach_id = (isset($data['quad_coach_id']) && !empty($data['quad_coach_id'])) ? $data['quad_coach_id'] : null;
    $course_session->five_coach_id = (isset($data['five_coach_id']) && !empty($data['five_coach_id'])) ? $data['five_coach_id'] : null;
    
      

    $course_session->primary_coach_name = (isset($data['primary_coach_name']) && !empty($data['primary_coach_name'])) ? $data['primary_coach_name'] : null;
    $course_session->secondary_coach_name = (isset($data['secondary_coach_name']) && !empty($data['secondary_coach_name'])) ? $data['secondary_coach_name'] : null;
    $course_session->triple_coach_name = (isset($data['triple_coach_name']) && !empty($data['triple_coach_name'])) ? $data['triple_coach_name'] : null;
    $course_session->quad_coach_name = (isset($data['quad_coach_name']) && !empty($data['quad_coach_name'])) ? $data['quad_coach_name'] : null;
    $course_session->five_coach_name = (isset($data['five_coach_name']) && !empty($data['five_coach_name'])) ? $data['five_coach_name'] : null;
    
    
    
    
    
    
    if($data['type_course'] == 'Cours'){
    $course_session->cost = number_format(($coach->course_pricing/60)*$course->during);
    }else{
    $course_session->cost =  '' ;
    }


    $course_session->course_session_date = $course_session_date->format('Y-m-d');
   
    $course_session->course_start_time = $course_start_time;
    $course_session->course_end_time = $course_end_time;
    $course_session->day_num = $course_session_date->format('z')+1;
    $course_session->week_num = $base_week_number;
    $course_session->week_year = $base_year_number;
    $course_session->course_session_month = $course_session_date->format('m');
    $course_session->course_session_year = $course_session_date->format('Y');
    $course_session->course_session_day = $course_session_date->format('d');
    $course_session->type_course = $data['type_course'];
    
    if($data['type_course'] == 'Cours'){
    $course_session->description_course = '';
    }else{
    $course_session->description_course = $data['description_course'];
    }
    
    if($force_validation){
      $course_session->forced = "Y";
    }else{
      $course_session->forced = null;
    }
    
    $course_session->save();
    return $this->sendResponse($data, ['Form.data'], true);
  }


   public function update(Request $request)
   {
    $data = $request->get('course_session');
    $force_validation = $request->get('force_validation');
    
    if(!$data){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $coach = Coach::find($data['primary_coach_id']);
    if(isset($data['secondary_coach_id']) AND !empty($data['secondary_coach_id'])){
      $coach = Coach::find($data['secondary_coach_id']);
    }
    $course = Course::find($data['course_id']);
    $room = Room::find($data['room_id']);
    $club = Club::find($data['club_id']);
    $course_session = CourseSession::find($data['id']);

    if(!$course_session || !$coach || !$course || !$room || !$club){
      return $this->sendResponse(null, ['Form.InvalidData'], false);
    }

    $course_session_date = Carbon::createFromFormat('Y-m-d',$data['course_session_date']);
    $course_session_datetime = Carbon::createFromFormat('Y-m-d H:i',$data['course_session_date'].' '.$data['course_start_time']);


    if(Holiday::where('date',$course_session_date->format('Y-m-d'))->count()){
      return $this->sendResponse(null, ['Planning.TheSelectedDateIsAHoliday'], false);
    }
    
    $course_start_time = Carbon::createFromFormat('Y-m-d H:i',$data['course_session_date'].' '.$data['course_start_time'])->format('H:i');
    $course_start_time_add1 = Carbon::createFromFormat('Y-m-d H:i',$data['course_session_date'].' '.$data['course_start_time'])->addMinutes(1)->format('H:i');
    $course_end_time = Carbon::createFromFormat('Y-m-d H:i',$data['course_session_date'].' '.$data['course_start_time'])->addMinutes($course->during)->format('H:i');
    $course_end_time_sub1 = Carbon::createFromFormat('Y-m-d H:i',$data['course_session_date'].' '.$data['course_start_time'])->addMinutes($course->during)->subMinutes(1)->format('H:i');

    $new_date = Carbon::createFromFormat('Y-m-d', $data['course_session_date']);
    $clone_new_date = clone $new_date;
    $base_week_number = $clone_new_date->startOfWeek()->add(new \DateInterval('P6D'))->format('W');
    $base_year_number = $clone_new_date->startOfWeek()->add(new \DateInterval('P6D'))->format('Y');



    if(
      !$force_validation
      AND (
        CourseSession::select('*')
        ->whereIn('active', ['I','Y'])
        ->where('id', '<>', $course_session->id)
        ->where('club_id', (int)$club->id)
        ->where('room_id', (int)$room->id)
        ->where('course_session_year', (int)$course_session_date->format('Y'))
        ->where('course_session_month', (int)$course_session_date->format('m'))
        ->where('course_session_day', (int)$course_session_date->format('d'))
        ->where( 'course_start_time', '<=',$course_start_time)
        ->where( 'course_end_time', '>',$course_start_time)
        ->count()
        OR
        CourseSession::select('*')
        ->whereIn('active', ['I','Y'])
        ->where('id', '<>', $course_session->id)
        ->where('club_id', (int)$club->id)
        ->where('room_id', (int)$room->id)
        ->where('course_session_year', (int)$course_session_date->format('Y'))
        ->where('course_session_month', (int)$course_session_date->format('m'))
        ->where('course_session_day', (int)$course_session_date->format('d'))
        ->where( 'course_start_time', '<',$course_end_time)
        ->where( 'course_end_time', '>=',$course_end_time)
        ->count()
      )
    ){
      return $this->sendResponse(null, ['Planning.TheRoomAlreadyReservedAtThisDateAndTime'], false, null, ['need_force_validation' => true]);
    }
    
    if(
      !$force_validation
      AND ($couch_week_during_hours = CourseSession::select(DB::raw("SUM(FORMAT((TIME_FORMAT(TIMEDIFF(course_end_time,course_start_time),'%h,%i')*60),0)) as during_hours"))
      ->whereIn('active', ['I','Y'])
      ->where('coach_id', (int)$coach->id)
      ->where('week_num', (int)$base_week_number)
      ->where('week_year', (int)$base_year_number)
      ->where('id', '<>', $course_session->id)
      ->first()) AND $couch_week_during_hours->during_hours AND (int)($couch_week_during_hours->during_hours+$course->during) > (int)($coach->week_hours_max*60)
    ){
      return $this->sendResponse(null, ['Planning.TheCoachHasExceededHisMaximumWeeklySchedule'], false, null, ['need_force_validation' => true]);
    }

    if(
      !$force_validation
      AND $coach->day_hours_max
      AND ($couch_count_courses_day = CourseSession::select("*")
      ->whereIn('active', ['I','Y'])
      ->where('id', '<>', $course_session->id)
      ->where('coach_id', (int)$coach->id)
      ->where('course_session_year', (int)$course_session_date->format('Y'))
      ->where('course_session_month', (int)$course_session_date->format('m'))
      ->where('course_session_day', (int)$course_session_date->format('d'))
      ->count())
      AND (int)($couch_count_courses_day) > (int)($coach->day_hours_max)
    ){
      return $this->sendResponse(null, ['Planning.TheCoachHasExceededHisMaximumNumberOfCoursesPerDay'], false, null, ['need_force_validation' => true]);
    }




    if(
      !$force_validation
      AND $course->max_courses
      AND ($couch_count_courses_current_day = CourseSession::select("*")
      ->whereIn('active', ['I','Y'])
      ->where('id', '<>', $course_session->id)
      ->where('coach_id', (int)$coach->id)
      ->where('course_id', (int)$course->id)
      ->where('course_session_year', (int)$course_session_date->format('Y'))
      ->where('course_session_month', (int)$course_session_date->format('m'))
      ->where('course_session_day', (int)$course_session_date->format('d'))
      ->count())
      AND (int)($couch_count_courses_current_day) > (int)($course->max_courses)
    ){
      return $this->sendResponse($course, ['Planning.TheCoachHasExceededHisMaximumNumberOfCoursesPerDayForThisCourse'], false, null, ['need_force_validation' => true]);
    }



    

    $opening_hour_field = 'opening_hour_'.$course_session_date->format('N');
    $opening_hour = $club->{$opening_hour_field};
    $closing_hour_field = 'closing_hour_'.$course_session_date->format('N');
    $closing_hour = $club->{$closing_hour_field};

    if(
      !$closing_hour
      OR !$opening_hour
      OR $opening_hour>$course_start_time
      OR $opening_hour>$course_end_time
      OR $closing_hour<$course_start_time
      OR $closing_hour<$course_end_time
    ){
      return $this->sendResponse(null, ['Planning.TheRoomIsClosedDuringTheCourseSessionHours'], false);
    }

    

    $startOfWeek_date = clone $course_session_date;
    $startOfWeek_date->startOfWeek();
    if($course_session->course_id != $data['course_id']) $course_session->course_id = $data['course_id'];

    $course_session->coach_id = (isset($data['primary_coach_id']) && !empty($data['primary_coach_id'])) ? $data['primary_coach_id'] : null;
    $course_session->primary_coach_id = (isset($data['primary_coach_id']) && !empty($data['primary_coach_id'])) ? $data['primary_coach_id'] : null;
    $course_session->secondary_coach_id = (isset($data['secondary_coach_id']) && !empty($data['secondary_coach_id'])) ? $data['secondary_coach_id'] : null;
    $course_session->triple_coach_id = (isset($data['triple_coach_id']) && !empty($data['triple_coach_id'])) ? $data['triple_coach_id'] : null;
    $course_session->quad_coach_id = (isset($data['quad_coach_id']) && !empty($data['quad_coach_id'])) ? $data['quad_coach_id'] : null;
    $course_session->five_coach_id = (isset($data['five_coach_id']) && !empty($data['five_coach_id'])) ? $data['five_coach_id'] : null;
    
    $course_session->primary_coach_name = (isset($data['primary_coach_name']) && !empty($data['primary_coach_name'])) ? $data['primary_coach_name'] : null;
    $course_session->secondary_coach_name = (isset($data['secondary_coach_name']) && !empty($data['secondary_coach_name'])) ? $data['secondary_coach_name'] : null;
    $course_session->triple_coach_name = (isset($data['triple_coach_name']) && !empty($data['triple_coach_name'])) ? $data['triple_coach_name'] : null;
    $course_session->quad_coach_name = (isset($data['quad_coach_name']) && !empty($data['quad_coach_name'])) ? $data['quad_coach_name'] : null;
    $course_session->five_coach_name = (isset($data['five_coach_name']) && !empty($data['five_coach_name'])) ? $data['five_coach_name'] : null;
    
  
    

    if($course_session->cost != number_format(($coach->course_pricing/60)*$course->during)) $course_session->cost = number_format(($coach->course_pricing/60)*$course->during);
    if($course_session->course_session_date != $course_session_date->format('Y-m-d')) $course_session->course_session_date = $course_session_date->format('Y-m-d');
    if($course_session->course_start_time != $course_start_time) $course_session->course_start_time = $course_start_time;
    if($course_session->course_end_time != $course_end_time) $course_session->course_end_time = $course_end_time;
    if($course_session->day_num != ($course_session_date->format('z') + 1)) $course_session->day_num = $course_session_date->format('z') + 1;
    if($course_session->week_num != $base_week_number ) $course_session->week_num = $base_week_number;
    if($course_session->week_year != $base_year_number) $course_session->week_year = $base_year_number;
    if($course_session->course_session_month != $course_session_date->format('m')) $course_session->course_session_month = $course_session_date->format('m');
    if($course_session->course_session_year != $course_session->course_session_year = $course_session_date->format('Y')) $course_session->course_session_year = $course_session_date->format('Y');
    if($course_session->course_session_day != $course_session_date->format('d')) $course_session->course_session_day = $course_session_date->format('d');
    
    if($course_session->isDirty()){
      $course_session->active = 'I';
    }

    if($force_validation){
      $course_session->forced = "Y";
    }else{
      $course_session->forced = null;
    }

    $course_session->save();


    $data=DB::table('course_session_fin')
                    ->select('id')
                    ->where('coach_session_id',$course_session->id)
                    ->get();
    
    
    foreach($data as $d){
     $id = $d->id;
    };



    $count = count($data);
    if($count != 0){
      $new_course_session_fin = CourseSessionFin::find($id);
      $new_course_session_fin->state = 'replace';
      $new_course_session_fin->save();
    }
    
    
    
    return $this->sendResponse($course_session->getDirty(), ['Form.DataSavedWithSuccess'], true);
  }


  public function request_cancel_cours($params = null)
	{
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }

    
    $data=DB::table('course_session_fin')
                    ->select('id')
                    ->where('coach_session_id',$coach_session_id)
                    ->get();

    foreach($data as $d) {
     $id = $d->id;
    };

    $count = count($data);
    if($count != 0){
      $new_course_session_fin = CourseSessionFin::find($id);
    }else{
      $new_course_session_fin = new CourseSessionFin;
    }
    $new_course_session_fin->active = $active;
    $new_course_session_fin->coach_session_id = (int)$coach_session_id;
    $new_course_session_fin->state = $state;
    $new_course_session_fin->nbr_paticipants = (int)$nbr_paticipants;
    $new_course_session_fin->coach = $coach;
    $new_course_session_fin->save();
    
    $id_course_session = $new_course_session_fin->coach_session_id ;
    $courseSession = CourseSession::find($id_course_session);
    

    $return[] = $new_course_session_fin;
    $rea_user = ReaUser::find(17);
    \Mail::send('emails.notif_controleur', ['courseSession' => $courseSession], function ($m) use($rea_user , $new_course_session_fin) {
      $m->to($rea_user->email, $rea_user->firstname)->subject('Demande d\'annulation de cours');
    });
    return $this->sendResponse($new_course_session_fin->id, ['Form.DataSavedWithSuccess'], true);
  }

  public function validate_objectif_remplissage($params = null)
	{
    
    
    
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }
    $data=DB::table('course_session_fin')
                    ->select('id')
                    ->where('coach_session_id',$coach_session_id)
                    ->get();

    
    
    foreach($data as $d) {
     $id = $d->id;
    };
    
    
   


    $count = count($data);
    if($count != 0){
      $new_course_session_fin = CourseSessionFin::find($id);
    }

    $new_course_session_fin->active = $active;
    $new_course_session_fin->coach_session_id = (int)$coach_session_id;
    $new_course_session_fin->check_validate_remp = $check_validate ;
    $new_course_session_fin->save();
    
    $return[] = $new_course_session_fin;
    return $this->sendResponse($new_course_session_fin->id, ['Form.DataSavedWithSuccess'], true);
  
  }

  public function replace_coach($params = null)
	{
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }
    
    $data=DB::table('course_session_fin')
                    ->select('id')
                    ->where('coach_session_id',$coach_session_id)
                    ->get();

    foreach($data as $d){
     $id = $d->id;
    };

    $dataCoach=DB::table('coach')
                    ->select('*')
                    ->where('registration_number',$coach)
                    ->get();

    foreach($dataCoach as $dC){

     $emailCoach = $dC->email;
     $nameCoach = 	$dC->firstname;
     $numCoach = 	$dC->registration_number;
    
    };

    

    $count = count($data);
    if($count != 0){
      $new_course_session_fin = CourseSessionFin::find($id);
    }else{
      $new_course_session_fin = new CourseSessionFin;
    }
    $new_course_session_fin->active = $active;
    $new_course_session_fin->coach_session_id = (int)$coach_session_id;
    $new_course_session_fin->state = $state;
    $new_course_session_fin->nbr_paticipants = (int)$nbr_paticipants;
    $new_course_session_fin->coach = $coach;
    $new_course_session_fin->save();
    $course_session = CourseSession::find((int)$coach_session_id);
    
    $dataCourse=DB::table('course')
    ->select('course_name')
    ->where('id',$course_session->course_id)
    ->get();
    
    foreach($dataCourse as $dCr){
      $nameCourse = $dCr->course_name;
    };

    $dataClub=DB::table('club')
    ->select('club_name')
    ->where('id',$course_session->club_id)
    ->get();
    
    foreach($dataClub as $dCl){
      $nameClub = $dCl->club_name;
    };

    $dataRoom=DB::table('room')
    ->select('room_name')
    ->where('id',$course_session->room_id)
    ->get();
    
    foreach($dataRoom as $dCr){
      $nameRoom = $dCr->room_name;
    };
    
    \Mail::send('emails.notif_coach', ['course_session' => $course_session , 'nameCourse' => $nameCourse , 'nameClub' => $nameClub , 'nameRoom' => $nameRoom], function ($m) use($emailCoach , $nameCoach , $nameCourse , $nameClub , $nameRoom , $new_course_session_fin , $course_session) {
      $m->to($emailCoach, $nameCoach)->subject('Notification de remplacement du coach');
    });

    $emailTechnique = 'technique@california-gym.com';
    $nameTechnique = 'Technique';

    \Mail::send('emails.notif_technique', ['course_session' => $course_session , 'nameCoach' => $nameCoach , 'numCoach' => $numCoach , 'nameCourse' => $nameCourse , 'nameClub' => $nameClub , 'nameRoom' => $nameRoom], function ($m) use($emailTechnique , $nameTechnique ,  $nameCourse , $nameClub , $nameRoom , $new_course_session_fin , $course_session , $nameCoach , $numCoach ) {
      $m->to($emailTechnique , $nameTechnique)->subject('Notification de remplacement du coach');
    });
   
    $return[] = $new_course_session_fin;
    return $this->sendResponse($new_course_session_fin->id, ['Form.DataSavedWithSuccess'], true);
  }

  
  public function add_review($params = null)
	{
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }
    $course_session = CourseSession::find((int)$coach_session_id);
    $course_session->review = $review;
    $course_session->save();
    return $this->sendResponse($course_session->id, ['Form.DataSavedWithSuccess'], true);
  }

  public function replace_course($params = null)
	{
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }

    $course_session = CourseSession::find((int)$coach_session_id);
    $ancienIdCours = $course_session->course_id ;
    $ancienIdCoach = $course_session->coach_id ;
    $course_session->course_id = $course;
    $course_session->coach_id = $coach;

   /* edit multiple coachs */
   if($coach_primary_id != "null"){
      $course_session->primary_coach_id = $coach_primary_id ;
      $course_session->primary_coach_name = $coach_primary_name ;
    }else{
      $course_session->primary_coach_id = null ;
      $course_session->primary_coach_name = null ;
    }

    if($coach_secondary_id != "null"){
      $course_session->secondary_coach_id = $coach_secondary_id ;
      $course_session->secondary_coach_name = $coach_secondary_name ;
    }else{
      $course_session->secondary_coach_id = null ;
      $course_session->secondary_coach_name = null ;
    }


    if($coach_triple_id != "null"){
      $course_session->triple_coach_id = $coach_triple_id ;
      $course_session->triple_coach_name = $coach_triple_name ;
    }else{
      $course_session->triple_coach_id = null ;
      $course_session->triple_coach_name = null ;
    }


    if($coach_quad_id != "null"){
      $course_session->quad_coach_id = $coach_quad_id ;
      $course_session->quad_coach_name = $coach_quad_name ;
    }else{
      $course_session->quad_coach_id = null ;
      $course_session->quad_coach_name = null ;
    }

    if($coach_five_id != "null"){
      $course_session->five_coach_id = $coach_five_id ;
      $course_session->five_coach_name = $coach_five_name ;
    }else{
      $course_session->five_coach_id = null ;
      $course_session->five_coach_name = null ;
    }

    
    /* edit multiple coachs */

    $course_session->save();
    $data=DB::table('course_session_fin')
                    ->select('id')
                    ->where('coach_session_id',$coach_session_id)
                    ->get();
    
    
    foreach($data as $d){
     $id = $d->id;
    };



    
    $dataCoach=DB::table('coach')
                    ->select('*')
                    ->where('id',$coach)
                    ->get();
    
    
    foreach($dataCoach as $dC){
     $emailCoach = $dC->email;
     $nameCoach = 	$dC->firstname;
     $numCoach = 	$dC->registration_number;
    };

    $count = count($data);
    if($count != 0){
      $new_course_session_fin = CourseSessionFin::find($id);
    }else{
      $new_course_session_fin = new CourseSessionFin;
    }
    
    $new_course_session_fin->active = $active;
    $new_course_session_fin->coach_session_id = (int)$coach_session_id;
    $new_course_session_fin->state = $state;
    /*
    $new_course_session_fin->nbr_paticipants = (int)$nbr_paticipants;
    */


    $dataAncienCoach=DB::table('coach')
    ->select('*')
    ->where('id',$ancienIdCoach)
    ->get();
    foreach($dataAncienCoach as $dCr){
    $AncienCoach = $dCr->firstname.''.$dCr->lastname;
    $new_course_session_fin->ancien_coach = $AncienCoach;
    };

        

    $dataAncienCourse=DB::table('course')
      ->select('course_name')
      ->where('id',$ancienIdCours)
      ->get();
      foreach($dataAncienCourse as $dCr){
      $AncienCourse = $dCr->course_name;
      $new_course_session_fin->ancien_cours = $AncienCourse;
    };

    

    if($coach != ''){

      $new_course_session_fin->coach = $nameCoach;
      $course_session = CourseSession::find((int)$coach_session_id);
      $dataCourse=DB::table('course')
      ->select('course_name')
      ->where('id',$course_session->course_id)
      ->get();
      
      foreach($dataCourse as $dCr){
        $nameCourse = $dCr->course_name;
      };

      $dataClub=DB::table('club')
      ->select('club_name')
      ->where('id',$course_session->club_id)
      ->get();
      
      foreach($dataClub as $dCl){
        $nameClub = $dCl->club_name;
      };

      $dataRoom=DB::table('room')
      ->select('room_name')
      ->where('id',$course_session->room_id)
      ->get();
      
      foreach($dataRoom as $dCr){
        $nameRoom = $dCr->room_name;
      };

      \Mail::send('emails.notif_coach', ['course_session' => $course_session , 'nameCourse' => $nameCourse , 'nameClub' => $nameClub , 'nameRoom' => $nameRoom], function ($m) use($emailCoach , $nameCoach , $nameCourse , $nameClub , $nameRoom , $new_course_session_fin , $course_session) {
        $m->to($emailCoach, $nameCoach)->subject('Notification de remplacement du coach');
      });

      $emailTechnique = 'technique@california-gym.com';
      $nameTechnique = 'Technique';

      \Mail::send('emails.notif_technique', ['course_session' => $course_session , 'nameCoach' => $nameCoach , 'numCoach' => $numCoach , 'nameCourse' => $nameCourse , 'nameClub' => $nameClub , 'nameRoom' => $nameRoom], function ($m) use($emailTechnique , $nameTechnique ,  $nameCourse , $nameClub , $nameRoom , $new_course_session_fin , $course_session , $nameCoach , $numCoach ) {
        $m->to($emailTechnique , $nameTechnique)->subject('Notification de remplacement du coach');
      });

      $emailDigital = 'digital@california-gym.com';
      $nameDigital = 'Digital';

      
      \Mail::send('emails.notif_technique', ['course_session' => $course_session , 'nameCoach' => $nameCoach , 'numCoach' => $numCoach , 'nameCourse' => $nameCourse , 'nameClub' => $nameClub , 'nameRoom' => $nameRoom], function ($m) use($emailDigital , $nameDigital ,  $nameCourse , $nameClub , $nameRoom , $new_course_session_fin , $course_session , $nameCoach , $numCoach ) {
        $m->to($emailDigital , $nameDigital)->subject('Notification de remplacement du coach');
      });

    
    
    }


    if($course != ''){
    $new_course_session_fin->course = $course;
    }
    $new_course_session_fin->save();


    $return[] = $new_course_session_fin;
    return $this->sendResponse($new_course_session_fin->id, ['Form.DataSavedWithSuccess'], true);
  
  }

  public function nbr_paritcipants_cours($params = null)
	{
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }

    $data=DB::table('course_session_fin')
                    ->select('id')
                    ->where('coach_session_id',$coach_session_id)
                    ->get();

    foreach($data as $d) {
     $id = $d->id;
    };

    $count = count($data);
    if($count != 0){
      $new_course_session_fin = CourseSessionFin::find($id);
    }else{
      $new_course_session_fin = new CourseSessionFin;
    }
    $new_course_session_fin->active = $active;
    $new_course_session_fin->coach_session_id = (int)$coach_session_id;
    $new_course_session_fin->state = $state;
    $new_course_session_fin->nbr_paticipants = (int)$nbr_paticipants;
    $new_course_session_fin->coach = $coach;
    $new_course_session_fin->save();
    $return[] = $new_course_session_fin;
    return $this->sendResponse($new_course_session_fin->id, ['Form.DataSavedWithSuccess'], true);
  }

  public function validate_planning(Request $request)
  {
    $club_id = $request->get('club_id');
    $room_id = $request->get('room_id');
    $week_num = $request->get('week_num');
    $week_year = $request->get('week_year');

    if(!$club_id || !$room_id || !$week_num || !$week_year){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $club = Club::find($club_id);
    $room = Room::find($room_id);

    if(!$room || !$club){
      return $this->sendResponse(null, ['Form.InvalidData'], false);
    }

    CourseSession::where('active', 'I')
    ->where('club_id', (int)$club_id)
    ->where('room_id', (int)$room_id)
    ->where('week_num', (int)$week_num)
    ->where('week_year', (int)$week_year)
    ->update(['active' => 'Y']);
    return $this->sendResponse(null, ['Planning.PlanningValidatedWithSuccess'], true);
  }






  public function analyse(Request $request)
  {
    $club_id = $request->get('club_id');
    $room_id = $request->get('room_id');
    $week_num = $request->get('week_num');
    $week_year = $request->get('week_year');

    if(!$club_id || !$room_id || !$week_num || !$week_year){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $club = Club::find($club_id);
    $room = Room::find($room_id);
    if(!$room || !$club){
      return $this->sendResponse(null, ['Form.InvalidData'], false);
    }

    $coaches = DB::select( DB::raw("SELECT t1.id, t1.firstname, t1.lastname FROM (
      SELECT c.*, count(cs.id) as count_sessions, SUM((TIME_FORMAT(TIMEDIFF(cs.course_end_time,cs.course_start_time),'%h,%i')*60)) as during, TRUNCATE(SUM((TIME_FORMAT(TIMEDIFF(cs.course_end_time,cs.course_start_time),'%h,%i')*60)/60),1) as during_hours
      FROM coach as c
      LEFT JOIN course_session cs ON cs.coach_id = c.id and cs.room_id = {$room_id} and cs.club_id = {$club_id} and cs.week_year = {$week_year} and cs.week_num = {$week_num} AND cs.active IN ('Y','I')
      WHERE clubs_mfk LIKE '%,{$club_id},%'
      AND c.active = 'Y'
      AND c.id IN (
        SELECT cch.id
        FROM course as crs
        INNER JOIN coach as cch on cch.courses_mfk LIKE concat(',%',crs.id,',%') AND cch.active='Y'
        WHERE crs.active='Y'
        AND crs.rooms_mfk LIKE '%,{$room_id},%'
        GROUP BY cch.id
      )
      GROUP BY c.id) as t1
      WHERE t1.during_hours < t1.week_hours_min OR t1.during_hours IS NULL
      ")
    );
    $attributes = [];
    if($coaches AND count($coaches)){
        $attributes['coaches'] = $coaches;
    }

    return $this->sendResponse(null, ['Global.GetDataWithSuccess'], true, null, $attributes);
  }

  public function duplicate_planning(Request $request)
  {
    $club_id = $request->get('club_id');
    $room_id = $request->get('room_id');
    $week_num = $request->get('week_num');
    $date = $request->get('date');
    $week_year = $request->get('week_year');

    if(!$date || !$club_id || !$room_id || !$week_num || !$week_year){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $club = Club::find($club_id);
    $room = Room::find($room_id);

    if(!$room || !$club){
      return $this->sendResponse(null, ['Form.InvalidData'], false);
    }

    
    // $new_year_number = ($new_date->format('W') > 10 && $new_date->format('m') == 1) ? (int)($new_date->format('Y') - 1) : (int)$new_date->format('Y');

    $new_date = Carbon::createFromFormat('Y-m-d',$date);
    $clone_new_date = clone $new_date;
    Carbon::setWeekStartsAt(Carbon::MONDAY);
    $base_week_number = $clone_new_date->startOfWeek()->add(new \DateInterval('P6D'))->format('W');
    $base_year_number = $clone_new_date->startOfWeek()->add(new \DateInterval('P6D'))->format('Y');


    if(CourseSession::select('*')
    //->whereIn('active', ['I','Y'])
    ->whereIn('active', ['Y'])
    ->where('club_id', (int)$club_id)
    ->where('room_id', (int)$room_id)
    ->where('week_num', $base_week_number)
    ->where('week_year', (int)$base_year_number)
    ->count()){
      return $this->sendResponse(null, ['Planning.SelectedWeekAlreadyHaveCourseSessions'], false);
    }

    $course_sessions = CourseSession::select('*')
    ->whereIn('active', ['I','Y'])
    ->where('club_id', (int)$club_id)
    ->where('room_id', (int)$room_id)
    ->where('week_num', (int)$week_num)
    ->where('week_year', (int)$week_year)
    ->orderBy('course_session_date', 'ASC')
    ->get();

    $return = [];
    foreach ($course_sessions as $course_session) {
      $old_course_session_date = Carbon::createFromFormat('Y-m-d',$course_session->course_session_date);
      $clone_new_date = clone $new_date;
      $new_course_session_date = $clone_new_date->addDays($old_course_session_date->format('N')-1);
      // $startOfWeek_date = clone $new_course_session_date;
      // $startOfWeek_date->startOfWeek();

      $new_course_session = new CourseSession;
      $new_course_session->club_id = $course_session->club_id;
      $new_course_session->room_id = $course_session->room_id;
      $new_course_session->course_id = $course_session->course_id;
      $new_course_session->coach_id = $course_session->coach_id;
      /* */
      $new_course_session->primary_coach_id = $course_session->primary_coach_id;
      $new_course_session->secondary_coach_id = $course_session->secondary_coach_id;
      $new_course_session->triple_coach_id = $course_session->triple_coach_id;
      $new_course_session->quad_coach_id = $course_session->quad_coach_id;
      $new_course_session->five_coach_id = $course_session->five_coach_id;
      $new_course_session->primary_coach_name = $course_session->primary_coach_name;
      $new_course_session->secondary_coach_name = $course_session->secondary_coach_name;
      $new_course_session->triple_coach_name = $course_session->triple_coach_name;
      $new_course_session->quad_coach_name = $course_session->quad_coach_name;
      $new_course_session->five_coach_name = $course_session->five_coach_name;
      $new_course_session->type_course = $course_session->type_course;
      $new_course_session->description_course = $course_session->description_course;
      /* */
      $new_course_session->active = $course_session->active;
      $new_course_session->course_start_time = $course_session->course_start_time;
      $new_course_session->course_end_time = $course_session->course_end_time;
      $new_course_session->cost = $course_session->cost;
      $new_course_session->week_num = $base_week_number;
      $new_course_session->week_year = $base_year_number;
      $new_course_session->course_session_date = $new_course_session_date->format('Y-m-d');
      $new_course_session->course_session_day = $new_course_session_date->format('d');
      $new_course_session->course_session_month = $new_course_session_date->format('m');
      $new_course_session->course_session_year = $new_course_session_date->format('Y');
      $new_course_session->day_num = $new_course_session_date->format('z')+1;
      $new_course_session->save();
      $return[] = $new_course_session;
      
    }

    return $this->sendResponse($return, ['Planning.PlanningDuplicatedWithSuccess'], true);
  }





  public function delete(Request $request)
  {
    $course_session_id = $request->get('course_session_id');

    if(!$course_session_id){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $course_session = CourseSession::find($course_session_id);
    if(!$course_session){
      return $this->sendResponse(null, ['CourseSession.InvalidCourseSession'], false);
    }
    $course_session->active = 'D';
    $course_session->save();

    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }





  public function stats($params)
  {
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }

    if(!isset($stats_type) OR !isset($startdate) OR !isset($enddate)){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $data = [];

    $startdate = Carbon::createFromTimestamp($startdate);
    $enddate = Carbon::createFromTimestamp($enddate);
    $params_query = [];
    $params_query[':active'] = 'Y';
    $extra_join_query = null;

    $extra_join_query .="\n
      AND cs.course_session_date>=:startdate
      AND cs.course_session_date<=:enddate
    ";


    $params_query[':startdate'] = $startdate->format('Y-m-d');
    $params_query[':enddate'] = $enddate->format('Y-m-d');

    /*
    if($stats_by=='by_year'){
      $extra_join_query .="\n
        AND cs.course_session_year=:course_session_year
      ";
      $params_query[':course_session_year'] = $date->format('Y');
    }
    if($stats_by=='by_month'){
      $extra_join_query .="\n
        AND cs.course_session_year=:course_session_year
        AND cs.course_session_month=:course_session_month
      ";
      $params_query[':course_session_year'] = $date->format('Y');
      $params_query[':course_session_month'] = $date->format('m');
    }
    if($stats_by=='by_week'){
      $extra_join_query .="\n
        AND cs.week_year=:week_year
        AND cs.week_num=:week_num
      ";
      $params_query[':week_year'] = $date->format('Y');
      $params_query[':week_num'] = $date->format('W');
    }*/


    if(isset($club_id)){
      $extra_join_query .="\n
        AND cs.club_id=:club_id
      ";
      $params_query[':club_id'] = (int)$club_id;
    }

    switch ($stats_type) {


      case 'price_paid_by_course':
        $data = DB::select(DB::raw("
          SELECT c.id, c.course_name, c.color, sum(cs.cost) as price_paid
          FROM course as c
          LEFT JOIN course_session as cs on cs.course_id = c.id AND cs.active IN ('Y') {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY sum(cs.cost) DESC, c.course_name ASC
          "),$params_query
        );
      break;

      
 
      
      case 'price_per_course_by_coach':
      
      if(isset($coach_id)){
      
        $params_query_price_per_course_by_coach[':coach_id'] = (int)$coach_id;
        $params_query_price_per_course_by_coach[':startdate'] = $startdate->format('Y-m-d');
        $params_query_price_per_course_by_coach[':enddate'] = $enddate->format('Y-m-d');
        

        $data = DB::select(DB::raw("
        SELECT ch.firstname , ch.lastname , ch.course_pricing , c.course_name , cs.course_id , COUNT(cs.course_id) as nbr , cs.club_id , cs.coach_id , cs.active ,  cl.club_name as club_name
        FROM course_session as cs , course as c , coach as ch , club as cl
        WHERE cs.active = 'Y' 
        AND cs.coach_id=:coach_id
        AND cs.course_id=c.id 
        AND cs.coach_id=ch.id 
        AND cs.club_id=cl.id
        AND cs.course_session_date>=:startdate
        AND cs.course_session_date<=:enddate
        GROUP BY cl.id
        ORDER BY cs.id ASC
        "),$params_query_price_per_course_by_coach
        );


      }
      break;


      case 'nmber_cour_per_coach_by_club':
      if(isset($coach_id)){
      
        $params_query_price_per_course_by_coach[':coach_id'] = (int)$coach_id;
        $params_query_price_per_course_by_coach[':startdate'] = $startdate->format('Y-m-d');
        $params_query_price_per_course_by_coach[':enddate'] = $enddate->format('Y-m-d');
        
        $data = DB::select(DB::raw("
        SELECT ch.firstname , ch.lastname , ch.course_pricing , c.course_name , cs.course_id , COUNT(cs.course_id) as nbr , cs.club_id , cs.coach_id , cs.active ,  cl.club_name as club_name
        FROM course_session as cs , course as c , coach as ch , club as cl
        WHERE cs.active = 'Y' 
        AND cs.coach_id=:coach_id
        AND cs.course_id=c.id 
        AND cs.coach_id=ch.id 
        AND cs.club_id=cl.id
        AND cs.course_session_date>=:startdate
        AND cs.course_session_date<=:enddate
        GROUP BY cl.id
        ORDER BY cs.id ASC
        "),$params_query_price_per_course_by_coach
        );


      }
      break;

      case 'cost_per_club':
        $data = DB::select(DB::raw("
          SELECT c.id, c.club_name, sum(cs.cost) as price_paid
          FROM club as c
          LEFT JOIN course_session as cs on cs.club_id = c.id AND cs.active IN ('Y') {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY sum(cs.cost) DESC, c.club_name ASC
          "),$params_query
        );
      break;



      case 'total_cost_per_coach':
        $data = DB::select(DB::raw("
          SELECT c.id, c.firstname, c.lastname, c.registration_number, sum(cs.cost) as price_paid
          FROM coach as c
          LEFT JOIN course_session as cs on cs.coach_id = c.id AND cs.active IN ('Y') {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY sum(cs.cost) DESC, c.firstname ASC, c.lastname ASC
          "),$params_query
        );
        break;




      case 'number_of_courses_per_concept':
        $data = DB::select(DB::raw("
          SELECT c.id, c.course_name, count(cs.id) as courses
          FROM course as c
          LEFT JOIN course_session as cs on cs.course_id = c.id AND cs.active IN ('Y') {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY count(cs.id) DESC, c.course_name ASC
          "),$params_query
        );
        break;



      case 'number_of_courses_per_coach':
        $data = DB::select(DB::raw("
          SELECT c.id, c.firstname, c.lastname, c.registration_number, count(cs.id) as courses
          FROM coach as c
          LEFT JOIN course_session as cs on cs.coach_id = c.id AND cs.active IN ('Y') {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY count(cs.id) DESC, c.firstname ASC, c.lastname ASC
          "),$params_query
        );




        break;



      case 'number_of_courses_by_coach':
        $data = DB::select(DB::raw("
          SELECT c.id, c.firstname, c.lastname, c.registration_number, count(cs.id) as courses
          FROM coach as c
          LEFT JOIN course_session as cs on cs.coach_id = c.id AND cs.active IN ('Y') {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY count(cs.id) DESC, c.firstname ASC, c.lastname ASC
          "),$params_query
        );
        break;



      case 'number_of_classes_per_club':
        $data = DB::select(DB::raw("
          SELECT c.id, c.club_name, count(cs.id) as courses
          FROM club as c
          LEFT JOIN course_session as cs on cs.club_id = c.id AND cs.active IN ('Y') {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY count(cs.id) DESC, c.club_name ASC
          "),$params_query
        );
        break;






      case 'number_of_hours_worked_per_course':
        $data = DB::select(DB::raw("
          SELECT stats_table.* FROM (
            SELECT c.id, c.course_name, SUM((TIME_FORMAT(TIMEDIFF(cs.course_end_time,cs.course_start_time),'%h,%i')*60)) as during, TRUNCATE(SUM((TIME_FORMAT(TIMEDIFF(cs.course_end_time,cs.course_start_time),'%h,%i')*60)/60),1) as during_hours
            FROM course as c
            LEFT JOIN course_session as cs on cs.course_id = c.id AND cs.active IN ('Y') {$extra_join_query}
            WHERE 1
            AND c.active= :active
            GROUP BY c.id
            ORDER BY count(cs.id) DESC, c.course_name ASC
          ) as stats_table
          ORDER BY stats_table.during_hours DESC
          "),$params_query
        );
        break;




        case 'replacement_number_per_coach':
        $data = DB::select(DB::raw("
          SELECT c.id, c.firstname, c.lastname, c.registration_number, count(cs.id) as courses
          FROM coach as c
          LEFT JOIN course_session as cs on cs.coach_id = c.id AND cs.active IN ('Y') AND cs.coach_id=cs.secondary_coach_id {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY count(cs.id) DESC, c.firstname ASC, c.lastname ASC
          "),$params_query
        );
        break;

        case 'number_of_replaced_by_coach':
        $data = DB::select(DB::raw("
          SELECT c.id, c.firstname, c.lastname, c.registration_number, count(cs.id) as courses
          FROM coach as c
          LEFT JOIN course_session as cs on cs.primary_coach_id = c.id AND cs.active IN ('Y') AND cs.coach_id<>cs.primary_coach_id {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY count(cs.id) DESC, c.firstname ASC, c.lastname ASC
          "),$params_query
        );
        break;




      case 'number_of_substitutes_per_club':
        $data = DB::select(DB::raw("
          SELECT c.id, c.club_name, count(cs.id) as courses
          FROM club as c
          LEFT JOIN course_session as cs on cs.club_id = c.id AND cs.active IN ('Y') AND cs.coach_id<>cs.primary_coach_id {$extra_join_query}
          WHERE 1
          AND c.active= :active
          GROUP BY c.id
          ORDER BY count(cs.id) DESC, c.club_name ASC
          "),$params_query
        );
        break;
    }
    
    
    if( isset($export) ){
      $data=array_map(function($item){

          $item = (array) $item;
          if(isset($item['id'])) unset($item['id']);
          if(isset($item['color'])) unset($item['color']);
          return $item;

      },$data);
        //$model = 'stats-'.$stats_type.'-'.$stats_by;
        $model = $stats_type;
        //$Filename = $model.'-'.date('Y-m-d--H\hm');
        $Filename = $model;
        Excel::create($Filename, function($excel) use($data, $model) {
          $excel->sheet($model, function($sheet) use($data) {
              $sheet->fromArray($data);
          });
      })->export('xls');
      exit();
    }
    
    return $this->sendResponse($data, ['Global.GetDataWithSuccess'], true);
  }

  public function exportgrh($params)
  {
    
    if($params){
      $parametres = decode_url_params($params);
      extract($parametres);
    }

    if(!isset($startdate) OR !isset($enddate)){
      return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $data = [];
    $params_query = [];
    $params_query[':startdate'] = $startdate;
    $params_query[':enddate'] = $enddate;

    $data = DB::select(DB::raw("
        SELECT  cs.id as ID  , cl.club_name as Salle , cl.code_club as Code_analytique_salle , cs.course_session_day as Jour , cs.course_session_date as Date , cs.course_session_month as Mois , cs.course_session_month as MoisPaie , ch.registration_number as Matricule , ch.firstname as Coach ,  c.course_name as Cours , c.course_code as Code_du_cours , c.during as Nbr_heure  , csf.check_validate_remp as remplissage , cs.review as Commentaire , csf.nbr_paticipants as Participants    
        FROM course_session as cs , course as c , coach as ch , club as cl , course_session_fin as csf
        WHERE cs.active = 'Y' 
        AND cs.course_id=c.id
        AND ( cs.coach_id=ch.id OR cs.secondary_coach_id=ch.id OR cs.triple_coach_id=ch.id OR cs.quad_coach_id=ch.id OR cs.five_coach_id=ch.id )
        AND cs.club_id=cl.id
        AND cs.id=csf.coach_session_id
        AND cs.course_session_date>=:startdate
        AND cs.course_session_date<=:enddate
        ORDER BY cs.course_session_date ASC
        "),$params_query
    );

    foreach($data as $item){
      $this->add_remplissage_item_after_get($item);
    }
      
    if(isset($export)){
       $data=array_map(function($item){
            $item = (array) $item;
            if(isset($item['id'])) unset($item['id']);
            if(isset($item['color'])) unset($item['color']);
            return $item;
        },$data);

        $model = 'export_grh';
        $Filename = $model.'-'.$startdate.'==='.$enddate;
        Excel::create($Filename, function($excel) use($data, $model) {
            $excel->sheet($model, function($sheet) use($data) {
                $sheet->fromArray($data);
            });
        })->export('xls');
        exit();
    }
    return $this->sendResponse($data, ['Global.GetDataWithSuccess'], true);
  }
  
  public function send_week_email($course_session_year, $week_num, $club_id = null, $room_id = null, $coach_id = null, $step = 0){

    $courseSessionFactory = new \App\Libraries\Factories\CourseSessionFactory;
    $data = $courseSessionFactory->send_week_email($course_session_year, $week_num, $club_id, $room_id, $coach_id, $step);
    $attributes = [];
    if(isset($data['steps']) AND (($step+1)>=$data['steps']) ){
      $attributes['finish'] = true;
    }
    return $this->sendResponse($data, ['Global.SendPlanningWithSuccess'], true, null, $attributes);
  
  }

  public function getCalendarFilters($club_id, $room_id = null, $coach_id = null, $course_id = null, $start_date = null, $end_date = null) {

    if ($start_date) {
      $start_date = (new Carbon(date('Y-m-d', strtotime($start_date))))->format('Y-m-d');
    }
    if ($end_date) {
      $end_date = (new Carbon(date('Y-m-d', strtotime($end_date))))->format('Y-m-d');
    }

    return $this->sendResponse([
      'rooms' => $this->getFiltersRooms($club_id, $room_id, $coach_id, $course_id, $start_date, $end_date),
      'courses' => $this->getFiltersCourses($club_id, $room_id, $coach_id, $course_id, $start_date, $end_date),
      'coachs' => $this->getFiltersCoachs($club_id, $room_id, $coach_id, $course_id, $start_date, $end_date),
    ], ['Global.GetDataWithSuccess']);
  }

  public function getFiltersCourses($club_id, $room_id = null, $coach_id = null, $course_id = null, $start_date = null, $end_date = null) {

    if ($course_id == 'autre') {
      return [
        [
          'id' => 'autre',
          'course_name' => 'Autre',
        ],
      ];
    }

    $params_query = [
      'active' => 'Y',
    ];
    $where_extend = '';
    $sql = "SELECT c.id, c.course_name, c.course_code, c.color, c.logo_file_id, c.during
    FROM  course as c
    INNER JOIN course_session as cs on (cs.course_id = c.id) AND cs.active = :active 
    WHERE c.active = :active AND cs.club_id = :club_id
    EXTEND_WHERE
    GROUP BY c.id
    ORDER BY c.course_name";

    if ((int)$club_id) {
      $where_extend .= ' AND cs.club_id = :club_id ';
      $params_query['club_id'] = (int)$club_id;
    }

    if ((int)$room_id) {
      $where_extend .= ' AND cs.room_id = :room_id ';
      $params_query['room_id'] = (int)$room_id;
    }

    if ((int)$coach_id) {
      $where_extend .= ' AND (
      cs.coach_id = :coach_id
      OR cs.primary_coach_id = :coach_id
      OR cs.secondary_coach_id = :coach_id
      OR cs.triple_coach_id = :coach_id
      OR cs.quad_coach_id = :coach_id
      OR cs.five_coach_id = :coach_id
    ) ';
      $params_query['coach_id'] = (int)$coach_id;
    }

    if ((int)$course_id) {
      $where_extend .= ' AND c.id = :course_id ';
      $params_query['course_id'] = (int)$course_id;
    }

    if ($start_date) {
      $where_extend .= ' AND cs.course_session_date >= :start_date ';
      $params_query['start_date'] = $start_date;
    }

    if ($end_date) {
      $where_extend .= ' AND cs.course_session_date <= :end_date ';
      $params_query['end_date'] = $end_date;
    }

    $sql = str_replace('EXTEND_WHERE', $where_extend, $sql);
    foreach($params_query as $param_name => $val) {
      $sql = str_replace(':'.$param_name, "'" . $val. "'", $sql);
    }
    // return DB::select(DB::raw($sql), $params_query);
    return DB::select($sql, []);
  }

  public function getFiltersRooms($club_id, $room_id = null, $coach_id = null, $course_id = null, $start_date = null, $end_date = null) {
    $params_query = [
      'active' => 'Y',
    ];
    $where_extend = '';
    $sql = "SELECT r.id, r.room_name
    FROM  room as r
    INNER JOIN course_session as cs on cs.room_id = r.id AND cs.active = :active 
    WHERE r.active = :active AND cs.club_id = :club_id
    EXTEND_WHERE
    GROUP BY r.id
    ORDER BY r.room_name";

    if ((int)$club_id) {
      $where_extend .= ' AND cs.club_id = :club_id ';
      $params_query['club_id'] = (int)$club_id;
    }

    /*if ((int)$room_id) {
      $where_extend .= ' AND cs.room_id = :room_id ';
      $params_query['room_id'] = (int)$room_id;
    }*/

    if ((int)$coach_id) {
      $where_extend .= ' AND (
      cs.coach_id = :coach_id
      OR cs.primary_coach_id = :coach_id
      OR cs.secondary_coach_id = :coach_id
      OR cs.triple_coach_id = :coach_id
      OR cs.quad_coach_id = :coach_id
      OR cs.five_coach_id = :coach_id
    ) ';
      $params_query['coach_id'] = (int)$coach_id;
    }

    if ((int)$course_id) {
      $where_extend .= ' AND cs.course_id = :course_id ';
      $params_query['course_id'] = (int)$course_id;
    } elseif ($course_id == 'autre') {
      $where_extend .= ' AND cs.type_course = :type_course ';
      $params_query['type_course'] = 'autre';
    }

    if ($start_date) {
      $where_extend .= ' AND cs.course_session_date >= :start_date ';
      $params_query['start_date'] = $start_date;
    }

    if ($end_date) {
      $where_extend .= ' AND cs.course_session_date <= :end_date ';
      $params_query['end_date'] = $end_date;
    }

    $sql = str_replace('EXTEND_WHERE', $where_extend, $sql);
    foreach($params_query as $param_name => $val) {
      $sql = str_replace(':'.$param_name, "'" . $val. "'", $sql);
    }
    // return DB::select(DB::raw($sql), $params_query);
    return DB::select($sql, []);
  }

  public function getFiltersCoachs($club_id, $room_id = null, $coach_id = null, $course_id = null, $start_date = null, $end_date = null) {
    $params_query = [
      'active' => 'Y',
    ];
    $where_extend = '';
    $sql = "SELECT ch.id, ch.firstname, ch.lastname, ch.photo_file_id, ch.descenrichi
    FROM  coach as ch
    INNER JOIN course_session as cs on (cs.coach_id = ch.id
    OR cs.primary_coach_id = ch.id
    OR cs.secondary_coach_id = ch.id
    OR cs.triple_coach_id = ch.id
    OR cs.quad_coach_id = ch.id
    OR cs.five_coach_id = ch.id
    ) AND cs.active = :active 
    WHERE ch.active = :active AND cs.club_id = :club_id
    EXTEND_WHERE
    
    GROUP BY ch.id
    ORDER BY ch.firstname";

    if ((int)$club_id) {
      $where_extend .= ' AND cs.club_id = :club_id ';
      $params_query['club_id'] = (int)$club_id;
    }

    if ((int)$room_id) {
      $where_extend .= ' AND cs.room_id = :room_id ';
      $params_query['room_id'] = (int)$room_id;
    }

    if ((int)$coach_id) {
      $where_extend .= ' AND ch.id = :coach_id ';
      $params_query['coach_id'] = (int)$coach_id;
    }

    if ((int)$course_id) {
      $where_extend .= ' AND cs.course_id = :course_id ';
      $params_query['course_id'] = (int)$course_id;
    } elseif ($course_id == 'autre') {
      $where_extend .= ' AND cs.type_course = :type_course ';
      $params_query['type_course'] = 'autre';
    }

    if ($start_date) {
      $where_extend .= ' AND cs.course_session_date >= :start_date ';
      $params_query['start_date'] = $start_date;
    }

    if ($end_date) {
      $where_extend .= ' AND cs.course_session_date <= :end_date ';
      $params_query['end_date'] = $end_date;
    }

    $sql = str_replace('EXTEND_WHERE', $where_extend, $sql);
    foreach($params_query as $param_name => $val) {
      $sql = str_replace(':'.$param_name, "'" . $val. "'", $sql);
    }
    // return DB::select(DB::raw($sql), $params_query);
    return DB::select($sql, []);
  } 


}
