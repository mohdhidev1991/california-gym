<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseSessionFinRepository;
use App\Models\CourseSessionFin;
use App\Models\CourseSession;
use App\Models\Club;
use App\Models\ReaUser;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use DB;
use Carbon\Carbon;
use Excel;
use Mail;


class CourseSessionFinAPIController extends AppBaseController
{
  

  function __construct(CourseSessionFinRepository $course_session_finRepo)
  {
    parent::__construct();
    $this->courseSessionFinRepository = $course_session_finRepo;
  }

  public function get($params = null)
  {
    if($params){
      $parametres = decode_url_params($params);
    }

    $params_query = [];
    $extra_join_query = null;
    $extra_join_query2 = null;
    $state = $parametres['state'];

    $startdate = $parametres['startdate'];
    $params_query[':startdate'] = $startdate;

    $enddate = $parametres['enddate'];
    $params_query[':enddate'] = $enddate;
    
    if($state == 'annuler'){
      $extra_join_query .="\n
       AND csf.state	LIKE '%ann%'
      ";
    }else if($state == 'participe'){
      $extra_join_query .="\n
      AND csf.state	LIKE '%part%'
      ";
    }
    else if($state == 'replace'){
      $extra_join_query .="\n
      AND csf.state	LIKE '%replace%'
      ";
    }
    

    $club = $parametres['club'];
    $params_query[':club'] = $club;
    if($club == null){
      $extra_join_query2 .="\n
      AND cs.club_id = cl.id
      ";
    }else{
      $extra_join_query2 .="\n
      AND cs.club_id = :club
      ";
    }

    $data = DB::select(DB::raw("
    SELECT csf.coach_session_id as Id , cs.course_session_date as Date , 
    cs.course_start_time as Heure_debut , cs.course_end_time as Heure_fin , csf.ancien_coach as ancien_coach , csf.ancien_cours as ancien_cours  ,
    r.room_name as Salle , cl.club_name as Club , cr.course_name as nouveau_cours , ch.firstname as nouveau_coach 
    FROM course_session as cs , course_session_fin as csf , course as cr , coach as ch , room as r , club as cl
    WHERE csf.active = 'Y'
    {$extra_join_query}
    AND csf.coach_session_id	= cs.id
    AND cs.course_id = cr.id
    AND cs.coach_id = ch.id
    AND cs.room_id = r.id
    AND cs.club_id = cl.id
    AND cs.course_session_date>=:startdate
    AND cs.course_session_date<=:enddate
    {$extra_join_query2}
    ORDER BY csf.id DESC
    "),$params_query
    );
    
    
    if(isset($parametres['export'])){
        
       $data=array_map(function($item){
           $item = (array) $item;
           if(isset($item['id'])) unset($item['id']);
           if(isset($item['color'])) unset($item['color']);
           return $item;
       },$data);
       
       $model = 'courses-'.$state;
       $Filename = $model.'-'.$startdate.'---'.$enddate;
       Excel::create($Filename, function($excel) use($data, $model) {
           $excel->sheet($model, function($sheet) use($data) {
               $sheet->fromArray($data);
           });
       })->export('xls');
       exit();
   }
   

    if(!$data){
      return $this->sendResponse(null, ['Global.EmptyResults'], false);
    }
    return $this->sendResponse($data, ['Global.GetDataWithSuccess'], true);
  }


  public function valid_hebdomadaire($params = null)
	{
    
    if($params){
      $parametres = decode_url_params($params);
    }

    $week_num = $parametres['week_num'];
    $minDate = $parametres['minDate'];
    $maxDate = $parametres['maxDate'];
    $club_id = $parametres['club'];
    $club = Club::find($club_id);
    $club_name = $club->club_name;
    
    if(!$week_num){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
    }

    $role_id = 4 ;
    $dataUser = DB::select(DB::raw("
    SELECT *
    FROM user as us
    WHERE active = 'Y'
    AND roles_mfk LIKE '%,{$role_id},%'
    ")
    );

    $data = DB::select(DB::raw("
        SELECT  cs.id as ID  , cl.club_name as Salle , cl.code_club as Code_analytique_salle , cs.course_session_day as Jour , cs.course_session_date as Date , cs.course_session_month as Mois , cs.course_session_month as MoisPaie , ch.registration_number as Matricule , ch.firstname as Coach ,  c.course_name as Cours , c.course_code as Code_du_cours , c.during as Nbr_heure  , csf.check_validate_remp as remplissage , cs.review as Commentaire , csf.nbr_paticipants as Participants    
        FROM course_session as cs , course as c , coach as ch , club as cl , course_session_fin as csf
        WHERE cs.active = 'Y' 
        AND cs.course_id=c.id
        AND cs.coach_id=ch.id 
        AND cs.club_id=cl.id
        AND cs.id=csf.coach_session_id
        ORDER BY cs.course_session_date ASC
        ")
    );

    

    foreach($data as $item){
      $this->add_remplissage_item_after_get($item);
    }
      
    $data=array_map(function($item){
        $item = (array) $item;
        if(isset($item['id'])) unset($item['id']);
        if(isset($item['color'])) unset($item['color']);
        return $item;
    },$data);

    $model = 'export_grh';
    $Filename = $model.'-'.$minDate.'>'.$maxDate;

    $excelFile = \Excel::create($Filename, function($excel) use($data, $model) {
        $excel->sheet($model, function($sheet) use($data) {
            $sheet->fromArray($data);
        });
    })
    ->store('xls', storage_path('excel/exports'));
     $file = storage_path('excel/exports') . '/'.$Filename.'.xls';
    
       
    foreach ($dataUser as $row) {
       $email = $row->email;
       $firstname = $row->firstname;
       \Mail::send('emails.notif_grh_validation_hebdomadaire', ['week_num' => $week_num , 'minDate' => $minDate , 'maxDate' => $maxDate , 'club_name' => $club_name ], function ($m) use($email , $firstname , $week_num , $minDate , $maxDate , $club_name , $file) {
        $m->subject('Nouvelle validation hebdomadaire');
        $m->to($email,$firstname);
        $m->attach($file);
       });
      }

      $emailGRH = 'grh@california-gym.com';
      $firstnameGRH = 'GRH';
      \Mail::send('emails.notif_grh_validation_hebdomadaire', ['week_num' => $week_num , 'minDate' => $minDate , 'maxDate' => $maxDate , 'club_name' => $club_name ], function ($m) use($emailGRH , $firstnameGRH , $week_num , $minDate , $maxDate , $club_name , $file) {
      $m->subject('Nouvelle validation hebdomadaire');
      $m->to($emailGRH,$firstnameGRH);
      $m->attach($file);
      });

    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }


  

  public function getParticipate($params = null)
  {
    if($params){
      $parametres = decode_url_params($params);
    }
  
    $extra_join_query2 = null;
    $club = $parametres['club'];
    $params_query[':club'] = $club;

    $startdate = $parametres['startdate'];
    $params_query[':startdate'] = $startdate;

    $enddate = $parametres['enddate'];
    $params_query[':enddate'] = $enddate;

    if($club == null){
      $extra_join_query2 .="\n
      AND cs.club_id = cl.id
      ";
    }else{
      $extra_join_query2 .="\n
      AND cs.club_id = :club
      ";
    }
   

    $data = DB::select(DB::raw("
    SELECT   csf.coach_session_id as Id , cs.course_session_date as Date ,
    cs.course_start_time as Heure_debut , cs.course_end_time as Heure_fin , 
    cr.course_name as Cours , ch.firstname as Nom_Coach , ch.lastname as Prenom_Coach , r.room_name as Salle , cl.club_name as Club , CONVERT(csf.nbr_paticipants, SIGNED INTEGER) as nbr_paticipants  
    FROM course_session as cs , course_session_fin as csf , course as cr , coach as ch , room as r , club as cl
    WHERE csf.active = 'Y'
    AND csf.nbr_paticipants <> 0
    AND csf.coach_session_id	= cs.id
    AND cs.course_id = cr.id
    AND cs.coach_id = ch.id
    AND cs.room_id = r.id
    AND cs.club_id = cl.id
    AND cs.course_session_date>=:startdate
    AND cs.course_session_date<=:enddate
    {$extra_join_query2}
    ORDER BY csf.id DESC
    "),$params_query
    );
    
    
    $state = 'partipants' ;
    if(isset($parametres['export'])){
      $data=array_map(function($item){
          $item = (array) $item;
          if(isset($item['id'])) unset($item['id']);
          if(isset($item['color'])) unset($item['color']);
          return $item;
      },$data);
      $model = 'courses-'.$state;
      $Filename = $model.'-'.$startdate.'---'.$enddate;
      Excel::create($Filename, function($excel) use($data, $model) {
          $excel->sheet($model, function($sheet) use($data) {
              $sheet->fromArray($data);
          });
      })->export('xls');
      exit();
    }

    if(!$data){
      return $this->sendResponse(null,['Global.EmptyResults'], false);
    }
    return $this->sendResponse($data,['Global.GetDataWithSuccess'], true);
  }


  function add_remplissage_item_after_get(&$item){

    $id = $item->ID;
    $result = CourseSessionFin::select('*')->where('coach_session_id', (int)$id)->first();
      if($result){
        if($result->check_validate_remp == 'on'){
          $item->remplissage = '1' ;
        }else{
          $item->remplissage = '' ;
        }
        
      }else{
        $item->remplissage = '' ;
      }
      
      $during = $item->Nbr_heure; 
      if($during >= 45){
        $item->Nbr_heure = '1' ;
      }else{
        $item->Nbr_heure = '0.5' ;
      }
      
     if($item->Jour < 21){
      $item->MoisPaie = $item->Mois ;
     }else{
      $item->MoisPaie = ($item->Mois)+1 ;
     }

      if (setlocale(LC_TIME, 'fr_FR') == '') {
        setlocale(LC_TIME, 'FRA');  //correction problème pour windows
        $format_jour = '%#d';
        } else {
        $format_jour = '%e';
      }
      $date = $item->Date;
      
      $dateLettre = strftime("%A $format_jour %B %Y", strtotime($date));
      $findme = ' ';
      $jourLettre = substr($dateLettre, 0 , strpos($dateLettre, $findme));   
      $item->Jour = $jourLettre;

      $date_modifie = preg_replace('#([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2})#',"$3/$2/$1", $date);
      $item->Date = $date_modifie;

    }


  public function annulerrefuse(Request $request)
	{   

		$course_session_id = $request->get('course_session_id');
        if(!$course_session_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
		
		$courseSessionFin = CourseSessionFin::find($course_session_id);
		
		if(!$courseSessionFin){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
		}
    

    $courseSessionFin->state = "validerannulerrefuse";
    $courseSessionFin->save();
    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }


  public function participerefuse(Request $request)
	{   
    $course_session_id = $request->get('course_session_id');
        if(!$course_session_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
		
		$courseSessionFin = CourseSessionFin::find($course_session_id);
		
		if(!$courseSessionFin){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
		}
    

    $courseSessionFin->state = "validerparticiperefuse";
    $courseSessionFin->save();
    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }

  public function replacerefuse(Request $request)
	{   
    $course_session_id = $request->get('course_session_id');
        if(!$course_session_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
		
		$courseSessionFin = CourseSessionFin::find($course_session_id);
		
		if(!$courseSessionFin){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
		}
    

    $courseSessionFin->state = "validerreplacerefuse";
    $courseSessionFin->save();
    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }

  public function valid(Request $request)
	{   
    $course_session_id = $request->get('course_session_id');
    if(!$course_session_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
    
    $courseSessionFin = CourseSessionFin::find($course_session_id);
    $courseSessionFin->state = "validerann";
    $courseSessionFin->save();
    if(!$courseSessionFin){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
    }
    
    $id_course_session = $courseSessionFin->coach_session_id ;
    $courseSession = CourseSession::find($id_course_session);
    $courseSession->active = "D";
    $courseSession->save();
    
    $rea_user = ReaUser::find(16);
    \Mail::send('emails.notif_grh', ['courseSession' => $courseSession], function ($m) use($rea_user , $courseSession) {
      $m->to($rea_user->email, $rea_user->firstname)->subject('Validation pour une annulation de cours par le controleur');
    });
    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }

  public function validnbr(Request $request)
	{   
    $course_session_id = $request->get('course_session_id');
    if(!$course_session_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
    
    $courseSessionFin = CourseSessionFin::find($course_session_id);
    $courseSessionFin->state = "validerpart";
    $courseSessionFin->save();
    if(!$courseSessionFin){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
    }
    
    
    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }

  public function validreplace(Request $request)
	{   
    $course_session_id = $request->get('course_session_id');
    if(!$course_session_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
    $courseSessionFin = CourseSessionFin::find($course_session_id);
    $courseSessionFin->state = "validerreplace";
    $courseSessionFin->save();
    if(!$courseSessionFin){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
    }
    return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
  }
  
 
  
 
  

  



}
