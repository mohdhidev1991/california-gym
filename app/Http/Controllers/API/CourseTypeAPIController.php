<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CourseTypeRepository;
use App\Models\CourseType;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CourseTypeAPIController extends AppBaseController
{
	/** @var  CourseTypeRepository */
	private $course_typeRepository;

	function __construct(CourseTypeRepository $course_typeRepo)
	{
		parent::__construct();
		$this->course_typeRepository = $course_typeRepo;
	}




	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CourseType();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}

		$query = CourseType::select(
			$model_fields
		);
		$query->where($model->getTable().'.active','<>',"D");

		if(isset($course_type_id)){
			$query->where($model->getTable().'.id',$course_type_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		$params = \Request::__get('params');
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		if( isset($join_obj) AND !empty($join_obj) ){
			$join_obj = explode('!', $join_obj);
		}else{
			$join_obj = [];
		}


		if( in_array('room',$join_obj) or in_array('all',$join_obj) ){
			$model_room = new Room();
			$item->rooms = Room::select($model_room->getFillable())->where('course_type_id',$item->id)->where('active','Y')->get();
		}
	}

	public function save(Request $request)
	{
		$data = $request->get('course_type');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$course_type = new CourseType;
		}else{
			$course_type = CourseType::find($data['id']);
		}

		if(!$course_type){
			return $this->sendResponse(null, ['CourseType.InvalidCourseType'], false);
		}

		$model = new CourseType;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $course_type->{$item} = $data[$item];
			if( empty($data[$item]) ) $course_type->{$item} = null;
		}
		$course_type->save();

		return $this->sendResponse($course_type->id, ['Form.DataSavedWithSuccess'], true);
	}


	public function delete(Request $request)
	{
		$course_type_id = $request->get('course_type_id');

		if(!$course_type_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$course_type = CourseType::find($course_type_id);
		if(!$course_type){
			return $this->sendResponse(null, ['CourseType.InvalideCourseType'], false);
		}
		$course_type->active = 'D';
		$course_type->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
