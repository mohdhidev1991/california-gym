<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CoursesConfigItemRepository;
use App\Models\Course;
use App\Models\CoursesConfigItem;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CoursesConfigItemAPIController extends AppBaseController
{
	/** @var  CoursesConfigItemRepository */
	private $coursesConfigItemRepository;

	function __construct(CoursesConfigItemRepository $coursesConfigItemRepo)
	{
		parent::__construct();
		$this->coursesConfigItemRepository = $coursesConfigItemRepo;
	}

	/**
	 * Q0019 - Q0020
	 * List by coursesConfigTemplateId and levelClassId
	 * HEAD|GET /CoursesConfigItem/listByCoursesConfigTemplateAndLevelClassId/cct_id/{courses_config_template_id}/lc_id/{level_class_id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function listByCoursesConfigTemplateAndLevelClassId($courses_config_template_id,$level_class_id)
	{

		$select = Course::select(
			DB::Raw($this->user->id),
			DB::Raw('now()'),
			DB::Raw('"Y"'),
			DB::raw(0),
			DB::raw('course.id as course_id'),
			DB::raw('courses_config_template.id as courses_config_template_id'),
			DB::Raw($level_class_id)
		)
			->join('courses_template', 'courses_template.course_mfk', 'LIKE', DB::Raw('CONCAT("%,",course.id,",%")'))
			->join('courses_config_template', 'courses_config_template.courses_template_id', '=', 'courses_template.id')
			->join('courses_config_item', function ($join) use ($level_class_id,$courses_config_template_id){
				$join->on('courses_config_item.courses_config_template_id', '=', 'courses_config_template.id')
					->on('courses_config_item.course_id', '=', 'course.id')
					->on('courses_config_item.level_class_id', '=', DB::raw($level_class_id));
			}, null, null, 'left outer')
			->where('courses_config_template.id', '=', DB::raw($courses_config_template_id))
			->whereNull('courses_config_item.id');
		$bindings = $select->getBindings();
		$insertQuery = 'INSERT into courses_config_item (created_by,created_at,active,version,course_id,courses_config_template_id,level_class_id) '
			. $select->toSql();

		$query  = DB::insert($insertQuery, $bindings);

		$result = CoursesConfigItem::select(
			'courses_config_item.id',
			'course.course_name_ar',
			'course.course_name_en',
			'courses_config_item.session_nb',
			'courses_config_item.coef'
		)
			->join('course', 'course.id', '=', 'courses_config_item.course_id')
			->where('courses_config_item.courses_config_template_id', '=', $courses_config_template_id)
			->where('courses_config_item.level_class_id','=',$level_class_id)->get();
		if(!$result)
			return $this->sendResponse(null, 'global.errors.emplty_results', false);


		foreach ($result as &$item) {
			if($item->session_nb!=null) $item->session_nb = (int)$item->session_nb;
			if($item->coef!=null) $item->coef = (int)$item->coef;
		}

		return $this->sendResponse($result, 'global.success.results_exists');
	}








	public function save(Request $request)
	{
		$data = $request->get('courses_config_item');

		if(!$data){
			return $this->sendResponse(null, Lang::get('courses_config_item.errors.empty_courses_config_item_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$courses_config_item = new CoursesConfigItem;
		}else{
			$courses_config_item = CoursesConfigItem::find($data['id']);
		}

		
		if(!$courses_config_item){
			return $this->sendResponse(null, Lang::get('courses_config_item.errors.courses_config_item_not_existe'), false);
		}

		if(isset($data['active'])) $courses_config_item->active = $data['active'];
		if(isset($data['courses_config_template_id'])) $courses_config_item->courses_config_template_id = $data['courses_config_template_id'];
		if(isset($data['course_id'])) $courses_config_item->course_id = $data['course_id'];
		if(isset($data['level_class_id'])) $courses_config_item->level_class_id = $data['level_class_id'];
		if(isset($data['session_nb'])) $courses_config_item->session_nb = $data['session_nb'];
		if(isset($data['coef'])) $courses_config_item->coef = $data['coef'];
		
		$courses_config_item->save();

		return $this->sendResponse($courses_config_item, Lang::get('global.success.save'), true);
	}







	public function multisave(Request $request)
	{
		$courses_config_items = $request->get('courses_config_items');

		if(!$courses_config_items){
			return $this->sendResponse(null, Lang::get('courses_config_item.errors.empty_courses_config_items_data'), false);
		}

		//return $this->sendResponse($courses_config_items, Lang::get('courses_config_item.errors.empty_courses_config_item_data'), false);
		foreach($courses_config_items as $item):
			if( !isset($item['id']) OR !$item['id'] ){
				return $this->sendResponse(null, Lang::get('courses_config_item.errors.empty_courses_config_item_data'), false);
			}
			$courses_config_item = CoursesConfigItem::find($item['id']);
			if( !$courses_config_item ){
				return $this->sendResponse(null, Lang::get('courses_config_item.errors.courses_config_item_not_existe'), false);
			}
			if(isset($item['active'])) $courses_config_item->active = $item['active'];
			if(isset($item['courses_config_template_id'])) $courses_config_item->courses_config_template_id = $item['courses_config_template_id'];
			if(isset($item['course_id']) ) $courses_config_item->course_id = $item['course_id'];
			if(isset($item['level_class_id'])) $courses_config_item->level_class_id = $item['level_class_id'];
			if(isset($item['session_nb'])) $courses_config_item->session_nb = $item['session_nb'];
			if(isset($item['coef'])) $courses_config_item->coef = $item['coef'];
			
			$courses_config_item->save();
		endforeach;

		

		return $this->sendResponse($courses_config_items, Lang::get('global.success.save'), true);
	}

}
