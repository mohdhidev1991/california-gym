<?php namespace App\Http\Controllers\API;

use App\Helpers\Server;
use App\Http\Requests;
use App\Libraries\Repositories\CoursesConfigTemplateRepository;
use App\Models\CoursesConfigTemplate;
use App\Models\SchoolLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CoursesConfigTemplateAPIController extends AppBaseController
{
	/** @var  CoursesConfigTemplateRepository */
	private $coursesConfigTemplateRepository;

	function __construct(CoursesConfigTemplateRepository $coursesConfigTemplateRepo)
	{
		parent::__construct();
		$this->coursesConfigTemplateRepository = $coursesConfigTemplateRepo;
	}







	/*** depricated in get ***/
	/**
	 * Q0017
	 * Created at 21-04-2016
	 * Created by Aziz
	 * Display a listing of the CoursesConfigTemplate by school id.
	 * GET|HEAD /CoursesConfigTemplates/listBySchoolId/{school_id}
	 *
	 * @return Response
	 */
	public function listBySchoolId($school_id = null)
	{
		if(!$school_id){
			$school_id = 0;
			if(isset($this->user->current_school_id)){
				$school_id = $this->user->current_school_id;
			}
		}
		$result = CoursesConfigTemplate::select('id', 'session_duration_min', 'courses_config_template_name as label', 'levels_template_id as levels_template_id', 'courses_template_id as courses_template_id')
										->where('school_id','=',$school_id)
										->where('active','=','Y')
										->get();
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.no_results'), false);
		}

		return $this->sendResponse($result, Lang::get('global.errors.results_exist'), true);
	}



	/*** depricated in get ***/
	/**
	 * Created at 01-05-2016
	 * Created by Achraf
	 * Display a //Q0018.
	 * GET|HEAD SchoolLevel/listByCoursesConfigTemplateId/{courses_config_template_id}
	 * BF 421-422 Q0018
	 * @return Response
	 */
	public function listByCoursesConfigTemplateId($courses_config_template_id)
	{

		$result = SchoolLevel::select('id', 'school_level_name_ar as label')
										->where('levels_template_id','=',$courses_config_template_id)
										->where('active','=','Y')
										->get();
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.no_results'), false);
		}

		return $this->sendResponse($result, Lang::get('global.errors.results_exist'), true);
	}









	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = CoursesConfigTemplate::select([
			'courses_config_template.*'
		]);

		if(isset($courses_config_template_id)){
			$query->where('courses_config_template.id',$courses_config_template_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where('courses_config_template.active',"Y");
		}


		if( isset($school_id) ){
			$query->where('courses_config_template.school_id',$school_id);
		}

		/* Used in Q0017 */
		if( isset($current_user_school) ){
			$query->orWhere(function($query)
            {
            	if( isset($this->user->current_school_id) ){
            		$query->where('courses_config_template.school_id', $this->user->current_school_id);
            	}
                $query->where('courses_config_template.created_by', $this->user->id);
            });
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('courses_config_template.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
            $this->filter_item_after_get($item);
        }
		$count = $query->count();
		$return = ['courses_config_templates' => $result, 'total' => $count];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}

	function filter_item_after_get(&$item){
		if($item->session_duration_min!=null) $item->session_duration_min = (int)$item->session_duration_min;
    }








	public function save(Request $request)
	{
		$data = $request->get('courses_config_template');

		if(!$data){
			return $this->sendResponse(null, Lang::get('courses_config_template.errors.empty_courses_config_template_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$courses_config_template = new CoursesConfigTemplate;
		}else{
			$courses_config_template = CoursesConfigTemplate::find($data['id']);	
		}

		
		if(!$courses_config_template){
			return $this->sendResponse(null, Lang::get('courses_config_template.errors.courses_config_template_not_existe'), false);
		}

		if( isset($data['active']) ) $courses_config_template->active = $data['active'];
		if( isset($data['courses_config_template_name_ar']) ) $courses_config_template->courses_config_template_name_ar = $data['courses_config_template_name_ar'];
		if( isset($data['courses_config_template_name_en']) ) $courses_config_template->courses_config_template_name_en = $data['courses_config_template_name_en'];
		if( isset($data['levels_template_id']) ) $courses_config_template->levels_template_id = $data['levels_template_id'];
		if($data['levels_template_id']==null) $courses_config_template->levels_template_id = null;
		if( isset($data['courses_template_id']) ) $courses_config_template->courses_template_id = $data['courses_template_id'];
		if( isset($data['session_duration_min']) ) $courses_config_template->session_duration_min = $data['session_duration_min'];
		if( isset($data['school_id']) ) $courses_config_template->school_id = $data['school_id'];
		
		$courses_config_template->save();

		return $this->sendResponse($courses_config_template, Lang::get('global.success.save'), true);
	}



}
