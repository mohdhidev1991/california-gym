<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CoursesTemplateRepository;
use App\Models\CoursesTemplate;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Helpers\MfkHelper;

class CoursesTemplateAPIController extends AppBaseController
{
	/** @var  CoursesTemplateRepository */
	private $courses_templateRepository;

	function __construct(CoursesTemplateRepository $courses_templateRepo)
	{
		parent::__construct();
		$this->courses_templateRepository = $courses_templateRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = CoursesTemplate::select([
			'courses_template.*'
		]);

		if(isset($courses_template_id)){
			$query->where('courses_template.id',$courses_template_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('courses_template.active',"Y");
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('courses_template.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}
		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['courses_templates' => $result, 'total' => $count];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($item){
		if( isset($item->course_mfk) AND !empty($item->course_mfk) ){
			$item->courses = MfkHelper::mfkIdsDecode($item->course_mfk);
		}else{
			$item->courses = [];
		}
	}





	public function save(Request $request)
	{
		$data = $request->get('courses_template');

		if(!$data){
			return $this->sendResponse(null, Lang::get('courses_template.errors.empty_courses_template_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$courses_template = new CoursesTemplate;
		}else{
			$courses_template = CoursesTemplate::find($data['id']);	
		}

		
		if(!$courses_template){
			return $this->sendResponse(null, Lang::get('courses_template.errors.courses_template_not_existe'), false);
		}

		if(isset($data['active'])) $courses_template->active = $data['active'];
		if(isset($data['courses_template_name_ar'])) $courses_template->courses_template_name_ar = $data['courses_template_name_ar'];
		if(isset($data['courses_template_name_en'])) $courses_template->courses_template_name_en = $data['courses_template_name_en'];
		if(isset($data['course_mfk'])) $courses_template->course_mfk = $data['course_mfk'];
		
		$courses_template->save();

		return $this->sendResponse($courses_template, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$courses_template_id = $request->get('courses_template_id');

		if(!$courses_template_id){
			return $this->sendResponse(null, Lang::get('courses_template.errors.empty_courses_template_id'), false);
		}

		$destroy = CoursesTemplate::destroy($courses_template_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('courses_template.errors.courses_template_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}
}
