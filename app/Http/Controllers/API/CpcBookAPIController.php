<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcBookRepository;
use App\Models\CpcBook;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CpcBookAPIController extends AppBaseController
{
	/** @var  CpcBookRepository */
	private $cpcBookRepository;

	function __construct(CpcBookRepository $cpcBookRepo)
	{
		parent::__construct();
		$this->cpcBookRepository = $cpcBookRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcBook;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcBook::select(
			$model_fields
		);

		if(isset($exclude_id)){
			$query->where($model->table.'.id','<>', (int)$exclude_id);
		}

		if(isset($cpc_book_id)){
			$query->where($model->table.'.id',$cpc_book_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','book_name']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);

		$result = $query->get();

		$total = $query->count();
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		$item->level_classes = \App\Helpers\MfkHelper::mfkIdsDecode($item->level_class_mfk);
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_book');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_book = new CpcBook;
		}else{
			$cpc_book = CpcBook::find($data['id']);
		}

		if(!$cpc_book){
			return $this->sendResponse(null, ['CpcBook.InvalideCpcBook'], false);
		}

		
		$model = new CpcBook;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field]) OR empty($data[$field])) $cpc_book->$field = $data[$field];
		}
		$cpc_book->save();
		return $this->sendResponse($cpc_book->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$cpc_book_id = $request->get('cpc_book_id');

		if(!$cpc_book_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcBook::destroy($cpc_book_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcBook.InvalideCpcBook'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}

}
