<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcBookCategoryRepository;
use App\Models\CpcBookCategory;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CpcBookCategoryAPIController extends AppBaseController
{
	/** @var  CpcBookCategoryRepository */
	private $cpcBookCategoryRepository;

	function __construct(CpcBookCategoryRepository $cpcBookCategoryRepo)
	{
		parent::__construct();
		$this->cpcBookCategoryRepository = $cpcBookCategoryRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcBookCategory;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcBookCategory::select(
			$model_fields
		);

		if(isset($cpc_book_category_id)){
			$query->where('cpc_book_category.id',$cpc_book_category_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where('cpc_book_category.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('cpc_book_category.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_book_category');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_book_category = new CpcBookCategory;
		}else{
			$cpc_book_category = CpcBookCategory::find($data['id']);
		}

		if(!$cpc_book_category){
			return $this->sendResponse(null, ['CpcBookCategory.InvalideCpcBookCategory'], false);
		}

		if(isset($data['active'])) $cpc_book_category->active = $data['active'];

		$model = new CpcBookCategory;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $cpc_book_category->$field = $data[$field];
		}
		$cpc_book_category->save();
		return $this->sendResponse($cpc_book_category->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$cpc_book_category_id = $request->get('cpc_book_category_id');

		if(!$cpc_book_category_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcBookCategory::destroy($cpc_book_category_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcBookCategory.InvalideCpcBookCategory'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
