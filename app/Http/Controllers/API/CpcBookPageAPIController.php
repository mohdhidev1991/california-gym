<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcBookPageRepository;
use App\Models\CpcBookPage;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Input;
use App\Models\File;
use App\Models\CpcBook;

class CpcBookPageAPIController extends AppBaseController
{
	/** @var  CpcBookPageRepository */
	private $cpcBookPageRepository;

	function __construct(CpcBookPageRepository $cpcBookPageRepo)
	{
		parent::__construct();
		$this->cpcBookPageRepository = $cpcBookPageRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcBookPage;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcBookPage::select(
			$model_fields
		);



		if( isset($join) AND !empty($join) ){
			$join = explode('!', $join);
		}else{
			$join = [];
		}

		if( in_array('cpc_book',$join) or in_array('all',$join) ){
			$query->join('cpc_book','cpc_book.id','=',$model->table.'.book_id');
			$query->AddSelect(['cpc_book.book_name']);
		}

		if(isset($book_id)){
			$query->where( $model->table.'.book_id',(int)$book_id);
		}


		if(isset($cpc_book_page_id)){
			$query->where($model->table.'.id',$cpc_book_page_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}




		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}


		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','book_page_num','book_page_name']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);


		$result = $query->get();
		$total = $query->count();

		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_book_page');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if(!isset($data['book_id'])){
			return $this->sendResponse(null, ['CPC.BookNotSelected'], false);
		}

		$cpc_book = CpcBook::find($data['book_id']);
		if(!$cpc_book){
			return $this->sendResponse(null, ['CPC.InvalidBook'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$query_exists_number = CpcBookPage::select('id')->where('book_id', $data['book_id'])->where('book_page_num', $data['book_page_num'])->count();
		}else{
			$query_exists_number = CpcBookPage::select('id')->where('book_id', $data['book_id'])->where('book_page_num', $data['book_page_num'])->where('id','<>',$data['id'])->count();
		}
		if($query_exists_number){
			return $this->sendResponse(null, ['CPC.BookPageNumberAlreadyUsed'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_book_page = new CpcBookPage;
		}else{
			$cpc_book_page = CpcBookPage::find($data['id']);
		}



		if(!$cpc_book_page){
			return $this->sendResponse(null, ['CpcBookPage.InvalideCpcBookPage'], false);
		}

		$model = new CpcBookPage;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $cpc_book_page->$field = $data[$field];
		}
		$cpc_book_page->save();
		return $this->sendResponse($cpc_book_page->id, ['Form.DataSavedWithSuccess'], true);
	}



	public function delete(Request $request)
	{
		$cpc_book_page_id = $request->get('cpc_book_page_id');

		if(!$cpc_book_page_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcBookPage::destroy($cpc_book_page_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcBookPage.InvalideCpcBookPage'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}



	public function upload_book(Request $request)
    {
    	$cpc_book_id = $request->get('cpc_book_id');
        $file = array('file' => Input::file('file'));
        $mime_type = Input::file('file')->getClientMimeType();

        if ( !$cpc_book_id ) {
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $cpc_book = CpcBook::find($cpc_book_id);
        if ( !$cpc_book ) {
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $mimes_ = array(
          'application/pdf',
        );
        $mime_type = Input::file('file')->getClientMimeType();
        $size = Input::file('file')->getSize();

        if ( !$size || !in_array($mime_type, $mimes_) ) {
            return $this->sendResponse(null, ['File.FileIsNotValid'], false);
        }

        

        /*if($size>config('app.max_upload_size')){
        	return $this->sendResponse(null, ['File.FileIsToBig'], false);
        }*/
        $rules = array('file' => 'required');
        $validator = \Validator::make($file, $rules);
        
        if ($validator->fails()) {
            return $this->sendResponse(null, ['File.FileIsNotValid'], false);
        }
        else {
            // checking file is valid.
            if (Input::file('file')->isValid()){

            	$destinationPath = storage_path("cpc/books");
                
                $extension = Input::file('file')->getClientOriginalExtension();

                $file = new File;
                $file->save();
                $file->active = 'Y';
                $file->file_name = $file->id.'.'.$extension;
                $file->original_name = Input::file('file')->getClientOriginalName();
                $file->file_ext = $extension;
                $file->file_type = $mime_type;
                $file->doc_type_id = config('doc_type.cpc_book',8);
                $fileName = $file->id.'.'.$extension;
                $file->picture = 'N';
                
                $file->file_size = $size;
                $file->owner_id = $this->user->id;
                $file->save();
                Input::file('file')->move($destinationPath, $fileName); // uploading file to given path

                $cpc_book->file_id = $file->id;
                $cpc_book->save();
                \App\Libraries\Factories\CpcBookFactory::generate_book_pages_from_file($cpc_book->id);

                return $this->sendResponse($file->id, ['File.SuccessUploadFile']);
            }
            else {
                // sending back with error message.
                return $this->sendResponse(null, ['File.FileIsNotValid'], false);
            }
        }
    }
}

