<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcBookTypeRepository;
use App\Models\CpcBookType;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CpcBookTypeAPIController extends AppBaseController
{
	/** @var  CpcBookTypeRepository */
	private $cpcBookTypeRepository;

	function __construct(CpcBookTypeRepository $cpcBookTypeRepo)
	{
		parent::__construct();
		$this->cpcBookTypeRepository = $cpcBookTypeRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcBookType;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcBookType::select(
			$model_fields
		);

		if(isset($cpc_book_type_id)){
			$query->where($model->table.'.id',$cpc_book_type_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->table.'.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_book_type');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_book_type = new CpcBookType;
		}else{
			$cpc_book_type = CpcBookType::find($data['id']);
		}

		if(!$cpc_book_type){
			return $this->sendResponse(null, ['CpcBookType.InvalideCpcBookType'], false);
		}

		if(isset($data['active'])) $cpc_book_type->active = $data['active'];

		$model = new CpcBookType;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $cpc_book_type->$field = $data[$field];
		}
		$cpc_book_type->save();
		return $this->sendResponse($cpc_book_type->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$cpc_book_type_id = $request->get('cpc_book_type_id');

		if(!$cpc_book_type_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcBookType::destroy($cpc_book_type_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcBookType.InvalideCpcBookType'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
