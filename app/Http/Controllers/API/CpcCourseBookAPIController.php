<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcCourseBookRepository;
use App\Models\CpcCourseBook;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CpcCourseBookAPIController extends AppBaseController
{
	/** @var  CpcCourseBookRepository */
	private $cpcCourseBookRepository;

	function __construct(CpcCourseBookRepository $cpcCourseBookRepo)
	{
		parent::__construct();
		$this->cpcCourseBookRepository = $cpcCourseBookRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcCourseBook;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcCourseBook::select(
			$model_fields
		);

		if(isset($cpc_course_book_id)){
			$query->where($model->table.'.id',$cpc_course_book_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->table.'.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_course_book');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_course_book = new CpcCourseBook;
		}else{
			$cpc_course_book = CpcCourseBook::find($data['id']);
		}

		if(!$cpc_course_book){
			return $this->sendResponse(null, ['CpcCourseBook.InvalideCpcCourseBook'], false);
		}

		if(isset($data['active'])) $cpc_course_book->active = $data['active'];

		$model = new CpcCourseBook;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $cpc_course_book->$field = $data[$field];
		}
		$cpc_course_book->save();
		return $this->sendResponse($cpc_course_book->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$cpc_course_book_id = $request->get('cpc_course_book_id');

		if(!$cpc_course_book_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcCourseBook::destroy($cpc_course_book_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcCourseBook.InvalideCpcCourseBook'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
