<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcCoursePlanRepository;
use App\Models\CpcCoursePlan;
use App\Models\CpcBook;
use App\Models\LevelClass;
use App\Models\CpcCourseProgram;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CpcCoursePlanAPIController extends AppBaseController
{
	/** @var  CpcCoursePlanRepository */
	private $cpcCoursePlanRepository;

	function __construct(CpcCoursePlanRepository $cpcCoursePlanRepo)
	{
		parent::__construct();
		$this->cpcCoursePlanRepository = $cpcCoursePlanRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcCoursePlan;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcCoursePlan::select(
			$model_fields
		);

		if(isset($cpc_course_plan_id)){
			$query->where($model->table.'.id',$cpc_course_plan_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if( isset($course_program_id) ){
			$query->where($model->table.'.course_program_id',$course_program_id);
		}
		if( isset($book_id) ){
			$query->where($model->table.'.course_book_id',$book_id);
		}
		if( isset($level_class_id) ){
			$query->where($model->table.'.level_class_id',$level_class_id);
		}
		
		if( isset($join) AND !empty($join) ){
			$join = explode('!', $join);
		}else{
			$join = [];
		}
		if( in_array('cpc_course_program',$join) or in_array('all',$join) ){
			$model_cpc_course_program = new CpcCourseProgram;
			$query->join($model_cpc_course_program->table,$model_cpc_course_program->table.'.id','=',$model->table.'.course_program_id');
			$query->AddSelect([$model_cpc_course_program->table.'.course_program_name_ar', $model_cpc_course_program->table.'.course_program_name_en']);
		}
		if( in_array('cpc_book',$join) or in_array('all',$join) ){
			$model_cpc_book = new CpcBook;
			$query->leftJoin($model_cpc_book->table,$model_cpc_book->table.'.id','=',$model->table.'.course_book_id');
			$query->AddSelect([$model_cpc_book->table.'.book_name']);
		}
		if( in_array('level_class',$join) or in_array('all',$join) ){
			$model_level_class = new LevelClass;
			$query->leftJoin($model_level_class->table,$model_level_class->table.'.id','=',$model->table.'.level_class_id');
			$query->AddSelect([$model_level_class->table.'.level_class_name_ar',$model_level_class->table.'.level_class_name_en']);
		}
		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','course_num']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);
		
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_course_plan');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_course_plan = new CpcCoursePlan;
		}else{
			$cpc_course_plan = CpcCoursePlan::find($data['id']);
		}

		if(!$cpc_course_plan){
			return $this->sendResponse(null, ['CpcCoursePlan.InvalideCpcCoursePlan'], false);
		}

		if(isset($data['active'])) $cpc_course_plan->active = $data['active'];

		$model = new CpcCoursePlan;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $cpc_course_plan->$field = $data[$field];
		}
		$cpc_course_plan->save();
		return $this->sendResponse($cpc_course_plan->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$cpc_course_plan_id = $request->get('cpc_course_plan_id');

		if(!$cpc_course_plan_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcCoursePlan::destroy($cpc_course_plan_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcCoursePlan.InvalideCpcCoursePlan'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
