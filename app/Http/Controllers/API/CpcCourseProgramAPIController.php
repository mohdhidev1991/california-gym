<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcCourseProgramRepository;
use App\Models\CpcCourseProgram;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CpcCourseProgramAPIController extends AppBaseController
{
	/** @var  CpcCourseProgramRepository */
	private $cpcCourseProgramRepository;

	function __construct(CpcCourseProgramRepository $cpcCourseProgramRepo)
	{
		parent::__construct();
		$this->cpcCourseProgramRepository = $cpcCourseProgramRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcCourseProgram;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcCourseProgram::select(
			$model_fields
		);

		if(isset($cpc_course_program_id)){
			$query->where($model->table.'.id',$cpc_course_program_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}



		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','course_program_name_ar','course_program_name_en']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_course_program');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_course_program = new CpcCourseProgram;
		}else{
			$cpc_course_program = CpcCourseProgram::find($data['id']);
		}

		if(!$cpc_course_program){
			return $this->sendResponse(null, ['CpcCourseProgram.InvalideCpcCourseProgram'], false);
		}

		$model = new CpcCourseProgram;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $cpc_course_program->$field = $data[$field];
		}
		$cpc_course_program->save();
		return $this->sendResponse($cpc_course_program->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$cpc_course_program_id = $request->get('cpc_course_program_id');

		if(!$cpc_course_program_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcCourseProgram::destroy($cpc_course_program_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcCourseProgram.InvalideCpcCourseProgram'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
