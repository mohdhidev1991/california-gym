<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CpcCourseProgramBookRepository;
use App\Models\CpcCourseProgramBook;
use App\Models\CpcCourseProgram;
use App\Models\CpcBook;
use App\Models\LevelClass;
use App\Models\Course;
use App\Models\CpcCoursePlan;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CpcCourseProgramBookAPIController extends AppBaseController
{
	/** @var  CpcCourseProgramBookRepository */
	private $cpcCourseProgramBookRepository;

	function __construct(CpcCourseProgramBookRepository $cpcCourseProgramBookRepo)
	{
		parent::__construct();
		$this->cpcCourseProgramBookRepository = $cpcCourseProgramBookRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CpcCourseProgramBook;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CpcCourseProgramBook::select(
			$model_fields
		);

		if(isset($cpc_course_program_book_id)){
			$query->where($model->table.'.id',$cpc_course_program_book_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if( isset($join) AND !empty($join) ){
			$join = explode('!', $join);
		}else{
			$join = [];
		}
		if( in_array('cpc_course_program',$join) or in_array('all',$join) ){
			$model_cpc_course_program = new CpcCourseProgram;
			$query->join($model_cpc_course_program->table,$model_cpc_course_program->table.'.id','=',$model->table.'.course_program_id');
			$query->AddSelect([$model_cpc_course_program->table.'.course_program_name_ar', $model_cpc_course_program->table.'.course_program_name_en']);
		}
		if( in_array('cpc_book',$join) or in_array('all',$join) ){
			$model_cpc_book = new CpcBook;
			$query->leftJoin($model_cpc_book->table,$model_cpc_book->table.'.id','=',$model->table.'.book_id');
			$query->AddSelect([$model_cpc_book->table.'.book_name']);
		}
		if( in_array('level_class',$join) or in_array('all',$join) ){
			$model_level_class = new LevelClass;
			$query->leftJoin($model_level_class->table,$model_level_class->table.'.id','=',$model->table.'.level_class_id');
			$query->AddSelect([$model_level_class->table.'.level_class_name_ar',$model_level_class->table.'.level_class_name_en']);
		}
		
		if( in_array('count_course_plan',$join) or in_array('all',$join) ){
			$model_course_plan_class = new CpcCoursePlan;
			$query->leftJoin($model_course_plan_class->table, function($d_join) use($model_course_plan_class, $model)
			{
				$d_join->on($model_course_plan_class->table.'.level_class_id', '=', $model->table.'.level_class_id')
				->on($model_course_plan_class->table.'.course_book_id', '=', $model->table.'.book_id')
				->on($model_course_plan_class->table.'.course_program_id', '=', $model->table.'.course_program_id')
				;
			});
			$query->AddSelect(\DB::raw('count('.$model_course_plan_class->table.'.id) as course_plans'));
			$query->GroupBy(\DB::raw($model->table.'.course_program_id,'.$model_course_plan_class->table.'.level_class_id,'.$model_course_plan_class->table.'.course_book_id'));
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($course_program_id) ){
			$query->where($model->table.'.course_program_id',$course_program_id);
		}
		if( isset($book_id) ){
			$query->where($model->table.'.book_id',$book_id);
		}
		if( isset($level_class_id) ){
			$query->where($model->table.'.level_class_id',$level_class_id);
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->table.'.id', 'ASC');
		$result = $query->get();
		
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
		if( isset($single) AND isset($result[0]) ){
			$result = $result[0];
		}
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
			if(isset($item->course_plans)) $item->course_plans = (int)$item->course_plans;
	}



	public function save(Request $request)
	{
		$data = $request->get('cpc_course_program_book');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$cpc_course_program_book = new CpcCourseProgramBook;
		}else{
			$cpc_course_program_book = CpcCourseProgramBook::find($data['id']);
		}

		if(!$cpc_course_program_book){
			return $this->sendResponse(null, ['CpcCourseProgramBook.InvalideCpcCourseProgramBook'], false);
		}

		if(isset($data['active'])) $cpc_course_program_book->active = $data['active'];

		$model = new CpcCourseProgramBook;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $cpc_course_program_book->$field = $data[$field];
		}
		$cpc_course_program_book->save();
		return $this->sendResponse($cpc_course_program_book->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$cpc_course_program_book_id = $request->get('cpc_course_program_book_id');

		if(!$cpc_course_program_book_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CpcCourseProgramBook::destroy($cpc_course_program_book_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CpcCourseProgramBook.InvalideCpcCourseProgramBook'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}






	public function save_book(Request $request)
	{
		$course_program_id = $request->get('course_program_id');
		$book_id = $request->get('book_id');
		$course_id = $request->get('course_id');
		$level_class_id = $request->get('level_class_id');
		$new = $request->get('new');
		$course_nb = $request->get('course_nb');
		$course_program_book = $request->get('course_program_book');
		$course_program_book_id = $request->get('id');
		
		if(
			!$course_program_id
			OR !$book_id
			OR !$level_class_id
			OR !$course_nb
		){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$course_program = CpcCourseProgram::find($course_program_id);
		if(!$course_program){
			return $this->sendResponse(null, ['CPC.InvalidCourseProgram'], false);
		}

		$book = CpcBook::find($book_id);
		if(!$book){
			return $this->sendResponse(null, ['CPC.InvalidBook'], false);
		}


		$level_class = LevelClass::find($level_class_id);
		if(!$level_class){
			return $this->sendResponse(null, ['LevelClass.InvalidLevelClass'], false);
		}

		if( $new ){
			$course_program_book_exists = CpcCourseProgramBook::where('book_id',$book_id)
			->where('level_class_id',$level_class_id)
			->where('course_program_id',$course_program_id)
			->count();
			if($course_program_book_exists){
				return $this->sendResponse(null, ['CPC.CourseProgramBookAlreadyExists'], false);
			}
			$cpc_course_program_book = new CpcCourseProgramBook;
		}else{
			$cpc_course_program_book = CpcCourseProgramBook::find($course_program_book_id);
		}

		if(!$cpc_course_program_book){
			return $this->sendResponse(null, ['CPC.InvalidCourseProgramPlan'], false);
		}

		$cpc_course_program_book->active = 'Y';
		$cpc_course_program_book->book_id = $book_id;
		$cpc_course_program_book->course_program_id = $course_program_id;
		$cpc_course_program_book->level_class_id = $level_class_id;
		$cpc_course_program_book->course_nb = $course_nb;
		$cpc_course_program_book->save();
		return $this->sendResponse($cpc_course_program_book->id, ['Form.DataSavedWithSuccess'], true);
	}



}
