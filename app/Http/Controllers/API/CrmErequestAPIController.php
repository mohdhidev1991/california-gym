<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CrmErequestRepository;
use App\Models\CrmErequest;
use App\Models\File;
use App\Models\CrmErequestFup;
use App\Models\HrmLeave;
use App\Models\CrmErequestStatus;
use App\Models\CrmErequestCat;
use App\Models\CrmErequestType;
use App\Models\HrmLeaveType;
use App\Models\ReaUser;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Libraries\Factories\ReaUserFactory;
use App\Libraries\Factories\CrmErequestFactory;

class CrmErequestAPIController extends AppBaseController
{
	/** @var  CrmErequestRepository */
	private $crmErequestRepository;

	function __construct(CrmErequestRepository $crmErequestRepo)
	{
		parent::__construct();
		$this->crmErequestRepository = $crmErequestRepo;
	}



	function erequest_levels(){
		return [
			1 => [
				'id' => 1,
				'erequest_level_name_ar' => translate('CRM','NormalLevel', null, 'ar'),
				'erequest_level_name_en' => translate('CRM','NormalLevel', null,'en'),
			],
			2 => [
				'id' => 2,
				'erequest_level_name_ar' => translate('CRM','HighLevel', null, 'ar'),
				'erequest_level_name_en' => translate('CRM','HighLevel', null, 'en'),
			],
		];
	}
	/*
		Crud methods
	*/
	public function get_levels($params = null)
	{
		$result = array_values($this->erequest_levels());
		if(empty($result)){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		$total = count($result);
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}



	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CrmErequest();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CrmErequest::select(
			$model_fields
		);


		if( isset($join) AND !empty($join) ){
			$join = explode('!', $join);
		}else{
			$join = [];
		}
		if( in_array('crm_erequest_status',$join) or in_array('all',$join) ){
			$query->join('crm_erequest_status','crm_erequest_status.id','=','crm_erequest.crm_erequest_status_id');
			$query->AddSelect([
				'crm_erequest_status.crm_erequest_status_name_ar',
				'crm_erequest_status.crm_erequest_status_name_en',
			]);
		}
		if( in_array('crm_erequest_type',$join) or in_array('all',$join) ){
			$query->join('crm_erequest_type','crm_erequest_type.id','=','crm_erequest.crm_erequest_type_id');
			$query->AddSelect([
				'crm_erequest_type.lookup_code',
				'crm_erequest_type.crm_erequest_type_name_ar',
				'crm_erequest_type.crm_erequest_type_name_en',
				'crm_erequest_type.atable_id',
			]);
		}
		if( in_array('crm_erequest_cat',$join) or in_array('all',$join) ){
			$query->join('crm_erequest_cat','crm_erequest_cat.id','=','crm_erequest.crm_erequest_cat_id');
			$query->AddSelect([
				'crm_erequest_cat.lookup_code as crm_erequest_cat_lookup_code',
				'crm_erequest_cat.crm_erequest_cat_name_ar',
				'crm_erequest_cat.crm_erequest_cat_name_en',
			]);
		}

		if( in_array('auser',$join) or in_array('all',$join) ){
			$query->join(config('database.connections.mysql.database').'.rea_user as auser_request','auser_request.id','=','crm_erequest.auser_id');
			$query->AddSelect([
				'auser_request.firstname',
				'auser_request.lastname',
				'auser_request.f_firstname',
				'auser_request.email',
				'auser_request.idn',
				'auser_request.mobile',
			]);
		}
		
		/*if( in_array('crm_erequest_fup_2',$join) or in_array('all',$join) ){
			//$query->leftJoin('crm_erequest_fup as tfup_not_readed','tfup_not_readed.crm_erequest_id','=','crm_erequest.id');
			$query->leftJoin('crm_erequest_fup as tfup_not_readed', function ($query) {
            	$query->on('tfup_not_readed.crm_erequest_id','=','crm_erequest.id');
            	$query->where('tfup_not_readed.is_read','=','N');
        	});
			$query->AddSelect([
				\DB::raw('count(tfup_not_readed.id) as fup_not_readed_xxx'),
			]);
		}*/

		/* Can get only my erequests */
		if(isset($received) AND $received){
			$query->where('crm_erequest.owner_auser_id', $this->user->id);
		}else{
			$query->where('crm_erequest.auser_id', $this->user->id);
		}
		$query->where(function ($query) {
	          // will added this after
	          //orWhere('crm_erequest.owner_stakeholder_id',$this->user->id);
        });

		if(isset($crm_erequest_id)){
			$query->where('crm_erequest.id',$crm_erequest_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			if(isset($full_infos)){
				$this->full_infos($single_item);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}


		if(!isset($active) OR ($active!='all') ){
			$query->where('crm_erequest.active',"Y");
		}

		if( !isset($include_archived) ){
			$query->where('crm_erequest.crm_erequest_status_id','<>',config('crm.crm_erequest_status.archived'));
			$query->where('crm_erequest.crm_erequest_status_id','<>',config('crm.crm_erequest_status.canceled'));
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','crm_erequest_hdate','crm_erequest_time']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);


		if(isset($order_by2) AND in_array($order_by2,['id','active','crm_erequest_hdate','crm_erequest_time']) ){
			$_order_by2 = $model->table.'.'.$order_by2;
			$_order2 = 'ASC';
			if( isset($order2) AND in_array($order2,['ASC','DESC']) ){
				$_order2 = $order2;
			}
			$query->orderBy($_order_by2,$_order2);
		}

		$query->orderBy($model->table.'.id','DESC');
		
		


		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count($model->table.'.id');
		//$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){

		$item = (object) $item;
		if(isset($item->atable_id) AND $item->atable_id) $item->atable_id = (int)$item->atable_id;
		$item->crm_files = (isset($item->crm_file_mfk) AND $item->crm_file_mfk)? \App\Helpers\MfkHelper::mfkDecode($item->crm_file_mfk) : null;

		$item->count_fup_not_read = $this->crmErequestRepository->count_fup_not_owner_not_readed_by_erequest_id($item->id, $this->user->id);
		$item->count_crm_erequest_fup = $this->crmErequestRepository->count_fup_by_erequest_id($item->id);
	}


	function full_infos(&$item){
		$attachements = array();
		if(isset($item->crm_files) AND !empty($item->crm_files)){
			foreach ($item->crm_files as $file_id) {
				$model_file = new File();
				$model_file_fields = $model_file->getFillable();
				foreach ($model_file_fields as &$item_field) {
					$item_field = $model_file->table.'.'.$item_field;
				}
				$file_ = (array) File::select($model_file_fields)->where('id',$file_id)->where('active','Y')->first()->toArray();
				$attachements[] = $file_;
			}
			$item->attachements = $attachements;
		}

		$item->auser = ReaUser::select(['rea_user.email','rea_user.firstname','rea_user.f_firstname','rea_user.lastname'])
		->where('id',$item->auser_id)
		->where('active','Y')
		->first();

		$item->count_fup_not_read = $this->crmErequestRepository->count_fup_not_owner_not_readed_by_erequest_id($item->id, $this->user->id);
		$item->count_crm_erequest_fup = $this->crmErequestRepository->count_fup_by_erequest_id($item->id);
		$item->settings = null;
		switch ($item->atable_id) {
			case 3462:
				$model_hrm_leave = new HrmLeave();
				$model_fields_hrm_leave = $model_hrm_leave->getFillable();
				foreach ($model_fields_hrm_leave as &$item_field) {
					$item_field = $model_hrm_leave->table.'.'.$item_field;
				}
				$model_fields_hrm_leave[] =  'hrm_leave_type.hrm_leave_type_name_ar';
				$model_fields_hrm_leave[] =  'hrm_leave_type.hrm_leave_type_name_en';
				$model_fields_hrm_leave[] =  'hrm_leave_type.free_time';
				$query_hrml_leave = HrmLeave::select(
					$model_fields_hrm_leave
				)
				->join('hrm_leave_type','hrm_leave_type.id','=',$model_hrm_leave->table.'.hrm_leave_type_id')
				->where('crm_erequest_id',$item->id)
				;
				$item->settings = $query_hrml_leave->first();
				break;
			
			default:
				# code...
				break;
		}
	}



	public function save(Request $request)
	{
		$data = $request->get('crm_erequest');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$crm_erequest = new CrmErequest;
		}else{
			$crm_erequest = CrmErequest::find($data['id']);
		}

		
		if(!$crm_erequest){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}

		if(isset($data['active'])) $crm_erequest->active = $data['active'];
		if(isset($data['auser_id'])) $crm_erequest->auser_id = $data['auser_id'];
		if(isset($data['crm_erequest_type_id'])) $crm_erequest->crm_erequest_type_id = $data['crm_erequest_type_id'];
		if(isset($data['crm_erequest_cat_id'])) $crm_erequest->crm_erequest_cat_id = $data['crm_erequest_cat_id'];
		if(isset($data['crm_erequest_level_enum'])) $crm_erequest->crm_erequest_level_enum = $data['crm_erequest_level_enum'];
		if(isset($data['crm_erequest_status_id'])) $crm_erequest->crm_erequest_status_id = $data['crm_erequest_status_id'];
		if(isset($data['crm_erequest_hdate'])) $crm_erequest->crm_erequest_hdate = $data['crm_erequest_hdate'];
		if(isset($data['crm_erequest_time'])) $crm_erequest->crm_erequest_time = $data['crm_erequest_time'];
		if(isset($data['crm_erequest_fup_hdate'])) $crm_erequest->crm_erequest_fup_hdate = $data['crm_erequest_fup_hdate'];
		if(isset($data['crm_erequest_fup_time'])) $crm_erequest->crm_erequest_fup_time = $data['crm_erequest_fup_time'];
		if(isset($data['crm_erequest_name'])) $crm_erequest->crm_erequest_name = $data['crm_erequest_name'];
		if(isset($data['crm_erequest_details'])) $crm_erequest->crm_erequest_details = $data['crm_erequest_details'];
		if(isset($data['crm_file_mfk'])) $crm_erequest->crm_file_mfk = $data['crm_file_mfk'];
		if(isset($data['owner_stakeholder_id'])) $crm_erequest->owner_stakeholder_id = $data['owner_stakeholder_id'];
		if(isset($data['owner_auser_id'])) $crm_erequest->owner_auser_id = $data['owner_auser_id'];
		
		$crm_erequest->save();

		return $this->sendResponse($crm_erequest->id, ['Form.DataSavedWithSuccess'], true);
	}



	public function delete(Request $request)
	{
		$crm_erequest_id = $request->get('crm_erequest_id');

		if(!$crm_erequest_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CrmErequest::destroy($crm_erequest_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


	public function cancel(Request $request)
	{
		$crm_erequest_id = $request->get('crm_erequest_id');

		if(!$crm_erequest_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$crm_erequest = CrmErequest::find($crm_erequest_id);
		if(!$crm_erequest OR $crm_erequest->active!='Y' OR $crm_erequest->crm_erequest_status_id!=config('crm.crm_erequest_status.new')){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}

		$count_crm_erequest_fup = $this->crmErequestRepository->count_fup_by_erequest_id($crm_erequest->id);

		if($count_crm_erequest_fup){
			return $this->sendResponse(null, ['CRM.RequestAlreadyHaveFollowUps'], false);
		}


		$crm_erequest->crm_erequest_status_id = config('crm.crm_erequest_status.canceled');
		$crm_erequest->save();


		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->is_read = 'N';
		$crm_erequest_fup->crm_erequest_id = $crm_erequest->id;
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->fup_comments = null;
		$crm_erequest_fup->fup_changes = null;
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = $crm_erequest->auser_id;
		$crm_erequest_fup->before_erequest_status_id = $crm_erequest->crm_erequest_status_id;
		$crm_erequest_fup->after_erequest_status_id = config('crm.crm_erequest_status.canceled');
		$status_from = CrmErequestStatus::find(config('crm.crm_erequest_status.new'));
		$status_to = CrmErequestStatus::find($crm_erequest_fup->after_erequest_status_id);
		$crm_erequest_fup->fup_changes = translate('CRM','RequestStatusChangedFromTo', ['status_from'=>$status_from->crm_erequest_status_name_ar, 'status_to'=>$status_to->crm_erequest_status_name_ar], 'ar');
		$crm_erequest_fup->save();

		return $this->sendResponse(null, ['CRM.RequestCanceledWithSuccess'], true);
	}



	public function submit(Request $request)
	{
		$data = $request->get('crm_erequest');

		if(!$data
			OR !isset($data['crm_erequest_type_id'])
			OR !isset($data['owner_auser_id'])
			OR !isset($data['crm_erequest_name'])
			OR !isset($data['crm_erequest_level_enum'])
			OR !isset($data['crm_erequest_details'])
			OR !isset($data['crm_erequest_cat_id'])
			OR empty($data['crm_erequest_type_id'])
			OR empty($data['owner_auser_id'])
			OR empty($data['crm_erequest_name'])
			OR empty($data['crm_erequest_level_enum'])
			OR empty($data['crm_erequest_details'])
			OR empty($data['crm_erequest_cat_id'])
			){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$attributes = [];
		$attributes['no_policy_selected'] = false;
		$school_stacke_holder_id = ReaUserFactory::get_current_school_stacke_holder_id();
		$my_jobs_ids = ReaUserFactory::get_my_school_jobs_ids();
		$my_jobrole_mfk = (!empty($my_jobs_ids))? \App\Helpers\MfkHelper::mfkEncode($my_jobs_ids) : ",,";

		$erequest_sh_id = null;
		$erequest_owner_id = null;

		$stakeholder_and_owner_request = CrmErequestFactory::get_stakeholder_and_owner_request($school_stacke_holder_id, $data['crm_erequest_type_id'], $data['crm_erequest_cat_id'], $data['crm_erequest_level_enum'], $my_jobrole_mfk);
		if(!$stakeholder_and_owner_request['erequest_sh_id'] OR !$stakeholder_and_owner_request['erequest_owner_id']){
			$attributes['no_policy_selected'] = ['CRM.TheRequestSubmittedButNotTransferredToThePartyConcernedBecausePolicySpecifiedForTheDistributionOfElectronicRequestsPleaseReferToYourAdministration'];
		}
		$crm_erequest = new CrmErequest;
		$crm_erequest->active = 'Y';
		$crm_erequest->auser_id = $this->user->id;
		$crm_erequest->crm_erequest_type_id = (int)$data['crm_erequest_type_id'];
		$crm_erequest->crm_erequest_cat_id = (int)$data['crm_erequest_cat_id'];
		$crm_erequest->crm_erequest_level_enum = (int)$data['crm_erequest_level_enum'];
		$crm_erequest->crm_erequest_status_id = config('crm.crm_erequest_status.new');
		$crm_erequest->crm_erequest_fup_hdate = null;
		$crm_erequest->crm_erequest_name = $data['crm_erequest_name'];
		$crm_erequest->crm_erequest_details = $data['crm_erequest_details'];
		$crm_erequest->owner_stakeholder_id = $stakeholder_and_owner_request['erequest_sh_id'];
		$crm_erequest->owner_auser_id = $stakeholder_and_owner_request['erequest_owner_id'];
		$crm_erequest->crm_erequest_time = date('H:i');
		$h_current_day = \App\Helpers\HijriDateHelper::to_hijri(date('Ymd'));
		$crm_erequest->crm_erequest_hdate = $h_current_day;

		$crm_erequest->crm_file_mfk = null;
		if( isset($data['files']) AND !empty($data['files']) ){
			foreach ($data['files'] as $item_file) {
				$file_ = File::find($item_file);
				$file_->active = 'Y';
				$file_->save();
			}
			$crm_erequest->crm_file_mfk = \App\Helpers\MfkHelper::mfkEncode($data['files']);
		}
		$crm_erequest->save();

		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->crm_erequest_id = $crm_erequest->id;
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = 0;
		$crm_erequest_fup->before_erequest_status_id = $crm_erequest->crm_erequest_status_id;
		$crm_erequest_fup->after_erequest_status_id = $crm_erequest->crm_erequest_status_id;
		if(!$stakeholder_and_owner_request['erequest_sh_id'] OR !$stakeholder_and_owner_request['erequest_owner_id']){
			$crm_erequest_fup->is_read = 'N';
			$crm_erequest_fup->fup_changes = translate('CRM','TheRequestSubmittedButNotTransferredToThePartyConcernedBecausePolicySpecifiedForTheDistributionOfElectronicRequestsPleaseReferToYourAdministration');
			$crm_erequest_fup->fup_comments = null;
		}else{
			$crm_erequest_fup->is_read = 'Y';
			$crm_erequest_fup->fup_changes = translate('CRM','TheRequestWasCreatedWithSuccessAndTransferredToThePartyConcernedAndAnEmployeeAssigneToAnswerWithTheDistributionPolicyNumber', ['policy_number' => $stakeholder_and_owner_request['crm_erequest_policy_id']]);
			$crm_erequest_fup->fup_comments = null;
		}
		$crm_erequest_fup->save();

		return $this->sendResponse($crm_erequest->id, ['Form.DataSavedWithSuccess']);
	}



	public function submit_settings(Request $request)
	{
		$data = $request->get('erequest_settings');

	
		if(!$data OR !isset($data['erequest_id']) ){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$crm_erequest = CrmErequest::select(['crm_erequest.id','crm_erequest.auser_id','crm_erequest_type.atable_id','crm_erequest.crm_erequest_type_id','crm_erequest.crm_erequest_cat_id'])
		->where('crm_erequest.id',(int)$data['erequest_id'])
		->where('crm_erequest.auser_id',(int)$this->user->id)
		->where('crm_erequest.crm_erequest_status_id',config('crm.crm_erequest_status.new'))
		->join('crm_erequest_type','crm_erequest_type.id','=','crm_erequest.crm_erequest_type_id')
		->first()
		->toArray();

		if(!$crm_erequest OR !isset($crm_erequest['atable_id']) ){
			return $this->sendResponse(null, ['CRM.InvalidRequestSettings'], false);
		}

		switch ($crm_erequest['atable_id']) {
			case 3462:
					$hrm_leave_already_inserted = HrmLeave::select(['hrm_leave.id'])
					->where('hrm_leave.crm_erequest_id',(int)$data['erequest_id'])
					->count();
					if(
						$hrm_leave_already_inserted
					){
						return $this->sendResponse(null, ['CRM.RequestSettingsAlreadySubmitted'], false);
					}

					if(
						!isset($data['leave_type_id']) OR empty($data['leave_type_id'])
					){
						return $this->sendResponse(null, ['CRM.InvalidLeaveType'], false);
					}

					$hrm_leave_type = HrmLeaveType::select(['hrm_leave_type.id','hrm_leave_type.free_time'])
					->where('hrm_leave_type.id',(int)$data['leave_type_id'])
					->where('hrm_leave_type.crm_erequest_type_id',(int)$crm_erequest['crm_erequest_type_id'])
					->where('hrm_leave_type.crm_erequest_cat_id',(int)$crm_erequest['crm_erequest_cat_id'])
					->first()->toArray();
					if(
						!$hrm_leave_type
					){
						return $this->sendResponse(null, ['CRM.InvalidLeaveType'], false);
					}

					if(
						0
						OR ($hrm_leave_type['free_time']=='Y' AND (!isset($data['hrm_leave_end_time']) OR empty($data['hrm_leave_end_time']) ) )
						OR ($hrm_leave_type['free_time']=='Y' AND ( !isset($data['hrm_leave_start_time']) OR empty($data['hrm_leave_start_time']) ) )

						OR !isset($data['hrm_leave_start_hdate'])
						OR empty($data['hrm_leave_start_hdate'])

						OR !isset($data['hrm_leave_end_hdate'])
						OR empty($data['hrm_leave_end_hdate'])

						OR !isset($data['hrm_leave_name'])
						OR empty($data['hrm_leave_name'])

						OR !isset($data['hrm_leave_reason'])
						OR empty($data['hrm_leave_reason'])
					){
						return $this->sendResponse(null, ['Form.EmptyData'], false);
					}
					$current_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
					$hrm_leave_start_hdate = date('Ymd',strtotime($data['hrm_leave_start_hdate']));
					$hrm_leave_end_hdate = date('Ymd',strtotime($data['hrm_leave_end_hdate']));
					$hrm_leave_start_time = null;
					if(isset($data['hrm_leave_start_time'])){
						$hrm_leave_start_time = date('H:i',strtotime($data['hrm_leave_start_time']));
					}
					$hrm_leave_end_time = null;
					if(isset($data['hrm_leave_end_time'])){
						$hrm_leave_end_time = date('H:i',strtotime($data['hrm_leave_end_time']));
					}
					
					if(
						$hrm_leave_start_hdate<=$current_hdate
						OR $hrm_leave_end_hdate<=$current_hdate
					){
						return $this->sendResponse(null, ['CRM.YouCantSelectDateInThePast'], false);
					}

					if(
						$hrm_leave_start_hdate>$hrm_leave_end_hdate
					){
						return $this->sendResponse(null, ['CRM.LeaveEndDateShouldBeGreaterOrEqualToTheLeaveStartDate'], false);
					}

					if(
						1
						AND $hrm_leave_type['free_time']=='Y'
						AND $hrm_leave_start_hdate==$hrm_leave_end_hdate
						AND $hrm_leave_start_time>=$hrm_leave_end_time
					){
						return $this->sendResponse(null, ['CRM.LeaveEndTimeShouldBeGreaterThanTheLeaveStartTime'], false);
					}

					$hrm_leave = new HrmLeave;
					$hrm_leave->active = 'Y';
					$hrm_leave->hrm_leave_start_hdate = $hrm_leave_start_hdate;
					$hrm_leave->hrm_leave_end_hdate = $hrm_leave_end_hdate;
					$hrm_leave->hrm_leave_start_time = $hrm_leave_start_time;
					$hrm_leave->hrm_leave_end_hdate = $hrm_leave_end_hdate;
					$hrm_leave->hrm_leave_name = $data['hrm_leave_name'];
					$hrm_leave->hrm_leave_reason = $data['hrm_leave_reason'];
					$hrm_leave->approved = null;
					$hrm_leave->crm_erequest_id = (int)$crm_erequest['id'];
					$hrm_leave->owner_id = (int)$crm_erequest['auser_id'];
					$hrm_leave->hrm_leave_type_id = (int)$data['leave_type_id'];
					$hrm_leave->save();
				break;
			
			default:
				return $this->sendResponse(null, ['CRM.InvalidRequestSettings'], false);
				break;
		}

		return $this->sendResponse(null, ['CRM.RequestSettingsSubmittedWithSuccess']);
	}



	public function submit_follow_up(Request $request)
	{
		$data = $request->get('follow_up');

		if(!$data OR !isset($data['erequest_id']) ){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$crm_erequest = CrmErequest::select(['crm_erequest.id','crm_erequest.auser_id','crm_erequest.crm_erequest_status_id','crm_erequest.owner_auser_id'])
		->where('crm_erequest.id',(int)$data['erequest_id'])
		->where('crm_erequest.auser_id',(int)$this->user->id)
		->first();

		if(!$crm_erequest){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}
		$crm_erequest = $crm_erequest->toArray();

		if(!in_array($crm_erequest['crm_erequest_status_id'],[config('crm.crm_erequest_status.new'), config('crm.crm_erequest_status.ongoing'), config('crm.crm_erequest_status.need_verification')])){
			return $this->sendResponse(null, ['CRM.YouCantAddNewFollowUpThisRequestAlreadyClosedOrArchived'], false);
		}

		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->is_read = 'N';
		$crm_erequest_fup->crm_erequest_id = $data['erequest_id'];
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->fup_comments = $data['fup_comments'];
		$crm_erequest_fup->fup_changes = null;
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = $crm_erequest['auser_id'];
		$crm_erequest_fup->before_erequest_status_id = $crm_erequest['crm_erequest_status_id'];
		$crm_erequest_fup->after_erequest_status_id = $data['status_id'];
		$crm_erequest_fup->save();

		if($data['status_id']!=$crm_erequest['crm_erequest_status_id']){
			$_crm_erequest = CrmErequest::find($data['erequest_id']);
			$_crm_erequest->crm_erequest_status_id = $data['status_id'];
			$_crm_erequest->save();
			$status_from = CrmErequestStatus::find($crm_erequest['crm_erequest_status_id']);
			$status_to = CrmErequestStatus::find($data['status_id']);
			$crm_erequest_fup->fup_changes = translate('CRM','RequestStatusChangedFromTo', ['status_from'=>$status_from->crm_erequest_status_name_ar, 'status_to'=>$status_to->crm_erequest_status_name_ar]);
			$crm_erequest_fup->save();
		}
		

		return $this->sendResponse(null, ['CRM.FollowUpAddedWithSuccess']);
	}

	public function submit_reply_follow_up(Request $request)
	{
		$data = $request->get('follow_up');

		if(!$data OR !isset($data['erequest_id']) ){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$crm_erequest = CrmErequest::select(['crm_erequest.id','crm_erequest.auser_id','crm_erequest.crm_erequest_status_id','crm_erequest.owner_auser_id','crm_erequest_type.atable_id'])
		->join('crm_erequest_type','crm_erequest_type.id','=','crm_erequest.crm_erequest_type_id')
		->where('crm_erequest.id',(int)$data['erequest_id'])
		->where('crm_erequest.owner_auser_id',(int)$this->user->id)
		->first();

		if(!$crm_erequest){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}
		$crm_erequest = $crm_erequest->toArray();

		if(!in_array($crm_erequest['crm_erequest_status_id'],[config('crm.crm_erequest_status.new'), config('crm.crm_erequest_status.ongoing'), config('crm.crm_erequest_status.need_verification')])){
			return $this->sendResponse(null, ['CRM.YouCantAddNewFollowUpThisRequestAlreadyClosedOrArchived'], false);
		}

		if($data['status_id']==config('crm.crm_erequest_status.done') AND isset($crm_erequest['atable_id']) AND ($crm_erequest['atable_id'])){
			switch ($crm_erequest['atable_id']) {
				case 3462:
					$hrml_leave = HrmLeave::select('*')

					->where('crm_erequest_id',$crm_erequest['id'])
					->first();
					if(!$hrml_leave){
						return $this->sendResponse(null, ['CRM.LeaveInformationsNotInsertedForThisRequest'], false);
					}

					if(!$hrml_leave->approved){
						return $this->sendResponse(null, ['CRM.PleaseAnswerToTheRequestSettingsBeforeChangingRequestStatusToDone'], false);
					}
					break;
				
				default:
					# code...
					break;
			}
			
		}


		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->is_read = 'N';
		$crm_erequest_fup->crm_erequest_id = $data['erequest_id'];
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->fup_comments = $data['fup_comments'];
		$crm_erequest_fup->fup_changes = null;
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = $crm_erequest['owner_auser_id'];
		$crm_erequest_fup->before_erequest_status_id = $crm_erequest['crm_erequest_status_id'];
		$crm_erequest_fup->after_erequest_status_id = $data['status_id'];
		$crm_erequest_fup->save();

		if($data['status_id']!=$crm_erequest['crm_erequest_status_id']){
			$_crm_erequest = CrmErequest::find($data['erequest_id']);
			$_crm_erequest->crm_erequest_status_id = $data['status_id'];
			$_crm_erequest->save();
			$status_from = CrmErequestStatus::find($crm_erequest['crm_erequest_status_id']);
			$status_to = CrmErequestStatus::find($data['status_id']);
			$crm_erequest_fup->fup_changes = translate('CRM','RequestStatusChangedFromTo', ['status_from'=>$status_from->crm_erequest_status_name_ar, 'status_to'=>$status_to->crm_erequest_status_name_ar]);
			$crm_erequest_fup->save();
		}
		

		return $this->sendResponse(null, ['CRM.FollowUpAddedWithSuccess']);
	}



	public function approve_leave(Request $request)
	{
		$crm_erequest_id = $request->get('crm_erequest_id');

		if(!$crm_erequest_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}


		$crm_erequest = CrmErequest::select('*')
		->whereIn('crm_erequest_status_id',[config('crm.crm_erequest_status.new'),config('crm.crm_erequest_status.ongoing'),config('crm.crm_erequest_status.need_verification')])
		->where('owner_auser_id',$this->user->id)
		->where('active','Y')
		->where('id',$crm_erequest_id)
		->first();
		if(!$crm_erequest){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}

		$hrml_leave = HrmLeave::select('hrm_leave.*')
		->where('hrm_leave.crm_erequest_id',$crm_erequest->id)
		->first();
		if(!$hrml_leave){
			return $this->sendResponse(null, ['CRM.LeaveInformationsNotInsertedForThisRequest'.$crm_erequest['atable_id']], false);
		}

		if(in_array($hrml_leave->approved, ['Y','N'])){
			return $this->sendResponse(null, ['CRM.LeaveRequestAlreadyAnswered'], false);
		}
		
		$hrml_leave->approved = 'Y';
		$hrml_leave->approval_auser_id = $this->user->id;
		$hrml_leave->save();
		return $this->sendResponse(null, ['CRM.LeaveRequestApproved']);
	}

	public function disapprove_leave(Request $request)
	{
		$crm_erequest_id = $request->get('crm_erequest_id');

		if(!$crm_erequest_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}


		$crm_erequest = CrmErequest::select('*')
		->whereIn('crm_erequest_status_id',[config('crm.crm_erequest_status.new'),config('crm.crm_erequest_status.ongoing'),config('crm.crm_erequest_status.need_verification')])
		->where('owner_auser_id',$this->user->id)
		->where('active','Y')
		->where('id',$crm_erequest_id)
		->first();
		if(!$crm_erequest){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}

		$hrml_leave = HrmLeave::select('hrm_leave.*')
		->where('hrm_leave.crm_erequest_id',$crm_erequest->id)
		->first();
		if(!$hrml_leave){
			return $this->sendResponse(null, ['CRM.LeaveInformationsNotInsertedForThisRequest'.$crm_erequest['atable_id']], false);
		}

		if(in_array($hrml_leave->approved, ['Y','N'])){
			return $this->sendResponse(null, ['CRM.LeaveRequestAlreadyAnswered'], false);
		}
		
		$hrml_leave->approved = 'N';
		$hrml_leave->approval_auser_id = $this->user->id;
		$hrml_leave->save();
		return $this->sendResponse(null, ['CRM.LeaveRequestDisapproved']);
	}




	public function redirect_erequest(Request $request)
	{
		$data = $request->get('crm_erequest');

		if(!$data OR !isset($data['id'])
			OR !isset($data['crm_erequest_type_id'])
			OR !isset($data['crm_erequest_cat_id'])
			OR !isset($data['crm_erequest_level_enum'])
			OR !isset($data['comment'])
		){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}


		$crm_erequest = CrmErequest::select('*')
		->whereIn('crm_erequest_status_id',[config('crm.crm_erequest_status.new'),config('crm.crm_erequest_status.ongoing'),config('crm.crm_erequest_status.need_verification')])
		->where('owner_auser_id',$this->user->id)
		->where('active','Y')
		->where('id',$data['id'])
		->first();
		if(!$crm_erequest){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}

		$crm_erequest->crm_erequest_type_id = $data['crm_erequest_type_id'];
		$crm_erequest->crm_erequest_cat_id = $data['crm_erequest_cat_id'];
		$crm_erequest->crm_erequest_level_enum = $data['crm_erequest_level_enum'];
		$crm_erequest->crm_erequest_status_id = config('crm.crm_erequest_status.new');
		

		if(!$crm_erequest->isDirty()){
			return $this->sendResponse(null, ['CRM.YouDidNotChangeAnythingForThisRequest'], false);
		}

		
		$original_crm_erequest_status_id = $crm_erequest->crm_erequest_status_id;
		$attributes = [];
		$attributes['no_policy_selected'] = false;
		
		$school_id = ReaUserFactory::get_current_school_id();
		$school_stacke_holder_id = ReaUserFactory::get_current_school_stacke_holder_id();
		$jobs_ids = ReaUserFactory::get_rea_user_school_jobs_ids($crm_erequest->auser_id, $school_id);

		$jobrole_mfk = (!empty($jobs_ids))? \App\Helpers\MfkHelper::mfkEncode($jobs_ids) : ",,";
		$erequest_sh_id = null;
		$erequest_owner_id = null;
		$stakeholder_and_owner_request = CrmErequestFactory::get_stakeholder_and_owner_request($school_stacke_holder_id, $data['crm_erequest_type_id'], $data['crm_erequest_cat_id'], $data['crm_erequest_level_enum'], $jobrole_mfk);

		$crm_erequest->owner_stakeholder_id = $stakeholder_and_owner_request['erequest_sh_id'];
		$crm_erequest->owner_auser_id = $stakeholder_and_owner_request['erequest_owner_id'];

		$changes = $crm_erequest->getDirty();
		$originals = $crm_erequest->getOriginal();
		$crm_erequest->save();
		

		$text_changes = null;
		if(isset($changes['crm_erequest_type_id'])){
			$type_from = CrmErequestType::find($originals['crm_erequest_type_id']);
			$type_to = CrmErequestType::find($changes['crm_erequest_type_id']);
			$text_changes .= translate('CRM','RequestTypeChangedFromTo', ['type_from' => $type_from->crm_erequest_type_name_ar, 'type_to' => $type_to->crm_erequest_type_name_ar] );
		}

		if(isset($changes['crm_erequest_cat_id'])){
			$cat_from = CrmErequestCat::find($originals['crm_erequest_cat_id']);
			$cat_to = CrmErequestCat::find($changes['crm_erequest_cat_id']);
			if($text_changes)$text_changes .= ' - ';
			$text_changes .= translate('CRM','RequestCategoryChangedFromTo', ['cat_from' => $cat_from->crm_erequest_cat_name_ar, 'cat_to' => $cat_to->crm_erequest_cat_name_ar] );
		}

		if(isset($changes['crm_erequest_level_enum'])){
			$levels = $this->erequest_levels();
			$level_from = (isset($levels[$originals['crm_erequest_level_enum']]))? $levels[$originals['crm_erequest_level_enum']]['erequest_level_name_ar'] : null;
			$level_to = (isset($levels[$changes['crm_erequest_level_enum']]))? $levels[$changes['crm_erequest_level_enum']]['erequest_level_name_ar'] : null;
			if($text_changes) $text_changes .= ' - ';
			$text_changes .= translate('CRM','RequestLevelChangedFromTo', ['level_from' => $level_from, 'level_to' => $level_to] );
		}

		/* Follow up redirected */
		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->crm_erequest_id = $crm_erequest->id;
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = $this->user->id;
		$crm_erequest_fup->before_erequest_status_id = $original_crm_erequest_status_id;
		$crm_erequest_fup->after_erequest_status_id = config('crm.crm_erequest_status.new');
		$crm_erequest_fup->is_read = 'N';
		$crm_erequest_fup->fup_changes = $text_changes;
		$crm_erequest_fup->fup_comments = $data['comment'];
		$crm_erequest_fup->save();


		/* Follow up select now policy */
		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->crm_erequest_id = $crm_erequest->id;
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = 0;
		$crm_erequest_fup->before_erequest_status_id = $original_crm_erequest_status_id;
		$crm_erequest_fup->after_erequest_status_id = config('crm.crm_erequest_status.new');
		$crm_erequest_fup->fup_comments = null;
		if(!$stakeholder_and_owner_request['erequest_sh_id'] OR !$stakeholder_and_owner_request['erequest_owner_id']){
			$crm_erequest_fup->is_read = 'N';
			$crm_erequest_fup->fup_changes = translate('CRM','TheRequestRedirectetButNotTransferredToThePartyConcernedBecausePolicySpecifiedForTheDistributionOfElectronicRequestsPleaseReferToYourAdministration');
		}else{
			$crm_erequest_fup->is_read = 'Y';
			$crm_erequest_fup->fup_changes = translate('CRM','TheRequestRedirectedAndTransferredToThePartyConcernedAndAnEmployeeAssigneToAnswerWithTheDistributionPolicyNumber', ['policy_number' => $stakeholder_and_owner_request['crm_erequest_policy_id']]);
		}
		$crm_erequest_fup->save();

		$attributes = [];
		if($this->user->id!=$stakeholder_and_owner_request['erequest_owner_id']){
			$attributes['owner_changed'] = true;
		}else{
			$attributes['owner_changed'] = false;
		}
		return $this->sendResponse(null, ['CRM.RequestRedirectedWithSuccess'], true, null, $attributes);
	}

}


