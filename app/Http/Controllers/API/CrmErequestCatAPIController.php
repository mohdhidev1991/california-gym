<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CrmErequestCatRepository;
use App\Models\CrmErequestCat;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class CrmErequestCatAPIController extends AppBaseController
{
	/** @var  CrmErequestCatRepository */
	private $crmErequestCatRepository;

	function __construct(CrmErequestCatRepository $crmErequestCatRepo)
	{
		parent::__construct();
		$this->crmErequestCatRepository = $crmErequestCatRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new CrmErequestCat();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CrmErequestCat::select(
			$model_fields
		);

		if(isset($crm_erequest_cat_id)){
			$query->where('crm_erequest_cat.id',$crm_erequest_cat_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if( isset($crm_erequest_type_id) ){
			$query->where('crm_erequest_cat.crm_erequest_type_id',(int)$crm_erequest_type_id);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('crm_erequest_cat.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('crm_erequest_cat.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
	}



	public function save(Request $request)
	{
		$data = $request->get('crm_erequest_cat');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$crm_erequest_cat = new CrmErequestCat;
		}else{
			$crm_erequest_cat = CrmErequestCat::find($data['id']);	
		}

		
		if(!$crm_erequest_cat){
			return $this->sendResponse(null, ['ErequestCat.InvalideDay'], false);
		}

		if(isset($data['active'])) $crm_erequest_cat->active = $data['active'];
		if(isset($data['lookup_code'])) $crm_erequest_cat->lookup_code = $data['lookup_code'];
		if(isset($data['domain_id'])) $crm_erequest_cat->domain_id = $data['domain_id'];
		if(isset($data['crm_erequest_cat_name_ar'])) $crm_erequest_cat->crm_erequest_cat_name_ar = $data['crm_erequest_cat_name_ar'];
		if(isset($data['crm_erequest_cat_name_en'])) $crm_erequest_cat->crm_erequest_cat_name_en = $data['crm_erequest_cat_name_en'];
		if(isset($data['public'])) $crm_erequest_cat->public = $data['public'];
		if(isset($data['authorized_jobrole_mfk'])) $crm_erequest_cat->authorized_jobrole_mfk = $data['authorized_jobrole_mfk'];
		if(isset($data['atable_id'])) $crm_erequest_cat->atable_id = $data['atable_id'];
		
		$crm_erequest_cat->save();

		return $this->sendResponse($crm_erequest_cat->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$crm_erequest_cat_id = $request->get('crm_erequest_cat_id');

		if(!$crm_erequest_cat_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CrmErequestCat::destroy($crm_erequest_cat_id);
		if(!$destroy){
			return $this->sendResponse(null, ['ErequestCat.InvalideDay'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
