<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CrmErequestFupRepository;
use App\Models\CrmErequestFup;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Libraries\Factories\ReaUserFactory;

class CrmErequestFupAPIController extends AppBaseController
{
	/** @var  CrmErequestFupRepository */
	private $crmErequestFupRepository;

	function __construct(CrmErequestFupRepository $crmErequestFupRepo)
	{
		parent::__construct();
		$this->crmErequestFupRepository = $crmErequestFupRepo;
	}

	/*
		Crud methods
	*/
	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}


		$model = new CrmErequestFup();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CrmErequestFup::select(
			$model_fields
		);

		if(isset($crm_erequest_fup_id)){
			$query->where('crm_erequest_fup.id',$crm_erequest_fup_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('crm_erequest_fup.active',"Y");
		}
		

		if( isset($crm_erequest_id) ){
			$query->where('crm_erequest_fup.crm_erequest_id', (int)$crm_erequest_id);
		}

		/* Can get only my erequests */
		//$query->where('crm_erequest_fup.owner_id', $this->user->id);
		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','fup_hdate','fup_time']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);


		if(isset($order_by2) AND in_array($order_by2,['id','active','fup_hdate','fup_time']) ){
			$_order_by2 = $model->table.'.'.$order_by2;
			$_order2 = 'ASC';
			if( isset($order2) AND in_array($order2,['ASC','DESC']) ){
				$_order2 = $order2;
			}
			$query->orderBy($_order_by2,$_order2);
		}

		$result = $query->get();
		$total = $query->count();
		
		if(!$total){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		if($item->owner_id==$this->user->id) $item->is_read = null;
	}



	public function save(Request $request)
	{
		$data = $request->get('crm_erequest_fup');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$crm_erequest_fup = new CrmErequestFup;
		}else{
			$crm_erequest_fup = CrmErequestFup::find($data['id']);	
		}

		
		if(!$crm_erequest_fup){
			return $this->sendResponse(null, ['CRM.InvalideRequestFollowUp'], false);
		}

		if(isset($data['active'])) $crm_erequest_fup->active = $data['active'];
		if(isset($data['lookup_code'])) $crm_erequest_fup->lookup_code = $data['lookup_code'];
		if(isset($data['domain_id'])) $crm_erequest_fup->domain_id = $data['domain_id'];
		if(isset($data['crm_erequest_fup_name_ar'])) $crm_erequest_fup->crm_erequest_fup_name_ar = $data['crm_erequest_fup_name_ar'];
		if(isset($data['crm_erequest_fup_name_en'])) $crm_erequest_fup->crm_erequest_fup_name_en = $data['crm_erequest_fup_name_en'];
		if(isset($data['public'])) $crm_erequest_fup->public = $data['public'];
		if(isset($data['authorized_jobrole_mfk'])) $crm_erequest_fup->authorized_jobrole_mfk = $data['authorized_jobrole_mfk'];
		if(isset($data['atable_id'])) $crm_erequest_fup->atable_id = $data['atable_id'];
		
		$crm_erequest_fup->save();

		return $this->sendResponse($crm_erequest_fup->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$crm_erequest_fup_id = $request->get('crm_erequest_fup_id');

		if(!$crm_erequest_fup_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CrmErequestFup::destroy($crm_erequest_fup_id);
		if(!$destroy){
			return $this->sendResponse(null, ['CRM.InvalideRequestFollowUp'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}

	public function is_read(Request $request)
	{
		$crm_erequest_fup_id = $request->get('crm_erequest_fup_id');

		if(!$crm_erequest_fup_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$crm_erequest_fup = CrmErequestFup::find($crm_erequest_fup_id);
		if(!$crm_erequest_fup OR $crm_erequest_fup->active!='Y'){
			return $this->sendResponse(null, ['CRM.InvalideRequestFollowUp'], false);
		}
		if($crm_erequest_fup->owner_id==$this->user->id){
			return $this->sendResponse(null, ['CRM.YouAreAlreadyTheOwnerForThisFollowUp'], false);
		}
		if($crm_erequest_fup->is_read=='Y'){
			return $this->sendResponse(null, ['CRM.RequestFollowUpAlreadyReaded'], false);
		}

		$crm_erequest_fup->is_read = 'Y';
		$crm_erequest_fup->save();

		return $this->sendResponse(null, ['CRM.ReadFollowUp']);
	}


}
