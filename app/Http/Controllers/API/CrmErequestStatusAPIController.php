<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CrmErequestStatusRepository;
use App\Models\CrmErequestStatus;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class CrmErequestStatusAPIController extends AppBaseController
{
	/** @var  CrmErequestStatusRepository */
	private $crmErequestStatusRepository;

	function __construct(CrmErequestStatusRepository $crmErequestStatusRepo)
	{
		parent::__construct();
		$this->crmErequestStatusRepository = $crmErequestStatusRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$model = new CrmErequestStatus();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CrmErequestStatus::select(
			$model_fields
		);

		if(isset($crm_erequest_status_id)){
			$query->where('crm_erequest_status.id',$crm_erequest_status_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('crm_erequest_status.active',"Y");
		}

		
		if( isset($user) ){
			$query->whereIn('crm_erequest_status.id',[config('crm.crm_erequest_status.new'),config('crm.crm_erequest_status.canceled')]);
		}
		if( isset($owner) ){
			$query->whereNotIn('crm_erequest_status.id',[config('crm.crm_erequest_status.new'),config('crm.crm_erequest_status.archived'),config('crm.crm_erequest_status.canceled')]);
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('crm_erequest_status.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
	}



	public function save(Request $request)
	{
		$data = $request->get('crm_erequest_status');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$crm_erequest_status = new CrmErequestStatus;
		}else{
			$crm_erequest_status = CrmErequestStatus::find($data['id']);	
		}

		
		if(!$crm_erequest_status){
			return $this->sendResponse(null, ['ErequestStatus.InvalideDay'], false);
		}

		if(isset($data['active'])) $crm_erequest_status->active = $data['active'];
		if(isset($data['lookup_code'])) $crm_erequest_status->lookup_code = $data['lookup_code'];
		if(isset($data['crm_erequest_status_name_ar'])) $crm_erequest_status->crm_erequest_status_name_ar = $data['crm_erequest_status_name_ar'];
		if(isset($data['crm_erequest_status_name_en'])) $crm_erequest_status->crm_erequest_status_name_en = $data['crm_erequest_status_name_en'];
		
		$crm_erequest_status->save();

		return $this->sendResponse($crm_erequest_status->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$crm_erequest_status_id = $request->get('crm_erequest_status_id');

		if(!$crm_erequest_status_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CrmErequestStatus::destroy($crm_erequest_status_id);
		if(!$destroy){
			return $this->sendResponse(null, ['ErequestStatus.InvalideDay'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
