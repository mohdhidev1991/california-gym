<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CrmErequestTypeRepository;
use App\Models\CrmErequestType;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Libraries\Factories\ReaUserFactory;

class CrmErequestTypeAPIController extends AppBaseController
{
	/** @var  CrmErequestTypeRepository */
	private $crmErequestTypeRepository;

	function __construct(CrmErequestTypeRepository $crmErequestTypeRepo)
	{
		parent::__construct();
		$this->crmErequestTypeRepository = $crmErequestTypeRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}


		$model = new CrmErequestType();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = CrmErequestType::select(
			$model_fields
		);

		if(isset($crm_erequest_type_id)){
			$query->where('crm_erequest_type.id',$crm_erequest_type_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('crm_erequest_type.active',"Y");
		}
		

		if( isset($school_jobs) ){
			$school_jobs = ReaUserFactory::get_my_school_jobs_ids();
			if(!empty($school_jobs)){
				$query->where(function ($query) use($school_jobs) {
					$query->where('crm_erequest_type.public',"Y");
					$query->orWhere(function ($query) use($school_jobs) {
						foreach ($school_jobs as $item_job) {
							$query->orWhere('crm_erequest_type.authorized_jobrole_mfk', 'LIKE' ,"%,".(int)$item_job.",%" );
						}
					});
	            });
			}else{
				$query->where('crm_erequest_type.public',"Y");
			}
			
		}



		if( isset($domain_ids) ){
			$domain_ids = explode(':', $domain_ids);
			if(is_array($domain_ids) AND !empty($domain_ids)){
				$query->whereIn('crm_erequest_type.domain_id', $domain_ids);
			}
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('crm_erequest_type.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
	}



	public function save(Request $request)
	{
		$data = $request->get('crm_erequest_type');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$crm_erequest_type = new CrmErequestType;
		}else{
			$crm_erequest_type = CrmErequestType::find($data['id']);	
		}

		
		if(!$crm_erequest_type){
			return $this->sendResponse(null, ['ErequestType.InvalideDay'], false);
		}

		if(isset($data['active'])) $crm_erequest_type->active = $data['active'];
		if(isset($data['lookup_code'])) $crm_erequest_type->lookup_code = $data['lookup_code'];
		if(isset($data['domain_id'])) $crm_erequest_type->domain_id = $data['domain_id'];
		if(isset($data['crm_erequest_type_name_ar'])) $crm_erequest_type->crm_erequest_type_name_ar = $data['crm_erequest_type_name_ar'];
		if(isset($data['crm_erequest_type_name_en'])) $crm_erequest_type->crm_erequest_type_name_en = $data['crm_erequest_type_name_en'];
		if(isset($data['public'])) $crm_erequest_type->public = $data['public'];
		if(isset($data['authorized_jobrole_mfk'])) $crm_erequest_type->authorized_jobrole_mfk = $data['authorized_jobrole_mfk'];
		if(isset($data['atable_id'])) $crm_erequest_type->atable_id = $data['atable_id'];
		
		$crm_erequest_type->save();

		return $this->sendResponse($crm_erequest_type->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$crm_erequest_type_id = $request->get('crm_erequest_type_id');

		if(!$crm_erequest_type_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = CrmErequestType::destroy($crm_erequest_type_id);
		if(!$destroy){
			return $this->sendResponse(null, ['ErequestType.InvalideDay'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
