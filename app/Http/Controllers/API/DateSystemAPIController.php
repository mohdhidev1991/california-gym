<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use App\Models\DateSystem;
use Illuminate\Http\Request;
use App\Libraries\Repositories\DateSystemRepository;
use Response;
use Illuminate\Support\Facades\Lang;


class DateSystemAPIController extends AppBaseController
{
	/** @var  DateSystemRepository */
	private $dateSystemRepository;

	function __construct(DateSystemRepository $dateSystemRepo)
	{
		parent::__construct();
		$this->dateSystemRepository = $dateSystemRepo;
	}



	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 06/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = DateSystem::select([
			'date_system.*'
		]);

		if(isset($date_system_id)){
			$query->where('date_system.id',$date_system_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('date_system.active',"Y");
		}
		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('date_system.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['dates_system' => $result, 'total' => $count];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($date_system){
		
	}





	public function save(Request $request)
	{
		$data = $request->get('date_system');

		if(!$data){
			return $this->sendResponse(null, Lang::get('date_system.errors.empty_date_system_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$date_system = new DateSystem;
		}else{
			$date_system = DateSystem::find($data['id']);	
		}

		
		if(!$date_system){
			return $this->sendResponse(null, Lang::get('date_system.errors.date_system_not_existe'), false);
		}

		if(isset($data['active'])) $date_system->active = $data['active'];
		if(isset($data['date_system_name_ar'])) $date_system->date_system_name_ar = $data['date_system_name_ar'];
		if(isset($data['date_system_name_en'])) $date_system->date_system_name_en = $data['date_system_name_en'];
		if(isset($data['lookup_code'])) $date_system->lookup_code = $data['lookup_code'];
		
		$date_system->save();

		return $this->sendResponse($date_system, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$date_system_id = $request->get('date_system_id');

		if(!$date_system_id){
			return $this->sendResponse(null, Lang::get('date_system.errors.empty_date_system_id'), false);
		}

		$destroy = DateSystem::destroy($date_system_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('date_system.errors.date_system_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}
}
