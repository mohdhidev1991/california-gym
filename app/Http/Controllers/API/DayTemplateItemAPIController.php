<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\DayTemplateItemRepository;
use App\Models\DayTemplateItem;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class DayTemplateItemAPIController extends AppBaseController
{
	/** @var  DayTemplateItemRepository */
	private $day_template_itemRepository;

	function __construct(DayTemplateItemRepository $day_template_itemRepo)
	{
		parent::__construct();
		$this->day_template_itemRepository = $day_template_itemRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = DayTemplateItem::select([
			'day_template_item.*'
		]);

		if(isset($day_template_item_id)){
			$query->where('day_template_item.id',$day_template_item_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('day_template_item.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('day_template_item.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['day_template_items' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($item){
	}



	public function save(Request $request)
	{
		$data = $request->get('day_template_item');

		if(!$data){
			return $this->sendResponse(null, Lang::get('day_template_item.errors.empty_day_template_item_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$day_template_item = new DayTemplateItem;
		}else{
			$day_template_item = DayTemplateItem::find($data['id']);	
		}

		
		if(!$day_template_item){
			return $this->sendResponse(null, Lang::get('day_template_item.errors.day_template_item_not_existe'), false);
		}

		if(isset($data['active'])) $day_template_item->active = $data['active'];
		if(isset($data['session_start_time'])) $day_template_item->session_start_time = $data['session_start_time'];
		if(isset($data['session_end_time'])) $day_template_item->session_end_time = $data['session_end_time'];
		if(isset($data['day_template_id'])) $day_template_item->day_template_id = $data['day_template_id'];
		if(isset($data['session_order'])) $day_template_item->session_order = $data['session_order'];
		
		$day_template_item->save();

		return $this->sendResponse($day_template_item, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$day_template_item_id = $request->get('day_template_item_id');

		if(!$day_template_item_id){
			return $this->sendResponse(null, Lang::get('day_template_item.errors.empty_day_template_item_id'), false);
		}

		$destroy = DayTemplateItem::destroy($day_template_item_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('day_template_item.errors.day_template_item_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}

