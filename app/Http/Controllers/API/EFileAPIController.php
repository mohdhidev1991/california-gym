<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\EFileRepository;
use App\Models\EFile;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class EFileAPIController extends AppBaseController
{
	/** @var  EFileRepository */
	private $efileRepository;

	function __construct(EFileRepository $efileRepo)
	{
		parent::__construct();
		$this->efileRepository = $efileRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = EFile::select([
			'efile.*'
		]);

		if(isset($efile_id)){
			$query->where('efile.id',$efile_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('efile.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('efile.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['efiles' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($item){
	}



	public function save(Request $request)
	{
		$data = $request->get('efile');

		if(!$data){
			return $this->sendResponse(null, Lang::get('efile.errors.empty_efile_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$efile = new EFile;
		}else{
			$efile = EFile::find($data['id']);	
		}

		
		if(!$efile){
			return $this->sendResponse(null, Lang::get('efile.errors.efile_not_existe'), false);
		}

		if(isset($data['active'])) $efile->active = $data['active'];
		if(isset($data['efile_name_ar'])) $efile->efile_name_ar = $data['efile_name_ar'];
		if(isset($data['efile_name_en'])) $efile->efile_name_en = $data['efile_name_en'];
		
		$efile->save();

		return $this->sendResponse($efile, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$efile_id = $request->get('efile_id');

		if(!$efile_id){
			return $this->sendResponse(null, Lang::get('efile.errors.empty_efile_id'), false);
		}

		$destroy = EFile::destroy($efile_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('efile.errors.efile_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
