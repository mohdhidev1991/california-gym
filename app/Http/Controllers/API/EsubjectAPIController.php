<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\EsubjectRepository;
use App\Models\Esubject;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class EsubjectAPIController extends AppBaseController
{
	/** @var  EsubjectRepository */
	private $esubjectRepository;

	function __construct(EsubjectRepository $esubjectRepo)
	{
		parent::__construct();
		$this->esubjectRepository = $esubjectRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new Esubject;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = Esubject::select(
			$model_fields
		);

		if(isset($exclude_id)){
			$query->where($model->table.'.id','<>', (int)$exclude_id);
		}

		if(isset($esubject_id)){
			$query->where($model->table.'.id',$esubject_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','book_name']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);

		$result = $query->get();

		$total = $query->count();
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		$item->level_classes = \App\Helpers\MfkHelper::mfkIdsDecode($item->level_class_mfk);
	}



	public function save(Request $request)
	{
		$data = $request->get('esubject');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$esubject = new Esubject;
		}else{
			$esubject = Esubject::find($data['id']);
		}

		if(!$esubject){
			return $this->sendResponse(null, ['Esubject.InvalideEsubject'], false);
		}

		
		$model = new Esubject;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field]) OR empty($data[$field])) $esubject->$field = $data[$field];
		}
		$esubject->save();
		return $this->sendResponse($esubject->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$esubject_id = $request->get('esubject_id');

		if(!$esubject_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Esubject::destroy($esubject_id);
		if(!$destroy){
			return $this->sendResponse(null, ['Esubject.InvalideEsubject'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}

}
