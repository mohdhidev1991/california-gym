<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\EwallRepository;
use App\Models\Ewall;
use App\Models\RserviceStudent;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class EwallAPIController extends AppBaseController
{
	/** @var  EwallRepository */
	private $ewallRepository;

	function __construct(EwallRepository $ewallRepo)
	{
		parent::__construct();
		$this->ewallRepository = $ewallRepo;
	}


	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new Ewall;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = Ewall::select(
			$model_fields
		);

		if(isset($exclude_id)){
			$query->where($model->table.'.id','<>', (int)$exclude_id);
		}

		if(isset($ewall_id)){
			$query->where($model->table.'.id',$ewall_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','book_name']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);

		$result = $query->get();

		$total = $query->count();
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
		$item->level_classes = \App\Helpers\MfkHelper::mfkIdsDecode($item->level_class_mfk);
	}



	public function save(Request $request)
	{
		$data = $request->get('ewall');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$ewall = new Ewall;
		}else{
			$ewall = Ewall::find($data['id']);
		}

		if(!$ewall){
			return $this->sendResponse(null, ['Ewall.InvalideEwall'], false);
		}

		
		$model = new Ewall;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field]) OR empty($data[$field])) $ewall->$field = $data[$field];
		}
		$ewall->save();
		return $this->sendResponse($ewall->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$ewall_id = $request->get('ewall_id');

		if(!$ewall_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Ewall::destroy($ewall_id);
		if(!$destroy){
			return $this->sendResponse(null, ['Ewall.InvalideEwall'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}




	/*
		BF: BF1408
		Method: POST + GET
		Params: []
	*/
	public function get_my_ewalls(Request $request)
	{
		$student_id = $request->get('student_id');
		$my_students = RserviceStudent::get_available_students_by_rea_user_id($this->user->id, $student_id);
		$my_walls = collect([]);
		foreach ($my_students as $student_id):
			$student_files = Student::get_student_school_fullinfos($student_id, true);
			if(!empty($student_files)){
				foreach($student_files as &$student_file){
					$prefix_db_crm = config('database.connections.mysqlcrm.database');
					$sql = "SELECT ew.id,
							$student_file->student_id as student_id,
							'$student_file->school_name_ar' as school_name_ar,
							'$student_file->school_name_en' as school_name_en,
							'$student_file->sdepartment_name_ar' as sdepartment_name_ar,
							'$student_file->sdepartment_name_en' as sdepartment_name_en,
							'$student_file->firstname' as student_firstname,
							'$student_file->lastname' as student_lastname,
							'$student_file->firstname' as student_f_firstname,
							'$student_file->level_class_name_ar' as level_class_name_ar,
							'$student_file->level_class_name_en' as level_class_name_en,
							'$student_file->school_level_name_ar' as school_level_name_ar,
							'$student_file->school_level_name_en' as school_level_name_en,
							'$student_file->school_year_name_ar' as school_year_name_ar,
							'$student_file->school_year_name_en' as school_year_name_en,
							ew.ewall_name_ar,
							ew.ewall_name_en,
							SUM(IF(fupr.is_read = 'N', 1, 0)) AS nb_new,
							COUNT(fup.id) AS nb_fup
						FROM ewall ew
						INNER JOIN esubject sub ON sub.ewall_id = ew.id
						INNER JOIN {$prefix_db_crm}.crm_erequest req ON req.id = sub.crm_erequest_id
						INNER JOIN {$prefix_db_crm}.crm_erequest_fup fup ON req.id = fup.crm_erequest_id
						INNER JOIN {$prefix_db_crm}.crm_erequest_fup_reader fupr ON fup.id = fupr.crm_erequest_fup_id
							AND fupr.reader_auser_id = :p_parent_auser_id
						WHERE ew.school_id = :p_school_id
							AND ( ew.sdepartment_id = :p_sdepartment_id OR ew.sdepartment_id = 0 )
							AND( ew.level_class_id = :p_level_class_id OR ew.level_class_id = 0 )
							AND ( ew.school_class_id = :p_school_class_id OR ew.school_class_id = 0 )
							AND ( ew.student_id = :p_student_id OR ew.student_id = 0 )
						GROUP BY ew.id,
							ew.ewall_name_ar,
							ew.ewall_name_en
						ORDER BY nb_new DESC
					";

					$student_file_walls = \DB::select( \DB::raw($sql),[
							"p_parent_auser_id" => $this->user->id,
							"p_school_id" => $student_file->school_id,
							"p_sdepartment_id" => $student_file->sdepartment_id,
							"p_level_class_id" => $student_file->level_class_id,
							"p_school_class_id" => $student_file->school_class_id,
							"p_student_id" => $student_file->student_id,
						]
					);
					foreach($student_file_walls as &$student_file_walls_item){
		                $student_file_walls_item->id = (int)$student_file_walls_item->id;
		                $student_file_walls_item->student_id = (int)$student_file_walls_item->student_id;
		                $student_file_walls_item->nb_new = (int)$student_file_walls_item->nb_new;
		                $student_file_walls_item->nb_fup = (int)$student_file_walls_item->nb_fup;
		            }
					$my_walls = $my_walls->merge($student_file_walls);
					
				}
			}
		endforeach;
		if(!$my_walls){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		$total = count($my_walls);
		return $this->sendResponse($my_walls, ['Global.GetDataWithSuccess'], true,$total);
	}




	/*
		BF: BF1408
		Method: GET
		Params: []
	*/
	public function get_ewall_esubjects(Request $request)
	{
		$ewall_id = $request->get('ewall_id');
		if(!$ewall_id){
			return $this->sendResponse(null, ['Ewall.EmptyWallID'], false);
		}
		$prefix_db_crm = config('database.connections.mysqlcrm.database');
		$sql = "SELECT sub.ewall_id AS ewall_id,
				sub.id AS esubject_id,
				req.crm_erequest_hdate AS subject_hdate,
				req.crm_erequest_time AS subject_time,
				req.crm_erequest_name AS subject_name,
				SUM(IF(fupr.is_read = 'N', 1, 0)) AS nb_new,
				COUNT(fup.id) AS nb_fup
			FROM esubject sub
			INNER JOIN {$prefix_db_crm}.crm_erequest req ON req.id = sub.crm_erequest_id
			INNER JOIN {$prefix_db_crm}.crm_erequest_fup fup ON req.id = fup.crm_erequest_id
			INNER JOIN {$prefix_db_crm}.crm_erequest_fup_reader fupr ON fup.id = fupr.crm_erequest_fup_id
				AND fupr.reader_auser_id = :p_parent_auser_id
			WHERE sub.ewall_id = :ewall_id
			GROUP BY sub.id
			ORDER BY nb_new DESC
		";

		$subjects = \DB::select( \DB::raw($sql),[
				"p_parent_auser_id" => $this->user->id,
				"ewall_id" => $ewall_id
			]
		);

		if(!$subjects){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($subjects as &$subject){
            $subject->ewall_id = (int)$subject->ewall_id;
            $subject->esubject_id = (int)$subject->esubject_id;
            $subject->nb_new = (int)$subject->nb_new;
            $subject->nb_fup = (int)$subject->nb_fup;
        }
		$total = count($subjects);
		return $this->sendResponse($subjects, ['Global.GetDataWithSuccess'], true,$total);
	}




	/*
		BF: BF1408
		Method: GET
		Params: []
	*/
	public function get_esubject_comments(Request $request)
	{
		$esubject_id = $request->get('esubject_id');
		if(!$esubject_id){
			return $this->sendResponse(null, ['Ewall.EmptySubjectID'], false);
		}
		$prefix_db_crm = config('database.connections.mysqlcrm.database');
		echo $sql = "SELECT
				  sub.id AS esubject_id,
				  fup.fup_changes,
				   fup.fup_comments,
				   fup.fup_hdate,
				   fup.fup_time,
				  fupr.is_read,
                  usr.firstname as parent_firstname,
                  usr.lastname as parent_lastname,
                  usr.f_firstname as parent_f_firstnamen,
                  st.firstname as student_firstname,
                  st.lastname as student_lastname,
                  st.f_firstname as student_f_firstname
				FROM
				  {$prefix_db_crm}.crm_erequest_fup AS fup
				INNER JOIN
				  {$prefix_db_crm}.crm_erequest req ON req.id = fup.crm_erequest_id
				INNER JOIN
				  esubject sub ON sub.crm_erequest_id = req.id
				INNER JOIN
				  {$prefix_db_crm}.crm_erequest_fup_reader fupr ON fup.id = fupr.crm_erequest_fup_id AND fupr.reader_auser_id = :p_parent_auser_id
                  LEFT JOIN
				  rea_user usr ON usr.id = fup.owner_id
                  LEFT JOIN
				  rservice_student rs ON rs.parent_user_id = usr.id
                  LEFT JOIN
				  student st ON st.id = rs.student_id
				WHERE
				  sub.id = :esubject_id AND fup.active='Y'
				GROUP BY
				  fup.id
				ORDER BY fup.fup_hdate ASC, fup.fup_time ASC
			";
			exit();

		$comments = \DB::select( \DB::raw($sql),[
				"p_parent_auser_id" => $this->user->id,
				"esubject_id" => $esubject_id
			]
		);

		if(!$comments){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($comments as &$comment){
            $comment->esubject_id = (int)$comment->esubject_id;
        }
		$total = count($comments);
		return $this->sendResponse($comments, ['Global.GetDataWithSuccess'], true,$total);
	}





	public function submit_subject(Request $request)
	{
		$data = $request->get('crm_erequest');

		if(!$data
			OR !isset($data['crm_erequest_type_id'])
			OR !isset($data['owner_auser_id'])
			OR !isset($data['crm_erequest_name'])
			OR !isset($data['crm_erequest_level_enum'])
			OR !isset($data['crm_erequest_details'])
			OR !isset($data['crm_erequest_cat_id'])
			OR empty($data['crm_erequest_type_id'])
			OR empty($data['owner_auser_id'])
			OR empty($data['crm_erequest_name'])
			OR empty($data['crm_erequest_level_enum'])
			OR empty($data['crm_erequest_details'])
			OR empty($data['crm_erequest_cat_id'])
			){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$attributes = [];
		$attributes['no_policy_selected'] = false;
		$school_stacke_holder_id = ReaUserFactory::get_current_school_stacke_holder_id();
		$my_jobs_ids = ReaUserFactory::get_my_school_jobs_ids();
		$my_jobrole_mfk = (!empty($my_jobs_ids))? \App\Helpers\MfkHelper::mfkEncode($my_jobs_ids) : ",,";

		$erequest_sh_id = null;
		$erequest_owner_id = null;

		$stakeholder_and_owner_request = CrmErequestFactory::get_stakeholder_and_owner_request($school_stacke_holder_id, $data['crm_erequest_type_id'], $data['crm_erequest_cat_id'], $data['crm_erequest_level_enum'], $my_jobrole_mfk);
		if(!$stakeholder_and_owner_request['erequest_sh_id'] OR !$stakeholder_and_owner_request['erequest_owner_id']){
			$attributes['no_policy_selected'] = ['CRM.TheRequestSubmittedButNotTransferredToThePartyConcernedBecausePolicySpecifiedForTheDistributionOfElectronicRequestsPleaseReferToYourAdministration'];
		}
		$crm_erequest = new CrmErequest;
		$crm_erequest->active = 'Y';
		$crm_erequest->auser_id = $this->user->id;
		$crm_erequest->crm_erequest_type_id = (int)$data['crm_erequest_type_id'];
		$crm_erequest->crm_erequest_cat_id = (int)$data['crm_erequest_cat_id'];
		$crm_erequest->crm_erequest_level_enum = (int)$data['crm_erequest_level_enum'];
		$crm_erequest->crm_erequest_status_id = config('crm.crm_erequest_status.new');
		$crm_erequest->crm_erequest_fup_hdate = null;
		$crm_erequest->crm_erequest_name = $data['crm_erequest_name'];
		$crm_erequest->crm_erequest_details = $data['crm_erequest_details'];
		$crm_erequest->owner_stakeholder_id = $stakeholder_and_owner_request['erequest_sh_id'];
		$crm_erequest->owner_auser_id = $stakeholder_and_owner_request['erequest_owner_id'];
		$crm_erequest->crm_erequest_time = date('H:i');
		$h_current_day = \App\Helpers\HijriDateHelper::to_hijri(date('Ymd'));
		$crm_erequest->crm_erequest_hdate = $h_current_day;

		$crm_erequest->crm_file_mfk = null;
		if( isset($data['files']) AND !empty($data['files']) ){
			foreach ($data['files'] as $item_file) {
				$file_ = File::find($item_file);
				$file_->active = 'Y';
				$file_->save();
			}
			$crm_erequest->crm_file_mfk = \App\Helpers\MfkHelper::mfkEncode($data['files']);
		}
		$crm_erequest->save();

		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->crm_erequest_id = $crm_erequest->id;
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = 0;
		$crm_erequest_fup->before_erequest_status_id = $crm_erequest->crm_erequest_status_id;
		$crm_erequest_fup->after_erequest_status_id = $crm_erequest->crm_erequest_status_id;
		if(!$stakeholder_and_owner_request['erequest_sh_id'] OR !$stakeholder_and_owner_request['erequest_owner_id']){
			$crm_erequest_fup->is_read = 'N';
			$crm_erequest_fup->fup_changes = translate('CRM','TheRequestSubmittedButNotTransferredToThePartyConcernedBecausePolicySpecifiedForTheDistributionOfElectronicRequestsPleaseReferToYourAdministration');
			$crm_erequest_fup->fup_comments = null;
		}else{
			$crm_erequest_fup->is_read = 'Y';
			$crm_erequest_fup->fup_changes = translate('CRM','TheRequestWasCreatedWithSuccessAndTransferredToThePartyConcernedAndAnEmployeeAssigneToAnswerWithTheDistributionPolicyNumber', ['policy_number' => $stakeholder_and_owner_request['crm_erequest_policy_id']]);
			$crm_erequest_fup->fup_comments = null;
		}
		$crm_erequest_fup->save();

		return $this->sendResponse($crm_erequest->id, ['Form.DataSavedWithSuccess']);
	}




	public function submit_follow_up(Request $request)
	{
		$data = $request->get('follow_up');

		if(!$data OR !isset($data['erequest_id']) ){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$crm_erequest = CrmErequest::select(['crm_erequest.id','crm_erequest.auser_id','crm_erequest.crm_erequest_status_id','crm_erequest.owner_auser_id'])
		->where('crm_erequest.id',(int)$data['erequest_id'])
		->where('crm_erequest.auser_id',(int)$this->user->id)
		->first();

		if(!$crm_erequest){
			return $this->sendResponse(null, ['CRM.InvalidRequest'], false);
		}
		$crm_erequest = $crm_erequest->toArray();

		if(!in_array($crm_erequest['crm_erequest_status_id'],[config('crm.crm_erequest_status.new'), config('crm.crm_erequest_status.ongoing'), config('crm.crm_erequest_status.need_verification')])){
			return $this->sendResponse(null, ['CRM.YouCantAddNewFollowUpThisRequestAlreadyClosedOrArchived'], false);
		}

		$crm_erequest_fup = new CrmErequestFup;
		$crm_erequest_fup->active = 'Y';
		$crm_erequest_fup->is_read = 'N';
		$crm_erequest_fup->crm_erequest_id = $data['erequest_id'];
		$crm_erequest_fup->fup_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		$crm_erequest_fup->fup_time = date('H:i');
		$crm_erequest_fup->fup_comments = $data['fup_comments'];
		$crm_erequest_fup->fup_changes = null;
		$crm_erequest_fup->before_erequest_path_id = 1;
		$crm_erequest_fup->crm_erequest_path_id = 1;
		$crm_erequest_fup->owner_id = $crm_erequest['auser_id'];
		$crm_erequest_fup->before_erequest_status_id = $crm_erequest['crm_erequest_status_id'];
		$crm_erequest_fup->after_erequest_status_id = $data['status_id'];
		$crm_erequest_fup->save();

		if($data['status_id']!=$crm_erequest['crm_erequest_status_id']){
			$_crm_erequest = CrmErequest::find($data['erequest_id']);
			$_crm_erequest->crm_erequest_status_id = $data['status_id'];
			$_crm_erequest->save();
			$status_from = CrmErequestStatus::find($crm_erequest['crm_erequest_status_id']);
			$status_to = CrmErequestStatus::find($data['status_id']);
			$crm_erequest_fup->fup_changes = translate('CRM','RequestStatusChangedFromTo', ['status_from'=>$status_from->crm_erequest_status_name_ar, 'status_to'=>$status_to->crm_erequest_status_name_ar]);
			$crm_erequest_fup->save();
		}
		

		return $this->sendResponse(null, ['CRM.FollowUpAddedWithSuccess']);
	}



	public function is_read(Request $request)
	{
		$crm_erequest_fup_id = $request->get('crm_erequest_fup_id');

		if(!$crm_erequest_fup_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$crm_erequest_fup = CrmErequestFup::find($crm_erequest_fup_id);
		if(!$crm_erequest_fup OR $crm_erequest_fup->active!='Y'){
			return $this->sendResponse(null, ['CRM.InvalideRequestFollowUp'], false);
		}
		if($crm_erequest_fup->owner_id==$this->user->id){
			return $this->sendResponse(null, ['CRM.YouAreAlreadyTheOwnerForThisFollowUp'], false);
		}
		if($crm_erequest_fup->is_read=='Y'){
			return $this->sendResponse(null, ['CRM.RequestFollowUpAlreadyReaded'], false);
		}

		$crm_erequest_fup->is_read = 'Y';
		$crm_erequest_fup->save();

		return $this->sendResponse(null, ['CRM.ReadFollowUp']);
	}

}


