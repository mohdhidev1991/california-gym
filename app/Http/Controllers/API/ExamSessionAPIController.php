<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ExamSessionRepository;
use App\Models\ExamSession;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class ExamSessionAPIController extends AppBaseController
{
	/** @var  ExamSessionRepository */
	private $exam_sessionRepository;

	function __construct(ExamSessionRepository $exam_sessionRepo)
	{
		parent::__construct();
		$this->exam_sessionRepository = $exam_sessionRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = ExamSession::select([
			'exam_session.*'
		]);

		if(isset($exam_session_id)){
			$query->where('exam_session.id',$exam_session_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('exam_session.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('exam_session.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['exam_sessions' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($item){
	}



	public function save(Request $request)
	{
		$data = $request->get('exam_session');

		if(!$data){
			return $this->sendResponse(null, Lang::get('exam_session.errors.empty_exam_session_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$exam_session = new ExamSession;
		}else{
			$exam_session = ExamSession::find($data['id']);	
		}

		
		if(!$exam_session){
			return $this->sendResponse(null, Lang::get('exam_session.errors.exam_session_not_existe'), false);
		}

		if(isset($data['active'])) $exam_session->active = $data['active'];
		if(isset($data['exam_session_name_ar'])) $exam_session->exam_session_name_ar = $data['exam_session_name_ar'];
		if(isset($data['exam_session_name_en'])) $exam_session->exam_session_name_en = $data['exam_session_name_en'];
		
		$exam_session->save();

		return $this->sendResponse($exam_session, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$exam_session_id = $request->get('exam_session_id');

		if(!$exam_session_id){
			return $this->sendResponse(null, Lang::get('exam_session.errors.empty_exam_session_id'), false);
		}

		$destroy = ExamSession::destroy($exam_session_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('exam_session.errors.exam_session_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
