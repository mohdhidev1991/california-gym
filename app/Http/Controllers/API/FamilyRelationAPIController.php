<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FamilyRelationRepository;
use App\Models\FamilyRelation;
use App\Models\Student;
use App\Models\ReaUser;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use App\Helpers\validateIdn;

class FamilyRelationAPIController extends AppBaseController
{
	/** @var  FamilyRelationRepository */
	private $schoolJobRepository;

	function __construct(FamilyRelationRepository $schoolJobRepo)
	{
        parent::__construct();
		$this->schoolJobRepository = $schoolJobRepo;
	}



    public function get_my_relations($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = FamilyRelation::select([
            'family_relation.id',
            'family_relation.relship_id',
            'family_relation.resp_rea_user_id',
            'family_relation.resp_relationship',
            
            'student.firstname as student_firstname',
            'student.lastname as student_lastname',
            'student.f_firstname as student_f_firstname',
            'student.idn',
            
            'relship.relship_name_ar',
            'relship.relship_name_en',
            
            'rea_user.firstname as resp_rea_user_firstname',
            'rea_user.f_firstname as resp_rea_user_f_firstname',
            'rea_user.lastname as resp_rea_user_lastname',
            ]
        );

        $query->join('student','student.id','=','family_relation.student_id');
        $query->join('relship','relship.id','=','family_relation.relship_id');
        $query->join('rea_user','rea_user.id','=','family_relation.resp_rea_user_id');



        $query->where('family_relation.resp_rea_user_id',$this->user->id);

        if( isset($family_relation_id) ){
            $query->where('family_relation.id',$family_relation_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
            $result = $query->get();
        }

        if(!isset($active) OR ($active!='all') ){
            $query->where('family_relation.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('family_relation.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }





    public function get_my_relation($family_relation_id, $params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = FamilyRelation::select([
            'family_relation.id',
            'family_relation.relship_id',
            'family_relation.resp_rea_user_id',
            'family_relation.resp_relationship',
            
            'student.firstname as student_firstname',
            'student.lastname as student_lastname',
            'student.f_firstname as student_f_firstname',
            'student.idn',
            
            'relship.relship_name_ar',
            'relship.relship_name_en',
            
            'rea_user.firstname as resp_rea_user_firstname',
            'rea_user.f_firstname as resp_rea_user_f_firstname',
            'rea_user.lastname as resp_rea_user_lastname',
            ]
        );

        $query->join('student','student.id','=','family_relation.student_id');
        $query->join('relship','relship.id','=','family_relation.relship_id');
        $query->join('rea_user','rea_user.id','=','family_relation.resp_rea_user_id');

        $query->where('family_relation.resp_rea_user_id',$this->user->id);
        $query->where('family_relation.id',$family_relation_id);

        $single_item = $query->first();
        if(!$single_item){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }
        return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
    }



    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = FamilyRelation::select([
            'family_relation.*'
        ]);

        if(isset($family_relation_id)){
            $query->where('family_relation.id',$family_relation_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('family_relation.active',"Y");
        }

        if(isset($resp_rea_user_id)){
            $query->where('family_relation.resp_rea_user_id',(int)$resp_rea_user_id);
        }
        if(isset($student_id)){
            $query->where('family_relation.student_id',(int)$student_id);
        }

        if( isset($join) AND !empty($join) ){
            $join = explode('!', $join);
        }else{
            $join = [];
        }
    
        if(in_array('student', $join) OR in_array('all', $join)){
            $query->join('student','student.id','=','family_relation.student_id');
            $query->leftJoin('level_class','level_class.id','=','student.current_level_class_id');
            $query->addSelect([
                'student.firstname as student_firstname',
                'student.lastname as student_lastname',
                'student.f_firstname as student_f_firstname',
                'student.idn as student_idn',
                'student.idn_type_id as student_idn_type_id',
                'student.country_id as student_country_id',
                'student.birth_date as student_birth_date',
                'student.genre_id as student_genre_id',
                'student.current_level_class_id as current_level_class_id',
                'level_class.school_level_id as current_school_level_id',
                //'student.current_school_level_id as current_level_class_id',
            ]);
        }
        if(in_array('responsable', $join) OR in_array('all', $join)){
            $query->join('rea_user','rea_user.id','=','family_relation.resp_rea_user_id');
            $query->addSelect([
                'rea_user.firstname as resp_rea_user_firstname',
                'rea_user.lastname as resp_rea_user_lastname',
                'rea_user.f_firstname as resp_rea_user_f_firstname',
                'rea_user.idn as resp_rea_user_idn',
                'rea_user.mobile as resp_rea_user_mobile',
                'rea_user.email as resp_rea_user_email',
            ]);
        }
        if(in_array('student_file', $join) OR in_array('all', $join)){
            $query->leftJoin('student_file','student_file.student_id','=','family_relation.student_id');
            $query->addSelect([
                'student_file.student_place as student_place',
                'student_file.student_num as student_num',
                'student_file.school_class_id as school_class_id',
                'student_file.symbol as symbol',
                'student_file.student_file_status_id as student_file_status_id',
                'student_file.paid_amount as paid_amount',
                'student_file.paid_hdate as paid_hdate',
                'student_file.paid_comment as paid_comment',
                'student_file.id as student_file_id',
            ]);
        }

        


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('family_relation.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }

        foreach($result as $item){
            $this->filter_item_after_get($item);
        }

        $total = $query->count();

        if(isset($resp_rea_user_id) AND isset($student_id)){
            if(empty($result)) $result = [0=>null];
            $result = $result[0];
        }

        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }


    function filter_item_after_get(&$item){
        if( isset($item->student_idn_type_id) ) $item->student_idn_type_id = (int)$item->student_idn_type_id;
        if( isset($item->student_country_id) ) $item->student_country_id = (int)$item->student_country_id;
        if( isset($item->student_genre_id) ) $item->student_genre_id = (int)$item->student_genre_id;
        if( isset($item->relship_id) ) $item->relship_id = (int)$item->relship_id;
        if( isset($item->current_level_class_id) ) $item->current_level_class_id = (int)$item->current_level_class_id;
        if( isset($item->current_school_level_id) ) $item->current_school_level_id = (int)$item->current_school_level_id;
    }


    public function save(Request $request)
    {
        $data = $request->get('family_relation');

        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $family_relation = new FamilyRelation;
        }else{
            $family_relation = FamilyRelation::find($data['id']);
        }


        if(!$family_relation){
            return $this->sendResponse(null, ['FamilyRelation.FamilyRelationNotExists'], false);
        }

        if(isset($data['active'])) $family_relation->active = $data['active'];
        if(isset($data['student_id'])) $family_relation->student_id = $data['student_id'];
        if(isset($data['relship_id'])) $family_relation->relship_id = $data['relship_id'];
        if(isset($data['resp_rea_user_id'])) $family_relation->resp_rea_user_id = $data['resp_rea_user_id'];
        if(isset($data['resp_relationship'])) $family_relation->resp_relationship = $data['resp_relationship'];
        $family_relation->save();

        return $this->sendResponse($family_relation->id, ['Form.DataSavedWithSuccess']);
    }


    public function delete(Request $request)
    {
        $family_relation_id = $request->get('family_relation_id');

        if(!$family_relation_id){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $destroy = FamilyRelation::destroy($family_relation_id);
        if(!$destroy){
            return $this->sendResponse(null, ['FamilyRelation.FamilyRelationNotExists'], false);
        }

        return $this->sendResponse(null, ['Form.DataDeletedWithSuccess']);
    }


    public function delete_my_relation(Request $request)
    {
        $family_relation_id = $request->get('family_relation_id');

        if(!$family_relation_id){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $destroy = FamilyRelation::where('id', '=', $family_relation_id)
        ->where('resp_rea_user_id', '=', $this->user->id)
        ->delete();

        if(!$destroy){
            return $this->sendResponse(null, ['FamilyRelation.FamilyRelationNotExists'], false);
        }

        return $this->sendResponse(null, ['Form.DataDeletedWithSuccess']);
    }






    public function add_relation_from_idn(Request $request)
    {
        $data = $request->get('student');


        if( !$data['relship_id'] OR !$data['resp_relationship'] OR !$data['id'] ){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $exists = FamilyRelation::where('student_id', '=', $data['id'])
        ->where('resp_rea_user_id', '=', $this->user->id)
        ->count();

        if($exists){
            return $this->sendResponse(null, ['FamilyRelation.ThisFamilyRelationshipAlreadyExists'], false);
        }

        $family_relation = new FamilyRelation;

        $family_relation->active = "Y";
        $family_relation->student_id = $data['id'];
        $family_relation->relship_id = $data['relship_id'];
        $family_relation->resp_rea_user_id = $this->user->id;
        $family_relation->resp_relationship = $data['resp_relationship'];

        $family_relation->save();

        return $this->sendResponse($family_relation->id, ['Form.DataSavedWithSuccess']);
    }


    public function save_my_family_relation(Request $request)
    {
        $data = $request->get('family_relation');


        if( !$data['relship_id'] OR !$data['id'] ){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $family_relation = FamilyRelation::find($data['id']);

        if(!$family_relation){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( $family_relation->resp_rea_user_id!=$this->user->id ){
            return $this->sendResponse(null, ['FamilyRelation.YouCantChangeThisFamilyRelationship'], false);
        }

        $family_relation->relship_id = $data['relship_id'];
        $family_relation->resp_relationship = $data['resp_relationship'];
        $family_relation->save();

        return $this->sendResponse($family_relation->id, ['Form.DataSavedWithSuccess']);
    }





    public function save_new_student(Request $request)
    {
        $data = $request->get('student');

        $v = Validator::make($data, [
            'city_id' => 'required',
            'country_id' => 'required',
            'f_firstname' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'idn_type_id' => 'required',
            'relship_id' => 'required',
            'idn' => 'required',
        ]);


        if ( $v->fails() )
        {   
            return $this->sendResponse(null, $v->messages(), false);
        }

        $valide_idn = validateIdn::check($data['idn']);
        if ( !$valide_idn )
        {   
            return $this->sendResponse(null, ['Form.InvalideIDN'], false);
        }

        $student = new Student;
        $student->firstname = $data['firstname'];
        $student->firstname = "Y";
        $student->f_firstname = $data['f_firstname'];
        $student->lastname = $data['lastname'];
        $student->idn_type_id = $data['idn_type_id'];
        $student->city_id = $data['city_id'];
        $student->country_id = $data['country_id'];
        $student->idn = $data['idn'];
        $student->save();

        $family_relation = new FamilyRelation;
        $family_relation->active = "Y";
        $family_relation->student_id = $student->id;
        $family_relation->relship_id = $data['relship_id'];
        $family_relation->resp_rea_user_id = $this->user->id;
        if(isset($data['resp_relationship'])) $family_relation->resp_relationship = $data['resp_relationship'];
        $family_relation->save();

        return $this->sendResponse($family_relation->id, ['Form.DataSavedWithSuccess']);
    }





    public function save_parent(Request $request)
    {
        $data = $request->get('parent_user');

        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if (
            !isset($data['country_id'])
            OR !isset($data['idn_type_id'])
            OR !isset($data['idn'])
            OR !isset($data['firstname'])
            OR !isset($data['lastname'])
            OR !isset($data['f_firstname'])
            OR !isset($data['mobile'])
            OR !isset($data['genre_id'])
            //OR !isset($data['email'])
        ){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( !validateIdn::check($data['idn']) ){
            return $this->sendResponse(null, ['Form.InvalideIDN'], false);
        }

        if( isset($data['email']) AND $data['email']!="" AND !filter_var($data['email'], FILTER_VALIDATE_EMAIL) ){
            return $this->sendResponse(null, ['User.InvalideEmail'], false);
        }


        if( isset($data['new']) AND $data['new']==true ){
            $parent_user = new ReaUser;
        }else{
            $parent_user = ReaUser::find($data['id']);
        }


        if(!$parent_user){
            return $this->sendResponse(null, ['User.UserNotExists'], false);
        }

        $parent_user->active = 'Y';
        $parent_user->genre_id = (int)$data['genre_id'];
        //$parent_user->city_id = (int)$data['city_id'];
        $parent_user->country_id = (int)$data['country_id'];
        $parent_user->idn_type_id = (int)$data['idn_type_id'];
        $parent_user->idn = $data['idn'];
        if(isset($data['email'])) $parent_user->email = $data['email'];
        $parent_user->firstname = $data['firstname'];
        $parent_user->lastname = $data['lastname'];
        $parent_user->f_firstname = $data['f_firstname'];
        $parent_user->mobile = $data['mobile'];

        if( isset($data['new']) AND $data['new']==true ){
            $parent_user->valide_email = 'N';
            $parent_user->valide_mobile = 'N';
            $data['newpwd'] = str_random(8);
            $parent_user->pwd = bcrypt($data['newpwd']);
        }
        $parent_user->save();

        if( isset($data['new']) AND $data['new']==true ){
            \DB::table('module_rea_user')->insert(
                ['parent_user_id' => $parent_user->id, 'module_id' => config('lookup.rea_user_module_id',44)]
            );
        }

        return $this->sendResponse($parent_user->id, ['Form.DataSavedWithSuccess']);
    }




    public function save_student_for_parent(Request $request)
    {
        $student = $request->get('student');
        $family_relation = $request->get('family_relation');
        $parent_user = $request->get('parent_user');
        
        /*$v = Validator::make($student, [
            'country_id' => 'required',
            'f_firstname' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'idn_type_id' => 'required',
            'idn' => 'required',
            'birth_date' => 'required',
        ]);
        if ( $v->fails() )
        {   
            return $this->sendResponse(['success' => false, 'errors' => $v->messages()], Lang::get('rea_user.error.invalide_data'), true);
        }
        */
        if (
            !isset($student['country_id']) OR empty($student['country_id'])
            OR !isset($student['idn_type_id']) OR empty($student['idn_type_id'])
            OR !isset($student['idn']) OR empty($student['idn'])
            OR !isset($student['firstname']) OR empty($student['firstname'])
            OR !isset($student['lastname']) OR empty($student['lastname'])
            OR !isset($student['f_firstname']) OR empty($student['f_firstname'])
            //OR !isset($student['mobile']) OR empty($student['mobile'])
            OR !isset($student['genre_id']) OR empty($student['genre_id'])
            OR !isset($student['birth_date']) OR empty($student['birth_date'])
            OR !isset($student['current_level_class_id']) OR empty($student['current_level_class_id'])
            
            OR !isset($parent_user['id'])

            OR !isset($family_relation['relship_id'])
            OR !isset($family_relation['resp_relationship'])
        ){
            return $this->sendResponse(null, ['Form.ErrorData'], false);
        }

        if ( !validateIdn::check($student['idn']) )
        {   
            return $this->sendResponse(null, ['Form.InvalideIDN'], false);
        }



        if( isset($student['new']) AND $student['new']==true ){
            $student_model = new Student;
        }else{
            $student_model = Student::find($student['id']);
        }
        $student_model->active = "Y";
        $student_model->firstname = $student['firstname'];
        $student_model->f_firstname = $student['f_firstname'];
        $student_model->lastname = $student['lastname'];
        $student_model->idn_type_id = $student['idn_type_id'];
        $student_model->idn = $student['idn'];
        //$student->city_id = (int)$student['city_id'];
        $student_model->country_id = (int)$student['country_id'];
        $student_model->birth_date = $student['birth_date'];
        $student_model->current_level_class_id = $student['current_level_class_id'];
        if( ($parent_user['genre_id']==1) AND ($family_relation['relship_id']==1) ){
            $student_model->father_rea_user_id = $parent_user['id'];
        }elseif( ($parent_user['genre_id']==2) AND ($family_relation['relship_id']==1) ){
            $student_model->mother_rea_user_id = $parent_user['id'];
        }elseif(!$student_model->resp1_rea_user_id){
            $student_model->resp1_rea_user_id = $parent_user['id'];
        }
        $student_model->save();


        if( isset($student['new']) AND $student['new']==true ){
            $family_relation_model = new FamilyRelation;
        }else{
            $family_relation_model = FamilyRelation::find($family_relation['id']);
        }
        $family_relation_model->active = "Y";
        $family_relation_model->student_id = $student_model->id;
        $family_relation_model->resp_rea_user_id = $parent_user['id'];
        $family_relation_model->relship_id = $family_relation['relship_id'];
        $family_relation_model->resp_relationship = $family_relation['resp_relationship'];
        if(isset($family_relation['active_account']) AND $family_relation['active_account']=="Y") $family_relation_model->active_account = 'Y';
        else $family_relation_model->active_account = 'N';
        $family_relation_model->save();

        return $this->sendResponse([ 'family_relation_id' => $family_relation_model->id, 'student_id' => $student_model->id], ['Form.DataSavedWithSuccess']);
    }
}
