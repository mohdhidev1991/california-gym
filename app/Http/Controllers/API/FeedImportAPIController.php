<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FeedImportRepository;
use App\Models\FeedImport;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use App\Libraries\Factories\FeedImportFactory;
use Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Input;
use Validator;

class FeedImportAPIController extends AppBaseController
{
    /** @var  FeedImportRepository */
    private $feedImportRepository;

    function __construct(FeedImportRepository $feedImportRepo)
    {
        $this->feedImportRepository = $feedImportRepo;
    }

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = FeedImport::select([
            'feed_import.*'
        ]);

        if(isset($feed_import_id)){
            $query->where('feed_import.id',$feed_import_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('feed_import.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('feed_import.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }

        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }


    function filter_item_after_get(&$item){
    }



    public function save(Request $request)
    {
        $data = $request->get('feed_import');

        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $feed_import = new FeedImport;
        }else{
            $feed_import = FeedImport::find($data['id']);
        }


        if(!$feed_import){
            return $this->sendResponse(null, ['FeedImport.InvalideFeedImport'], false);
        }



        if(isset($data['active'])) $feed_import->active = $data['active'];
        if(isset($data['school_id'])) $feed_import->school_id = (int)$data['school_id'];
        if(isset($data['feed_name'])) $feed_import->feed_name = $data['feed_name'];
        if(isset($data['source_name'])) $feed_import->source_name = $data['source_name'];
        $feed_import->save();

        return $this->sendResponse($feed_import->id, ['Form.DataSavedWithSuccess'], true);
    }


    public function delete(Request $request)
    {
        $feed_import_id = $request->get('feed_import_id');

        if(!$feed_import_id){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $destroy = FeedImport::destroy($feed_import_id);
        if(!$destroy){
            return $this->sendResponse(null, ['FeedImport.InvalideFeedImport'], false);
        }

        return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
    }




    public function upload(Request $request)
    {
        // getting all of the post data
        $file = array('file' => Input::file('file'));

        $mimes_xls = array(
          'application/vnd.ms-excel',
          'application/msexcel',
          'application/x-msexcel',
          'application/x-ms-excel',
          'application/x-excel',
          'application/x-dos_ms_excel',
          'application/xls',
          'application/x-xls',
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'application/vnd.ms-office',
        );
        $extension = Input::file('file')->getClientMimeType();
        if ( !in_array($extension, $mimes_xls) ) {
            return $this->sendResponse(null, ['Form.FileIsNotValid'], false);
        }

        $rules = array('file' => 'required');//mimes:jpeg,bmp,png and for max size max:10000

        $validator = Validator::make($file, $rules);
        
        if ($validator->fails()) {
            return $this->sendResponse(null, ['Form.FileIsNotValid'], false);
        }
        else {
            // checking file is valid.
            if (Input::file('file')->isValid()) {

                $destinationPath = storage_path("feed-import");
                $extension = Input::file('file')->getClientOriginalExtension();

                $feed_import = new FeedImport;
                $feed_import->active = 'Y';
                $feed_import->school_id = 1;
                $feed_import->feed_name = Input::file('file')->getClientOriginalName();
                $feed_import->save();

                $fileName = $feed_import->id.'.'.$extension;
                $feed_import->source_name = $fileName;
                $feed_import->save();

                Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
                
                return $this->sendResponse($feed_import->id, ['Form.SuccessUploadFile']);
            }
            else {
                // sending back with error message.
                return $this->sendResponse(null, ['Form.FileIsNotValid'], false);
            }
        }
    }





    public function process(Request $request)
    {
        $feed_import_id = $request->get('feed_import_id');
        $processor = $request->get('processor');
        $item = $request->get('item');
        $options = $request->get('options');

        

        if ( !$feed_import_id OR !$processor ) {
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if ( !in_array($processor, ['students_parent','school_employees']) ) {
            return $this->sendResponse(null, ["FeedImport.InvalidProcessor"], false);
        }

        $feed_import = FeedImport::find($feed_import_id);

        if ( !$feed_import ) {
            return $this->sendResponse(null, ['FeedImport.InvalideFeedImport'], false);
        }

        /*if( 1 OR !get_cache('feed_import:'.$feed_import->id) ){
            
            set_cache('feed_import:'.$feed_import_id,$datas);
        }else{
            $datas = get_cache('feed_import:'.$feed_import_id);    
        }*/
        // disable cache
        $destinationFile = storage_path("feed-import").'/'.$feed_import->source_name;
        $datas = \Excel::load($destinationFile)->all()->toArray();

        
        if ( empty($datas) ) {
            return $this->sendResponse(null, ['FeedImport.EmptyFileData'], false);
        }        

        $total = count($datas);
        if ( !$item ) {
            return $this->sendResponse(null, ["FeedImport.InitProcessImportation"], true, $total, ['init' => true]);
        }

        if ( $item>$total ) {
            return $this->sendResponse(null, ["FeedImport.FeedImportationFinishedWithSuccess"], true, null, ['finish' => true]);
        }else{
            
            if($item==1){
                $feed_import->log = null;
                $feed_import->processor = $processor;
                $feed_import->processed = 'Y';
                $feed_import->processed_at = date('Y-m-d H:i:s');
                $feed_import->save();
            }
            $log = (array) json_decode($feed_import->log);
            $log['options'] = $options;

            if( !isset($log['total']) ) $log['total'] = $total;
            if( !isset($log['processed']) ) $log['processed'] = 0;
            if( !isset($log['inserted']) ) $log['inserted'] = 0;
            if( !isset($log['updated']) ) $log['updated'] = 0;
            if( !isset($log['failed']) ) $log['failed'] = 0;
            if( !isset($log['skipped']) ) $log['skipped'] = 0;
            if( !isset($log['messages']) ) $log['messages'] = [];
            


            $import = FeedImportFactory::process_import($processor,$datas,$item, $options);
            $log['processed']++;

            if(!$import['success']){
                $log['failed']++;
                $log['messages'][] = $import['errors'];
                $feed_import->log = json_encode($log);
                $feed_import->save();

                // return status true because is an error in importation item
                return $this->sendResponse(null, ['FeedImport.ItemImportedBadWithError'], true, null, ['item' => $item, 'message' => $import['errors'], 'success' => false ]);
            }

            switch ($import['status']) {
                case 'inserted':
                    $log['inserted']++;
                    break;
                case 'updated':
                    $log['updated']++;
                    break;
                case 'skipped':
                    $log['skipped']++;
                    break;
            }
            $feed_import->log = json_encode($log);
            $feed_import->save();


            return $this->sendResponse(null, ['FeedImport.ItemImportedWithSuccess'], true, null, ['item' => $import['item'], 'message' => $import['message'], 'success' => true, 'log' => $log, 'import' => $import] );
        }
    }
}




