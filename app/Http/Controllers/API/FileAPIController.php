<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FileRepository;
use App\Models\File;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;


class FileAPIController extends AppBaseController
{
    /** @var  FileRepository */
    private $fileRepository;

    function __construct(FileRepository $fileRepo)
    {
        parent::__construct();
        $this->fileRepository = $fileRepo;
    }


    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $model = new CourseFor();
        $model_fields = $model->getFillable();
        foreach ($model_fields as &$item) {
            $item = $model->getTable().'.'.$item;
        }
        $query = CourseFor::select(
            $model_fields
        );

        if(isset($file_id)){
            $query->where($model->getTable().'.id',$file_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where($model->getTable().'.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }

        $query->orderBy($model->getTable().'.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }

        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }

    function filter_item_after_get(&$item){

    }


    public function save(Request $request)
    {
        $data = $request->get('file');

        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $file = new File;
        }else{
            $file = File::find($data['id']);
        }


        if(!$file){
            return $this->sendResponse(null, ['File.InvalidFile'], false);
        }

        if(isset($data['active'])) $file->active = $data['active'];
        if(isset($data['lookup_code'])) $file->lookup_code = $data['lookup_code'];
        if(isset($data['file_name_ar'])) $file->file_name_ar = $data['file_name_ar'];
        if(isset($data['file_name_en'])) $file->file_name_en = $data['file_name_en'];
        $file->save();

        return $this->sendResponse($file->id, ['Form.DataSavedWithSuccess'], true);
    }



    public function delete(Request $request)
    {
        $file_id = $request->get('file_id');

        if(!$file_id){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $destroy = File::destroy($file_id);
        if(!$destroy){
            return $this->sendResponse(null, ['File.InvalideFile'], false);
        }

        return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
    }



    public function upload(Request $request)
    {
        $this->user = Auth::user();
        $file = array('file' => Input::file('file'));
        $is_active = $request->get('active');
        $doc_type = $request->get('doc_type');
        switch ($doc_type) {
            case 'logo':
                $doc_type_id = config('lookup.doc_type.logo');
                break;

            default:
                $doc_type_id = config('lookup.doc_type.unknown');
                break;
        }

        $mimes_ = array(
          'image/jpeg',
          'image/png',
          'image/bmp',
          'application/msword',
          'application/vnd.ms-excel',
          'application/msexcel',
          'application/x-msexcel',
          'application/x-ms-excel',
          'application/x-excel',
          'application/x-dos_ms_excel',
          'application/xls',
          'application/x-xls',
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'application/vnd.ms-office',
        );
        $mime_type = Input::file('file')->getClientMimeType();
        $size = Input::file('file')->getSize();

        if ( !$size || !in_array($mime_type, $mimes_) ) {
            return $this->sendResponse(null, ['File.FileIsNotValid'], false);
        }
        if($size>config('app.max_upload_size')){
            return $this->sendResponse(null, ['File.FileIsToBig'], false);
        }
        $rules = array('file' => 'required');
        $validator = Validator::make($file, $rules);
        
        if ($validator->fails()) {
            return $this->sendResponse(null, ['File.FileIsNotValid'], false);
        } else {
            if (Input::file('file')->isValid()) {
                $destinationPath = storage_path("public");
                
                $extension = Input::file('file')->getClientOriginalExtension();

                $file = new File;
                $file->save();
                $file->active = 'Y';
                $file->file_name = $file->id.'.'.$extension;
                $file->original_name = Input::file('file')->getClientOriginalName();
                $file->file_ext = $extension;
                $file->file_type = $mime_type;
                $file->doc_type_id = $doc_type_id;
                $file->owner_id = $this->user->id;

                $fileName = $file->id.'.'.$extension;

                $mimes_images = array(
                    'image/jpeg',
                    'image/png',
                    'image/bmp',
                );
                if( in_array($mime_type, $mimes_images) ){
                    $file->picture = 'Y';
                }else{
                    $file->picture = 'N';
                }
                
                $file->file_size = $size;
                $file->save();
                Input::file('file')->move($destinationPath, $fileName);
                return $this->sendResponse($file->id, ['File.SuccessUploadFile']);
            }
            else {
                return $this->sendResponse(null, ['File.FileIsNotValid'], false);
            }
        }
    }
}
