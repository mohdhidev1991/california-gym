<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\GRemarkRepository;
use App\Models\GRemark;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
class GRemarkAPIController extends AppBaseController
{
	/** @var  GRemarkRepository */
	private $gRemarkRepository;

	function __construct(GRemarkRepository $gRemarkRepo)
	{
		$this->gRemarkRepository = $gRemarkRepo;
	}


	/**
	 *	Created by Aziz
	 *	Created at 02/05/2016
	 */
	public function get($params = null)
	{

		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = GRemark::select([
			'gremark.*'
		]);

		if(isset($gremark_id)){
			$query->where('gremark.id',$gremark_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}


		if(!isset($active) OR ($active!='all') ){
			$query->where('gremark.active',"Y");
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}



		$query->orderBy('gremark.id', 'ASC');
		$result = $query->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}
		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['gremarks' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get(&$item){
	}




	public function save(Request $request)
	{
		$data = $request->get('gremark');

		if(!$data){
			return $this->sendResponse(null, Lang::get('gremark.errors.empty_gremark_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$gremark = new GRemark;
		}else{
			$gremark = GRemark::find($data['id']);
		}


		if(!$gremark){
			return $this->sendResponse(null, Lang::get('gremark.errors.gremark_not_existe'), false);
		}

		if(isset($data['active'])) $gremark->active = $data['active'];
		if(isset($data['lookup_code'])) $gremark->lookup_code = $data['lookup_code'];
		if(isset($data['gremark_name_ar'])) $gremark->gremark_name_ar = $data['gremark_name_ar'];
		if(isset($data['gremark_name_en'])) $gremark->gremark_name_en = $data['gremark_name_en'];
		$gremark->save();

		return $this->sendResponse($gremark, Lang::get('global.success.save'), true);
	}


	public function delete(Request $request)
	{
		$gremark_id = $request->get('gremark_id');

		if(!$gremark_id){
			return $this->sendResponse(null, Lang::get('school.errors.empty_gremark_id'), false);
		}

		$destroy = GRemark::destroy($gremark_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('school.errors.gremark_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}
}
