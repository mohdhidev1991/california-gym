<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\GenreRepository;
use App\Models\Genre;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class GenreAPIController extends AppBaseController
{
	/** @var  GenreRepository */
	private $genreRepository;

	function __construct(GenreRepository $genreRepo)
	{
		parent::__construct();
		$this->genreRepository = $genreRepo;
	}




	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Genre::select([
			'genre.*'
		]);

		if(isset($genre_id)){
			$query->where('genre.id',$genre_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}


		if(!isset($active) OR ($active!='all') ){
			$query->where('genre.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('genre.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
	}



	public function save(Request $request)
    {
        $data = $request->get('genre');

        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $genre = new Genre;
        }else{
            $genre = Genre::find($data['id']);
        }


        if(!$genre){
            return $this->sendResponse(null, ['Genre.InvalideGenre'], false);
        }

        if(isset($data['active'])) $genre->active = $data['active'];
        if(isset($data['lookup_code'])) $genre->lookup_code = $data['lookup_code'];
        if(isset($data['genre_name_ar'])) $genre->genre_name_ar = $data['genre_name_ar'];
        if(isset($data['genre_name_en'])) $genre->genre_name_en = $data['genre_name_en'];
        $genre->save();

        return $this->sendResponse($genre->id, ['Form.DataSavedWithSuccess'], true);
    }


    public function delete(Request $request)
    {
        $genre_id = $request->get('genre_id');

        if(!$genre_id){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $destroy = Genre::destroy($genre_id);
        if(!$destroy){
            return $this->sendResponse(null, ['Genre.InvalideGenre'], false);
        }

        return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
    }
	
}
