<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\HolidayRepository;
use App\Models\Holiday;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Carbon\Carbon;

class HolidayAPIController extends AppBaseController
{
	/** @var  HolidayRepository */
	private $holidayRepository;

	function __construct(HolidayRepository $holidayRepo)
	{
		parent::__construct();
		$this->holidayRepository = $holidayRepo;
	}




	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new Holiday();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}

		$query = Holiday::select(
			$model_fields
		);
		$query->where($model->getTable().'.active','<>',"D");

		if(isset($holiday_id)){
			$query->where($model->getTable().'.id',$holiday_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		
	}

	public function save(Request $request)
	{
		$data = $request->get('holiday');

		if(!$data OR !isset($data['date'])){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$holiday = new Holiday;
		}else{
			$holiday = Holiday::find($data['id']);
		}

		if(!$holiday){
			return $this->sendResponse(null, ['Holiday.InvalidHoliday'], false);
		}

		$holiday_date = Carbon::parse($data['date']);
		if(Holiday::where('date',$holiday_date->format('Y-m-d'))->where('active','Y')->count()){
	      return $this->sendResponse(null, ['Holiday.DateAlreadyReserved'], false);
	    }

		$model = new Holiday;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $holiday->{$item} = $data[$item];
			if( empty($data[$item]) ) $holiday->{$item} = null;
		}
		$holiday->save();

		return $this->sendResponse($holiday->id, ['Form.DataSavedWithSuccess'], true);
	}


	public function delete(Request $request)
	{
		$holiday_id = $request->get('holiday_id');

		if(!$holiday_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$holiday = Holiday::find($holiday_id);
		if(!$holiday){
			return $this->sendResponse(null, ['Holiday.InvalideHoliday'], false);
		}
		$holiday->active = 'D';
		$holiday->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
