<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\HrmLeaveTypeRepository;
use App\Models\HrmLeaveType;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Libraries\Factories\ReaUserFactory;

class HrmLeaveTypeAPIController extends AppBaseController
{
	/** @var  HrmLeaveTypeRepository */
	private $hrmLeaveTypeRepository;

	function __construct(HrmLeaveTypeRepository $hrmLeaveTypeRepo)
	{
		parent::__construct();
		$this->hrmLeaveTypeRepository = $hrmLeaveTypeRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}


		$model = new HrmLeaveType();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = HrmLeaveType::select(
			$model_fields
		);

		if(isset($hrm_leave_type_id)){
			$query->where($model->table.'.id',$hrm_leave_type_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('hrm_leave_type.active',"Y");
		}
		

		if( isset($school_jobs) ){
			$school_jobs = ReaUserFactory::get_my_school_jobs_ids();
			foreach ($school_jobs as $item_job) {
				$query->where('hrm_leave_type.public',"Y");
				$query->orWhere(function ($query) use($item_job) {
					$query->orWhere('hrm_leave_type.authorized_jobrole_mfk', 'LIKE' ,"%,".(int)$item_job.",%" );
	            });
			}
		}

		if( isset($crm_erequest_type_id) ){
			$query->where($model->table.'.crm_erequest_type_id', (int)$crm_erequest_type_id);
		}
		if( isset($crm_erequest_cat_id) ){
			$query->where($model->table.'.crm_erequest_cat_id', (int)$crm_erequest_cat_id);
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('hrm_leave_type.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
	}



	public function save(Request $request)
	{
		$data = $request->get('hrm_leave_type');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$hrm_leave_type = new HrmLeaveType;
		}else{
			$hrm_leave_type = HrmLeaveType::find($data['id']);	
		}

		
		if(!$hrm_leave_type){
			return $this->sendResponse(null, ['ErequestType.InvalideDay'], false);
		}

		if(isset($data['active'])) $hrm_leave_type->active = $data['active'];
		if(isset($data['lookup_code'])) $hrm_leave_type->lookup_code = $data['lookup_code'];
		if(isset($data['domain_id'])) $hrm_leave_type->domain_id = $data['domain_id'];
		if(isset($data['hrm_leave_type_name_ar'])) $hrm_leave_type->hrm_leave_type_name_ar = $data['hrm_leave_type_name_ar'];
		if(isset($data['hrm_leave_type_name_en'])) $hrm_leave_type->hrm_leave_type_name_en = $data['hrm_leave_type_name_en'];
		if(isset($data['public'])) $hrm_leave_type->public = $data['public'];
		if(isset($data['authorized_jobrole_mfk'])) $hrm_leave_type->authorized_jobrole_mfk = $data['authorized_jobrole_mfk'];
		if(isset($data['atable_id'])) $hrm_leave_type->atable_id = $data['atable_id'];
		
		$hrm_leave_type->save();

		return $this->sendResponse($hrm_leave_type->id, ['Form.DataSavedWithSuccess'], true);
	}

    public function delete(Request $request)
	{
		$hrm_leave_type_id = $request->get('hrm_leave_type_id');

		if(!$hrm_leave_type_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = HrmLeaveType::destroy($hrm_leave_type_id);
		if(!$destroy){
			return $this->sendResponse(null, ['ErequestType.InvalideDay'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
