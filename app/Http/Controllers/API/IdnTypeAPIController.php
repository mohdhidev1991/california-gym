<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\IdnTypeRepository;
use App\Models\IdnType;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Routing\Route;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;


class IdnTypeAPIController extends AppBaseController
{
	/** @var  IdnTypeRepository */
	private $idnTypeRepository;
	private $api_guest;

	function __construct(IdnTypeRepository $idnTypeRepo, Router $router)
	{
		parent::__construct();

		$this->api_guest = false;
		if( $router->is('api.guest.*') ){
			$this->api_guest = true;
		}
		$this->idnTypeRepository = $idnTypeRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 06/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = IdnType::select([
			'idn_type.id','idn_type.lookup_code','idn_type.idn_type_name_ar','idn_type.idn_type_name_en'
		]);

		if(isset($idn_type_id)){
			$query->where('idn_type.id',$idn_type_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('idn_type.active',"Y");
		}

		if( isset($country_id) ){
			$query->where('idn_type.country_id',(int)$country_id);
		}
		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('idn_type.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		if($item->country_id!=null) $item->country_id = (int)$item->country_id;
	}





	public function save(Request $request)
	{
		$data = $request->get('idn_type');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$idn_type = new IdnType;
		}else{
			$idn_type = IdnType::find($data['id']);	
		}

		
		if(!$idn_type){
			return $this->sendResponse(null, ['IdnType.InvalideIdnType'], false);
		}

		if(isset($data['active'])) $idn_type->active = $data['active'];
		if(isset($data['lookup_code'])) $idn_type->lookup_code = $data['lookup_code'];
		if(isset($data['country_id'])) $idn_type->country_id = $data['country_id'];
		if(isset($data['idn_type_name_ar'])) $idn_type->idn_type_name_ar = $data['idn_type_name_ar'];
		if(isset($data['idn_type_name_en'])) $idn_type->idn_type_name_en = $data['idn_type_name_en'];
		if(isset($data['idn_validation_function'])) $idn_type->idn_validation_function = $data['idn_validation_function'];
		
		$idn_type->save();

		return $this->sendResponse($idn_type->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$idn_type_id = $request->get('idn_type_id');

		if(!$idn_type_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = IdnType::destroy($idn_type_id);
		if(!$destroy){
			return $this->sendResponse(null, ['IdnType.InvalideIdnType'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}


