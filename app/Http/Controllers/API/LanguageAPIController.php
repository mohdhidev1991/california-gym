<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\LanguageRepository;
use App\Models\Language;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class LanguageAPIController extends AppBaseController
{
	/** @var  LanguageRepository */
	private $languageRepository;

	function __construct(LanguageRepository $languageRepo)
	{
		parent::__construct();
		$this->languageRepository = $languageRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Language::select([
			'lang.*'
		]);

		if(isset($language_id)){
			$query->where('lang.id',$language_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.ErrorGetData'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.SuccessGetData'], true);
		}


		if(!isset($active) OR ($active!='all') ){
			$query->where('lang.active',"Y");
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}



		$query->orderBy('lang.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyData'], false);
		}
		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
		return $this->sendResponse($result, ['Global.SuccessGetData'], true, $total);
	}


	function filter_item_after_get(&$item){
	}




	public function save(Request $request)
	{
		$data = $request->get('language');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$language = new Language;
		}else{
			$language = Language::find($data['id']);	
		}

		
		if(!$language){
			return $this->sendResponse(null, ['Language.LanguageNotExists'], false);
		}

		if(isset($data['active'])) $language->active = $data['active'];
		if(isset($data['lookup_code'])) $language->lookup_code = $data['lookup_code'];
		if(isset($data['lang_name_ar'])) $language->lang_name_ar = $data['lang_name_ar'];
		if(isset($data['lang_name_en'])) $language->lang_name_en = $data['lang_name_en'];
		
		$language->save();
		return $this->sendResponse(null, ['Global.DataSavedWithSuccess'], true);
	}


	public function delete(Request $request)
	{
		$language_id = $request->get('language_id');

		if(!$language_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Language::destroy($language_id);
		if(!$destroy){
			return $this->sendResponse(null, ['Language.LanguageNotExists'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}
}
