<?php namespace App\Http\Controllers\API;

use App\Helpers\Server;
use App\Http\Requests;
use App\Libraries\Repositories\LevelClassRepository;
use App\Models\LevelClass;
use App\Models\SchoolScope;
use App\Libraries\Factories\ReaUserFactory;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class LevelClassAPIController extends AppBaseController
{
	/** @var  LevelClassRepository */
	private $levelClassRepository;

	function __construct(Request $request, LevelClassRepository $levelClassRepo)
	{
		parent::__construct();
		$this->levelClassRepository = $levelClassRepo;
	}




	function prepre_prof_data(){

      $data_rea_user = get_cache('rea_user:'.$this->user->id);

      if(
        !isset($data_rea_user->current_school)
        OR !isset($data_rea_user->current_school->school_employee_id)
        OR !isset($data_rea_user->current_school->group_num)
        OR !isset($data_rea_user->current_school->school_id)
      ){
        return false;
      }



      $this->school_employee_id = (isset($data_rea_user->current_school->school_employee_id)) ? $data_rea_user->current_school->school_employee_id : null;
      $this->group_num = (isset($data_rea_user->current_school->group_num)) ? $data_rea_user->current_school->group_num : null;
      $this->school_id = (isset($data_rea_user->current_school->school_id)) ? $data_rea_user->current_school->school_id : null;
      $this->session_status = config('lookup.session_status');

      return true;
   }


	/**
	 * Q0014 - Q0015
	 * Get level Class list
	 * LevelClass/getAllBySchoolIdAndYearIdAndDomaineMfk/school_id/{school_id}/year_id/{year_id}
	 *
	 * @param  int $school_id
	 * @param  int $school_year_id
	 *
	 * @return Response
	 */
	public function getAllBySchoolIdAndYearIdAndDomaineMfk($school_id, $school_year_id)
	{
		if(!$this->prepre_prof_data()){
			return $this->sendResponse(null, ['SchoolEmployee.ErrorTeacherInformations'], false);
		}

		$select = LevelClass::select(
			DB::raw($school_year_id),
			'level_class.school_level_id',
			DB::raw('level_class.id as level_class_id'),
			DB::raw(0),
			DB::Raw('"Y"'),
			DB::Raw(0),
			DB::Raw($this->user->id),
			DB::Raw('now()')
		)
			->join('school_level', 'school_level.id', '=', 'level_class.school_level_id')
			->join('school', function ($join) use ($school_id) {
				$join->on('school.id', '=', DB::raw($school_id))
					->on('school_level_mfk', 'LIKE', DB::Raw('CONCAT("%,",school_level.id,",%")'));
			})
			->join('school_year', function ($join) use ($school_year_id) {
				$join->on('school_year.id', '=', DB::raw($school_year_id))
					->on('school_year.school_id', '=', 'school.id');
			})
			->join('school_scope', function ($join) {
				$join->on('school_year.id', '=', 'school_scope.school_year_id')
					->on('school_scope.school_level_id', '=', 'school_level.id')
					->on('school_scope.level_class_id', '=', 'level_class.id');
			}, null, null, 'left outer')
			->whereNull('school_scope.id')
			->where('school_level.active', '=', 'Y')
			->where('level_class.active', '=', 'Y');
		$bindings = $select->getBindings();
		$insertQuery = 'INSERT into school_scope (school_year_id,school_level_id,level_class_id,class_nb,active,version,created_by,created_at) '
			. $select->toSql();
		
		$query  = DB::insert($insertQuery, $bindings);

		$query = LevelClass::select('school_scope.id','school_level.school_level_name_ar', 'school_level.school_level_name_en', 'level_class.level_class_name_ar', 'level_class.level_class_name_en','school_scope.class_nb', 'school_scope.sdepartment_id')
			->join('school_level', 'school_level.id', '=', 'level_class.school_level_id')
			->join('school', function ($join) use ($school_id) {
				$join->on('school.id', '=', DB::raw($school_id))
					->on('school_level_mfk', 'LIKE', DB::Raw('CONCAT("%,",school_level.id,",%")'));
			})
			->join('school_year', function ($join) use ($school_year_id) {
				$join->on('school_year.id', '=', DB::raw($school_year_id))
					->on('school_year.school_id', '=', 'school.id');
			})
			->join('school_scope', function ($join) {
				$join->on('school_year.id', '=', 'school_scope.school_year_id')
					->on('school_scope.school_level_id', '=', 'school_level.id')
					->on('school_scope.level_class_id', '=', 'level_class.id');
			})
			->where('school_level.active', '=', 'Y')
			->where('level_class.active', '=', 'Y');

		$result = $query->get();
		

		if (!$result) {
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		$total = $query->count();
		foreach($result as &$item){
			if( $item->class_nb!=null ) $item->class_nb = (int)$item->class_nb;
			if($item->sdepartment_id!=null) $item->sdepartment_id = (int)$item->sdepartment_id;
		}
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}



	/**
	 * Creat at 01/05/2016
	 * Creat by Achraf
	 * Function key: BF264 / Q0008
	 * Get level Class by school level id
	 * LevelClass/getAllLevelClassBySchoolLevelID/{school_level_id}
	 *
	 * @param  int $school_level_id
	 *
	 * @return Response
	 */
	public function getAllLevelClassBySchoolLevelID($school_level_id)
	{
		$query = LevelClass::select('id', 'level_class_name_ar as label')
							->where('school_level_id','=',$school_level_id)
							->where('active','=','Y')
							->distinct();
		$result = $query->get();
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		$total = $result->count();
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}





	/**
	 * Creat at 03/05/2016
	 * Creat by Achraf
	 * HEAD | POST
	 * Function key: BF265 / Q save
	 * Save level class Scope
	 * LevelClass/SaveScopeSchoolLevelClass
	 *
	 * @param  object $school_levels_config
	 *
	 * @return Response
	 */
	public function SaveScopeSchoolLevelClass(Request $request)
	{
		$school_levels_config = $request->get('school_levels_config');
		if(!$school_levels_config){
			return $this->sendResponse($result, ['Form.EmptyData'], false);
		}

		foreach($school_levels_config as $item){
			if( !isset($item['id']) OR !isset($item['class_nb']) ){
				return $this->sendResponse($result, ['LevelClass.InvalideLevelClass'], false);
			}
			$SchoolScope = SchoolScope::find($item['id']);
			if( !$SchoolScope ){
				return $this->sendResponse($result, ['LevelClass.InvalideLevelClass'], false);
			}

			if(isset($item['class_nb'])) $SchoolScope->class_nb = (int)$item['class_nb'];
			
			if( $item['sdepartment_id']==null ){
				$SchoolScope->sdepartment_id = null;
			}elseif( isset($item['sdepartment_id']) AND $item['sdepartment_id']!=null ){
				$SchoolScope->sdepartment_id = (int)$item['sdepartment_id'];
			}

			$SchoolScope->save();
		}
		return $this->sendResponse(null, ['Form.DataMultiSavedWithSuccess'], true);
	}













	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = LevelClass::select([
			'level_class.*'
		]);

		if(isset($level_class_id)){
			$query->where('level_class.id',$level_class_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where('level_class.active',"Y");
		}

		if( isset($school_level_id) ){
			$query->where('level_class.school_level_id',$school_level_id);
		}

		if( isset($level_class_ids) ){
			$level_class_ids = explode('!', $level_class_ids);
			$query->whereIn('level_class.id',$level_class_ids);
		}

		if( isset($cpc_course_program_id) ){
			$level_classes_in_course_program = \App\Models\CpcCourseProgram::select(['level_class.id'])
			->where('cpc_course_program.id',(int)$cpc_course_program_id)
			->join('school_level','school_level.levels_template_id','=','cpc_course_program.levels_template_id')
			->join('level_class','level_class.school_level_id','=','school_level.id')
			->GroupBy('level_class.id')
			->get()->toArray();
			if($level_classes_in_course_program){
				$level_classes_in_course_program = array_column($level_classes_in_course_program, 'id');
				
				if($level_classes_in_course_program){
					$query->whereIn('level_class.id',$level_classes_in_course_program);
				}
			}else{
				$query->whereIn('level_class.id',[]);
			}
			
		}


		if( isset($current_school_year) ){
			$school_year_id = ReaUserFactory::get_current_school_year_id();
		}

		if( isset($school_year_id) ){
			$query->join( 'school_class','level_class.id','=','school_class.level_class_id');
			$query->where( 'school_class.school_year_id',$school_year_id);
			$query->groupBy('school_class.level_class_id');
		}

	

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('level_class.id', 'ASC');
		
		$result = $query->get();
		

		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach ($result as $item) {
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}






	function filter_item_after_get(&$item){
		if($item->school_level_id) $item->school_level_id = (int)$item->school_level_id;
		if($item->level_class_order) $item->level_class_order = (int)$item->level_class_order;
		if($item->session_duration_min) $item->session_duration_min = (int)$item->session_duration_min;
	}


	public function save(Request $request)
	{
		$data = $request->get('level_class');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$level_class = new LevelClass;
		}else{
			$level_class = LevelClass::find($data['id']);
		}

		
		if(!$level_class){
			return $this->sendResponse(null, ['LevelClass.InvalideLevelClass'], false);
		}

		if(isset($data['active'])) $level_class->active = $data['active'];
		if(isset($data['lookup_code'])) $level_class->lookup_code = $data['lookup_code'];
		if(isset($data['school_level_id'])) $level_class->school_level_id = $data['school_level_id'];
		if(isset($data['level_class_name_ar'])) $level_class->level_class_name_ar = $data['level_class_name_ar'];
		if(isset($data['level_class_name_en'])) $level_class->level_class_name_en = $data['level_class_name_en'];
		if(isset($data['level_class_order'])) $level_class->level_class_order = $data['level_class_order'];
		
		$level_class->save();

		return $this->sendResponse($level_class->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$level_class_id = $request->get('level_class_id');

		if(!$level_class_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = LevelClass::destroy($level_class_id);
		if(!$destroy){
			return $this->sendResponse(null, ['LevelClass.InvalideLevelClass'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}



}
