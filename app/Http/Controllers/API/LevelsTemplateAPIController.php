<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use App\Libraries\Repositories\LevelsTemplateRepository;
use App\Models\LevelsTemplate;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;

class LevelsTemplateAPIController extends AppBaseController
{
	/** @var  LevelsTemplateRepository */
	private $LevelsTemplateRepository;

	function __construct(LevelsTemplateRepository $LevelsTemplateRepository)
	{
		parent::__construct();
		$this->LevelsTemplateRepository = $LevelsTemplateRepository;
	}

	public function GetAll()
	{

		$result = DB::table('levels_template')
         ->select('id as id','levels_template_name_ar as label')
         ->where('active', '=', 'Y')
         ->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.no_results'), false);
		}

		return $this->sendResponse($result, Lang::get('global.errors.results_exist'), true);
	}



	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 06/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new LevelsTemplate;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->table.'.'.$item;
		}
		$query = LevelsTemplate::select(
			$model_fields
		);

		if(isset($levels_template_id)){
			$query->where($model->table.'.id',$levels_template_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->table.'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = $model->table.'.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','course_program_name_ar','course_program_name_en']) ){
			$_order_by = $model->table.'.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$levels_template){
		
	}





	public function save(Request $request)
	{
		$data = $request->get('levels_template');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$levels_template = new LevelsTemplate;
		}else{
			$levels_template = LevelsTemplate::find($data['id']);
		}

		if(!$levels_template){
			return $this->sendResponse(null, ['LevelsTemplate.InvalideLevelsTemplate'], false);
		}

		$model = new LevelsTemplate;
		$model_fields = $model->getFillable();
		foreach($model_fields as $field){
			if(isset($data[$field])) $levels_template->$field = $data[$field];
		}
		$levels_template->save();
		return $this->sendResponse($levels_template->id, ['Form.DataSavedWithSuccess'], true);
	}





	public function delete(Request $request)
	{
		$levels_template_id = $request->get('levels_template_id');

		if(!$levels_template_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = LevelsTemplate::destroy($levels_template_id);
		if(!$destroy){
			return $this->sendResponse(null, ['LevelsTemplate.InvalideLevelsTemplate'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}

}
