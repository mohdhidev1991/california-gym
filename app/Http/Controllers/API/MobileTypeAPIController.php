<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\MobileTypeRepository;
use App\Models\MobileType;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class MobileTypeAPIController extends AppBaseController
{
	/** @var  MobileTypeRepository */
	private $mobileTypeRepository;

	function __construct(MobileTypeRepository $mobileTypeRepo)
	{
        parent::__construct();
		$this->mobileTypeRepository = $mobileTypeRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = MobileType::select([
            'mobile_type.*'
        ]);

        if(isset($mobile_type_id)){
            $query->where('mobile_type.id',$mobile_type_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('mobile_type.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('mobile_type.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['mobile_types' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }


    function filter_item_after_get(&$item){
    }



    public function save(Request $request)
    {
        $data = $request->get('mobile_type');

        if(!$data){
            return $this->sendResponse(null, Lang::get('mobile_type.errors.empty_mobile_type_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $mobile_type = new MobileType;
        }else{
            $mobile_type = MobileType::find($data['id']);
        }


        if(!$mobile_type){
            return $this->sendResponse(null, Lang::get('mobile_type.errors.mobile_type_not_existe'), false);
        }

        if(isset($data['active'])) $mobile_type->active = $data['active'];
        if(isset($data['lookup_code'])) $mobile_type->lookup_code = $data['lookup_code'];
        if(isset($data['mobile_type_name_ar'])) $mobile_type->mobile_type_name_ar = $data['mobile_type_name_ar'];
        if(isset($data['mobile_type_name_en'])) $mobile_type->mobile_type_name_en = $data['mobile_type_name_en'];
        $mobile_type->save();

        return $this->sendResponse($mobile_type, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $mobile_type_id = $request->get('mobile_type_id');

        if(!$mobile_type_id){
            return $this->sendResponse(null, Lang::get('mobile_type.errors.empty_mobile_type_id'), false);
        }

        $destroy = MobileType::destroy($mobile_type_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('mobile_type.errors.mobile_type_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
