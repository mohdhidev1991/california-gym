<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ModelTermRepository;
use App\Models\ModelTerm;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class ModelTermAPIController extends AppBaseController
{
	/** @var  ModelTermRepository */
	private $model_termRepository;

	function __construct(ModelTermRepository $model_termRepo)
	{
		parent::__construct();
		$this->model_termRepository = $model_termRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = ModelTerm::select([
			'model_term.*'
		]);

		if(isset($model_term_id)){
			$query->where('model_term.id',$model_term_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('model_term.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('model_term.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['model_terms' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($item){
	}



	public function save(Request $request)
	{
		$data = $request->get('model_term');

		if(!$data){
			return $this->sendResponse(null, Lang::get('model_term.errors.empty_model_term_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$model_term = new ModelTerm;
		}else{
			$model_term = ModelTerm::find($data['id']);	
		}

		
		if(!$model_term){
			return $this->sendResponse(null, Lang::get('model_term.errors.model_term_not_existe'), false);
		}

		if(isset($data['active'])) $model_term->active = $data['active'];
		if(isset($data['end_study_hdate'])) $model_term->end_study_hdate = $data['end_study_hdate'];
		if(isset($data['end_vacancy_hdate'])) $model_term->end_vacancy_hdate = $data['end_vacancy_hdate'];
		if(isset($data['start_study_hdate'])) $model_term->start_study_hdate = $data['start_study_hdate'];
		if(isset($data['start_vacancy_hdate'])) $model_term->start_vacancy_hdate = $data['start_vacancy_hdate'];
		if(isset($data['school_id'])) $model_term->school_id = $data['school_id'];
		if(isset($data['model_term_name_ar'])) $model_term->model_term_name_ar = $data['model_term_name_ar'];
		if(isset($data['model_term_name_en'])) $model_term->model_term_name_en = $data['model_term_name_en'];
		
		$model_term->save();

		return $this->sendResponse($model_term, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$model_term_id = $request->get('model_term_id');

		if(!$model_term_id){
			return $this->sendResponse(null, Lang::get('model_term.errors.empty_model_term_id'), false);
		}

		$destroy = ModelTerm::destroy($model_term_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('model_term.errors.model_term_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
