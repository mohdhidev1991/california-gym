<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PeriodRepository;
use App\Models\Period;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class PeriodAPIController extends AppBaseController
{
	/** @var  PeriodRepository */
	private $periodRepository;

	function __construct(PeriodRepository $periodRepo)
	{
		parent::__construct();
		$this->periodRepository = $periodRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Period::select([
			'period.*'
		]);

		if(isset($period_id)){
			$query->where('period.id',$period_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('period.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('period.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['periods' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}

	function filter_item_after_get(&$item){
	}



	public function save(Request $request)
	{
		$data = $request->get('period');

		if(!$data){
			return $this->sendResponse(null, Lang::get('period.errors.empty_period_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$period = new Period;
		}else{
			$period = Period::find($data['id']);	
		}

		
		if(!$period){
			return $this->sendResponse(null, Lang::get('period.errors.period_not_existe'), false);
		}

		if(isset($data['active'])) $period->active = $data['active'];
		if(isset($data['lookup_code'])) $period->lookup_code = $data['lookup_code'];
		if(isset($data['period_name_ar'])) $period->period_name_ar = $data['period_name_ar'];
		if(isset($data['period_name_en'])) $period->period_name_en = $data['period_name_en'];
		
		$period->save();

		return $this->sendResponse($period, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$period_id = $request->get('period_id');

		if(!$period_id){
			return $this->sendResponse(null, Lang::get('period.errors.empty_period_id'), false);
		}

		$destroy = Period::destroy($period_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('period.errors.period_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
