<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PermissionRepository;
use App\Models\Permission;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class PermissionAPIController extends AppBaseController
{
	/** @var  PermissionRepository */
	private $permissionRepository;

	function __construct(PermissionRepository $permissionRepo)
	{
		parent::__construct();
		$this->permissionRepository = $permissionRepo;
	}




	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new Permission();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}

		$query = Permission::select(
			$model_fields
		);
		$query->where($model->getTable().'.active','<>',"D");

		if(isset($permission_id)){
			$query->where($model->getTable().'.id',$permission_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		$params = \Request::__get('params');
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		if( isset($join_obj) AND !empty($join_obj) ){
			$join_obj = explode('!', $join_obj);
		}else{
			$join_obj = [];
		}


		if( in_array('room',$join_obj) or in_array('all',$join_obj) ){
			$model_room = new Room();
			$item->rooms = Room::select($model_room->getFillable())->where('permission_id',$item->id)->where('active','Y')->get();
		}
	}

	public function save(Request $request)
	{
		$data = $request->get('permission');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$permission = new Permission;
		}else{
			$permission = Permission::find($data['id']);
		}

		if(!$permission){
			return $this->sendResponse(null, ['Permission.InvalidPermission'], false);
		}

		$model = new Permission;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $permission->{$item} = $data[$item];
			if( empty($data[$item]) ) $permission->{$item} = null;
		}
		$permission->save();

		return $this->sendResponse($permission->id, ['Form.DataSavedWithSuccess'], true);
	}


	public function delete(Request $request)
	{
		$permission_id = $request->get('permission_id');

		if(!$permission_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$permission = Permission::find($permission_id);
		if(!$permission){
			return $this->sendResponse(null, ['Permission.InvalidePermission'], false);
		}
		$permission->active = 'D';
		$permission->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
