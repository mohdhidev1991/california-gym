<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RatingRepository;
use App\Models\Rating;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class RatingAPIController extends AppBaseController
{
	/** @var  RatingRepository */
	private $ratingRepository;

	function __construct(RatingRepository $ratingRepo)
	{
		parent::__construct();
		$this->ratingRepository = $ratingRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Rating::select([
			'rating.*'
		]);

		if(isset($rating_id)){
			$query->where('rating.id',$rating_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('rating.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('rating.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		$count = $query->count();
		foreach ($result as $item) {
			$this->filter_item_after_get($item);
		}
		$return = ['ratings' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get(&$item){
		if($item->school_id) $item->school_id = (int)$item->school_id;
		//if($item->max_pct) $item->max_pct = number_format($item->max_pct, 2, '.','');
		//if($item->min_pct) $item->min_pct = floatval($item->min_pct);
	}


	public function save(Request $request)
	{
		$data = $request->get('rating');

		if(!$data){
			return $this->sendResponse(null, Lang::get('rating.errors.empty_rating_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$rating = new Rating;
		}else{
			$rating = Rating::find($data['id']);	
		}

		
		if(!$rating){
			return $this->sendResponse(null, Lang::get('rating.errors.rating_not_existe'), false);
		}

		if(isset($data['active'])) $rating->active = $data['active'];
		if(isset($data['school_id'])) $rating->school_id = $data['school_id'];
		if(isset($data['lookup_code'])) $rating->lookup_code = $data['lookup_code'];
		if(isset($data['max_pct'])) $rating->max_pct = $data['max_pct'];
		if(isset($data['min_pct'])) $rating->min_pct = $data['min_pct'];
		if(isset($data['rating_name_ar'])) $rating->rating_name_ar = $data['rating_name_ar'];
		if(isset($data['rating_name_en'])) $rating->rating_name_en = $data['rating_name_en'];
		
		$rating->save();

		return $this->sendResponse($rating, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$rating_id = $request->get('rating_id');

		if(!$rating_id){
			return $this->sendResponse(null, Lang::get('rating.errors.empty_rating_id'), false);
		}

		$destroy = Rating::destroy($rating_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('rating.errors.rating_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
