<?php namespace App\Http\Controllers\API;

use App\Helpers\validateIdn;
use App\Http\Requests;
use App\Libraries\Repositories\ReaUserRepository;
use App\Models\ReaUser;
use App\Models\Club;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Urlparams;
use App\Helpers\MfkHelper;
use App\Helpers\StringHelper;
use DB;
use Auth;
use App\Libraries\Factories\ReaUserFactory;

class ReaUserAPIController extends AppBaseController
{
    /** @var  ReaUserRepository */
    private $reaUserRepository;

    function __construct(ReaUserRepository $reaUserRepo)
    {
        parent::__construct();
        $this->reaUserRepository = $reaUserRepo;
    }

    
    public function me()
    {
        
        $this->user = Auth::user();
        if(!$this->user){
            return $this->sendResponse(['guest' => true],'rea_user.success.guest',true);
        }

        $datas = get_cache('rea_user:'.$this->user->id);
        if(!$datas){
            $this->reaUserRepository->authentification_data($this->user);
            $datas = get_cache('rea_user:'.$this->user->id);
        }
        return $this->sendResponse($datas,'rea_user.success.datas',true);
    }


    public function set_current_school(Request $request)
    {
        $datas = get_cache('rea_user:'.$this->user->id);
        if(!$datas){
            $this->reaUserRepository->authentification_data($this->user);
            $datas = get_cache('rea_user:'.$this->user->id);
        }
        $new_current_school_id = $request->get('school_id');
        foreach ($datas->schools as $school) {
            if($school->school_id==$new_current_school_id){
                $datas->current_school = clone $school;
                continue;
            }
        }
        set_cache('rea_user:'.$this->user->id, $datas);
        return $this->sendResponse($datas,'rea_user.success.datas',true);
    }



    public function set_current_sdepartment(Request $request)
    {
        $datas = get_cache('rea_user:'.$this->user->id);
        if(!$datas){
            $this->reaUserRepository->authentification_data($this->user);
            $datas = get_cache('rea_user:'.$this->user->id);
        }
        $new_current_sdepartment_id = $request->get('sdepartment_id');
        foreach ($datas->sdepartments as $sdepartment) {
            if($sdepartment->sdepartment_id==$new_current_sdepartment_id){
                $datas->current_sdepartment = clone $sdepartment;
                continue;
            }
        }
        set_cache('rea_user:'.$this->user->id, $datas);
        return $this->sendResponse($datas,['Global.GetResultsWithSuccess']);
    }




/**
     * Check if user is registred by idn_type and idn.
     * GET|HEAD /reaUsers
     *
     * @return Response
     */
    public function checkRegistration($idn_type,$idn)
    {
        $check = ReaUser::checkRegistration($idn,$idn_type);
        if($check){
            $data = [
                'students'=> Student::getByResponsibleId($check)
            ];
            return $this->sendResponse($data,null);
        }else{
            return $this->sendResponse(null,['User.UserNotRegistred'],false);
        }
    }

    /**
     * Set android user keys
     * GET|HEAD /users/setPhoneKey/{type}/{key}
     *
     * @return Response
     */
    public function setPhoneKey($type,$key)
    {
        $rea_user_id = $this->user->id;
        $data = ['ggn_code' =>$key,'mobile_type_id'=>$type];
        $update = ReaUser::where('id','=',$rea_user_id)->update($data);
        if($update)
            return $this->sendResponse(null,['User.GGNCodeUpdatedWithSuccess'],false);
        else
            return $this->sendResponse(null,['User.UpdateGGNCodeFailed'],false);
    }

    /**
     * Get android user keys
     * GET|HEAD /users/unsetPhoneKey/{key}
     *
     * @return Response
     */
    public function unsetPhoneKey()
    {
        $rea_user_id = $this->user->id;
        $data = ['ggn_code'=>null,'mobile_type_id'=>0];
        $update = ReaUser::where('id','=',$rea_user_id)->update($data);
        if($update)
            return $this->sendResponse(null,'users.success.update_token_success');
        else
            return $this->sendResponse(null,'users.errors.update_token_failed',false);
    }
    /**
     * Get android user keys
     * GET|HEAD /users/getPhoneKey/{type}/{rea_user_id}
     *
     * @return Response
     */
    public function getPhoneKeys()
    {
        if(!isset($this->user->id))
            return $this->sendResponse($keys,'rea_user.errors.failed_get_phone_key');

        $ggn_code = ReaUser::where('id','=',$this->user->id)->value('ggn_code');

        $keys = MfkHelper::mfkIdsDecode($ggn_code);
        if($keys)
            return $this->sendResponse($keys,'rea_user.success.get_phone_key');
        else
            return $this->sendResponse(null,'rea_user.errors.empty_get_phone_key');
    }









    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $model = new ReaUser();
        $model_fields = $model->getFillable();
        foreach ($model_fields as &$item) {
            $item = $model->table.'.'.$item;
        }
        $query = $model->select($model_fields);

        if(isset($rea_user_id) OR isset($myinfos) OR isset($idn) ){
            $this->user = Auth::user();
            if( isset($myinfos) ){
                $rea_user_id = $this->user->id;
            }
            if(isset($rea_user_id)){
                $query->where($model->getTable().'.id',$rea_user_id);    
            }

            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('user.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }

        $query->orderBy($model->getTable().'.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }

        foreach ($result as $item) {
            $this->filter_item_after_get($item);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }




    function filter_item_after_get(&$item){
        
        $item->roles = MfkHelper::mfkIdsDecode($item->roles_mfk);
        if($item->club != "0"){
            $club = Club::find(intval($item->club));
            $item->club_name = $club->club_name ;
        }else{
            $item->club_name = "" ; 
        }
        
    }


    public function save(Request $request) {
       
        $data = $request->get('rea_user');
        
        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }



        if(
            (isset($data['id']) AND ReaUser::where('email', $data['email'] )->where('id', '!=', $data['id'])->count())
            OR (isset($data['new']) AND ReaUser::where('email', $data['email'] )->count())
        ){
            return $this->sendResponse(null, ['User.EmailAlreadyUsedByAnotherUser'], false);
        }
        if(
            (isset($data['id']) AND ReaUser::where('email', $data['username'] )->where('id', '!=', $data['id'])->count())
            OR (isset($data['new']) AND ReaUser::where('email', $data['email'] )->count())
        ){
            return $this->sendResponse(null, ['User.UsernameAlreadyUsedByAnotherUser'], false);
        }
        if( clean($data['username']) != $data['username'] ){
            return $this->sendResponse(null, ['User.UsernameNotClean'], false);
        }
        

        if( isset($data['new']) AND $data['new']==true ){
            $rea_user = new ReaUser;
        }else{
            $rea_user = ReaUser::find($data['id']);
        }

        if(!$rea_user){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        if(isset($data['active'])) $rea_user->active = $data['active'];
        if(isset($data['email'])) $rea_user->email = $data['email'];
        if(isset($data['username'])) $rea_user->username = $data['username'];
        if(isset($data['firstname'])) $rea_user->firstname = $data['firstname'];
        if(isset($data['lastname'])) $rea_user->lastname = $data['lastname'];
        if(isset($data['roles_mfk'])) $rea_user->roles_mfk = $data['roles_mfk'];
        if(isset($data['club'])) $rea_user->club = $data['club'];
        if(isset($data['newpwd']) AND !empty($data['newpwd']) ) $rea_user->pwd = bcrypt($data['newpwd']);
        $rea_user->save();
        unset_cache('rea_user:'.$rea_user->id);
        return $this->sendResponse($rea_user->id, ['Form.DataSavedWithSuccess'], true);
    }

    public function save_myinfos(Request $request)
    {
        $data = $request->get('rea_user');
        $this->user = Auth::user();
        if(!isset($this->user->id) OR !$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if(ReaUser::where('email',$data['email'])->where('id','!=',$this->user->id)->count()){
            return $this->sendResponse(null, ['User.TheEmailAddressEntredAlreadyUsed'], false);
        }


        if( 
            !isset($data['email']) OR ($data['email']=="")
            OR !isset($data['genre_id']) OR ($data['genre_id']=="")
            OR !isset($data['country_id']) OR ($data['country_id']=="")
            OR !isset($data['lang_id']) OR ($data['lang_id']=="")
            OR !isset($data['address']) OR ($data['address']=="")
            OR !isset($data['cp']) OR ($data['cp']=="")
            OR !isset($data['email']) OR ($data['email']=="")
            OR !isset($data['firstname']) OR ($data['firstname']=="")
            OR !isset($data['f_firstname']) OR ($data['f_firstname']=="")
            OR !isset($data['idn']) OR ($data['idn']=="")
            OR !isset($data['lastname']) OR ($data['lastname']=="")
            OR !isset($data['mobile']) OR ($data['mobile']=="")
            OR !isset($data['quarter']) OR ($data['quarter']=="")
            OR (
                ( isset($data['newpwd']) AND ($data['newpwd']!="") AND (strlen($data['newpwd'])<6) )
                )
        ){
            return $this->sendResponse(null, ['User.PleaseEnterAValideData'], false);
        }

        $rea_user = ReaUser::find($this->user->id);
        $clone_rea_user = clone $rea_user;
        if(!$rea_user){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        if(isset($data['genre_id'])) $rea_user->genre_id = $data['genre_id'];
        if(isset($data['city_id'])) $rea_user->city_id = $data['city_id'];
        if(isset($data['country_id'])) $rea_user->country_id = $data['country_id'];
        if(isset($data['idn_type_id'])) $rea_user->idn_type_id = $data['idn_type_id'];
        if(isset($data['lang_id'])) $rea_user->lang_id = $data['lang_id'];
        if(isset($data['address'])) $rea_user->address = $data['address'];
        if(isset($data['cp'])) $rea_user->cp = $data['cp'];
        if(isset($data['email'])) $rea_user->email = $data['email'];
        if(isset($data['firstname'])) $rea_user->firstname = $data['firstname'];
        if(isset($data['f_firstname'])) $rea_user->f_firstname = $data['f_firstname'];
        if(isset($data['idn'])) $rea_user->idn = $data['idn'];
        if(isset($data['lastname'])) $rea_user->lastname = $data['lastname'];
        if(isset($data['mobile'])) $rea_user->mobile = $data['mobile'];
        if(isset($data['quarter'])) $rea_user->quarter = $data['quarter'];
        if(isset($data['newpwd']) AND !empty($data['newpwd']) ) $rea_user->pwd = bcrypt($data['newpwd']);

        if( $clone_rea_user->email != $rea_user->email ){
            $rea_user->valide_email = false;
        }
        if( $clone_rea_user->mobile != $rea_user->mobile ){
            $rea_user->valide_mobile = false;
        }

        unset_cache('rea_user:'.$rea_user->id);
        $rea_user->save();

        return $this->sendResponse(null, ['User.InformationSavedWithSuccess']);
    }

    public function remove_connexion(Request $request)
    {
        $social = $request->get('social');

        if(!isset($this->user->id) OR !$social){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        $rea_user = ReaUser::find($this->user->id);

        if(!$rea_user){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        if(!$rea_user->email){
            return $this->sendResponse(null, ['User.EmailNotExists'], false);
        }

        switch ($social) {
            case 'facebook':
                $rea_user->fb_id = null;
                break;

            case 'twitter':
                $rea_user->twitter_id = null;
                break;

            case 'google':
                $rea_user->google_id = null;
                break;
        }
        
        $rea_user->save();

        return $this->sendResponse(null, ['User.ConnexionRemovedWithSuccess']);
    }


    public function delete(Request $request)
    {
        $user_id = $request->get('user_id');

        if(!$user_id){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        $destroy = ReaUser::destroy($user_id);
        if(!$destroy){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
    }



    public function register(Request $request)
    {
        $data = $request->get('rea_user');


        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $rea_user = new ReaUser;


        if(!$rea_user){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        $rea_user->active = "N";
        $rea_user->email = $data['email'];
        $rea_user->mobile = $data['mobile'];
        $rea_user->pwd = bcrypt($data['pwd']);
        $rea_user->save();

        return $this->sendResponse(null, ['User.RegistrationWithSuccess']);
    }



    public function send_active_email(Request $request)
    {

        if( !isset($this->user->id) ){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        $rea_user = ReaUser::find($this->user->id);


        if(!$rea_user){
            return $this->sendResponse(null, ['User.InvalideUser'], false);
        }

        if($rea_user->valide_email=='Y'){
            return $this->sendResponse(null, ['User.EmailAlreadyActived'], false);
        }

        $this->reaUserRepository->send_activation_email($rea_user->id);
        
        return $this->sendResponse(null, ['User.EmailSendedWithSuccess']);
    }





    public function send_active_mobile(Request $request)
    {

        $data = ['mobile' => $request->get('mobile')];
        $v = Validator::make($data, [
            'mobile' => 'required|phone:US,BE,TN',
        ]);

        if ( $v->fails() )
        {   
            return $this->sendResponse(null, ['User.InvalideMobileNumber'], false);
        }

        if( !isset($this->user->id) ){
            return $this->sendResponse(null, ['User.NotLoggedUser'], false);
        }

        $rea_user = ReaUser::find($this->user->id);

        if(!$rea_user){
            return $this->sendResponse(null, ['User.UserNotExists'], false);
        }

        if($rea_user->mobile==$request->get('mobile') && $rea_user->valide_mobile=='Y'){
            return $this->sendResponse(null, ['User.MobileNumberAlreadyValidate'], false);
        }

        $rea_user->mobile = $request->get('mobile');
        $rea_user->save();

        $this->reaUserRepository->send_activation_mobile($rea_user->id);
        
        return $this->sendResponse(null, ['User.MobileCodeSendedWithSuccess']);
    }


    public function submit_activation_mobile_code(Request $request) {
        if( !isset($this->user->id) ){
            return $this->sendResponse(['success' => false], Lang::get('rea_user.errors.not_logged_user'), false);
        }

        $mobile_activation_code = $request->get('mobile_activation_code');
        if( !$mobile_activation_code ){
            return $this->sendResponse(null, ['User.InvalideMobileCode'], flase);
        }

        $rea_user = ReaUser::find($this->user->id);
        if(!$rea_user){
            return $this->sendResponse(null, ['User.InvalideUser'], flase);
        }

        if($rea_user->valide_mobile=='Y'){
            return $this->sendResponse(null, ['User.MobileAlreadyActive'], flase);
        }


        if( $this->reaUserRepository->verifications_mobile_code($rea_user->id, $mobile_activation_code) ){
            $rea_user->valide_mobile = 'Y';
            $rea_user->save();
            return $this->sendResponse(null, ['User.MobileActivedWithSuccess']);
        }else{
            return $this->sendResponse(null, ['User.InvalideMobileCode'], flase);
        }
    }






    public function notifications(Request $request)
    {
        $this->user = Auth::user();
        $data = [];
        $data['notifications'] = $this->reaUserRepository->get_notifications($this->user->id);
        $data['total'] = count($data['notifications']);
        return $this->sendResponse($data, ['ReaUser.GetNotificationsWithSuccess']);
    }




    public function change_password(Request $request)
    {
        $data_request = $request->get('rea_user');

        $v = Validator::make($data_request, [
            'pwd' => 'required|min:6|max:25',
        ]);

        if ( $v->fails() )
        {   
            return $this->sendResponse(null, $v->messages(), false);
        }
        else{
            $rea_user = ReaUser::find($this->user->id);
            if($rea_user->active!="Y"){
                $rea_user->active = "Y";    
            }
            $rea_user->pwd = bcrypt($data_request['pwd']);
            $rea_user->remember_token = NULL;
            $rea_user->save();
            return $this->sendResponse(null, ["ReaUser.PasswordChangedWithSuccess"]);
        }
    }




    /*
        BF: BF1408
        Method: GET
        Params: []
    */
    public function get_my_students(Request $request)
    {
        $student_id = $request->get('student_id');
        $my_students = \App\Models\RserviceStudent::get_available_students_by_rea_user_id($this->user->id, $student_id);
        $students = collect([]);
        foreach ($my_students as $student_id):
            $student_files = Student::get_student_school_fullinfos($student_id, true);
            foreach($student_files as &$student_file){
                $student_file->student_id = (int)$student_file->student_id;
                $student_file->student_file_id = (int)$student_file->student_file_id;
                $student_file->school_class_id = (int)$student_file->school_class_id;
                $student_file->level_class_id = (int)$student_file->level_class_id;
                $student_file->student_place = (int)$student_file->student_place;
                $student_file->school_year_id = (int)$student_file->school_year_id;
                $student_file->school_id = (int)$student_file->school_id;
                $student_file->sdepartment_id = (int)$student_file->sdepartment_id;
            }
            $students = $students->merge($student_files);
        endforeach;
        if(!$students){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }
        $total = count($students);
        return $this->sendResponse($students, ['Global.GetDataWithSuccess'], true,$total);
    }
}
