<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RelshipRepository;
use App\Models\Relship;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
class RelshipAPIController extends AppBaseController
{
	/** @var  RelshipRepository */
	private $relshipRepository;

	function __construct(RelshipRepository $relshipRepo)
	{
		$this->relshipRepository = $relshipRepo;
	}


	/**
	 *	Created by Aziz
	 *	Created at 02/05/2016
	 */
	public function get($params = null)
	{

		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Relship::select([
			'relship.*'
		]);

		if(isset($relship_id)){
			$query->where('relship.id',$relship_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}


		if(!isset($active) OR ($active!='all') ){
			$query->where('relship.active',"Y");
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}



		$query->orderBy('relship.id', 'ASC');
		$result = $query->get();

		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		$count = $query->count();
		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}

	function filter_item_after_get(&$item){
	}


	public function save(Request $request)
	{
		$data = $request->get('relship');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$relship = new Relship;
		}else{
			$relship = Relship::find($data['id']);
		}


		if(!$relship){
			return $this->sendResponse(null, ['RelationShip.InvalideRelationShip'], false);
		}

		if(isset($data['active'])) $relship->active = $data['active'];
		if(isset($data['lookup_code'])) $relship->lookup_code = $data['lookup_code'];
		if(isset($data['relship_name_ar'])) $relship->relship_name_ar = $data['relship_name_ar'];
		if(isset($data['relship_name_en'])) $relship->relship_name_en = $data['relship_name_en'];
		$relship->save();

		return $this->sendResponse($relship->id, ['Form.DataSavedWithSuccess']);
	}


	public function delete(Request $request)
	{
		$relship_id = $request->get('relship_id');

		if(!$relship_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Relship::destroy($relship_id);
		if(!$destroy){
			return $this->sendResponse(null, ['RelationShip.InvalideRelationShip'], false);
		}

		return $this->sendResponse(null, ['Form.DataDeletedWithSuccess']);
	}
}
