<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RemarkRepository;
use App\Models\Remark;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;


class RemarkAPIController extends AppBaseController
{
	/** @var  RemarkRepository */
	private $remarkRepository;

	function __construct(RemarkRepository $remarkRepo)
	{
		$this->remarkRepository = $remarkRepo;
	}


	/**
	 *	Created by Aziz
	 *	Created at 02/05/2016
	 */
	public function get($params = null)
	{

		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Remark::select([
			'remark.*'
		]);

		if(isset($remark_id)){
			$query->where('remark.id',$remark_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}


		if(!isset($active) OR ($active!='all') ){
			$query->where('remark.active',"Y");
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}



		$query->orderBy('remark.id', 'ASC');
		$result = $query->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['remarks' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get(&$item){
	}


	public function save(Request $request)
	{
		$data = $request->get('remark');

		if(!$data){
			return $this->sendResponse(null, Lang::get('remark.errors.empty_remark_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$remark = new Remark;
		}else{
			$remark = Remark::find($data['id']);
		}


		if(!$remark){
			return $this->sendResponse(null, Lang::get('remark.errors.remark_not_existe'), false);
		}

		if(isset($data['active'])) $remark->active = $data['active'];
		if(isset($data['lookup_code'])) $remark->lookup_code = $data['lookup_code'];
		if(isset($data['gremark_id'])) $remark->gremark_id = $data['gremark_id'];
		if(isset($data['remark_name_ar'])) $remark->remark_name_ar = $data['remark_name_ar'];
		if(isset($data['remark_name_en'])) $remark->remark_name_en = $data['remark_name_en'];
		$remark->save();

		return $this->sendResponse($remark, Lang::get('global.success.save'), true);
	}


	public function delete(Request $request)
	{
		$remark_id = $request->get('remark_id');

		if(!$remark_id){
			return $this->sendResponse(null, Lang::get('school.errors.empty_remark_id'), false);
		}

		$destroy = Remark::destroy($remark_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('school.errors.remark_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}
}
