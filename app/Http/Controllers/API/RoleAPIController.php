<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RoleRepository;
use App\Helpers\MfkHelper;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class RoleAPIController extends AppBaseController
{
	/** @var  RoleRepository */
	private $roleRepository;

	function __construct(RoleRepository $roleRepo)
	{
		parent::__construct();
		$this->roleRepository = $roleRepo;
	}




	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new Role();
		$model_fields = $model->getFillable();
		
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
        }

		$query = Role::select(
			$model_fields
		);
		$query->where($model->getTable().'.active','<>',"D");

		if(isset($role_id)){
			$query->where($model->getTable().'.id',$role_id);
			
			//dd($query);

			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		$item->permissions = MfkHelper::mfkIdsDecode($item->permissions_mfk);
	}

	public function save(Request $request)
	{
		$data = $request->get('role');
        if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
		
		if( isset($data['new']) AND $data['new']==true ){
			$role = new Role;
		}else{
			$role = Role::find($data['id']);
		}

		if(!$role){
			return $this->sendResponse(null, ['Role.InvalidRole'], false);
		}

		$model = new Role;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $role->{$item} = $data[$item];
			if( empty($data[$item]) ) $role->{$item} = null;
		}
		$role->save();
        return $this->sendResponse($role->id, ['Form.DataSavedWithSuccess'], true);
	}


	public function delete(Request $request)
	{
		$role_id = $request->get('role_id');
        if(!$role_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
		
		$role = Role::find($role_id);
		if(!$role){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
		}
		$role->active = 'D';
		$role->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
