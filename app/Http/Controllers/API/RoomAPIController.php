<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RoomRepository;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Helpers\MfkHelper;

class RoomAPIController extends AppBaseController
{
	/** @var  RoomRepository */
	private $roomRepository;

	function __construct(RoomRepository $roomRepo)
	{
		parent::__construct();
		$this->roomRepository = $roomRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}


		$model = new Room();
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}
		$query = Room::select(
			$model_fields
		);

		if(isset($room_id)){
			$query->where($model->getTable().'.id',$room_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if( isset($club_id) ){
			$query->where($model->getTable().'.club_id',(int)$club_id);
		}
		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		$item->disponibilities = MfkHelper::mfkIdsDecode($item->disponibility);
	}



	public function save(Request $request)
	{
		$data = $request->get('room');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$room = new Room;
		}else{
			$room = Room::find($data['id']);
		}

		if(!$room){
			return $this->sendResponse(null, ['Room.InvalidRoom'], false);
		}

		$model = new Room;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $room->{$item} = $data[$item];
			if( empty($data[$item]) ) $room->{$item} = null;
		}
		$room->save();

		return $this->sendResponse($room->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$room_id = $request->get('room_id');

		if(!$room_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Room::destroy($room_id);
		if(!$destroy){
			return $this->sendResponse(null, ['Room.InvalidRoom'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
