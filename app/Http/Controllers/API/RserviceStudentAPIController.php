<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RserviceStudentRepository;
use App\Models\RserviceStudent;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class RserviceStudentAPIController extends AppBaseController
{
	/** @var  RserviceStudentRepository */
	private $rserviceStudentRepository;

	function __construct(RserviceStudentRepository $rserviceStudentRepo)
	{
		$this->rserviceStudentRepository = $rserviceStudentRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = RserviceStudent::select([
            'rservice_student.*'
        ]);

        if(isset($rservice_student_id)){
            $query->where('rservice_student.id',$rservice_student_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('rservice_student.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('rservice_student.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        $count = $query->count();
        $return = ['rservice_students' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }




    public function save(Request $request)
    {
        $data = $request->get('rservice_student');

        if(!$data){
            return $this->sendResponse(null, Lang::get('rservice_student.errors.empty_rservice_student_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $rservice_student = new RserviceStudent;
        }else{
            $rservice_student = RserviceStudent::find($data['id']);
        }


        if(!$rservice_student){
            return $this->sendResponse(null, Lang::get('rservice_student.errors.rservice_student_not_existe'), false);
        }

        if(isset($data['active'])) $rservice_student->active = $data['active'];
        if(isset($data['lookup_code'])) $rservice_student->lookup_code = $data['lookup_code'];
        if(isset($data['rservice_student_name'])) $rservice_student->rservice_student_name = $data['rservice_student_name'];
        $rservice_student->save();

        return $this->sendResponse($rservice_student, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $rservice_student_id = $request->get('rservice_student_id');

        if(!$rservice_student_id){
            return $this->sendResponse(null, Lang::get('rservice_student.errors.empty_rservice_student_id'), false);
        }

        $destroy = RserviceStudent::destroy($rservice_student_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('rservice_student.errors.rservice_student_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
