<?php namespace App\Http\Controllers\API;

use App;
use App\Helpers\Server;
use App\Helpers\HijriDateHelper;
use App\Helpers\Urlparams;
use App\Helpers\MfkHelper;
use App\Http\Requests;
use App\Libraries\Repositories\SchoolRepository;
use App\Models\School;
use App\Events\ReaUserEvent;
use App\Models\SchoolLevel;
use App\Models\Period;
use App\Models\SchoolYear;
use App\Models\Sdepartment;
use App\Models\SchoolEmployee;
use App\Models\SchoolJob;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Session;
use Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redis;


class SchoolAPIController extends AppBaseController
{
	/** @var  SchoolRepository */
	private $schoolRepository;

	function __construct(SchoolRepository $schoolRepo)
	{
		parent::__construct();

		//$this->group_num = Server::get('SCHOOLS',$this->user->current_school_id,'group_num');
		//$this->school_id = $this->user->current_school_id;
		//$this->prof_id = $this->user->id;
		$this->schoolRepository = $schoolRepo;

	}


	/**
	*	Created by Achraf
	*	Created at 29/04/2016
	*	Function key: Get All Schools For User
	*/
	public function get_my_schools()
	{

		$query = School::select('school.*')
		->join('school_employee','school_employee.school_id','=','school.id')
		->where('school_employee.rea_user_id','=',$this->user->id);


		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			
		}

		foreach ($result as &$school) {
			$this->filter_item_after_get($school);
		}

		$count = $query->count();
		$return = ['schools' => $result, 'total' => $count];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}



	/**
	*	Created by Achraf
	*	Created at 06/03/2016
	*	Function key: BF264 / Get WeekDays By School ID
	*/
	public function GetWeekDays($school_id)
	{
		$result = School::select('country.we_days_mfk')
		->join('city','city.id','=','school.city_id')
		->join('country','country.id','=','city.country_id')
		->where('school.id',$school_id)
		->first();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('school_year.errors.empty_we_days'), false);	
		}
		$days = $result->we_days_mfk;
		$days = explode(',',$days);
		$days = array_values(array_filter($days));

		return $this->sendResponse($days, Lang::get('school_year.success.valide_we_days'), true);	
	}








	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{

		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$query = School::select([
			'school.*'
		]);


		if(isset($school_id)){
			$query->where('school.id',$school_id);
			if(isset($fullinfos)){
				$query->leftJoin('school_type', 'school_type.id', '=', 'school.school_type_id');
				$query->leftJoin('genre', 'genre.id', '=', 'school.genre_id');
				$query->leftJoin('holiday', 'holiday.id', '=', 'school.holidays_school_id');
				$query->leftJoin('courses_config_template', 'courses_config_template.id', '=', 'school.courses_config_template_id');
				$query->leftJoin('courses_template', 'courses_template.id', '=', 'school.courses_template_id');
				$query->leftJoin('lang', 'lang.id', '=', 'school.lang_id');
				$query->leftJoin('school as t_school_group', 't_school_group.id', '=', 'school.group_school_id');
				$query->leftJoin('city', 'city.id', '=', 'school.city_id');
				$query->leftJoin('country', 'country.id', '=', 'city.country_id');
				$query->addSelect([
					'school_type.school_type_name_ar',
					'school_type.school_type_name_en',
					'genre.genre_name_ar',
					'genre.genre_name_en',
					'courses_config_template.courses_config_template_name_ar',
					'courses_config_template.courses_config_template_name_en',
					'courses_template.courses_template_name_ar',
					'courses_template.courses_template_name_en',
					'lang.lang_name_ar',
					'lang.lang_name_en',
					'holiday.holiday_name_ar',
					'holiday.holiday_name_en',
					'city.city_name_ar',
					'city.city_name_en',
					'country.country_name_ar',
					'country.country_name_en',
					't_school_group.school_name_ar as group_school_name_ar',
					't_school_group.school_name_ar as group_school_name_en',
					]);
			}
			$school = $query->first();

			$this->filter_item_after_get($school);

			if(isset($fullinfos)){
				$this->fullinfos_fields($school);
			}

			if(!$school){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			return $this->sendResponse($school, Lang::get('global.success.results'), true);
		}




		if( isset($join) AND !empty($join) ){
			$join = explode('!', $join);
		}else{
			$join = [];
		}

		if( in_array('city',$join) or in_array('all',$join) ){
			$query->leftJoin('city','city.id','=','school.city_id');
			$query->AddSelect(['city.city_name_ar','city.city_name_en']);
		}
		if( in_array('country',$join) or in_array('all',$join) ){
			$query->leftJoin('country','country.id','=','city.country_id');
			$query->AddSelect(['country.country_name_ar','country.country_name_en']);
		}
		if( in_array('group_school',$join) or in_array('all',$join) ){
			$query->leftJoin('school as group_school','group_school.id','=','school.group_school_id');
			$query->AddSelect(['group_school.school_name_ar as group_school_name_ar', 'group_school.school_name_en as group_school_name_en']);
		}

		if( in_array('school_type',$join) or in_array('all',$join) ){
			$query->leftJoin('school_type','school_type.id','=','school.school_type_id');
			$query->AddSelect(['school_type.school_type_name_ar','school_type.school_type_name_en']);
		}
		if( in_array('levels_template',$join) or in_array('all',$join) ){
			$query->leftJoin('levels_template','levels_template.id','=','school.levels_template_id');
			$query->AddSelect(['levels_template.levels_template_name_ar','levels_template.levels_template_name_en']);
		}
		if( in_array('courses_template',$join) or in_array('all',$join) ){
			$query->leftJoin('courses_template','courses_template.id','=','school.courses_template_id');
			$query->AddSelect(['courses_template.courses_template_name_ar','courses_template.courses_template_name_ar']);
		}
		if( in_array('courses_config_template',$join) or in_array('all',$join) ){
			$query->leftJoin('courses_config_template','courses_config_template.id','=','school.courses_config_template_id');
			$query->AddSelect(['courses_config_template.courses_config_template_name_ar','courses_config_template.courses_config_template_name_en']);
		}
		if( in_array('holiday',$join) or in_array('all',$join) ){
			$query->leftJoin('holiday','holiday.id','=','school.holidays_school_id');
			$query->AddSelect(['holiday.holiday_name_ar','holiday.holiday_name_en']);
		}



		if(isset($exclude_id)){
			$query->where('school.id','<>', (int)$exclude_id);
		}
		if(isset($school_type_id)){
			$query->where('school.school_type_id',(int)$school_type_id);
		}

		if(isset($genre_id)){
			$query->where('school.genre_id',(int)$genre_id);
		}
		if(isset($lang_id)){
			$query->where('school.lang_id',(int)$lang_id);
		}
		if(isset($levels_template_id)){
			$query->where('school.levels_template_id',(int)$levels_template_id);
		}
		if(isset($courses_template_id)){
			$query->where('school.courses_template_id',(int)$courses_template_id);
		}
		if(isset($group_num)){
			$query->where('school.group_num',(int)$group_num);
		}
		if(isset($country_id)){
			$query->join('city as fcity','fcity.id','=','school.city_id');
			$query->join('country as fcountry','fcountry.id','=','fcity.country_id');
			$query->where('fcountry.id',(int)$country_id);
		}
		if(isset($city_id)){
			$query->where('school.city_id',(int)$city_id);
		}
		if(isset($name)){
			$query->where(function ($query) use($name) {
                $query->where('school.school_name_ar', 'LIKE', '%'.urldecode($name).'%' );
                $query->orWhere('school.school_name_en', 'LIKE', '%'.urldecode($name).'%' );
            });
		}
		if(isset($name_or_id)){
			$query->where(function ($query) use($name_or_id) {
                $query->where('school.id', (int)$name_or_id );
                $query->orWhere('school.school_name_ar', 'LIKE', '%'.urldecode($name_or_id).'%' );
                $query->orWhere('school.school_name_en', 'LIKE', '%'.urldecode($name_or_id).'%' );
            });
		}

		if(isset($period_id)){
			$query->where('school.period_mfk', 'LIKE', '%,'.(int)($period_id).',%' );
		}

		if(isset($school_level_id)){
			$query->where('school.school_level_mfk', 'LIKE', '%,'.(int)($school_level_id).',%' );
		}

		if(isset($status)){
			$active = $status;
		}
		if( isset($active) AND in_array($active,['Y','N','all']) ){
			if( in_array($active,['Y','N']) ){
				$query->where('school.active',$active);
			}
		}else{
			$query->where('school.active',"Y");
		}


		
		


		$_order_by = 'school.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','school_name_ar','school_name_en']) ){
			$_order_by = 'school.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);


		if( isset($export) ){
			unset($limit);
		}
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}
		$result = $query->get();

		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			
		}

		foreach ($result as &$school) {
			$this->filter_item_after_get($school);
		}

		

		$count = $query->count();
		$return = ['schools' => $result, 'total' => $count];

		if( isset($export) ){
			\App\Helpers\ExcelHelper::export_model($result->toArray(),'schools',School::getExportedData());
		}

		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}

	function filter_item_after_get(&$school){
		$school->school_levels = MfkHelper::mfkIdsDecode($school->school_level_mfk);
		$school->periods = MfkHelper::mfkIdsDecode($school->period_mfk);
		if($school->levels_template_id) $school->levels_template_id = (int)$school->levels_template_id;
		if($school->courses_template_id) $school->courses_template_id = (int)$school->courses_template_id;
		if($school->courses_config_template_id) $school->courses_config_template_id = (int)$school->courses_config_template_id;
		if($school->holidays_school_id) $school->holidays_school_id = (int)$school->holidays_school_id;
		if($school->lang_id) $school->lang_id = (int)$school->lang_id;
		if($school->city_id) $school->city_id = (int)$school->city_id;
		if($school->genre_id) $school->genre_id = (int)$school->genre_id;
		if($school->expiring_hdate) $school->expiring_hdate_formated = $school->expiring_hdate;
		if($school->date_system_id) $school->date_system_id = (int)$school->date_system_id;
	}


	function fullinfos_fields(&$school){
		if($school->school_levels){
			$school->school_levels = SchoolLevel::select([
				'school_level.id',
				'school_level.school_level_name_ar',
				'school_level.levels_template_id',
				'levels_template.levels_template_name_ar',
				])
			->leftJoin('levels_template','levels_template.id', '=' ,'school_level.levels_template_id')
			->whereIn('school_level.id', $school->school_levels)->get();
		}
		if($school->periods){
			$school->periods = Period::select([
				'period.id',
				'period.period_name_ar',
				'period.period_name_en'
				])->whereIn('id',$school->periods)->get();
		}

		$school->school_years = SchoolYear::select([
				'school_year.id',
				'school_year.school_year_name_ar',
				'school_year.school_year_name_en',
				'school_year.school_year_start_hdate',
				'school_year.school_year_end_hdate',
				'school_year.year',
				])->where('school_id','=',$school->id)->get();


		$school->sdepartments = Sdepartment::select([
				'sdepartment.id',
				'sdepartment.sdepartment_name_ar',
				'sdepartment.sdepartment_name_en',
				'sdepartment.week_template_id',
				'week_template.week_template_name_ar',
				'week_template.week_template_name_en',
				])
		->leftJoin('week_template','week_template.id', '=' ,'sdepartment.week_template_id')
		->where('sdepartment.school_id','=',$school->id)->get();



		$school->school_employees = SchoolEmployee::select([
				'school_employee.id',
				'school_employee.job_description',
				'school_employee.rea_user_id',
				'school_employee.school_job_mfk',
				'rea_user.firstname',
				'rea_user.lastname',
				'rea_user.f_firstname',
				'sdepartment.sdepartment_name_ar',
				'sdepartment.sdepartment_name_en',
				'school.school_name_ar',
				'school.school_name_en',
				])
		->leftJoin('rea_user','rea_user.id', '=' ,'school_employee.rea_user_id')
		->leftJoin('school','school.id', '=' ,'school_employee.school_id')
		->leftJoin('sdepartment','sdepartment.id', '=' ,'school_employee.sdepartment_id')
		->where('school_employee.school_id','=',$school->id)->get();
		foreach($school->school_employees as &$school_employee){
			$school_employee->school_jobs = MfkHelper::mfkIdsDecode($school_employee->school_job_mfk);
			if($school_employee->school_jobs){
				$school_employee->school_jobs = SchoolJob::select([
					'school_job.id',
					'school_job.school_job_name_ar',
					'school_job.school_job_name_en',
					])
				->whereIn('school_job.id', $school_employee->school_jobs)
				->get();
			}
		}
	}



	/**
	* Crud Save API
	* Created by achraf
	* Created at 03/05/2016
	*/
	public function save(Request $request)
	{
		$data = $request->get('school');

		if(!$data){
			return $this->sendResponse(null, Lang::get('school.errors.empty_school_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$school = new School;
		}else{
			$school = School::find($data['id']);	
		}

		
		if(!$school){
			return $this->sendResponse(null, Lang::get('school.errors.school_not_existe'), false);
		}

		if(isset($data['active'])) $school->active = $data['active'];
		if(isset($data['school_name_ar'])) $school->school_name_ar = $data['school_name_ar'];
		if(isset($data['school_name_en'])) $school->school_name_en = $data['school_name_en'];
		if(isset($data['genre_id'])) $school->genre_id = $data['genre_id'];
		if(isset($data['address'])) $school->address = $data['address'];
		if(isset($data['pc'])) $school->pc = $data['pc'];
		if(isset($data['quarter'])) $school->quarter = $data['quarter'];
		if(isset($data['city_id'])) $school->city_id = $data['city_id'];
		if(isset($data['school_type_id'])) $school->school_type_id = $data['school_type_id'];
		if(isset($data['group_school_id'])) $school->group_school_id = $data['group_school_id'];
		if(isset($data['maps_location_url'])) $school->maps_location_url = $data['maps_location_url'];
		if(isset($data['period_mfk'])) $school->period_mfk = $data['period_mfk'];
		if(isset($data['scapacity'])) $school->scapacity = $data['scapacity'];
		if(isset($data['expiring_hdate'])) $school->expiring_hdate = $data['expiring_hdate'];
		if(isset($data['sp1'])) $school->sp1 = $data['sp1'];
		if(isset($data['sp2'])) $school->sp2 = $data['sp2'];
		if(isset($data['lang_id'])) $school->lang_id = $data['lang_id'];
		if(isset($data['levels_template_id'])) $school->levels_template_id = $data['levels_template_id'];
		if(isset($data['courses_template_id'])) $school->courses_template_id = $data['courses_template_id'];
		if(isset($data['courses_config_template_id'])) $school->courses_config_template_id = $data['courses_config_template_id'];
		if(isset($data['school_level_mfk'])) $school->school_level_mfk = $data['school_level_mfk'];
		if(isset($data['holidays_school_id'])) $school->holidays_school_id = $data['holidays_school_id'];
		if(isset($data['group_num'])) $school->group_num = $data['group_num'];
		if(isset($data['date_system_id'])) $school->date_system_id = $data['date_system_id'];
		
		$school->save();

		return $this->sendResponse($school, Lang::get('global.success.save'), true);
	}


	public function delete(Request $request)
	{
		$school_id = $request->get('school_id');

		if(!$school_id){
			return $this->sendResponse(null, Lang::get('school.errors.empty_school_id'), false);
		}

		$destroy = School::destroy($school_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('school.errors.school_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}
}