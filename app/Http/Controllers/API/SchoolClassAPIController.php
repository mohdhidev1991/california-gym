<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolClassRepository;
use App\Models\SchoolClass;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SchoolClassAPIController extends AppBaseController
{
	/** @var  SchoolClassRepository */
	private $schoolClassRepository;

	function __construct(SchoolClassRepository $schoolClassRepo)
	{
		parent::__construct();
		$this->schoolClassRepository = $schoolClassRepo;
	}


	/**
	*	Created by Achraf
	*	Created at 07/03/2016
	*	Function key: BF264 / Q0009
	*/
	public function getAllSchoolClassBySchoolClassIDAndLevelClassID($school_class_id,$level_class_id)
	{

		$result = SchoolClass::select('school_class.id','school_class.symbol')
			->where('school_class.active','=',"Y")
			->where('school_class.school_class_id',"=",$school_class_id)
			->where('school_class.level_class_id',"=",$level_class_id)
			->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('school_class.errors.empty_school_class'), false);	
		}

		return $this->sendResponse($result, Lang::get('school_class.success.valide_school_class'), true);	
	}





	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = SchoolClass::select([
			'school_class.*'
		]);


		if( isset($join) AND !empty($join) ){
            $join = explode('!', $join);
        }else{
            $join = [];
        }
    
        if(in_array('room', $join) OR in_array('all', $join)){
            $query->join('room','room.id','=','school_class.room_id');
            $query->addSelect([
                'room.capacity',
                'room.room_num',
                'room.room_name_ar',
                'room.room_name_en',
            ]);
        }

		if(isset($school_class_id)){
			$query->where('school_class.id',$school_class_id);

			if(isset($fullinfos)){
				$query->leftJoin('level_class','level_class.id', '=' ,'school_class.level_class_id');
				$query->leftJoin('room','room.id', '=' ,'school_class.room_id');
				$query->addSelect([
					'school_class.symbol',
					'level_class.level_class_name_ar',
					'room.room_name',
					'room.capacity',
				]);
			}
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
		}



		if(!isset($active) OR ($active!='all') ){
			$query->where('school_class.active',"Y");
		}

		if( isset($level_class_id) ){
			$query->where('school_class.level_class_id',"=",$level_class_id);
		}
		if( isset($school_year_id) ){
			$query->where('school_class.school_year_id',"=",$school_year_id);
		}
		

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('school_class.id', 'ASC');
		$result = $query->get();
		$total = $query->count();
		if(!$total){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		if($item->room_id) $item->room_id = (int)$item->room_id;
	}



	public function save(Request $request)
	{
		$data = $request->get('school_class');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$school_class = new SchoolClass;
		}else{
			$school_class = SchoolClass::find($data['id']);	
		}

		
		if(!$school_class){
			return $this->sendResponse(null, ['SchoolClass.InvalideSchoolClass'], false);
		}


		if(isset($data['active'])) $school_class->active = $data['active'];
		if(isset($data['symbol'])) $school_class->symbol = $data['symbol'];
		if(isset($data['level_class_id'])) $school_class->level_class_id = $data['level_class_id'];
		if(isset($data['school_year_id'])) $school_class->school_year_id = $data['school_year_id'];
		if(isset($data['room_id'])) $school_class->room_id = $data['room_id'];
		
		$school_class->save();

		return $this->sendResponse($school_class->id, ['Form.DataSavedWithSuccess']);
	}




	public function delete(Request $request)
	{
		$school_class_id = $request->get('school_class_id');

		if(!$school_class_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = SchoolClass::destroy($school_class_id);
		if(!$destroy){
			return $this->sendResponse(null, ['SchoolClass.InvalideSchoolClass'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess']);
	}




}
