<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolClassCourseRepository;
use App\Models\SchoolClass;
use App\Models\SchoolClassCourse;
use App\Models\School;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use DB;

class SchoolClassCourseAPIController extends AppBaseController
{
	/** @var  SchoolClassCourseRepository */
	private $schoolClassCourseRepository;

	function __construct(SchoolClassCourseRepository $schoolClassCourseRepo)
	{
        parent::__construct();
		$this->schoolClassCourseRepository = $schoolClassCourseRepo;
	}

    /**
     * Q0014 - Q0015
     * Get level Class list
     * LevelClass/getAllBySchoolIdAndYearIdAndDomaineMfk/school_id/{school_id}/year_id/{year_id}
     *
     * @param  int $school_id
     * @param  int $school_year_id
     *
     * @return Response
     */
    public function getAllBySchoolYearIdAndCourseConfigTemplateId($school_year_id,$level_class_id,$cct_id)
    {
        $select = SchoolClass::select(
            'school_class.school_year_id',
            'school_class.level_class_id',
            'school_class.symbol',
            'courses_config_item.course_id',
            DB::raw(null),
            DB::Raw('"Y"'),
            DB::Raw(0),
            DB::Raw($this->user->id),
            DB::Raw('now()')
        )
            ->join('courses_config_item', function ($join) use ($school_year_id,$level_class_id) {
                    $join->on('school_class.school_year_id', '=', DB::Raw($school_year_id))
                         ->on('school_class.level_class_id', '=', DB::Raw($level_class_id))
                         ->on('courses_config_item.level_class_id', '=', 'school_class.level_class_id')
                         ->on('courses_config_item.session_nb', '>', DB::Raw(0));
            })
            ->leftJoin('school_class_course', function ($join){
                $join->on('school_class_course.school_year_id', '=', 'school_class.school_year_id')
                    ->on('school_class_course.level_class_id', '=', 'school_class.level_class_id')
                    ->on('school_class_course.symbol', '=', 'school_class.symbol')
                    ->on('school_class_course.course_id', '=', 'courses_config_item.course_id');
            })
            ->whereNull('school_class_course.id')
            ->where('courses_config_item.courses_config_template_id', '=', $cct_id);
        $bindings = $select->getBindings();
        $insertQuery = "INSERT into school_scope (school_year_id,school_level_id,level_class_id,class_nb,active,version,created_by,created_at) ". $select->toSql();

        $query  = DB::insert($insertQuery, $bindings);


        return $this->sendResponse(null, Lang::get('school_year.success.valide_level_class'), true);
    }

    public function getLevelClassListByTeacherId($teacher_id = null)
    {
        if(!isset($teacher_id))
            $teacher_id = $this->user->id;

        if(isset($teacher_id) && empty($teacher_id))
            return $this->sendResponse(null,['SchoolClassCourse.errors.teacherIdIsRequired'],false);

        $level_class = SchoolClassCourse::select(
            'school_class_course.level_class_id',
            'school_class_course.symbol',
            'level_class.level_class_name_ar',
            'level_class.level_class_name_en',
            'school_level.school_level_name_en',
            'school_level.school_level_name_ar',
            DB::raw('COUNT(DISTINCT(student_file.id)) AS nb_students'),
            DB::raw('COUNT(DISTINCT(course.id)) AS nb_courses'),
            DB::raw('MAX(course.course_name_ar) AS course_name_ar_max'),
            DB::raw('MAX(course.course_name_en) AS course_name_en_max'),
            DB::raw('MIN(course.course_name_ar) AS course_name_ar_min'),
            DB::raw('MIN(course.course_name_en) AS course_name_en_min')
        )
        ->join('level_class','level_class.id','=','school_class_course.level_class_id')
        ->join('school_level','school_level.id','=','level_class.school_level_id')
        ->join('course','course.id','=','school_class_course.course_id')
        ->join('student_file',function($join){
            $join->on('student_file.level_class_id','=','school_class_course.level_class_id');
            $join->on('student_file.symbol','=','school_class_course.symbol');
        })
        ->where('school_class_course.prof_id',$teacher_id)
        ->where('school_class_course.active','Y')
        ->groupBY('school_class_course.level_class_id','school_class_course.symbol')
        ->get();
        if( count($level_class->toArray()) > 0 )
            return $this->sendResponse($level_class,['SchoolClassCourse.success.notEmptyResult'],true,count($level_class));
        else
            return $this->sendResponse(null,['SchoolClassCourse.errors.emptyResult']);
    }

    public function get($params = null)
    {
        $scc = new SchoolClassCourse();

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $fields = [
            'school_class_course.level_class_id',
            'school_class_course.symbol'
        ];
        $query = SchoolClassCourse::select($fields);

        if(isset($school_class_course_id)){
            $query->where('school_class_course.id',$school_class_course_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }

        $query->where('school_class_course.prof_id',$this->rea_user->current_school->school_employee_id);

        if( isset($join) AND !empty($join) ){
            $join = explode('!', $join);
        }else{
            $join = [];
        }

        if(in_array('level_class', $join) OR in_array('all', $join)){
            $query->join('level_class','level_class.id','=','school_class_course.level_class_id');
            $query->addSelect([
                'level_class.level_class_name_ar',
                'level_class.level_class_name_en',
            ]);
        }

        if(in_array('school_level', $join) OR in_array('all', $join)){
            $query->join('school_level','school_level.id','=','level_class.school_level_id');
            $query->addSelect([
                'school_level.school_level_name_ar',
                'school_level.school_level_name_en',
            ]);
        }

        if(in_array('course', $join) OR in_array('all', $join)){
            $query->join('course','course.id','=','school_class_course.course_id');
            $query->addSelect([
                DB::raw('course.id AS course_id'),
                'course.course_name_en',
                'course.course_name_ar'
            ]);
        }

        if(in_array('student_file', $join) OR in_array('all', $join)){
            $query->join('student_file','student_file.level_class_id','=','level_class.id');
            $query->addSelect([
                DB::raw('COUNT(student_file.id) AS nb_students'),
            ]);
        }
        if(!isset($active) OR ($active!='all') ){
            $query->where('school_class_course.active',"Y");
        }


        if( isset($groupBy) AND !empty($groupBy) ){
            $groupBy = explode(':', $groupBy);
        }else{
            $groupBy = [];
        }
        if(in_array('level_class_id',$groupBy))
            $query->groupBy('school_class_course.level_class_id');
        if(in_array('symbol',$groupBy))
            $query->groupBy('school_class_course.symbol');
        if(in_array('level_class',$groupBy)){
            $query->groupBy('level_class.level_class_name_en');
            $query->groupBy('level_class.level_class_name_ar');
        }
        if(in_array('school_level',$groupBy)){
            $query->groupBy('school_level.school_level_name_en');
            $query->groupBy('school_level.school_level_name_ar');
        }

        if(in_array('course',$groupBy)){
            $query->groupBy('course.id');
            $query->groupBy('course.course_name_en');
            $query->groupBy('course.course_name_ar');
        }

        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }

        $query->orderBy('school_class_course.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, [Lang::get('global.errors.empty')], false);
        }
        $count = $query->count();
        return $this->sendResponse($result, [Lang::get('global.success.results')], true,$count);
    }




    public function save(Request $request)
    {
        $data = $request->get('school_class_course');

        if(!$data){
            return $this->sendResponse(null, Lang::get('school_class_course.errors.empty_school_class_course_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $school_class_course = new SchoolClassCourse;
        }else{
            $school_class_course = SchoolClassCourse::find($data['id']);
        }


        if(!$school_class_course){
            return $this->sendResponse(null, Lang::get('school_class_course.errors.school_class_course_not_existe'), false);
        }

        if(isset($data['active'])) $school_class_course->active = $data['active'];
        if(isset($data['course_id'])) $school_class_course->course_id = $data['course_id'];
        if(isset($data['level_class_id'])) $school_class_course->level_class_id = $data['level_class_id'];
        if(isset($data['prof_id'])) $school_class_course->school_class_course_name = $data['prof_id'];
        if(isset($data['school_year_id'])) $school_class_course->school_class_course_name = $data['school_year_id'];
        if(isset($data['symbol'])) $school_class_course->school_class_course_name = $data['symbol'];
        $school_class_course->save();

        return $this->sendResponse($school_class_course, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $school_class_course_id = $request->get('school_class_course_id');

        if(!$school_class_course_id){
            return $this->sendResponse(null, Lang::get('school_class_course.errors.empty_school_class_course_id'), false);
        }

        $destroy = SchoolClassCourse::destroy($school_class_course_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('school_class_course.errors.school_class_course_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }






    public function generate(Request $request)
    {
        $data = $request->all();
        extract($data);

        if( !isset($school_year_id) OR !isset($level_class_id) OR !isset($school_id) ){
            return $this->sendResponse(['success' => false, 'errors' => ['Global.EmptyData'] ], Lang::get('global.errors.empty_data'), false);
        }

        $school = School::find($school_id);
        if( !isset($school->courses_config_template_id) ){
            return $this->sendResponse(['success' => false, 'errors' => ['Global.EmptyData'] ], Lang::get('global.errors.empty_data'), false);
        }


        $sql = "INSERT
            INTO
              school_class_course(
                school_year_id,
                level_class_id,
                symbol,
                course_id,
                prof_id,
                active,
                VERSION,
                created_by,
                created_at
              )
            SELECT
              sc.school_year_id,
              sc.level_class_id,
              sc.symbol,
              cci.course_id,
              NULL,
              'Y',
              0,
              :rea_user_id,
              NOW()
            FROM
              school_class sc
            INNER JOIN
              courses_config_item cci ON sc.school_year_id = :school_year_id AND sc.level_class_id = :level_class_id AND cci.level_class_id = sc.level_class_id AND cci.session_nb > 0
            LEFT JOIN
              school_class_course scc ON scc.school_year_id = sc.school_year_id AND scc.level_class_id = sc.level_class_id AND scc.symbol = sc.symbol AND scc.course_id = cci.course_id
            WHERE
              cci.courses_config_template_id = :courses_config_template_id AND scc.id IS NULL";
        DB::insert( DB::raw($sql), [
            'rea_user_id' => $this->user->id,
            'school_year_id' => (int)$school_year_id,
            'level_class_id' => (int)$level_class_id,
            'courses_config_template_id' => (int)$school->courses_config_template_id
            ] );

        $query = SchoolClassCourse::select([
            'school_class_course.id',
            'school_class_course.symbol',
            'school_class_course.prof_id',
            'school_class_course.level_class_id',
            'school_class_course.school_year_id',
            'school_class_course.course_id',
            'course.course_name_ar',
            'course.course_name_en',
            'level_class.level_class_name_ar',
            'level_class.level_class_name_en',
            ])
        ->join('course','course.id','=','school_class_course.course_id')
        ->join('level_class','level_class.id','=','school_class_course.level_class_id')
        ->where('school_class_course.school_year_id',$school_year_id)
        ->where('school_class_course.level_class_id',$level_class_id)
        ->where('school_class_course.active','Y')
        ->orderBy('school_class_course.symbol','ASC');
        $results = $query->get();

        $response = ['school_classes_courses' => $results];
        return $this->sendResponse($response, Lang::get('global.success.generate'), true);
    }


    public function multisave(Request $request)
    {
        $data = $request->get('school_classes_courses');

        if(!$data){
            return $this->sendResponse(null, Lang::get('school_class_course.errors.empty_school_class_course_data'), false);
        }

        foreach($data as $item){
            if( !isset($item['id']) OR !isset($item['prof_id']) ){
                return $this->sendResponse(null, Lang::get('school_class_course.errors.empty_school_class_course_data'), false);
            }
            $school_class_course = SchoolClassCourse::find($item['id']);
            if($school_class_course->prof_id!=$item['prof_id']){
                $school_class_course->prof_id = $item['prof_id'];
                $school_class_course->version++;
                $school_class_course->save();    
            }
        }

        return $this->sendResponse($data, Lang::get('global.success.multisave'), true);
    }
}
