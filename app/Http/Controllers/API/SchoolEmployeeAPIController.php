<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolEmployeeRepository;
use App\Models\SchoolEmployee;
use App\Models\SchoolClassCourse;
use App\Models\CourseSession;
use App\Models\ReaUser;
use App\Models\School;
use App\Models\Sdepartment;
use App\Models\Employee;
use App\Models\SProf;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use App\Helpers\MfkHelper;
use App\Libraries\Factories\ReaUserFactory;

class SchoolEmployeeAPIController extends AppBaseController
{
	/** @var  SchoolEmployeeRepository */
	private $school_employeeRepository;

	function __construct(SchoolEmployeeRepository $school_employeeRepo)
	{
		parent::__construct();
		$this->school_employeeRepository = $school_employeeRepo;
	}


	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = SchoolEmployee::select([
			'school_employee.*'
		]);

		$current_school_id = ReaUserFactory::get_current_school_id();
		if(!$current_school_id){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		$query->where('school_employee.school_id',(int)$current_school_id);
		$query->addSelect(\DB::raw("CONCAT(school_employee.firstname,' ',school_employee.f_firstname,' ',school_employee.lastname) as fullname"));

		if(isset($school_employee_id)){
			$query->where('school_employee.id',$school_employee_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('school_employee.active',"Y");
		}






		if( isset($school_id) ){
			$query->where('school_employee.school_id',(int)$school_id);
		}



		if( isset($join) AND !empty($join) ){
			$join = explode('!', $join);
		}else{
			$join = [];
		}

		if( in_array('rea_user',$join) or in_array('all',$join) ){
			$query->leftJoin('rea_user','rea_user.id','=','school_employee.rea_user_id');
            $query->addSelect(['rea_user.idn']);
		}

		if( isset($sdepartment_id) ){
			$query->where('school_employee.sdepartment_id',(int)$sdepartment_id);
		}
		if( isset($exclude_id) ){
			$query->where('school_employee.id','<>',(int)$exclude_id);
		}

		if( isset($course_id) ){
			$query->where('school_employee.course_mfk','LIKE','%,'.(int)$course_id.',%');
		}

		if( isset($school_job) AND ($school_job=='teacher') ){
			$query->where('school_employee.school_job_mfk', 'LIKE' ,"%,".School_Job_Teacher.",%" );
		}
		if( isset($key_search) ){
			$query->where(function ($query) use ($key_search, $join) {
                $query->where('school_employee.g_f_firstname', 'LIKE' ,'%'.$key_search.'%');
                $query->orWhere('school_employee.firstname', 'LIKE' ,'%'.$key_search.'%');
                $query->orWhere('school_employee.lastname', 'LIKE' ,'%'.$key_search.'%');
                $query->orWhere('school_employee.f_firstname', 'LIKE' ,'%'.$key_search.'%');
                $query->orWhere('school_employee.job_description', 'LIKE' ,'%'.$key_search.'%');
                $query->orWhere(\DB::raw("CONCAT(school_employee.firstname,' ',school_employee.f_firstname,' ',school_employee.lastname)"), 'LIKE' ,'%'.$key_search.'%');
                if(is_numeric($key_search)){
                	$query->orWhere('school_employee.phone', 'LIKE',$key_search);
                	$query->orWhere('school_employee.mobile', 'LIKE',$key_search);
                }
                if(is_numeric($key_search) AND (in_array('rea_user',$join) OR in_array('all',$join)) ){
                	$query->orWhere('rea_user.idn', 'LIKE',$key_search);
                }
                
            });
		}


		if(isset($same_wdays)){
			$employee_same_wdays = SchoolEmployee::find((int)$same_wdays);
			$employee_same_wdays->wdays = (array) MfkHelper::mfkIdsDecode($employee_same_wdays->wday_mfk);
			if(!empty($employee_same_wdays->wdays)){
				foreach ($employee_same_wdays->wdays as $item_day) {
					$query->where('school_employee.wday_mfk', 'LIKE', '%,'.(int)$item_day.',%');
				}	
			}else{
				$query->where('school_employee.wday_mfk', 'LIKE', 0);
			}
		}
		if(isset($same_wdays_or_null)){
			$employee_same_wdays = SchoolEmployee::find((int)$same_wdays_or_null);
			$employee_same_wdays->wdays = (array) MfkHelper::mfkIdsDecode($employee_same_wdays->wday_mfk);
			if(!empty($employee_same_wdays->wdays)){
				$query->where(function ($query) use($employee_same_wdays) {
					$query->where(function ($query) use($employee_same_wdays) {
						foreach ($employee_same_wdays->wdays as $item_day) {
							$query->where('school_employee.wday_mfk', 'LIKE', '%,'.(int)$item_day.',%');
						}
					});
					$query->orWhereNull('school_employee.wday_mfk');
            	});
			}
		}

		if(isset($same_courses)){
			if($same_courses AND isset($same_wdays) AND ($same_wdays==$same_courses) ){
				$employee_same_courses = $employee_same_wdays;
			}else{
				$employee_same_courses = SchoolEmployee::find((int)$same_courses);
			}
			$employee_same_courses->courses = (array) MfkHelper::mfkIdsDecode($employee_same_courses->course_mfk);
			if(!empty($employee_same_courses->courses)){
				foreach ($employee_same_courses->courses as $item_course) {
					$query->where('school_employee.course_mfk', 'LIKE', '%,'.(int)$item_course.',%');
				}	
			}else{
				$query->where('school_employee.course_mfk', 'LIKE', 0);
			}
		}
		if(isset($same_courses_or_null)){
			if( $same_courses_or_null AND isset($same_wdays_or_null) AND ($same_wdays_or_null==$same_courses_or_null) ){
				$employee_same_courses = $employee_same_wdays;
			}else{
				$employee_same_courses = SchoolEmployee::find((int)$same_courses_or_null);
			}
			$employee_same_courses->courses = (array) MfkHelper::mfkIdsDecode($employee_same_courses->course_mfk);
			if(!empty($employee_same_courses->courses)){
				$query->where(function ($query) use($employee_same_courses) {
					$query->where(function ($query) use($employee_same_courses) {
						foreach ($employee_same_courses->courses as $item_course) {
							$query->where('school_employee.course_mfk', 'LIKE', '%,'.(int)$item_course.',%');
						}
					});
					$query->orWhereNull('school_employee.course_mfk');
            	});
			}
		}
		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('school_employee.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
		return $this->sendResponse($result, ['Form.SuccessGetData'], true, $total);
	}


	function filter_item_after_get(&$item){
		if(isset($item->country_id)) $item->country_id = (int) $item->country_id;
		if(isset($item->city_id)) $item->city_id = (int) $item->city_id;
		if(isset($item->rea_user_id)) $item->rea_user_id = (int) $item->rea_user_id;
		if(isset($item->school_stakeholder_id)) $item->school_stakeholder_id = (int) $item->school_stakeholder_id;

		$item->school_jobs = MfkHelper::mfkIdsDecode($item->school_job_mfk);
		$item->courses = MfkHelper::mfkIdsDecode($item->course_mfk);
		$item->wdays = MfkHelper::mfkIdsDecode($item->wday_mfk);
	}



	function fullinfos($school_employee_id = null){

		if(!$school_employee_id){
			$school_employee_id = ReaUserFactory::get_my_school_employee_id();
		}
		$query = SchoolEmployee::select([
			'school_employee.*'
		]);

		$current_school_id = ReaUserFactory::get_current_school_id();
		$current_school_year_id = ReaUserFactory::get_current_school_year_id();
		$current_sdepartment_id = ReaUserFactory::get_current_sdepartment_id();
		
		if(!$current_school_id){
			return $this->sendResponse(null, ['SchoolEmployee.CurrentSchoolNotSelected'], false);
		}

		$query->addSelect(\DB::raw("CONCAT(school_employee.firstname,' ',school_employee.f_firstname,' ',school_employee.lastname) as fullname"));

		$query->where('school_employee.school_id',(int)$current_school_id);
		$query->leftJoin('school','school.id','=','school_employee.school_id');
		$query->addSelect(['school.school_name_ar', 'school.school_name_en']);

		$query->leftJoin('sdepartment','sdepartment.id','=','school_employee.sdepartment_id');
		$query->addSelect(['sdepartment.sdepartment_name_ar', 'sdepartment.sdepartment_name_en']);

		$query->leftJoin('sdepartment as sdepartment_2','sdepartment_2.id','=','school_employee.sdepartment_2_id');
		$query->addSelect(['sdepartment_2.sdepartment_name_ar as sdepartment_2_name_ar', 'sdepartment_2.sdepartment_name_en as sdepartment_2_name_en']);

		$query->leftJoin('sdepartment as sdepartment_3','sdepartment_3.id','=','school_employee.sdepartment_3_id');
		$query->addSelect(['sdepartment_3.sdepartment_name_ar as sdepartment_3_name_ar', 'sdepartment_3.sdepartment_name_en as sdepartment_3_name_en']);
			

		$query->where('school_employee.id',$school_employee_id);
		$school_employee = $query->first();
		if(!$school_employee){
			return $this->sendResponse(null, ['SchoolEmployee.CantGetSchoolEmployee'], false);
		}

		if(isset($school_employee->rea_user_id) AND $school_employee->rea_user_id AND $current_school_year_id){
			$school_employee->sdepartments = Sdepartment::getSdepartmentsByUserId($school_employee->rea_user_id, $current_school_year_id);
		}
		
		
		if($current_school_year_id){
			$school_employee->school_class_courses = SchoolClassCourse::select([
				'school_class_course.id',
				'school_class_course.school_year_id',
				'school_class_course.level_class_id',
				'school_class_course.symbol',
				'school_class_course.course_id',
				'school_class_course.course_id',
				'course.course_name_ar',
				'course.course_name_en',
				'level_class.level_class_name_ar',
				'level_class.level_class_name_en',
				'school_year.school_year_name_ar',
				'school_year.school_year_name_en',
				])
			->where('school_class_course.school_year_id',$current_school_year_id)
			->join('level_class','level_class.id','=','school_class_course.level_class_id')
			->join('course','course.id','=','school_class_course.course_id')
			->join('school_year','school_year.id','=','school_class_course.school_year_id')
			->where('school_class_course.prof_id',$school_employee->id)
			->where('school_class_course.active','Y')
			->orderBy('school_class_course.level_class_id','ASC')
			->orderBy('school_class_course.symbol','ASC')
			->get();
		}

		
		$school_employee->current_school_id = $current_school_id;
		$school_employee->current_school_year_id = $current_school_year_id;
		$school_employee->current_sdepartment_id = $current_sdepartment_id;
		
		
		$day_template_id = null;
		$week_template_ids = (array) \DB::select(\DB::raw("SELECT wt.* FROM school_employee se
		JOIN sdepartment as sd on sd.id = se.sdepartment_id
		JOIN week_template as wt on wt.id = sd.week_template_id
		WHERE se.id = {$school_employee->id}
		AND sd.id = {$current_sdepartment_id}
		LIMIT 1"));
		if(!$week_template_ids OR !isset($week_template_ids[0]) ){
			$school_employee->schedules_errors = ["SchoolEmployee.NoWeekTemplateDefinedForThisSchoolDepartment"];
		}

		if( isset($week_template_ids[0]) AND (
			$week_template_ids[0]->day1_template_id != $week_template_ids[0]->day2_template_id
			OR $week_template_ids[0]->day1_template_id != $week_template_ids[0]->day3_template_id
			OR $week_template_ids[0]->day1_template_id != $week_template_ids[0]->day4_template_id
			OR $week_template_ids[0]->day1_template_id != $week_template_ids[0]->day5_template_id
			OR $week_template_ids[0]->day1_template_id != $week_template_ids[0]->day6_template_id
			OR $week_template_ids[0]->day1_template_id != $week_template_ids[0]->day7_template_id
			)
		){
			$school_employee->schedules_errors = ["SchoolEmployee.TheWeekTemplateDefinedFoThisSchoolDepartmentHaveDifferentDayTemplatesCalendarRetrieveForThisCaseIsNotImplemented"];
		}elseif(isset($week_template_ids[0]->day1_template_id)){
			$day_template_id = $week_template_ids[0]->day1_template_id;
		}
		$school_employee->schedules = null;

		if($day_template_id):
			$schedules = (array) \DB::select(\DB::raw("select
				concat({$school_employee->id},dti.session_order) as id, dti.session_order, dti.session_start_time, dti.session_end_time,
				psi1.id as psi_1, psi2.id as psi_2, psi3.id as psi_3, psi4.id as psi_4, psi5.id as psi_5, psi6.id as psi_6, psi7.id as psi_7,
				psi1.course_id as course_id_1, psi2.course_id as course_id_2, psi3.course_id as course_id_3, psi4.course_id as course_id_4, psi5.course_id as course_id_5, psi6.course_id as course_id_6, psi7.course_id as course_id_7,
				psi1.symbol as symbol_1, psi2.symbol as symbol_2, psi3.symbol as symbol_3, psi4.symbol as symbol_4, psi5.symbol as symbol_5, psi6.symbol as symbol_6, psi7.symbol as symbol_7,
				psi1.level_class_id as level_class_id_1, psi2.level_class_id as level_class_id_2, psi3.level_class_id as level_class_id_3, psi4.level_class_id as level_class_id_4, psi5.level_class_id as level_class_id_5, psi6.level_class_id as level_class_id_6, psi7.level_class_id as level_class_id_7
			from day_template_item dti
			left outer join prof_sched_item psi1
			    on psi1.school_year_id = {$current_school_year_id}
			    and psi1.prof_id = {$school_employee->id}
			    and psi1.wday_id = 1
			    and psi1.session_order = dti.session_order 
			left outer join prof_sched_item psi2 on psi2.school_year_id = {$current_school_year_id}
			    and psi2.prof_id = {$school_employee->id}
			    and psi2.wday_id = 2
			    and psi2.session_order = dti.session_order
			left outer join prof_sched_item psi3 on psi3.school_year_id = {$current_school_year_id}
			    and psi3.prof_id = {$school_employee->id}
			    and psi3.wday_id = 3
			    and psi3.session_order = dti.session_order  
			left outer join prof_sched_item psi4 on psi4.school_year_id = {$current_school_year_id}
			    and psi4.prof_id = {$school_employee->id}
			    and psi4.wday_id = 4
			    and psi4.session_order = dti.session_order  
			left outer join prof_sched_item psi5 on psi5.school_year_id = {$current_school_year_id}
			    and psi5.prof_id = {$school_employee->id}
			    and psi5.wday_id = 5
			    and psi5.session_order = dti.session_order  
			left outer join prof_sched_item psi6 on psi6.school_year_id = {$current_school_year_id}
			    and psi6.prof_id = {$school_employee->id}
			    and psi6.wday_id = 6
			    and psi6.session_order = dti.session_order  
			left outer join prof_sched_item psi7 on psi7.school_year_id = {$current_school_year_id}
			    and psi7.prof_id = {$school_employee->id}
			    and psi7.wday_id = 7
			    and psi7.session_order = dti.session_order
			    WHERE dti.day_template_id = {$day_template_id}
			    ") );

			$school_employee->schedules = $schedules;
			foreach($school_employee->schedules as $schedule){
				if(isset($schedule->id) AND $schedule->id) $schedule->id = (int)$schedule->id;
				if(isset($schedule->session_order) AND $schedule->session_order) $schedule->session_order = (int)$schedule->session_order;
				if(isset($schedule->psi_1) AND $schedule->psi_1) $schedule->psi_1 = (int)$schedule->psi_1;
				if(isset($schedule->psi_2) AND $schedule->psi_2) $schedule->psi_2 = (int)$schedule->psi_2;
				if(isset($schedule->psi_3) AND $schedule->psi_3) $schedule->psi_3= (int)$schedule->psi_3;
				if(isset($schedule->psi_4) AND $schedule->psi_4) $schedule->psi_4= (int)$schedule->psi_4;
				if(isset($schedule->psi_5) AND $schedule->psi_5) $schedule->psi_5= (int)$schedule->psi_5;
				if(isset($schedule->psi_6) AND $schedule->psi_6) $schedule->psi_6= (int)$schedule->psi_6;
				if(isset($schedule->psi_7) AND $schedule->psi_7) $schedule->psi_7= (int)$schedule->psi_7;

				if(isset($schedule->course_id_1) AND $schedule->course_id_1) $schedule->course_id_1 = (int)$schedule->course_id_1;
				if(isset($schedule->course_id_2) AND $schedule->course_id_2) $schedule->course_id_2 = (int)$schedule->course_id_2;
				if(isset($schedule->course_id_3) AND $schedule->course_id_3) $schedule->course_id_3= (int)$schedule->course_id_3;
				if(isset($schedule->course_id_4) AND $schedule->course_id_4) $schedule->course_id_4= (int)$schedule->course_id_4;
				if(isset($schedule->course_id_5) AND $schedule->course_id_5) $schedule->course_id_5= (int)$schedule->course_id_5;
				if(isset($schedule->course_id_6) AND $schedule->course_id_6) $schedule->course_id_6= (int)$schedule->course_id_6;
				if(isset($schedule->course_id_7) AND $schedule->course_id_7) $schedule->course_id_7= (int)$schedule->course_id_7;

				if(isset($schedule->symbol_1) AND $schedule->symbol_1) $schedule->symbol_1 = $schedule->symbol_1;
				if(isset($schedule->symbol_2) AND $schedule->symbol_2) $schedule->symbol_2 = $schedule->symbol_2;
				if(isset($schedule->symbol_3) AND $schedule->symbol_3) $schedule->symbol_3= $schedule->symbol_3;
				if(isset($schedule->symbol_4) AND $schedule->symbol_4) $schedule->symbol_4= $schedule->symbol_4;
				if(isset($schedule->symbol_5) AND $schedule->symbol_5) $schedule->symbol_5= $schedule->symbol_5;
				if(isset($schedule->symbol_6) AND $schedule->symbol_6) $schedule->symbol_6= $schedule->symbol_6;
				if(isset($schedule->symbol_7) AND $schedule->symbol_7) $schedule->symbol_7= $schedule->symbol_7;


				if(isset($schedule->level_class_id_1) AND $schedule->level_class_id_1) $schedule->level_class_id_1 = (int)$schedule->level_class_id_1;
				if(isset($schedule->level_class_id_2) AND $schedule->level_class_id_2) $schedule->level_class_id_2 = (int)$schedule->level_class_id_2;
				if(isset($schedule->level_class_id_3) AND $schedule->level_class_id_3) $schedule->level_class_id_3= (int)$schedule->level_class_id_3;
				if(isset($schedule->level_class_id_4) AND $schedule->level_class_id_4) $schedule->level_class_id_4= (int)$schedule->level_class_id_4;
				if(isset($schedule->level_class_id_5) AND $schedule->level_class_id_5) $schedule->level_class_id_5= (int)$schedule->level_class_id_5;
				if(isset($schedule->level_class_id_6) AND $schedule->level_class_id_6) $schedule->level_class_id_6= (int)$schedule->level_class_id_6;
				if(isset($schedule->level_class_id_7) AND $schedule->level_class_id_7) $schedule->level_class_id_7= (int)$schedule->level_class_id_7;
			}
		endif;

		
		$this->filter_item_after_get($school_employee);
		return $this->sendResponse($school_employee, ['Global.GetDataWithSuccess']);
	}



	public function save(Request $request)
	{

		$current_school_id = ReaUserFactory::get_current_school_id();

		if(!$current_school_id){
			return $this->sendResponse(null, ['School.CurrentSchoolNotSelected'], false);
		}


		$data = $request->get('school_employee');
		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}



		if( isset($data['new']) AND $data['new']==true ){
			$employee = new Employee;
			$employee->active = 'Y';
			$school = School::select('school.stakeholder_id')->where('school.id',$current_school_id)->first();
			if(!$school OR !$school->stakeholder_id){
				return $this->sendResponse(null, ['School.SchoolDontHaveStakeHolder'], false);
			}
			$sdepartment = Sdepartment::select('sdepartment.stakeholder_id')->where('sdepartment.id',(int)$data['sdepartment_id'])->first();
			if(!$sdepartment OR !$sdepartment->stakeholder_id ){
				return $this->sendResponse(null, ['Sdepartment.SchoolDepartmentDontHaveStakeHolder'], false);
			}
			$employee->id_sh_org = $school->stakeholder_id;
			$employee->id_sh_div = $sdepartment->stakeholder_id;
			$employee->save();
		}else{
			$employee = Employee::find($data['id']);
		}

		
		if(!$employee){
			return $this->sendResponse(null, ['SchoolEmployee.ErrorToGetSchoolEmployee'], false);
		}

		if(isset($data['gender_id'])) $employee->gender_id = $data['gender_id'];
		if(isset($data['firstname'])) $employee->firstname = $data['firstname'];
		if(isset($data['f_firstname'])) $employee->f_firstname= $data['f_firstname'];
		if(isset($data['g_f_firstname'])) $employee->g_f_firstname= $data['g_f_firstname'];
		if(isset($data['lastname'])) $employee->lastname = $data['lastname'];
		if(isset($data['birth_date'])) $employee->birth_date = $data['birth_date'];
		if(isset($data['country_id'])) $employee->country_id = $data['country_id'];
		if(isset($data['city_id'])) $employee->city_id = $data['city_id'];
		if(isset($data['address'])) $employee->address = $data['address'];
		if(isset($data['mobile'])) $employee->mobile = $data['mobile'];
		if(isset($data['phone'])) $employee->phone = $data['phone'];
		if(isset($data['email'])) $employee->email = $data['email'];
		if(isset($data['job_description'])) $employee->job = $data['job_description'];
		if(isset($data['school_job_mfk'])) $employee->jobrole_mfk = $data['school_job_mfk'];
		$employee->save();


		$sprof = SProf::find($employee->id);
		if(!$sprof){
			$sprof = new SProf;
			$sprof->active = 'Y';
			$sprof->id = $employee->id;
		}

		if(isset($data['course_mfk'])) $sprof->course_mfk = $data['course_mfk'];
		if(isset($data['wday_mfk'])) $sprof->wday_mfk = $data['wday_mfk'];
		$sprof->save();

		return $this->sendResponse([$employee->id], ['Form.DataSavedWithSuccess']);
	}




	public function delete(Request $request)
	{
		$school_employee_id = $request->get('school_employee_id');

		if(!$school_employee_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = SchoolEmployee::destroy($school_employee_id);
		if(!$destroy){
			return $this->sendResponse(null, ['SchoolEmployee.InvalideSchoolEmployee'], false);
		}

		return $this->sendResponse(null, ['Form.DataDeletedWithSuccess']);
	}



	public function replace(Request $request)
	{

		$this->school_id = ReaUserFactory::get_current_school_id();
		$this->group_num = ReaUserFactory::get_group_num();
		if(!$this->school_id OR !$this->group_num){
			return $this->sendResponse(null, ['SchoolEmployee.CurrentSchoolNotSelected'], false);
		}

		$school_employee_id = $request->get('school_employee_id');
		$school_employee_to_replace_id = $request->get('school_employee_to_replace_id');
		$cancel_employee_account = $request->get('cancel_employee_account');

		if(!$school_employee_id OR !$school_employee_to_replace_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$school_employee = SchoolEmployee::find($school_employee_id);
		$school_employee_to_replace = SchoolEmployee::find($school_employee_to_replace_id);
		if(!$school_employee OR !$school_employee_to_replace){
			return $this->sendResponse(null, ['SchoolEmployee.SchoolEmployeeNotExists'], false);
		}

		if(SchoolClassCourse::where('prof_id', $school_employee_to_replace->id)->count()){
			return $this->sendResponse(null, ['SchoolEmployee.NewSchoolEmployeeAlreadyHaveTask'], false);
		}


		

		$courses_ok = false;
		$days_ok = false;
		if($school_employee_to_replace->course_mfk==null){
			$school_employee_to_replace->course_mfk = $school_employee->course_mfk;
			//$school_employee_to_replace->save();
		}

		if($school_employee_to_replace->wday_mfk==null){
			$school_employee_to_replace->wday_mfk = $school_employee->wday_mfk;
			//$school_employee_to_replace->save();
		}
		if($school_employee_to_replace->course_mfk==$school_employee->course_mfk){
			$courses_ok = true;
		}
		if($school_employee_to_replace->wdays_mfk==$school_employee->wdays_mfk){
			$wdays_ok = true;
		}

		$school_employee->wdays = ($school_employee->wday_mfk)? (array) MfkHelper::mfkIdsDecode($school_employee->wday_mfk) : array();
		$school_employee_to_replace->wdays = ($school_employee_to_replace->wday_mfk)? (array) MfkHelper::mfkIdsDecode($school_employee_to_replace->wday_mfk) : array();
		$school_employee->courses =  ($school_employee->course_mfk)? (array) MfkHelper::mfkIdsDecode($school_employee->course_mfk) : array();
		$school_employee_to_replace->courses = ($school_employee_to_replace->course_mfk)? (array) MfkHelper::mfkIdsDecode($school_employee_to_replace->course_mfk) : array();
		
		if(!$courses_ok){
			foreach ($school_employee->courses as $course_item) {
				if(!in_array($course_item, $school_employee_to_replace->courses)){
					return $this->sendResponse(null, ['SchoolEmployee.TheNewTeacherHoldsSettingsAreNotCommensurateWithTheCurrentProcessOfReceiptOfTheEmployeesDutiesFirstModifyThemAndThenTryAgain'], false);
				}
			}
		}
		if(!$wdays_ok){
			foreach ($school_employee->wdays as $wday_item) {
				if(!in_array($wday_item, $school_employee_to_replace->wdays)){
					return $this->sendResponse(null, ['SchoolEmployee.TheNewTeacherHoldsSettingsAreNotCommensurateWithTheCurrentProcessOfReceiptOfTheEmployeesDutiesFirstModifyThemAndThenTryAgain'], false);
				}
			}
		}


		SchoolClassCourse::where('prof_id', $school_employee->id)
		->update([
			'prof_id' => $school_employee_to_replace->id
		]);

		$h_current_day = \App\Helpers\HijriDateHelper::to_hijri(date('Ymd'));
		CourseSession::where('group_num',(int)$this->group_num)
		->where('school_id',(int)$this->school_id)
		->where('prof_id',$school_employee->id)
		->where('session_hdate','>=',$h_current_day)
		->update([
			'prof_id' => $school_employee_to_replace
		]);

	      if($cancel_employee_account AND $school_employee->rea_user_id){
	      	$rea_user = ReaUser::find($school_employee->rea_user_id);
	      	if($rea_user){
	      		$rea_user->active = 'N';
				$rea_user->save();
	      	}
	    	$school_employee->active = 'N';
			$school_employee->save();
	      }

		return $this->sendResponse(null, ['Form.DataSavedWithSuccess']);
	}


}
