<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolJobRepository;
use App\Models\SchoolJob;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SchoolJobAPIController extends AppBaseController
{
	/** @var  SchoolJobRepository */
	private $schoolJobRepository;

	function __construct(SchoolJobRepository $schoolJobRepo)
	{
		$this->schoolJobRepository = $schoolJobRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = SchoolJob::select([
            'school_job.*'
        ]);

        if(isset($school_job_id)){
            $query->where('school_job.id',$school_job_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('school_job.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('school_job.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }

        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }


    function filter_item_after_get(&$item){
    }



    public function save(Request $request)
    {
        $data = $request->get('school_job');

        if(!$data){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $school_job = new SchoolJob;
        }else{
            $school_job = SchoolJob::find($data['id']);
        }


        if(!$school_job){
            return $this->sendResponse(null, ['SchoolJob.InvalideSchoolJob'], false);
        }

        if(isset($data['active'])) $school_job->active = $data['active'];
        if(isset($data['school_job_name_ar'])) $school_job->school_job_name_ar = $data['school_job_name_ar'];
        if(isset($data['school_job_name_en'])) $school_job->school_job_name_en = $data['school_job_name_en'];
        if(isset($data['id_domain'])) $school_job->id_domain = $data['id_domain'];
        $school_job->save();

        return $this->sendResponse($school_job->id, ['Form.DataSavedWithSuccess'], true);
    }


    public function delete(Request $request)
    {
        $school_job_id = $request->get('school_job_id');

        if(!$school_job_id){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $destroy = SchoolJob::destroy($school_job_id);
        if(!$destroy){
            return $this->sendResponse(null, ['SchoolJob.InvalideSchoolJob'], false);
        }

        return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
    }
}
