<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolLevelRepository;
use App\Models\SchoolLevel;
use App\Models\SchoolClass;
use App\Models\School;
use App\Models\ReaUser;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use App\Libraries\Repositories\ReaUserRepository;
use App\Helpers\MfkHelper;
use App\Libraries\Factories\ReaUserFactory;

class SchoolLevelAPIController extends AppBaseController
{
	/** @var  SchoolLevelRepository */
	private $schoolLevelRepository;

	function __construct(SchoolLevelRepository $schoolLevelRepo)
	{
		parent::__construct();
		$this->schoolLevelRepository = $schoolLevelRepo;
	}



	/**
	*	Created by Achraf
	*	Created at 07/03/2016
	*	Function key: BF264 / Q0008
	*/
	public function getAllSchoolLevelsBySchoolYearID($school_year_id)
	{
		$user_lang = app()->getLocale();

		$result = SchoolClass::select('level_class.id','level_class.level_class_name_'.$user_lang.' as level_class_name')
		->join('level_class','level_class.id','=','school_class.level_class_id')
			->where('school_class.active','=',"Y")
			->where('school_class.school_year_id',"=",$school_year_id)
			->groupBy('level_class.id')
			->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('school_year.errors.empty_school_levels'), false);	
		}

		return $this->sendResponse($result, Lang::get('school_year.success.valide_school_levels'), true);	
	}
	/**
	 * Created at 21-04-2016
	 * Created by Aziz
	 * Display a listing of the SchoolLevel by listByCoursesconfigTemplateId.
	 * GET|HEAD /SchoolLevel/listByCoursesconfigTemplateId/{courses_config_template_id}
	 * Q0018
	 * @return Response
	 */
	public function listByCoursesConfigTemplateId($courses_config_template_id)
	{
		$result = SchoolLevel::select('school_level.id,school_level_name_ar,school_level_name_en')
					->join('levels_template','levels_template.id','=','school_level.levels_template_id')
					->join('courses_config_template','courses_config_template.levels_template_id','=','levels_template.id')
					->where('courses_config_template.id','=',$courses_config_template_id)
					->get();

		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.no_results'), false);
		}

		return $this->sendResponse($result, Lang::get('global.errors.results_exist'), true);
	}


	








	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = SchoolLevel::select([
			'school_level.*'
		]);

		if(isset($school_level_id)){
			$query->where('school_level.id',$school_level_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('school_level.active',"Y");
		}

		if( isset($levels_template_id) ){
			$query->where('school_level.levels_template_id',$levels_template_id);
		}

		if( isset($current_school_levels) ){
			$current_school_id = ReaUserFactory::get_current_school_id();
			$school = School::select('school_level_mfk')->where('id',$current_school_id)->first();
			$current_school_levels = [];
			if( isset($school->school_level_mfk) ){
				$current_school_levels = MfkHelper::mfkIdsDecode($school->school_level_mfk);
			}
			$query->whereIn('school_level.id',$current_school_levels);
		}
		

		/* Used in Q0018 */
		if( isset($courses_config_template_id) ){
			$query->join('levels_template', 'levels_template.id', '=', 'school_level.levels_template_id');
			$query->join('courses_config_template', 'courses_config_template.levels_template_id', '=', 'levels_template.id');
			$query->where('courses_config_template.id',(int)$courses_config_template_id);
		}



		/* Used in Q1053 */
		if( isset($school_id) ){
			$school = School::find($school_id);
			if( !isset($school->courses_config_template_id) ){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$query->join('levels_template', 'levels_template.id', '=', 'school_level.levels_template_id');
			$query->join('courses_config_template', 'courses_config_template.levels_template_id', '=', 'levels_template.id');
			$query->where('courses_config_template.id',(int)$school->courses_config_template_id);
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('school_level.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		$item->levels_template_id = (int)$item->levels_template_id;
	}




	public function save(Request $request)
	{
		$data = $request->get('school_level');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$school_level = new SchoolLevel;
		}else{
			$school_level = SchoolLevel::find($data['id']);	
		}

		
		if(!$school_level){
			return $this->sendResponse(null, ['SchoolLevel.InvalideSchoolLevel'], false);
		}

		if(isset($data['active'])) $school_level->active = $data['active'];
		if(isset($data['school_level_name_ar'])) $school_level->school_level_name_ar = $data['school_level_name_ar'];
		if(isset($data['school_level_name_en'])) $school_level->school_level_name_en = $data['school_level_name_en'];
		if(isset($data['levels_template_id'])) $school_level->levels_template_id = $data['levels_template_id'];
		
		$school_level->save();

		return $this->sendResponse($school_level->id, ['Form.DataSavedWithSuccess'], true);
	}






	public function delete(Request $request)
	{
		$school_level_id = $request->get('school_level_id');

		if(!$school_level_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = SchoolLevel::destroy($school_level_id);
		if(!$destroy){
			return $this->sendResponse(null, ['SchoolLevel.InvalideSchoolLevel'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess']);
	}


}
