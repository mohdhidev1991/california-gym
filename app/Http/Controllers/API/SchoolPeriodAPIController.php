<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolPeriodRepository;
use App\Models\SchoolPeriod;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SchoolPeriodAPIController extends AppBaseController
{
	/** @var  SchoolPeriodRepository */
	private $attendanceStatusRepository;

	function __construct(SchoolPeriodRepository $attendanceStatusRepo)
	{
        parent::__construct();
		$this->attendanceStatusRepository = $attendanceStatusRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = SchoolPeriod::select([
            'school_period.*'
        ]);

        if(isset($school_period_id)){
            $query->where('school_period.id',$school_period_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('school_period.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('school_period.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }

        foreach ($result as $item) {
        	$this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['school_periods' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }

    function filter_item_after_get(&$item){
    	if($item->capacity) $item->capacity = (int)$item->capacity;
    	if($item->period_id) $item->period_id = (int)$item->period_id;
    	if($item->school_id) $item->school_id = (int)$item->school_id;
    }


    public function save(Request $request)
    {
        $data = $request->get('school_period');

        if(!$data){
            return $this->sendResponse(null, Lang::get('school_period.errors.empty_school_period_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $school_period = new SchoolPeriod;
        }else{
            $school_period = SchoolPeriod::find($data['id']);
        }


        if(!$school_period){
            return $this->sendResponse(null, Lang::get('school_period.errors.school_period_not_existe'), false);
        }

        if(isset($data['active'])) $school_period->active = $data['active'];
        if(isset($data['school_id'])) $school_period->school_id = $data['school_id'];
        if(isset($data['period_id'])) $school_period->period_id = $data['period_id'];
        if(isset($data['period_name_ar'])) $school_period->period_name_ar = $data['period_name_ar'];
        if(isset($data['period_name_en'])) $school_period->period_name_en = $data['period_name_en'];
        if(isset($data['capacity'])) $school_period->capacity = $data['capacity'];
        if(isset($data['door_open_time'])) $school_period->door_open_time = $data['door_open_time'];
        if(isset($data['door_close_time'])) $school_period->door_close_time = $data['door_close_time'];
        if(isset($data['start_exit_time'])) $school_period->start_exit_time = $data['start_exit_time'];
        if(isset($data['end_coming_time'])) $school_period->end_coming_time = $data['end_coming_time'];
        $school_period->save();

        return $this->sendResponse($school_period, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $school_period_id = $request->get('school_period_id');

        if(!$school_period_id){
            return $this->sendResponse(null, Lang::get('school_period.errors.empty_school_period_id'), false);
        }

        $destroy = SchoolPeriod::destroy($school_period_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('school_period.errors.school_period_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
