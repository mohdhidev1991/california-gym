<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolRegistrationRepository;
use App\Models\SchoolRegistration;
use App\Models\School;
use App\Models\SchoolEmployee;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;
use Illuminate\Support\Facades\Validator;

class SchoolRegistrationAPIController extends AppBaseController
{
	/** @var  SchoolRegistrationRepository */
	private $school_registrationRepository;

	function __construct(SchoolRegistrationRepository $school_registrationRepo)
	{
		parent::__construct();
		$this->school_registrationRepository = $school_registrationRepo;
	}



	/**
	* Crud methods
	**/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$query = SchoolRegistration::select([
			'school_registration.*', 'school.school_name_ar', 'school.school_name_en'
		]);
		$query->leftJoin('school', 'school.id', '=', 'school_registration.school_id');

		if(isset($school_registration_id)){
			$query->where('school_registration.id',$school_registration_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		
		if(!isset($active) OR ($active!='all') ){
			$query->where('school_registration.active',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		//$query->orderBy('school_registration.school_registration_name', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}
		foreach ($result as &$item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['school_registrations' => $result, 'total' => $count];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}

	function filter_item_after_get(&$school_registration){
		if( isset($school_registration->city_id)) $school_registration->city_id = (int) $school_registration->city_id;
		if( isset($school_registration->school_id)) $school_registration->school_id = (int) $school_registration->school_id;
	}








	public function save(Request $request)
	{
		$data = $request->get('school_registration');

		if(!$data){
			return $this->sendResponse(null, Lang::get('school_registration.errors.empty_school_registration_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$school_registration = new SchoolRegistration;
		}else{
			$school_registration = SchoolRegistration::find($data['id']);	
		}

		
		if(!$school_registration){
			return $this->sendResponse(null, Lang::get('school_registration.errors.school_registration_not_existe'), false);
		}

		if(isset($data['active'])) $school_registration->active = $data['active'];
		if(isset($data['school_name'])) $school_registration->school_name = $data['school_name'];
		if(isset($data['firstname'])) $school_registration->firstname = $data['firstname'];
		if(isset($data['lastname'])) $school_registration->lastname = $data['lastname'];
		if(isset($data['email'])) $school_registration->email = $data['email'];
		if(isset($data['mobile'])) $school_registration->mobile = $data['mobile'];
		if(isset($data['city_id'])) $school_registration->city_id = $data['city_id'];
		if(isset($data['school_id'])) $school_registration->school_id = $data['school_id'];
		if(isset($data['school_registration'])) $school_registration->school_registration = $data['school_registration'];

		$school_registration->save();

		return $this->sendResponse($school_registration, Lang::get('global.success.save'), true);
	}




	public function submit(Request $request)
	{
		$data_request = $request->get('school_registration');

		$v = Validator::make($data_request, [
            'email' => 'required|email',
            'school_name' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'mobile' => 'required|numeric',
            'city' => 'required',
        ]);

		if ( $v->fails() )
        {   
            return $this->sendResponse(['success' => false, 'errors' => $v->messages()], Lang::get('school_registration.error.invlide_data'), false);
        }

		$school_registration = new SchoolRegistration;
		$school_registration->active = 'Y';
		if(isset($data_request['school_name'])) $school_registration->school_name = $data_request['school_name'];
		if(isset($data_request['firstname'])) $school_registration->firstname = $data_request['firstname'];
		if(isset($data_request['lastname'])) $school_registration->lastname = $data_request['lastname'];
		if(isset($data_request['email'])) $school_registration->email = $data_request['email'];
		if(isset($data_request['mobile'])) $school_registration->mobile = $data_request['mobile'];
		if(isset($data_request['city'])) $school_registration->city = $data_request['city'];


		$school_registration->save();
		$this->school_registrationRepository->send_submit_to_email_admin($school_registration);
		return $this->sendResponse(['success' => true ], Lang::get('global.success.submit'), true);
	}




	public function delete(Request $request)
	{
		$school_registration_id = $request->get('school_registration_id');

		if(!$school_registration_id){
			return $this->sendResponse(null, Lang::get('school_registration.errors.empty_school_registration_id'), false);
		}

		$destroy = SchoolRegistration::destroy($school_registration_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('school_registration.errors.school_registration_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}



	public function convert_to_school(Request $request)
	{
		$school_registration_id = $request->get('school_registration_id');

		if(!$school_registration_id){
			return $this->sendResponse(['success' => false, 'errors' => ['Global.EmptyData'] ], Lang::get('school_registration.errors.empty_school_registration_id'), false);
		}

		$school_registration = SchoolRegistration::find($school_registration_id);

		if(!$school_registration ){
			return $this->sendResponse(['success' => false, 'errors' => ['SchoolRegistration.SchoolRegistrationNotExists'] ], Lang::get('school_registration.errors.school_registration_not_exists'), false);
		}

		if($school_registration->school_id){
			return $this->sendResponse(['success' => false, 'errors' => ['SchoolRegistration.ThisSubmitAlreadyConverted'] ], Lang::get('school_registration.errors.school_registration_not_existe'), false);
		}

		$school = new School;
		$school->active = 'Y';
		$school->city_id = $school_registration->city_id;
		$school->school_name_ar = $school_registration->school_name;
		$school->save();


		$school_employee = new SchoolEmployee;
		$school_employee->active = 'Y';
		$school_employee->school_id = $school->id;
		$school_employee->email = $school_registration->email;
		$school_employee->firstname = $school_registration->firstname;
		$school_employee->lastname = $school_registration->lastname;
		$school_employee->save();


		return $this->sendResponse(['success' => true, 'school'=>$new_school,'school_employee' => $school_employee], Lang::get('global.success.convert_to_school'), true);
	}

}
