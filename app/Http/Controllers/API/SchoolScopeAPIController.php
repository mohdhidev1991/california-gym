<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolScopeRepository;
use App\Models\SchoolScope;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use App\Helpers\HijriDateHelper;

class SchoolScopeAPIController extends AppBaseController
{
    /** @var  SchoolScopeRepository */
    private $schoolScopeRepository;

    function __construct(SchoolScopeRepository $schoolScopeRepo)
    {
        $this->schoolScopeRepository = $schoolScopeRepo;
    }

    /**
     * Display a listing of the SchoolScope.
     * GET|HEAD /schoolScopes
     *
     * @return Response
     */
    public function index()
    {
        $schoolScopes = $this->schoolScopeRepository->all();

        return $this->sendResponse($schoolScopes->toArray(), "SchoolScopes retrieved successfully");
    }

    /**
     * Show the form for creating a new SchoolScope.
     * GET|HEAD /schoolScopes/create
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created SchoolScope in storage.
     * POST /schoolScopes
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if(sizeof(SchoolScope::$rules) > 0)
            $this->validateRequestOrFail($request, SchoolScope::$rules);

        $input = $request->all();

        $schoolScopes = $this->schoolScopeRepository->create($input);

        return $this->sendResponse($schoolScopes->toArray(), "SchoolScope saved successfully");
    }

    /**
     * Display the specified SchoolScope.
     * GET|HEAD /schoolScopes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $schoolScope = $this->schoolScopeRepository->apiFindOrFail($id);

        return $this->sendResponse($schoolScope->toArray(), "SchoolScope retrieved successfully");
    }

    /**
     * Show the form for editing the specified SchoolScope.
     * GET|HEAD /schoolScopes/{id}/edit
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
    }

    /**
     * Update the specified SchoolScope in storage.
     * PUT/PATCH /schoolScopes/{id}
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var SchoolScope $schoolScope */
        $schoolScope = $this->schoolScopeRepository->apiFindOrFail($id);

        $result = $this->schoolScopeRepository->updateRich($input, $id);

        $schoolScope = $schoolScope->fresh();

        return $this->sendResponse($schoolScope->toArray(), "SchoolScope updated successfully");
    }

    /**
     * Remove the specified SchoolScope from storage.
     * DELETE /schoolScopes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->schoolScopeRepository->apiDeleteOrFail($id);

        return $this->sendResponse($id, "SchoolScope deleted successfully");
    }

}




