<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolTermRepository;
use App\Models\SchoolTerm;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SchoolTermAPIController extends AppBaseController
{
	/** @var  SchoolTermRepository */
	private $schoolTermRepository;

	function __construct(SchoolTermRepository $schoolTermRepository)
	{
        parent::__construct();
		$this->schoolTermRepository = $schoolTermRepository;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = SchoolTerm::select([
            'school_term.*'
        ]);

        if(isset($school_term_id)){
            $query->where('school_term.id',$school_term_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('school_term.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('school_term.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }

        foreach ($result as $item) {
        	$this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['school_terms' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }

    function filter_item_after_get(&$item){
    	if($item->capacity) $item->capacity = (int)$item->capacity;
    	if($item->term_id) $item->term_id = (int)$item->term_id;
    	if($item->school_id) $item->school_id = (int)$item->school_id;
    }


    public function save(Request $request)
    {
        $data = $request->get('school_term');

        if(!$data){
            return $this->sendResponse(null, Lang::get('school_term.errors.empty_school_term_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $school_term = new SchoolTerm;
        }else{
            $school_term = SchoolTerm::find($data['id']);
        }


        if(!$school_term){
            return $this->sendResponse(null, Lang::get('school_term.errors.school_term_not_existe'), false);
        }

        if(isset($data['active'])) $school_term->active = $data['active'];
        if(isset($data['school_id'])) $school_term->school_id = $data['school_id'];
        if(isset($data['end_study_hdate'])) $school_term->end_study_hdate = $data['end_study_hdate'];
        if(isset($data['end_vacancy_hdate'])) $school_term->end_vacancy_hdate = $data['end_vacancy_hdate'];
        if(isset($data['start_study_hdate'])) $school_term->start_study_hdate = $data['start_study_hdate'];
        if(isset($data['start_vacancy_hdate'])) $school_term->start_vacancy_hdate = $data['start_vacancy_hdate'];
        if(isset($data['school_term_name_ar'])) $school_term->school_term_name_ar = $data['school_term_name_ar'];
        if(isset($data['school_term_name_en'])) $school_term->school_term_name_en = $data['school_term_name_en'];
        $school_term->save();

        return $this->sendResponse($school_term, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $school_term_id = $request->get('school_term_id');

        if(!$school_term_id){
            return $this->sendResponse(null, Lang::get('school_term.errors.empty_school_term_id'), false);
        }

        $destroy = SchoolTerm::destroy($school_term_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('school_term.errors.school_term_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
