<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolTypeRepository;
use App\Models\SchoolType;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SchoolTypeAPIController extends AppBaseController
{
	/** @var  SchoolTypeRepository */
	private $schoolTypeRepository;

	function __construct(SchoolTypeRepository $schoolTypeRepo)
	{
        parent::__construct();
		$this->schoolTypeRepository = $schoolTypeRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = SchoolType::select([
            'school_type.*'
        ]);

        if(isset($school_type_id)){
            $query->where('school_type.id',$school_type_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('school_type.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('school_type.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        foreach($result as $item){
            $this->filter_item_after_get($item);
        }
        $count = $query->count();
        $return = ['school_types' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }



    function filter_item_after_get(&$item){
    }




    public function save(Request $request)
    {
        $data = $request->get('school_type');

        if(!$data){
            return $this->sendResponse(null, Lang::get('school_type.errors.empty_school_type_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $school_type = new SchoolType;
        }else{
            $school_type = SchoolType::find($data['id']);
        }


        if(!$school_type){
            return $this->sendResponse(null, Lang::get('school_type.errors.school_type_not_existe'), false);
        }

        if(isset($data['active'])) $school_type->active = $data['active'];
        if(isset($data['lookup_code'])) $school_type->lookup_code = $data['lookup_code'];
        if(isset($data['school_type_name_ar'])) $school_type->school_type_name_ar = $data['school_type_name_ar'];
        if(isset($data['school_type_name_en'])) $school_type->school_type_name_en = $data['school_type_name_en'];
        if(isset($data['tservice_id'])) $school_type->tservice_id = $data['tservice_id'];
        $school_type->save();

        return $this->sendResponse($school_type, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $school_type_id = $request->get('school_type_id');

        if(!$school_type_id){
            return $this->sendResponse(null, Lang::get('school_type.errors.empty_school_type_id'), false);
        }

        $destroy = SchoolType::destroy($school_type_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('school_type.errors.school_type_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
