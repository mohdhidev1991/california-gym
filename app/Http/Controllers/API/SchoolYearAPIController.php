<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SchoolYearRepository;
use App\Models\SchoolYear;
use App\Models\SchoolScope;
use App\Models\SchoolClass;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use App\Helpers\HijriDateHelper;
use App\Libraries\Factories\ReaUserFactory;

class SchoolYearAPIController extends AppBaseController
{
	/** @var  SchoolYearRepository */
	private $schoolYearRepository;

	function __construct(SchoolYearRepository $schoolYearRepo)
	{
		parent::__construct();
		$this->schoolYearRepository = $schoolYearRepo;
	}

	


	/**
	*	Created by Achraf
	*	Created at 06/03/2016
	*	Function key: BF264 / Q0007
	*/
	public function getAllSchoolYearsBySchoolID($school_id)
	{
		$current_day = HijriDateHelper::getHdateByFormat(date('Ymd'));
		$result = SchoolYear::select('school_year.id','school_year.school_year_name_ar','school_year.school_year_name_en')
		->where('school_id',$school_id)
		->where('school_year_end_hdate', '>=', $current_day)
		->get();
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		return $this->sendResponse($result, ['Global.GetDataWithSuccess']);
	}







	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = SchoolYear::select([
			'school_year.*'
		]);

		if(isset($school_year_id)){
			$query->where('school_year.id',$school_year_id);
			

			if(isset($fullinfos)){
				$query->leftJoin('school', 'school.id', '=', 'school_year.school_id');
				$query->addSelect([
					'school.school_name_ar'
				]);
			}

			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}

			$this->filter_item_after_get($single_item);

			if(isset($fullinfos)){
				$this->fullinfos_fields($single_item);
			}

			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}


		if(isset($status)){
			$active = $status;
		}
		if( isset($active) AND in_array($active,['Y','N','all']) ){
			if( in_array($active,['Y','N']) ){
				$query->where('school_year.active',$active);
			}
		}else{
			$query->where('school_year.active',"Y");
		}

		if( isset($school_id) OR isset($current_school_id) ){
			if(isset($current_school_id)) $school_id = (int)ReaUserFactory::get_current_school_id();
			$current_day = HijriDateHelper::getHdateByFormat(date('Ymd'));
			$query->where('school_year.school_id',$school_id);
			$query->where('school_year.school_year_end_hdate', '>=', $current_day);
		}


		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}


		$_order_by = 'school_year.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','school_year_name_ar','school_year_name_en', 'school_year_end_hdate']) ){
			$_order_by = 'school_year.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);
		$result = $query->get();

		if( isset($export) ){
			\App\Helpers\ExcelHelper::export_model($result->toArray(),'school_years',SchoolYear::getExportedData());
		}
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		$total = $query->count();
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$school_year){
		//
	}


	function fullinfos_fields(&$school_year){

		$school_year->school_scopes = SchoolScope::select([
				'school_scope.id',
				'school_scope.class_nb',
				'school_level.school_level_name_ar',
				'level_class.level_class_name_ar',
				'sdepartment.sdepartment_name',
				])
			->leftJoin('school_level','school_level.id', '=' ,'school_scope.school_level_id')
			->leftJoin('level_class','level_class.id', '=' ,'school_scope.level_class_id')
			->leftJoin('sdepartment','sdepartment.id', '=' ,'school_scope.sdepartment_id')
			->where('school_scope.school_year_id', $school_year->id)->get();


		$school_year->school_classes = SchoolClass::select([
				'school_class.symbol',
				'level_class.level_class_name_ar',
				'room.room_name',
				'room.capacity',
				])
			->leftJoin('level_class','level_class.id', '=' ,'school_class.level_class_id')
			->leftJoin('room','room.id', '=' ,'school_class.room_id')
			->where('school_class.school_year_id', $school_year->id)->get();
	}


	public function save(Request $request)
	{
		$data = $request->get('school_year');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$school_year = new SchoolYear;
		}else{
			$school_year = SchoolYear::find($data['id']);	
		}

		
		if(!$school_year){
			return $this->sendResponse(null, ['FeedImport.InvalideSchoolYear'], false);
		}

		if(isset($data['active'])) $school_year->active = $data['active'];
		if(isset($data['school_id'])) $school_year->school_id = $data['school_id'];
		if(isset($data['school_year_name_ar'])) $school_year->school_year_name_ar = $data['school_year_name_ar'];
		if(isset($data['school_year_name_en'])) $school_year->school_year_name_en = $data['school_year_name_en'];
		if(isset($data['school_year_start_hdate'])) $school_year->school_year_start_hdate = $data['school_year_start_hdate'];
		if(isset($data['school_year_end_hdate'])) $school_year->school_year_end_hdate = $data['school_year_end_hdate'];
		if(isset($data['year'])) $school_year->year = $data['year'];
		
		$school_year->save();

		return $this->sendResponse($school_year->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$school_year_id = $request->get('school_year_id');

		if(!$school_year_id){
			return $this->sendResponse(null, Lang::get('school_year.errors.empty_school_year_id'), false);
		}

		$destroy = SchoolYear::destroy($school_year_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('school_year.errors.school_year_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}



}




