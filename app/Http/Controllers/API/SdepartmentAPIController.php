<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SdepartmentRepository;
use App\Libraries\Factories\ReaUserFactory;
use App\Models\Sdepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SdepartmentAPIController extends AppBaseController
{
	/** @var  SdepartmentRepository */
	private $sdepartmentRepository;

	function __construct(SdepartmentRepository $sdepartmentRepo)
	{
		parent::__construct();
		$this->sdepartmentRepository = $sdepartmentRepo;
	}
	/**
	 * Q0016
	 * Display a listing of the Sdepartment for a school.
	 * GET|HEAD /sdepartments/school_id/{school_id}
	 *
	 * @return Response
	 */
	public function listBySchoolId($school_id)
	{
		$result = Sdepartment::where('school_id','=',$school_id)->get();
		if($result)
			$this->sendResponse($result,'global.success.results_exist');
		else
			$this->sendResponse(null,'global.errors.no_results');
	}

	


	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Sdepartment::select([
			'sdepartment.*'
		]);

		if(isset($sdepartment_id)){
			$query->where('sdepartment.id',$sdepartment_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess'], true);
		}


		

		if( isset($join) AND !empty($join) ){
			$join = explode('!', $join);
		}else{
			$join = [];
		}
		if( in_array('school',$join) or in_array('all',$join) ){
			$query->leftJoin('school','school.id','=','sdepartment.school_id');
			$query->addSelect(['school.school_name_ar','school.school_name_en']);
		}
		if( in_array('week_template',$join) or in_array('all',$join) ){
			$query->leftJoin('week_template','week_template.id','=','sdepartment.week_template_id');
			$query->addSelect(['week_template.week_template_name_ar','week_template.week_template_name_en']);
		}

		if(isset($current_school)){
			$current_school_id = ReaUserFactory::get_current_school_id();
			$query->where('sdepartment.school_id','=',$current_school_id)->get();
		}

		if(isset($school_id)){
			$query->where('sdepartment.school_id','=',$school_id)->get();
		}

		if(isset($week_template_id)){
			$query->where('sdepartment.week_template_id','=',$week_template_id)->get();
		}

		if(isset($name_or_id)){
			$query->where(function ($query) use($name_or_id) {
                $query->where('sdepartment.id', (int)$name_or_id );
                $query->orWhere('sdepartment.sdepartment_name_ar', 'LIKE', '%'.urldecode($name_or_id).'%' );
                $query->orWhere('sdepartment.sdepartment_name_en', 'LIKE', '%'.urldecode($name_or_id).'%' );
            });
		}


		if(isset($status)){
			$active = $status;
		}
		if( isset($active) AND in_array($active,['Y','N','all']) ){
			if( in_array($active,['Y','N']) ){
				$query->where('sdepartment.active',$active);
			}
		}else{
			$query->where('sdepartment.active',"Y");
		}

		if( isset($export) ){
			unset($limit);
		}
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}


		$_order_by = 'sdepartment.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','sdepartment_name_ar','sdepartment_name_en','school_id','week_template_id']) ){
			$_order_by = 'sdepartment.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);


		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach ($result as $item) {
			$this->filter_item_after_get($item);
		}

		if( isset($export) ){
			\App\Helpers\ExcelHelper::export_model($result->toArray(),'sdepartments',Sdepartment::getExportedData());
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$sdepartment){
		if( $sdepartment->school_id ){
			$sdepartment->school_id = (int)$sdepartment->school_id;
		}
		if( $sdepartment->week_template_id ){
			$sdepartment->week_template_id = (int)$sdepartment->week_template_id;
		}
	}




	public function save(Request $request)
	{
		$data = $request->get('sdepartment');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$sdepartment = new Sdepartment;
		}else{
			$sdepartment = Sdepartment::find($data['id']);	
		}

		
		if(!$sdepartment){
			return $this->sendResponse(null, ['SchoolDepartment.SchoolDepartmentNotValid'], false);
		}


		if(isset($data['active'])) $sdepartment->active = $data['active'];
		if(isset($data['sdepartment_name_ar'])) $sdepartment->sdepartment_name_ar = $data['sdepartment_name_ar'];
		if(isset($data['sdepartment_name_en'])) $sdepartment->sdepartment_name_en = $data['sdepartment_name_en'];
		if(isset($data['school_id'])) $sdepartment->school_id = $data['school_id'];
		if(isset($data['week_template_id'])) $sdepartment->week_template_id = $data['week_template_id'];
		
		$sdepartment->save();

		return $this->sendResponse($sdepartment->id, ['Form.DataSavedWithSuccess']);
	}




	public function delete(Request $request)
	{
		$sdepartment_id = $request->get('sdepartment_id');

		if(!$sdepartment_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Sdepartment::destroy($sdepartment_id);
		if(!$destroy){
			return $this->sendResponse(null, ['SchoolDepartment.SchoolDepartmentNotValid'], false);
		}

		return $this->sendResponse(null, ['Form.DataDeletedWithSuccess']);
	}
}
