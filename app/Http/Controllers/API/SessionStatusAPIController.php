<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\SessionStatusRepository;
use App\Models\SessionStatus;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class SessionStatusAPIController extends AppBaseController
{
	/** @var  SessionStatusRepository */
	private $attendanceStatusRepository;

	function __construct(SessionStatusRepository $attendanceStatusRepo)
	{
        parent::__construct();
		$this->attendanceStatusRepository = $attendanceStatusRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = SessionStatus::select([
            'session_status.*'
        ]);

        if(isset($session_status_id)){
            $query->where('session_status.id',$session_status_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            $this->filter_item_after_get($single_item);
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess'], true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('session_status.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('session_status.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }
        foreach ($result as $item) {
        	$this->filter_item_after_get($item);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }

    function filter_item_after_get(&$item){

    }


    public function save(Request $request)
    {
        $data = $request->get('session_status');

        if(!$data){
            return $this->sendResponse($result, ['Form.EmptyData'], true);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $session_status = new SessionStatus;
        }else{
            $session_status = SessionStatus::find($data['id']);
        }


        if(!$session_status){
            return $this->sendResponse($result, ['SessionSatatus.InvalideSessionStatus'], false);
        }

        if(isset($data['active'])) $session_status->active = $data['active'];
        if(isset($data['lookup_code'])) $session_status->lookup_code = $data['lookup_code'];
        if(isset($data['session_status_name_ar'])) $session_status->session_status_name_ar = $data['session_status_name_ar'];
        if(isset($data['session_status_name_en'])) $session_status->session_status_name_en = $data['session_status_name_en'];
        $session_status->save();

        return $this->sendResponse($session_status->id, ['Form.DataSavedWithSuccess'], true);
    }


    public function delete(Request $request)
    {
        $session_status_id = $request->get('session_status_id');

        if(!$session_status_id){
            return $this->sendResponse($result, ['Form.EmptyData'], false);
        }

        $destroy = SessionStatus::destroy($session_status_id);
        if(!$destroy){
            return $this->sendResponse($result, ['SessionSatatus.InvalideSessionStatus'], false);
        }

        return $this->sendResponse($session_status_id, Lang::get('global.success.delete'), true);
    }
}
