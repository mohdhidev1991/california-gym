<?php namespace App\Http\Controllers\API;

use App\Helpers\Server;
use App\Http\Requests;
use Dingo\Api\Auth\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Libraries\Repositories\StudentRepository;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;


class StudentAPIController extends AppBaseController
{
	/** @var  StudentRepository */
	private $studentRepository;

	function __construct(StudentRepository $studentRepo)
	{
		parent::__construct();
		
		$this->studentRepository = $studentRepo;
	}



	/*
		Function Key:
		Created By Aziz: 12/03/2016
		List the student of a Course Session
	*/
	public function listStudentsByCourseSession(Request $request)
	{
		/* moved from __construct */
		$this->group_num = Server::get('SCHOOLS',$this->user->current_school_id,'group_num');
		$this->school_id = $this->user->current_school_id;

		$course_session = $request->all();
		extract($course_session);
		$students = Student::select(
			DB::Raw('student.id AS student_id'),
			DB::Raw('CONCAT(student.firstname," ",student.f_firstname," ",student.lastname) AS fullname'),
			'student_session.coming_status_id','student_session.exit_status_id','student_session.coming_time',
			'student_session.exit_time','student_session.coming_alert_id','student_session.exit_alert_id'
		)
			->join('student_session','student_session.student_id','=','student.id')
			->where('student_session.group_num','=',$this->group_num)
			->where('student_session.school_id','=',$this->school_id)
			->where('student_session.year','=',$year)
			->where('student_session.hmonth','=',$hmonth)
			->where('student_session.hday_num','=',$hday_num)
			->where('student_session.level_class_id','=',$level_class_id)
			->where('student_session.symbol','=',$symbol)
			->where('student_session.session_order','=',$session_order)
			->get();

		return $this->sendResponse($students,null);
	}
	





	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = Student::select([
			'student.*'
		]);


		if(isset($student_id)){
			$query->where('student.id',$student_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}


		if(isset($idn)){
			$query->where('student.idn',$idn);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('student.active',"Y");
		}



		if( isset($key_search) ){
			$query->where(function ($query) use ($key_search) {
                $query->where('student.idn', $key_search);
                $query->orWhere('student.mobile', $key_search);
                $query->orWhere('student.firstname', 'like' ,$key_search.'%');
                $query->orWhere('student.lastname', 'like' ,$key_search.'%');
                $query->orWhere('student.f_firstname', 'like' ,$key_search.'%');
            });
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('student.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		$count = $query->count();
		foreach ($result as $item) {
			$this->filter_item_after_get($item);
		}
		$return = ['students' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get(&$item){
		$item->country_id = (int) $item->country_id;
		$item->city_id = (int) $item->city_id;
		$item->idn_type_id = (int) $item->idn_type_id;
		$item->genre_id = (int) $item->genre_id;
	}


	public function save(Request $request)
	{
		$data = $request->get('student');

		if(!$data){
			return $this->sendResponse(null, Lang::get('student.errors.empty_student_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$student = new Student;
		}else{
			$student = Student::find($data['id']);	
		}

		
		if(!$student){
			return $this->sendResponse(null, Lang::get('student.errors.student_not_existe'), false);
		}

		if(isset($data['active'])) $student->active = $data['active'];
		if(isset($data['city_id'])) $student->city_id = $data['city_id'];
		if(isset($data['country_id'])) $student->country_id = $data['country_id'];
		if(isset($data['birth_date'])) $student->birth_date = $data['birth_date'];
		if(isset($data['idn_type_id'])) $student->idn_type_id = $data['idn_type_id'];
		if(isset($data['idn'])) $student->idn = $data['idn'];
		if(isset($data['father_rea_user_id'])) $student->father_rea_user_id = $data['father_rea_user_id'];
		if(isset($data['mother_rea_user_id'])) $student->mother_rea_user_id = $data['mother_rea_user_id'];
		if(isset($data['rea_user_id'])) $student->rea_user_id = $data['rea_user_id'];
		if(isset($data['resp1_rea_user_id'])) $student->resp1_rea_user_id = $data['resp1_rea_user_id'];
		if(isset($data['resp2_rea_user_id'])) $student->resp2_rea_user_id = $data['resp2_rea_user_id'];
		if(isset($data['address'])) $student->address = $data['address'];
		if(isset($data['cp'])) $student->cp = $data['cp'];
		if(isset($data['email'])) $student->email = $data['email'];
		if(isset($data['firstname'])) $student->firstname = $data['firstname'];
		if(isset($data['f_firstname'])) $student->f_firstname = $data['f_firstname'];
		if(isset($data['lastname'])) $student->lastname = $data['lastname'];
		if(isset($data['mobile'])) $student->mobile = $data['mobile'];
		if(isset($data['quarter'])) $student->quarter = $data['quarter'];
		if(isset($data['genre_id'])) $student->genre_id = $data['genre_id'];
		
		$student->save();

		return $this->sendResponse($student, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$student_id = $request->get('student_id');

		if(!$student_id){
			return $this->sendResponse(null, Lang::get('student.errors.empty_student_id'), false);
		}

		$destroy = Student::destroy($student_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('student.errors.student_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
