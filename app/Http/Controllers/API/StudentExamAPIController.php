<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentExamRepository;
use App\Models\StudentExam;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class StudentExamAPIController extends AppBaseController
{
	/** @var  StudentExamRepository */
	private $student_examRepository;

	function __construct(StudentExamRepository $student_examRepo)
	{
		parent::__construct();
		$this->student_examRepository = $student_examRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = StudentExam::select([
			'student_exam.*'
		]);

		if(isset($student_exam_id)){
			$query->where('student_exam.id',$student_exam_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('student_exam.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('student_exam.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['student_exams' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($item){
	}



	public function save(Request $request)
	{
		$data = $request->get('student_exam');

		if(!$data){
			return $this->sendResponse(null, Lang::get('student_exam.errors.empty_student_exam_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$student_exam = new StudentExam;
		}else{
			$student_exam = StudentExam::find($data['id']);	
		}

		
		if(!$student_exam){
			return $this->sendResponse(null, Lang::get('student_exam.errors.student_exam_not_existe'), false);
		}

		if(isset($data['active'])) $student_exam->active = $data['active'];
		if(isset($data['student_exam_name_ar'])) $student_exam->student_exam_name_ar = $data['student_exam_name_ar'];
		if(isset($data['student_exam_name_en'])) $student_exam->student_exam_name_en = $data['student_exam_name_en'];
		
		$student_exam->save();

		return $this->sendResponse($student_exam, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$student_exam_id = $request->get('student_exam_id');

		if(!$student_exam_id){
			return $this->sendResponse(null, Lang::get('student_exam.errors.empty_student_exam_id'), false);
		}

		$destroy = StudentExam::destroy($student_exam_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('student_exam.errors.student_exam_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
