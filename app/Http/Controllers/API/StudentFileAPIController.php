<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentFileRepository;
use App\Models\StudentFile;
use App\Models\SchoolScope;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\CourseSchedItem;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use App\Helpers\HijriDateHelper;
use Illuminate\Support\Facades\DB;
use App\Libraries\Factories\ReaUserFactory;


class StudentFileAPIController extends AppBaseController
{
	/** @var  StudentFileRepository */
	private $studentFileRepository;

	function __construct(StudentFileRepository $studentFileRepo)
	{
		parent::__construct();
		$this->studentFileRepository = $studentFileRepo;
	}

	


	/**
	*	Created by Achraf
	*	Created at 06/03/2016
	*	Function key: BF264 / Q0007
	*/
	public function getAllStudentFilesBySchoolID($school_id)
	{
		$current_day = HijriDateHelper::getHdateByFormat(date('Ymd'));
		$result = StudentFile::select('student_file.id','student_file.student_file_name_ar','student_file.student_file_name_en')
		->where('school_id',$school_id)
		->where('student_file_end_hdate', '>=', $current_day)
		->get();
		if(!$result){
			return $this->sendResponse(null, Lang::get('student_file.errors.empty_student_files'), false);	
		}

		return $this->sendResponse($result, Lang::get('student_file.success.valide_student_files'), true);	
	}







	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$sf = new StudentFile();
		$sf_fields = $sf->getFillable();
		foreach ($sf_fields as &$item) {
			$item = 'student_file.'.$item;
		}
		$query = StudentFile::select($sf_fields);

		if(isset($student_file_id)){
			$query->where('student_file.id',$student_file_id);

			if(isset($fullinfos)){
				$query->leftJoin('school', 'school.id', '=', 'student_file.school_id');
				$query->addSelect([
					'school.school_name_ar'
				]);
			}

			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}

			$this->filter_item_after_get($single_item);

			if(isset($fullinfos)){
				$this->fullinfos_fields($single_item);
			}

			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}


		if(isset($status)){
			$active = $status;
		}
		if( isset($active) AND in_array($active,['Y','N','all']) ){
			if( in_array($active,['Y','N']) ){
				$query->where('student_file.active',$active);
			}
		}else{
			$query->where('student_file.active',"Y");
		}

		if( isset($school_id) ){
			$query->where('student_file.school_id',$school_id);
		}

		if( isset($student_id) ){
			$query->where('student_file.student_id',(int)$student_id);
		}

		if( isset($join) AND !empty($join) ){
            $join = explode('!', $join);
        }else{
            $join = [];
        }
    
        if(in_array('level_class', $join) OR in_array('all', $join)){
            $query->join('level_class','level_class.id','=','student_file.level_class_id');
            $query->addSelect([
                'level_class.level_class_name_ar as level_class_name_ar',
                'level_class.level_class_name_en as level_class_name_en',
            ]);
        }
        if(in_array('student', $join) OR in_array('all', $join)){
            $query->join('student','student.id','=','student_file.student_id');
            $query->addSelect([
                DB::raw('CONCAT(student.firstname," ",student.f_firstname," ",student.lastname) AS student_name'),
            ]);
        }

		if(in_array('school', $join) OR in_array('all', $join)){
			$query->join('school','school.id','=','student_file.school_id');
			$query->addSelect([
				'school.school_name_en',
				'school.school_name_ar',
			]);
		}

		if( isset($current_school_year) ){
			$current_school_id = ReaUserFactory::get_current_school_id();
			$current_school_year_id = ReaUserFactory::get_current_school_year_id();
			$query->join('school_year','school_year.year','=','student_file.year');
			$query->where('school_year.id', '=', $current_school_year_id);
			$query->where('student_file.school_id', '=', $current_school_id);
		}

		if( isset($school_class_id) ){
			$query->where('student_file.school_class_id', '=', (int)$school_class_id);
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$_order_by = 'student_file.id';
		$_order = 'ASC';
		if(isset($order_by) AND in_array($order_by,['id','active','student_num','student_place']) ){
			$_order_by = 'student_file.'.$order_by;
		}
		if( isset($order) AND in_array($order,['ASC','DESC']) ){
			$_order = $order;
		}
		$query->orderBy($_order_by,$_order);
		$result = $query->get();

		if( isset($export) ){
			\App\Helpers\ExcelHelper::export_model($result->toArray(),'student_files',StudentFile::getExportedData());
		}
		
		$total = $query->count();
		if(!$total){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach ($result as $item) {
			$this->filter_item_after_get($item);
		}

		if(isset($first) AND isset($result[0])){
			$single_item = $result[0];
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(isset($first)){
			if(isset($result[0])){
				$result = $result[0];
			}
		}
		return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
		if(isset($item->student_id)) $item->student_id = (int)$item->student_id;
		if(isset($item->school_class_id)) $item->school_class_id = (int)$item->school_class_id;
		if(isset($item->school_id)) $item->school_id = (int)$item->school_id;
		if(isset($item->level_class_id)) $item->level_class_id = (int)$item->level_class_id;
		if(isset($item->student_file_status_id)) $item->student_file_status_id = (int)$item->student_file_status_id;
		if(isset($item->student_num)) $item->student_num = (int) $item->student_num;
		if(isset($item->school_level_id)) $item->school_level_id = (int)$item->school_level_id;
	}




	public function save(Request $request)
	{
		$data = $request->get('student_file');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$student_file = new StudentFile;
		}else{
			$student_file = StudentFile::find($data['id']);	
		}

		
		if(!$student_file){
			return $this->sendResponse(['success' => false, 'errors' => ["Form.ErrorData"] ], Lang::get('student_file.errors.student_file_not_existe'));
		}

		if(isset($data['active'])) $student_file->active = $data['active'];
		if(isset($data['student_id'])) $student_file->student_id = $data['student_id'];
		if(isset($data['student_num'])) $student_file->student_num = $data['student_num'];
		if(isset($data['school_class_id'])) $student_file->school_class_id = $data['school_class_id'];
		if(isset($data['school_id'])) $student_file->school_id = $data['school_id'];
		if(isset($data['year'])) $student_file->year = $data['year'];
		if(isset($data['level_class_id'])) $student_file->level_class_id = $data['level_class_id'];
		if(isset($data['symbol'])) $student_file->symbol = $data['symbol'];
		if(isset($data['student_file_status_id'])) $student_file->student_file_status_id = $data['student_file_status_id'];
		if(isset($data['final_rating_id'])) $student_file->final_rating_id = $data['final_rating_id'];
		if(isset($data['paid_amount'])) $student_file->paid_amount = $data['paid_amount'];
		if(isset($data['paid_hdate'])) $student_file->paid_hdate = $data['paid_hdate'];
		if(isset($data['paid_comment'])) $student_file->paid_comment = $data['paid_comment'];
		if(isset($data['student_place'])) $student_file->student_place = $data['student_place'];
		
		$student_file->save();

		return $this->sendResponse($student_file->id, ['Form.DataSavedWithSuccess']);
	}



	public function save_student(Request $request)
	{
		$data = $request->get('student_file');



		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$current_school_id = null;
		if(isset($this->user) AND isset(get_cache('rea_user:'.$this->user->id)->current_school->school_id) ){
			$current_school_id = get_cache('rea_user:'.$this->user->id)->current_school->school_id;
		}elseif( isset($this->user->last_selected_school) ){
			$current_school_id = $this->user->last_selected_school;
		}else{
			return $this->sendResponse(null, ['User.CurrentSchoolNotSelected'], false);
		}



		if( isset($data['new']) AND $data['new']==true ){
			$student_file = new StudentFile;
		}else{
			$student_file = StudentFile::find($data['id']);
		}		

		if(!$student_file){
			return $this->sendResponse(null, ['StudentFile.StudentFileInvalid'], false);
		}

		$student_file->student_file_status_id = 1;
		$student_file->active = 'Y';
		$student_file->level_class_id = (int)$data['level_class_id'];
		$student_file->school_class_id = (int)$data['school_class_id'];
		$student_file->symbol = $data['symbol'];
		$student_file->student_id = (int)$data['student_id'];
		$student_file->school_id = (int)$current_school_id;
		if(isset($data['paid_amount'])) $student_file->paid_amount = $data['paid_amount'];
		if(isset($data['paid_hdate'])) $student_file->paid_hdate = $data['paid_hdate'];
		if(isset($data['paid_comment'])) $student_file->paid_comment = $data['paid_comment'];
		if(isset($data['student_place'])) $student_file->student_place = $data['student_place'];
		if(isset($data['student_num'])) $student_file->student_num = $data['student_num'];

		$student_file->save();
		return $this->sendResponse($student_file->id, ['Form.DataSavedWithSuccess']);
	}






	public function delete(Request $request)
	{
		$student_file_id = $request->get('student_file_id');

		if(!$student_file_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = StudentFile::destroy($student_file_id);
		if(!$destroy){
			return $this->sendResponse(null, ['StudentFile.StudentFileInvalid'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess']);
	}



	public function reorder_student_places(Request $request)
	{
		$school_class_id = $request->get('school_class_id');

		if(!$school_class_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$current_school_id = ReaUserFactory::get_current_school_id();
		$current_school_year_id = ReaUserFactory::get_current_school_year_id();
		$school_year = SchoolYear::find($current_school_year_id);

		if(!$current_school_id || !$current_school_year_id || !isset($school_year->year) ){
			return $this->sendResponse(null, ['StudentFile.CantGetCurrentSchoolYear'], false);
		}

		
		$school_class = SchoolClass::select(['school_class.id', 'room.capacity'])
		->where('school_class.id', $school_class_id)
		->join('room','room.id','=','school_class.room_id')
		->get()->first()->toArray();
		

		if(!isset($school_class['capacity']) OR !$school_class['capacity']){
			return $this->sendResponse(null, ['SchoolClass.SchoolClassCapacityNotConfigurated'], false);
		}

		$students_files = StudentFile::where('school_class_id', $school_class_id)
		->where('school_id',$current_school_id)
		->where('year',$school_year->year)
		->get()
		->toArray();

		if(!$students_files){
			return $this->sendResponse(null, ['StudentFile.EmptyStudents'], false);
		}

		$places = [];
		for ($i=1; $i <= (int)$school_class['capacity']; $i++) { 
			$places[] = $i;
		}
		$places_reserved = [];
		foreach($students_files as $student_file){
			$places_reserved[] = $student_file['student_place'];
		}

		$missing = array_diff($places,$places_reserved);
		

		foreach($students_files as &$student_file){
			if(!isset($student_file['student_place']) OR empty($student_file['student_place']) OR (int)$student_file['student_place']>(int)$school_class['capacity'] ){
				$first_allowed = key($missing);
				$student_file['student_place'] = $missing[$first_allowed];
				unset($missing[$first_allowed]);
				$student_file_obj = StudentFile::find($student_file['id']);
				$student_file_obj->student_place = $student_file['student_place'];
				$student_file_obj->save();
			}
		}
		return $this->sendResponse(null, ['SchoolClass.SuccessReoarderSchoolClassPlaces']);
	}



	public function replace_student_place(Request $request)
	{
		$school_class_id = $request->get('school_class_id');
		$student_id = $request->get('student_id');
		$new_student_place = $request->get('new_student_place');
		$new_school_class_id = $request->get('new_school_class_id');
		
		if(!$new_school_class_id || !$school_class_id || !$student_id || !$new_student_place){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$current_school_id = ReaUserFactory::get_current_school_id();
		$current_school_year_id = ReaUserFactory::get_current_school_year_id();
		$school_year = SchoolYear::find($current_school_year_id);



		if(!$current_school_id || !$current_school_year_id || !isset($school_year->year) ){
			return $this->sendResponse(null, ['StudentFile.CantGetCurrentSchoolYear'], false);
		}

		$school_class = SchoolClass::find($school_class_id);
		$new_school_class = SchoolClass::find($new_school_class_id);
		if( !$school_class || !$new_school_class ){
			return $this->sendResponse(null, ['StudentFile.InvalidSchoolClass'], false);
		}
		
		


		$student_file = StudentFile::where('school_class_id',$school_class_id)
		->where('student_id',$student_id)
		->where('school_id',$current_school_id)
		->where('year',$school_year->year)
		->first();

		
		$student_file_to_replace_with = StudentFile::where('school_class_id',$new_school_class_id)
		->where('student_place',$new_student_place)
		->where('school_id',$current_school_id)
		->where('year',$school_year->year)
		->first();

		/* IF empty place AND same school_class */
		if(!$student_file_to_replace_with AND $new_school_class_id==$school_class_id){
			$student_file->student_place = $new_student_place;
			$student_file->save();
		}

		/* switch students places in same school_class */
		if($student_file_to_replace_with AND $new_school_class_id==$school_class_id){
			$old_student_place = $student_file->student_place;
			
			$student_file->student_place = $new_student_place;
			$student_file->save();

			$student_file_to_replace_with->student_place = $old_student_place;
			$student_file_to_replace_with->save();
		}


		/* IF empty place AND change school_class */
		if(!$student_file_to_replace_with AND $new_school_class_id!=$school_class_id){

			$h_current_day = \App\Helpers\HijriDateHelper::to_hijri(date('Ymd'));

			/*
			$course_sched_item = CourseSchedItem::where('school_year_id',$current_school_year_id)
			->where('symbol',$new_school_class->symbol)
			->where('level_class_id',$new_school_class->level_class_id)
			->where('active','Y')
			->get()->toArray();


			echo $h_current_day;
			echo $current_school_year_id;
			print_r($course_sched_item);
			exit();

			
			StudentSession::where('group_num',(int)$this->group_num)
			->where('school_id',(int)$this->school_id)
			->where('prof_id',$school_employee->id)
			->where('session_hdate','>=',$h_current_day)
			->update([
				'prof_id' => $school_employee_to_replace
			]);*/
			$student_file->student_place = $new_student_place;
			$student_file->school_class_id = $new_school_class_id;
			$student_file->symbol = $new_school_class->symbol;
			$student_file->save();
		}

		/* switch students places and school class */
		if($student_file_to_replace_with AND $new_school_class_id!=$school_class_id){
			$old_student_place = $student_file->student_place;
			
			$student_file->student_place = $new_student_place;
			$student_file->school_class_id = $new_school_class_id;
			$student_file->symbol = $new_school_class->symbol;
			$student_file->save();

			$student_file_to_replace_with->student_place = $old_student_place;
			$student_file_to_replace_with->school_class_id = $school_class_id;
			$student_file_to_replace_with->symbol = $school_class->symbol;
			$student_file_to_replace_with->save();
		}



		return $this->sendResponse(null, ['StudentFile.StudentPlaceChangedWithSuccess']);
	}

}




