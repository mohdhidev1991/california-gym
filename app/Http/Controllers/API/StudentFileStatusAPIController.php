<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\StudentFileStatusRepository;
use App\Models\StudentFileStatus;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class StudentFileStatusAPIController extends AppBaseController
{
	/** @var  StudentFileStatusRepository */
	private $student_file_statusRepository;

	function __construct(StudentFileStatusRepository $student_file_statusRepo)
	{
		parent::__construct();
		$this->student_file_statusRepository = $student_file_statusRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = StudentFileStatus::select([
			'student_file_status.*'
		]);

		if(isset($student_file_status_id)){
			$query->where('student_file_status.id',$student_file_status_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('student_file_status.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('student_file_status.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['student_file_statuss' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($item){
	}



	public function save(Request $request)
	{
		$data = $request->get('student_file_status');

		if(!$data){
			return $this->sendResponse(null, Lang::get('student_file_status.errors.empty_student_file_status_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$student_file_status = new StudentFileStatus;
		}else{
			$student_file_status = StudentFileStatus::find($data['id']);	
		}

		
		if(!$student_file_status){
			return $this->sendResponse(null, Lang::get('student_file_status.errors.student_file_status_not_existe'), false);
		}

		if(isset($data['active'])) $student_file_status->active = $data['active'];
		if(isset($data['lookup_code'])) $student_file_status->lookup_code = $data['lookup_code'];
		if(isset($data['student_file_status_name_ar'])) $student_file_status->student_file_status_name_ar = $data['student_file_status_name_ar'];
		if(isset($data['student_file_status_name_en'])) $student_file_status->student_file_status_name_en = $data['student_file_status_name_en'];
		
		$student_file_status->save();

		return $this->sendResponse($student_file_status, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$student_file_status_id = $request->get('student_file_status_id');

		if(!$student_file_status_id){
			return $this->sendResponse(null, Lang::get('student_file_status.errors.empty_student_file_status_id'), false);
		}

		$destroy = StudentFileStatus::destroy($student_file_status_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('student_file_status.errors.student_file_status_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
