<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;

class SystemAPIController extends AppBaseController
{

	function __construct()
	{
		parent::__construct();
	}



	function clear_cache($module)
	{
		switch ($module) {
            case 'rea_user':
                $rea_user = Auth::user();
                delete_cache('rea_user:'.$rea_user->id);
                break;

            case 'all':
                # code...
                break;
            
            default:
                # code...
                break;
        }
	}




	
}
