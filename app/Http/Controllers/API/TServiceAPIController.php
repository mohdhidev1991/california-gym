<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TServiceRepository;
use App\Models\TService;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class TServiceAPIController extends AppBaseController
{
	/** @var  TServiceRepository */
	private $tServiceRepository;

	function __construct(TServiceRepository $tServiceRepo)
	{
		parent::__construct();
		$this->tServiceRepository = $tServiceRepo;
	}




	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 06/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = TService::select([
			'tservice.*'
		]);

		if(isset($tservice_id)){
			$query->where('tservice.id',$tservice_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('tservice.active',"Y");
		}
		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('tservice.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$count = $query->count();
		$return = ['tservices' => $result, 'total' => $count];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get($tservice){
		
	}





	public function save(Request $request)
	{
		$data = $request->get('tservice');

		if(!$data){
			return $this->sendResponse(null, Lang::get('tservice.errors.empty_tservice_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$tservice = new TService;
		}else{
			$tservice = TService::find($data['id']);	
		}

		
		if(!$tservice){
			return $this->sendResponse(null, Lang::get('tservice.errors.tservice_not_existe'), false);
		}

		if(isset($data['active'])) $tservice->active = $data['active'];
		if(isset($data['lookup_code'])) $tservice->lookup_code = $data['lookup_code'];
		if(isset($data['tservice_name_ar'])) $tservice->tservice_name_ar = $data['tservice_name_ar'];
		if(isset($data['tservice_name_en'])) $tservice->tservice_name_en = $data['tservice_name_en'];
		
		$tservice->save();

		return $this->sendResponse($tservice, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$tservice_id = $request->get('tservice_id');

		if(!$tservice_id){
			return $this->sendResponse(null, Lang::get('tservice.errors.empty_tservice_id'), false);
		}

		$destroy = TService::destroy($tservice_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('tservice.errors.tservice_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}
}
