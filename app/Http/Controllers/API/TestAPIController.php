<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TestRepository;
use App\Helpers\MfkHelper;
use App\Models\Test;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TestAPIController extends AppBaseController
{
	/** @var  TestRepository */
	private $TestRepository;

	function __construct(TestRepository $TestRepo)
	{
		parent::__construct();
		$this->TestRepository = $TestRepo;
	}

    public function get($params = null)
	{

		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}

		$model = new Test();
		$model_fields = $model->getFillable();
		
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
        }

		$query = Test::select(
			$model_fields
		);
		$query->where($model->getTable().'.champ_1','<>',"D");

		if(isset($Test_id)){
            
            $query->where($model->getTable().'.id',$Test_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
            
        }

		if(!isset($champ_1) OR ($champ_1!='all') ){
			$query->where($model->getTable().'.champ_1',"Y");
		}

		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');

		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}
        
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	public function save(Request $request)
	{   
		
		$data = $request->get('test');
		
		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$test = new Test;
		}else{
			$test = Test::find($data['id']);
		}

		if(!$test){
			return $this->sendResponse(null, ['Test.InvalidTest'], false);
		}
		
		$model = new Test;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $test->{$item} = $data[$item];
			if( empty($data[$item]) ) $test->{$item} = null;
		}

		$test->save();
		return $this->sendResponse($test->id, ['Form.DataSavedWithSuccess'], true);
	}

	public function delete(Request $request)
	{   

		$test_id = $request->get('test_id');
        if(!$test_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}
		
		$test = Test::find($test_id);
		
		if(!$test){
			return $this->sendResponse(null, ['Role.InvalideRole'], false);
		}

        $test->champ_1 = 'D';
		$test->save();
		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);

	}

    
}
