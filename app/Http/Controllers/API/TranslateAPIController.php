<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TranslateRepository;
use App\Models\Translate;
use App\Models\Language;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;
use DB;

class TranslateAPIController extends AppBaseController
{
	/** @var  TranslateRepository */
	private $translateRepository;

	function __construct(TranslateRepository $translateRepo)
	{
		$this->translateRepository = $translateRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = Translate::select([
            'translate.*',
            'translate_module.lookup_code as module',
        ]);
        $query->join('translate_module','translate_module.id','=','translate.translate_module_id' );
        $query->groupBy('translate.meta_key');

        if(isset($translate_id)){
            $query->where('translate.id',$translate_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.EmptyResults'], false);
            }
            return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('translate.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('translate.id', 'ASC');
        $result = $query->get();



        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], true, 0);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
    }



    public function multiget($params = null)
    {
        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }

        $languages = Language::select('*')->get();
        $extend_join = null;
        $extend_select = null;
        $extend_where = null;
        foreach($languages as $lang){
            $extend_join .= "
                LEFT JOIN (
                    SELECT CONCAT(tm.lookup_code,'.',t.meta_key) as lang_tcode, t.meta_value as meta_value
                    FROM translate as t
                    INNER JOIN translate_module as tm on tm.id = t.translate_module_id
                    WHERE t.lang_id=".$lang->id."
                    GROUP BY t.meta_key, t.translate_module_id
                ) t_{$lang->lookup_code} on t_{$lang->lookup_code}.lang_tcode = CONCAT(tm.lookup_code,'.',t.meta_key)
            ";
            $extend_select .= " , t_".$lang->lookup_code.".meta_value as lang_".$lang->id." ";
        }

        if( isset($translate_module_id) ){
            $extend_where .= " AND t.translate_module_id = ".(int)$translate_module_id." ";
        }

        $params_query = [];
        if( isset($key_search) ){
            $extend_where .= " AND (
                t.meta_key LIKE ?
                OR
                t.meta_value LIKE ?
            )";
            $params_query[] = '%'.urldecode($key_search).'%';
            $params_query[] = '%'.urldecode($key_search).'%';
        }


        $limit_sql = null;
        if( isset($limit) ){
            $limit_sql = " LIMIT ".$limit." ";
        }

        

        $offset_sql = null;
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $offset_sql = " OFFSET ".$skip." ";
        }

        $sql = "SELECT tm.id as translate_module_id, tm.lookup_code, t.meta_key, CONCAT(tm.lookup_code,'.',t.meta_key) as tcode {$extend_select}
            FROM translate as t
            INNER JOIN translate_module as tm on tm.id = t.translate_module_id

            {$extend_join}
            WHERE 1
            {$extend_where}
            GROUP BY t.meta_key, t.translate_module_id
            ORDER BY tm.lookup_code ASC
            {$limit_sql}
            {$offset_sql}
            ";
        $result = DB::select( DB::raw($sql), $params_query );
        if(!$result){
            return $this->sendResponse(null, ['Translate.EmptyResults'], false, 0);    
        }
        $sql_count = "SELECT COUNT(*) as count
            FROM (SELECT t.id
            FROM translate as t
            INNER JOIN translate_module as tm on tm.id = t.translate_module_id

            {$extend_join}
            WHERE 1
            {$extend_where}
            GROUP BY t.meta_key, t.translate_module_id
            ORDER BY tm.lookup_code ASC) as r
            ";
        $total = DB::select( DB::raw($sql_count), $params_query )[0]->count;        

        return $this->sendResponse($result, ['Global.SuccessGetData'], true, $total);
    }






    public function get_lang($lang_code)
    {
        $extend_join = null;
        $extend_select = null;
        $extend_where = null;

        $extend_join .= "
            LEFT JOIN (
                SELECT CONCAT(tm.lookup_code,'.',t.meta_key) as lang_tcode, t.meta_value as meta_value
                FROM translate as t
                INNER JOIN translate_module as tm on tm.id = t.translate_module_id
                INNER JOIN lang as lang on lang.id = t.lang_id
                WHERE lang.lookup_code='".$lang_code."'
                GROUP BY t.meta_key, t.translate_module_id
            ) t_{$lang_code} on t_{$lang_code}.lang_tcode = CONCAT(tm.lookup_code,'.',t.meta_key)
        ";
        $extend_select .= " , t_".$lang_code.".meta_value as meta_value ";


        $sql = "SELECT tm.id as translate_module_id, tm.lookup_code, t.meta_key, CONCAT(tm.lookup_code,'.',t.meta_key) as tcode {$extend_select}
            FROM translate as t
            INNER JOIN translate_module as tm on tm.id = t.translate_module_id
            INNER JOIN lang as lang on lang.id = t.lang_id
            {$extend_join}
            WHERE 1
            AND lang.lookup_code='".$lang_code."'
            {$extend_where}
            GROUP BY t.meta_key, t.translate_module_id
            ORDER BY tm.lookup_code ASC
            ";


        $result = DB::select( DB::raw($sql) );

        $translates = collect($result);
        $return = [];
        $translates->each(function($item) use (&$return){
            if(is_null($item->meta_value)) $item->meta_value = $item->tcode;
            $return[$item->lookup_code][$item->meta_key] = $item->meta_value;
        });
        echo json_encode($return);
        die();
    }


    public function multisave(Request $request)
    {
        $translates = $request->get('translates');
        if(!$translates){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $translates = collect($translates);
        $languages = Language::select('*')->get();
        $translates->each(function ($item, $key) use ($languages) {
            foreach ($languages as $lang) {
                $exist_tr = Translate::select('id')->where('lang_id',$lang->id)->where('translate_module_id',$item['translate_module_id'])->where('meta_key',$item['meta_key'])->first();
                if($exist_tr){
                    Translate::where('id',$exist_tr->id)
                    ->update([
                        'meta_value' => $item['lang_'.$lang->id],
                    ]);
                }else{
                    Translate::insert([
                        'meta_value' => $item['lang_'.$lang->id],
                        'lang_id' => $lang->id,
                        'meta_key' => $item['meta_key'],
                        'active' => 'Y',
                        'translate_module_id' => $item['translate_module_id']
                    ]);
                }
            }
        });
        return $this->sendResponse(null, ['Form.DataSavedWithSuccess'], true);
    }


    public function save(Request $request)
    {
        $data = $request->get('translate');

        if(!$data
            OR !isset($data['translate_module_id']) OR !isset($data['meta_key'])
            OR ($data['translate_module_id']=="") OR ($data['meta_key']=="")
        ){
            return $this->sendResponse(null, ["Form.EmptyData"], false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $translate = new Translate;
            $if_exists = Translate::where('meta_key',$data['meta_key'])->where('translate_module_id',$data['translate_module_id'])->count();
            if($if_exists){
                return $this->sendResponse(null, ["Translate.AlreadyExists"], false);
            }
        }
        
        $languages = Language::select('*')->get();
        foreach ($languages as $lang) {
            if( !isset($data['lang_'.$lang->id]) ){
                $data['lang_'.$lang->id] = null;
            }
            $exist_tr = Translate::select('id')->where('lang_id',$lang->id)->where('meta_key',$data['meta_key'])->where('translate_module_id',$data['translate_module_id'])->first();
            if($exist_tr){
                Translate::where('id',$exist_tr->id)
                ->update([
                    'meta_value' => $data['lang_'.$lang->id]
                ]);
            }else{
                Translate::insert([
                    'meta_value' => $data['lang_'.$lang->id],
                    'active' => 'Y',
                    'lang_id' => $lang->id,
                    'meta_key' => $data['meta_key'],
                    'translate_module_id' => $data['translate_module_id']
                ]);
            }
        }
        return $this->sendResponse(null, ['Form.DataSavedWithSuccess'], true);
    }


    public function delete(Request $request)
    {

        $translate_module_id = $request->get('translate_module_id');
        $meta_key = $request->get('meta_key');

        if(!$translate_module_id OR !$meta_key){
            return $this->sendResponse(null, ["Form.EmptyData"], false);
        }

        $translates = Translate::select('id')->where(['translate_module_id' => $translate_module_id, 'meta_key' => $meta_key])->get()->toArray();
        $destroy = Translate::destroy($translates);
        if(!$destroy){
            return $this->sendResponse(null, ["Translate.TranslateNotExists"], false);
        }

        return $this->sendResponse(null, ["Global.DataDeletedWithSuccess"]);
    }
}
