<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TranslateModuleRepository;
use App\Models\TranslateModule;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class TranslateModuleAPIController extends AppBaseController
{
	/** @var  TranslateModuleRepository */
	private $translateModuleRepository;

	function __construct(TranslateModuleRepository $translateModuleRepo)
	{
		$this->translateModuleRepository = $translateModuleRepo;
	}

    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = TranslateModule::select([
            'translate_module.*'
        ]);

        if(isset($translate_module_id)){
            $query->where('translate_module.id',$translate_module_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, ['Global.CantGetData'], false);
            }
            return $this->sendResponse($single_item, ['Global.SuccessGetData'], true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('translate_module.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('translate_module.id', 'ASC');
        $result = $query->get();
        if(!$result){
            return $this->sendResponse(null, ['Global.EmptyResults'], false);
        }
        $total = $query->count();
        return $this->sendResponse($result, ['Global.SuccessGetGata'], true, $total);
    }




    public function save(Request $request)
    {
        $data = $request->get('translate_module');

        if(!$data OR !isset($data['lookup_code']) OR ($data['lookup_code']=="") ){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $translate_module = new TranslateModule;
            $if_exists = TranslateModule::where('lookup_code',$data['lookup_code'])->count();
            if($if_exists){
                return $this->sendResponse(null, ['TranslateModule.TranslateModuleAlreadyExists'], false);
            }
        }else{
            $translate_module = TranslateModule::find($data['id']);
        }


        if(!$translate_module){
            return $this->sendResponse(null, ['TranslateModule.TranslateModuleNotExists'], false);
        }

        if(isset($data['active'])) $translate_module->active = $data['active'];
        if(isset($data['lookup_code'])) $translate_module->lookup_code = $data['lookup_code'];
        $translate_module->active = "Y";
        $translate_module->save();

        return $this->sendResponse(null, ['Form.DataSavedWithSuccess'], true);
    }


    public function delete(Request $request)
    {
        $translate_module_id = $request->get('translate_module_id');

        if(!$translate_module_id){
            return $this->sendResponse(null, ['Form.EmptyData'], false);
        }

        $destroy = TranslateModule::destroy($translate_module_id);
        if(!$destroy){
            return $this->sendResponse(null, ['TranslateModule.TranslateModuleNotExists'], false);
        }

        return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
    }
}
