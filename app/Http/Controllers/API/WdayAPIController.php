<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\WdayRepository;
use App\Models\Wday;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Response;

class WdayAPIController extends AppBaseController
{
	/** @var  WdayRepository */
	private $wdayRepository;

	function __construct(WdayRepository $wdayRepo)
	{
		parent::__construct();
		$this->wdayRepository = $wdayRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$model = new Wday;
		$model_fields = $model->getFillable();
		foreach ($model_fields as &$item) {
			$item = $model->getTable().'.'.$item;
		}
		$query = Wday::select(
			$model_fields
		);

		if(isset($wday_id)){
			$query->where($model->getTable().'.id',$wday_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, ['Global.EmptyResults'], false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, ['Global.GetDataWithSuccess']);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where($model->getTable().'.active',"Y");
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy($model->getTable().'.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, ['Global.EmptyResults'], false);
		}

		foreach($result as $item){
			$this->filter_item_after_get($item);
		}
		$total = $query->count();
        return $this->sendResponse($result, ['Global.GetDataWithSuccess'], true, $total);
	}


	function filter_item_after_get(&$item){
	}



	public function save(Request $request)
	{
		$data = $request->get('wday');

		if(!$data){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$wday = new Wday;
		}else{
			$wday = Wday::find($data['id']);
		}

		
		if(!$wday){
			return $this->sendResponse(null, ['WDay.InvalideDay'], false);
		}


		$model = new Wday;
		$model_fields = $model->getFillable();
		foreach ($model_fields as $item) {
			if( isset($data[$item]) ) $wday->{$item} = $data[$item];
			if( empty($data[$item]) ) $wday->{$item} = null;
		}
		$wday->save();

		return $this->sendResponse($wday->id, ['Form.DataSavedWithSuccess'], true);
	}




	public function delete(Request $request)
	{
		$wday_id = $request->get('wday_id');

		if(!$wday_id){
			return $this->sendResponse(null, ['Form.EmptyData'], false);
		}

		$destroy = Wday::destroy($wday_id);
		if(!$destroy){
			return $this->sendResponse(null, ['WDay.InvalideDay'], false);
		}

		return $this->sendResponse(null, ['Global.DataDeletedWithSuccess'], true);
	}


}
