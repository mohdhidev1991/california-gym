<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\WeekTemplateRepository;
use App\Models\WeekTemplate;
use Illuminate\Http\Request;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Response;

class WeekTemplateAPIController extends AppBaseController
{
	/** @var  WeekTemplateRepository */
	private $week_templateRepository;

	function __construct(WeekTemplateRepository $week_templateRepo)
	{
		parent::__construct();
		$this->week_templateRepository = $week_templateRepo;
	}

	/*
		Crud methods
	*/
	/**
	*	Created by Achraf
	*	Created at 02/05/2016
	*/
	public function get($params = null)
	{
		
		if($params){
			$parametres = decode_url_params($params);
			extract($parametres);
		}
		$query = WeekTemplate::select([
			'week_template.*'
		]);

		if(isset($week_template_id)){
			$query->where('week_template.id',$week_template_id);
			$single_item = $query->first();
			if(!$single_item){
				return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
			}
			$this->filter_item_after_get($single_item);
			return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
		}

		if(!isset($active) OR ($active!='all') ){
			$query->where('week_template.active',"Y");
		}

		if( isset($school_year_id) AND isset($level_class_id) ){
			$query->whereIn('week_template.id',
				function($query) use ($level_class_id, $school_year_id)
	            {
	                $query->select(DB::raw('sdepartment.week_template_id'))
	                      ->from('school_scope')
	                      ->join('sdepartment','sdepartment.id','=','school_scope.sdepartment_id')
	                      ->where('school_scope.school_year_id',11436)
	                      ->where('school_scope.level_class_id',$level_class_id)
	                      ;
	            }
			);
		}

		
		if( isset($limit) ){
			$query->take($limit);
		}
		if( isset($page) AND isset($limit) ){
			$skip = ($page-1)*$limit;
			$query->skip($skip);
		}

		$query->orderBy('week_template.id', 'ASC');
		$result = $query->get();
		
		if(!$result){
			return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
		}

		foreach ($result as $item) {
			$this->filter_item_after_get($item);
		}

		$count = $query->count();
		$return = ['week_templates' => $result, 'total' => $count ];
		return $this->sendResponse($return, Lang::get('global.success.results'), true);
	}


	function filter_item_after_get(&$week_template){
		if( $week_template->day1_template_id ){
			$week_template->day1_template_id = (int)$week_template->day1_template_id;
		}
		if( $week_template->day2_template_id ){
			$week_template->day2_template_id = (int)$week_template->day2_template_id;
		}
		if( $week_template->day3_template_id ){
			$week_template->day3_template_id = (int)$week_template->day3_template_id;
		}
		if( $week_template->day4_template_id ){
			$week_template->day4_template_id = (int)$week_template->day4_template_id;
		}
		if( $week_template->day5_template_id ){
			$week_template->day5_template_id = (int)$week_template->day5_template_id;
		}
		if( $week_template->day6_template_id ){
			$week_template->day6_template_id = (int)$week_template->day6_template_id;
		}
		if( $week_template->day7_template_id ){
			$week_template->day7_template_id = (int)$week_template->day7_template_id;
		}
		if( $week_template->level_class_id ){
			$week_template->level_class_id = (int)$week_template->level_class_id;
		}
		if( $week_template->school_id ){
			$week_template->school_id = (int)$week_template->school_id;
		}

		$days_template_id = array();
		for($i=1; $i<=7; $i++){
			$name_ = "day".$i."_template_id";
			//$week_template->days_template_id[$i] = $week_template->$name_;
			$days_template_id[$i] = $week_template->$name_;
		}
		$week_template->days_template_id = $days_template_id;
	}


	public function save(Request $request)
	{
		$data = $request->get('week_template');

		if(!$data){
			return $this->sendResponse(null, Lang::get('week_template.errors.empty_week_template_data'), false);
		}

		if( isset($data['new']) AND $data['new']==true ){
			$week_template = new WeekTemplate;
		}else{
			$week_template = WeekTemplate::find($data['id']);	
		}

		
		if(!$week_template){
			return $this->sendResponse(null, Lang::get('week_template.errors.week_template_not_existe'), false);
		}

		if(isset($data['active'])) $week_template->active = $data['active'];
		if(isset($data['week_template_name_ar'])) $week_template->week_template_name_ar = $data['week_template_name_ar'];
		if(isset($data['week_template_name_en'])) $week_template->week_template_name_en = $data['week_template_name_en'];
		if(isset($data['school_id'])) $week_template->school_id = $data['school_id'];
		if(isset($data['level_class_id'])) $week_template->level_class_id = $data['level_class_id'];

		if( isset($data['days_template_id']) AND !empty($data['days_template_id']) ){
			foreach ($data['days_template_id'] as $day_id => $day_template_id) {
				$name_ = "day{$day_id}_template_id";
				$week_template->$name_ = $day_template_id;
			}
		}
		$week_template->save();

		return $this->sendResponse($week_template, Lang::get('global.success.save'), true);
	}




	public function delete(Request $request)
	{
		$week_template_id = $request->get('week_template_id');

		if(!$week_template_id){
			return $this->sendResponse(null, Lang::get('week_template.errors.empty_week_template_id'), false);
		}

		$destroy = WeekTemplate::destroy($week_template_id);
		if(!$destroy){
			return $this->sendResponse(null, Lang::get('week_template.errors.week_template_not_existe'), false);
		}

		return $this->sendResponse(null, Lang::get('global.success.delete'), true);
	}


}
