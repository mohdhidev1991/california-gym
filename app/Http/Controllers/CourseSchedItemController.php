<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCourseSchedItemRequest;
use App\Http\Requests\UpdateCourseSchedItemRequest;
use App\Libraries\Repositories\CourseSchedItemRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CourseSchedItemController extends AppBaseController
{

	/** @var  CourseSchedItemRepository */
	private $courseSchedItemRepository;

	function __construct(CourseSchedItemRepository $courseSchedItemRepo)
	{
		$this->courseSchedItemRepository = $courseSchedItemRepo;
	}

	/**
	 * Display a listing of the CourseSchedItem.
	 *
	 * @return Response
	 */
	public function index()
	{
		$courseSchedItems = $this->courseSchedItemRepository->paginate(10);

		return view('courseSchedItems.index')
			->with('courseSchedItems', $courseSchedItems);
	}

	/**
	 * Show the form for creating a new CourseSchedItem.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('courseSchedItems.create');
	}

	/**
	 * Store a newly created CourseSchedItem in storage.
	 *
	 * @param CreateCourseSchedItemRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCourseSchedItemRequest $request)
	{
		$input = $request->all();

		$courseSchedItem = $this->courseSchedItemRepository->create($input);

		Flash::success('CourseSchedItem saved successfully.');

		return redirect(route('courseSchedItems.index'));
	}

	/**
	 * Display the specified CourseSchedItem.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$courseSchedItem = $this->courseSchedItemRepository->find($id);

		if(empty($courseSchedItem))
		{
			Flash::error('CourseSchedItem not found');

			return redirect(route('courseSchedItems.index'));
		}

		return view('courseSchedItems.show')->with('courseSchedItem', $courseSchedItem);
	}

	/**
	 * Show the form for editing the specified CourseSchedItem.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$courseSchedItem = $this->courseSchedItemRepository->find($id);

		if(empty($courseSchedItem))
		{
			Flash::error('CourseSchedItem not found');

			return redirect(route('courseSchedItems.index'));
		}

		return view('courseSchedItems.edit')->with('courseSchedItem', $courseSchedItem);
	}

	/**
	 * Update the specified CourseSchedItem in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCourseSchedItemRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCourseSchedItemRequest $request)
	{
		$courseSchedItem = $this->courseSchedItemRepository->find($id);

		if(empty($courseSchedItem))
		{
			Flash::error('CourseSchedItem not found');

			return redirect(route('courseSchedItems.index'));
		}

		$this->courseSchedItemRepository->updateRich($request->all(), $id);

		Flash::success('CourseSchedItem updated successfully.');

		return redirect(route('courseSchedItems.index'));
	}

	/**
	 * Remove the specified CourseSchedItem from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$courseSchedItem = $this->courseSchedItemRepository->find($id);

		if(empty($courseSchedItem))
		{
			Flash::error('CourseSchedItem not found');

			return redirect(route('courseSchedItems.index'));
		}

		$this->courseSchedItemRepository->delete($id);

		Flash::success('CourseSchedItem deleted successfully.');

		return redirect(route('courseSchedItems.index'));
	}
}
