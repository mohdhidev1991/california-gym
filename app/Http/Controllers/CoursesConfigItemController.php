<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCoursesConfigItemRequest;
use App\Http\Requests\UpdateCoursesConfigItemRequest;
use App\Libraries\Repositories\CoursesConfigItemRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CoursesConfigItemController extends AppBaseController
{

	/** @var  CoursesConfigItemRepository */
	private $coursesConfigItemRepository;

	function __construct(CoursesConfigItemRepository $coursesConfigItemRepo)
	{
		$this->coursesConfigItemRepository = $coursesConfigItemRepo;
	}

	/**
	 * Display a listing of the CoursesConfigItem.
	 *
	 * @return Response
	 */
	public function index()
	{
		$coursesConfigItems = $this->coursesConfigItemRepository->paginate(10);

		return view('coursesConfigItems.index')
			->with('coursesConfigItems', $coursesConfigItems);
	}

	/**
	 * Show the form for creating a new CoursesConfigItem.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('coursesConfigItems.create');
	}

	/**
	 * Store a newly created CoursesConfigItem in storage.
	 *
	 * @param CreateCoursesConfigItemRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCoursesConfigItemRequest $request)
	{
		$input = $request->all();

		$coursesConfigItem = $this->coursesConfigItemRepository->create($input);

		Flash::success('CoursesConfigItem saved successfully.');

		return redirect(route('coursesConfigItems.index'));
	}

	/**
	 * Display the specified CoursesConfigItem.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$coursesConfigItem = $this->coursesConfigItemRepository->find($id);

		if(empty($coursesConfigItem))
		{
			Flash::error('CoursesConfigItem not found');

			return redirect(route('coursesConfigItems.index'));
		}

		return view('coursesConfigItems.show')->with('coursesConfigItem', $coursesConfigItem);
	}

	/**
	 * Show the form for editing the specified CoursesConfigItem.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$coursesConfigItem = $this->coursesConfigItemRepository->find($id);

		if(empty($coursesConfigItem))
		{
			Flash::error('CoursesConfigItem not found');

			return redirect(route('coursesConfigItems.index'));
		}

		return view('coursesConfigItems.edit')->with('coursesConfigItem', $coursesConfigItem);
	}

	/**
	 * Update the specified CoursesConfigItem in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCoursesConfigItemRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCoursesConfigItemRequest $request)
	{
		$coursesConfigItem = $this->coursesConfigItemRepository->find($id);

		if(empty($coursesConfigItem))
		{
			Flash::error('CoursesConfigItem not found');

			return redirect(route('coursesConfigItems.index'));
		}

		$this->coursesConfigItemRepository->updateRich($request->all(), $id);

		Flash::success('CoursesConfigItem updated successfully.');

		return redirect(route('coursesConfigItems.index'));
	}

	/**
	 * Remove the specified CoursesConfigItem from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$coursesConfigItem = $this->coursesConfigItemRepository->find($id);

		if(empty($coursesConfigItem))
		{
			Flash::error('CoursesConfigItem not found');

			return redirect(route('coursesConfigItems.index'));
		}

		$this->coursesConfigItemRepository->delete($id);

		Flash::success('CoursesConfigItem deleted successfully.');

		return redirect(route('coursesConfigItems.index'));
	}
}
