<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCoursesConfigTemplateRequest;
use App\Http\Requests\UpdateCoursesConfigTemplateRequest;
use App\Libraries\Repositories\CoursesConfigTemplateRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CoursesConfigTemplateController extends AppBaseController
{

	/** @var  CoursesConfigTemplateRepository */
	private $coursesConfigTemplateRepository;

	function __construct(CoursesConfigTemplateRepository $coursesConfigTemplateRepo)
	{
		$this->coursesConfigTemplateRepository = $coursesConfigTemplateRepo;
	}

	/**
	 * Display a listing of the CoursesConfigTemplate.
	 *
	 * @return Response
	 */
	public function index()
	{
		$coursesConfigTemplates = $this->coursesConfigTemplateRepository->paginate(10);

		return view('coursesConfigTemplates.index')
			->with('coursesConfigTemplates', $coursesConfigTemplates);
	}

	/**
	 * Show the form for creating a new CoursesConfigTemplate.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('coursesConfigTemplates.create');
	}

	/**
	 * Store a newly created CoursesConfigTemplate in storage.
	 *
	 * @param CreateCoursesConfigTemplateRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCoursesConfigTemplateRequest $request)
	{
		$input = $request->all();

		$coursesConfigTemplate = $this->coursesConfigTemplateRepository->create($input);

		Flash::success('CoursesConfigTemplate saved successfully.');

		return redirect(route('coursesConfigTemplates.index'));
	}

	/**
	 * Display the specified CoursesConfigTemplate.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$coursesConfigTemplate = $this->coursesConfigTemplateRepository->find($id);

		if(empty($coursesConfigTemplate))
		{
			Flash::error('CoursesConfigTemplate not found');

			return redirect(route('coursesConfigTemplates.index'));
		}

		return view('coursesConfigTemplates.show')->with('coursesConfigTemplate', $coursesConfigTemplate);
	}

	/**
	 * Show the form for editing the specified CoursesConfigTemplate.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$coursesConfigTemplate = $this->coursesConfigTemplateRepository->find($id);

		if(empty($coursesConfigTemplate))
		{
			Flash::error('CoursesConfigTemplate not found');

			return redirect(route('coursesConfigTemplates.index'));
		}

		return view('coursesConfigTemplates.edit')->with('coursesConfigTemplate', $coursesConfigTemplate);
	}

	/**
	 * Update the specified CoursesConfigTemplate in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCoursesConfigTemplateRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCoursesConfigTemplateRequest $request)
	{
		$coursesConfigTemplate = $this->coursesConfigTemplateRepository->find($id);

		if(empty($coursesConfigTemplate))
		{
			Flash::error('CoursesConfigTemplate not found');

			return redirect(route('coursesConfigTemplates.index'));
		}

		$this->coursesConfigTemplateRepository->updateRich($request->all(), $id);

		Flash::success('CoursesConfigTemplate updated successfully.');

		return redirect(route('coursesConfigTemplates.index'));
	}

	/**
	 * Remove the specified CoursesConfigTemplate from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$coursesConfigTemplate = $this->coursesConfigTemplateRepository->find($id);

		if(empty($coursesConfigTemplate))
		{
			Flash::error('CoursesConfigTemplate not found');

			return redirect(route('coursesConfigTemplates.index'));
		}

		$this->coursesConfigTemplateRepository->delete($id);

		Flash::success('CoursesConfigTemplate deleted successfully.');

		return redirect(route('coursesConfigTemplates.index'));
	}
}
