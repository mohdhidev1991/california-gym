<?php namespace App\Http\Controllers\FRONT;

use App\Http\Requests;
use App\Http\Requests\CreateSchoolRequest;
use App\Http\Requests\UpdateSchoolRequest;
use App\Libraries\Repositories\SchoolRepository;
use App\Models\CourseSchedItem;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use Dingo\Api\Http\Request;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Session;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class SchoolFRONTController extends AppBaseController
{

	/** @var  SchoolRepository */
	private $schoolRepository;

	function __construct(SchoolRepository $schoolRepo)
	{
		$this->schoolRepository = $schoolRepo;
		$user_id = Auth::user()->id;
		$user_lang = app()->getLocale();
	}

	public function getSearchGenerator()
	{
		return "xx";
		$items = [];
		$items['schools'] = School::join('school_employee','school_employee.school_id','=','school.id')
			->where('school_employee.rea_user_id','=',$user_id)
			->lists('school.school_name_'.$user_lang,'school.id','school.id')->toArray();
		$items = $this->getFormSearchGenerator();
		extract($items);
		return view('schools.generator_search')->with(compact('schools'));
	}

}


