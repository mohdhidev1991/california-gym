<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateHijraDateBaseRequest;
use App\Http\Requests\UpdateHijraDateBaseRequest;
use App\Libraries\Repositories\HijraDateBaseRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class HijraDateBaseController extends AppBaseController
{

	/** @var  HijraDateBaseRepository */
	private $hijraDateBaseRepository;

	function __construct(HijraDateBaseRepository $hijraDateBaseRepo)
	{
		$this->hijraDateBaseRepository = $hijraDateBaseRepo;
	}

	/**
	 * Display a listing of the HijraDateBase.
	 *
	 * @return Response
	 */
	public function index()
	{
		$hijraDateBases = $this->hijraDateBaseRepository->paginate(10);

		return view('hijraDateBases.index')
			->with('hijraDateBases', $hijraDateBases);
	}

	/**
	 * Show the form for creating a new HijraDateBase.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('hijraDateBases.create');
	}

	/**
	 * Store a newly created HijraDateBase in storage.
	 *
	 * @param CreateHijraDateBaseRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateHijraDateBaseRequest $request)
	{
		$input = $request->all();

		$hijraDateBase = $this->hijraDateBaseRepository->create($input);

		Flash::success('HijraDateBase saved successfully.');

		return redirect(route('hijraDateBases.index'));
	}

	/**
	 * Display the specified HijraDateBase.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hijraDateBase = $this->hijraDateBaseRepository->find($id);

		if(empty($hijraDateBase))
		{
			Flash::error('HijraDateBase not found');

			return redirect(route('hijraDateBases.index'));
		}

		return view('hijraDateBases.show')->with('hijraDateBase', $hijraDateBase);
	}

	/**
	 * Show the form for editing the specified HijraDateBase.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$hijraDateBase = $this->hijraDateBaseRepository->find($id);

		if(empty($hijraDateBase))
		{
			Flash::error('HijraDateBase not found');

			return redirect(route('hijraDateBases.index'));
		}

		return view('hijraDateBases.edit')->with('hijraDateBase', $hijraDateBase);
	}

	/**
	 * Update the specified HijraDateBase in storage.
	 *
	 * @param  int              $id
	 * @param UpdateHijraDateBaseRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateHijraDateBaseRequest $request)
	{
		$hijraDateBase = $this->hijraDateBaseRepository->find($id);

		if(empty($hijraDateBase))
		{
			Flash::error('HijraDateBase not found');

			return redirect(route('hijraDateBases.index'));
		}

		$this->hijraDateBaseRepository->updateRich($request->all(), $id);

		Flash::success('HijraDateBase updated successfully.');

		return redirect(route('hijraDateBases.index'));
	}

	/**
	 * Remove the specified HijraDateBase from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$hijraDateBase = $this->hijraDateBaseRepository->find($id);

		if(empty($hijraDateBase))
		{
			Flash::error('HijraDateBase not found');

			return redirect(route('hijraDateBases.index'));
		}

		$this->hijraDateBaseRepository->delete($id);

		Flash::success('HijraDateBase deleted successfully.');

		return redirect(route('hijraDateBases.index'));
	}
}
