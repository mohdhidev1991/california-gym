<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\ReaUser;
use App\Libraries\Repositories\ReaUserRepository;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReaUserController extends AppBaseController
{

	/** @var  ReaUserRepository */
	private $reaUserRepository;

	function __construct(ReaUserRepository $reaUserRepo)
	{
		parent::__construct();
		$this->reaUserRepository = $reaUserRepo;
	}


	function activation($token)
	{
		$data = [
			'already_active' => false,
			'exipred' => false,
			'activation' => false,
			'invalide' => false,
		];


		$query = DB::table('rea_user')->select('id')->where('email_token',$token);
		//$rea_user = DB::table('rea_user')->where('email_token',$token)->count();

		$result = $query->first();

		if($result){
			$rea_user = ReaUser::find($result->id);

			$EmailTokenExipiry = Carbon::createFromFormat('Y-m-d H:i:s',$rea_user->email_token_expiry_date);
			$diffExpireDate = Carbon::now()->diffInSeconds($EmailTokenExipiry,false);

			if( $diffExpireDate<0 ){
				$data['exipred'] = true;
			}elseif( $rea_user->valide_email=='Y' ){
				$data['already_active'] = true;
			}else{
				auth()->login($rea_user);
				$data['activation'] = true;
				$rea_user->valide_email = "Y";
				$rea_user->save();
			}
		}else{
			$data['invalide'] = true;
		}



		

		return view('auth/activation', $data);
	}






	function reset_password($token)
	{
		$data = [
			'exipred' => false,
			'invalide' => false,
			'valide' => false,
		];


		$query = DB::table('rea_user')->select('id')->where('remember_token',$token);

		$result = $query->first();

		if($result){
			$rea_user = ReaUser::find($result->id);

			$ResetTokenExipiry = Carbon::createFromFormat('Y-m-d H:i:s',$rea_user->remember_token_expiry_date);
			$diffExpireDate = Carbon::now()->diffInSeconds($ResetTokenExipiry,false);

			if( $diffExpireDate<0 ){
				$data['exipred'] = true;
			}else{
				$data['valide'] = true;
				auth()->login($rea_user);
				if($rea_user->active!="Y"){
					$rea_user->active = "Y";
					$rea_user->save();	
				}
			}
		}else{
			$data['invalide'] = true;
		}



		

		return view('auth/reset_password', $data);
	}

	


}
