<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRserviceRequest;
use App\Http\Requests\UpdateRserviceRequest;
use App\Libraries\Repositories\RserviceRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class RserviceController extends AppBaseController
{

	/** @var  RserviceRepository */
	private $rserviceRepository;

	function __construct(RserviceRepository $rserviceRepo)
	{
		$this->rserviceRepository = $rserviceRepo;
	}


    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = Rservice::select([
            'rservice.*'
        ]);

        if(isset($rservice_id)){
            $query->where('rservice.id',rservice_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('rservice.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('rservice.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        $count = $query->count();
        $return = ['rservices' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }




    public function save(Request $request)
    {
        $data = $request->get('rservice');

        if(!$data){
            return $this->sendResponse(null, Lang::get('rservice.errors.empty_rservice_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $rservice = new Rservice;
        }else{
            $rservice = Rservice::find($data['id']);
        }


        if(!$rservice){
            return $this->sendResponse(null, Lang::get('rservice.errors.rservice_not_existe'), false);
        }

        if(isset($data['active'])) $rservice->active = $data['active'];
        if(isset($data['lookup_code'])) $rservice->lookup_code = $data['lookup_code'];
        if(isset($data['rservice_name'])) $rservice->rservice_name = $data['rservice_name'];
        $rservice->save();

        return $this->sendResponse($rservice, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $rservice_id = $request->get('rservice_id');

        if(!$rservice_id){
            return $this->sendResponse(null, Lang::get('rservice.errors.empty_rservice_id'), false);
        }

        $destroy = Rservice::destroy($rservice_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('rservice.errors.rservice_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
