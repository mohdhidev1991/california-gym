<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRserviceStudentRequest;
use App\Http\Requests\UpdateRserviceStudentRequest;
use App\Libraries\Repositories\RserviceStudentRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Lang;

class RserviceStudentController extends AppBaseController
{

	/** @var  RserviceStudentRepository */
	private $rserviceStudentRepository;

	function __construct(RserviceStudentRepository $rserviceStudentRepo)
	{
		$this->rserviceStudentRepository = $rserviceStudentRepo;
	}


    public function get($params = null)
    {

        if($params){
            $parametres = decode_url_params($params);
            extract($parametres);
        }
        $query = RserviceStudent::select([
            'rserviceStudent.*'
        ]);

        if(isset($rserviceStudent_id)){
            $query->where('rserviceStudent.id',rserviceStudent_id);
            $single_item = $query->first();
            if(!$single_item){
                return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
            }
            return $this->sendResponse($single_item, Lang::get('global.success.results'), true);
        }


        if(!isset($active) OR ($active!='all') ){
            $query->where('rserviceStudent.active',"Y");
        }


        if( isset($limit) ){
            $query->take($limit);
        }
        if( isset($page) AND isset($limit) ){
            $skip = ($page-1)*$limit;
            $query->skip($skip);
        }



        $query->orderBy('rserviceStudent.id', 'ASC');
        $result = $query->get();

        if(!$result){
            return $this->sendResponse(null, Lang::get('global.errors.empty'), false);
        }
        $count = $query->count();
        $return = ['rserviceStudents' => $result, 'total' => $count ];
        return $this->sendResponse($return, Lang::get('global.success.results'), true);
    }




    public function save(Request $request)
    {
        $data = $request->get('rserviceStudent');

        if(!$data){
            return $this->sendResponse(null, Lang::get('rserviceStudent.errors.empty_rserviceStudent_data'), false);
        }

        if( isset($data['new']) AND $data['new']==true ){
            $rserviceStudent = new RserviceStudent;
        }else{
            $rserviceStudent = RserviceStudent::find($data['id']);
        }


        if(!$rserviceStudent){
            return $this->sendResponse(null, Lang::get('rserviceStudent.errors.rserviceStudent_not_existe'), false);
        }

        if(isset($data['active'])) $rserviceStudent->active = $data['active'];
        if(isset($data['lookup_code'])) $rserviceStudent->lookup_code = $data['lookup_code'];
        if(isset($data['rserviceStudent_name'])) $rserviceStudent->rserviceStudent_name = $data['rserviceStudent_name'];
        $rserviceStudent->save();

        return $this->sendResponse($rserviceStudent, Lang::get('global.success.save'), true);
    }


    public function delete(Request $request)
    {
        $rserviceStudent_id = $request->get('rserviceStudent_id');

        if(!$rserviceStudent_id){
            return $this->sendResponse(null, Lang::get('rserviceStudent.errors.empty_rserviceStudent_id'), false);
        }

        $destroy = RserviceStudent::destroy($rserviceStudent_id);
        if(!$destroy){
            return $this->sendResponse(null, Lang::get('rserviceStudent.errors.rserviceStudent_not_existe'), false);
        }

        return $this->sendResponse(null, Lang::get('global.success.delete'), true);
    }
}
