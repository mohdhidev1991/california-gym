<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSdepartmentRequest;
use App\Http\Requests\UpdateSdepartmentRequest;
use App\Libraries\Repositories\SdepartmentRepository;
use Flash;
use App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SdepartmentController extends AppBaseController
{

	/** @var  SdepartmentRepository */
	private $sdepartmentRepository;

	function __construct(SdepartmentRepository $sdepartmentRepo)
	{
		$this->sdepartmentRepository = $sdepartmentRepo;
	}

	/**
	 * Display a listing of the Sdepartment.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sdepartments = $this->sdepartmentRepository->paginate(10);

		return view('sdepartments.index')
			->with('sdepartments', $sdepartments);
	}

	/**
	 * Show the form for creating a new Sdepartment.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sdepartments.create');
	}

	/**
	 * Store a newly created Sdepartment in storage.
	 *
	 * @param CreateSdepartmentRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSdepartmentRequest $request)
	{
		$input = $request->all();

		$sdepartment = $this->sdepartmentRepository->create($input);

		Flash::success('Sdepartment saved successfully.');

		return redirect(route('sdepartments.index'));
	}

	/**
	 * Display the specified Sdepartment.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$sdepartment = $this->sdepartmentRepository->find($id);

		if(empty($sdepartment))
		{
			Flash::error('Sdepartment not found');

			return redirect(route('sdepartments.index'));
		}

		return view('sdepartments.show')->with('sdepartment', $sdepartment);
	}

	/**
	 * Show the form for editing the specified Sdepartment.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$sdepartment = $this->sdepartmentRepository->find($id);

		if(empty($sdepartment))
		{
			Flash::error('Sdepartment not found');

			return redirect(route('sdepartments.index'));
		}

		return view('sdepartments.edit')->with('sdepartment', $sdepartment);
	}

	/**
	 * Update the specified Sdepartment in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSdepartmentRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSdepartmentRequest $request)
	{
		$sdepartment = $this->sdepartmentRepository->find($id);

		if(empty($sdepartment))
		{
			Flash::error('Sdepartment not found');

			return redirect(route('sdepartments.index'));
		}

		$this->sdepartmentRepository->updateRich($request->all(), $id);

		Flash::success('Sdepartment updated successfully.');

		return redirect(route('sdepartments.index'));
	}

	/**
	 * Remove the specified Sdepartment from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$sdepartment = $this->sdepartmentRepository->find($id);

		if(empty($sdepartment))
		{
			Flash::error('Sdepartment not found');

			return redirect(route('sdepartments.index'));
		}

		$this->sdepartmentRepository->delete($id);

		Flash::success('Sdepartment deleted successfully.');

		return redirect(route('sdepartments.index'));
	}
}
