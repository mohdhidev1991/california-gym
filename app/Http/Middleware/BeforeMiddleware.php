<?php
/**
 * Created by PhpStorm.
 * User: Aziz Trabelsi
 * Date: 4/18/2016
 * Time: 9:18 PM
 */

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class BeforeMiddleware
{
    public function handle($request, Closure $next)
    {
        DB::enableQueryLog();

        DB::listen(
            function ($sql) {

                // $sql is an object with the properties:
                //  sql: The query
                //  bindings: the sql query variables
                //  time: The execution time for the query
                //  connectionName: The name of the connection

                // To save the executed queries to file:
                // Process the sql and the bindings:
                foreach ($sql->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else {
                        if (is_string($binding)) {
                            $sql->bindings[$i] = "'$binding'";
                        }
                    }
                }


                

                // Insert bindings into query
                $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);

                $query = vsprintf($query, $sql->bindings);
                // Save the query to file


                if(Auth::check())
                    $logFile = fopen(storage_path('logs/queries/' . date('Y-m-d').'__userId_'. Auth::user()->id . '__queries.log'), 'a+');
                else
                    $logFile = fopen(storage_path('logs/queries/' . date('Y-m-d').'__guest__queries.log'), 'a+');
                fwrite($logFile, '[ '.date('Y-m-d H:i:s') . ' ] : SQL [ ' . $query . ' ]' . "\n");
                fclose($logFile);
            }
        );
        return $next($request);
    }

}