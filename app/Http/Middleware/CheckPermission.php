<?php

namespace App\Http\Middleware;

use Closure;

class CheckPermission
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        /*if (! $request->user()->hasRole($role)) {
            // Redirect...
        }*/
        echo $permission;
        exit();

        return $next($request);
    }

}
