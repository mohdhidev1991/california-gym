<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class CreateToken extends Request
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'login'    => 'required_without:sim_card_sn',
            'password'    => 'required_with:login',
            'sim_card_sn' => 'required_without_all:login,password',
            "lang" => "required"
        ];
    }
}
