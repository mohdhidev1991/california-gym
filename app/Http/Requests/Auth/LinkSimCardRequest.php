<?php
/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 24/02/16
 * Time: 06:44 ص
 */

namespace App\Http\Requests\Auth;


use App\Http\Requests\Request;

class LinkSimCardRequest extends Request
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'sim_card_sn' => 'required',
        ];
    }
}