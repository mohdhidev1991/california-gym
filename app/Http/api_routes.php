<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/



//Function key: Check if rea user is registred
$api->get('users/checkRegistration/{idn_type}/{idn}', [
    "uses"  => "ReaUserAPIController@checkRegistration",
    "as"    => "api.users.check_registration",
]);

$api->group([ 'prefix' => "Guest", "parameters"    => ["guest" => true ] ], function ($api) {
    $api->get('IdnType/get/{params?}', [
        "uses"  => "IdnTypeAPIController@get",
        "as"    => "api.guest.idn_type.get",
        "parameters"    => ["guest" => true ],
    ]);
});

$api->get('Translate/lang/{lang_code}.js', [
    "uses"  => "TranslateAPIController@get_lang",
    "as"    => "api.translate.get_lang",
]);

$api->group([ 'middleware' => ['web'] ], function ($api) {
    $api->post('web_register', [
        "uses"  => "AuthAPIController@web_register",
        "as"    => "api.auth.web_register",
    ]);

    $api->post('SchoolRegistration/submit', [
        "uses"  => "SchoolRegistrationAPIController@submit",
        "as"    => "api.school_registration.submit",
    ]);

        
    $api->get('ReaUser/me', [
        "uses"  => "ReaUserAPIController@me",
        "as"    => "api.rea_user.me"
    ]);

});

$api->group([ 'namespace' => 'Auth', 'middleware' => ['web'] ], function ($api) {

    

    $api->get('fb_redirect', [
        "uses"  => "AuthAPIController@fb_redirect",
        "as"    => "api.auth.fb_redirect",
    ]);
    $api->get('fb_callback', [
        "uses"  => "AuthAPIController@fb_callback",
        "as"    => "api.auth.fb_callback",
    ]);

    $api->get('google_redirect', [
        "uses"  => "AuthAPIController@google_redirect",
        "as"    => "api.auth.google_redirect",
    ]);
    $api->get('google_callback', [
        "uses"  => "AuthAPIController@google_callback",
        "as"    => "api.auth.google_callback",
    ]);

    $api->get('twitter_redirect', [
        "uses"  => "AuthAPIController@twitter_redirect",
        "as"    => "api.auth.twitter_redirect",
    ]);
    $api->get('twitter_callback', [
        "uses"  => "AuthAPIController@twitter_callback",
        "as"    => "api.auth.twitter_callback",
    ]);
    $api->post('weblogin', 'AuthAPIController@weblogin');
    $api->post('webregister', 'AuthAPIController@webregister');
    $api->post('reset_password', 'AuthAPIController@send_remember_password');

});

// Set our namespace for the underlying routes
$api->group(['namespace' => 'Auth'], function ($api) {

    //Login route
    $api->post('auth', 'AuthAPIController@authenticate');
    $api->post('register', 'AuthAPIController@register');
    //Dogs! All routes in here are protected and thus need a valid token
    //$api->group( [ 'protected' => true, 'middleware' => 'jwt.refresh' ], function ($api) {
    $api->group([ 'middleware' => 'jwt.auth' ], function ($api) {
        $api->get('users/me', 'AuthAPIController@me');
        $api->get('validate_token', 'AuthAPIController@validateToken');
        // Function key : BF 228 / Link the account with the sim card
        $api->get('users/linkSimCard/{sim_card_sn}', 'AuthAPIController@LinkSimCard');
    });


    $api->get('auth/fb_token/{token}', [
        "uses"  => "AuthAPIController@auth_by_fb_token",
        "as"    => "api.auth.auth_by_fb_token",
    ]);
    $api->get('auth/google_token/{token}', [
        "uses"  => "AuthAPIController@auth_by_google_token",
        "as"    => "api.auth.auth_by_google_token",
    ]);
    $api->get('auth/twitter_token/{token}/{secret}', [
        "uses"  => "AuthAPIController@auth_by_twitter_token",
        "as"    => "api.auth.auth_by_twitter_token",
    ]);

});


$api->group( [ 'middleware' => ['auth_or_token'] ], function ($api) {
    $api->get('users/getPhoneKey', 'ReaUserAPIController@getPhoneKeys');
    $api->get('users/setPhoneKey/{type}/{key}', 'ReaUserAPIController@setPhoneKey');
    $api->get('users/unsetPhoneKey', 'ReaUserAPIController@unsetPhoneKey');
    $api->get('Alert/getReceivers', 'AlertAPIController@getReceivers');
    $api->post('Alert/sendByReceiver', 'AlertAPIController@sendByReceiver');

    /* Crud API */
    $api->get('School/get/{params?}', [
        "uses"  => "SchoolAPIController@get",
        "as"    => "api.school.get",
    ]);
    //Function key: Get My Schools
    $api->get('School/get_my_schools', [
        "uses"  => "SchoolEmployeeAPIController@get_my_schools",
        "as"    => "api.school.get_my_schools",
    ]);
    $api->post('School/save', [
        "uses"  => "SchoolAPIController@save",
        "as"    => "api.school.save",
    ]);
    $api->post('School/delete', [
        "uses"  => "SchoolAPIController@delete",
        "as"    => "api.school.delete",
    ]);



    $api->get('Genre/get/{params?}', [
        "uses"  => "GenreAPIController@get",
        "as"    => "api.genre.get",
    ]);
    $api->post('Genre/save', [
        "uses"  => "GenreAPIController@save",
        "as"    => "api.genre.save",
    ]);
    $api->post('Genre/delete', [
        "uses"  => "GenreAPIController@delete",
        "as"    => "api.genre.delete",
    ]);




    $api->get('SchoolType/get/{params?}', [
        "uses"  => "SchoolTypeAPIController@get",
        "as"    => "api.school_type.get",
    ]);
    $api->post('SchoolType/save', [
        "uses"  => "SchoolTypeAPIController@save",
        "as"    => "api.school_type.save",
    ]);
    $api->post('SchoolType/delete', [
        "uses"  => "SchoolLevelAPIController@delete",
        "as"    => "api.school_type.delete",
    ]);


    $api->get('SchoolLevel/get/{params?}', [
        "uses"  => "SchoolLevelAPIController@get",
        "as"    => "api.schoo_level.get",
    ]);
    $api->post('SchoolLevel/save', [
        "uses"  => "SchoolLevelAPIController@save",
        "as"    => "api.school_level.save",
    ]);
    $api->post('SchoolLevel/delete', [
        "uses"  => "SchoolLevelAPIController@delete",
        "as"    => "api.school_level.delete",
    ]);




    $api->get('SchoolPeriod/get/{params?}', [
        "uses"  => "SchoolPeriodAPIController@get",
        "as"    => "api.schoo_period.get",
    ]);
    $api->post('SchoolPeriod/save', [
        "uses"  => "SchoolPeriodAPIController@save",
        "as"    => "api.schoo_period.save",
    ]);
    $api->post('SchoolPeriod/delete', [
        "uses"  => "SchoolPeriodAPIController@delete",
        "as"    => "api.schoo_period.delete",
    ]);


    $api->get('Period/get/{params?}', [
        "uses"  => "PeriodAPIController@get",
        "as"    => "api.period.get",
    ]);
    $api->post('Period/save', [
        "uses"  => "PeriodAPIController@save",
        "as"    => "api.period.save",
    ]);
    $api->post('Period/delete', [
        "uses"  => "PeriodAPIController@delete",
        "as"    => "api.period.delete",
    ]);


    $api->get('LevelClass/get/{params?}', [
        "uses"  => "LevelClassAPIController@get",
        "as"    => "api.level_class.get",
    ]);
    $api->post('LevelClass/save', [
        "uses"  => "LevelClassAPIController@save",
        "as"    => "api.level_class.save",
    ]);
    $api->post('LevelClass/delete', [
        "uses"  => "LevelClassAPIController@delete",
        "as"    => "api.level_class.delete",
    ]);


    $api->get('Holiday/get/{params?}', [
        "uses"  => "HolidayAPIController@get",
        "as"    => "api.holiday.get",
    ]);
    $api->post('Holiday/save', [
        "uses"  => "HolidayAPIController@save",
        "as"    => "api.holiday.save",
    ]);
    $api->post('Holiday/delete', [
        "uses"  => "HolidayAPIController@delete",
        "as"    => "api.holiday.delete",
    ]);


    $api->get('Language/get/{params?}', [
        "uses"  => "LanguageAPIController@get",
        "as"    => "api.language.get",
    ]);
    $api->post('Language/save', [
        "uses"  => "LanguageAPIController@save",
        "as"    => "api.language.save",
    ]);
    $api->post('Language/delete', [
        "uses"  => "LanguageAPIController@delete",
        "as"    => "api.language.delete",
    ]);







    $api->get('Gremark/get/{params?}', [
        "uses"  => "GRemarkAPIController@get",
        "as"    => "api.gremark.get",
    ]);
    $api->post('Gremark/save', [
        "uses"  => "GRemarkAPIController@save",
        "as"    => "api.gremark.save",
    ]);
    $api->post('Gremark/delete', [
        "uses"  => "GRemarkAPIController@delete",
        "as"    => "api.gremark.delete",
    ]);



    $api->get('Relship/get/{params?}', [
        "uses"  => "RelshipAPIController@get",
        "as"    => "api.relship.get",
    ]);
    $api->post('Relship/save', [
        "uses"  => "RelshipAPIController@save",
        "as"    => "api.relship.save",
    ]);
    $api->post('Relship/delete', [
        "uses"  => "RelshipAPIController@delete",
        "as"    => "api.relship.delete",
    ]);




    $api->get('Remark/get/{params?}', [
        "uses"  => "RemarkAPIController@get",
        "as"    => "api.remark.get",
    ]);
    $api->post('Remark/save', [
        "uses"  => "RemarkAPIController@save",
        "as"    => "api.remark.save",
    ]);
    $api->post('Remark/delete', [
        "uses"  => "RemarkAPIController@delete",
        "as"    => "api.remark.delete",
    ]);

    $api->get('LevelsTemplate/get/{params?}', [
        "uses"  => "LevelsTemplateAPIController@get",
        "as"    => "api.levels_template.get",
    ]);
    $api->post('LevelsTemplate/save', [
        "uses"  => "LevelsTemplateAPIController@save",
        "as"    => "api.levels_template.save",
    ]);
    $api->post('LevelsTemplate/delete', [
        "uses"  => "LevelsTemplateAPIController@delete",
        "as"    => "api.levels_template.delete",
    ]);


    $api->get('CoursesTemplate/get/{params?}', [
        "uses"  => "CoursesTemplateAPIController@get",
        "as"    => "api.courses_template.get",
    ]);


    $api->get('CoursesConfigTemplate/get/{params?}', [
        "uses"  => "CoursesConfigTemplateAPIController@get",
        "as"    => "api.courses_config_template.get",
    ]);
    $api->post('CoursesConfigTemplate/save', [
        "uses"  => "CoursesConfigTemplateAPIController@save",
        "as"    => "api.courses_config_template.save",
    ]);
    $api->post('CoursesConfigTemplate/delete', [
        "uses"  => "CoursesConfigTemplateAPIController@delete",
        "as"    => "api.courses_config_template.delete",
    ]);


    // Sdepartment ( Q0016 included with option school_id)
    $api->get('Sdepartment/get/{params?}', [
        "uses"  => "SdepartmentAPIController@get",
        "as"    => "api.sdepartment.get",
    ]);
    $api->post('Sdepartment/save', [
        "uses"  => "SdepartmentAPIController@save",
        "as"    => "api.sdepartment.save",
    ]);
    $api->post('Sdepartment/delete', [
        "uses"  => "SdepartmentAPIController@delete",
        "as"    => "api.sdepartment.delete",
    ]);



    $api->get('TService/get/{params?}', [
        "uses"  => "TServiceAPIController@get",
        "as"    => "api.tservice.get",
    ]);
    $api->post('TService/save', [
        "uses"  => "TServiceAPIController@save",
        "as"    => "api.tservice.save",
    ]);
    $api->post('TService/delete', [
        "uses"  => "TServiceAPIController@delete",
        "as"    => "api.tservice.delete",
    ]);




    $api->get('SchoolYear/get/{params?}', [
        "uses"  => "SchoolYearAPIController@get",
        "as"    => "api.school_year.get",
    ]);
    $api->post('SchoolYear/save', [
        "uses"  => "SchoolYearAPIController@save",
        "as"    => "api.school_year.save",
    ]);
    $api->post('SchoolYear/delete', [
        "uses"  => "SchoolYearAPIController@delete",
        "as"    => "api.school_year.delete",
    ]);


    $api->get('SchoolClass/get/{params?}', [
        "uses"  => "SchoolClassAPIController@get",
        "as"    => "api.school_class.get",
    ]);
    $api->post('SchoolClass/save', [
        "uses"  => "SchoolClassAPIController@save",
        "as"    => "api.school_class.save",
    ]);
    $api->post('SchoolClass/delete', [
        "uses"  => "SchoolClassAPIController@delete",
        "as"    => "api.school_class.delete",
    ]);

    $api->get('SchoolClassCourse/get/{params?}', [
        "uses"  => "SchoolClassCourseAPIController@get",
        "as"    => "api.school_class_course.get",
    ]);
    $api->post('SchoolClassCourse/save', [
        "uses"  => "SchoolClassCourseAPIController@save",
        "as"    => "api.school_class_course.save",
    ]);
    $api->post('SchoolClassCourse/delete', [
        "uses"  => "SchoolClassCourseAPIController@delete",
        "as"    => "api.school_class_course.delete",
    ]);


    $api->get('CourseSchedItem/get/{params?}', [
        "uses"  => "CourseSchedItemAPIController@get",
        "as"    => "api.course_sched_item.get",
    ]);
    $api->post('CourseSchedItem/save', [
        "uses"  => "CourseSchedItemAPIController@save",
        "as"    => "api.course_sched_item.save",
    ]);
    $api->post('CourseSchedItem/multisave', [
        "uses"  => "CourseSchedItemAPIController@multisave",
        "as"    => "api.course_sched_item.multisave",
    ]);
    $api->post('CourseSchedItem/delete', [
        "uses"  => "CourseSchedItemAPIController@delete",
        "as"    => "api.course_sched_item.delete",
    ]);


    $api->get('WeekTemplate/get/{params?}', [
        "uses"  => "WeekTemplateAPIController@get",
        "as"    => "api.week_template.get",
    ]);
    $api->post('WeekTemplate/save', [
        "uses"  => "WeekTemplateAPIController@save",
        "as"    => "api.week_template.save",
    ]);
    $api->post('WeekTemplate/delete', [
        "uses"  => "WeekTemplateAPIController@delete",
        "as"    => "api.week_template.delete",
    ]);


    $api->get('AlertType/get/{params?}', [
        "uses"  => "AlertTypeAPIController@get",
        "as"    => "api.alert_type.get",
    ]);
    $api->post('AlertType/save', [
        "uses"  => "AlertTypeAPIController@save",
        "as"    => "api.alert_type.save",
    ]);
    $api->post('AlertType/delete', [
        "uses"  => "AlertTypeAPIController@delete",
        "as"    => "api.alert_type.delete",
    ]);


    $api->get('AlertStatus/get/{params?}', [
        "uses"  => "AlertStatusAPIController@get",
        "as"    => "api.alert_status.get",
    ]);
    $api->post('AlertStatus/save', [
        "uses"  => "AlertStatusAPIController@save",
        "as"    => "api.alert_status.save",
    ]);
    $api->post('AlertStatus/delete', [
        "uses"  => "AlertStatusAPIController@delete",
        "as"    => "api.alert_status.delete",
    ]);




    $api->get('MobileType/get/{params?}', [
        "uses"  => "MobileTypeAPIController@get",
        "as"    => "api.mobile_type.get",
    ]);
    $api->post('MobileType/save', [
        "uses"  => "MobileTypeAPIController@save",
        "as"    => "api.mobile_type.save",
    ]);
    $api->post('MobileType/delete', [
        "uses"  => "MobileTypeAPIController@delete",
        "as"    => "api.mobile_type.delete",
    ]);



    $api->get('Attendance/get/{params?}', [
        "uses"  => "AttendanceAPIController@get",
        "as"    => "api.attendance.get",
    ]);
    $api->post('Attendance/save', [
        "uses"  => "AttendanceAPIController@save",
        "as"    => "api.attendance.save",
    ]);
    $api->post('Attendance/delete', [
        "uses"  => "AttendanceAPIController@delete",
        "as"    => "api.attendance.delete",
    ]);

    $api->get('AttendanceStatus/get/{params?}', [
        "uses"  => "AttendanceStatusAPIController@get",
        "as"    => "api.attendance_status.get",
    ]);
    $api->post('AttendanceStatus/save', [
        "uses"  => "AttendanceStatusAPIController@save",
        "as"    => "api.attendance_status.save",
    ]);
    $api->post('AttendanceStatus/delete', [
        "uses"  => "AttendanceStatusAPIController@delete",
        "as"    => "api.attendance_status.delete",
    ]);



    $api->get('AttendanceType/get/{params?}', [
        "uses"  => "AttendanceTypeAPIController@get",
        "as"    => "api.attendance_type.get",
    ]);
    $api->post('AttendanceType/save', [
        "uses"  => "AttendanceTypeAPIController@save",
        "as"    => "api.attendance_type.save",
    ]);
    $api->post('AttendanceType/delete', [
        "uses"  => "AttendanceTypeAPIController@delete",
        "as"    => "api.attendance_type.delete",
    ]);

    $api->get('SchoolJob/get/{params?}', [
        "uses"  => "SchoolJobAPIController@get",
        "as"    => "api.school_job.get",
    ]);
    $api->post('SchoolJob/save', [
        "uses"  => "SchoolJobAPIController@save",
        "as"    => "api.school_job.save",
    ]);
    $api->post('SchoolJob/delete', [
        "uses"  => "SchoolJobAPIController@delete",
        "as"    => "api.school_job.delete",
    ]);

    $api->get('SchoolType/get/{params?}', [
        "uses"  => "SchoolTypeAPIController@get",
        "as"    => "api.school_type.get",
    ]);
    $api->post('SchoolType/save', [
        "uses"  => "SchoolTypeAPIController@save",
        "as"    => "api.school_type.save",
    ]);
    $api->post('SchoolType/delete', [
        "uses"  => "SchoolTypeAPIController@delete",
        "as"    => "api.school_type.delete",
    ]);




    $api->get('Course/get/{params?}', [
        "uses"  => "CourseAPIController@get",
        "as"    => "api.course.get",
    ]);
    $api->post('Course/save', [
        "uses"  => "CourseAPIController@save",
        "as"    => "api.course.save",
    ]);
    $api->post('Course/delete', [
        "uses"  => "CourseAPIController@delete",
        "as"    => "api.course.delete",
    ]);


    $api->get('City/get/{params?}', [
        "uses"  => "CityAPIController@get",
        "as"    => "api.city.get",
    ]);
    $api->post('City/save', [
        "uses"  => "CityAPIController@save",
        "as"    => "api.city.save",
    ]);
    $api->post('City/delete', [
        "uses"  => "CityAPIController@delete",
        "as"    => "api.city.delete",
    ]);



    $api->get('Country/get/{params?}', [
        "uses"  => "CountryAPIController@get",
        "as"    => "api.country.get",
    ]);
    $api->post('Country/save', [
        "uses"  => "CountryAPIController@save",
        "as"    => "api.country.save",
    ]);
    $api->post('Country/delete', [
        "uses"  => "CountryAPIController@delete",
        "as"    => "api.country.delete",
    ]);


    $api->get('DateSystem/get/{params?}', [
        "uses"  => "DateSystemAPIController@get",
        "as"    => "api.date_system.get",
    ]);
    $api->post('DateSystem/save', [
        "uses"  => "DateSystemAPIController@save",
        "as"    => "api.date_system.save",
    ]);
    $api->post('DateSystem/delete', [
        "uses"  => "DateSystemAPIController@delete",
        "as"    => "api.date_system.delete",
    ]);


    $api->get('IdnType/get/{params?}', [
        "uses"  => "IdnTypeAPIController@get",
        "as"    => "api.idn_type.get",
    ]);
    $api->post('IdnType/save', [
        "uses"  => "IdnTypeAPIController@save",
        "as"    => "api.idn_type.save",
    ]);
    $api->post('IdnType/delete', [
        "uses"  => "IdnTypeAPIController@delete",
        "as"    => "api.idn_type.delete",
    ]);


    $api->get('Wday/get/{params?}', [
        "uses"  => "WdayAPIController@get",
        "as"    => "api.wday.get",
    ]);
    $api->post('Wday/save', [
        "uses"  => "WdayAPIController@save",
        "as"    => "api.wday.save",
    ]);
    $api->post('Wday/delete', [
        "uses"  => "WdayAPIController@delete",
        "as"    => "api.wday.delete",
    ]);




    $api->get('Room/get/{params?}', [
        "uses"  => "RoomAPIController@get",
        "as"    => "api.room.get",
    ]);
    $api->post('Room/save', [
        "uses"  => "RoomAPIController@save",
        "as"    => "api.room.save",
    ]);
    $api->post('Room/delete', [
        "uses"  => "RoomAPIController@delete",
        "as"    => "api.room.delete",
    ]);



    $api->get('SessionStatus/get/{params?}', [
        "uses"  => "SessionStatusAPIController@get",
        "as"    => "api.session_status.get",
    ]);
    $api->post('SessionStatus/save', [
        "uses"  => "SessionStatusAPIController@save",
        "as"    => "api.session_status.save",
    ]);
    $api->post('SessionStatus/delete', [
        "uses"  => "SessionStatusAPIController@delete",
        "as"    => "api.session_status.delete",
    ]);


    $api->get('CandidateStatus/get/{params?}', [
        "uses"  => "CandidateStatusAPIController@get",
        "as"    => "api.candidate_status.get",
    ]);
    $api->post('CandidateStatus/save', [
        "uses"  => "CandidateStatusAPIController@save",
        "as"    => "api.candidate_status.save",
    ]);
    $api->post('CandidateStatus/delete', [
        "uses"  => "CandidateStatusAPIController@delete",
        "as"    => "api.candidate_status.delete",
    ]);



    $api->get('ReaUser/get/{params?}', [
        "uses"  => "ReaUserAPIController@get",
        "as"    => "api.rea_user.get",
    ]);
    $api->post('ReaUser/save', [
        "uses"  => "ReaUserAPIController@save",
        "as"    => "api.rea_user.save",
    ]);
    $api->post('ReaUser/save_myinfos', [
        "uses"  => "ReaUserAPIController@save_myinfos",
        "as"    => "api.rea_user.save_myinfos",
    ]);
    $api->post('ReaUser/send_active_email', [
        "uses"  => "ReaUserAPIController@send_active_email",
        "as"    => "api.rea_user.send_active_email",
    ]);
    $api->post('ReaUser/send_active_mobile', [
        "uses"  => "ReaUserAPIController@send_active_mobile",
        "as"    => "api.rea_user.send_active_mobile",
    ]);
    $api->post('ReaUser/submit_activation_mobile_code', [
        "uses"  => "ReaUserAPIController@submit_activation_mobile_code",
        "as"    => "api.rea_user.submit_activation_mobile_code",
    ]);
    $api->post('ReaUser/remove_connexion', [
        "uses"  => "ReaUserAPIController@remove_connexion",
        "as"    => "api.rea_user.remove_connexion",
    ]);
    $api->post('ReaUser/delete', [
        "uses"  => "ReaUserAPIController@delete",
        "as"    => "api.rea_user.delete",
    ]);
    $api->get('ReaUser/notifications', [
        "uses"  => "ReaUserAPIController@notifications",
        "as"    => "api.rea_user.notifications",
    ]);
    $api->post('ReaUser/change_password', [
        "uses"  => "ReaUserAPIController@change_password",
        "as"    => "api.rea_user.change_password",
    ]);
    $api->post('ReaUser/set_current_school', [
        "uses"  => "ReaUserAPIController@set_current_school",
        "as"    => "api.rea_user.set_current_school",
    ]);
    $api->post('ReaUser/set_current_sdepartment', [
        "uses"  => "ReaUserAPIController@set_current_sdepartment",
        "as"    => "api.rea_user.set_current_sdepartment",
    ]);
    $api->get('ReaUser/get_my_students', [
        "uses"  => "ReaUserAPIController@get_my_students",
        "as"    => "api.rea_user.get_my_students",
    ]);




    $api->get('SchoolTerm/get/{params?}', [
        "uses"  => "SchoolTermAPIController@get",
        "as"    => "api.school_term.get",
    ]);
    $api->post('SchoolTerm/save', [
        "uses"  => "SchoolTermAPIController@save",
        "as"    => "api.school_term.save",
    ]);
    $api->post('SchoolTerm/delete', [
        "uses"  => "SchoolTermAPIController@delete",
        "as"    => "api.school_term.delete",
    ]);



    $api->get('Rating/get/{params?}', [
        "uses"  => "RatingAPIController@get",
        "as"    => "api.rating.get",
    ]);
    $api->post('Rating/save', [
        "uses"  => "RatingAPIController@save",
        "as"    => "api.rating.save",
    ]);
    $api->post('Rating/delete', [
        "uses"  => "RatingAPIController@delete",
        "as"    => "api.rating.delete",
    ]);




    $api->get('CoursesTemplate/get/{params?}', [
        "uses"  => "CoursesTemplateAPIController@get",
        "as"    => "api.courses_template.get",
    ]);
    $api->post('CoursesTemplate/save', [
        "uses"  => "CoursesTemplateAPIController@save",
        "as"    => "api.courses_template.save",
    ]);
    $api->post('CoursesTemplate/delete', [
        "uses"  => "CoursesTemplateAPIController@delete",
        "as"    => "api.courses_template.delete",
    ]);




    $api->get('Wday/get/{params?}', [
        "uses"  => "WdayAPIController@get",
        "as"    => "api.wday.get",
    ]);
    $api->post('Wday/save', [
        "uses"  => "WdayAPIController@save",
        "as"    => "api.wday.save",
    ]);
    $api->post('Wday/delete', [
        "uses"  => "WdayAPIController@delete",
        "as"    => "api.wday.delete",
    ]);



    $api->get('ExamSession/get/{params?}', [
        "uses"  => "ExamSessionAPIController@get",
        "as"    => "api.exam_session.get",
    ]);
    $api->post('ExamSession/save', [
        "uses"  => "ExamSessionAPIController@save",
        "as"    => "api.exam_session.save",
    ]);
    $api->post('ExamSession/delete', [
        "uses"  => "ExamSessionAPIController@delete",
        "as"    => "api.exam_session.delete",
    ]);


    $api->get('EFile/get/{params?}', [
        "uses"  => "EFileAPIController@get",
        "as"    => "api.efile.get",
    ]);
    $api->post('EFile/save', [
        "uses"  => "EFileAPIController@save",
        "as"    => "api.efile.save",
    ]);
    $api->post('EFile/delete', [
        "uses"  => "EFileAPIController@delete",
        "as"    => "api.efile.delete",
    ]);


    $api->get('DayTemplate/get/{params?}', [
        "uses"  => "DayTemplateAPIController@get",
        "as"    => "api.day_template.get",
    ]);
    $api->post('DayTemplate/save', [
        "uses"  => "DayTemplateAPIController@save",
        "as"    => "api.day_template.save",
    ]);
    $api->post('DayTemplate/delete', [
        "uses"  => "DayTemplateAPIController@delete",
        "as"    => "api.day_template.delete",
    ]);


    $api->get('DayTemplateItem/get/{params?}', [
        "uses"  => "DayTemplateItemAPIController@get",
        "as"    => "api.day_template_item.get",
    ]);
    $api->post('DayTemplateItem/save', [
        "uses"  => "DayTemplateItemAPIController@save",
        "as"    => "api.day_template_item.save",
    ]);
    $api->post('DayTemplateItem/delete', [
        "uses"  => "DayTemplateItemAPIController@delete",
        "as"    => "api.day_template_item.delete",
    ]);


    $api->get('ModelTerm/get/{params?}', [
        "uses"  => "ModelTermAPIController@get",
        "as"    => "api.model_term.get",
    ]);
    $api->post('ModelTerm/save', [
        "uses"  => "ModelTermAPIController@save",
        "as"    => "api.model_term.save",
    ]);
    $api->post('ModelTerm/delete', [
        "uses"  => "ModelTermAPIController@delete",
        "as"    => "api.model_term.delete",
    ]);


    $api->get('StudentExam/get/{params?}', [
        "uses"  => "StudentExamAPIController@get",
        "as"    => "api.student_exam.get",
    ]);
    $api->post('StudentExam/save', [
        "uses"  => "StudentExamAPIController@save",
        "as"    => "api.student_exam.save",
    ]);
    $api->post('StudentExam/delete', [
        "uses"  => "StudentExamAPIController@delete",
        "as"    => "api.student_exam.delete",
    ]);


    $api->get('StudentFileStatus/get/{params?}', [
        "uses"  => "StudentFileStatusAPIController@get",
        "as"    => "api.student_file_status.get",
    ]);
    $api->post('StudentFileStatus/save', [
        "uses"  => "StudentFileStatusAPIController@save",
        "as"    => "api.student_file_status.save",
    ]);
    $api->post('StudentFileStatus/delete', [
        "uses"  => "StudentFileStatusAPIController@delete",
        "as"    => "api.student_file_status.delete",
    ]);



    $api->get('StudentFile/get/{params?}', [
        "uses"  => "StudentFileAPIController@get",
        "as"    => "api.student_file.get",
    ]);
    $api->post('StudentFile/save', [
        "uses"  => "StudentFileAPIController@save",
        "as"    => "api.student_file.save",
    ]);
    $api->post('StudentFile/save_student', [
        "uses"  => "StudentFileAPIController@save_student",
        "as"    => "api.student_file.save_student",
    ]);
    $api->post('StudentFile/delete', [
        "uses"  => "StudentFileAPIController@delete",
        "as"    => "api.student_file.delete",
    ]);
    $api->post('StudentFile/reorder_student_places', [
        "uses"  => "StudentFileAPIController@reorder_student_places",
        "as"    => "api.student_file.reorder_student_places",
    ]);
    $api->post('StudentFile/replace_student_place', [
        "uses"  => "StudentFileAPIController@replace_student_place",
        "as"    => "api.student_file.replace_student_place",
    ]);


    $api->get('SchoolRegistration/get/{params?}', [
        "uses"  => "SchoolRegistrationAPIController@get",
        "as"    => "api.school_registration.get",
    ]);
    $api->post('SchoolRegistration/save', [
        "uses"  => "SchoolRegistrationAPIController@save",
        "as"    => "api.school_registration.save",
    ]);
    $api->post('SchoolRegistration/delete', [
        "uses"  => "SchoolRegistrationAPIController@delete",
        "as"    => "api.school_registration.delete",
    ]);
    $api->post('SchoolRegistration/convert_to_school', [
        "uses"  => "SchoolRegistrationAPIController@convert_to_school",
        "as"    => "api.school_registration.convert_to_school",
    ]);





    $api->get('FamilyRelation/get/{params?}', [
        "uses"  => "FamilyRelationAPIController@get",
        "as"    => "api.family_relation.get",
    ]);
    $api->post('FamilyRelation/save', [
        "uses"  => "FamilyRelationAPIController@save",
        "as"    => "api.family_relation.save",
    ]);
    $api->post('FamilyRelation/delete', [
        "uses"  => "FamilyRelationAPIController@delete",
        "as"    => "api.family_relation.delete",
    ]);
    $api->get('FamilyRelation/get_my_relations', [
        "uses"  => "FamilyRelationAPIController@get_my_relations",
        "as"    => "api.family_relation.get_my_relations",
    ]);
    $api->post('FamilyRelation/delete_my_relation', [
        "uses"  => "FamilyRelationAPIController@delete_my_relation",
        "as"    => "api.family_relation.delete_my_relation",
    ]);
    $api->post('FamilyRelation/add_relation_from_idn', [
        "uses"  => "FamilyRelationAPIController@add_relation_from_idn",
        "as"    => "api.family_relation.add_relation_from_idn",
    ]);
    $api->get('FamilyRelation/get_my_relation/{family_relation_id}', [
        "uses"  => "FamilyRelationAPIController@get_my_relation",
        "as"    => "api.family_relation.get_my_relation",
    ]);
    $api->post('FamilyRelation/save_my_family_relation/', [
        "uses"  => "FamilyRelationAPIController@save_my_family_relation",
        "as"    => "api.family_relation.save_my_family_relation",
    ]);
    $api->post('FamilyRelation/save_new_student/', [
        "uses"  => "FamilyRelationAPIController@save_new_student",
        "as"    => "api.family_relation.save_new_student",
    ]);
    $api->post('FamilyRelation/save_parent/', [
        "uses"  => "FamilyRelationAPIController@save_parent",
        "as"    => "api.family_relation.save_parent",
    ]);

    $api->post('FamilyRelation/save_student_for_parent/', [
        "uses"  => "FamilyRelationAPIController@save_student_for_parent",
        "as"    => "api.family_relation.save_student_for_parent",
    ]);




    $api->get('Student/get/{params?}', [
        "uses"  => "StudentAPIController@get",
        "as"    => "api.student.get",
    ]);
    $api->post('Student/save', [
        "uses"  => "StudentAPIController@save",
        "as"    => "api.student.save",
    ]);
    $api->post('Student/delete', [
        "uses"  => "StudentAPIController@delete",
        "as"    => "api.student.delete",
    ]);



    $api->get('SchoolEmployee/get/{params?}', [
        "uses"  => "SchoolEmployeeAPIController@get",
        "as"    => "api.school_employee.get",
    ]);
    $api->get('SchoolEmployee/fullinfos/{school_employee_id?}', [
        "uses"  => "SchoolEmployeeAPIController@fullinfos",
        "as"    => "api.school_employee.fullinfos",
    ]);
    $api->post('SchoolEmployee/save', [
        "uses"  => "SchoolEmployeeAPIController@save",
        "as"    => "api.school_employee.save",
    ]);
    $api->post('SchoolEmployee/delete', [
        "uses"  => "SchoolEmployeeAPIController@delete",
        "as"    => "api.school_employee.delete",
    ]);
    $api->post('SchoolEmployee/replace', [
        "uses"  => "SchoolEmployeeAPIController@replace",
        "as"    => "api.school_employee.replace",
    ]);


    $api->get('ClassCourseExam/get/{params?}', [
        "uses"  => "ClassCourseExamAPIController@get",
        "as"    => "api.class_course_exam.get",
    ]);
    $api->post('ClassCourseExam/save', [
        "uses"  => "ClassCourseExamAPIController@save",
        "as"    => "api.class_course_exam.save",
    ]);
    $api->post('ClassCourseExam/delete', [
        "uses"  => "ClassCourseExamAPIController@delete",
        "as"    => "api.class_course_exam.delete",
    ]);




    $api->get('Translate/get/{params?}', [
        "uses"  => "TranslateAPIController@get",
        "as"    => "api.translate.get",
    ]);
    $api->get('Translate/multiget/{params?}', [
        "uses"  => "TranslateAPIController@multiget",
        "as"    => "api.translate.multiget",
    ]);
    $api->post('Translate/multisave', [
        "uses"  => "TranslateAPIController@multisave",
        "as"    => "api.translate.multisave",
    ]);
    $api->post('Translate/save', [
        "uses"  => "TranslateAPIController@save",
        "as"    => "api.translate.save",
    ]);
    $api->post('Translate/delete', [
        "uses"  => "TranslateAPIController@delete",
        "as"    => "api.translate.delete",
    ]);
    



    $api->get('TranslateModule/get/{params?}', [
        "uses"  => "TranslateModuleAPIController@get",
        "as"    => "api.translate_module.get",
    ]);
    $api->post('TranslateModule/save', [
        "uses"  => "TranslateModuleAPIController@save",
        "as"    => "api.translate_module.save",
    ]);
    $api->post('TranslateModule/delete', [
        "uses"  => "TranslateModuleAPIController@delete",
        "as"    => "api.translate_module.delete",
    ]);




    $api->get('System/cache/clear_cache/{module?}', [
        "uses"  => "SystemAPIController@clear_cache",
        "as"    => "api.system.cache.clear_cache",
    ]);




    $api->get('FeedImport/get/{params?}', [
        "uses"  => "FeedImportAPIController@get",
        "as"    => "api.feed_import.get",
    ]);
    $api->post('FeedImport/save', [
        "uses"  => "FeedImportAPIController@save",
        "as"    => "api.feed_import.save",
    ]);
    $api->post('FeedImport/delete', [
        "uses"  => "FeedImportAPIController@delete",
        "as"    => "api.feed_import.delete",
    ]);
    $api->post('FeedImport/upload', [
        "uses"  => "FeedImportAPIController@upload",
        "as"    => "api.feed_import.upload",
    ]);
    $api->post('FeedImport/process', [
        "uses"  => "FeedImportAPIController@process",
        "as"    => "api.feed_import.process",
    ]);






    $api->resource("SchoolScope", "SchoolScopeAPIController");
    //Function key: #/ Prof Course session list of one day
    $api->get(
        "courseSession/timeline/{date?}",
        [
            "as"=>"courseSession.timeline",
            "uses"=>"CourseSessionAPIController@timeline"
        ]);
    $api->get(
        "courseSession/previous",
        [
            "as"=>"courseSession.previous",
            "uses"=>"CourseSessionAPIController@previous"
        ]);

    // Function key : BF 254 / Student session list
    $api->get(
        "studentCourseSession/list/{course_session_id}",
        [
            "as"=>"studentCourseSession.listByCourseSessionId",
            "uses"=>"StudentCourseSessionAPIController@listByCourseSessionId"
        ]);


    //Function key: BF BF245 / CourseSession infos used in student list if course session empty
    $api->get('courseSession/infos/{year}/{hmonth}/{hday_num}/{level_class_id}/{symbol}/{session_order}', [
        "as"    => "courseSession.infos",
        "uses"  => "CourseSessionAPIController@infos",
        ]);


    //Function key: BF BF245 / Get Current Course Session
    $api->get('courseSession/current/{date?}', [
        "as"    => "courseSession.current",
        "uses"  => "CourseSessionAPIController@current",
        ]);


    //Function Key: BF255 / Cancel Session
    $api->post('CourseSession/cancel', [
        "as"    => "courseSession.cancel",
        "uses"  => "CourseSessionAPIController@cancel",
    ]);


    //Function Key: BF255 / Cancel admin_cancel
    $api->post('CourseSession/admin_cancel', [
        "as"    => "courseSession.admin_cancel",
        "uses"  => "CourseSessionAPIController@admin_cancel",
    ]);
    $api->post('CourseSession/change_teacher', [
        "as"    => "courseSession.change_teacher",
        "uses"  => "CourseSessionAPIController@change_teacher",
    ]);

    //Function Key: Comment Student Session
    $api->post('CourseSession/comment_student_session', [
        "as"    => "courseSession.comment_student_session",
        "uses"  => "CourseSessionAPIController@comment_student_session",
    ]);

    //Function key: BF 256 / Update Only changed fields in StudentCourseSession
    $api->post(
        "StudentCourseSession/performUpdate/{course_session_id}",
        [
            "as"=>"studentCourseSession.performUpdate",
            "uses"=>"StudentCourseSessionAPIController@performUpdate"
        ]);

    //Function key: BF BF245 / Start Course Session
    $api->post('CourseSession/start', [
        "as"    => "courseSession.start",
        "uses"  => "CourseSessionAPIController@start",
        ]);

    //Function key: BF BF256 Update Students Course Session
    $api->post('CourseSession/update_students', [
        "as"    => "courseSession.update_students",
        "uses"  => "CourseSessionAPIController@update_students",
        ]);

    //Function key: BF BF246 Close Course Session
    $api->post('CourseSession/close', [
        "as"    => "api.courseSession.close",
        "uses"  => "CourseSessionAPIController@close"
        ]);

    //Function key: save Course Session

    $api->post('CourseSession/StudentsSession/save', [
        "as"    => "api.courseSession.studentsSession.save",
        "uses"  => "CourseSessionAPIController@saveStudentsSession",
        ]);
    $api->post('Alert/setAlertReaded',[
        "as" => "api.alert.setAlertReaded",
        "uses" => "AlertAPIController@setAlertReaded",
    ]);
    //Function key: List students of one student session
    $api->post('CourseSession/listStudentsByCourseSession', [
            "uses"  => "CourseSessionAPIController@listStudentsByCourseSession",
            "as"    => "api.course_session.list_students_by_course_session",
        ]);


    // BF264 Select API Functions

    //Function key: Q0007 Get
    $api->get('Course/getAllCourses', [
        "uses"  => "CourseAPIController@getAllCourses",
        "as"    => "api.course.get_all_courses",
    ]);

    //Function key: Q0007 Get
    $api->get('SchoolYear/getAllSchoolYearsBySchoolID/{school_id}', [
        "uses"  => "SchoolYearAPIController@getAllSchoolYearsBySchoolID",
        "as"    => "api.schoolYear.get_by_school_id",
    ]);

    


    //Function key: Get Weekend Days
    $api->get('School/GetWeekDays/{school_id}', [
        "uses"  => "SchoolAPIController@GetWeekDays",
        "as"    => "api.school.get_week_days",
    ]);

    //Function key: Q0008 Get
    $api->get('SchoolLevel/getAllSchoolLevelsBySchoolYearID/{school_year_id}', [
        "uses"  => "SchoolLevelAPIController@getAllSchoolLevelsBySchoolYearID",
        "as"    => "api.schoolLevel.get_by_school_year_id",
    ]);


    //Function key: Q0009 Get School Class
    $api->get('SchoolClass/getAllSchoolClassBySchoolYearIDAndLevelClassID/{school_year_id}/{level_class_id}', [
        "uses"  => "SchoolClassAPIController@getAllSchoolClassBySchoolYearIDAndLevelClassID",
        "as"    => "api.schoolClass.get_by_school_year_id",
    ]);
    //Function key: Q0010 Get Course Sched Item
    $api->post('CourseSchedItem/GetItems', [
        "uses"  => "CourseSchedItemAPIController@GetItems",
        "as"    => "api.courseSchedItem.get_items",
    ]);
    //Function key: Q0011 Get Default Template
    $api->get('WeekTemplate/GetDefaultTemplate', [
        "uses"  => "CourseSchedItemAPIController@GetDefaultWeekTemplate",
        "as"    => "api.course_sched.get_week_template_template",
    ]);
    //Function key: Q0012 Creat New CourseSched From Template
    $api->post('CourseSchedItem/CreatFromWeekTemplate', [
        "uses"  => "CourseSchedItemAPIController@CreatFromWeekTemplate",
        "as"    => "api.course_sched_item.creat_from_week_tempate",
    ]);
    //Function key: Save course_sched_items
    $api->post('CourseSchedItem/SaveItems', [
        "uses"  => "CourseSchedItemAPIController@SaveItems",
        "as"    => "api.course_sched_item.save",
    ]);

    // BF265
    // Q0014 - Q0015
    // Get level Class list
    $api->get('LevelClass/getAllBySchoolIdAndYearIdAndDomaineMfk/school_id/{school_id}/school_year_id/{school_year_id}', [
        "uses"  => "LevelClassAPIController@getAllBySchoolIdAndYearIdAndDomaineMfk",
        "as"    => "api.level_class.getAllBySchoolIdAndYearIdAndDomaineMfk",
    ]);
    // BF 265
    // Q0016
    // Get level Class list
    $api->get('Sdepartment/ListBySchoolId/{school_id}', [
        "uses"  => "SdepartmentAPIController@listBySchoolId",
        "as"    => "api.sdepartment.listBySchoolId",
    ]);

    //BF 421-422
    //Q0017
    $api->get('CoursesConfigTemplates/listBySchoolId/{school_id?}', [
        "uses"  => "CoursesConfigTemplateAPIController@listBySchoolId",
        "as"    => "api.CoursesConfigTemplates.listBySchoolId",
    ]);
    //Q0018
    $api->get('SchoolLevel/listByCoursesConfigTemplateId/{courses_config_template_id}', [
        "uses"  => "CoursesConfigTemplateAPIController@listByCoursesConfigTemplateId",
        "as"    => "api.CoursesConfigTemplates.listByCoursesConfigTemplateId",
    ]);
    //: Function key: BF264 / Q0008
    $api->get('LevelClass/getAllLevelClassBySchoolLevelID/{school_level_id}', [
        "uses"  => "LevelClassAPIController@getAllLevelClassBySchoolLevelID",
        "as"    => "api.levelClass.get_by_school_level_id",
    ]);
    //Q0019 - Q0020
    $api->get('CoursesConfigItems/listByCoursesConfigTemplateAndLevelClassId/cct_id/{courses_config_template_id}/lc_id/{level_class_id}', [
        "uses"  => "CoursesConfigItemAPIController@listByCoursesConfigTemplateAndLevelClassId",
        "as"    => "api.CoursesConfigItem.listByCoursesConfigTemplateAndLevelClassId",
    ]);
    
    $api->post('CoursesConfigItem/multisave', [
        "uses"  => "CoursesConfigItemAPIController@multisave",
        "as"    => "api.CoursesConfigItem.multisave",
    ]);
    // Q Save scope school level class
    $api->post('LevelClass/SaveScopeSchoolLevelClass', [
        "uses"  => "LevelClassAPIController@SaveScopeSchoolLevelClass",
        "as"    => "api.LevelClass.SaveScopeSchoolLevelClass",
    ]);

    $api->get('LevelsTemplate/GetAll', [
        "uses"  => "LevelsTemplateAPIController@GetAll",
        "as"    => "api.LevelsTemplateAPIController.GetAll",
    ]);






    // Insert school_class_course and retour liste BF-1053
    $api->post('SchoolClassCourse/generate', [
        "uses"  => "SchoolClassCourseAPIController@generate",
        "as"    => "api.school_class_course.generate",
    ]);
    // save school_class_course and retour liste BF-1053
    $api->post('SchoolClassCourse/multisave', [
        "uses"  => "SchoolClassCourseAPIController@multisave",
        "as"    => "api.school_class_course.multisave",
    ]);
    // Get list of class by teacher
    $api->get('SchoolClassCourse/getClassListByTeacher/{teacher_id?}', [
        "uses"  => "SchoolClassCourseAPIController@getLevelClassListByTeacherId",
        "as"    => "api.school_class_course.getLevelClassListByTeacherId",
    ]);









    $api->post('File/upload', [
        "uses"  => "FileAPIController@upload",
        "as"    => "api.file.upload",
    ]);












    /*
    CRM
    */
    /* CrmErequest */
    $api->get('CrmErequest/get/{params?}', [
        "uses"  => "CrmErequestAPIController@get",
        "as"    => "api.crm_erequest.get",
    ]);
    $api->post('CrmErequest/save', [
        "uses"  => "CrmErequestAPIController@save",
        "as"    => "api.crm_erequest.save",
    ]);
    $api->post('CrmErequest/delete', [
        "uses"  => "CrmErequestAPIController@delete",
        "as"    => "api.crm_erequest.delete",
    ]);
    $api->post('CrmErequest/cancel', [
        "uses"  => "CrmErequestAPIController@cancel",
        "as"    => "api.crm_erequest.cancel",
    ]);
    $api->post('CrmErequest/submit', [
        "uses"  => "CrmErequestAPIController@submit",
        "as"    => "api.crm_erequest.submit",
    ]);
    $api->post('CrmErequest/submit_settings', [
        "uses"  => "CrmErequestAPIController@submit_settings",
        "as"    => "api.crm_erequest.submit_settings",
    ]);
    $api->post('CrmErequest/submit_follow_up', [
        "uses"  => "CrmErequestAPIController@submit_follow_up",
        "as"    => "api.crm_erequest.submit_follow_up",
    ]);
    $api->post('CrmErequest/submit_reply_follow_up', [
        "uses"  => "CrmErequestAPIController@submit_reply_follow_up",
        "as"    => "api.crm_erequest.submit_reply_follow_up",
    ]);
    $api->post('CrmErequest/approve_leave', [
        "uses"  => "CrmErequestAPIController@approve_leave",
        "as"    => "api.crm_erequest.approve_leave",
    ]);
    $api->post('CrmErequest/disapprove_leave', [
        "uses"  => "CrmErequestAPIController@disapprove_leave",
        "as"    => "api.crm_erequest.disapprove_leave",
    ]);
    $api->post('CrmErequest/redirect_erequest', [
        "uses"  => "CrmErequestAPIController@redirect_erequest",
        "as"    => "api.crm_erequest.redirect_erequest",
    ]);


    $api->get('CrmErequestLevel/get/{params?}', [
        "uses"  => "CrmErequestAPIController@get_levels",
        "as"    => "api.crm_erequest.get_levels",
    ]);

    /* CrmErequestType */
    $api->get('CrmErequestType/get/{params?}', [
        "uses"  => "CrmErequestTypeAPIController@get",
        "as"    => "api.crm_erequest_type.get",
    ]);
    $api->post('CrmErequestType/save', [
        "uses"  => "CrmErequestTypeAPIController@save",
        "as"    => "api.crm_erequest_type.save",
    ]);
    $api->post('CrmErequestType/delete', [
        "uses"  => "CrmErequestTypeAPIController@delete",
        "as"    => "api.crm_erequest_type.delete",
    ]);

    /* CrmErequestCat */
    $api->get('CrmErequestCat/get/{params?}', [
        "uses"  => "CrmErequestCatAPIController@get",
        "as"    => "api.crm_erequest_cat.get",
    ]);
    $api->post('CrmErequestCat/save', [
        "uses"  => "CrmErequestCatAPIController@save",
        "as"    => "api.crm_erequest_cat.save",
    ]);
    $api->post('CrmErequestCat/delete', [
        "uses"  => "CrmErequestCatAPIController@delete",
        "as"    => "api.crm_erequest_cat.delete",
    ]);
    $api->get('CrmErequestStatus/get/{params?}', [
        "uses"  => "CrmErequestStatusAPIController@get",
        "as"    => "api.crm_erequest_status.get",
    ]);




    /* CrmErequestFup */
    $api->get('CrmErequestFup/get/{params?}', [
        "uses"  => "CrmErequestFupAPIController@get",
        "as"    => "api.crm_erequest_fup.get",
    ]);
    $api->post('CrmErequestFup/is_read', [
        "uses"  => "CrmErequestFupAPIController@is_read",
        "as"    => "api.crm_erequest_fup.is_read",
    ]);





    /* HrmLeaveType */
    $api->get('HrmLeaveType/get/{params?}', [
        "uses"  => "HrmLeaveTypeAPIController@get",
        "as"    => "api.hrm_leave_type.get",
    ]);












    /* CpcBook */
    $api->get('CpcBook/get/{params?}', [
        "uses"  => "CpcBookAPIController@get",
        "as"    => "api.cpc_book.get",
    ]);
    $api->post('CpcBook/save', [
        "uses"  => "CpcBookAPIController@save",
        "as"    => "api.cpc_book.save",
    ]);
    $api->post('CpcBook/delete', [
        "uses"  => "CpcBookAPIController@delete",
        "as"    => "api.cpc_book.delete",
    ]);


    /* CpcBookCategory */
    $api->get('CpcBookCategory/get/{params?}', [
        "uses"  => "CpcBookCategoryAPIController@get",
        "as"    => "api.cpc_book_category.get",
    ]);
    $api->post('CpcBookCategory/save', [
        "uses"  => "CpcBookCategoryAPIController@save",
        "as"    => "api.cpc_book_category.save",
    ]);
    $api->post('CpcBookCategory/delete', [
        "uses"  => "CpcBookCategoryAPIController@delete",
        "as"    => "api.cpc_book_category.delete",
    ]);



    /* CpcBookPage */
    $api->get('CpcBookPage/get/{params?}', [
        "uses"  => "CpcBookPageAPIController@get",
        "as"    => "api.cpc_book_page.get",
    ]);
    $api->post('CpcBookPage/save', [
        "uses"  => "CpcBookPageAPIController@save",
        "as"    => "api.cpc_book_page.save",
    ]);
    $api->post('CpcBookPage/delete', [
        "uses"  => "CpcBookPageAPIController@delete",
        "as"    => "api.cpc_book_page.delete",
    ]);
    $api->post('CpcBookPage/upload_book', [
        "uses"  => "CpcBookPageAPIController@upload_book",
        "as"    => "api.cpc_book_page.upload_book",
    ]);



    /* CpcBookType */
    $api->get('CpcBookType/get/{params?}', [
        "uses"  => "CpcBookTypeAPIController@get",
        "as"    => "api.cpc_book_type.get",
    ]);
    $api->post('CpcBookType/save', [
        "uses"  => "CpcBookTypeAPIController@save",
        "as"    => "api.cpc_book_type.save",
    ]);
    $api->post('CpcBookType/delete', [
        "uses"  => "CpcBookTypeAPIController@delete",
        "as"    => "api.cpc_book_type.delete",
    ]);



    /* CpcCourseBook */
    $api->get('CpcCourseBook/get/{params?}', [
        "uses"  => "CpcCourseBookAPIController@get",
        "as"    => "api.cpc_course_book.get",
    ]);
    $api->post('CpcCourseBook/save', [
        "uses"  => "CpcCourseBookAPIController@save",
        "as"    => "api.cpc_course_book.save",
    ]);
    $api->post('CpcCourseBook/delete', [
        "uses"  => "CpcCourseBookAPIController@delete",
        "as"    => "api.cpc_course_book.delete",
    ]);



    /* CpcCourseBookPage */
    $api->get('CpcCourseBookPage/get/{params?}', [
        "uses"  => "CpcCourseBookPageAPIController@get",
        "as"    => "api.cpc_course_book.get",
    ]);
    $api->post('CpcCourseBookPage/save', [
        "uses"  => "CpcCourseBookPageAPIController@save",
        "as"    => "api.cpc_course_book.save",
    ]);
    $api->post('CpcCourseBookPage/delete', [
        "uses"  => "CpcCourseBookPageAPIController@delete",
        "as"    => "api.cpc_course_book.delete",
    ]);



    /* CpcCourseBookPage */
    $api->get('CpcCoursePlan/get/{params?}', [
        "uses"  => "CpcCoursePlanAPIController@get",
        "as"    => "api.cpc_course_plan.get",
    ]);
    $api->post('CpcCoursePlan/save', [
        "uses"  => "CpcCoursePlanAPIController@save",
        "as"    => "api.cpc_course_plan.save",
    ]);
    $api->post('CpcCoursePlan/delete', [
        "uses"  => "CpcCoursePlanAPIController@delete",
        "as"    => "api.cpc_course_plan.delete",
    ]);


    /* CpcCourseProgram */
    $api->get('CpcCourseProgram/get/{params?}', [
        "uses"  => "CpcCourseProgramAPIController@get",
        "as"    => "api.cpc_course_program.get",
    ]);
    $api->post('CpcCourseProgram/save', [
        "uses"  => "CpcCourseProgramAPIController@save",
        "as"    => "api.cpc_course_program.save",
    ]);
    $api->post('CpcCourseProgram/delete', [
        "uses"  => "CpcCourseProgramAPIController@delete",
        "as"    => "api.cpc_course_program.delete",
    ]);



    /* CpcCourseProgramBook */
    $api->get('CpcCourseProgramBook/get/{params?}', [
        "uses"  => "CpcCourseProgramBookAPIController@get",
        "as"    => "api.cpc_course_program_book.get",
    ]);
    $api->post('CpcCourseProgramBook/save', [
        "uses"  => "CpcCourseProgramBookAPIController@save",
        "as"    => "api.cpc_course_program_book.save",
    ]);
    $api->post('CpcCourseProgramBook/delete', [
        "uses"  => "CpcCourseProgramBookAPIController@delete",
        "as"    => "api.cpc_course_program_book.delete",
    ]);
    $api->post('CpcCourseProgramBook/save_book', [
        "uses"  => "CpcCourseProgramBookAPIController@save_book",
        "as"    => "api.cpc_course_plan_book.save_book",
    ]);





    /* Ewall */
    $api->get('Ewall/get/{params?}', [
        "uses"  => "EwallAPIController@get",
        "as"    => "api.ewall.get",
    ]);
    $api->post('Ewall/save', [
        "uses"  => "EwallAPIController@save",
        "as"    => "api.ewall.save",
    ]);
    $api->post('Ewall/delete', [
        "uses"  => "EwallAPIController@delete",
        "as"    => "api.ewall.delete",
    ]);
    $api->get('Ewall/get_my_ewalls', [
        "uses"  => "EwallAPIController@get_my_ewalls",
        "as"    => "api.ewall.get_my_ewalls",
    ]);
    $api->post('Ewall/get_my_ewalls', [
        "uses"  => "EwallAPIController@get_my_ewalls",
        "as"    => "api.ewall.get_my_ewalls",
    ]);
    $api->post('Ewall/submit_subject', [
        "uses"  => "EwallAPIController@submit_subject",
        "as"    => "api.ewall.submit_subject",
    ]);
    $api->post('Ewall/submit_follow_up', [
        "uses"  => "EwallAPIController@submit_follow_up",
        "as"    => "api.ewall.submit_follow_up",
    ]);
    $api->post('Ewall/is_read', [
        "uses"  => "EwallAPIController@is_read",
        "as"    => "api.ewall.is_read",
    ]);
    $api->get('Ewall/get_ewall_esubjects', [
        "uses"  => "EwallAPIController@get_ewall_esubjects",
        "as"    => "api.ewall.get_ewall_esubjects",
    ]);
    $api->post('Ewall/get_ewall_esubjects', [
        "uses"  => "EwallAPIController@get_ewall_esubjects",
        "as"    => "api.ewall.get_ewall_esubjects",
    ]);
    $api->get('Ewall/get_esubject_comments', [
        "uses"  => "EwallAPIController@get_esubject_comments",
        "as"    => "api.ewall.get_esubject_comments",
    ]);
    $api->post('Ewall/get_esubject_comments', [
        "uses"  => "EwallAPIController@get_esubject_comments",
        "as"    => "api.ewall.get_esubject_comments",
    ]);






   $api->get('Role/get/{params?}', [
        "uses"  => "RoleAPIController@get",
        "as"    => "api.role.get",
    ]);
    $api->post('Role/save', [
        "uses"  => "RoleAPIController@save",
        "as"    => "api.role.save",
    ]);
    $api->post('Role/delete', [
        "uses"  => "RoleAPIController@delete",
        "as"    => "api.role.delete",
    ]);

    $api->get('Test/get/{params?}', [
        "uses"  => "TestAPIController@get",
        "as"    => "api.test.get",
    ]);
    $api->post('Test/save', [
        "uses"  => "TestAPIController@save",
        "as"    => "api.test.save",
    ]);
    $api->post('Test/delete', [
        "uses"  => "TestAPIController@delete",
        "as"    => "api.test.delete",
    ]);


    $api->get('Club/get/{params?}', [
        "uses"  => "ClubAPIController@get",
        "as"    => "api.club.get",
    ]);
    $api->post('Club/save', [
        "uses"  => "ClubAPIController@save",
        "as"    => "api.club.save",
    ]);
    $api->post('Club/delete', [
        "uses"  => "ClubAPIController@delete",
        "as"    => "api.club.delete",
    ]);



    $api->get('CourseType/get/{params?}', [
        "uses"  => "CourseTypeAPIController@get",
        "as"    => "api.course_type.get",
    ]);
    $api->post('CourseType/save', [
        "uses"  => "CourseTypeAPIController@save",
        "as"    => "api.course_type.save",
    ]);
    $api->post('CourseType/delete', [
        "uses"  => "CourseTypeAPIController@delete",
        "as"    => "api.course_type.delete",
    ]);




    $api->get('CourseFor/get/{params?}', [
        "uses"  => "CourseForAPIController@get",
        "as"    => "api.course_for.get",
    ]);
    $api->post('CourseFor/save', [
        "uses"  => "CourseForAPIController@save",
        "as"    => "api.course_for.save",
    ]);
    $api->post('CourseFor/delete', [
        "uses"  => "CourseForAPIController@delete",
        "as"    => "api.course_for.delete",
    ]);





    $api->get('Coach/get/{params?}', [
        "uses"  => "CoachAPIController@get",
        "as"    => "api.coach.get",
    ]);

    $api->get('Coach/getAll', [
        "uses"  => "CoachAPIController@getAll",
        "as"    => "api.coach.getAll",
    ]);

    
    $api->get('Coach/search/{params?}', [
        "uses"  => "CoachAPIController@search",
        "as"    => "api.coach.search",
    ]);
    
    $api->post('Coach/save', [
        "uses"  => "CoachAPIController@save",
        "as"    => "api.coach.save",
    ]);
    $api->post('Coach/delete', [
        "uses"  => "CoachAPIController@delete",
        "as"    => "api.coach.delete",
    ]);




    $api->get('Permission/get/{params?}', [
        "uses"  => "PermissionAPIController@get",
        "as"    => "api.permission.get",
    ])
    //->middleware('permission:manage_permission')
    ;
    
    $api->post('Permission/save', [
        "uses"  => "PermissionAPIController@save",
        "as"    => "api.permission.save",
    ]);
    $api->post('Permission/delete', [
        "uses"  => "PermissionAPIController@delete",
        "as"    => "api.permission.delete",
    ]);




    
    $api->get('CourseSession/get/{params?}', [
        "uses"  => "CourseSessionAPIController@get",
        "as"    => "api.course_session.get",
    ]);

    $api->get('CourseSession/getbycoach/{params?}', [
        "uses"  => "CourseSessionAPIController@getbycoach",
        "as"    => "api.course_session.getbycoach",
    ]);


    $api->get('CourseSession/getCoursesAllRoomsHome/{params?}', [
        "uses"  => "CourseSessionAPIController@getCoursesAllRoomsHome",
        "as"    => "api.course_session.getCoursesAllRoomsHome",
    ]);

    $api->post('CourseSession/add_course_session', [
        "uses"  => "CourseSessionAPIController@add_course_session",
        "as"    => "api.course_session.add_course_session",
    ]);

    
    $api->post('CourseSession/update', [
        "uses"  => "CourseSessionAPIController@update",
        "as"    => "api.course_session.update",
    ]);

    
    $api->post('CourseSession/delete', [
        "uses"  => "CourseSessionAPIController@delete",
        "as"    => "api.course_session.delete",
    ]);

    
    $api->post('CourseSession/validate_planning', [
        "uses"  => "CourseSessionAPIController@validate_planning",
        "as"    => "api.course_session.validate_planning",
    ]);


    $api->post('CourseSession/analyse', [
        "uses"  => "CourseSessionAPIController@analyse",
        "as"    => "api.course_session.analyse",
    ]);


    $api->post('CourseSession/duplicate_planning', [
        "uses"  => "CourseSessionAPIController@duplicate_planning",
        "as"    => "api.course_session.duplicate_planning",
    ]);



    $api->get('CourseSession/stats/{params?}', [
        "uses"  => "CourseSessionAPIController@stats",
        "as"    => "api.course_session.stats",
    ]);


    $api->get('CourseSession/exportgrh/{params?}', [
        "uses"  => "CourseSessionAPIController@exportgrh",
        "as"    => "api.course_session.exportgrh",
    ]);



    $api->get('CourseSession/send_week_email/{course_session_year}/{week_num}/{club_id?}/{room_id?}/{coach_id?}/{step?}', [
        "uses"  => "CourseSessionAPIController@send_week_email",
        "as"    => "api.course_session.send_week_email",
    ]);
    

    $api->get('CourseSession/request_cancel_cours/{params?}', [
        "uses"  => "CourseSessionAPIController@request_cancel_cours",
        "as"    => "api.course_session.request_cancel_cours",
    ]);

    $api->get('CourseSession/validate_objectif_remplissage/{params?}', [
        "uses"  => "CourseSessionAPIController@validate_objectif_remplissage",
        "as"    => "api.course_session.validate_objectif_remplissage",
    ]);

    $api->get('CourseSession/replace_coach/{params?}', [
        "uses"  => "CourseSessionAPIController@replace_coach",
        "as"    => "api.course_session.replace_coach",
    ]);
    
    $api->get('CourseSession/replace_course/{params?}', [
        "uses"  => "CourseSessionAPIController@replace_course",
        "as"    => "api.course_session.replace_course",
    ]);

    $api->get('CourseSession/nbr_paritcipants_cours/{params?}', [
        "uses"  => "CourseSessionAPIController@nbr_paritcipants_cours",
        "as"    => "api.course_session.nbr_paritcipants_cours",
    ]);

    

    $api->get('CourseSession/add_review/{params?}', [
        "uses"  => "CourseSessionAPIController@add_review",
        "as"    => "api.course_session.add_review",
    ]);


    $api->get('CourseSessionFin/get/{params?}', [
        "uses"  => "CourseSessionFinAPIController@get",
        "as"    => "api.course_session_fin.get",
    ]);
    

    $api->get('CourseSessionFin/valid_hebdomadaire/{params?}',[
        "uses"  => "CourseSessionFinAPIController@valid_hebdomadaire",
        "as"    => "api.course_session_fin.valid_hebdomadaire",
    ]);
    

    $api->get('CourseSessionFin/getParticipate/{params?}', [
        "uses"  => "CourseSessionFinAPIController@getParticipate",
        "as"    => "api.course_session_fin.getParticipate",
    ]);

    $api->post('CourseSessionFin/annulerrefuse', [
        "uses"  => "CourseSessionFinAPIController@annulerrefuse",
        "as"    => "api.course_session_fin.annulerrefuse",
    ]);

    $api->post('CourseSessionFin/participerefuse', [
        "uses"  => "CourseSessionFinAPIController@participerefuse",
        "as"    => "api.course_session_fin.participerefuse",
    ]);

    $api->post('CourseSessionFin/valid', [
        "uses"  => "CourseSessionFinAPIController@valid",
        "as"    => "api.course_session_fin.valid",
    ]);

    $api->post('CourseSessionFin/validnbr', [
        "uses"  => "CourseSessionFinAPIController@validnbr",
        "as"    => "api.course_session_fin.validnbr",
    ]);

    $api->post('CourseSessionFin/validreplace', [
        "uses"  => "CourseSessionFinAPIController@validreplace",
        "as"    => "api.course_session_fin.validreplace",
    ]);

    $api->post('CourseSessionFin/replacerefuse', [
        "uses"  => "CourseSessionFinAPIController@replacerefuse",
        "as"    => "api.course_session_fin.replacerefuse",
    ]);




});



$api->get('public/Planning/get_clubs_and_rooms', [
    "uses"  => "ClubAPIController@get_clubs_and_rooms",
    "as"    => "api.public.planning.get_clubs_and_rooms"
]);

$api->get('public/Planning/get_clubs_and_rooms_platinum', [
    "uses"  => "ClubAPIController@get_clubs_and_rooms_platinum",
    "as"    => "api.public.planning.get_clubs_and_rooms_platinum"
]);

$api->get('public/Planning/get_club_with_rooms/{club_id}', [
    "uses"  => "ClubAPIController@get_club_with_rooms",
    "as"    => "api.public.planning.get_club_with_rooms"
]);



$api->get('public/Planning/get_cours/{room_id}', [
    "uses"  => "CourseAPIController@get_cours",
    "as"    => "api.public.planning.get_cours"
]);

$api->get('public/Planning/get_coachs/{cours_id}/{club_id}', [
    "uses"  => "CoachAPIController@get_coachs",
    "as"    => "api.public.planning.get_coachs"
]);

$api->get('public/Planning/get_liste_coachs/{primary_coach_id}/{secondary_coach_id}/{triple_coach_id}/{quad_coach_id}/{five_coach_id}', [
    "uses"  => "CoachAPIController@get_liste_coachs",
    "as"    => "api.public.planning.get_liste_coachs"
]);

$api->get('public/Planning/get_sessions_cours/{club_id}/{room_id}/{cours_id}/{coach_id}/{week_num}/{week_year}', [
    "uses"  => "CourseSessionAPIController@get_sessions_cours",
    "as"    => "api.public.planning.get_sessions_cours"
]);

$api->get('public/Planning/get_sessions_cours_autres/{club_id}/{room_id}/{coach_id}/{week_num}/{week_year}', [
    "uses"  => "CourseSessionAPIController@get_sessions_cours_autres",
    "as"    => "api.public.planning.get_sessions_cours_autres"
]);

$api->get('public/Planning/getCalendarFilters/{club_id}/{room_id?}/{coach_id?}/{course_id?}/{start_date?}/{end_date?}', [
    "uses"  => "CourseSessionAPIController@getCalendarFilters",
    "as"    => "api.course_session.calendar.get_filters",
]);




