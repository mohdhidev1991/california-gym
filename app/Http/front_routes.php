<?php

/*
|--------------------------------------------------------------------------
| FRONT Routes
|--------------------------------------------------------------------------
|
| Here is where all FRONT routes are defined.
|
*/

$front->group(['prefix' => 'tpl'], function ($front) {
    
    Blade::setContentTags('<%', '%>');
    Blade::setEscapedContentTags('<%%', '%%>');

    $front->get('admin', function () {
        return view('front/admin');
    });

    Route::get('admin/blocks/header', function () {
        return view('front/blocks/header');
    });

    Route::get('admin/blocks/aside', function () {
        return view('front/blocks/aside');
    });

    Route::get('admin/blocks/nav', function () {
        return view('front/blocks/nav');
    });

    $front->get('dashbord', function () {
        //echo "Dashbord Admin";
    });


    $front->get('course_session/modal_confirm_cancel', function () {
        return view('front/course_session/modal_confirm_cancel');
    });

    $front->get('course_session/modal_student_presence', function () {
        return view('front/course_session/modal_student_presence');
    });
    $front->get('course_session/manage', function () {
        return view('front/course_session/manage');
    });
    $front->get('course_session/manage_item', function () {
        return view('front/course_session/manage_item');
    });



    
    Route::get('course_session/timeline', function () {
        return view('front/course_session/timeline');
    });
    Route::get('course_session/open', function () {
        return view('front/course_session/open');
    });

    Route::get('course_session/current', function () {
        return view('front/course_session/current');
    });
    Route::get('course_session/edit_current', function () {
        return view('front/course_session/edit_current');
    });

    Route::get('course_session_fin/request_cancel_cours', function () {
        return view('front/course_session_fin/request_cancel_cours');
    });

    Route::get('course_session_fin/replace_coach', function () {
        return view('front/course_session_fin/replace_coach');
    });

    Route::get('course_session_fin/nbr_paritcipants_cours', function () {
        return view('front/course_session_fin/nbr_paritcipants_cours');
    });
    


    Route::get('courses_config_item/select_template', function () {
        return view('front/courses_config_item/select_template');
    });
    Route::get('courses_config_item/config_item', function () {
        return view('front/courses_config_item/config_item');
    });



    Route::get('school_level/config_school_level', function () {
        return view('front/school_level/config_school_level');
    });
    Route::get('school_level/all', function () {
        return view('front/school_level/all');
    });
    Route::get('school_level/form', function () {
        return view('front/school_level/form');
    });




    /*
    TPL School
    */
    $front->get('school/generator', [
            'as' => 'school.generator',
            'uses' => 'SchoolController@getSearchGeneratorFrontTpl',
    ]);
    Route::get('school/all', function () {
        return view('front/school/all');
    });
    Route::get('school/course_sched_generator', function () {
        return view('front/school/course_sched_generator');
    });
    Route::get('school/form', function () {
        return view('front/school/form');
    });
    Route::get('school/infos', function () {
        return view('front/school/infos');
    });




    /*
    TPL Language
    */
    Route::get('language/all', function () {
        return view('front/language/all');
    });
    Route::get('language/form', function () {
        return view('front/language/form');
    });


    /*
    TPL Gremark
    */
    Route::get('gremark/all', function () {
        return view('front/gremark/all');
    });
    Route::get('gremark/form', function () {
        return view('front/gremark/form');
    });


    /*
    TPL Remark
    */
    Route::get('remark/all', function () {
        return view('front/remark/all');
    });
    Route::get('remark/form', function () {
        return view('front/remark/form');
    });



    /*
    TPL Period
    */
    Route::get('period/all', function () {
        return view('front/period/all');
    });
    Route::get('period/form', function () {
        return view('front/period/form');
    });


    /*
    TPL LevelsTemplate
    */
    Route::get('levels_template/all', function () {
        return view('front/levels_template/all');
    });
    Route::get('levels_template/form', function () {
        return view('front/levels_template/form');
    });




    /*
    TPL Country
    */
    Route::get('country/all', function () {
        return view('front/country/all');
    });
    Route::get('country/form', function () {
        return view('front/country/form');
    });
    

    /*
    TPL Country
    */
    Route::get('city/all', function () {
        return view('front/city/all');
    });
    Route::get('city/form', function () {
        return view('front/city/form');
    });



    /*
    TPL Course
    */
    Route::get('course/all', function () {
        return view('front/course/all');
    });
    Route::get('course/form', function () {
        return view('front/course/form');
    });




    /*
    TPL Room
    */
    Route::get('room/all', function () {
        return view('front/room/all');
    });
    Route::get('room/form', function () {
        return view('front/room/form');
    });




    /*
    TPL School Class
    */
    Route::get('school_class/all', function () {
        return view('front/school_class/all');
    });
    Route::get('school_class/form', function () {
        return view('front/school_class/form');
    });




    /*
    TPL School Period
    */
    Route::get('school_period/all', function () {
        return view('front/school_period/all');
    });
    Route::get('school_period/form', function () {
        return view('front/school_period/form');
    });




    /*
    TPL Sdepartments
    */
    Route::get('sdepartment/all', function () {
        return view('front/sdepartment/all');
    });
    Route::get('sdepartment/form', function () {
        return view('front/sdepartment/form');
    });


    /*
    TPL TService
    */
    Route::get('tservice/all', function () {
        return view('front/tservice/all');
    });
    Route::get('tservice/form', function () {
        return view('front/tservice/form');
    });



    /*
    TPL WeekTemplate
    */
    Route::get('week_template/all', function () {
        return view('front/week_template/all');
    });
    Route::get('week_template/form', function () {
        return view('front/week_template/form');
    });

    
    /*
    TPL AlertType
    */
    Route::get('alert_type/all', function () {
        return view('front/alert_type/all');
    });
    Route::get('alert_type/form', function () {
        return view('front/alert_type/form');
    });
    


    /*
    TPL alert_status
    */
    Route::get('alert_status/all', function () {
        return view('front/alert_status/all');
    });
    Route::get('alert_status/form', function () {
        return view('front/alert_status/form');
    });
    


    /*
    TPL AttendanceStatus
    */
    Route::get('attendance_status/all', function () {
        return view('front/attendance_status/all');
    });
    Route::get('attendance_status/form', function () {
        return view('front/attendance_status/form');
    });


    /*
    TPL AttendanceType
    */
    Route::get('attendance_type/all', function () {
        return view('front/attendance_type/all');
    });
    Route::get('attendance_type/form', function () {
        return view('front/attendance_type/form');
    });


    /*
    TPL Attendance
    */
    Route::get('attendance/all', function () {
        return view('front/attendance/all');
    });
    Route::get('attendance/form', function () {
        return view('front/attendance/form');
    });


    /*
    TPL SchoolJob
    */
    Route::get('school_job/all', function () {
        return view('front/school_job/all');
    });
    Route::get('school_job/form', function () {
        return view('front/school_job/form');
    });
    /*
    TPL SchoolJob
    */
    Route::get('school_type/all', function () {
        return view('front/school_type/all');
    });
    Route::get('school_type/form', function () {
        return view('front/school_type/form');
    });


    /*
    TPL SchoolJob
    */
    Route::get('date_system/all', function () {
        return view('front/date_system/all');
    });
    Route::get('date_system/form', function () {
        return view('front/date_system/form');
    });


    /*
    TPL Genre
    */
    Route::get('genre/all', function () {
        return view('front/genre/all');
    });
    Route::get('genre/form', function () {
        return view('front/genre/form');
    });



    /*
    TPL IdnType
    */
    Route::get('idn_type/all', function () {
        return view('front/idn_type/all');
    });
    Route::get('idn_type/form', function () {
        return view('front/idn_type/form');
    });




    /*
    TPL SchoolYear
    */
    Route::get('school_year/all', function () {
        return view('front/school_year/all');
    });
    Route::get('school_year/form', function () {
        return view('front/school_year/form');
    });
    Route::get('school_year/infos', function () {
        return view('front/school_year/infos');
    });




    /*
    TPL Session Status
    */
    Route::get('session_status/all', function () {
        return view('front/session_status/all');
    });
    Route::get('session_status/form', function () {
        return view('front/session_status/form');
    });


    /*
    TPL Holiday
    */
    Route::get('holiday/all', function () {
        return view('front/holiday/all');
    });
    Route::get('holiday/form', function () {
        return view('front/holiday/form');
    });



    /*
    TPL Users
    */
    Route::get('rea_user/all', function () {
        return view('front/rea_user/all');
    });
    Route::get('rea_user/form', function () {
        return view('front/rea_user/form');
    });
    Route::get('rea_user/myinfos', function () {
        return view('front/rea_user/myinfos');
    });




    /*
    TPL Users
    */
    Route::get('school_term/all', function () {
        return view('front/school_term/all');
    });
    Route::get('school_term/form', function () {
        return view('front/school_term/form');
    });



    /*
    TPL Relship
    */
    Route::get('relship/all', function () {
        return view('front/relship/all');
    });
    Route::get('relship/form', function () {
        return view('front/relship/form');
    });



    /*
    TPL Relship
    */
    Route::get('rating/all', function () {
        return view('front/rating/all');
    });
    Route::get('rating/form', function () {
        return view('front/rating/form');
    });


    /*
    TPL CoursesTemplate
    */
    Route::get('courses_template/all', function () {
        return view('front/courses_template/all');
    });
    Route::get('courses_template/form', function () {
        return view('front/courses_template/form');
    });




    /*
    TPL CoursesTemplate
    */
    Route::get('wday/all', function () {
        return view('front/wday/all');
    });
    Route::get('wday/form', function () {
        return view('front/wday/form');
    });


    /*
    TPL LevelClass
    */
    Route::get('level_class/all', function () {
        return view('front/level_class/all');
    });
    Route::get('level_class/form', function () {
        return view('front/level_class/form');
    });




    /*
    TPL exam_session
    */
    Route::get('exam_session/all', function () {
        return view('front/exam_session/all');
    });
    Route::get('exam_session/form', function () {
        return view('front/exam_session/form');
    });




    /*
    TPL efile
    */
    Route::get('efile/all', function () {
        return view('front/efile/all');
    });
    Route::get('efile/form', function () {
        return view('front/efile/form');
    });



    /*
    TPL day_template
    */
    Route::get('day_template/all', function () {
        return view('front/day_template/all');
    });
    Route::get('day_template/form', function () {
        return view('front/day_template/form');
    });



    /*
    TPL day_template_item
    */
    Route::get('day_template_item/all', function () {
        return view('front/day_template_item/all');
    });
    Route::get('day_template_item/form', function () {
        return view('front/day_template_item/form');
    });


    /*
    TPL model_term
    */
    Route::get('model_term/all', function () {
        return view('front/model_term/all');
    });
    Route::get('model_term/form', function () {
        return view('front/model_term/form');
    });



    /*
    TPL student_exam
    */
    Route::get('student_exam/all', function () {
        return view('front/student_exam/all');
    });
    Route::get('student_exam/form', function () {
        return view('front/student_exam/form');
    });


    /*
    TPL student_file_status
    */
    Route::get('student_file_status/all', function () {
        return view('front/student_file_status/all');
    });
    Route::get('student_file_status/form', function () {
        return view('front/student_file_status/form');
    });



    /*
    TPL candidate_status
    */
    Route::get('candidate_status/all', function () {
        return view('front/candidate_status/all');
    });
    Route::get('candidate_status/form', function () {
        return view('front/candidate_status/form');
    });


    /*
    TPL mobile_type
    */
    Route::get('mobile_type/all', function () {
        return view('front/mobile_type/all');
    });
    Route::get('mobile_type/form', function () {
        return view('front/mobile_type/form');
    });


    
    /*
    TPL class_course_exam
    */
    Route::get('class_course_exam/all', function () {
        return view('front/class_course_exam/all');
    });
    Route::get('class_course_exam/form', function () {
        return view('front/class_course_exam/form');
    });




    
    /*
    TPL school_employee
    */
    Route::get('school_employee/all', function () {
        return view('front/school_employee/all');
    });
    Route::get('school_employee/form', function () {
        return view('front/school_employee/form');
    });
    Route::get('school_employee/replace', function () {
        return view('front/school_employee/replace');
    });
    Route::get('school_employee/schedule', function () {
        return view('front/school_employee/schedule');
    });
    


    
    /*
    TPL student
    */
    Route::get('student/all', function () {
        return view('front/student/all');
    });
    Route::get('student/form', function () {
        return view('front/student/form');
    });
    Route::get('student/add_to_school', function () {
        return view('front/student/add_to_school');
    });
    Route::get('student/transfer_seat', function () {
        return view('front/student/transfer_seat');
    });


    
    /*
    TPL school_registration
    */
    Route::get('school_registration/all', function () {
        return view('front/school_registration/all');
    });
    Route::get('school_registration/form', function () {
        return view('front/school_registration/form');
    });




    /*
    TPL family_relation
    */
    Route::get('family_relation/all', function () {
        return view('front/family_relation/all');
    });
    Route::get('family_relation/form', function () {
        return view('front/family_relation/form');
    });
    Route::get('family_relation/my_relations', function () {
        return view('front/family_relation/my_relations');
    });
    Route::get('family_relation/add_new_relation', function () {
        return view('front/family_relation/add_new_relation');
    });
    Route::get('family_relation/edit_my_relation', function () {
        return view('front/family_relation/edit_my_relation');
    });



    /*
    TPL alert
    */
    Route::get('alert/send', function () {
        return view('front/alert/send');
    });
    


    /*
    TPL translate
    */
    Route::get('translate/all', function () {
        return view('front/translate/all');
    });
    


    /*
    TPL school class course
    */
    Route::get('school_class_course/config_item', function () {
        return view('front/school_class_course/config_item');
    });
    

    /*
    TPL feed_import
    */
    Route::get('feed_import/all', function () {
        return view('front/feed_import/all');
    });
    Route::get('feed_import/process', function () {
        return view('front/feed_import/process');
    });




    /*
    TPL crm
    */
    Route::get('crm/erequest/submit', function () {
        return view('front/crm/erequest/submit');
    });
    Route::get('crm/erequest/settings', function () {
        return view('front/crm/erequest/settings');
    });
    Route::get('crm/erequest/my_erequests', function () {
        return view('front/crm/erequest/my_erequests');
    });
    Route::get('crm/erequest/follow_up', function () {
        return view('front/crm/erequest/follow_up');
    });
    Route::get('crm/erequest/received_erequests', function () {
        return view('front/crm/erequest/received_erequests');
    });
    Route::get('crm/erequest/reply_erequest', function () {
        return view('front/crm/erequest/reply_erequest');
    });



    /*
    TPL cpc
    */
    Route::get('cpc/book/all', function () {
        return view('front/cpc/book/all');
    });
    Route::get('cpc/book/form', function () {
        return view('front/cpc/book/form');
    });
    Route::get('cpc/book/pages', function () {
        return view('front/cpc/book/pages');
    });
    Route::get('cpc/book/page_form', function () {
        return view('front/cpc/book/page_form');
    });
    Route::get('cpc/course_program/all', function () {
        return view('front/cpc/course_program/all');
    });
    Route::get('cpc/course_program/form', function () {
        return view('front/cpc/course_program/form');
    });
    Route::get('cpc/course_program/books', function () {
        return view('front/cpc/course_program/books');
    });
    Route::get('cpc/course_program_plan/all_by_book_and_level', function () {
        return view('front/cpc/course_program_plan/all_by_book_and_level');
    });








    /*
    TPL role
    */
    Route::get('role/all', function () {
        return view('front/role/all');
    });
    Route::get('role/form', function () {
        return view('front/role/form');
    });


    /*
    TPL role
    */
    Route::get('test/all', function (){
        return view('front/test/all');
    });
    Route::get('test/form', function () {
        return view('front/test/form');
    });
    





    /*
    TPL club
    */
    Route::get('club/all', function () {
        return view('front/club/all');
    });
    Route::get('club/form', function () {
        return view('front/club/form');
    });



    /*
    TPL course_type
    */
    Route::get('course_type/all', function () {
        return view('front/course_type/all');
    });
    Route::get('course_type/form', function () {
        return view('front/course_type/form');
    });



    /*
    TPL coach
    */
    Route::get('coach/all', function () {
        return view('front/coach/all');
    });
    
    Route::get('coach/form', function () {
        return view('front/coach/form');
    });


    /*
    TPL planning
    */

    Route::get('planning/manage', function () {
        return view('front/planning/manage');
    });

    Route::get('planning/manage_list_calendar', function () {
        return view('front/planning/manage_list_calendar');
    });

    Route::get('planning/manage_coach', function () {
        return view('front/planning/manage_coach');
    });



    Route::get('planning/stats', function () {
        return view('front/planning/stats');
    });


    Route::get('planning/exportgrh', function () {
        return view('front/planning/exportgrh');
    });
    
    Route::get('planning/history', function () {
        return view('front/planning/history');
    });


    

    Route::get('holiday/manage', function () {
        return view('front/holiday/manage');
    });



    Route::get('cms/unauthorized', function () {
        return view('front/cms/unauthorized');
    });


    Route::get('setting/send_emails', function () {
        return view('front/setting/send_emails');
    });

});

