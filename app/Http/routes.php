<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return true;
});
return;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/', function () {
        return view('layouts/app_front');
    });
});



Route::group(['middleware' => ['web']], function () {

    Route::auth();


    Route::get('activation/{token}', [
        'as' => 'reaUsers.activation',
        'uses' => 'ReaUserController@activation',
    ]);

    Route::get('resetpassword/{token}', [
        'as' => 'reaUsers.reset_password',
        'uses' => 'ReaUserController@reset_password',
    ]);

    Route::get('register/school', function () {
        return view('front/school_registration/submit');
    });

});



Route::group([
    'version'   => 'v1',
    'prefix' => 'front',
    'namespace' => config('generator.namespace_front_controller'),
    'middleware' => ['web','auth']
], function ($front)
{
    include_once __DIR__ . '/front_routes.php';
});


Route::group(['middleware' => ['web','auth']], function () {

    Route::resource('reaUsers', 'ReaUserController');

    Route::get('reaUsers/{id}/delete', [
        'as' => 'reaUsers.delete',
        'uses' => 'ReaUserController@destroy',
    ]);

    //Route::resource('schools', 'SchoolController');
    Route::get('schools/generator', [
        'as' => 'schools.generator',
        'uses' => 'SchoolController@getSearchGenerator',
    ]);
    /*Route::post('schools/generator', [
        'as' => 'schools.generator',
        'uses' => 'SchoolController@postSearchGenerator',
    ]);*/
    Route::get('schools/{id}/delete', [
        'as' => 'schools.delete',
        'uses' => 'SchoolController@destroy',
    ]);

    Route::resource('schoolLevels', 'SchoolLevelController');

    Route::get('schoolLevels/{id}/delete', [
        'as' => 'schoolLevels.delete',
        'uses' => 'SchoolLevelController@destroy',
    ]);

});


/*
|--------------------------------------------------------------------------
| dingo/api and mitulgolakiya/laravel-api-generator api routes
|--------------------------------------------------------------------------
*/

$api = app('api.router');
//$api = app('api.router');

$api->group([
    'version'   => 'v1',
    'prefix' => 'api',
    'namespace' => config('generator.namespace_api_controller'),
    'middleware' => ['cors']
], function ($api)
{

    include_once __DIR__ . '/api_routes.php';

    $api->get('errors/{id}', function ($id)
    {
        return \Mitul\Generator\Errors::getErrors([$id]);
    });

    $api->get('errors', function ()
    {
        return \Mitul\Generator\Errors::getErrors([], [], true);
    });

    $api->get('/', function (){
        $links = [];
        return ['links' => $links];
    });

});





Route::get('file/book/page/{filename}', function($filename)
{
    $file_path = storage_path() .'/cpc/book_pages/'. $filename;

    if(!File::exists($file_path)) abort(404);

    $file = File::get($file_path);
    $type = File::mimeType($file_path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    $response->header("Pragma", 'max-age=604800, public');
    $response->header("Cache-Control", 'max-age=86400');
    $response->header("Expires", gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));

    return $response;
})
->where('filename', '[A-Za-z0-9\-\_\.]+');


// Download Route
Route::get('download/{filename}', function($filename)
{
    // Check if file exists in app/storage/file folder
    $file_path = storage_path() .'/public/'. $filename;
    if (file_exists($file_path))
    {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
})
->where('filename', '[A-Za-z0-9\-\_\.]+');



// Files private Route
Route::group(['middleware' => ['web','auth']], function () {
    Route::get('file/private/{filename}', function($filename)
    {
        $file_path = storage_path() .'/private/'. $filename;
        if(!File::exists($file_path)) abort(404);


        $user = Auth::user();
        
        $file = \App\Models\File::where('file_name',$filename)->first();
        if(!$file) abort(404);

        if($file->owner_id!=$user->id){
            exit('You dont have permission to read this file!');
        }

        $file = File::get($file_path);
        $type = File::mimeType($file_path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    })
    ->where('filename', '[A-Za-z0-9\-\_\.]+');
});



Route::get('file/public/{file_id}', function($file_id)
{
    $file = \App\Models\File::where('id',$file_id)->first();
    if(!$file) {
        $file_path = storage_path() .'/public/no-image.jpg';
        $file = File::get($file_path);
        $type = File::mimeType($file_path);
        // abort(404);
    } else {
        $file_path = storage_path() .'/public/'. $file->file_name;
        if ( !file_exists($file_path) ) {
            $file_path = storage_path() .'/public/no-image.jpg';
        }
        $file = File::get($file_path);
        $type = File::mimeType($file_path);
    }

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    $response->header("Pragma", 'max-age=604800, public');
    $response->header("Cache-Control", 'max-age=86400');
    $response->header("Expires", gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));

    return $response;
})->where('file_id', '[A-Za-z0-9\-\_\.]+');












Route::get('planning/{club_id}/all_rooms/{week_year?}/{week_num?}/{print_pdf?}', function($club_id,$week_year = null,$week_num = null, $print_pdf = null)
{
    
    if(!$week_num){
        $week_num = date("W");
    }


    if(!$week_year){
        $week_year = date("Y");
    }
    
    /*
    if($print_pdf){
        $url_ = App::make('url')->to('/').'/planning/'.$club_id.'/all_rooms/'.$week_year.'/'.$week_num;
        return SnappyPDF::loadFile($url_)->setOption('lowquality',false)->setPaper('a3')->setOrientation('landscape')->setOption('margin-right', 2)->setOption('margin-left', 2)->setOption('margin-top', 2)->inline('planning.pdf');
    }
    */
    
    $club_data = \App\Models\Club::find($club_id);
    
    $rooms = \App\Models\Room::where('club_id',$club_id)
    ->where('active','Y')
    ->whereNotIn('id',[22,23])
    ->get();

    if( !$club_data ){
        return '<h2 style="color: red;" >Données invalides</h2>';
    }



    $model = new \App\Models\CourseSession;
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }

    $course_sesssions = \App\Models\CourseSession::select($model_fields)
    ->where('club_id', $club_id)
    ->where('week_year', $week_year)
    ->where('week_num', $week_num)
    ->where('active','Y')
    ->orderBy('course_session_date', 'ASC')
    ->orderBy('course_start_time', 'ASC')
    ->get()
    ;
    $min_hour = null;
    $max_hour = null;
    $course_sessions_days = [
        1 => [],
        2 => [],
        3 => [],
        4 => [],
        5 => [],
        6 => [],
        7 => [],
    ];
    $max_per_day = 0;
    if(!$course_sesssions OR !count($course_sesssions)){
        return '<h2 style="color: red;" >Planning vide</h2>';
    }
    foreach($course_sesssions as &$item){

        if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
            continue;
        }

        $model_coach = new \App\Models\Coach;
        $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
        $model_room = new \App\Models\Room;
        $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
        $model_course = new \App\Models\Course;
        $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

        if($min_hour>$item->course_start_time OR !$min_hour){
            $min_hour = $item->course_start_time;
        }
        if($max_hour<$item->course_end_time OR !$max_hour){
            $max_hour = $item->course_end_time;
        }
        $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
        $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
        $item->during_hours = (float)($item->during_minutes/60);
        //$course_sessions_days = 
        $course_sessions_days[$course_session_date->format('N')][] = clone $item;
        if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
            $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
        }
    }

    $min_hour = substr($min_hour, 0,2).':00';
    $old_max_hour = $max_hour;
    $max_hour = substr($max_hour, 0,2).':00';
    $min_hour = (isset($_GET['min_hour']))? $_GET['min_hour'] : $min_hour;
    $max_hour = (isset($_GET['max_hour']))? $_GET['max_hour'] : $max_hour;
    if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
    $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    if($max_hour=='00:00' OR $min_hour>$max_hour){
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    }
    
    if ( \Carbon\Carbon::parse('01-01-'.$week_year)->format('N')==1 ) {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');
    } else {
        
    }

    $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
    $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');
   
  
    $data = [
        'week_day1' => 'Lundi',
        'week_day2' => 'Mardi',
        'week_day3' => 'Mercredi',
        'week_day4' => 'Jeudi',
        'week_day5' => 'Vendredi',
        'week_day6' => 'Samedi',
        'week_day7' => 'Dimanche',
        'max_per_day' => $max_per_day,
        'min_hour' => $min_hour,
        'max_hour' => $max_hour,
        'diff_hours' => $diff_hours,
        'course_sessions_days' => $course_sessions_days,
         'club' => $club_data,
        'rooms' => $rooms,
        'count_rooms' => count($rooms),
        'date_from' => $date_from,
        'date_to' => $date_to,
        'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
    ];
    
    
    
    return View::make('pdf.planning_all', $data);
});

/*
Route::get('planning/manage', function()
{
    
    $club_id = 9 ;
    $week_num = 31 ;
    $week_year = 2019 ;
    $today = date("Y-m-d");   
    if(!$week_num){
        $week_num = date("W");
    }
    if(!$week_year){
        $week_year = date("Y");
    }
    
    
    
    
    $club_data = \App\Models\Club::find($club_id);
    
    
    $rooms = \App\Models\Room::where('club_id',$club_id)
    ->where('active','Y')
    ->whereNotIn('id',[22,23])
    ->get();

    if( !$club_data ){
        return '<h2 style="color: red;" >Données invalides</h2>';
    }



    $model = new \App\Models\CourseSession;
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }

    $course_sesssions = \App\Models\CourseSession::select($model_fields)
    ->where('club_id', $club_id)
    ->where('course_session_date', $today)
    ->where('active','Y')
    ->orderBy('course_session_date', 'ASC')
    ->orderBy('course_start_time', 'ASC')
    ->get()
    ;
    $min_hour = null;
    $max_hour = null;
    $course_sessions_days = [
        1 => [],
        2 => [],
        3 => [],
        4 => [],
        5 => [],
        6 => [],
        7 => [],
    ];
    $max_per_day = 0;
    if(!$course_sesssions OR !count($course_sesssions)){
        return '<h2 style="color: red;" >Planning vide</h2>';
    }
    foreach($course_sesssions as &$item){

        if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
            continue;
        }

        $model_coach = new \App\Models\Coach;
        $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
        $model_room = new \App\Models\Room;
        $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
        $model_course = new \App\Models\Course;
        $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

        if($min_hour>$item->course_start_time OR !$min_hour){
            $min_hour = $item->course_start_time;
        }
        if($max_hour<$item->course_end_time OR !$max_hour){
            $max_hour = $item->course_end_time;
        }
        $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
        $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
        $item->during_hours = (float)($item->during_minutes/60);
        //$course_sessions_days = 
        $course_sessions_days[$course_session_date->format('N')][] = clone $item;
        if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
            $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
        }
    }

    $min_hour = substr($min_hour, 0,2).':00';
    $old_max_hour = $max_hour;
    $max_hour = substr($max_hour, 0,2).':00';
    $min_hour = (isset($_GET['min_hour']))? $_GET['min_hour'] : $min_hour;
    $max_hour = (isset($_GET['max_hour']))? $_GET['max_hour'] : $max_hour;
    if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
    $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    if($max_hour=='00:00' OR $min_hour>$max_hour){
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    }
    
    if ( \Carbon\Carbon::parse('01-01-'.$week_year)->format('N')==1 ) {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->endOfWeek()->format('d-m-Y');
    } else {
        
    }

    $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->startOfWeek()->format('d-m-Y');
    $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->endOfWeek()->format('d-m-Y');
   
  
    $data = [
        'week_day1' => 'Lundi',
        'week_day2' => 'Mardi',
        'week_day3' => 'Mercredi',
        'week_day4' => 'Jeudi',
        'week_day5' => 'Vendredi',
        'week_day6' => 'Samedi',
        'week_day7' => 'Dimanche',
        'max_per_day' => $max_per_day,
        'min_hour' => $min_hour,
        'max_hour' => $max_hour,
        'diff_hours' => $diff_hours,
        'course_sessions_days' => $course_sessions_days,
         'club' => $club_data,
        'rooms' => $rooms,
        'count_rooms' => count($rooms),
        'date_from' => $date_from,
        'date_to' => $date_to,
        'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
        'today' => $today
    ];
    
    return View::make('front.planning.calendar_rooms', $data);
});
*/

Route::get('planning/manage', function()
{
    
    $club_id=9;
    $week_year=2019;
    $week_num=31;
    
    if(!$week_num){
        $week_num = date("W");
    }
    if(!$week_year){
        $week_year = date("Y");
    }
    
    $club_data = \App\Models\Club::find($club_id);
    $rooms = \App\Models\Room::where('club_id',$club_id)
    ->where('active','Y')
    ->whereNotIn('id',[22,23])
    ->get();

    if( !$club_data ){
        return '<h2 style="color: red;" >Données invalides</h2>';
    }



    $model = new \App\Models\CourseSession;
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }
    
    $today = date("Y-m-d");    
    
    
    $course_sesssions = \App\Models\CourseSession::select($model_fields)
    ->where('club_id', $club_id)
    ->where('course_session_date', $today)
    ->where('active','Y')
    ->orderBy('course_session_date', 'ASC')
    ->orderBy('course_start_time', 'ASC')
    ->get();
    
    
    
    $min_hour = null;
    $max_hour = null;
    $course_sessions_days = [
        1 => [],
        2 => [],
        3 => [],
        4 => [],
        5 => [],
        6 => [],
        7 => [],
    ];
    $max_per_day = 0;
    if(!$course_sesssions OR !count($course_sesssions)){
        return '<h2 style="color: red;" >Planning vide</h2>';
    }
    foreach($course_sesssions as &$item){

        if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
            continue;
        }

        $model_coach = new \App\Models\Coach;
        $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
        $model_room = new \App\Models\Room;
        $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
        $model_course = new \App\Models\Course;
        $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

        if($min_hour>$item->course_start_time OR !$min_hour){
            $min_hour = $item->course_start_time;
        }

        
        if($max_hour<$item->course_end_time OR !$max_hour){
            $max_hour = $item->course_end_time;
        }
        $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
        $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
        $item->during_hours = (float)($item->during_minutes/60);
        //$course_sessions_days = 
        $course_sessions_days[$course_session_date->format('N')][] = clone $item;
        if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
            $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
        }
    }

    $min_hour = substr($min_hour, 0,2).':00';
    $old_max_hour = $max_hour;
    $max_hour = substr($max_hour, 0,2).':00';
    $min_hour = (isset($_GET['min_hour']))? $_GET['min_hour'] : $min_hour;
    $max_hour = (isset($_GET['max_hour']))? $_GET['max_hour'] : $max_hour;
    
    if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
    $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    
    if($max_hour=='00:00' OR $min_hour>$max_hour){
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    }
    
    if ( \Carbon\Carbon::parse('01-01-'.$week_year)->format('N')==1 ) {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->endOfWeek()->format('d-m-Y');
    } else {
        
    }

    $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->startOfWeek()->format('d-m-Y');
    $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->endOfWeek()->format('d-m-Y');
   
  
    $data = [
        'week_day1' => 'Lundi',
        'week_day2' => 'Mardi',
        'week_day3' => 'Mercredi',
        'week_day4' => 'Jeudi',
        'week_day5' => 'Vendredi',
        'week_day6' => 'Samedi',
        'week_day7' => 'Dimanche',
        'max_per_day' => $max_per_day,
        'min_hour' => $min_hour,
        'max_hour' => $max_hour,
        'diff_hours' => $diff_hours,
        'course_sessions_days' => $course_sessions_days,
        'club' => $club_data,
        'rooms' => $rooms,
        'count_rooms' => count($rooms),
        'date_from' => $date_from,
        'date_to' => $date_to,
        'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
        'today' => $today,
    ];
    //dd($data);
    return View::make('front.planning.calendar_rooms', $data);
});

Route::get('planning/coach/{club_id}/{coach_id}/{week_year}/{week_num}/{print_pdf?}', function($club_id,$coach_id,$week_year,$week_num, $print_pdf = null){
   
    if(!$week_num){
        $week_num = date("W");
    }
    if(!$week_year){
        $week_year = date("Y");
    }

    /*
    if($print_pdf){
        $url_ = App::make('url')->to('/').'/planning/coach/'.$coach_id.'/'.$week_year.'/'.$week_num;
        return SnappyPDF::loadFile($url_)->setOption('lowquality',false)->setPaper('a2')->setOrientation('landscape')->setOption('margin-bottom', 0)->inline('planning.pdf');
    }
    */
    
    
    $coach_data = \App\Models\Coach::find($coach_id);
    if( !$coach_data ){
        return '<h2 style="color: red;" >Données invalides</h2>';
    }

    


    $model = new \App\Models\CourseSession;
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }

    if($club_id == 0){
    $course_sesssions = \App\Models\CourseSession::select($model_fields)
    ->where('coach_id',$coach_id)
    ->where('week_year',$week_year)
    ->where('week_num',$week_num)
    ->where('active','Y')
    ->orderBy('course_session_date', 'ASC')
    ->orderBy('course_start_time', 'ASC')
    ->get()
    ;
    }else{
        $course_sesssions = \App\Models\CourseSession::select($model_fields)
        ->where('coach_id',$coach_id)
        ->where('club_id',$club_id)
        ->where('week_year',$week_year)
        ->where('week_num',$week_num)
        ->where('active','Y')
        ->orderBy('course_session_date', 'ASC')
        ->orderBy('course_start_time', 'ASC')
        ->get()
        ;
    }
    $min_hour = null;
    $max_hour = null;
    $course_sessions_days = [
        1 => [],
        2 => [],
        3 => [],
        4 => [],
        5 => [],
        6 => [],
        7 => [],
    ];
    $max_per_day = 0;
    if(!$course_sesssions OR !count($course_sesssions)){
        return '<h2 style="color: red;" >Planning vide</h2>';
    }
    foreach($course_sesssions as &$item){
        if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
            continue;
        }
        $model_coach = new \App\Models\Coach;
        $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
        $model_room = new \App\Models\Room;
        $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
        $model_club = new \App\Models\Club;
        $item->club = ($item->club_id)? \App\Models\Club::select($model_club->getFillable())->where('active','Y')->find($item->club_id) : null;
        $model_course = new \App\Models\Course;
        $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

        if($min_hour>$item->course_start_time OR !$min_hour){
            $min_hour = $item->course_start_time;
        }
        if($max_hour<$item->course_end_time OR !$max_hour){
            $max_hour = $item->course_end_time;
        }
        $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
        $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
        $item->during_hours = (float)($item->during_minutes/60);
        //$course_sessions_days = 
        $course_sessions_days[$course_session_date->format('N')][] = clone $item;
        if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
            $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
        }
    }

    $min_hour = substr($min_hour, 0,2).':00';
    $old_max_hour = $max_hour;
    $max_hour = substr($max_hour, 0,2).':00';

    $min_hour = (isset($_GET['min_hour']))? $_GET['min_hour'] : $min_hour;
    $max_hour = (isset($_GET['max_hour']))? $_GET['max_hour'] : $max_hour;

    if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
    $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour));
    if($max_hour=='00:00' OR $min_hour>$max_hour){
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    }
    
    if ( \Carbon\Carbon::parse('01-01-'.$week_year)->format('N')==1 ) {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->endOfWeek()->format('d-m-Y');
    } else {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');
    }

    $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->startOfWeek()->format('d-m-Y');
    $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->endOfWeek()->format('d-m-Y');
    

    $data = [
        'week_day1' => 'Lundi',
        'week_day2' => 'Mardi',
        'week_day3' => 'Mercredi',
        'week_day4' => 'Jeudi',
        'week_day5' => 'Vendredi',
        'week_day6' => 'Samedi',
        'week_day7' => 'Dimanche',
        'max_per_day' => $max_per_day,
        'min_hour' => $min_hour,
        'max_hour' => $max_hour,
        'diff_hours' => $diff_hours,
        'course_sessions_days' => $course_sessions_days,
        'coach' => $coach_data,
        'date_from' => $date_from,
        'date_to' => $date_to,
        'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
    ];
    
   
    return View::make('pdf.planning_coach', $data);
});








Route::get('planning/{club_id}/{room_id}/{week_year?}/{week_num?}/{print_pdf?}', function($club_id,$room_id,$week_year = null,$week_num = null, $print_pdf = null){

   
   
    if(!$week_num){
        $week_num = date("W");
    }
    if(!$week_year){
        $week_year = date("Y");
    }

   
    
    /*
    if($print_pdf){
        $url_ = App::make('url')->to('/').'/planning/'.$club_id.'/'.$room_id.'/'.$week_year.'/'.$week_num;
        return SnappyPDF::loadFile($url_)->setOption('lowquality',false)->setPaper('a2')->setOrientation('landscape')->setOption('margin-bottom', 0)->inline('planning.pdf');
    }
    */
    
    

    $club_data = \App\Models\Club::find($club_id);
    $room_data = \App\Models\Room::find($room_id);

    if( !$club_data OR !$room_data ){
        return '<h2 style="color: red;" >Données invalides</h2>';
    }



    $model = new \App\Models\CourseSession;
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }

    
    $course_sesssions = \App\Models\CourseSession::select($model_fields)
    ->where('club_id', $club_id)
    ->where('room_id', $room_id)
    ->where('week_year', $week_year)
    ->where('week_num', $week_num)
    ->where('active','<>', 'D')
    ->orderBy('course_session_date', 'ASC')
    ->orderBy('course_start_time', 'ASC')
    ->get()
    ;

    $min_hour = null;
    $max_hour = null;
    
    $course_sessions_days = [
        1 => [],
        2 => [],
        3 => [],
        4 => [],
        5 => [],
        6 => [],
        7 => [],
    ];

    $max_per_day = 0;
    if(!$course_sesssions OR !count($course_sesssions)){
        return '<h2 style="color: red;" >Planning vide</h2>';
    }
    foreach($course_sesssions as &$item){
        if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
            continue;
        }
        $model_coach = new \App\Models\Coach;
        $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
        $model_room = new \App\Models\Room;
        $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
        $model_course = new \App\Models\Course;
        $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

        if($min_hour>$item->course_start_time OR !$min_hour){
            $min_hour = $item->course_start_time;
        }
        if($max_hour<$item->course_end_time OR !$max_hour){
            $max_hour = $item->course_end_time;
        }
        $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
        $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
        $item->during_hours = (float)($item->during_minutes/60);
        //$course_sessions_days = 
        $course_sessions_days[$course_session_date->format('N')][] = clone $item;
        if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
            $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
        }
    }


    


    $min_hour = substr($min_hour, 0,2).':00';
    $old_max_hour = $max_hour;
    $max_hour = substr($max_hour, 0,2).':00';

    $min_hour = (isset($_GET['min_hour']))? $_GET['min_hour'] : $min_hour;
    $max_hour = (isset($_GET['max_hour']))? $_GET['max_hour'] : $max_hour;


    if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
    
    $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour));
    if($max_hour=='00:00' OR $min_hour>$max_hour){
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    }

    /*
    if( $week_num == 1 ) {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->endOfWeek()->format('d-m-Y');
    } else {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');
    }
    */
    
    /*if ( \Carbon\Carbon::parse('01-01-'.$week_year)->format('N') == 1 ) {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->endOfWeek()->format('d-m-Y');
    } else {
        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');
    }*/


    $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
    $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');

    $data = [
        'week_day1' => 'Lundi',
        'week_day2' => 'Mardi',
        'week_day3' => 'Mercredi',
        'week_day4' => 'Jeudi',
        'week_day5' => 'Vendredi',
        'week_day6' => 'Samedi',
        'week_day7' => 'Dimanche',
        'max_per_day' => $max_per_day,
        'min_hour' => $min_hour,
        'max_hour' => $max_hour,
        'diff_hours' => $diff_hours,
        'course_sessions_days' => $course_sessions_days,
        'club' => $club_data,
        'room' => $room_data,
        'date_from' => $date_from,
        'date_to' => $date_to,
        'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
    ];
    
    return View::make('pdf.planning', $data);
});




Route::get('fix_primary_coach_id', function(){
    $course_sessions = \App\Models\CourseSession::select('*')->whereNull('primary_coach_id')->get();
	foreach($course_sessions as $course_session) {
        \App\Models\CourseSession::where('id', '=', $course_session->id)->update(['primary_coach_id' => $course_session->coach_id]);
    }
});


/* begin update filtre concept 20-03-2020 */

Route::get('planning/{club_id}/{room_id}/{cours_id}/{coach_id}/{week_year?}/{week_num?}/{print_pdf?}', function($club_id,$room_id,$cours_id,$coach_id,$week_year = null,$week_num = null, $print_pdf = null){

   
   
    if(!$week_num){
        $week_num = date("W");
    }
    if(!$week_year){
        $week_year = date("Y");
    }

    /*
    if($print_pdf){
        $url_ = App::make('url')->to('/').'/planning/'.$club_id.'/'.$room_id.'/'.$week_year.'/'.$week_num;
        return SnappyPDF::loadFile($url_)->setOption('lowquality',false)->setPaper('a2')->setOrientation('landscape')->setOption('margin-bottom', 0)->inline('planning.pdf');
    }
    */
    
    

    $club_data = \App\Models\Club::find($club_id);
    $room_data = \App\Models\Room::find($room_id);

    if( !$club_data OR !$room_data ){
        return '<h2 style="color: red;" >Données invalides</h2>';
    }



    $model = new \App\Models\CourseSession;
    $model_fields = $model->getFillable();
    foreach ($model_fields as &$item) {
      $item = $model->getTable().'.'.$item;
    }

    if($cours_id != 0 && $coach_id != 0){
        $course_sesssions = \App\Models\CourseSession::select($model_fields)
        ->where('club_id', $club_id)
        ->where('room_id', $room_id)
        ->where('course_id', $cours_id)
        ->where('coach_id', $coach_id)
        ->where('week_year', $week_year)
        ->where('week_num', $week_num)
        ->orderBy('course_session_date', 'ASC')
        ->orderBy('course_start_time', 'ASC')
        ->get()
        ;
    }else if($cours_id == 0 && $coach_id != 0){
        $course_sesssions = \App\Models\CourseSession::select($model_fields)
        ->where('club_id', $club_id)
        ->where('room_id', $room_id)
        ->where('coach_id', $coach_id)
        ->where('week_year', $week_year)
        ->where('week_num', $week_num)
        ->orderBy('course_session_date', 'ASC')
        ->orderBy('course_start_time', 'ASC')
        ->get()
        ;
    }else if($cours_id != 0 && $coach_id == 0){

        $course_sesssions = \App\Models\CourseSession::select($model_fields)
        ->where('club_id', $club_id)
        ->where('room_id', $room_id)
        ->where('course_id', $cours_id)
        ->where('week_year', $week_year)
        ->where('week_num', $week_num)
        ->orderBy('course_session_date', 'ASC')
        ->orderBy('course_start_time', 'ASC')
        ->get()
        ;
    
    }else{
        $course_sesssions = \App\Models\CourseSession::select($model_fields)
        ->where('club_id', $club_id)
        ->where('room_id', $room_id)
        ->where('week_year', $week_year)
        ->where('week_num', $week_num)
        ->orderBy('course_session_date', 'ASC')
        ->orderBy('course_start_time', 'ASC')
        ->get()
        ;
    }

    $min_hour = null;
    $max_hour = null;
    
    $course_sessions_days = [
        1 => [],
        2 => [],
        3 => [],
        4 => [],
        5 => [],
        6 => [],
        7 => [],
    ];

    $max_per_day = 0;
    if(!$course_sesssions OR !count($course_sesssions)){
        return '<h2 style="color: red;" >Planning vide</h2>';
    }
    foreach($course_sesssions as &$item){
        if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
            continue;
        }
        $model_coach = new \App\Models\Coach;
        $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
        $model_room = new \App\Models\Room;
        $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
        $model_course = new \App\Models\Course;
        $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

        if($min_hour>$item->course_start_time OR !$min_hour){
            $min_hour = $item->course_start_time;
        }
        if($max_hour<$item->course_end_time OR !$max_hour){
            $max_hour = $item->course_end_time;
        }
        $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
        $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
        $item->during_hours = (float)($item->during_minutes/60);
        //$course_sessions_days = 
        $course_sessions_days[$course_session_date->format('N')][] = clone $item;
        if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
            $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
        }
    }

    $min_hour = substr($min_hour, 0,2).':00';
    $old_max_hour = $max_hour;
    $max_hour = substr($max_hour, 0,2).':00';

    $min_hour = (isset($_GET['min_hour']))? $_GET['min_hour'] : $min_hour;
    $max_hour = (isset($_GET['max_hour']))? $_GET['max_hour'] : $max_hour;


    if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
    
    $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour));
    if($max_hour=='00:00' OR $min_hour>$max_hour){
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
    }

    
    $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->startOfWeek()->format('d-m-Y');
    $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->endOfWeek()->format('d-m-Y');

    $data = [

        'week_day1' => 'Lundi',
        'week_day2' => 'Mardi',
        'week_day3' => 'Mercredi',
        'week_day4' => 'Jeudi',
        'week_day5' => 'Vendredi',
        'week_day6' => 'Samedi',
        'week_day7' => 'Dimanche',
        'max_per_day' => $max_per_day,
        'min_hour' => $min_hour,
        'max_hour' => $max_hour,
        'diff_hours' => $diff_hours,
        'course_sessions_days' => $course_sessions_days,
        'club' => $club_data,
        'room' => $room_data,
        'date_from' => $date_from,
        'date_to' => $date_to,
        'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
    ];
    
    return View::make('pdf.planning', $data);
});

/* end update filtre concept 20-03-2020 */