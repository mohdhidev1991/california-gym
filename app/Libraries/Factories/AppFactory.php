<?php namespace App\Libraries\Factories;



class AppFactory extends BaseFactory
{
    
	public static function get_current_hdate(){
		return self::get_hdate( date('Ymd') );
	}

	public static function get_hdate($gdate){
		if( get_cache('hdate:'.date('Ymd', strtotime($gdate))) ){
			return get_cache('hdate:'.date('Ymd', strtotime($gdate)));
		}
		$hdate = \App\Helpers\HijriDateHelper::to_hijri( date('Ymd', strtotime($gdate)) );
		set_cache('hdate:'.date('Ymd', strtotime($gdate)), $hdate);
		return $hdate;
	}


	public static function get_current_hday_num($school_year_id = null){
		return self::get_hday_num( date('Ymd'), $school_year_id );
	}

	public static function get_hday_num($gdate, $school_year_id = null){
		if(!$school_year_id) $school_year_id = \App\Libraries\Factories\ReaUserFactory::get_current_school_year_id();
		if(!$school_year_id) return null;

		if( get_cache('hday_num:'.$school_year_id.':'.date('Ymd', strtotime($gdate))) ){
			return get_cache('hday_num:'.$school_year_id.':'.date('Ymd', strtotime($gdate)));
		}
		$hday_num = \App\Helpers\HijriDateHelper::get_hday_num( date('Ymd', strtotime($gdate)), $school_year_id );
		set_cache('hday_num:'.$school_year_id.':'.date('Ymd', strtotime($gdate)), $hday_num);
		return $hday_num;
	}
	
}
