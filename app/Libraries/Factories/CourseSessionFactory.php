<?php
namespace App\Libraries\Factories;

use Auth;
use App\Libraries\Factories\AppFactory;
use App\Models\CourseSession;
use Illuminate\Support\Facades\View;
use Mail;


class CourseSessionFactory extends BaseFactory {



    public function send_week_email($week_year,$week_num, $club_id = null, $room_id = null, $coach_id = null, $step = 0){
        $return = [
            'coaches' => $this->getCoachesWeek($week_year, $week_num, $club_id, $room_id, $coach_id),
            'clubs' => $this->getClubsWeek($week_year, $week_num, $club_id, $room_id, $coach_id),
        ];

       
        if($step<count($return['clubs'])):
            
            $data_planning = $this->getClubPlanningData($week_year,$week_num, $return['clubs'][($step)]);
            
            if($data_planning AND !empty($data_planning)) $this->send_email_planning_club($data_planning);
            
            /*foreach($return['clubs'] as $item_club_id):
                $data_planning = $this->getClubPlanningData($week_year,$week_num,$item_club_id);
                if($data_planning) $this->send_email_planning_club($data_planning);
            endforeach;*/
        endif;

        

       

        if($step>(count($return['clubs'])-1) AND isset($return['coaches'][($step-count($return['clubs']))])):
            if( isset( $return['coaches'][($step-count($return['clubs']))] ) AND ($item_coach_id = $return['coaches'][($step-count($return['clubs']))]) ){
                $data_planning = $this->getCoachPlanningData($week_year, $week_num, $item_coach_id);
                if($data_planning AND !empty($data_planning)) $this->send_email_planning_coach($data_planning);
            }
            /*foreach($return['coaches'] as $item_coach_id):
                $data_planning = $this->getCoachPlanningData($week_year,$week_num,$item_coach_id);
                if($data_planning) $this->send_email_planning_coach($data_planning);
            endforeach;
            */
        endif;
        
        $return['steps'] = count($return['clubs'])+count($return['coaches']);
        return $return;
    }


    public function send_email_planning_coach($data_planning){

        $data = [];
        if(!isset($data_planning['coach']) OR !$data_planning['coach'] OR !isset($data_planning['coach']->email) OR !filter_var($data_planning['coach']->email, FILTER_VALIDATE_EMAIL) ){
            return;
        }

        $pdf = \SnappyPDF::loadView('pdf.planning_coach', $data_planning)->setOption('lowquality',false)->setPaper('a2')->setOrientation('landscape')->setOption('margin-bottom', 0);
        Mail::send('emails.planning_coach', $data_planning, function($message) use($pdf, $data_planning) {
            
            /*$email_to = [];
            if(isset($data_planning['coach']->email) AND filter_var($data_planning['coach']->email, FILTER_VALIDATE_EMAIL) ){
                $email_to[] = $data_planning['coach']->email;
            }*/
            // $email_to = ['achraffessi@gmail.com'];

            $email_to = ['mohamed.mohdhi@digit-u.com'];
            $message->to($email_to)->subject('Planning du '.$data_planning['date_from'].' au '.$data_planning['date_to']);
            $message->attachData($pdf->output(), 'Planning du '.$data_planning['date_from'].' au '.$data_planning['date_to'].".pdf");
        
        });
    }


    public function send_email_planning_club($data_planning){
        $data = [];
        $pdf = \SnappyPDF::loadView('pdf.planning_all', $data_planning)->setOption('lowquality',false)->setPaper('a2')->setOrientation('landscape')->setOption('margin-bottom', 0);
        if(!isset($data_planning['club']) OR !$data_planning['club']
            OR (!isset($data_planning['club']->email_1) AND !isset($data_planning['club']->email_2))
            OR ( !filter_var($data_planning['club']->email_1, FILTER_VALIDATE_EMAIL) AND !filter_var($data_planning['club']->email_2, FILTER_VALIDATE_EMAIL) )
        ){
            return;
        }
        Mail::send('emails.planning_all', $data_planning, function($message) use($pdf, $data_planning) {
           /*$email_to = [];
            if(isset($data_planning['club']->email_1) AND filter_var($data_planning['club']->email_1, FILTER_VALIDATE_EMAIL) ){
                $email_to[] = $data_planning['club']->email_1;
            }
            if(isset($data_planning['club']->email_2) AND filter_var($data_planning['club']->email_2, FILTER_VALIDATE_EMAIL) ){
                $email_to[] = $data_planning['club']->email_2;
            }
            */
            //$extra_name = ($data_planning['club']->email_1)? ' (dev: sould be sended to '.$data_planning['club']->email_1.' & '.$data_planning['club']->email_2.')' : null;
            //$email_to = [];
            $email_to = ['mohdhidev@gmail.com'];
            $message->to($email_to)->subject('Planning du '.$data_planning['date_from'].' au '.$data_planning['date_to']);
            $message->attachData($pdf->output(), 'Planning du '.$data_planning['date_from'].' au '.$data_planning['date_to'].".pdf");
        });
    }


    public function getClubsWeek($week_year, $week_num, $club_id = null, $room_id = null, $coach_id = null){
        $model = new \App\Models\CourseSession;
        $query = $model->select('club_id')
        ->where('active','Y')
        ->where('week_num',$week_num)
        ->where('week_year',$week_year)
        ->groupBy('club_id');
        if($club_id) $query->where('club_id',$club_id);
        if($room_id) $query->where('room_id',$room_id);
        if($coach_id) $query->where('coach_id',$coach_id);
        $results = $query->get()->lists('club_id');
        return $results;
    }


    public function getCoachesWeek($week_year, $week_num, $club_id = null, $room_id = null, $coach_id = null){
        $model = new \App\Models\CourseSession;
        $query = $model->select('coach_id')
        ->where('active','Y')
        ->where('week_num',$week_num)
        ->where('week_year',$week_year)
        ->groupBy('coach_id');
        if($club_id) $query->where('club_id',$club_id);
        if($room_id) $query->where('room_id',$room_id);
        if($coach_id) $query->where('coach_id',$coach_id);
        $results = $query->get()->lists('coach_id');
        return $results;
    }


    public function getCoachPlanningData($week_year, $week_num, $coach_id = null){
        $coach_data = \App\Models\Coach::find($coach_id);

        $model = new \App\Models\CourseSession;
        $model_fields = $model->getFillable();
        foreach ($model_fields as &$item) {
          $item = $model->getTable().'.'.$item;
        }
        $course_sesssions = \App\Models\CourseSession::select($model_fields)
        ->where('coach_id',$coach_id)
        ->where('week_year',$week_year)
        ->where('week_num',$week_num)
        ->where('active','Y')
        ->orderBy('course_session_date', 'ASC')
        ->orderBy('course_start_time', 'ASC')
        ->get()
        ;
        $min_hour = null;
        $max_hour = null;
        $course_sessions_days = [
            1 => [],
            2 => [],
            3 => [],
            4 => [],
            5 => [],
            6 => [],
            7 => [],
        ];
        $max_per_day = 0;
        if(!$course_sesssions OR !count($course_sesssions)){
            return false;
        }
        foreach($course_sesssions as &$item){
            if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
                continue;
            }
            $model_coach = new \App\Models\Coach;
            $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
            $model_room = new \App\Models\Room;
            $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
            $model_club = new \App\Models\Club;
            $item->club = ($item->club_id)? \App\Models\Club::select($model_club->getFillable())->where('active','Y')->find($item->club_id) : null;
            $model_course = new \App\Models\Course;
            $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

            if($min_hour>$item->course_start_time OR !$min_hour){
                $min_hour = $item->course_start_time;
            }
            if($max_hour<$item->course_end_time OR !$max_hour){
                $max_hour = $item->course_end_time;
            }
            $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
            $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
            $item->during_hours = (float)($item->during_minutes/60);
            //$course_sessions_days = 
            $course_sessions_days[$course_session_date->format('N')][] = clone $item;
            if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
                $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
            }
        }

        $min_hour = substr($min_hour, 0,2).':00';
        $old_max_hour = $max_hour;
        $max_hour = substr($max_hour, 0,2).':00';
        if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour));
        if($max_hour=='00:00' OR $min_hour>$max_hour){
            $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
        }
        
        /*if ( \Carbon\Carbon::parse('01-01-'.$week_year)->format('N')==1 ) {
            $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->startOfWeek()->format('d-m-Y');
            $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->endOfWeek()->format('d-m-Y');
        } else {
            $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
            $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');
        }*/

        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->endOfWeek()->format('d-m-Y');

        $data = [
            'week_day1' => 'Lundi',
            'week_day2' => 'Mardi',
            'week_day3' => 'Mercredi',
            'week_day4' => 'Jeudi',
            'week_day5' => 'Vendredi',
            'week_day6' => 'Samedi',
            'week_day7' => 'Dimanche',
            
            'max_per_day' => $max_per_day,
            'min_hour' => $min_hour,
            'max_hour' => $max_hour,
            'diff_hours' => $diff_hours,

            'course_sessions_days' => $course_sessions_days,

            'coach' => $coach_data,

            'date_from' => $date_from,
            'date_to' => $date_to,
            'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
        ];
        return $data;
    }



    public function getClubPlanningData($week_year, $week_num, $club_id = null){
        $club_data = \App\Models\Club::find($club_id);
        $rooms = \App\Models\Room::where('club_id',$club_id)->where('active','Y')->get();

        $model = new \App\Models\CourseSession;
        $model_fields = $model->getFillable();
        foreach ($model_fields as &$item) {
          $item = $model->getTable().'.'.$item;
        }
        $course_sesssions = \App\Models\CourseSession::select($model_fields)
        ->where('club_id',$club_id)
        ->where('week_year',$week_year)
        ->where('week_num',$week_num)
        ->where('active','Y')
        ->orderBy('course_session_date', 'ASC')
        ->orderBy('course_start_time', 'ASC')
        ->get()
        ;
        $min_hour = null;
        $max_hour = null;
        $course_sessions_days = [
            1 => [],
            2 => [],
            3 => [],
            4 => [],
            5 => [],
            6 => [],
            7 => [],
        ];
        $max_per_day = 0;
        if(!$course_sesssions OR !count($course_sesssions)){
            return false;
        }
        foreach($course_sesssions as &$item){
            if(!$item->course_start_time OR !$item->course_end_time OR !$item->course_session_date){
                continue;
            }
            $model_coach = new \App\Models\Coach;
            $item->coach = ($item->coach_id)? \App\Models\Coach::select($model_coach->getFillable())->where('active','Y')->find($item->coach_id) : null;
            $model_room = new \App\Models\Room;
            $item->room = ($item->room_id)? \App\Models\Room::select($model_room->getFillable())->where('active','Y')->find($item->room_id) : null;
            $model_course = new \App\Models\Course;
            $item->course = ($item->course_id)? \App\Models\Course::select($model_course->getFillable())->where('active','Y')->find($item->course_id) : null;

            if($min_hour>$item->course_start_time OR !$min_hour){
                $min_hour = $item->course_start_time;
            }
            if($max_hour<$item->course_end_time OR !$max_hour){
                $max_hour = $item->course_end_time;
            }
            $course_session_date = \Carbon\Carbon::createFromFormat('Y-m-d',$item->course_session_date);
            $item->during_minutes = \Carbon\Carbon::createFromFormat('H:i',$item->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$item->course_end_time));
            $item->during_hours = (float)($item->during_minutes/60);
            //$course_sessions_days = 
            $course_sessions_days[$course_session_date->format('N')][] = clone $item;
            if(count($course_sessions_days[$course_session_date->format('N')])>$max_per_day){
                $max_per_day = count($course_sessions_days[$course_session_date->format('N')]);
            }
        }

        $min_hour = substr($min_hour, 0,2).':00';
        $old_max_hour = $max_hour;
        $max_hour = substr($max_hour, 0,2).':00';
        if($old_max_hour!=$max_hour) $max_hour = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->addHours(1)->format('H:i');
        $diff_hours = \Carbon\Carbon::createFromFormat('H:i',$max_hour)->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour));
        if($max_hour=='00:00' OR $min_hour>$max_hour){
            $diff_hours = \Carbon\Carbon::createFromFormat('H:i','23:00')->diffInHours(\Carbon\Carbon::createFromFormat('H:i',$min_hour))+1;
        }
        
        /*if ( \Carbon\Carbon::parse('01-01-'.$week_year)->format('N')==1 ) {
            $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->startOfWeek()->format('d-m-Y');
            $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num-1)->endOfWeek()->format('d-m-Y');
        } else {
            $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->startOfWeek()->format('d-m-Y');
            $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num)->endOfWeek()->format('d-m-Y');
        }*/

        $date_from = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->startOfWeek()->format('d-m-Y');
        $date_to = \Carbon\Carbon::parse('01-01-'.$week_year)->addWeeks($week_num - 1)->endOfWeek()->format('d-m-Y');

        return [
            'week_day1' => 'Lundi',
            'week_day2' => 'Mardi',
            'week_day3' => 'Mercredi',
            'week_day4' => 'Jeudi',
            'week_day5' => 'Vendredi',
            'week_day6' => 'Samedi',
            'week_day7' => 'Dimanche',
            
            'max_per_day' => $max_per_day,
            'min_hour' => $min_hour,
            'max_hour' => $max_hour,
            'diff_hours' => $diff_hours,

            'course_sessions_days' => $course_sessions_days,

            'club' => $club_data,
            'rooms' => $rooms,
            'count_rooms' => count($rooms),

            'date_from' => $date_from,
            'date_to' => $date_to,
            'days_to_show' => (isset($_GET['days_to_show']))? explode(',', $_GET['days_to_show']) : [1,2,3,4,5,6,7],
        ];
        
    }

}


