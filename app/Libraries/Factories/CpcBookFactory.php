<?php namespace App\Libraries\Factories;

use App\Models\CpcBook;
use App\Models\CpcBookPage;
use App\Models\File;
use \Spatie\PdfToImage\Pdf as PdfToImage;


class CpcBookFactory extends BaseFactory
{
    
    function __construct()
    {
        parent::__construct();
    }

    public static function generate_book_pages_from_file($cpc_book_id)
    {
        $cpc_book = CpcBook::find($cpc_book_id);
        if(!$cpc_book OR !$cpc_book->file_id){
            return false;
        }
        $file = File::find($cpc_book->file_id);
        if(!$file){
            return false;
        }
        $pathToPdf = storage_path("cpc/books").'/'.$file->id.'.'.$file->file_ext;

        $pdf = new PdfToImage($pathToPdf);
        foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) {
            $invID = str_pad($pageNumber, 3, '0', STR_PAD_LEFT);
            $image_file_name = $cpc_book->id.'_'.$invID.'.jpg';
            $pathToImagePage = storage_path("cpc/book_pages").'/'.$image_file_name;

            $pdf->setPage($pageNumber)
            ->saveImage($pathToImagePage);

            $cpc_book_page = CpcBookPage::where('book_id',$cpc_book_id)
            ->where('book_page_num',$pageNumber)
            ->first();

            if($cpc_book_page){
                $cpc_book_page = CpcBookPage::find($cpc_book_page->id);
                if($cpc_book_page->version){
                    $cpc_book_page->version++;
                }else{
                    $cpc_book_page->version = 1;
                }
                $cpc_book_page->book_page_url = $image_file_name;
                $cpc_book_page->save();
            }else{
                $cpc_book_page = new CpcBookPage();
                $cpc_book_page->active = 'Y';
                $cpc_book_page->version = 1;
                $cpc_book_page->book_id = $cpc_book_id;
                $cpc_book_page->book_page_num = $pageNumber;
                $cpc_book_page->book_page_name = translate('CPC','PageNumber',['page_num'=>$pageNumber],'ar');
                $cpc_book_page->book_page_content = null;
                $cpc_book_page->book_page_url = $image_file_name;
                $cpc_book_page->save();
            }

        }
    }

}
