<?php namespace App\Libraries\Factories;

use App\Models\CpcCoursePlan;
use App\Models\CpcCourseProgram;
use App\Models\CpcBook;
use App\Models\CpcBookPage;


class CpcCoursePlanFactory extends BaseFactory
{
    
    function __construct()
    {
        parent::__construct();
    }

    public static function generate_course_plan_from_course_program($cpc_course_program_id)
    {
        $cpc_course_program = CpcCourseProgram::find($cpc_course_program_id);
        if(!$cpc_course_program){
            return ['success' => false, 'error' => 'CPC.IvalideCourseProgram'];
        }

        /* Get Books  */
        $books = CpcBook::select(['cpc_book.id as book_id','cpc_book.course_id','cpc_book.book_nb_pages','cpc_course_program_book.level_class_id','cpc_course_program_book.course_program_id','cpc_course_program_book.course_nb'])
        ->join('cpc_course_program_book','cpc_course_program_book.book_id','=','cpc_book.id')
        ->where('cpc_course_program_book.course_program_id',$cpc_course_program->id)
        ->where('cpc_book.active','Y')
        ->get();

        if(empty($books)){
            return ['success' => false, 'error' => 'CPC.EmptyCourseProgramBooks'];
        }
        foreach ($books as $book) {
            /* Count Plans Generated */
            $count_plans = CpcCoursePlan::select(['cpc_book.id','cpc_book.course_id','cpc_book.book_nb_pages','cpc_course_program_book.level_class_id'])
            ->where('course_book_id',$book->book_id)
            ->where('course_id',$book->course_id)
            ->where('level_class_id',$book->level_class_id)
            ->where('course_program_id',$book->course_program_id)
            ->count();

            /* No Course Plan generated for book */
            /* OR Book Plan generated but not all course_nb */
            if($count_plans==0 OR $count_plans<$book->course_nb){
                //echo "No Course Plan generated for book\n";
                for($i_missed_page = $count_plans+1; $i_missed_page <= $book->course_nb ; $i_missed_page++){
                    //echo "generet missed {$i_missed_page} \n";
                    $new_plan = new CpcCoursePlan();
                    $new_plan->active = 'Y';
                    $new_plan->course_program_id = $book->course_program_id;
                    $new_plan->course_book_id = $book->book_id;
                    $new_plan->level_class_id = $book->level_class_id;
                    $new_plan->course_id = $book->course_id;
                    $new_plan->course_num = $i_missed_page;
                    /*if($book->course_nb AND $book->book_nb_pages){
                        $position_ = (float)($book->book_nb_pages/$book->course_nb);
                        $new_plan->course_book_from_page = intval(($position_)*$i_missed_page);
                        $new_plan->course_book_to_page = intval(($position_)*($i_missed_page+1))-1;
                    }*/
                    $new_plan->save();
                }
            }
            /* Book Plan already generated for all course_nb */
            elseif($count_plans==$book->course_nb){
                /* No action */
            }elseif($count_plans>$book->course_nb){
                /* Return error */
                if(empty($books)){
                    return ['success' => false, 'error' => 'CPC.ErrorCoursePlansForBook'];
                }
            }
        }
        return ['success' => true];
    }

}
