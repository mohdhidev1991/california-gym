<?php namespace App\Libraries\Factories;

use App\Models\CrmShParams;


class CrmErequestFactory extends BaseFactory
{
    
	function __construct()
	{
		parent::__construct();
	}

        public static function get_stakeholder_and_owner_request($school_stacke_holder_id, $erequest_type_id, $erequest_cat_id, $request_level_enum, $my_jobrole_mfk = ",0," )
        {
                $erequest_sh_id = null;
                $erequest_owner_id = null;

                $my_jobrole_mfk = '"'.$my_jobrole_mfk.'"';
                $crm_sh = CrmShParams::where('stakeholder_id',$school_stacke_holder_id)->first();
                if($crm_sh AND isset($crm_sh->crm_erequest_policy_id)){
                        $crm_erequest_policy_id = $crm_sh->crm_erequest_policy_id;

                        $query_raw = \DB::raw("SELECT  id, `internal`, `external_stakeholder_id`, `dep_crm_activity_id`, `owner_jobrole_id`
                                FROM `crm_erequest_policy_det` 
                                WHERE `crm_erequest_policy_id` = {$crm_erequest_policy_id} 
                                and ({$my_jobrole_mfk} like concat('%,',`requester_jobrole_id`,',%') or `requester_jobrole_id`=0) 
                                and (`crm_erequest_type_id` = {$erequest_type_id} ) 
                                and (`crm_erequest_cat_id` = {$erequest_cat_id} or `crm_erequest_cat_id` = 0) 
                                and (`crm_erequest_level_enum` = {$request_level_enum} or `crm_erequest_level_enum` = 0)
                                and `active` = 'Y'
                                order by requester_jobrole_id desc, crm_erequest_cat_id desc, crm_erequest_level_enum desc
                        limit 1;");
                        $crm_erequest_policy_det = \DB::connection('mysqlcrm')->select($query_raw);
                        $crm_erequest_policy_det = reset($crm_erequest_policy_det);

                        if($crm_erequest_policy_det AND isset($crm_erequest_policy_det->id)){
                                if($crm_erequest_policy_det->internal=='Y' AND $crm_erequest_policy_det->dep_crm_activity_id AND $crm_erequest_policy_det->owner_jobrole_id){
                                        $query_raw = \DB::raw("SELECT `stakeholder_id` 
                                                FROM `crm_sh_params` 
                                                WHERE `crm_activity_id` = {$crm_erequest_policy_det->dep_crm_activity_id} 
                                                and  `stakeholder_id` in (select id from ".config('database.connections.mysqlpag.database').".stakeholder sh where sh.id_sh_org = {$school_stacke_holder_id} and sh.avail='Y')
                                                limit 1;");
                                        $crm_sh_params = \DB::connection('mysqlcrm')->select($query_raw);
                                        $crm_sh_params = reset($crm_sh_params);

                                        if($crm_sh_params AND $crm_sh_params->stakeholder_id){
                                                $erequest_sh_id = $crm_sh_params->stakeholder_id;
                                                $query_raw = \DB::raw("select emp.auser_id, count(req.id) as nb_req 
                                                  from c0hrm.employee emp
                                                  left join c0crm.crm_erequest req on req.owner_auser_id = emp.auser_id and req.active ='Y' and req.crm_erequest_status_id in (1,2) 
                                                where (emp.id_sh_div = {$erequest_sh_id}
                                                        or emp.id_sh_div2 = {$erequest_sh_id}
                                                        or emp.id_sh_div3 = {$erequest_sh_id})
                                                  and emp.active = 'Y'
                                                  and emp.jobrole_mfk like concat('%,',$crm_erequest_policy_det->owner_jobrole_id,',%')
                                                group by emp.auser_id
                                                order by nb_req asc
                                                limit 1;");
                                                $employee = \DB::connection('mysqlcrm')->select($query_raw);
                                                $employee = reset($employee);
                                                if($employee AND $employee->auser_id){
                                                        return [
                                                                'erequest_sh_id' => $erequest_sh_id,
                                                                'erequest_owner_id' => $employee->auser_id,
                                                                'crm_erequest_policy_id' => $crm_erequest_policy_det->id,
                                                        ];
                                                }else{
                                                        return [
                                                                'erequest_sh_id' => $erequest_sh_id,
                                                                'erequest_owner_id' => null,
                                                                'crm_erequest_policy_id' => $crm_erequest_policy_det->id,
                                                        ];
                                                }
                                        }
                                }
                        }
                }

                return [
                        'erequest_sh_id' => null,
                        'erequest_owner_id' => null
                ];
        }

}
