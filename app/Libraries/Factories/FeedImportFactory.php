<?php namespace App\Libraries\Factories;

use App\Models\FeedImport;
use App\Models\Student;
use App\Models\StudentFile;
use App\Models\ReaUser;
use App\Models\SchoolYear;
use App\Models\FamilyRelation;
use App\Models\SchoolEmployee;
use App\Models\Employee;
use App\Models\School;
use App\Models\Sdepartment;
use App\Models\SProf;
use App\Helpers\validateIdn;
use DB;
use App\Helpers\MfkHelper;

class FeedImportFactory extends BaseFactory
{
    
	function __construct()
	{
		parent::__construct();
	}


	public static function process_import($processor = null, $datas = null, $item = null, $options = [])
	{
		switch ($processor) {
			case 'students_parent':
				return self::import_students_parent($datas, $item, $options);
				break;
			default:

			case 'school_employees':
				return self::import_school_employees($datas, $item, $options);
				break;

			default:
				return [
					'success' => false,
					'errors' => ["FeedImport.InvalidProcessor"],
				];
				break;
		}
		
	}


	public static function import_students_parent($datas, $item, $options)
	{
		$return = [];

		if( !ReaUserFactory::get_current_school_id() ){
			return [
				'success' => false,
				'errors' => ["SchoolEmployee.SchoolNotSelected"],
			];
		}
		$current_school_id = ReaUserFactory::get_current_school_id();

		$options['school_id'] = $current_school_id;

		$school_year = SchoolYear::select('year')->where('school_id',$options['school_id'])->where('id',$options['school_year_id'])->first();
		if(!$school_year){
			return [
				'success' => false,
				'errors' => ["SchoolEmployee.SchoolYearInvalid"],
			];
		}
		$options['hyear'] = $school_year->year;

		if( !isset($datas[$item-1]) ){
			return [
				'success' => false,
				'errors' => ["Form.EmptyData"],
			];
		}
		$total = count($datas);
		$data = (object) $datas[$item-1];

		if(!isset($options['maintain_students'])){
			$options['maintain_students'] = false;
		}

		$element_name = (isset($data->student_idn) AND !empty($data->student_idn))? "Student IDN {$data->student_idn} :"  : "Element Number {$item}:" ;
		if( !isset($data->level_class) OR empty($data->level_class) ){
			return [ 'success' => false, 'errors' => [$element_name." level class not exists."]];
		}
		if( $options['maintain_students'] AND (!isset($data->school_class) OR empty($data->school_class)) ){
			return [ 'success' => false, 'errors' => [$element_name." school class not exists."]];
		}
		if( !isset($data->parent_idn) OR empty($data->parent_idn) ){
			return [ 'success' => false, 'errors' => [$element_name." parent idn not exists."]];
		}
		if( !isset($data->parent_idn_type) OR empty($data->parent_idn_type) ){
			return [ 'success' => false, 'errors' => [$element_name." parent idn type not exists."]];
		}
		if( !isset($data->parent_genre) OR empty($data->parent_genre) ){
			return [ 'success' => false, 'errors' => [$element_name." parent genre not exists."]];
		}
		if( !isset($data->parent_nationality) OR empty($data->parent_nationality) ){
			return [ 'success' => false, 'errors' => [$element_name." parent nationality not exists."]];
		}
		if( !isset($data->parent_mobile) OR empty($data->parent_mobile) ){
			return [ 'success' => false, 'errors' => [$element_name." parent mobile not exists."]];
		}
		if( !isset($data->parent_firstname) OR empty($data->parent_firstname) ){
			return [ 'success' => false, 'errors' => [$element_name." parent first name not exists."]];
		}
		if( !isset($data->parent_lastname) OR empty($data->parent_lastname) ){
			return [ 'success' => false, 'errors' => [$element_name." parent last name not exists."]];
		}
		if( !isset($data->parent_fatherfirstname) OR empty($data->parent_fatherfirstname) ){
			return [ 'success' => false, 'errors' => [$element_name." parent father name not exists."]];
		}
		if( !isset($data->student_idn) OR empty($data->student_idn) ){
			return [ 'success' => false, 'errors' => [$element_name." student idn not exists."]];
		}
		if( !isset($data->student_idn_type) OR empty($data->student_idn_type) ){
			return [ 'success' => false, 'errors' => [$element_name." student idn type not exists."]];
		}
		if( !isset($data->student_genre) OR empty($data->student_genre) ){
			return [ 'success' => false, 'errors' => [$element_name." student genre not exists."]];
		}
		if( !isset($data->student_nationality) OR empty($data->student_nationality) ){
			return [ 'success' => false, 'errors' => [$element_name." student nationality not exists."]];
		}
		if( !isset($data->student_firstname) OR empty($data->student_firstname) ){
			return [ 'success' => false, 'errors' => [$element_name." student first name not exists."]];
		}
		if( !isset($data->student_lastname) OR empty($data->student_lastname) ){
			return [ 'success' => false, 'errors' => [$element_name." student last name not exists."]];
		}
		if( !isset($data->student_fatherfirstname) OR empty($data->student_fatherfirstname) ){
			return [ 'success' => false, 'errors' => [$element_name." student father name not exists."]];
		}
		if( !isset($data->student_birthday) OR empty($data->student_birthday) ){
			return [ 'success' => false, 'errors' => [$element_name." student birthday not exists."]];
		}
		if( !isset($data->student_level_class) OR empty($data->student_level_class) ){
			return [ 'success' => false, 'errors' => [$element_name." student level class not exists."]];
		}
		if( !isset($data->relationship_type) OR empty($data->relationship_type) ){
			return [ 'success' => false, 'errors' => [$element_name." student level class not exists."]];
		}
		if( !isset($data->family_relation_description) OR empty($data->family_relation_description) ){
			return [ 'success' => false, 'errors' => [$element_name." family relation description not exists."]];
		}

		if( isset($data->parent_email) AND !empty($data->parent_email) AND !filter_var($data->parent_email, FILTER_VALIDATE_EMAIL) ){
			return [ 'success' => false, 'errors' => [$element_name." parent email not valid."]];
		}
		

		//$parent_user = ReaUser::where('idn',$data->parent_idn)->where('idn_type_id',$data->parent_idn_type)->first()->toArray();


		if(!validateIdn::check($data->parent_idn)){
			return [ 'success' => false, 'errors' => [$element_name." parent idn invalid."]];
		}
		$data->parent_idn_type_id =  self::meta_id_by_key('id', 'idn_type', $data->parent_idn_type);
		if(!$data->parent_idn_type_id){
			return [ 'success' => false, 'errors' => [$element_name." parent idn type invalid."]];
		}

		/*if(!validateIdn::check($data->student_idn)){
			return [ 'success' => false, 'errors' => [$element_name." student idn invalid."]];
		}*/
		$data->student_idn_type_id =  self::meta_id_by_key('id', 'idn_type', $data->student_idn_type);
		if(!$data->student_idn_type_id){
			return [ 'success' => false, 'errors' => [$element_name." student idn type invalid."]];
		}

		$data->student_genre_id =  self::meta_id_by_key('id', 'genre', $data->student_genre);
		if(!$data->student_genre_id){
			return [ 'success' => false, 'errors' => [$element_name." student genre invalid."]];
		}

		$data->parent_genre_id =  self::meta_id_by_key('id', 'genre', $data->parent_genre);
		if(!$data->parent_genre_id){
			return [ 'success' => false, 'errors' => [$element_name." parent genre invalid."]];
		}

		$data->parent_nationality_id =  self::meta_id_by_key('id', 'nationality', $data->parent_nationality);
		if(!$data->parent_nationality_id){
			return [ 'success' => false, 'errors' => [$element_name." parent nationality invalid."]];
		}


		$data->student_nationality_id =  self::meta_id_by_key('id', 'nationality', $data->student_nationality);
		if(!$data->student_nationality_id){
			return [ 'success' => false, 'errors' => [$element_name." student nationality invalid."]];
		}


		$data->relationship_type_id =  self::meta_id_by_key('id', 'relationship_type', $data->relationship_type);
		if(!$data->relationship_type_id){
			return [ 'success' => false, 'errors' => [$element_name." relationship invalid."]];
		}


		$data->level_class_id =  self::meta_id_by_key('id', 'level_class', $data->level_class, $options);
		if(!$data->level_class_id){
			return [ 'success' => false, 'errors' => [$element_name." level class invalid."]];
		}
		$options['level_class_id'] = $data->level_class_id;
		$data->school_class_id =  self::meta_id_by_key('id', 'school_class', $data->school_class, $options);
		if(!$data->school_class_id){
			return [ 'success' => false, 'errors' => [$element_name." school class invalid."]];
		}


		$data->student_level_class_id =  self::meta_id_by_key('id', 'level_class', $data->student_level_class, $options);
		if(!$data->student_level_class_id){
			return [ 'success' => false, 'errors' => [$element_name." student level class invalid."]];
		}

		$message = "Process ".$item."/".$total.': student_idn '.$data->student_idn;
		
		
		$status = 'inseted';

		$parent = ReaUser::select('id')->where('idn',$data->parent_idn)
		->where('idn_type_id',$data->parent_idn_type_id)
		->first();

		if($parent){
			$message .= " - Parent already exists";
			$data->parent_id = $parent->id;
			if(isset($options['overwrite']) AND $options['overwrite']){
				$message .= " (update)";
				$status = 'updated';
				$parent->active = 'Y';
				$parent->email = $data->parent_email;
				$parent->idn = $data->parent_idn;
				$parent->idn_type_id = $data->parent_idn_type_id;
				$parent->firstname = $data->parent_firstname;
				$parent->lastname = $data->parent_lastname;
				$parent->f_firstname = $data->parent_fatherfirstname;
				$parent->genre_id = $data->parent_genre_id;
				$parent->mobile = $data->parent_mobile;
				$parent->country_id = $data->parent_nationality_id;
				$parent->save();
			}else{
				$status = 'skipped';
			}
		}else{
			$parent = new ReaUser;
			$parent->active = 'Y';
			$parent->email = $data->parent_email;
			$parent->idn = $data->parent_idn;
			$parent->idn_type_id = $data->parent_idn_type_id;
			$parent->firstname = $data->parent_firstname;
			$parent->lastname = $data->parent_lastname;
			$parent->f_firstname = $data->parent_fatherfirstname;
			$parent->genre_id = $data->parent_genre_id;
			$parent->mobile = $data->parent_mobile;
			$parent->country_id = $data->parent_nationality_id;
			$parent->save();
			DB::table('module_parent_user')->insert(
                ['rea_user_id' => $parent->id, 'module_id' => config('parent_user_module_id',44)]
            );
			$data->parent_id = $parent->id;
		}






		$student = Student::select('id')->where('idn',$data->student_idn)->where('idn_type_id',$data->student_idn_type_id)->first();
		if($student){
			$message .= " - student already exists";
			$data->student_id = $student->id;
			if(isset($options['overwrite']) AND $options['overwrite']){
				$status = 'updated';
				$message .= " (update)";
				$student->active = 'Y';
				$student->idn = $data->student_idn;
				$student->idn_type_id = $data->student_idn_type_id;
				$student->firstname = $data->student_firstname;
				$student->lastname = $data->student_lastname;
				$student->f_firstname = $data->student_fatherfirstname;
				$student->genre_id = $data->student_genre_id;
				$student->country_id = $data->student_nationality_id;
				$student->current_level_class_id = $data->student_level_class_id;
				$student->birth_date = $data->student_birthday;

				if( $parent->genre_id==1 AND $data->relationship_type_id==1){
					$student->father_rea_user_id = $parent->id;
				}elseif( $parent->genre_id==2 AND $data->relationship_type_id==1){
					$student->mother_rea_user_id = $parent->id;
				}
				if($data->relationship_type_id==2){
					$student->resp1_rea_user_id = $parent->id;
				}
				$student->save();
			}else{
				$status = 'skipped';
			}
		}else{
			$student = new Student;
			$student->idn = $data->student_idn;
			$student->idn_type_id = $data->student_idn_type_id;
			$student->firstname = $data->student_firstname;
			$student->lastname = $data->student_lastname;
			$student->f_firstname = $data->student_fatherfirstname;
			$student->genre_id = $data->student_genre_id;
			$student->country_id = $data->student_nationality_id;
			$student->current_level_class_id = $data->student_level_class_id;
			$student->birth_date = $data->student_birthday;

			if( $parent->genre_id==1 AND $data->relationship_type_id==1){
				$student->father_rea_user_id = $parent->id;
			}elseif( $parent->genre_id==2 AND $data->relationship_type_id==1){
				$student->mother_rea_user_id = $parent->id;
			}
			if($data->relationship_type_id==2){
				$student->resp1_rea_user_id = $parent->id;
			}
			$student->save();
			$data->student_id = $student->id;
		}



		$family_relation = FamilyRelation::select('id')->where('student_id',$student->id)->where('resp_rea_user_id',$parent->id)->first();
		if($family_relation){
			$message .= " - family relation already exists";
			$data->family_relation_id = $family_relation->id;
			if(isset($options['overwrite']) AND $options['overwrite']){
				$status = 'updated';
				$message .= " (update)";
				$family_relation->active = 'Y';
				$family_relation->resp_rea_user_id = $data->parent_id;
				$family_relation->student_id = $data->student_id;
				$family_relation->relship_id = $data->relationship_type_id;
				$family_relation->resp_relationship = $data->family_relation_description;
				$family_relation->save();
			}else{
				$status = 'skipped';
			}
		}else{
			$family_relation = new FamilyRelation;
			$family_relation->active = 'Y';
			$family_relation->active_account = 'N';
			$family_relation->resp_rea_user_id = $data->parent_id;
			$family_relation->student_id = $data->student_id;
			$family_relation->relship_id = $data->relationship_type_id;
			$family_relation->resp_relationship = $data->family_relation_description;
			$family_relation->save();
			$data->family_relation_id = $family_relation->id;
		}



		$student_file = StudentFile::select('id')->where('student_id',$student->id)->where('school_id',$options['school_id'])->where('year',$options['hyear'])->first();
		if($student_file){
			$message .= " - student file already exists";
			$data->student_file_id = $student_file->id;
			if(isset($options['overwrite']) AND $options['overwrite']){
				$status = 'updated';
				$message .= " (update)";
				$student_file->active = 'Y';
				$student_file->level_class_id = $data->level_class_id;
				$student_file->school_class_id = $data->school_class_id;
				$student_file->symbol = $data->school_class;
				$student_file->student_file_status_id = 3;
				$student_file->save();
			}else{
				$status = 'skipped';
			}
		}else{
			$student_file = new StudentFile;
			$student_file->active = 'Y';
			$student_file->student_id = $data->student_id;
			$student_file->school_id = $options['school_id'];
			$student_file->year = $options['hyear'];
			$student_file->level_class_id = $data->level_class_id;

			if($options['maintain_students']){
				$student_file->school_class_id = $data->school_class_id;
				$student_file->symbol = $data->school_class;
			}else{
				$first_school_class_free = self::first_school_class_free($data->level_class_id, $options['school_year_id']);
				if($first_school_class_free){
					$student_file->school_class_id = $first_school_class_free->school_class_id;
					$student_file->symbol = $first_school_class_free->symbol;
				}else{
					return [ 'success' => false, 'errors' => [$element_name." no school class free for this student."]];
				}
			}
			$next_student_number = self::next_student_number((int)$student_file->school_class_id, $options['school_year_id']);
			$student_file->student_num = $next_student_number;
			$student_file->student_place = $next_student_number;
			$student_file->student_file_status_id = 3;
			$student_file->save();
			$data->student_file_id = $student_file->id;
		}

		return [
			'success' => true,
			'message' => $message,
			'item' => $data,
			'status' => $status,
		];
	}



	

	public static function first_school_class_free($level_class_id, $school_year_id)
	{

		$result = DB::select("SELECT tplaces.*
		FROM
		( SELECT sc.id AS school_class_id,
            sc.symbol,
            r.capacity,
            COUNT(sf.id) AS reserved
		   FROM school_class AS sc
		   LEFT JOIN room AS r ON r.id = sc.room_id
		   LEFT JOIN student_file AS sf ON sf.school_class_id = sc.id AND sf.active='Y'
		   WHERE sc.level_class_id = :level_class_id
		   AND sc.school_year_id = :school_year_id
		   GROUP BY sc.id
		) tplaces
		WHERE tplaces.capacity > tplaces.reserved
		ORDER BY tplaces.symbol ASC
		LIMIT 1", ['school_year_id' => $school_year_id, 'level_class_id' => $level_class_id]);
		if(isset($result[0])) return $result[0];

		return null;
	}


	public static function next_student_number($school_class_id, $school_year_id)
	{	

		$result = DB::select("SELECT MAX(sf.student_num)+1 as next_number
		   FROM student_file AS sf
		   LEFT JOIN school_year AS sy ON sy.year = sf.year
		   WHERE sf.school_class_id = :school_class_id AND sy.id = :school_year_id", ['school_year_id' => $school_year_id, 'school_class_id' => $school_class_id]);
		if(isset($result[0]) AND $result[0]->next_number) return $result[0]->next_number;

		return 1;
	}


	public static function meta_id_by_key($meta_key, $table, $meta_value, $options = [])
	{


		if(get_cache($table.'_'.$meta_key.'_'.$meta_value)) return get_cache($table.'_'.$meta_key.'_'.$meta_value);

		switch ($table.'_'.$meta_key) {
			case 'idn_type_id':
					$result = DB::select('select idn_type.id from idn_type
						WHERE idn_type.id = :p1 OR idn_type.idn_type_name_ar LIKE :p2 OR idn_type.idn_type_name_en LIKE :p3 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value]);
					if(isset($result[0]->id)){
						set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
						return $result[0]->id;
					}
				break;


			case 'genre_id':
				$result = DB::select('select genre.id from genre
					WHERE genre.id = :p1 OR genre.genre_name_ar LIKE :p2 OR genre.genre_name_en LIKE :p3 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value]);
				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;


			case 'nationality_id':
				$result = DB::select('select country.id from country
					WHERE country.id = :p1 OR country.country_name_ar LIKE :p2 OR country.country_name_en LIKE :p3 OR country.nationalty_name_ar LIKE :p4 OR country.nationalty_name_en LIKE :p5 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value, 'p4' => $meta_value, 'p5' => $meta_value]);
				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;


			case 'city_id':

				$result = DB::select('select city.id from city
					WHERE city.id = :p1 OR city.city_name_ar LIKE :p2 OR city.city_name_en LIKE :p3 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value]);

				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;

			case 'school_job_id':
				$result = DB::select('select school_job.id from school_job
					WHERE school_job.id = :p1 OR school_job.school_job_name_ar LIKE :p2 OR school_job.school_job_name_en LIKE :p3 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value]);

				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;

			case 'wday_id':
				$result = DB::select('select wday.id from wday
					WHERE wday.id = :p1 OR wday.wday_name_ar LIKE :p2 OR wday.wday_name_en LIKE :p3 OR wday.lookup_code LIKE :p4 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value, 'p4' => $meta_value]);

				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;

			case 'course_id':
				$result = DB::select('select course.id from course
					WHERE course.id = :p1 OR course.course_name_ar LIKE :p2 OR course.course_name_en LIKE :p3 OR course.lookup_code LIKE :p4 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value, 'p4' => $meta_value]);

				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;


			case 'relationship_type_id':
				$result = DB::select('select relship.id from relship
					WHERE relship.id = :p1 OR relship.lookup_code LIKE :p2 OR relship.relship_name_ar LIKE :p3 OR relship.relship_name_en LIKE :p4 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value, 'p4' => $meta_value]);
				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;


			case 'level_class_id':
				$result = DB::select('select level_class.id from level_class
					WHERE level_class.lookup_code LIKE :p1 OR level_class.level_class_name_ar LIKE :p2 OR level_class.level_class_name_en LIKE :p3 OR level_class.level_class_order LIKE :p4 LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value, 'p4' => $meta_value]);
				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;

			case 'school_class_id':
				$meta_value_strtolower = strtolower($meta_value);
				$new_meta_value = $meta_value_strtolower;
				$arabic_letters = [
					'أ' => 'a',
					'ب' => 'b',
					'ت' => 'c',
					'ث' => 'd',
					'ج' => 'e',
					'ح' => 'f',
					'خ' => 'g',
					'د' => 'h',
					'ذ' => 'i',
				];
				if(isset($arabic_letters[$new_meta_value])) $new_meta_value = $arabic_letters[$new_meta_value];

				$result = DB::select('select school_class.id from school_class
					WHERE (school_class.symbol LIKE :p1 OR school_class.symbol LIKE :p4) AND school_class.school_year_id = :p2 AND school_class.level_class_id = :p3 LIMIT 1', ['p1' => $new_meta_value, 'p2' => $options['school_year_id'] , 'p3' => $options['level_class_id'], 'p4' => $meta_value_strtolower ]);

				if(isset($result[0]->id)){
					set_cache($table.'_'.$meta_key.'_'.$meta_value,$result[0]->id);
					return $result[0]->id;
				}
			break;
			
			default:
				return null;
				break;
		}

		return null;
	}





	public static function get_sdepartment_id($meta_value, $school_id){
		if(get_cache('sdepartment_id_'.$meta_value)) return get_cache('sdepartment_id_'.$meta_value);

		$result = DB::select('select sdepartment.id from sdepartment
			WHERE (sdepartment.id = :p1
			OR sdepartment.sdepartment_name_ar LIKE :p2
			OR sdepartment.sdepartment_name_en LIKE :p3)
			AND sdepartment.school_id = :p4
			LIMIT 1', ['p1' => $meta_value, 'p2' => $meta_value, 'p3' => $meta_value,
			'p4' => $school_id]);

		if(isset($result[0]->id)){
			set_cache('sdepartment_id_'.$meta_value,$result[0]->id);
			return $result[0]->id;
		}
	}



	public static function import_school_employees($datas, $item, $options)
	{
		$return = [];

		if( !ReaUserFactory::get_current_school_id() ){
			return [
				'success' => false,
				'errors' => ["SchoolEmployee.SchoolNotSelected"],
			];
		}

		$current_school_id = ReaUserFactory::get_current_school_id();

		$options['school_id'] = $current_school_id;

		if( !isset($datas[$item-1]) ){
			return [
				'success' => false,
				'errors' => ["Form.EmptyData"],
			];
		}

		$total = count($datas);
		$data = (object) $datas[$item-1];

		$element_name = (isset($data->idn) AND !empty($data->idn))? "School employee {$data->idn} :"  : "Element Number {$item}:" ;
		if( !isset($data->idn) OR empty($data->idn) ){
			return [ 'success' => false, 'errors' => [$element_name." idn not exists."]];
		}
		if( !isset($data->idn_type) OR empty($data->idn_type) ){
			return [ 'success' => false, 'errors' => [$element_name." idn type not exists."]];
		}
		if( !isset($data->genre) OR empty($data->genre) ){
			return [ 'success' => false, 'errors' => [$element_name." genre not exists."]];
		}
		if( !isset($data->nationality) OR empty($data->nationality) ){
			return [ 'success' => false, 'errors' => [$element_name." nationality not exists."]];
		}
		if( !isset($data->mobile) OR empty($data->mobile) ){
			return [ 'success' => false, 'errors' => [$element_name." mobile not exists."]];
		}
		if( !isset($data->firstname) OR empty($data->firstname) ){
			return [ 'success' => false, 'errors' => [$element_name." first name not exists."]];
		}
		if( !isset($data->lastname) OR empty($data->lastname) ){
			return [ 'success' => false, 'errors' => [$element_name." last name not exists."]];
		}
		if( !isset($data->fatherfirstname) OR empty($data->fatherfirstname) ){
			return [ 'success' => false, 'errors' => [$element_name." father name not exists."]];
		}
		if( !isset($data->grandfathername) OR empty($data->grandfathername) ){
			return [ 'success' => false, 'errors' => [$element_name." grand father name not exists."]];
		}
		if( !isset($data->birthday) OR empty($data->birthday) ){
			return [ 'success' => false, 'errors' => [$element_name." birthday not exists."]];
		}
		if( isset($data->email) AND !empty($data->email) AND !filter_var($data->email, FILTER_VALIDATE_EMAIL) ){
			return [ 'success' => false, 'errors' => [$element_name." email invalid."]];
		}

		if( !isset($data->days) OR empty($data->days) ){
			return [ 'success' => false, 'errors' => [$element_name." week days invalid."]];
		}

		
		if(!validateIdn::check($data->idn)){
			return [ 'success' => false, 'errors' => [$element_name." idn invalid."]];
		}
		$data->idn_type_id =  self::meta_id_by_key('id', 'idn_type', $data->idn_type);
		if(!$data->idn_type_id){
			return [ 'success' => false, 'errors' => [$element_name." idn type invalid."]];
		}
		


		$data->genre_id =  self::meta_id_by_key('id', 'genre', $data->genre);
		if(!$data->genre_id){
			return [ 'success' => false, 'errors' => [$element_name." genre invalid."]];
		}

		

		$data->nationality_id =  self::meta_id_by_key('id', 'nationality', $data->nationality);
		if(!$data->nationality_id){
			return [ 'success' => false, 'errors' => [$element_name." nationality invalid."]];
		}

		$data->city_id =  self::meta_id_by_key('id', 'city', $data->city);


		if(!$data->city_id){
			return [ 'success' => false, 'errors' => [$element_name." city invalid."]];
		}


		
		$jobs = explode(',', $data->job );
		$data->jobs = [];
		foreach($jobs as $item_job){
			$item_job = trim($item_job);
			$data->jobs[] =  self::meta_id_by_key('id', 'school_job', $item_job);
		}
		$wdays = explode(',', $data->days );
		$data->wdays = [];
		foreach($wdays as $wday){
			$wday = trim($wday);
			$data->wdays[] =  self::meta_id_by_key('id', 'wday', $wday);
		}
		$courses = explode(',', $data->courses );
		$data->courses = [];
		foreach($courses as $item_course){
			$item_course = trim($item_course);
			$data->courses[] =  self::meta_id_by_key('id', 'course', $item_course);
		}

		$data->sdepartment_id = null;
		$data->sdepartment_2_id = null;
		$data->sdepartment_3_id = null;
		if( isset($data->morning_department) ){
			$data->sdepartment_id =  self::get_sdepartment_id($data->morning_department, $options['school_id']);
		}
		if(isset($data->evening_department)){
			$data->sdepartment_2_id =  self::get_sdepartment_id($data->evening_department, $options['school_id']);
		}
		if(isset($data->night_department)){
			$data->sdepartment_3_id =  self::get_sdepartment_id($data->night_department, $options['school_id']);
		}


		if(
			!isset($data->sdepartment_id)
			AND !isset($data->sdepartment_2_id)
			AND !isset($data->sdepartment_3_id)
			AND !($data->sdepartment_id)
			AND !($data->sdepartment_2_id)
			AND !($data->sdepartment_3_id)
		 ){
			return [
				'success' => false,
				'errors' => ["SchoolEmployee.SchoolDepartmentNotSelected"],
			];
		}



		$message = "Process ".$item."/".$total.': ';

		$status = 'inseted';
		
		$school_employee_rea_user = ReaUser::select('rea_user.id')
		->where('rea_user.idn',$data->idn)
		->where('rea_user.idn_type_id',$data->idn_type_id)
		->first();	
		

		if($school_employee_rea_user){
			$data->school_employee_rea_user_id = $school_employee_rea_user->id;

			$message .= " - School employee User already exists";
			if(isset($options['overwrite']) AND $options['overwrite']){
				$message .= " (update)";
				$status = 'updated';
				$school_employee_rea_user->email = $data->email;
				$school_employee_rea_user->firstname = $data->firstname;
				$school_employee_rea_user->lastname = $data->lastname;
				$school_employee_rea_user->f_firstname = $data->fatherfirstname;
				$school_employee_rea_user->genre_id = $data->genre_id;
				$school_employee_rea_user->mobile = $data->mobile;
				$school_employee_rea_user->country_id = $data->nationality_id;
				$school_employee_rea_user->city_id = $data->city_id;
				$school_employee_rea_user->save();
			}else{
				$status = 'skipped';
			}
		}else{
			$message .= " - New School employee User";
			$school_employee_rea_user = new ReaUser;
			$school_employee_rea_user->active = 'Y';
			$school_employee_rea_user->email = $data->email;
			$school_employee_rea_user->idn = $data->idn;
			$school_employee_rea_user->idn_type_id = $data->idn_type_id;
			$school_employee_rea_user->firstname = $data->firstname;
			$school_employee_rea_user->lastname = $data->lastname;
			$school_employee_rea_user->f_firstname = $data->fatherfirstname;
			$school_employee_rea_user->genre_id = $data->genre_id;
			$school_employee_rea_user->mobile = $data->mobile;
			$school_employee_rea_user->country_id = $data->nationality_id;
			$school_employee_rea_user->city_id = $data->city_id;
			$school_employee_rea_user->address = $data->address;
			$school_employee_rea_user->save();
			\DB::table('module_rea_user')->insert(
                [ 'active' => 'Y' ,'rea_user_id' => $school_employee_rea_user->id, 'module_id' => config('lookup.rea_user_module_id')]
            );
			$data->school_employee_rea_user_id = $school_employee_rea_user->id;
		}

		$school_employee = SchoolEmployee::select('school_employee.id')
		->where('school_employee.rea_user_id',$data->school_employee_rea_user_id)
		->first();

		if($school_employee){
			$message .= " - School employee profile already exists";
			if(isset($options['overwrite']) AND $options['overwrite']){
				$message .= " (update)";
				$status = 'updated';



				$school = School::select('school.stakeholder_id')->where('school.id',$options['school_id'])->first();
				if(!$school OR !$school->stakeholder_id){
					return [ 'success' => false, 'errors' => ["School.SchoolDontHaveStakeHolder"]];
				}

				$sdepartment_stakeholder_id = null;
				$sdepartment_2_stakeholder_id = null;
				$sdepartment_3_stakeholder_id = null;
				if(isset($data->sdepartment_id) AND $data->sdepartment_id){
					$sdepartment = Sdepartment::select('sdepartment.stakeholder_id')
					->where('sdepartment.id',$data->sdepartment_id)->first();
					if(!$sdepartment OR !$sdepartment->stakeholder_id ){
						return [ 'success' => false, 'errors' => ["Sdepartment.SchoolDepartmentDontHaveStakeHolder"]];
					}
					$sdepartment_stakeholder_id = $sdepartment->stakeholder_id;
				}
				if(isset($data->sdepartment_2_id) AND $data->sdepartment_2_id){
					$sdepartment_2 = Sdepartment::select('sdepartment.stakeholder_id')
					->where('sdepartment.id',$data->sdepartment_2_id)->first();
					if(!$sdepartment_2 OR !$sdepartment_2->stakeholder_id ){
						return [ 'success' => false, 'errors' => ["Sdepartment.SchoolDepartmentDontHaveStakeHolder"]];
					}
					$sdepartment_2_stakeholder_id = $sdepartment_2->stakeholder_id;
				}
				if(isset($data->sdepartment_3_id) AND $data->sdepartment_3_id){
					$sdepartment_3 = Sdepartment::select('sdepartment.stakeholder_id')
					->where('sdepartment.id',$data->sdepartment_3_id)->first();
					if(!$sdepartment_3 OR !$sdepartment_3->stakeholder_id ){
						return [ 'success' => false, 'errors' => ["Sdepartment.SchoolDepartmentDontHaveStakeHolder"]];
					}
					$sdepartment_3_stakeholder_id = $sdepartment_3->stakeholder_id;
				}
				
				$employee = Employee::find($school_employee->id);
				$employee->active = 'Y';
				$employee->email = $data->email;
				$employee->firstname = $data->firstname;
				$employee->lastname = $data->lastname;
				$employee->f_firstname = $data->fatherfirstname;
				$employee->g_f_firstname = $data->grandfathername;
				$employee->mobile = $data->mobile;
				$employee->phone = $data->phone;
				$employee->address = $data->address;
				$employee->gender_id = $data->genre_id;
				$employee->country_id = $data->nationality_id;
				$employee->city_id = $data->city_id;
				$employee->auser_id = $data->school_employee_rea_user_id;
				$employee->birth_date = $data->birthday;
				$employee->job = $data->job_description;
				$employee->id_sh_org = $school->stakeholder_id;
				$employee->id_sh_div = $sdepartment_stakeholder_id;
				$employee->id_sh_div2 = $sdepartment_2_stakeholder_id;
				$employee->id_sh_div3 = $sdepartment_3_stakeholder_id;

				if(isset($data->jobs) AND !empty($data->jobs)){
					$employee->jobrole_mfk = MfkHelper::mfkEncode($data->jobs);
				}
				$employee->save();

				$sprof = SProf::find($employee->id);
				if(!$sprof){
					$sprof = new SProf;
					$sprof->id = $employee->id;
					$sprof->active = 'Y';
				}
				$sprof->course_mfk = MfkHelper::mfkEncode($data->courses);
				$sprof->wday_mfk = MfkHelper::mfkEncode($data->wdays);
				$sprof->save();
			}else{
				$status = 'skipped';
			}
		}else{
			$message .= " - New School employee";

			$school = School::select('school.stakeholder_id')->where('school.id',$options['school_id'])->first();
			if(!$school OR !$school->stakeholder_id){
				return [ 'success' => false, 'errors' => ["School.SchoolDontHaveStakeHolder"]];
			}

			$sdepartment_stakeholder_id = null;
			$sdepartment_2_stakeholder_id = null;
			$sdepartment_3_stakeholder_id = null;
			if(isset($data->sdepartment_id) AND $data->sdepartment_id){
				$sdepartment = Sdepartment::select('sdepartment.stakeholder_id')
				->where('sdepartment.id',$data->sdepartment_id)->first();
				if(!$sdepartment OR !$sdepartment->stakeholder_id ){
					return [ 'success' => false, 'errors' => ["Sdepartment.SchoolDepartmentDontHaveStakeHolder"]];
				}
				$sdepartment_stakeholder_id = $sdepartment->stakeholder_id;
			}
			if(isset($data->sdepartment_2_id) AND $data->sdepartment_2_id){
				$sdepartment_2 = Sdepartment::select('sdepartment.stakeholder_id')
				->where('sdepartment.id',$data->sdepartment_2_id)->first();
				if(!$sdepartment_2 OR !$sdepartment_2->stakeholder_id ){
					return [ 'success' => false, 'errors' => ["Sdepartment.SchoolDepartmentDontHaveStakeHolder"]];
				}
				$sdepartment_2_stakeholder_id = $sdepartment_2->stakeholder_id;
			}
			if(isset($data->sdepartment_3_id) AND $data->sdepartment_3_id){
				$sdepartment_3 = Sdepartment::select('sdepartment.stakeholder_id')
				->where('sdepartment.id',$data->sdepartment_3_id)->first();
				if(!$sdepartment_3 OR !$sdepartment_3->stakeholder_id ){
					return [ 'success' => false, 'errors' => ["Sdepartment.SchoolDepartmentDontHaveStakeHolder"]];
				}
				$sdepartment_3_stakeholder_id = $sdepartment_3->stakeholder_id;
			}


			$employee = new Employee;
			$employee->active = 'Y';
			$employee->email = $data->email;
			$employee->firstname = $data->firstname;
			$employee->lastname = $data->lastname;
			$employee->f_firstname = $data->fatherfirstname;
			$employee->g_f_firstname = $data->grandfathername;
			$employee->mobile = $data->mobile;
			$employee->phone = $data->phone;
			$employee->address = $data->address;
			$employee->gender_id = $data->genre_id;
			$employee->country_id = $data->nationality_id;
			$employee->city_id = $data->city_id;
			$employee->auser_id = $data->school_employee_rea_user_id;
			$employee->birth_date = $data->birthday;
			$employee->job = $data->job_description;
			$employee->id_sh_org = $school->stakeholder_id;
			$employee->id_sh_div = $sdepartment_stakeholder_id;
			$employee->id_sh_div2 = $sdepartment_2_stakeholder_id;
			$employee->id_sh_div3 = $sdepartment_3_stakeholder_id;

			if(isset($data->jobs) AND !empty($data->jobs)){
				$employee->jobrole_mfk = MfkHelper::mfkEncode($data->jobs);
			}
			$employee->save();

			$sprof = SProf::find($employee->id);
			if(!$sprof){
				$sprof = new SProf;
				$sprof->id = $employee->id;
				$sprof->active = 'Y';
			}
			$sprof->course_mfk = MfkHelper::mfkEncode($data->courses);
			$sprof->wday_mfk = MfkHelper::mfkEncode($data->wdays);
			$sprof->save();
		}



		return [
			'success' => true,
			'message' => $message,
			'item' => $data,
			'status' => $status,
		];
	}


}



