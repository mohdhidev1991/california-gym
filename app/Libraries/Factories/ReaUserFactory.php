<?php namespace App\Libraries\Factories;

use App\Models\SchoolYear;
use Auth;
use App\Models\ReaUser;
use App\Models\Sdepartment;
use App\Models\SchoolEmployee;
use App\Models\School;


class ReaUserFactory extends BaseFactory
{
        function __construct() {
                parent::__construct();
        }


        public static function user() {
                return Auth::user();
        }


        public static function get_current_school_id()
        {
                
                if (Auth::user())
                {
                        $user = Auth::user();
                        if(get_cache('rea_user:'.$user->id)){
                                $rea_user = (array) get_cache('rea_user:'.$user->id);
                                if( isset($rea_user['current_school']->school_id) ){
                                        return (int)$rea_user['current_school']->school_id;
                                }
                        }
                        $rea_user = ReaUser::find($user->id);
                        if( isset($rea_user->last_selected_school) ){
                                return (int)$rea_user->last_selected_school;
                        }
                }

                return null;
        }


        public static function get_current_school_stacke_holder_id()
        {
                $school_id = self::get_current_school_id();
                return self::get_school_stacke_holder_id($school_id);
        }


        public static function get_school_stacke_holder_id($school_id)
        {
                $school_id = self::get_current_school_id();
                if(!$school_id) return null;
                if(get_cache('school_stacke_holder_id:'.$school_id)){
                        return get_cache('school_stacke_holder_id:'.$school_id);
                }
                $school = School::select('stakeholder_id')->where('id',$school_id)->first();
                if(!$school OR !$school->stakeholder_id) return null;
                set_cache('school_stacke_holder_id:'.$school_id, $school->stakeholder_id);
                return $school->stakeholder_id;
        }




        public static function get_current_sdepartment_id() {
                if (Auth::user())
                {
                        $user = Auth::user();
                        if(get_cache('rea_user:'.$user->id)){
                                $rea_user = (array) get_cache('rea_user:'.$user->id);
                                if( isset($rea_user['current_sdepartment']->sdepartment_id) ){
                                        return (int)$rea_user['current_sdepartment']->sdepartment_id;
                                }
                        }
                        $rea_user = ReaUser::find();
                        $school_id = self::get_current_school_id();
                        $my_sdepartments = Sdepartment::getSdepartmentsByUserIdAndSchoolId($user->id,(int)$school_id);
                        if( isset($my_sdepartments[0]->sdepartment_id) AND $my_sdepartments[0]->sdepartment_id)
                        return $my_sdepartments[0]->sdepartment_id;
                }

                return null;
        }



        public static function get_current_school_year_id($school_id = null)
        {
                $school_id  = ( isset($school_id) ) ? $school_id : self::get_current_school_id();
                if ($school_id)
                {
                        $user = Auth::user();
                        if(get_cache('rea_user:'.$user->id)){
                                $rea_user = (array) get_cache('rea_user:'.$user->id);
                                if( isset($rea_user['current_school']->school_year_id) ){
                                        return (int)$rea_user['current_school']->school_year_id;
                                }
                        }
                        $hdate = HijriDateHelper::getHdateByFormat(date('Ymd'),'hdate');
                        $school_year = SchoolYear::getSchoolYearBySchoolId($hdate,$school_id);
                        if( isset($school_year->id) ){
                                return (int)$school_year->id;
                        }
                }

                return null;
        }


        public static function get_current_school_year($school_id = null)
        {
                $school_id  = ( isset($school_id) ) ? $school_id : self::get_current_school_id();
                if ($school_id)
                {
                        $user = Auth::user();
                        if(get_cache('rea_user:'.$user->id)){
                                $rea_user = (array) get_cache('rea_user:'.$user->id);
                                if( isset($rea_user['current_school']->school_year) ){
                                        return (int)$rea_user['current_school']->school_year;
                                }
                        }
                        $hdate = HijriDateHelper::getHdateByFormat(date('Ymd'),'hdate');
                        $school_year = SchoolYear::getSchoolYearBySchoolId($hdate,$school_id);
                        if( isset($school_year->year) ){
                                return (int)$school_year->year;
                        }
                }

                return null;
        }

        public static function get_group_num()
        {
                
                if (Auth::user())
                {
                        $user = Auth::user();
                        if(get_cache('rea_user:'.$user->id)){
                                $rea_user = (array) get_cache('rea_user:'.$user->id);
                                if( isset($rea_user['current_school']->group_num) ){
                                        return (int)$rea_user['current_school']->group_num;
                                }
                        }

                        $school_id = self::get_current_school_id();
                        $school = \App\Models\School::select('group_num')->where('id',$school_id)->first();
                        if( isset($school->group_num) ){
                                return (int)$school->group_num;
                        }
                }
                return null;
        }


        public static function get_my_school_employee_id()
        {


                
                if (Auth::user())
                {
                        $user = Auth::user();
                        if(get_cache('rea_user:'.$user->id) AND 0){
                                $rea_user = (array) get_cache('rea_user:'.$user->id);
                                if( isset($rea_user['current_school']->school_employee_id) ){
                                        return (int)$rea_user['current_school']->school_employee_id;
                                }
                        }
                        $school_id = self::get_current_school_id();

                        return self::get_school_employee_id($user->id, $school_id);
                }
                return null;
        }





        public static function get_my_school_jobs_ids()
        {
                
                if (!Auth::user())
                {
                        return null;
                }

                $user = Auth::user();
                if(get_cache('rea_user:'.$user->id) AND 0){
                        $rea_user = (array) get_cache('rea_user:'.$user->id);
                        if( isset($rea_user['current_school']->school_jobs) AND !empty($rea_user['current_school']->school_jobs) ){
                                $jobs = [];
                                foreach ($rea_user['current_school']->school_jobs as $item_job) {
                                        $jobs[] = $item_job->id;
                                }
                                return $jobs;
                        }
                }
                $school_id = self::get_current_school_id();
                $jobs = self::get_rea_user_school_jobs_ids($user->id, $school_id);
                return $jobs;
        }

        public static function get_rea_user_school_jobs_ids($rea_user_id, $school_id = null)
        {
                if(!$school_id) $school_id = self::get_current_school_id();
                $school_employee_id = self::get_school_employee_id($rea_user_id, $school_id);
                $school_job_mfk = SchoolEmployee::select('school_job_mfk')
                ->where('rea_user_id',$rea_user_id)
                ->where('school_id',$school_id)->first();
                if(!isset($school_job_mfk->school_job_mfk) OR !$school_job_mfk->school_job_mfk) return null;
                $jobs = \App\Helpers\MfkHelper::mfkDecode($school_job_mfk->school_job_mfk);
                return $jobs;
        }


        public static function get_school_employee_id($rea_user_id=null, $school_id=null)
        {
                if(!$school_id) $school_id = self::get_current_school_id();
                if(!$rea_user_id) $rea_user_id = Auth::user()->id;

                $school_employee = \App\Models\SchoolEmployee::select('id')
                ->where('active','Y')
                ->where('school_id',$school_id)
                ->where('rea_user_id',$rea_user_id)->first();
                if( isset($school_employee->id) ){
                        return (int)$school_employee->id;
                }
                return null;
        }


}
