<?php namespace App\Libraries\Factories;



class TranslateFactory extends BaseFactory
{
    
	public static function translate($module, $string, $params = [], $lang = null){

		if(!$lang){
			\Lang::setLocale('ar');
			$lang = \Lang::locale('ar');
		}
		$translates_ = get_cache('translates');

		

		$translates = [];
		if(is_object($translates)){
			objToArray($translates_, $translates);
		}

		if(!$translates OR !isset($translates[$module][$string][$lang])){

			if(!$translates) $translates = [];
			$sql = "
			SELECT
			  tm.lookup_code AS module,
			  t.meta_key AS string,
			  t.meta_value AS value,
			  l.lookup_code AS lang
			FROM
			  translate t
			JOIN
			  translate_module tm ON tm.id = t.translate_module_id AND tm.lookup_code = :module
			JOIN
			  lang l ON l.id = t.lang_id AND l.lookup_code = :lang
			WHERE
			  1 AND t.meta_key = :string
			ORDER BY
			  tm.lookup_code,
			  t.meta_key,
			  t.meta_value,
			  l.lookup_code
			";
			$result = \DB::select($sql, ['module' => $module, 'lang'=>$lang, 'string'=>$string]);

			if(!isset($result[0]) OR !isset($result[0]->value)) return $module.'.'.$string;

			$translates[$module][$string][$lang] = $result[0]->value;
			set_cache('translates',$translates);
			$value = $result[0]->value;
		}else{
			$value = $translates[$module][$string][$lang];
		}
		if(!$value) $value = $module.'.'.$string;
		$value = self::translate_params($value, $params);
		return $value;
	}

	public static function translate_params($value, $params = []){
		if(empty($params)) return $value;
		foreach ($params as $key => $param) {
			$value = str_replace('{{'.$key.'}}', $param, $value);
		}
		return $value;
	}
	
}
