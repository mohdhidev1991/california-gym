<?php

namespace App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Controller;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Closure;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Logger;

class AppBaseController extends Controller
{
    protected $user;
    protected $rea_user;
    public function __construct()
    {
        $this->user = Auth::user();
        $this->rea_user = null;
        if(isset($this->user->id)){
            $this->rea_user = get_cache('rea_user:'.$this->user->id);    
        }
        Logger::http($this->user);
        Logger::queries($this->user);
    }
    /**
     * Validate request for current resource.
     *
     * @param Request $request
     * @param array   $rules
     * @param array   $messages
     * @param array   $customAttributes
     */
    public function validateRequestOrFail($request, array $rules, $messages = [], $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            throw new HttpException(400, json_encode($validator->errors()->getMessages()));
        }
    }

    public function makeResponse($result, $message, $success,$total, $attributes)
    {
        return [
            'status'  => $success,
            'message' => $message,
            'total' => $total,
            'data'    => $result,
            'attributes'    => $attributes,
        ];
    }

    public function sendResponse($result, $message,$success=true, $total=null, $attributes = null)
    {
        return Response::json($this->makeResponse($result, $message, $success, $total, $attributes));
    }
}