<?php

namespace App\Libraries\Reaaya\mitulgolakiya\src\Mitul\Generator\Generators;

interface GeneratorProvider
{
    public function generate();
}
