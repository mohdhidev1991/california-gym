<?php namespace App\Libraries\Repositories;

use App\Models\CrmErequest;
use App\Models\CrmErequestFup;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CrmErequestRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\Models\CrmErequest';
    }

	public function search($input)
    {
        $query = CrmErequest::query();

        $columns = Schema::getColumnListing('crmErequests');
        $attributes = array();

        foreach($columns as $attribute)
        {
            if(isset($input[$attribute]) and !empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] = $input[$attribute];
            }
            else
            {
                $attributes[$attribute] =  null;
            }
        }

        return [$query->get(), $attributes];
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "CrmErequest not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "CrmErequest not found");
        }

        return $model->delete();
    }


    public function count_fup_not_readed_by_erequest_id($erequest_id)
    {
        return CrmErequestFup::select(\DB::raw("count(crm_erequest_fup.id) as count"))
        ->where('crm_erequest_fup.crm_erequest_id',$erequest_id)
        ->where('crm_erequest_fup.is_read','N')
        ->where('crm_erequest_fup.active','Y')
        ->groupBy('crm_erequest_fup.crm_erequest_id')
        ->count();
    }



    public function count_fup_not_owner_not_readed_by_erequest_id($erequest_id,$user_id)
    {
        return CrmErequestFup::select(\DB::raw("count(crm_erequest_fup.id) as count"))
        ->where('crm_erequest_fup.crm_erequest_id',$erequest_id)
        ->where('crm_erequest_fup.is_read','N')
        ->where('crm_erequest_fup.active','Y')
        ->where('crm_erequest_fup.owner_id','<>',$user_id)
        ->groupBy('crm_erequest_fup.crm_erequest_id')
        ->count();
    }



    public function count_fup_by_erequest_id($erequest_id)
    {
        return CrmErequestFup::select(\DB::raw("count(crm_erequest_fup.id) as count"))
        ->where('crm_erequest_fup.crm_erequest_id',$erequest_id)
        ->where('crm_erequest_fup.active','Y')
        ->groupBy('crm_erequest_fup.crm_erequest_id')
        ->count();
    }
}
