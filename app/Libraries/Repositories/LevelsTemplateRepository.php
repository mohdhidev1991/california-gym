<?php namespace App\Libraries\Repositories;

use App\Models\LevelsTemplate;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LevelsTemplateRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\Models\LevelsTemplate';
    }

}
