<?php namespace App\Libraries\Repositories;

use App\Libraries\Factories\AppFactory;
use App\Models\ReaUser;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Mail;
use Carbon\Carbon;
use Curl\Curl;
use Illuminate\Support\Facades\Lang;
use App\Models\Language;
use App\Models\School;
use App\Models\SchoolJob;
use App\Models\SchoolYear;
use App\Models\Sdepartment;
use App\Helpers\MfkHelper;
use App\Helpers\HijriDateHelper;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;


class ReaUserRepository extends Repository
{

    
    public function model()
    {
      return 'App\Models\ReaUser';
    }

    public function search($input)
    {
        $query = ReaUser::query();

        $columns = Schema::getColumnListing('reaUsers');
        $attributes = array();

        foreach($columns as $attribute)
        {
            if(isset($input[$attribute]) and !empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] = $input[$attribute];
            }
            else
            {
                $attributes[$attribute] =  null;
            }
        }

        return [$query->get(), $attributes];
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "ReaUser not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "ReaUser not found");
        }

        return $model->delete();
    }



    public function authentification_data($rea_user = null, $lang_short_name = null)
    {
        if(!$rea_user){
            $rea_user = ReaUser::find($rea_user->id);
        }

        if( isset($rea_user->id) AND get_cache('rea_user:'.$rea_user->id) ){
            return (array) get_cache('rea_user:'.$rea_user->id);
        }
    
        $lang_demanded = Language::getByLookupCode($lang_short_name);
        
        $data_rea_user =  [
            'id'     => $rea_user->id,
            'firstname'     => $rea_user->firstname,
            'lastname'      => $rea_user->lastname,
            'email'         => $rea_user->email,
            'club'         => $rea_user->club,
            'permissions'   => $this->get_permisssions($rea_user),
        ];

        set_cache('rea_user:'.$rea_user->id, $data_rea_user);
        return $data_rea_user;
    }



    public function get_permisssions($rea_user){
        
        $permissions = DB::select(DB::raw("
          SELECT p.lookup_code
        FROM permission as p
        JOIN (SELECT r.* FROM role as r
        JOIN user as u on (u.roles_mfk LIKE CONCAT('%,',r.id,',%')) AND u.id = :uid_1
        WHERE u.id = :uid_2) as t_role on (t_role.permissions_mfk LIKE CONCAT('%,',p.id,',%'))
        GROUP BY p.id
          "),[':uid_1' => $rea_user->id, ':uid_2' => $rea_user->id]);

        $permissions = array_map(create_function('$o', 'return $o->lookup_code;'), $permissions);
	
        return $permissions;
    }


    public function send_activation_email($rea_user_id)
    {
        $rea_user = ReaUser::find($rea_user_id);

        $rea_user->email_token =  str_replace( '/', '', bcrypt(uniqid()) );
        $Carbon = Carbon::now();
        $rea_user->email_token_expiry_date = $Carbon->addDays(2)->format('Y-m-d H:i:s');
        $rea_user->save();

        Mail::send('emails.active_email', ['rea_user' => $rea_user], function ($message) use ($rea_user) {
            $message->from( env('site_email','admin@yklik.com'), env('site_name','Reaaya') );
            $message->to($rea_user->email, $rea_user->firstname)->subject("Please active your email");
        });

    }


    public function send_remember_password($rea_user_id)
    {
        $rea_user = ReaUser::find($rea_user_id);

        $rea_user->remember_token =  str_replace( '/', '', bcrypt(uniqid()) );
        $Carbon = Carbon::now();
        $rea_user->remember_token_expiry_date = $Carbon->addDays(2)->format('Y-m-d H:i:s');
        $rea_user->save();

        Mail::send('emails.remember_password', ['rea_user' => $rea_user], function ($message) use ($rea_user) {
            $message->from( env('site_email','admin@yklik.com'), env('site_name','Reaaya') );
            $message->to($rea_user->email, $rea_user->firstname)->subject(Lang::get('rea_user.emails.remember_password'));
        });

    }



    public function send_activation_mobile($rea_user_id)
    {
        $rea_user = ReaUser::find($rea_user_id);

        $key = config('services.sinch.key');
        $secret = config('services.sinch.secret');
        $key_secret_api_sms = "application\\" . $key . ":" . $secret;
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setopt(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setopt(CURLOPT_RETURNTRANSFER, true);
        $curl->setopt(CURLOPT_USERPWD, $key_secret_api_sms);
        $curl->setopt(CURLOPT_POSTFIELDS, true);
        $data_to_send = array(
            "identity" => [ "type" => "number", "endpoint" => $rea_user->mobile ],
            "method" => "sms",
        );
        $curl->post('https://api.sinch.com/verification/v1/verifications', json_encode($data_to_send));
        $curl->close();

        if ($curl->error) {
            return false;
        }
        else {
            $response = json_decode($curl->response);
            if(isset($response->id)){
                $rea_user->mobile_activation_id = $response->id;
                $rea_user->save();
            }
            return true;
        }
    }



    public function verifications_mobile_code($rea_user_id, $code)
    {
        $rea_user = ReaUser::find($rea_user_id);

        $key = config('services.sinch.key');
        $secret = config('services.sinch.secret');
        $key_secret_api_sms = "application\\" . $key . ":" . $secret;


        $data_to_send = array(
            'sms' => ["code" => $code],
            "code" => $code,
            'method' => 'sms',
        );
        $data_json = json_encode($data_to_send);


        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setopt(CURLOPT_URL, 'https://api.sinch.com/verification/v1/verifications/number/'.$rea_user->mobile);
        $curl->setopt(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setopt(CURLOPT_RETURNTRANSFER, true);
        $curl->setopt(CURLOPT_USERPWD, $key_secret_api_sms);
        $curl->setopt(CURLOPT_POSTFIELDS, $data_json);
        $curl->setopt(CURLOPT_CUSTOMREQUEST, "PUT");
        $curl->put('https://api.sinch.com/verification/v1/verifications/number/'.$rea_user->mobile);
        $curl->close();

        if ($curl->error) {
            return false;
        }
        else {
            return true;
        }
    }




    public function get_notifications($rea_user_id)
    {
        $rea_user = ReaUser::find($rea_user_id);

        $notification = [];

        if(
            !$rea_user->email OR $rea_user->email==""
            OR !$rea_user->country_id OR $rea_user->country_id==""
            OR !$rea_user->idn_type_id OR $rea_user->idn_type_id==""
            OR !$rea_user->lang_id OR $rea_user->lang_id==""
            OR !$rea_user->address OR $rea_user->address==""
            OR !$rea_user->cp OR $rea_user->cp==""
            OR !$rea_user->firstname OR $rea_user->firstname==""
            OR !$rea_user->f_firstname OR $rea_user->f_firstname==""
            OR !$rea_user->idn OR $rea_user->idn==""
            OR !$rea_user->genre_id OR $rea_user->genre_id==""
            OR !$rea_user->lastname OR $rea_user->lastname==""
            OR !$rea_user->mobile OR $rea_user->mobile==""
            OR !$rea_user->quarter OR $rea_user->quarter==""
        ){
            $notification[] = [
                'text_danger' => true,
                'sref' => 'rea_user.myinfos',
                'href' => null,
                'text' => 'ReaUser.PleaseCompleteYourProfil',
            ];
        }


        if($rea_user->valide_email!="Y"){
            $notification[] = [
                'text_danger' => true,
                'sref' => 'rea_user.myinfos',
                'href' => null,
                'text' => 'ReaUser.PleaseValideYourEmail',
            ];
        }


        if($rea_user->valide_mobile!="Y"){
            $notification[] = [
                'text_danger' => true,
                'sref' => 'rea_user.myinfos',
                'href' => null,
                'text' => 'ReaUser.PleaseValideYourMobile',
            ];
        }

        return $notification;
    }
}



