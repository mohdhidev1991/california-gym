<?php namespace App\Libraries\Repositories;

use App\Models\SchoolRegistration;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Mail;
use Illuminate\Support\Facades\Lang;

class SchoolRegistrationRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\Models\SchoolRegistration';
    }

	public function search($input)
    {
        $query = SchoolRegistration::query();

        $columns = Schema::getColumnListing('schoolRegistrations');
        $attributes = array();

        foreach($columns as $attribute)
        {
            if(isset($input[$attribute]) and !empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] = $input[$attribute];
            }
            else
            {
                $attributes[$attribute] =  null;
            }
        }

        return [$query->get(), $attributes];
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "SchoolRegistration not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "SchoolRegistration not found");
        }

        return $model->delete();
    }




    public function send_submit_to_email_admin($school_registration)
    {
     
        Mail::send('emails.school_registration_submit', ['school_registration' => $school_registration], function ($message) use ($school_registration) {
            $message->from( env('site_email','direction@reaaya.com'), env('site_name','Reaaya') );
            $message->to( env('admin_email','achraffessi@gmail.com'), env('site_name','Reaaya'))->subject( Lang::get('school_registration.emails.school_registration_request') );
        });

    }
}
