<?php namespace App\Libraries\Repositories;

use App\Models\SchoolScope;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SchoolScopeRepository extends Repository
{

    /**
     * Configure the Model
     *
     **/
    public function model()
    {
        return 'App\Models\SchoolScope';
    }

    public function search($input)
    {
        $query = SchoolScope::query();

        $columns = Schema::getColumnListing('schoolScopes');
        $attributes = array();

        foreach($columns as $attribute)
        {
            if(isset($input[$attribute]) and !empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] = $input[$attribute];
            }
            else
            {
                $attributes[$attribute] =  null;
            }
        }

        return [$query->get(), $attributes];
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "SchoolScope not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "SchoolScope not found");
        }

        return $model->delete();
    }
}
