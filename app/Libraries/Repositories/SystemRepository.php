<?php namespace App\Libraries\Repositories;


use Bosnadev\Repositories\Eloquent\Repository;
use Symfony\Component\HttpKernel\Exception\HttpException;


class SystemRepository extends Repository
{


    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\Models\System';
    }



    public function clear_cache($module = null){
        
    }

}
