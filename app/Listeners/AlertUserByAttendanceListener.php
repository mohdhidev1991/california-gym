<?php

namespace App\Listeners;

use App\Events\AlertUserByAttendanceEvent;
use App\Models\Alert;
use App\Models\AlertType;
use App\Models\CourseSession;
use App\Models\ReaUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class AlertUserByAttendanceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AlertUserByAttendanceEvent  $event
     * @return void
     */
    public function handle(AlertUserByAttendanceEvent $event)
    {
        $devices = $this->getDevicesByStudentsSession($event->students_session,$event->course_session);
        $response = Alert::saveNotification($devices,$event->course_session);
        return $response;
    }

    private function getDevicesByStudentsSession($students_session,$course_session)
    {

        // Merge names of coming_status_id and exit_status_id in attendance_status_id
        foreach($students_session as $k=>$v){
            $students_session[$k]['attendance_status_id'] = isset($v['attributes']['coming_status_id']) ? $v['attributes']['coming_status_id'] : $v['attributes']['exit_status_id'];
            if(isset($students_session[$k]['attributes']))
                unset($students_session[$k]['attributes']);
        }

        // Remove the students who attendance status do not notificated
        $students_session = collect($students_session)->filter(function($item){
            return !in_array($item['attendance_status_id'],[Attendance_status_Not_called_yet,Attendance_status_Waiting_he_come]);
        })->all();

        // Extract students list to be used in getDevicesByStudentId
        $students = collect($students_session)->pluck('student_id')->all();
        foreach ($students_session as $student){
            $attendance_status_ids[$student['attendance_status_id']] = $student['attendance_status_id'];
            $students_kv[$student['student_id']] = $student['attendance_status_id'];
        }

        // Get Attendances status infos
        $attendance_status = AlertType::getByAttendanceIds($attendance_status_ids);
        $attendance_status = collect($attendance_status)->keyBy('attendance_status_id')->all();
        // Get the users who will be notified
        $devices = $this->getDevicesByStudentIds($students);

        $i = 1;
        $course_session = CourseSession::info($course_session)->toArray();
        foreach ($devices as $key => $device){
            $devices[$key]['attendance_status_id'] = $attendance_status[$students_kv[$device['student_id']]]['attendance_status_id'];
            $devices[$key]['alert_type_id'] = $attendance_status[$students_kv[$device['student_id']]]['alert_type_id'];
            $devices[$key]['alert_type_name_ar'] = $attendance_status[$students_kv[$device['student_id']]]['alert_type_name_ar'];
            $devices[$key]['alert_type_name_en'] = $attendance_status[$students_kv[$device['student_id']]]['alert_type_name_en'];
            $devices[$key]['alert_type_desc_ar'] = $this->getAlertDesc($device['student_name'],$attendance_status[$students_kv[$device['student_id']]]['alert_type_desc_ar'],$course_session['course_name_ar']);
            $devices[$key]['alert_type_desc_en'] = $this->getAlertDesc($device['student_name'],$attendance_status[$students_kv[$device['student_id']]]['alert_type_desc_en'],$course_session['course_name_ar']);
            $devices[$key]['alert_cat_id'] = $attendance_status[$students_kv[$device['student_id']]]['alert_cat_id'];
            $i++;
        }
        $devices = collect($devices)->groupBy('student_id')->toArray();

        return $devices;
    }


    private function getAlertDesc($student_name,$desc,$course_session_name=null)
    {
        $str = str_replace('{{$student_name}}',$student_name,$desc);
        $str = str_replace('{{$course_session}}',$course_session_name,$str);
        return $str;
    }
    
    private function getDevicesByStudentIds($student_ids)
    {
        $results = ReaUser::getByStudentIds($student_ids);

        return $results;
    }
}
