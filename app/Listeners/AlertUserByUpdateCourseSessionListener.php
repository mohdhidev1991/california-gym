<?php

namespace App\Listeners;

use App\Events\AlertUserByUpdateCourseSessionEvent;
use App\Helpers\HijriDateHelper;
use App\Models\Alert;
use App\Models\AlertType;
use App\Models\CourseSession;
use App\Models\ReaUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use App\Libraries\Factories\AppFactory;
use Illuminate\Support\Facades\Lang;

class AlertUserByUpdateCourseSessionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AlertUserByUpdateCourseSessionEvent  $event
     * @return void
     */
    public function handle(AlertUserByUpdateCourseSessionEvent $event)
    {
		$alert = AlertType::find($event->alert_type_id)->toArray();
		$alert['year'] = $event->course_session['year'];
		$alert['alert_type_id'] = $event->alert_type_id;
		$alert['hday_num'] = AppFactory::get_current_hday_num();
		$CourseSessionInfos = CourseSession::info($event->course_session);
        $devices = $this->getDevicesByLevelClassIdAndSymbol(
			$CourseSessionInfos->level_class_id,
			$CourseSessionInfos->symbol,
			$CourseSessionInfos->school_id,
			$alert,$CourseSessionInfos);
        $response = Alert::saveNotification($devices,$event->course_session);
        return $response;
    }



    private function getAlertDesc($device,$course_session,$desc)
    {
		$hdate = HijriDateHelper::add_dashes_to_gregdate($course_session['session_hdate']);
        $str = str_replace('{{$student_name}}',$device['student_name'],$desc);
        $str = str_replace('{{$course_name_ar}}',$course_session['course_name_ar'],$str);
        $str = str_replace('{{$course_name_en}}',$course_session['course_name_en'],$str);
        $str = str_replace('{{$level_class_ar}}',$course_session['level_class_name_ar'],$str);
        $str = str_replace('{{$level_class_en}}',$course_session['level_class_name_en'],$str);
        $str = str_replace('{{$symbol_ar}}',config('lookup.symbol.ar.'.$course_session['symbol']),$str);
        $str = str_replace('{{$symbol_en}}',config('lookup.symbol.en.'.$course_session['symbol']),$str);
        $str = str_replace('{{$hdate}}',$hdate,$str);
        return $str;
    }
    
	private function getDevicesByLevelClassIdAndSymbol($level_class_id,$symbol,$school_id,$alert,$course_session)
	{

		$devices = ReaUser::getByLevelClassIdAndSymbol($level_class_id,$symbol,$school_id,$alert['year']);

		return $this->reformatDevices($devices,$alert,$course_session);
	}
	
	private function reformatDevices($devices,$alert,$course_sesion){

		foreach ($devices as $key => $device){

			$devices[$key]['alert_type_id'] = $alert['alert_type_id'];
			$devices[$key]['alert_type_name_ar'] = $alert['alert_type_name_ar'];
			$devices[$key]['alert_type_name_en'] = $alert['alert_type_name_en'];
			$devices[$key]['alert_type_desc_ar'] = $this->getAlertDesc($device,$course_sesion,$alert['alert_type_desc_ar']);
			$devices[$key]['alert_type_desc_en'] = $this->getAlertDesc($device,$course_sesion,$alert['alert_type_desc_en']);
			$devices[$key]['alert_cat_id'] = $alert['alert_cat_id'];
			$devices[$key]['year'] = $alert['year'];
			$devices[$key]['hday_num'] = $alert['hday_num'];
		}

		$devices = collect($devices)->groupBy('student_id')->toArray();


		return $devices;
	}
}
