<?php

namespace App\Listeners;

use App\Events\AlertUserEvents;
use App\Models\AlertUser;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlertUserListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AlertUserEvents  $event
     * @return void
     */
    public function handle(AlertUserEvents $event)
    {
        $response = false;
        $devices = collect($event->devices)->values()->all();

        foreach ($devices as $item){
            $device = $item['device'];
            $alert = $item['alert'];
            $lang = config('lookup.langs.'.$device['lang_id']);
            if($device['mobile_type_id'] == Mobile_Type_Android){
                $data = $this->getAndroidMessage(
                    $device['alert_type_name_'.$lang],
                    $device['alert_cat_id']
                );
                $message = PushNotification::Message($device['alert_type_desc_'.$lang],$data);
                $collection = PushNotification::app('parents_android')
                    ->to($device['ggn_code'])
                    ->send($message);
                // get response for each device push
                foreach ($collection->pushManager as $push) {
                    $success = $push->getAdapter()->getResponse()->getSuccessCount();
                }
                
                if($success){
                    $response = true;
                    $update = AlertUser::where('alert_user.owner_id',$alert['owner_id'])
                        ->where('alert_user.group_num',$alert['group_num'])
                        ->where('alert_user.hday_num',$alert['hday_num'])
                        ->where('alert_user.student_id',$alert['student_id'])
                        ->where('alert_user.parent_id',$alert['parent_id'])
                        ->where('alert_user.school_id',$alert['school_id'])
                        ->where('alert_user.year',$alert['year'])
                        ->where('alert_user.alert_num',$alert['alert_num'])
                        ->update(['alert_read'=>'N']);
                }
            }
        }
		
        return $response;
    }

    private function getAndroidMessage($title,$alert_cat_id){
        $to = "/notifications";
        $notification = [
            'to' => $to,
            'title'         => $title,
            'alert_cat_id'  => $alert_cat_id
        ];
        return $notification;
    }
}
