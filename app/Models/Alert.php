<?php namespace App\Models;

use App\Events\AlertUserEvents;
use App\Libraries\Factories\AppFactory;
use App\Libraries\Factories\ReaUserFactory;
use Illuminate\Support\Facades\App;

class Alert extends BaseModel
{
    
	public $table = "alert";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"alert_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"alert_name" => "string"
    ];

	public static $rules = [
	    
	];

	public static function saveNotification($devices,$course_session=null)
	{

		$i = 0;
		$alert_num = 1;
		foreach($devices as $student_id => $lists){

			$alert_type_id = $lists[0]['alert_type_id'];
			$alert_cat_id  = $lists[0]['alert_cat_id'];
			$alert_name_ar = $lists[0]['alert_type_name_ar'];
			$alert_name_en = $lists[0]['alert_type_name_en'];
			$alert_desc_ar = $lists[0]['alert_type_desc_ar'];
			$alert_desc_en = $lists[0]['alert_type_desc_en'];

			if(isset($course_session)){
				$year = $course_session['year'];
				$hday_num = $course_session['hday_num'];
			}else{
				$year = $lists[0]['year'];
				$hday_num = $lists[0]['hday_num'];
			}
			$alert_time = date('His');
			$alerts[] = [
				'active' => 'Y',
				'created_by' => ReaUserFactory::get_school_employee_id(),
				'created_at' => date('Y-m-d H:i:s'),
				'owner_id' => ReaUserFactory::get_school_employee_id(),
				'student_id' => $student_id,
				'school_id' => ReaUserFactory::get_current_school_id(),
				'group_num' => ReaUserFactory::get_group_num(),
				'year' => $year,
				'hday_num' => $hday_num,
				'send_method_mfk' => ','.Send_Method_MobileNotification.',',
				'receiver_enum' => Receiver_Enum_Student,
				'alert_num' => $alert_num,
				'alert_time' => $alert_time,
				'alert_type_id' => $alert_type_id,
				'alert_cat_id'  => $alert_cat_id,
				'alert_name_ar' => $alert_name_ar,
				'alert_name_en' => $alert_name_en,
				'alert_text_ar' => $alert_desc_ar,
				'alert_text_en' => $alert_desc_en
			];

			foreach($lists as $item){


				$alert_user = [
					'active' => 'Y',
					'created_by'	=> ReaUserFactory::get_school_employee_id(),
					'created_at'	=> date('Y-m-d H:i:s'),
					'owner_id'  	=> ReaUserFactory::get_school_employee_id(),
					'student_id'	=> $student_id,
					'parent_id' 	=> $item['parent_id'],
					'school_id' 	=> ReaUserFactory::get_current_school_id(),
					'group_num' 	=> ReaUserFactory::get_group_num(),
					'year' 			=> $year,
					'hday_num' 		=> $hday_num,
					'alert_num' 	=> $alert_num,
					'alert_time'	=> $alert_time,
					'alert_read'	=> 'W'
				];
				$alert_users[] = $alert_user;
				$devices_with_alert_user[$i]['device'] = $item;
				$devices_with_alert_user[$i]['alert'] = $alert_user;
				$i++;
			}
			$alert_num++;
		}
	
		if(isset($alerts) && count($alerts)>0){
			$insert = Alert::insert($alerts);
			if(!$insert)
				return 'alert.errors.insert_sql_problem';
		}
		if(isset($alert_users) && count($alert_users)>0){
			$insert = AlertUser::insert($alert_users);

			if(!$insert)
				return 'alert_user.errors.insert_sql_problem';
		}

		if(isset($devices_with_alert_user) && count($devices_with_alert_user)>0)
			$response = event(new AlertUserEvents($devices_with_alert_user));
		else
			$response = false;
		return $response;
	}

}
