<?php namespace App\Models;



class AlertStatus extends BaseModel
{
    
	public $table = "alert_status";
    

	public $fillable = [
	    "lookup_code",
		"alert_status_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"alert_status_name_ar" => "string",
		"alert_status_name_en" => "string"
    ];

	public static $rules = [
	    
	];

}
