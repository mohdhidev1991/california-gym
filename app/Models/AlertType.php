<?php namespace App\Models;


use Illuminate\Support\Facades\DB;
class AlertType extends BaseModel
{
    
	public $table = "alert_type";
    

	public $fillable = [
	    "lookup_code",
		"alert_type_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"alert_type_name" => "string"
    ];

	public static $rules = [
	    
	];

	public static function getByAttendanceIds(array $ids){
		$results = AlertType::select(
						'attendance_status_id',
						DB::raw('id as alert_type_id'),
						'alert_type_name_ar',
						'alert_type_name_en',
						'alert_type_desc_ar',
						'alert_type_desc_en',
						'alert_cat_id'
					)
					->whereIn('attendance_status_id',$ids)
					->get()->toArray();
		return $results;
	}
}
