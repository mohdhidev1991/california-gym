<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class AlertUser extends Model
{
    
	public $table = "alert_user";
    

	public $fillable = [
	    "group_num",
		"school_id",
		"owner_id",
		"year",
		"hday_num",
		"alert_time",
		"alert_num",
		"student_id",
		"parent_id",
		"alert_read",
		"alert_read_hdate",
		"alert_read_time"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"school_id" => "integer",
		"owner_id" => "integer",
		"year" => "string",
		"alert_time" => "string",
		"alert_num" => "integer",
		"student_id" => "integer",
		"parent_id" => "integer",
		"alert_read" => "string",
		"alert_read_hdate" => "string",
		"alert_read_time" => "string"
    ];

	public static $rules = [
	    
	];


}
