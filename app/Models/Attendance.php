<?php namespace App\Models;



class Attendance extends BaseModel
{
    
	public $table = "attendance";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"att_hdate",
		"att_time",
		"attendance_type_id",
		"rea_user_id",
		"rejected",
		"att_comments"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"att_hdate" => "string",
		"att_time" => "string",
		"attendance_type_id" => "integer",
		"rea_user_id" => "integer",
		"rejected" => "string",
		"att_comments" => "string"
    ];

	public static $rules = [
	    
	];

}
