<?php
/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 23/01/16
 * Time: 07:28 ص
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Auth;

class BaseModel extends Model
{
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const VERSION = 'version';

    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user();
            $model->created_by = 0;
            if($user){
                $model->created_by = $user->id;    
            }
        });

        
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = 0;
            if($user){
                $model->updated_by = $user->id;
            }
            if(isset($model->version) AND $model->version)
                $model->version++;
            else
                $model->version = 1;
        });
    }
    /**
     * Update the creation and update timestamps and user id of operation.
     *
     * @return void
     */
    protected function updateTimestamps()
    {
        $user_id = 0;
        if(Auth::user()){
            $user_id = Auth::user()->id;    
        }
        
        $time = $this->freshTimestamp();
        
        if (! $this->isDirty(static::UPDATED_AT)) {
            $this->setUpdatedAt($time);
            $this->setUpdatedBy($user_id);
            $this->incrimentVersion();
        }

        if (! $this->exists && ! $this->isDirty(static::CREATED_AT)) {
            $this->setCreatedAt($time);
            $this->setCreatedBy($user_id);
            $this->initVersion();
        }
    }

    /**
     * Get the name of the "created by" column.
     *
     * @return string
     */
    public function getUpdatedByColumn()
    {
        return static::UPDATED_BY;
    }

    /**
     * Set the value of the "updated by" attribute.
     *
     * @param  mixed  $value
     * @return $this
     */
    public function setUpdatedBy($value)
    {
        $this->{static::UPDATED_BY} = $value;

        return $this;
    }

    /**
     * Get the name of the "created by" column.
     *
     * @return string
     */
    public function getCreatedByColumn()
    {
        return static::CREATED_BY;
    }

    /**
     * Set the value of the "created by" attribute.
     *
     * @param  mixed  $value
     * @return $this
     */
    public function setCreatedBy($value)
    {
        $this->{static::CREATED_BY} = $value;

        return $this;
    }

    /**
     * Set the value of the "version" attribute.
     *
     * @param  mixed  $value
     * @return $this
     */
    public function incrimentVersion()
    {
        $this->{static::VERSION} = $this->{static::VERSION}++;

        return $this;
    }

    /**
     * Set the value of the "version" attribute.
     *
     * @param  mixed  $value
     * @return $this
     */
    public function initVersion()
    {
        $this->{static::VERSION} = 1;

        return $this;
    }
}