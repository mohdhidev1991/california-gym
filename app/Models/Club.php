<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Club extends BaseModel
{

	public $table = "club";

	public $fillable = [
		"id",
		"club_name",
		"code_club",
		"active",
		"phone",
		"address",
		"disponibility",
		"email_1",
		"email_2",
		"opening_hour_1",
		"opening_hour_2",
		"opening_hour_3",
		"opening_hour_4",
		"opening_hour_5",
		"opening_hour_6",
		"opening_hour_7",
		"closing_hour_1",
		"closing_hour_2",
		"closing_hour_3",
		"closing_hour_4",
		"closing_hour_5",
		"closing_hour_6",
		"closing_hour_7",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    	"id" => "integer",
    	"created_by" => "integer",
    	"created_at" => "string",
    	"update_by" => "integer",
		"update_at" => "string",
		"active" => "string",
		"version" => "integer",
		"club_name" => "string",
		"code_club" => "string",
		"phone" => "string",
		"address" => "string",
		"disponibility" => "string",
		"email_1" => "string",
		"email_2" => "string",
		"opening_hour_1" => "string",
		"opening_hour_2" => "string",
		"opening_hour_3" => "string",
		"opening_hour_4" => "string",
		"opening_hour_5" => "string",
		"opening_hour_6" => "string",
		"opening_hour_7" => "string",
		"closing_hour_1" => "string",
		"closing_hour_2" => "string",
		"closing_hour_3" => "string",
		"closing_hour_4" => "string",
		"closing_hour_5" => "string",
		"closing_hour_6" => "string",
		"closing_hour_7" => "string",
    ];

	public static $rules = [
	    
	];

}




