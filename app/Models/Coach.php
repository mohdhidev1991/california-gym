<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Coach extends BaseModel
{

	public $table = "coach";

	public $fillable = [
		"id",
    	"active",
		"user_id",
		"email",
		"firstname",
		"lastname",
		"course_pricing",
		"registration_number",
		"mobile",
		"courses_mfk",
		"disponibility",
		"clubs_mfk",
		"week_hours_min",
		"week_hours_max",
		"week_max_price",
		"day_hours_max",
		
		"morning_starting_hour_1",
		"morning_starting_hour_2",
		"morning_starting_hour_3",
		"morning_starting_hour_4",
		"morning_starting_hour_5",
		"morning_starting_hour_6",
		"morning_starting_hour_7",

		"morning_ending_hour_1",
		"morning_ending_hour_2",
		"morning_ending_hour_3",
		"morning_ending_hour_4",
		"morning_ending_hour_5",
		"morning_ending_hour_6",
		"morning_ending_hour_7",

		"afternoon_starting_hour_1",
		"afternoon_starting_hour_2",
		"afternoon_starting_hour_3",
		"afternoon_starting_hour_4",
		"afternoon_starting_hour_5",
		"afternoon_starting_hour_6",
		"afternoon_starting_hour_7",

		"afternoon_ending_hour_1",
		"afternoon_ending_hour_2",
		"afternoon_ending_hour_3",
		"afternoon_ending_hour_4",
		"afternoon_ending_hour_5",
		"afternoon_ending_hour_6",
		"afternoon_ending_hour_7",
		"descenrichi",
		"photo_file_id",
        "note",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    	"id" => "integer",
    	"created_by" => "integer",
    	"created_at" => "string",
    	"update_by" => "integer",
		"update_at" => "string",
		"active" => "string",
		"version" => "integer",
		"user_id" => "integer",
		"email" => "string",
		"firstname" => "string",
		"lastname" => "string",
		"course_pricing" => "float",
		"registration_number" => "string",
		"mobile" => "string",
		"courses_mfk" => "string",
		"disponibility" => "string",
		"clubs_mfk" => "string",
		"week_hours_min" => "integer",
		"week_hours_max" => "integer",
		"week_max_price" => "float",
		"day_hours_max" => "integer",

		"morning_starting_hour_1" => "string",
		"morning_starting_hour_2" => "string",
		"morning_starting_hour_3" => "string",
		"morning_starting_hour_4" => "string",
		"morning_starting_hour_5" => "string",
		"morning_starting_hour_6" => "string",
		"morning_starting_hour_7" => "string",

		"morning_ending_hour_1" => "string",
		"morning_ending_hour_2" => "string",
		"morning_ending_hour_3" => "string",
		"morning_ending_hour_4" => "string",
		"morning_ending_hour_5" => "string",
		"morning_ending_hour_6" => "string",
		"morning_ending_hour_7" => "string",

		"afternoon_starting_hour_1" => "string",
		"afternoon_starting_hour_2" => "string",
		"afternoon_starting_hour_3" => "string",
		"afternoon_starting_hour_4" => "string",
		"afternoon_starting_hour_5" => "string",
		"afternoon_starting_hour_6" => "string",
		"afternoon_starting_hour_7" => "string",

		"afternoon_ending_hour_1" => "string",
		"afternoon_ending_hour_2" => "string",
		"afternoon_ending_hour_3" => "string",
		"afternoon_ending_hour_4" => "string",
		"afternoon_ending_hour_5" => "string",
		"afternoon_ending_hour_6" => "string",
		"afternoon_ending_hour_7" => "string",
		
		"descenrichi" => "string",
		"photo_file_id" => "string",
		"note" => "string",
         
    ];

	public static $rules = [
	    
	];

}




