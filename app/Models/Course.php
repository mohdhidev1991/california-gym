<?php namespace App\Models;

class Course extends BaseModel
{
    
	public $table = "course";
    

	public $fillable = [
	    "id",
		"active",
		"course_name",
		"course_code",
		"nbr_hours",
		"during",
		"course_type_id",
		"logo_file_id",
		"course_for_mfk",
		"rooms_mfk",
		"max_courses",
		"color",
	];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"updated_by" => "integer",
		"updated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"course_name" => "string",
		"course_code" => "string",
		"nbr_hours" => "float",
		"during" => "integer",
		"course_type_id" => "integer",
		"logo_file_id" => "integer",
		"course_for_mfk" => "string",
		"rooms_mfk" => "string",
		"max_courses" => "integer",
		"color" => "string",
    ];

	public static $rules = [
	    
	];


}
