<?php namespace App\Models;

class CourseFor extends BaseModel
{
    
	public $table = "course_for";
    

	public $fillable = [
	    "id",
		"active",
		"course_for_name",
	];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"updated_by" => "integer",
		"updated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"course_for_name" => "string",
    ];

	public static $rules = [
	    
	];


}
