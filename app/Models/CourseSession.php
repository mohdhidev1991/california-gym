<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CourseSession extends BaseModel
{
    
	public $table = "course_session";
    

	public $fillable = [
		"id",
		"active",
		"course_id",
		"coach_id",
		"room_id",
		"club_id",
		"primary_coach_id",
		"secondary_coach_id",
		"triple_coach_id",
		"quad_coach_id",
		"five_coach_id",
		"primary_coach_name",
		"secondary_coach_name",
		"triple_coach_name",
		"quad_coach_name",
		"five_coach_name",
		"day_num",
		"week_num",
		"week_year",
		"course_session_year",
		"course_session_month",
		"course_session_day",
		"course_session_date",
		"course_start_time",
		"course_end_time",
		"cost",
		"forced",
		"review",
		"type_course",
		"description_course",
		"time_others_begin",
		"time_others_stop",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"created_at" => "string",
		"updated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"course_id" => "integer",
		"coach_id" => "integer",
		"primary_coach_id" => "integer",
		"secondary_coach_id" => "integer",
		"triple_coach_id" => "integer",
		"quad_coach_id" => "integer",
		"five_coach_id" => "integer",
		"primary_coach_name" => "string",
		"secondary_coach_name" => "string",
		"triple_coach_name" => "string",
		"quad_coach_name" => "string",
		"five_coach_name" => "string",
		"room_id" => "integer",
		"club_id" => "integer",
		"day_num" => "integer",
		"week_num" => "integer",
		"week_year" => "integer",
		"course_session_year" => "integer",
		"course_session_month" => "integer",
		"course_session_day" => "integer",
		"course_session_date" => "string",
		"course_start_time" => "string",
		"course_end_time" => "string",
		"cost" => "integer",
		"forced" => "integer",
		"review" => "string",
		"type_course" => "string",
		"description_course" => "string",
		"time_others_begin" => "string",
		"time_others_stop" => "string",
    ];

	public static $rules = [
	    
	];

}
