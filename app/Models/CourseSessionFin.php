<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseSessionFin extends Model
{
    public $table = "course_session_fin";
    
    public $fillable = [
		"id",
		"active",
		"coach_session_id",
        "state",
        "nbr_paticipants",
        "ancien_coach",
        "ancien_cours",
        "coach",
    ];

    protected $casts = [

        "id" => "integer",
        "active" => "string",
        "coach_session_id" => "integer",
		"state" => "string",
        "nbr_paticipants" => "integer",
        "ancien_coach" => "string",
        "ancien_cours" => "string",
        "coach" => "string",
    
    ];

    
    public static $rules = [
    ];

}
