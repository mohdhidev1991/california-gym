<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CourseType extends BaseModel
{

	public $table = "course_type";

	public $fillable = [
		"id",
		"active",
		"course_type_name",
	];


    protected $casts = [
        "id" => "integer",
        "created_by" => "integer",
        "created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"course_type_name" => "string",
    ];

	public static $rules = [
	    
	];

}