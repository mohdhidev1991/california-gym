<?php namespace App\Models;


class CoursesConfigItem extends BaseModel
{
    
	public $table = "courses_config_item";
    

	public $fillable = [
	    "courses_config_template_id",
		"course_id",
		"level_class_id",
		"session_nb",
		"coef"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"courses_config_template_id" => "integer",
		"course_id" => "integer",
		"level_class_id" => "integer"
    ];

	public static $rules = [
	    
	];

}
