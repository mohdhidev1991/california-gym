<?php namespace App\Models;


class CoursesConfigTemplate extends BaseModel
{
    
	public $table = "courses_config_template";
    

	public $fillable = [
	    "courses_config_template_name",
		"levels_template_id",
		"courses_template_id",
		"session_duration_min",
		"school_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"courses_config_template_name" => "string",
		"levels_template_id" => "integer",
		"courses_template_id" => "integer",
		"school_id" => "integer"
    ];

	public static $rules = [
	    
	];

}
