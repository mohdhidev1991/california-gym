<?php namespace App\Models;


class CoursesTemplate extends BaseModel
{
    
	public $table = "courses_template";
    

	public $fillable = [
	    "courses_template_name",
		"course_mfk",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"courses_template_name" => "string",
		"course_mfk" => "string",
    ];

	public static $rules = [
	    
	];

}
