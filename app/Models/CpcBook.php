<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CpcBook extends BaseModel
{

	public $table = "cpc_book";

	public $fillable = [
		"id",
		"active",
		"level_class_mfk",
		"course_id",
		"expiring_hdate",
		"parent_book_id",
		"book_type_id",
		"book_category_id",
		"book_name",
		"book_nb_pages",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"level_class_mfk" => "string",
		"course_id" => "integer",
		"expiring_hdate" => "string",
		"parent_book_id" => "integer",
		"book_type_id" => "integer",
		"book_category_id" => "integer",
		"book_name" => "string",
		"book_nb_pages" => "integer",
    ];

	public static $rules = [
	    
	];

}




