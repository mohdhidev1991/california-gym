<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CpcBookPage extends BaseModel
{

	public $table = "cpc_book_page";


	public $fillable = [
		"id",
		"active",
		"book_id",
		"book_page_num",
		"book_page_name",
		"book_page_content",
		"book_page_url",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"book_id" => "integer",
		"book_page_num" => "integer",
		"book_page_name" => "string",
		"book_page_content" => "string",
		"book_page_url" => "string",
    ];

	public static $rules = [
	    
	];

}




