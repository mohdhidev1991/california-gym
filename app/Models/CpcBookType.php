<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CpcBookType extends BaseModel
{

	public $table = "cpc_book_type";


	public $fillable = [
		"id",
		"active",
		"book_type_name_ar",
		"book_type_name_en",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"book_type_name_ar" => "string",
		"book_type_name_en" => "string",
    ];

	public static $rules = [
	    
	];

}




