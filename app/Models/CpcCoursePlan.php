<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CpcCoursePlan extends BaseModel
{

	public $table = "cpc_course_plan";


	public $fillable = [
		"id",
		"active",
		"course_program_id",
		"level_class_id",
		"course_id",
		"course_num",
		"course_content",
		"course_book_id",
		"course_book_from_page",
		"course_book_to_page",
		"course_comment",
		"next_devoir",
		"next_devoir_book_id",
		"next_devoir_book_from_page",
		"next_devoir_book_to_page",
		"sexam_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"course_program_id" => "integer",
		"level_class_id" => "integer",
		"course_id" => "integer",
		"course_num" => "integer",
		"course_content" => "string",
		"course_book_id" => "integer",
		"course_book_from_page" => "integer",
		"course_book_to_page" => "integer",
		"course_comment" => "string",
		"next_devoir" => "string",
		"next_devoir_book_id" => "integer",
		"next_devoir_book_from_page" => "integer",
		"next_devoir_book_to_page" => "integer",
		"sexam_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




