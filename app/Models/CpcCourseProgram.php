<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CpcCourseProgram extends BaseModel
{

	public $table = "cpc_course_program";


	public $fillable = [
		"id",
		"active",
		"course_program_name_ar",
		"course_program_name_en",
		"levels_template_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"course_program_name_ar" => "string",
		"course_program_name_en" => "string",
		"levels_template_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




