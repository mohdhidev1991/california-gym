<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CpcCourseProgramBook extends BaseModel
{

	public $table = "cpc_course_program_book";


	public $fillable = [
		"id",
		"active",
		"course_program_id",
		"book_id",
		"level_class_id",
		"course_nb",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"course_program_id" => "integer",
		"book_id" => "integer",
		"level_class_id" => "integer",
		"course_nb" => "integer",
    ];

	public static $rules = [
	    
	];

}




