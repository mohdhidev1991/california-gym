<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CrmErequest extends BaseModel
{

	public $table = "crm_erequest";

	public $connection = "mysqlcrm";

	public $fillable = [
		"id",
		"auser_id",
		"crm_erequest_type_id",
		"crm_erequest_cat_id",
		"crm_erequest_level_enum",
		"crm_erequest_status_id",
		"crm_erequest_hdate",
		"crm_erequest_time",
		"crm_erequest_fup_hdate",
		"crm_erequest_fup_time",
		"crm_erequest_name",
		"crm_erequest_details",
		"crm_file_mfk",
		"owner_stakeholder_id",
		"owner_auser_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"auser_id" => "integer",
		"crm_erequest_type_id" => "integer",
		"crm_erequest_cat_id" => "integer",
		"crm_erequest_level_enum" => "integer",
		"crm_erequest_status_id" => "integer",
		"crm_erequest_hdate" => "integer",
		"crm_erequest_time" => "string",
		"crm_erequest_fup_hdate" => "string",
		"crm_erequest_fup_time" => "string",
		"crm_erequest_name" => "string",
		"crm_erequest_details" => "string",
		"crm_file_mfk" => "string",
		"owner_stakeholder_id" => "integer",
		"owner_auser_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




