<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CrmErequestFup extends BaseModel
{

	public $table = "crm_erequest_fup";

	public $connection = "mysqlcrm";

	public $fillable = [
		"id",
		"crm_erequest_id",
		"fup_hdate",
		"fup_time",
		"is_read",
		"before_erequest_status_id",
		"after_erequest_status_id",
		"owner_id",
		"fup_changes",
		"fup_comments",
		"before_erequest_path_id",
		"crm_erequest_path_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"crm_erequest_id" => "integer",
		"fup_hdate" => "string",
		"fup_time" => "string",
		"is_read" => "string",
		"before_erequest_status_id" => "integer",
		"after_erequest_status_id" => "integer",
		"owner_id" => "integer",
		"fup_changes" => "string",
		"fup_comments" => "string",
		"before_erequest_path_id" => "integer",
		"crm_erequest_path_id" => "integer",
    	];



	public static $rules = [
	    
	];

}


