<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CrmErequestPath extends BaseModel
{

	public $table = "crm_erequest_path";

	public $connection = "mysqlcrm";

	public $fillable = [
		"id",
		"crm_erequest_type_id",
		"crm_erequest_cat_id",
		"crm_erequest_level_enum",
		"step_num",
		"step_name_ar",
		"step_name_en",
		"dep_stakeholder_id",
		"jobrole_id",
		"crm_emrelation_id",
		"step_tasks_ar",
		"step_tasks_en",
		"nb_hours_min",
		"nb_hours_max",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"crm_erequest_type_id" => "integer",
		"crm_erequest_cat_id" => "integer",
		"crm_erequest_level_enum" => "integer",
		"step_num" => "integer",
		"step_name_ar" => "string",
		"step_name_en" => "string",
		"dep_stakeholder_id" => "integer",
		"jobrole_id" => "integer",
		"crm_emrelation_id" => "integer",
		"step_tasks_ar" => "string",
		"step_tasks_en" => "string",
		"nb_hours_min" => "integer",
		"nb_hours_max" => "integer",
    ];

	public static $rules = [
	    
	];

}




