<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CrmErequestPolicyDet extends BaseModel
{

	public $table = "crm_erequest_policy_det";

	public $connection = "mysqlcrm";

	public $fillable = [
		"id",
		"crm_erequest_policy_id",
		"requester_jobrole_id",
		"crm_erequest_type_id",
		"crm_erequest_cat_id",
		"crm_erequest_level_enum",
		"internal",
		"external_stakeholder_id",
		"dep_crm_activity_id",
		"owner_jobrole_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"crm_erequest_policy_id" => "integer",
		"requester_jobrole_id" => "integer",
		"crm_erequest_type_id" => "integer",
		"crm_erequest_cat_id" => "integer",
		"crm_erequest_level_enum" => "integer",
		"internal" => "string",
		"external_stakeholder_id" => "integer",
		"dep_crm_activity_id" => "integer",
		"owner_jobrole_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




