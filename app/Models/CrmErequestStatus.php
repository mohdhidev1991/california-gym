<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CrmErequestStatus extends BaseModel
{

	public $table = "crm_erequest_status";

	public $connection = "mysqlcrm";

	public $fillable = [
		"id",
		"lookup_code",
		"crm_erequest_status_name_ar",
		"crm_erequest_status_name_en",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"crm_erequest_status_name_ar" => "string",
		"crm_erequest_status_name_en" => "string",
    ];

	public static $rules = [
	    
	];

}




