<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CrmErequestType extends BaseModel
{

	public $table = "crm_erequest_type";

	public $connection = "mysqlcrm";

	public $fillable = [
		"id",
		"lookup_code",
		"domain_id",
		"crm_erequest_type_name_ar",
		"crm_erequest_type_name_en",
		"public",
		"authorized_jobrole_mfk",
		"atable_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"domain_id" => "integer",
		"crm_erequest_type_name_ar" => "string",
		"crm_erequest_type_name_en" => "string",
		"public" => "string",
		"authorized_jobrole_mfk" => "string",
		"atable_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




