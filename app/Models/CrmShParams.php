<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class CrmShParams extends BaseModel
{

	public $table = "crm_sh_params";

	public $connection = "mysqlcrm";

	public $fillable = [
		"id",
		"stakeholder_id",
		"crm_activity_id",
		"crm_erequest_policy_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"stakeholder_id" => "integer",
		"crm_activity_id" => "integer",
		"crm_erequest_policy_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




