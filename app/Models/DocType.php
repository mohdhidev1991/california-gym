<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class DocType extends BaseModel
{

	public $table = "file";

	public $fillable = [
		"id",
		"lookup_code",
		"titre",
		"titre_short",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"titre" => "string",
		"titre_short" => "string",
    ];

	public static $rules = [
	    
	];

}




