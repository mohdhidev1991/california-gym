<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Employee extends BaseModel
{

	public $table = "employee";

	public $connection = "mysqlhrm";

	public $fillable = [
		"lastname",
		"job",
		"jobrole_mfk",
		"titre",
		"titre_short",
		"pwd",
		"valid",
		"em_mobile",
		"em_name",
		"em_relship_id",
		"address",
		"home_latitude",
		"pprop_mfk",
		"skill_type_mfk",
		"g_f_firstname",
		"home_longitude",
		"city_id",
		"cp",
		"desk",
		"country_id",
		"mobile",
		"phone",
		"auser_id",
		"username",
		"auser_id",
		"gender_id",
		"email",
		"id_sh_org",
		"id_sh_dep",
		"id_sh_div",
		"emp_num",
		"firstname",
		"prefixe",
		"f_firstname",
		"birth_date",
		"first_empl_date",
		"last_empl_date",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lastname" => "string",
		"job" => "string",
		"jobrole_mfk" => "string",
		"titre" => "string",
		"titre_short" => "string",
		"pwd" => "string",
		"valid" => "string",
		"em_mobile" => "string",
		"em_name" => "string",
		"em_relship_id" => "integer",
		"address" => "string",
		"home_latitude" => "string",
		"pprop_mfk" => "string",
		"skill_type_mfk" => "string",
		"g_f_firstname" => "string",
		"home_longitude" => "string",
		"city_id" => "integer",
		"cp" => "string",
		"desk" => "string",
		"country_id" => "integer",
		"mobile" => "string",
		"phone" => "string",
		"auser_id" => "integer",
		"username" => "string",
		"auser_id" => "integer",
		"gender_id" => "integer",
		"email" => "string",
		"id_sh_org" => "integer",
		"id_sh_dep" => "integer",
		"id_sh_div" => "integer",
		"emp_num" => "string",
		"firstname" => "string",
		"prefixe" => "string",
		"f_firstname" => "string",
		"birth_date" => "string",
		"first_empl_date" => "string",
		"last_empl_date" => "string",
    ];

	public static $rules = [
	    
	];

}




