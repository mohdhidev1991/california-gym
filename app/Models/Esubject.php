<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Esubject extends BaseModel
{

	public $table = "esubject";

	public $fillable = [
		"id",
		"active",
		"ewall_id",
		"owner_id",
		"concerned_student_id",
		"concerned_employee_id",
		"publish",
		"finished",
		"crm_erequest_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"ewall_id" => "integer",
		"owner_id" => "integer",
		"concerned_student_id" => "integer",
		"concerned_employee_id" => "integer",
		"publish" => "string",
		"finished" => "string",
		"crm_erequest_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




