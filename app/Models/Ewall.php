<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Ewall extends BaseModel
{

	public $table = "ewall";

	public $fillable = [
		"id",
		"active",
		"school_id",
		"sdepartment_id",
		"level_class_id",
		"symbol",
		"school_class_id",
		"student_id",
		"course_id",
		"ewall_name_ar",
		"ewall_name_en",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"school_id" => "integer",
		"sdepartment_id" => "integer",
		"level_class_id" => "integer",
		"symbol" => "string",
		"school_class_id" => "integer",
		"student_id" => "integer",
		"course_id" => "integer",
		"ewall_name_ar" => "string",
		"ewall_name_en" => "string",
    ];

	public static $rules = [
	    
	];

}




