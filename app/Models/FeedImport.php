<?php namespace App\Models;



class FeedImport extends BaseModel
{
    
	public $table = "feed_import";
    

	public $fillable = [
	    "school_id",
		"feed_name",
		"source_name",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"school_id" => "integer",
		"feed_name" => "string",
		"source_name" => "string"
    ];

	public static $rules = [
	    
	];

}
