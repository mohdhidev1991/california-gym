<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class File extends BaseModel
{

	public $table = "file";

	public $fillable = [
		"id",
		"active",
		"file_name",
		"owner_id",
		"doc_type_id",
		"original_name",
		"file_ext",
		"file_type",
		"file_size",
		"picture",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"active" => "string",
		"version" => "integer",
		"file_name" => "string",
		"owner_id" => "integer",
		"doc_type_id" => "integer",
		"original_name" => "string",
		"file_ext" => "string",
		"file_type" => "string",
		"file_size" => "string",
		"picture" => "string",
    ];

	public static $rules = [
	    
	];

}




