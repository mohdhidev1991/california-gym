<?php namespace App\Models;


class HDay extends BaseModel
{
    
	public $table = "hday";
    

	public $fillable = [
	    "school_year_id",
		"wday_id",
		"hday_num",
		"hmonth",
		"hday_gdat",
		"hday_date",
		"holiday",
		"hday_descr",
		"holiday_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "school_year_id" => "integer",
		"wday_id" => "integer",
		"hday_num" => "integer",
		"hmonth" => "string",
		"hday_gdat" => "string",
		"hday_date" => "string",
		"holiday" => "string",
		"hday_descr" => "string",
		"holiday_id" => "integer"
    ];

	public static $rules = [
	    
	];

}
