<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Holiday extends BaseModel
{

	public $table = "holiday";

	public $fillable = [
		"id",
		"active",
		"date",
		"description",
	];


    protected $casts = [
        "id" => "integer",
        "created_by" => "integer",
        "created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"active" => "string",
		"version" => "integer",
		"date" => "string",
		"description" => "string",
    ];

	public static $rules = [
	    
	];

}