<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class HrmLeave extends BaseModel
{

	public $table = "hrm_leave";

	public $connection = "mysqlhrm";

	public $fillable = [
		"id",
		"hrm_leave_type_id",
		"owner_id",
		"student_id",
		"hrm_leave_start_hdate",
		"hrm_leave_start_time",
		"hrm_leave_end_hdate",
		"hrm_leave_end_time",
		"hrm_leave_name",
		"hrm_leave_reason",
		"approved",
		"approval_auser_id",
		"crm_erequest_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"hrm_leave_type_id" => "integer",
		"owner_id" => "integer",
		"student_id" => "integer",
		"hrm_leave_start_hdate" => "integer",
		"hrm_leave_start_time" => "string",
		"hrm_leave_end_hdate" => "integer",
		"hrm_leave_end_time" => "string",
		"hrm_leave_name" => "string",
		"hrm_leave_reason" => "string",
		"approved" => "string",
		"approval_auser_id" => "integer",
		"crm_erequest_id" => "integer",
    ];

	public static $rules = [
	    
	];

}




