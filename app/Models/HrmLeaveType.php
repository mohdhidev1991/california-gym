<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class HrmLeaveType extends BaseModel
{

	public $table = "hrm_leave_type";

	public $connection = "mysqlhrm";

	public $fillable = [
		"id",
		"lookup_code",
		"hrm_leave_type_name_ar",
		"hrm_leave_type_name_en",
		"crm_erequest_type_id",
		"crm_erequest_cat_id",
		"free_time",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"hrm_leave_type_name_ar" => "string",
		"hrm_leave_type_name_en" => "string",
		"crm_erequest_type_id" => "integer",
		"crm_erequest_cat_id" => "integer",
		"free_time" => "string",
    ];

	public static $rules = [
	    
	];

}




