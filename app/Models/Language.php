<?php namespace App\Models;

class Language extends BaseModel
{
    
	public $table = "lang";
    

	public $fillable = [
	    "id",
		"creation_by",
		"update_by",
		"validation_by",
		"validation_at",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"creation_at",
		"update_at",
		"short_name",
		"folder_name",
		"long_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"short_name" => "string",
		"folder_name" => "string",
		"long_name" => "string"
    ];

	public static $rules = [
	    
	];

	public static function getByLookupCode($shortName)
	{
		return self::where('lookup_code','=',$shortName)->where('active','=','Y')->first();
	}
	public static function getById($id)
	{
		return self::find($id)->where('active','=','Y')->first();
	}
}
