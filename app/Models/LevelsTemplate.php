<?php namespace App\Models;

use Illuminate\Support\Facades\DB;
class LevelsTemplate extends BaseModel
{
    
	public $table = "levels_template";

	public $fillable = [
	    "id",
	    "active",
	    "levels_template_name_ar",
	    "levels_template_name_en",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"levels_template_name_ar" => "string",
		"levels_template_name_en" => "string",
    ];

	public static $rules = [
	    
	];

}
