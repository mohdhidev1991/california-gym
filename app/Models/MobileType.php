<?php namespace App\Models;



class MobileType extends BaseModel
{
    
	public $table = "mobile_type";
    

	public $fillable = [
	    "lookup_code",
		"mobile_type_name_ar",
		"mobile_type_name_en"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"mobile_type_name_ar" => "string",
		"mobile_type_name_en" => "string"
    ];

	public static $rules = [
	    
	];

}
