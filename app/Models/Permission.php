<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Permission extends BaseModel
{

	public $table = "permission";

	public $fillable = [
		"id",
		"active",
		"lookup_code",
		"permission_name",
	];


    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"lookup_code" => "string",
		"permission_name" => "string",
    ];

	public static $rules = [
	    
	];

}




