<?php namespace App\Models;


class Post extends BaseModel
{
    
	public $table = "posts";
    

	public $fillable = [
	    
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

	public static $rules = [
	    
	];

}
