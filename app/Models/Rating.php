<?php namespace App\Models;


class Rating extends BaseModel
{
    
	public $table = "rating";
    

	public $fillable = [
	    "lookup_code",
		"id_school",
		"max_pct",
		"min_pct",
		"rating_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "lookup_code" => "string",
		"id_school" => "integer",
		"rating_name" => "string"
    ];

	public static $rules = [
	    
	];

}
