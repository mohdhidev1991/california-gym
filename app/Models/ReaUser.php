<?php namespace App\Models;

use App\Libraries\Factories\AppFactory;
use App\Libraries\Factories\ReaUserFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class ReaUser extends Authenticatable {
    
    public $table = "user";

    public $fillable = [
        "id",
        "active",
        "firstname",
        "lastname",
        "email",
        "username",
        "roles_mfk",
        "club",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        "active" => "string",
        "created_by" => "integer",
        "updated_by" => "integer",
        "created_at" => "string",
        "validated_at" => "string",
        "version" => "string",
        "firstname" => "string",
        "lastname" => "string",
        "email" => "string",
        "username" => "string",
        "pwd" => "string",
        "roles_mfk" => "string",
        "club" => "string",
    ];

    public static $rules = [
        'email' => 'required|email|max:255|unique:users',
        'pwd' => 'required|confirmed|min:6|min:24',
    ];

    protected $hidden = [
        'pwd',
    ];

    protected $guarded = [
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
        "version",
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->attributes['id'];
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    Public function setPasswordAttribute($password)
    {
        return $this->attributes['pwd'] = bcrypt($password);
    }

    public function getAuthPassword()
    {
        return $this->pwd;
    }

    public static function getActiveUsers()
    {
        return ReaUser::where(['active'=>'Y'])->get();
    }
}
