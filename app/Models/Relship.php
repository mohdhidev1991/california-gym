<?php namespace App\Models;



class Relship extends BaseModel
{
    
	public $table = "relship";
    

	public $fillable = [
	    "lookup_code",
		"relship_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"relship_name" => "string"
    ];

	public static $rules = [
	    
	];

}
