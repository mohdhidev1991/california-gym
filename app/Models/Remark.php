<?php namespace App\Models;



class Remark extends BaseModel
{
    
	public $table = "remark";
    

	public $fillable = [
	    "lookup_code",
		"remark_name",
		"remark_name_en",
		"gremark_id",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"remark_name" => "string",
		"remark_name_en" => "string",
		"gremark_id" => "integer"
    ];

	public static $rules = [
	    
	];

}
