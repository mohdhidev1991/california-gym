<?php namespace App\Models;
use Illuminate\Support\Facades\DB;
class Role extends BaseModel
{

	public $table = "role";

	public $fillable = [
		"id",
		"active",
		"lookup_code",
		"role_name",
		"permissions_mfk",
	];


    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"lookup_code" => "string",
		"role_name" => "string",
		"permissions_mfk" => "string",
    ];

	public static $rules = [
	    
	];

}




