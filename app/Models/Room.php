<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Room extends BaseModel
{

	public $table = "room";

	public $fillable = [
		"id",
		"active",
		"room_name",
		"code_room",
		"club_id",
		"capacity",
		"disponibility",
	];


    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"active" => "string",
		"version" => "integer",
		"club_id" => "integer",
		"room_name" => "string",
		"code_room" => "string",
		"capacity" => "integer",
		"disponibility" => "string",
    ];

	public static $rules = [
	    
	];

}




