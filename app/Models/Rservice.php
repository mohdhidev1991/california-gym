<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Rservice extends Model
{
    
	public $table = "rservice";
    

	public $fillable = [
	    "lookup_code",
		"rservice_name_ar",
		"rservice_name_en",
		"for_parent",
		"for_school",
		"price",
		"currency_id",
		"payment_period_mfk"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"rservice_name_ar" => "string",
		"rservice_name_en" => "string",
		"for_parent" => "string",
		"for_school" => "string",
		"price" => "float",
		"currency_id" => "integer",
		"payment_period_mfk" => "string"
    ];

	public static $rules = [
	    
	];

}
