<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class RserviceStudent extends Model
{
    
	public $table = "rservice_student";


	public $fillable = [
	    "school_year_id",
		"rservice_id",
		"student_id",
		"parent_user_id",
		"payment_period_id",
		"start_hdate",
		"expire_hdate",
		"amount",
		"currency_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"school_year_id" => "integer",
		"rservice_id" => "integer",
		"student_id" => "integer",
		"parent_user_id" => "integer",
		"payment_period_id" => "integer",
		"start_hdate" => "string",
		"expire_hdate" => "string",
		"amount" => "float",
		"currency_id" => "integer"
    ];

	public static $rules = [
	    
	];

	public static function get_available_students_by_rea_user_id($rea_user_id, $student_id = null){
		$current_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		if($student_id){
			return RserviceStudent::select(['student_id'])
			->where('parent_user_id',(int)$rea_user_id)
			->where('start_hdate','<=',$current_hdate)
			->where('expire_hdate','>=',$current_hdate)
			->where('student_id',(int)$student_id)
			->get()
			->lists('student_id')
			->toArray();
		}else{
			return RserviceStudent::select(['student_id'])
			->where('parent_user_id',(int)$rea_user_id)
			->where('start_hdate','<=',$current_hdate)
			->where('expire_hdate','>=',$current_hdate)
			->get()
			->lists('student_id')
			->toArray();
		}
		
	}
}
