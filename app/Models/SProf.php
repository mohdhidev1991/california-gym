<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class SProf extends BaseModel
{

	public $table = "sprof";

	public $fillable = [
	    "course_mfk",
		"wday_mfk",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"course_mfk" => "string",
		"wday_mfk" => "string"
    ];

	public static $rules = [
	    
	];

}




