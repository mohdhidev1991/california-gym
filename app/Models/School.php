<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class School extends BaseModel
{
    
	public $table = "school";

	public $fillable = [
	    "scapacity",
		"expiring_hdate",
		"city_id",
		"genre_id",
		"group_school_id",
		"lang_id",
		"school_type_id",
		"period_mfk",
		"sp2",
		"address",
		"maps_location_url",
		"pc",
		"quarter",
		"school_name_ar",
		"school_name_en",
		"sp1",
		"group_num",
		"date_system_id"
	];


	public static $exported_data = [
	    "id" => "ID",
	    "active" => "Active",
	    "group_num" => "Group num",
	    "school_name_ar" => "Arabic name",
		"school_name_en" => "English name",
	    "city_name_ar" => "City ar",
	    "city_name_en" => "City en",
		"country_name_ar" => "Country ar",
		"country_name_ar" => "Country en",
		"group_school_name_ar" => "Group School ar",
		"group_school_name_en" => "Group School en",
		"genre_name_ar" => "Genre ar",
		"genre_name_en" => "Genre en",
		"lang_name_ar" => "Language ar",
		"lang_name_en" => "Language en",
		"school_type_name_ar" => "School type ar",
		"school_type_name_en" => "School type en",
		"levels_template_name_ar" => "Levels template ar",
		"levels_template_name_en" => "Levels template en",
		"courses_template_name_ar" => "Courses template ar",
		"courses_template_name_en" => "Courses template en",
		"courses_config_template_name_ar" => "Courses config template ar",
		"courses_config_template_name_en" => "Courses config template en",
		"holiday_name_ar" => "Holidays ar",
		"holiday_name_en" => "Holidays en",
		"scapacity" => "Capacity",
		"expiring_hdate" => "Expiring Hijri Date",
		"periods" => "Periods",
		"sp1" => "SP1",
		"sp2" => "SP2",
		"address" => "Address",
		"maps_location_url" => "Localisation",
		"pc" => "Postal code",
		"quarter" => "Quarter",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"scapacity" => "integer",
		"expiring_hdate" => "string",
		"city_id" => "integer",
		"genre_id" => "integer",
		"group_school_id" => "integer",
		"lang_id" => "integer",
		"school_type_id" => "integer",
		"period_mfk" => "string",
		"address" => "string",
		"maps_location_url" => "string",
		"pc" => "string",
		"quarter" => "string",
		"school_name_ar" => "string",
		"school_name_en" => "string",
		"group_num"	=> "integer",
		"date_system_id"	=> "integer",
    ];

	public static $rules = [
	    
	];

	public static function getSchoolsByUserId($id){
		return self::select(
			DB::Raw('school.id AS school_id'),
			'school_employee.school_job_mfk',
			DB::Raw('school_employee.id AS school_employee_id'),
			'school.school_name_ar',
			'school.school_name_en',
			'school.levels_template_id',
			'school.courses_template_id',
			'school.courses_config_template_id',
			'school.group_num',
			'school.sp1',
			'school.sp2',
			'school.date_system_id',
			'date_system.date_system_name_ar',
			'date_system.date_system_name_en',
			'date_system.lookup_code as date_system_lookup_code'
		)
			->join('school_employee','school_employee.school_id','=','school.id')
			->leftJoin('date_system','date_system.id','=','school.date_system_id')
			->where('school_employee.rea_user_id','=',$id)
			->get();
	}

	public static function getGroupNum($id)
	{
		return self::findOrFail($id)->value('group_num');
	}


	public static function getExportedData()
	{
		return self::$exported_data;
	}

}
