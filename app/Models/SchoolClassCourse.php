<?php namespace App\Models;



class SchoolClassCourse extends BaseModel
{
    
	public $table = "school_class_course";
    

	public $fillable = [
	    "course_id",
		"level_class_id",
		"prof_id",
		"school_year_id",
		"symbol"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"course_id" => "integer",
		"level_class_id" => "integer",
		"prof_id" => "integer",
		"school_year_id" => "integer",
		"symbol" => "string"
    ];

	public static $rules = [
	    
	];

}
