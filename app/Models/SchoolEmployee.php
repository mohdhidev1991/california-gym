<?php namespace App\Models;


use Illuminate\Support\Facades\DB;
use Schema;

class SchoolEmployee extends BaseModel
{
    
	public $table = "school_employee";
	    

	public $fillable = [
	    "id",
		"created_by",
		"created_at",
		"update_by",
		"update_at",
		"validated_by",
		"validated_at",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"gender_id",
		"firstname",
		"f_firstname",
		"g_f_firstname",
		"lastname",
		"birth_date",
		"country_id",
		"address",
		"city_id",
		"home_latitude",
		"home_longitude",
		"mobile",
		"phone",
		"email",
		"job_description",
		"rea_user_id",
		"school_job_mfk",
		"school_stakeholder_id",
		"sdepartment_stakeholder_id",
		"school_id",
		"sdepartment_id",
		"course_mfk",
		"wday_mfk",
	];

	

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"update_by" => "integer",
		"update_at" => "string",
		"validated_by" => "integer",
		"validated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"gender_id" => "integer",
		"firstname" => "string",
		"f_firstname" => "string",
		"g_f_firstname" => "string",
		"lastname" => "string",
		"birth_date" => "string",
		"country_id" => "integer",
		"address" => "string",
		"city_id" => "integer",
		"home_latitude" => "string",
		"home_longitude" => "string",
		"mobile" => "string",
		"phone" => "string",
		"email" => "string",
		"job" => "string",
		"rea_user_id" => "integer",
		"school_job_mfk" => "string",
		"school_stakeholder_id" => "integer",
		"sdepartment_stakeholder_id" => "integer",
		"school_id" => "integer",
		"sdepartment_id" => "integer",
		"course_mfk" => "string",
		"wday_mfk" => "string",
    ];

	public static $rules = [
	    
	];


	public function save_school_employee(){

		//print_r($this->attributes);
		//print_r($this);
		//print_r($this->fillable_sprof);
		
		
		$attributes_to_update = [];
	    foreach($this->fillable_sprof as $field){
	    	if( isset($this->attributes[$field]) ){
	    		$attributes_to_update[$field] = $this->attributes[$field]; 
	    	}
	    }
	    if(isset($this->id)){
	    	DB::table($this->table_sprof)
	        ->where('id', $this->id)
	        ->update($attributes_to_update);
	    }else{
	    	//
	    }


	    $this->setConnection('mysqlhrm');
	    $attributes_to_update = [];
	    foreach($this->fillable_sempl as $field){
	    	if( isset($this->attributes[$field]) ){
	    		$attributes_to_update[$field] = $this->attributes[$field]; 
	    	}
	    }
	    if(isset($this->id)){
	    	DB::table($this->table_sempl)
	        ->where('id', $this->id)
	        ->update($attributes_to_update);
	    }else{
	    	//
	    }
		//$this->setConnection('');
	}

}




