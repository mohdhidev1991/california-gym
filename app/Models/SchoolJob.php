<?php namespace App\Models;



class SchoolJob extends BaseModel
{
    
	public $table = "school_job";
    

	public $fillable = [
	    "lookup_code",
		"school_job_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"school_job_name" => "string"
    ];

	public static $rules = [
	    
	];

	public static function getByIdsMfk($ids)
	{
		$result = SchoolJob::select('school_job.id', 'school_job.school_job_name_ar', 'school_job.school_job_name_en')
							->whereIn('id',$ids)
							->get();
		return $result;
	}
}
