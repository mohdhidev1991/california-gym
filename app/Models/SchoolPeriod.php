<?php namespace App\Models;



class SchoolPeriod extends BaseModel
{
    
	public $table = "school_period";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"capacity",
		"door_close_time",
		"door_open_time",
		"end_coming_time",
		"start_exit_time",
		"period_id",
		"school_id",
		"period_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"capacity" => "integer",
		"door_close_time" => "string",
		"door_open_time" => "string",
		"end_coming_time" => "string",
		"start_exit_time" => "string",
		"period_id" => "integer",
		"school_id" => "integer",
		"period_name" => "string"
    ];

	public static $rules = [
	    
	];

}
