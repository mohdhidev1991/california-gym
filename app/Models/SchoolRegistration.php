<?php namespace App\Models;

class SchoolRegistration extends BaseModel
{
    
	public $table = "school_registration";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"school_name",
		"firstname",
		"lastname",
		"email",
		"mobile",
		"city"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"school_name" => "string",
		"firstname" => "string",
		"lastname" => "string",
		"email" => "string",
		"mobile" => "string",
		"city" => "string"
    ];

	public static $rules = [
	    
	];

}
