<?php namespace App\Models;


class SchoolScope extends BaseModel
{

    public $table = "school_scope";


    public $fillable = [
        "school_year_id",
        "school_level_id",
        "level_class_id",
        "class_nb",
        "sdepartment_id"
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        "creation_user_id" => "integer",
        "update_user_id" => "integer",
        "validation_user_id" => "integer",
        "active" => "string",
        "version" => "integer",
        "update_groups_mfk" => "string",
        "delete_groups_mfk" => "string",
        "display_groups_mfk" => "string",
        "sci_id" => "integer",
        "school_year_id" => "integer",
        "school_level_id" => "integer",
        "level_class_id" => "integer",
        "class_nb" => "integer",
        "sdepartment_id" => "integer"
    ];

    public static $rules = [

    ];

}