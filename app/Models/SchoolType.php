<?php namespace App\Models;



class SchoolType extends BaseModel
{
    
	public $table = "school_type";
    

	public $fillable = [
	    "lookup_code",
		"tservice_id",
		"school_type_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lookup_code" => "string",
		"tservice_id" => "integer",
		"school_type_name" => "string"
    ];

	public static $rules = [
	    
	];

}
