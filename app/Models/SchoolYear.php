<?php namespace App\Models;


class SchoolYear extends BaseModel
{
    
	public $table = "school_year";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"school_year_end_hdate",
		"school_year_start_hdate",
		"school_id",
		"school_year_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"school_year_end_hdate" => "string",
		"school_year_start_hdate" => "string",
		"school_id" => "integer",
		"school_year_name" => "string"
    ];

	public static $rules = [
	    
	];




	public static $exported_data = [
	    "id" => "ID",
	    "active" => "Active",
	    "school_year_name_ar" => "School year ar",
	    "school_year_name_ar" => "School year en",
	    "school_id" => "School ID",
	    "school_name_ar" => "School name ar",
	    "school_name_en" => "School name en",
	    "school_year_start_hdate" => "Start date",
	    "school_year_end_hdate" => "End date",
	    "year" => "Year",
	    "admission_start_hdate" => "Admission start date",
	    "admission_end_hdate" => "Admission end date",
	];

	public static function getExportedData()
	{
		return self::$exported_data;
	}

	public static function getSchoolYearBySchoolId($hdate,$school_id)
	{
		$school_year = self::select('id','year')
							->where('school_year.school_year_start_hdate','<=',$hdate)
							->where('school_year.school_year_end_hdate','>=',$hdate)
							->where('school_year.school_id','=',$school_id)->first();
		return $school_year;
	}

}
