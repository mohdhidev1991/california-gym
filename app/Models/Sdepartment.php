<?php namespace App\Models;

use DB;

class Sdepartment extends BaseModel
{
    
	public $table = "sdepartment";
    

	public $fillable = [
	    "sdepartment_name",
		"school_id",
		"week_template_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"sdepartment_name" => "string",
		"school_id" => "integer",
		"week_template_id" => "integer"
    ];

	public static $rules = [
	    
	];



	public static $exported_data = [
	    "id" => "ID",
	    "active" => "Active",
	    "sdepartment_name_ar" => "School department ar",
	    "sdepartment_name_en" => "School department en",
	    "school_id" => "School ID",
	    "school_name_ar" => "School name ar",
	    "school_name_en" => "School name en",
	    "week_template_name_ar" => "Week template ar",
	    "week_template_name_en" => "Week template en",
	];

	public static function getExportedData()
	{
		return self::$exported_data;
	}




	public static function getSdepartmentsByUserId($rea_user_id){
		return self::select(
				DB::Raw('sdepartment.id'),
				DB::Raw('sdepartment.sdepartment_name_ar'),
				DB::Raw('sdepartment.sdepartment_name_en')
			)
			->join('school_employee', function ($join) {
            	$join->on('school_employee.sdepartment_id', '=', 'sdepartment.id')
            	->orOn('school_employee.sdepartment_2_id', '=', 'sdepartment.id')
            	->orOn('school_employee.sdepartment_3_id', '=', 'sdepartment.id');
        	})
			->where('school_employee.rea_user_id','=',$rea_user_id)
			->get();
	}

	public static function getSdepartmentsByUserIdAndSchoolId($rea_user_id, $school_id){
		return self::select(
				DB::Raw('sdepartment.id as sdepartment_id'),
				DB::Raw('sdepartment.sdepartment_name_ar'),
				DB::Raw('sdepartment.sdepartment_name_en')
			)
			->join('school_employee', function ($join) {
            	$join->on('school_employee.sdepartment_id', '=', 'sdepartment.id')
            	->orOn('school_employee.sdepartment_2_id', '=', 'sdepartment.id')
            	->orOn('school_employee.sdepartment_3_id', '=', 'sdepartment.id');
        	})
			->where('school_employee.rea_user_id','=',$rea_user_id)
			->where('sdepartment.school_id','=',$school_id)
			->get();
	}

}
