<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Student extends BaseModel
{
    
	public $table = "student";
    

	public $fillable = [
	    "id",
		"active",
		"father_rea_user_id",
		"mother_rea_user_id",
		"rea_user_id",
		"resp_rea_user_id",
		"paid",
		"resp_relationship",
		"student_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"father_rea_user_id" => "integer",
		"mother_rea_user_id" => "integer",
		"rea_user_id" => "integer",
		"resp_rea_user_id" => "integer",
		"paid" => "string",
		"resp_relationship" => "string",
		"student_name" => "string"
    ];

	public static $rules = [
	    
	];

	public static function getByResponsibleId($responsible_id)
	{
		return Student::select(DB::Raw('CONCAT(firstname," ",f_firstname," ",lastname) AS fullname'))
					->join('family_relation','family_relation.rea_user_id','=','student.rea_user_id')
					->where('family_relation.resp_rea_user_id','=',$responsible_id)
					->get();
	}



	public static function get_student_school_fullinfos($student_id, $labels = false)
	{
		$current_hdate = \App\Libraries\Factories\AppFactory::get_current_hdate();
		if($labels){
			$extend_select = "
			, s.school_name_ar,
			s.school_name_en,
			sd.sdepartment_name_ar,
			sd.sdepartment_name_en,
			st.firstname,
			st.lastname,
			st.f_firstname,
			lc.level_class_name_ar,
			lc.level_class_name_en,
			sl.school_level_name_ar,
			sl.school_level_name_en,
			sy.school_year_name_ar,
			sy.school_year_name_en
			";
		}else{
			$extend_select = null;
		}
		$sql = "SELECT sf.student_id,
			sf.id AS student_file_id,
			sf.symbol,
			sf.school_class_id,
			sf.level_class_id,
			sf.student_place,
			sf.year,
			sc.school_year_id,
			sy.school_id,
			ssc.sdepartment_id
			{$extend_select}
			FROM student_file AS sf
			JOIN school_class AS sc ON sc.id = sf.school_class_id
				AND sc.active = 'Y'
			JOIN school_year AS sy ON sy.id = sc.school_year_id
				AND sy.active = 'Y'
			JOIN school_scope AS ssc ON ssc.school_year_id = sy.id
				AND ssc.level_class_id = sf.level_class_id
				AND ssc.active = 'Y'
			JOIN school AS s ON s.id = sy.school_id
				AND s.active = 'Y'
			JOIN sdepartment AS sd ON sd.id = ssc.sdepartment_id
				AND sd.active = 'Y'
			JOIN student AS st ON st.id = sf.student_id
				AND st.active = 'Y'
			JOIN level_class AS lc ON lc.id = sf.level_class_id
				AND lc.active = 'Y'
			JOIN school_level AS sl ON sl.id = ssc.school_level_id
				AND sl.active = 'Y'
			WHERE sf.student_id = :student_id
			  AND sf.student_file_status_id=1
			  AND sf.active = 'Y'
			  AND sy.school_year_start_hdate <= :hdate_now_1
			  AND sy.school_year_end_hdate >= :hdate_now_2
			GROUP BY sf.id
		";

		return \DB::select( \DB::raw($sql),
				["student_id" => $student_id,
				"hdate_now_1" => $current_hdate,
				"hdate_now_2" => $current_hdate]
		);
	}
}
