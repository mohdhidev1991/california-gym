<?php namespace App\Models;



class StudentCourseSession extends BaseModel
{
    
	public $table = "student_session";
    

	public $fillable = [
	    "id",
		"created_by",
		"created_at",
		"updated_by",
		"updated_at",
		"validated_by",
		"validated_at",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"course_session_id",
		"student_id",
		"coming_status_id",
		"exit_status_id",
		"comments",
		"coming_time",
		"exit_time",
		"coming_alert_id",
		"exit_alert_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

		"id" => "integer" ,
		"created_by" => "integer" ,
		"created_at" => "string" ,
		"updated_by" => "integer" ,
		"updated_at" => "string" ,
		"validated_by" => "integer" ,
		"validated_at" => "string" ,
		"active" => "string" ,
		"version" => "integer" ,
		"update_groups_mfk" => "string" ,
		"delete_groups_mfk" => "string" ,
		"display_groups_mfk" => "string" ,
		"sci_id" => "integer" ,
		"course_session_id" => "integer" ,
		"student_id" => "integer" ,
		"coming_status_id" => "integer" ,
		"exit_status_id" => "integer" ,
		"comments" => "string" ,
		"coming_time" => "string" ,
		"exit_time" => "string" ,
		"coming_alert_id" => "integer" ,
		"exit_alert_id" => "integer" 
    ];

	public static $rules = [
	    
	];
}
