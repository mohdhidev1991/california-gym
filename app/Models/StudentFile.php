<?php namespace App\Models;



class StudentFile extends BaseModel
{
    
	public $table = "student_file";
    

	public $fillable = [
		"school_class_id",
		"school_id",
		"student_id",
		"symbol",
		"paid_amount",
		"paid_hdate",
		"paid_comment",
		"final_rating_id",
		"level_class_id",
		"year",
		"student_file_status_id",
		"student_num",
		"student_place",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"final_rating_id" => "integer",
		"level_class_id" => "integer",
		"school_year_id" => "integer",
		"student_file_status_id" => "integer",
		"student_num" => "integer",
		"student_place" => "integer",
		"student_id" => "integer",
		"school_id" => "integer",
		"school_class_id" => "integer",
    ];

	public static $rules = [
	    
	];

}
