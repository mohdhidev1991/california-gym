<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    public $table = "test";
    
    public $fillable = [
        
        "id",
	    "champ_1",
        "champ_2"
    ];
   
}
