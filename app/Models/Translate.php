<?php namespace App\Models;



class Translate extends BaseModel
{
    
	public $table = "translate";
    

	public $fillable = [
	    "lang_id",
	    "translate_module_id",
	    "meta_key",
	    "meta_value"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"updated_by" => "integer",
		"validated_by" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"lang_id" => "integer",
		"translate_module_id" => "integer",
		"meta_key" => "string",
		"meta_value" => "string"
    ];

	public static $rules = [
	    
	];

}
