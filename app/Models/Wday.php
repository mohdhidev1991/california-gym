<?php namespace App\Models;

class Wday extends BaseModel
{
    
	public $table = "wday";
    

	public $fillable = [
	    "id",
		"active",
		"lookup_code",
		"wday_name",
		"wday_num"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"created_by" => "integer",
		"created_at" => "string",
		"updated_by" => "integer",
		"updated_at" => "string",
		"active" => "string",
		"version" => "integer",
		"lookup_code" => "string",
		"wday_name" => "string",
		"wday_num" => "integer"
    ];

	public static $rules = [
	    
	];

}
