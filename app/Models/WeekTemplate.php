<?php namespace App\Models;

class WeekTemplate extends BaseModel
{
    
	public $table = "week_template";
    

	public $fillable = [
	    "id",
		"creation_user_id",
		"creation_date",
		"update_user_id",
		"update_date",
		"validation_user_id",
		"validation_date",
		"active",
		"version",
		"update_groups_mfk",
		"delete_groups_mfk",
		"display_groups_mfk",
		"sci_id",
		"day1_template_id",
		"day2_template_id",
		"day3_template_id",
		"day4_template_id",
		"day5_template_id",
		"day6_template_id",
		"day7_template_id",
		"level_class_id",
		"school_id",
		"week_template_name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"creation_user_id" => "integer",
		"update_user_id" => "integer",
		"validation_user_id" => "integer",
		"active" => "string",
		"version" => "integer",
		"update_groups_mfk" => "string",
		"delete_groups_mfk" => "string",
		"display_groups_mfk" => "string",
		"sci_id" => "integer",
		"day1_template_id" => "integer",
		"day2_template_id" => "integer",
		"day3_template_id" => "integer",
		"day4_template_id" => "integer",
		"day5_template_id" => "integer",
		"day6_template_id" => "integer",
		"day7_template_id" => "integer",
		"level_class_id" => "integer",
		"school_id" => "integer",
		"week_template_name" => "string"
    ];

	public static $rules = [
	    
	];

}
