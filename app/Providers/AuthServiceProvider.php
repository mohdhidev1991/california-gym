<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\ReaUser;
use DB;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }






    public static function FbCreateOrGetUser($providerUser)
    {
        
        $userFbID = ReaUser::where('fb_id',$providerUser['id'])->first();
        if ($userFbID) {
            return $userFbID;
        } else {
            $userFbEmail = ReaUser::where('email',$providerUser['email'])->first();
            if($userFbEmail){
                if(!$userFbEmail->fb_id OR $userFbEmail->fb_id!=$providerUser['id']){
                    $userFbEmail->fb_id = $providerUser['id'];
                    $userFbEmail->save();
                }
                return $userFbEmail;
            }

            $new_user = new ReaUser([
                'fb_id' => $providerUser['id'],
                'email' => $providerUser['email'],
                'pwd' => bcrypt(str_random(8)),
                'active' => 'Y',
            ]);
            if(isset($providerUser['firstname'])){
                $new_user->firstname = $providerUser['firstname'];
            }
            if(isset($providerUser['lastname'])){
                $new_user->lastname = $providerUser['lastname'];
            }
            $new_user->save();

            DB::table('module_rea_user')->insert(
                ['rea_user_id' => $new_user->id, 'module_id' => config('rea_user_module_id',44)]
            );
            return $new_user;
        }

    }




    public static function GoogleCreateOrGetUser($providerUser)
    {
        
        $userGoogleID = ReaUser::where('google_id',$providerUser['id'])->first();
        if ($userGoogleID) {
            return $userGoogleID;
        } else {
            $userGoogleEmail = ReaUser::where('email',$providerUser['email'])->first();
            if($userGoogleEmail){
                if(!$userGoogleEmail->google_id OR $userGoogleEmail->google_id!=$providerUser['id']){
                    $userGoogleEmail->google_id = $providerUser['id'];
                    $userGoogleEmail->save();
                }
                return $userGoogleEmail;
            }

            $new_user = new ReaUser([
                'google_id' => $providerUser['id'],
                'email' => $providerUser['email'],
                'pwd' => bcrypt(str_random(8)),
                'active' => 'Y',
            ]);
            if(isset($providerUser['firstname'])){
                $new_user->firstname = $providerUser['firstname'];
            }
            if(isset($providerUser['lastname'])){
                $new_user->lastname = $providerUser['lastname'];
            }
            $new_user->save();

            DB::table('module_rea_user')->insert(
                ['rea_user_id' => $new_user->id, 'module_id' => config('rea_user_module_id',44)]
            );
            
            return $new_user;
        }

    }






    public static function TwitterCreateOrGetUser($providerUser)
    {
        $userTwitterID = ReaUser::where('twitter_id',$providerUser['id'])->first();
        if ($userTwitterID) {
            return $userTwitterID;
        } else {
            /*$userTwitterEmail = ReaUser::where('email',$providerUser['email'])->first();
            if($userTwitterEmail){
                if(!$userTwitterEmail->twitter_id OR $userTwitterEmail->twitter_id!=$providerUser['id']){
                    $userTwitterEmail->twitter_id = $providerUser['id'];
                    $userTwitterEmail->save();
                }
                return $userTwitterEmail;
            }*/
            $new_user = new ReaUser([
                'twitter_id' => $providerUser['id'],
                //'email' => $providerUser['id'].'@twitter.com',
                'pwd' => bcrypt(str_random(8)),
                'active' => 'Y',
            ]);
            if(isset($providerUser['firstname'])){
                $new_user->firstname = $providerUser['firstname'];
            }
            if(isset($providerUser['lasstname'])){
                $new_user->lasstname = $providerUser['lastname'];
            }
            $new_user->save();
            DB::table('module_rea_user')->insert(
                ['rea_user_id' => $new_user->id, 'module_id' => config('rea_user_module_id',44)]
            );
            return $new_user;
        }

    }

}
