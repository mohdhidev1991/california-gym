<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\AlertUserEvents' => [
            'App\Listeners\AlertUserListener',
        ],
        'App\Events\AlertUserByAttendanceEvent' => [
            'App\Listeners\AlertUserByAttendanceListener',
        ],
        'App\Events\AlertUserByUpdateCourseSessionEvent' => [
            'App\Listeners\AlertUserByUpdateCourseSessionListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
