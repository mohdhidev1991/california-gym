<?php
/**
 * Created by PhpStorm.
 * User: aziz
 * Date: 04/03/16
 * Time: 06:33 ص
 * Updated by achraf: 05/03/2016
 */
include 'defines.php';
return [
    'langs' => [
        Lang_Arabic => "ar",
        Lang_English => "en",
        Lang_French => "fr",
    ],
    'doc_type' => [
        'unknown' => Doc_Type_Unknown,
        'logo' => Doc_Type_Logo,
    ],
    'days' => [
        'en' => [
            1 => "Sunday",
            2 => "Monday",
            3 => "Tuesday",
            4 => "Wednesday",
            5 => "Thursday",
            6 => "Friday",
            7 => "Saturday"
        ]
    ]
];
