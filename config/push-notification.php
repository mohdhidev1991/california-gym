<?php

return array(

    'parents_iphone'     => array(
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'parents_android' => array(
        'environment' =>'development',
        'apiKey'      =>'AIzaSyB0xDWd0Ftm5o17k2gOCof3BwCUWxFEP8U',
        'service'     =>'gcm'
    )

);