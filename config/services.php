<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */


    'mandrill' => [
        'secret' => env('MANDRILL_SECRET',''),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => env('Facebook_Client_ID','1689839404609915'),
        'client_secret' => env('Facebook_Client_Secret','6feaecb14d8fccc5a68847d5feebbd00'),
        'redirect' => env('Facebook_Redirect','http://dev.reaaya.yklik.com/api/fb_callback'),
    ],

    'google' => [
        'client_id' => env('Google_Client_ID','412057407799-pv8dl6ehtmhgrl8tgks9d5ss22ev3l94.apps.googleusercontent.com'),
        'client_secret' => env('Google_Client_Secret','SPA5TFqTo2YPIES5LA6Attiu'),
        'redirect' => env('Google_Redirect','http://dev.reaaya.yklik.com/api/google_callback'),
    ],

    'twitter' => [
        'client_id' => env('Twitter_Client_ID','xtrswFARGTrNH4WSseZGKYfFC'),
        'client_secret' => env('Twitter_Client_Secret','DMjG4E9PZVZQll2VpPot5dcGjYSbvYvRTfit7Rt4MKtMOvLRQ9'),
        'redirect' => env('Twitter_Redirect','http://dev.reaaya.yklik.com/api/twitter_callback'),
    ],

    'mailgun' => array(
        'domain' => env('Mailgun_Domain','sandbox9b4bbb2702774157b890bb5a4ca026a4.mailgun.org'),
        'secret' => env('Mailgun_Secret','key-7522943cd5ae8ea0c647189d0551ccb3'),
    ),

    'sinch' => array(
        'domaine' => env('Sinch_Domain','clientapi.sinch.com'),
        'key' => env('Sinch_Key','0f9c6f6e-7210-436b-a471-b081ffbf8933'),
        'secret' => env('Sinch_Secret','pTiYIk8iUEWtOG5eTSe7bg=='),
    ),


];
