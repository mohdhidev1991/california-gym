<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRserviceStudentsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rservice_student', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->integer('validated_by');
			$table->dateTime('validated_at');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->integer('school_year_id');
			$table->integer('rservice_id');
			$table->integer('student_id');
			$table->integer('parent_user_id');
			$table->integer('payment_period_id');
			$table->string('start_hdate', 255);
			$table->string('expire_hdate', 255);
			$table->float('amount', 10, 0);
			$table->integer('currency_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rservice_student');
	}

}
