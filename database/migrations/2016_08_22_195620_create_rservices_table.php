<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRservicesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rservice', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->integer('validated_by');
			$table->dateTime('validated_at');
			$table->string('active', 255);
			$table->integer('version');
			$table->string('update_groups_mfk', 255);
			$table->string('delete_groups_mfk', 255);
			$table->string('display_groups_mfk', 255);
			$table->integer('sci_id');
			$table->string('lookup_code', 255);
			$table->string('rservice_name_ar', 255);
			$table->string('rservice_name_en', 255);
			$table->string('for_parent', 255);
			$table->string('for_school', 255);
			$table->float('price', 10, 0);
			$table->integer('currency_id');
			$table->string('payment_period_mfk', 255);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rservice');
	}

}
