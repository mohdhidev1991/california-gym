<?php

use Illuminate\Database\Seeder;
use App\Models\City;
class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::truncate();
        $city = [
            'active' => 'Y',
            'sci_id' => '0',
            'country_id' => '1',
            'city_name' => 'الرياض'
        ];
        City::create($city);
    }
}
