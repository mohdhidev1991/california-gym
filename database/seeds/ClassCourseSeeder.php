<?php

use Illuminate\Database\Seeder;
use App\Models\ClassCourse;
use App\Models\LevelClass;
use App\Models\Course;

Class ClassCourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClassCourse::truncate();
        $courses = Course::all();
        $levelClass = LevelClass::all();
        foreach($levelClass as $level){
            foreach($courses as $course)
                $classCourse[] = ['active' => 'Y','version' => '1','sci_id' => '0','course_id' => $course->id,'level_class_id' => $level->id,'class_course_name' => $level->level_class_name.'.'.$course->course_name];
        }
        ClassCourse::insert($classCourse);
    }
}
