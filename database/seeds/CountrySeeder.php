<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::truncate();
        $countries = [
            [
                'active' => 'Y',
                'sci_id' => '0',
                'time_offset' => '0',
                'maintenance_end_time' => '02:00',
                'maintenance_start_time' => '00:00',
                'date_system_id' => '0',
                'abrev' => 'SA',
                'country_name' => 'السعودية'
            ]
        ];
        Country::insert($countries);
    }
}
