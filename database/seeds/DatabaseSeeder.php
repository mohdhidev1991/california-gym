<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(LangTableSeeder::class);
        $this->call(SchoolSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(DateSystemSeeder::class);
        $this->call(GenreSeeder::class);
        $this->call(LevelClassSeeder::class);
        $this->call(PeriodSeeder::class);
        $this->call(ClassCourseSeeder::class);
        $this->call(SchoolTypeSeeder::class);
        $this->call(SchoolYearSeeder::class);
        $this->call(TserviceSeeder::class);
        $this->call(AlertSeeder::class);
        $this->call(AttendanceSeeder::class);
        $this->call(AttendanceStatusSeeder::class);
        $this->call(AttendanceTypeSeeder::class);
        $this->call(ClassCourseExamSeeder::class);
        $this->call(CourseSessionSeeder::class);
        $this->call(EFileSeeder::class);
        $this->call(ExamSessionSeeder::class);
        $this->call(IdnTypeSeeder::class);
        $this->call(ModelTermSeeder::class);
        $this->call(RatingSeeder::class);
        $this->call(SchoolClassSeeder::class);
        $this->call(SchoolLevelSeeder::class);
        $this->call(SchoolEmployeeSeeder::class);
        $this->call(SchoolMemberDaySeeder::class);
        $this->call(SchoolPeriodSeeder::class);
        $this->call(SchoolTermSeeder::class);
        $this->call(SessionStatusSeeder::class);
        $this->call(StudentExamSeeder::class);
        $this->call(StudentFileSeeder::class);
        $this->call(StudentFileStatusSeeder::class);
        $this->call(StudentSeeder::class);
        $this->call(StudentSessionSeeder::class);
    }
}
