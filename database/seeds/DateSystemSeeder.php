<?php

use Illuminate\Database\Seeder;
use App\Models\DateSystem;
class DateSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DateSystem::truncate();
        $date_system = array(
            array('id' => '1','creation_user_id' => '1','creation_date' => '2016-01-05 16:32:47','update_user_id' => '1','update_date' => '2016-01-05 16:32:47','validation_user_id' => '0','validation_date' => '2016-01-23 08:34:11','active' => 'Y','version' => '0','update_groups_mfk' => '','delete_groups_mfk' => '','display_groups_mfk' => '','sci_id' => '0','date_system_name' => 'هجري'),
            array('id' => '2','creation_user_id' => '1','creation_date' => '2016-01-05 16:32:55','update_user_id' => '1','update_date' => '2016-01-05 16:32:55','validation_user_id' => '0','validation_date' => '2016-01-23 08:34:11','active' => 'Y','version' => '0','update_groups_mfk' => '','delete_groups_mfk' => '','display_groups_mfk' => '','sci_id' => '0','date_system_name' => 'نصراني')
        );
        DateSystem::insert($date_system);
    }
}
