<?php

use Illuminate\Database\Seeder;
use App\Models\IdnType;
class IdnTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        IdnType::truncate();
        $idn_type = [];
        IdnType::insert($idn_type);
    }
}
