<?php

use Illuminate\Database\Seeder;
use App\Models\Language;
class LangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs = [
            ['lookup_code'=>"ar","lang_name"=>"العربية"],
            ['lookup_code'=>"en","lang_name"=>"English"],
        ];
        Language::insert($langs);
    }
}
