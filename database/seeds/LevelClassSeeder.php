<?php

use Illuminate\Database\Seeder;
use App\Models\LevelClass;
class LevelClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LevelClass::truncate();
        $level_class = array(
            array('id' => '1','creation_user_id' => '1','creation_date' => '2016-01-05 18:45:01','update_user_id' => '1','update_date' => '2016-01-05 18:45:01','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','school_level_id' => '1','level_class_name' => 'first'),
            array('id' => '2','creation_user_id' => '1','creation_date' => '2016-01-05 18:45:01','update_user_id' => '1','update_date' => '2016-01-05 18:45:01','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','school_level_id' => '1','level_class_name' => 'second'),
            array('id' => '3','creation_user_id' => '1','creation_date' => '2016-01-05 18:45:01','update_user_id' => '1','update_date' => '2016-01-05 18:45:01','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','school_level_id' => '1','level_class_name' => 'third'),
            array('id' => '4','creation_user_id' => '1','creation_date' => '2016-01-05 18:45:01','update_user_id' => '1','update_date' => '2016-01-05 18:45:01','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','school_level_id' => '1','level_class_name' => 'fouth'),
            array('id' => '5','creation_user_id' => '1','creation_date' => '2016-01-05 18:45:01','update_user_id' => '1','update_date' => '2016-01-05 18:45:01','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','school_level_id' => '1','level_class_name' => 'fifth'),
            array('id' => '6','creation_user_id' => '1','creation_date' => '2016-01-05 18:45:01','update_user_id' => '1','update_date' => '2016-01-05 18:45:01','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','school_level_id' => '1','level_class_name' => 'sixth')
        );
        LevelClass::insert($level_class);
    }
}
