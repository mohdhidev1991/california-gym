<?php

use Illuminate\Database\Seeder;
use App\Models\School;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::truncate();
        $school = [
            [
                'address' => 'الرياض الملز',
                'quarter' => 'الملز',
                'title' => 'المدرسة الاهلية التونسية السعودية',
                'active' => 'Y',
                'sci_id' => 0,
                'scapacity' => 500,
                'expiring_hdate' => '13:48'
            ],
            [
                'address' => 'الرياض قرطبة',
                'quarter' => 'قرطبة',
                'title' => 'المدرسة النموذجية',
                'active' => 'Y',
                'sci_id' => 0,
                'scapacity' => 500,
                'expiring_hdate' => '13:48'
            ],
            [
                'address' => 'الرياض غرناطة',
                'quarter' => 'عرناطة',
                'title' => 'مدرسة الرائد العالمية',
                'active' => 'Y',
                'sci_id' => 0,
                'scapacity' => 500,
                'expiring_hdate' => '13:48'
            ],
        ];
        School::insert($school);
    }
}
