<?php

use Illuminate\Database\Seeder;
use App\Models\SchoolType;
class SchoolTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolType::truncate();
        $school_type = array(
            array('id' => '1','creation_user_id' => '1','creation_date' => '2016-01-05 16:36:17','update_user_id' => '1','update_date' => '2016-01-05 16:36:17','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','tservice_id' => '1','school_type_name' => 'مدرسة'),
            array('id' => '2','creation_user_id' => '1','creation_date' => '2016-01-05 16:36:30','update_user_id' => '1','update_date' => '2016-01-05 16:36:30','validation_user_id' => '0','validation_date' => NULL,'active' => 'Y','version' => NULL,'update_groups_mfk' => NULL,'delete_groups_mfk' => NULL,'display_groups_mfk' => NULL,'sci_id' => '0','tservice_id' => '1','school_type_name' => 'مجموعة مدارس')
        );

        SchoolType::insert($school_type);
    }
}
