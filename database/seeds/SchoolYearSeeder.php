<?php

use Illuminate\Database\Seeder;
use App\Models\SchoolYear;
class SchoolYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolYear::truncate();
        $school_year = array(
            array('active' => 'Y','sci_id' => '0','school_year_end_hdate' => '14370610','school_year_start_hdate' => '14360915','school_id' => '1','school_year_name' => '1437-1436')
        );
        SchoolYear::insert($school_year);
    }
}
