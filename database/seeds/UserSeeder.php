<?php

use Illuminate\Database\Seeder;
use App\Models\ReaUser;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReaUser::truncate();
        $users = [
            [
                "email" => "aziz.trabelsi@gmail.com",
                "pwd"   => Hash::make("secret"),
                "mobile"=> "966591322006",
                "active"=>"Y",
                "firstname" => "عزيز",
                "f_firstname" => "الشاذلي",
                "lastname" => "الطرابلسي",
            ],
            [
                "email" => "nasriwahid90@gmail.com",
                "pwd"   => Hash::make("secret"),
                "mobile"=> "966544100545",
                "active"=>"Y",
                "firstname" => "وحيد",
                "f_firstname" => "",
                "lastname" => "نصري",
            ],
            [
                "email" => "rafiq.boubaker@gmail.com",
                "pwd"   => Hash::make("secret"),
                "mobile"=> "966598988330",
                "active"=>"Y",
                "firstname" => "رفيق",
                "f_firstname" => "",
                "lastname" => "بوبكر",
            ]
        ];
        ReaUser::insert($users);
    }
}
