'use strict';


angular.module('app', [
    
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'angularCSS',
    'ngTouch',
    'ngStorage',
    'ui.router',
    'ui-notification',
    'ui.bootstrap',
    'ui.utils',
    'ui.load',
    'ui.jq',
    'oc.lazyLoad',
    'pascalprecht.translate',
    'vcRecaptcha',
    'truncate',
    'multipleSelect',
    'textAngular',
    
]);



  
  
  
  


