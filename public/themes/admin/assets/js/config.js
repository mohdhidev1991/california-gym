// config
var app =
    angular.module('app')
    .config(
        ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function($controllerProvider, $compileProvider, $filterProvider, $provide) {

                // lazy controller, directive and service
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ])
    .config(['$translateProvider', function($translateProvider) {
        // Register a loader for the static files
        // So, the module will search missing translation tables under the specified urls.
        // Those urls are [prefix][langKey][suffix].
        /*$translateProvider.useStaticFilesLoader({
            prefix: '/themes/admin/assets/l10n/',
            suffix: '.js'
        });*/
        $translateProvider.useStaticFilesLoader({
            prefix: '/api/Translate/lang/',
            suffix: '.js'
        });
        // Tell the module what language to use by default
        $translateProvider.preferredLanguage('fr');
        // Tell the module to store the language in the local storage
        $translateProvider.useLocalStorage();
    }]);



// Service Language
app.factory('Language', function($translateProvider) {

    //add the languages you support here. ar stands for arabic
    var rtlLanguages = ['ar'];

    var isRtl = function() {
        var languageKey = $translateProvider.proposedLanguage() || $translateProvider.use();
        for (var i = 0; i < rtlLanguages.length; i += 1) {
            if (languageKey.indexOf(rtlLanguages[i]) > -1)
                return true;
        }
        return false;
    };

    //public api
    return {
        isRtl: isRtl,
        CurrentLang: isRtl,
    };
});




// Service DateSystem
app.factory('DateSystem', function($cookies) {
    var default_date_system = 1;
    var isHijriCalendar = function() {
        var rea_user = $cookies.getObject('rea_user');
        if(rea_user.date_system_id && rea_user.date_system_id===2){
            return false;
        }
        return true;
    };

    //public api
    return {
        isHijriCalendar: isHijriCalendar
    };
});






app.factory('routeTemplateMonitor', ['$rootScope',
  function($rootScope) {
    return {
      startMonitoring: function() {
        $rootScope.$on('$routeChangeSuccess', function() {
          
        });
      }
    };
  }]);





