'use strict';

/**
 * Config for the router
 */
angular.module('app')
    .run(
        ['$rootScope', '$state', '$stateParams',
            function($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;

                $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
                    
                });
            }
        ]
    )
    .config(
        ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG',
            function($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {
                


                var layout = "front/tpl/admin";
                $urlRouterProvider
                    .otherwise('/');


                $stateProvider
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: layout
                    })

                    //Routers Admin
                    .state('admin', {
                        abstract: true,
                        //url: '/admin',
                        templateUrl: layout
                    })
                    .state('admin.dashbord', {
                        url: '/',
                        templateUrl: 'front/tpl/dashbord',
                    })


                    .state('unauthorized', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('unauthorized.page', {
                        url: '/unauthorized',
                        templateUrl: 'front/tpl/cms/unauthorized'
                    })


                    //Routers ReaUser
                    .state('rea_user', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('rea_user.all', {
                        url: '/users',
                        templateUrl: 'front/tpl/rea_user/all',
                        resolve: load(['themes/admin/assets/js/controllers/rea_user/all.js']),
                        controller: 'ReaUserAllController',
                        permission: 'manage_users'
                    })
                    .state('rea_user.edit', {
                        url: '/user/edit/:rea_user_id',
                        templateUrl: 'front/tpl/rea_user/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/rea_user/form.js']),
                        controller: 'ReaUserFormController',
                        params: {action: 'edit'},
                        permission: 'manage_users'
                    })
                    .state('rea_user.add', {
                        url: '/user/add',
                        templateUrl: 'front/tpl/rea_user/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/rea_user/form.js']),
                        controller: 'ReaUserFormController',
                        params: {action: 'add'},
                        permission: 'manage_users'
                    })


                  


                    //Routers Role
                    .state('role', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('role.all', {
                        url: '/roles',
                        templateUrl: 'front/tpl/role/all',
                        resolve: load(['themes/admin/assets/js/controllers/role/all.js']),
                        controller: 'RoleAllController',
                        permission: 'manage_roles'
                    })
                    .state('role.edit', {
                        url: '/role/edit/:role_id',
                        templateUrl: 'front/tpl/role/form',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/role/form.js']),
                        controller: 'RoleFormController',
                        params: {action: 'edit'},
                        permission: 'manage_roles'
                    })
                    .state('role.add', {
                        url: '/role/add',
                        templateUrl: 'front/tpl/role/form',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/role/form.js']),
                        controller: 'RoleFormController',
                        params: {action: 'add'},
                        permission: 'manage_roles'
                    })
                    .state('role.trtrtr', {
                        url: '/role/trtrtr/1',
                        templateUrl: 'front/tpl/role/form',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/role/form.js']),
                        controller: 'RoleFormController',
                        params: {action: 'trtrtr'},
                        permission: 'manage_roles'
                    })

                    //Routers Test
                    .state('test', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('test.all', {
                        url: '/test',
                        templateUrl: 'front/tpl/test/all',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/test/all.js']),
                        controller: 'TestAllController',
                        
                    })
                    .state('test.edit',{
                        url: '/test/edit/:test_id',
                        templateUrl: 'front/tpl/test/form',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/test/form.js']),
                        controller: 'TestFormController',
                        params: {action: 'edit'},
                    })
                    .state('test.add', {
                        url: '/test/add',
                        templateUrl: 'front/tpl/test/form',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/test/form.js']),
                        controller: 'TestFormController',
                        params: {action: 'add'},
                        
                    })
                   
                    
                    //Routers Room
                    .state('setting', {
                        abstract: true,
                        templateUrl: layout
                    })
                    

                    /* Coach Crud */
                    .state('setting.coach', {
                        abstract: true,
                        template: '<div ui-view></div>'
                    })
                    
                    .state('setting.coach.all', {
                        url: '/setting/coachs',
                        templateUrl: 'front/tpl/coach/all',
                        resolve: load(['themes/admin/assets/js/controllers/coach/all.js']),
                        controller: 'CoachAllController',
                        permission: 'manage_coaches'
                    })
                    
                    .state('setting.coach.edit', {
                        url: '/setting/coach/edit/:coach_id',
                        templateUrl: 'front/tpl/coach/form',
                        resolve: load(['moment', 'angularFileUpload', 'checkbox_helper', 'themes/admin/assets/js/controllers/coach/form.js']),
                        controller: 'CoachFormController',
                        params: {action: 'edit'},
                        permission: 'manage_coaches'
                    })
                    
                    .state('setting.coach.add', {
                        url: '/setting/coach/add',
                        templateUrl: 'front/tpl/coach/form',
                        resolve: load(['moment', 'angularFileUpload', 'checkbox_helper', 'themes/admin/assets/js/controllers/coach/form.js']),
                        controller: 'CoachFormController',
                        params: {action: 'add'},
                        permission: 'manage_coaches'
                    })




                    /* Club Crud */
                    .state('setting.club', {
                        abstract: true,
                        template: '<div ui-view></div>'
                    })
                    .state('setting.club.all', {
                        url: '/setting/clubs',
                        templateUrl: 'front/tpl/club/all',
                        resolve: load(['themes/admin/assets/js/controllers/club/all.js']),
                        controller: 'ClubAllController',
                        permission: 'manage_club'
                    })
                    .state('setting.club.edit', {
                        url: '/setting/club/edit/:club_id',
                        templateUrl: 'front/tpl/club/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/club/form.js']),
                        controller: 'ClubFormController',
                        params: {action: 'edit'},
                        permission: 'manage_club'
                    })
                    .state('setting.club.add', {
                        url: '/setting/club/add',
                        templateUrl: 'front/tpl/club/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/club/form.js']),
                        controller: 'ClubFormController',
                        params: {action: 'add'},
                        permission: 'manage_club'
                    })


                    /* Room Crud */
                    .state('setting.club.room', {
                        abstract: true,
                        template: '<div ui-view></div>'
                    })
                    .state('setting.club.rooms', {
                        url: '/setting/club/:club_id/rooms',
                        templateUrl: 'front/tpl/room/all',
                        resolve: load(['themes/admin/assets/js/controllers/room/all.js']),
                        controller: 'RoomAllController',
                        permission: 'manage_rooms'
                    })
                    .state('setting.club.room.edit', {
                        url: '/setting/club/:club_id/room/edit/:room_id',
                        templateUrl: 'front/tpl/room/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/room/form.js']),
                        controller: 'RoomFormController',
                        params: {action: 'edit'},
                        permission: 'manage_rooms'
                    })
                    .state('setting.club.room.add', {
                        url: '/setting/club/:club_id/room/add',
                        templateUrl: 'front/tpl/room/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/room/form.js']),
                        controller: 'RoomFormController',
                        params: {action: 'add'},
                        permission: 'manage_rooms'
                    })




                    //Routers Course
                    .state('setting.course', {
                        abstract: true,
                        template: '<div ui-view></div>'
                    })
                    .state('setting.course.all', {
                        url: '/setting/courses',
                        templateUrl: 'front/tpl/course/all',
                        resolve: load(['themes/admin/assets/js/controllers/course/all.js']),
                        controller: 'CourseAllController',
                        permission: 'manage_cours'
                    })
                    .state('setting.course.edit', {
                        url: '/setting/course/edit/:course_id',
                        templateUrl: 'front/tpl/course/form',
                        resolve: load(['angularFileUpload', 'checkbox_helper', 'colorpicker.module', 'themes/admin/assets/js/controllers/course/form.js']),
                        controller: 'CourseFormController',
                        params: {action: 'edit'},
                        permission: 'manage_cours'
                    })
                    .state('setting.course.add', {
                        url: '/setting/course/add',
                        templateUrl: 'front/tpl/course/form',
                        resolve: load(['angularFileUpload', 'checkbox_helper', 'colorpicker.module', 'themes/admin/assets/js/controllers/course/form.js']),
                        controller: 'CourseFormController',
                        params: {action: 'add'},
                        permission: 'manage_cours'
                    })




                    //Routers CourseType
                    .state('setting.course_type', {
                        abstract: true,
                        template: '<div ui-view></div>'
                    })
                    .state('setting.course_type.all', {
                        url: '/setting/course_types',
                        templateUrl: 'front/tpl/course_type/all',
                        resolve: load(['themes/admin/assets/js/controllers/course_type/all.js']),
                        controller: 'CourseTypeAllController',
                        permission: 'manage_cours_type'
                    })
                    .state('setting.course_type.edit', {
                        url: '/setting/course_type/edit/:course_type_id',
                        templateUrl: 'front/tpl/course_type/form',
                        resolve: load(['angularFileUpload', 'checkbox_helper', 'colorpicker.module', 'themes/admin/assets/js/controllers/course_type/form.js']),
                        controller: 'CourseTypeFormController',
                        params: {action: 'edit'},
                        permission: 'manage_cours_type'
                    })
                    .state('setting.course_type.add', {
                        url: '/setting/course_type/add',
                        templateUrl: 'front/tpl/course_type/form',
                        resolve: load(['angularFileUpload', 'checkbox_helper', 'colorpicker.module', 'themes/admin/assets/js/controllers/course_type/form.js']),
                        controller: 'CourseTypeFormController',
                        params: {action: 'add'},
                        permission: 'manage_cours_type'
                    })



                    //Routers Holidays
                    .state('setting.holidays', {
                        url: '/setting/holidays',
                        templateUrl: 'front/tpl/holiday/manage',
                        resolve: load(['moment', 'fullcalendar', 'ui.calendar', 'themes/admin/assets/libs/jquery/fullcalendar/dist/lang/fr.js', 'themes/admin/assets/js/controllers/holiday/manage.js']),
                        controller: 'HolidayManageController',
                        permission: 'manage_holidays'
                    })



                    //Routers Notifications
                    .state('setting.send_emails', {
                        url: '/setting/send-emails',
                        templateUrl: 'front/tpl/setting/send_emails',
                        resolve: load(['moment', 'ui.calendar', 'themes/admin/assets/js/controllers/setting/send_emails.js']),
                        controller: 'SettingSendEmailsManageController',
                        permission: 'manage_settings'
                    })




                    //Routers ReaUser
                    .state('planning', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('planning.manage', {
                        url: '/planning/manage',
                        templateUrl: 'front/tpl/planning/manage',
                        resolve: load(['moment', 'fullcalendar', 'ui.calendar', 'themes/admin/assets/libs/jquery/fullcalendar/dist/lang/fr.js','themes/admin/assets/js/controllers/planning/manage.js']),
                        controller: 'PlanningManageController',
                        permission: 'manage_planning'
                    })

                    .state('planning.manage_list_calendar', {
                        url: '/planning/manage_list_calendar',
                        templateUrl: 'front/tpl/planning/manage_list_calendar',
                        resolve: load(['moment', 'fullcalendar', 'ui.calendar', 'themes/admin/assets/libs/jquery/fullcalendar/dist/lang/fr.js','themes/admin/assets/js/controllers/planning/manage_list_calendar.js']),
                        controller: 'PlanningListRoomManageController',
                        permission: 'manage_planning'
                    })
                    
                    .state('planning.manage_coach', {
                        url: '/planning/manage_coach',
                        templateUrl: 'front/tpl/planning/manage_coach',
                        resolve: load(['moment', 'fullcalendar', 'ui.calendar', 'themes/admin/assets/libs/jquery/fullcalendar/dist/lang/fr.js','themes/admin/assets/js/controllers/planning/manage_coach.js']),
                        controller: 'PlanningManageCoachController',
                        
                    })
                    .state('planning.stats', {
                        url: '/planning/stats',
                        templateUrl: 'front/tpl/planning/stats',
                        resolve: load(['moment', 'daterangepicker','Chart_js', 'chart.js', 'themes/admin/assets/js/controllers/planning/stats.js']),
                        controller: 'PlanningStatsController',
                        permission: 'read_stats'
                    })

                    .state('planning.exportgrh', {
                        url: '/planning/exportgrh',
                        templateUrl: 'front/tpl/planning/exportgrh',
                        resolve: load(['moment', 'daterangepicker','Chart_js', 'chart.js', 'themes/admin/assets/js/controllers/planning/exportgrh.js']),
                        controller: 'PlanningExportGRHController',
                        
                    })

                    .state('planning.history', {
                        url: '/planning/history',
                        templateUrl: 'front/tpl/planning/history',
                        resolve: load(['moment', 'fullcalendar', 'ui.calendar', 'themes/admin/assets/libs/jquery/fullcalendar/dist/lang/fr.js','themes/admin/assets/js/controllers/planning/history.js']),
                        controller: 'PlanningHistoryController',
                        permission: 'read_planning'
                    })


                    //Routers Translate
                    .state('translate', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('translate.all', {
                        url: '/translates',
                        templateUrl: 'front/tpl/translate/all',
                        resolve: load(['themes/admin/assets/js/controllers/translate/all.js']),
                        controller: 'TranslateAllController',
                        permission: 'manage_settings'
                    })









                    //Routers CourseSession
                    .state('course_session', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('course_session.timeline', {
                        url: '/course_session/timeline',
                        templateUrl: 'front/tpl/course_session/timeline',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/course_session/timeline.js']),
                        controller: 'CourseSessionTimelineController'
                    })
                    .state('course_session.timeline_fromdate', {
                        url: '/course_session/timeline/:date',
                        templateUrl: 'front/tpl/course_session/timeline',
                        resolve: load(['moment',  'themes/admin/assets/js/controllers/course_session/timeline.js']),
                        controller: 'CourseSessionTimelineController'
                    })
                    .state('course_session.open', {
                        url: '/course_session/open/:year/:hmonth/:hday_num/:level_class_id/:symbol/:session_order',
                        templateUrl: 'front/tpl/course_session/open',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/course_session/open.js']),
                        controller: 'CourseSessionOpenController'
                    })
                    .state('course_session.current', {
                        url: '/course_session/current',
                        templateUrl: 'front/tpl/course_session/current',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/course_session/current.js']),
                        controller: 'CourseSessionCurrentController'
                    })
                    .state('course_session.current_with_date', {
                        url: '/course_session/current/:date',
                        templateUrl: 'front/tpl/course_session/current',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/course_session/current.js']),
                        controller: 'CourseSessionCurrentController'
                    })
                    .state('course_session.edit_current', {
                        url: '/course_session/edit/:date',
                        templateUrl: 'front/tpl/course_session/edit_current',
                        resolve: load(['moment',  'themes/admin/assets/js/controllers/course_session/edit_current.js']),
                        controller: 'CourseSessionEditCurrentController'
                    })
                    .state('course_session.manage', {
                        url: '/course_session/manage/',
                        templateUrl: 'front/tpl/course_session/manage',
                        resolve: load(['moment',  'themes/admin/assets/js/controllers/course_session/manage.js']),
                        controller: 'CourseSessionManageController'
                    })
                    .state('course_session.manage_item', {
                        url: '/course_session/manage/item/:year/:hmonth/:hday_num/:level_class_id/:symbol/:session_order',
                        templateUrl: 'front/tpl/course_session/manage_item',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/course_session/manage_item.js']),
                        controller: 'CourseSessionManageItemController'
                    })


                    //Routers CourseSessionFin
                    .state('course_session_fin', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('course_session_fin.request_cancel_cours', {
                        url: '/course_session_fin/request_cancel_cours',
                        templateUrl: 'front/tpl/course_session_fin/request_cancel_cours',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/course_session_fin/request_cancel_cours.js']),
                        controller: 'RequestCancelCoursController',
                        
                    })
                    .state('course_session_fin.replace_coach', {
                        url: '/course_session_fin/replace_coach',
                        templateUrl: 'front/tpl/course_session_fin/replace_coach',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/course_session_fin/replace_coach.js']),
                        controller: 'ReplaceCoachController',
                    })
                    .state('course_session_fin.nbr_paritcipants_cours', {
                        url: '/course_session_fin/nbr_paritcipants_cours',
                        templateUrl: 'front/tpl/course_session_fin/nbr_paritcipants_cours',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/course_session_fin/nbr_paritcipants_cours.js']),
                        controller: 'NbrParticipantsController',
                    })
                    

                    .state('courses_config_item', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('courses_config_item.select_template', {
                        url: '/courses/config/template',
                        templateUrl: 'front/tpl/courses_config_item/select_template',
                        resolve: load(['themes/admin/assets/js/controllers/courses_config_item/select_template.js']),
                        controller: 'CoursesConfigItemSelectTemplateController'
                    })
                    .state('courses_config_item.config_item', {
                        url: '/courses/config/item',
                        templateUrl: 'front/tpl/courses_config_item/config_item',
                        resolve: load(['themes/admin/assets/js/controllers/courses_config_item/config_item.js']),
                        controller: 'CoursesConfigItemConfigItemController'
                    })


                    .state('school_level', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_level.config_school_level', {
                        url: '/school_level/config',
                        templateUrl: 'front/tpl/school_level/config_school_level',
                        resolve: load(['themes/admin/assets/js/controllers/school_level/config_school_level.js']),
                        controller: 'SchoolLevelConfigController'
                    })
                    .state('school_level.all', {
                        url: '/school_levels',
                        templateUrl: 'front/tpl/school_level/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_level/all.js']),
                        controller: 'SchoolLevelAllController'
                    })
                    .state('school_level.edit', {
                        url: '/school_level/edit/:school_level_id',
                        templateUrl: 'front/tpl/school_level/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_level/form.js']),
                        controller: 'SchoolLevelFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_level.add', {
                        url: '/school_level/add',
                        templateUrl: 'front/tpl/school_level/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_level/form.js']),
                        controller: 'SchoolLevelFormController',
                        params: {action: 'add'}
                    })


                    //Routers School
                    .state('school', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school.all', {
                        url: '/schools',
                        templateUrl: 'front/tpl/school/all',
                        resolve: load(['themes/admin/assets/js/controllers/school/all.js']),
                        controller: 'SchoolAllController'
                    })
                    .state('school.edit', {
                        url: '/school/edit/:school_id',
                        templateUrl: 'front/tpl/school/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/school/form.js']),
                        controller: 'SchoolFormController',
                        params: {action: 'edit'}
                    })
                    .state('school.add', {
                        url: '/school/add',
                        templateUrl: 'front/tpl/school/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/school/form.js']),
                        controller: 'SchoolFormController',
                        params: {action: 'add'}
                    })
                    .state('school.course_sched_generator', {
                        url: '/school/course_sched_generator',
                        templateUrl: 'front/tpl/school/course_sched_generator',
                        resolve: load(['themes/admin/assets/js/controllers/school/course_sched_generator.js']),
                        controller: 'SchoolCourseSchedGeneratorController'
                    })
                    .state('school.infos', {
                        url: '/school/infos/:school_id',
                        templateUrl: 'front/tpl/school/infos',
                        resolve: load(['themes/admin/assets/js/controllers/school/infos.js']),
                        controller: 'SchoolInfosController'
                    })





                    //Routers Language
                    .state('language', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('language.all', {
                        url: '/languages',
                        templateUrl: 'front/tpl/language/all',
                        resolve: load(['themes/admin/assets/js/controllers/language/all.js']),
                        controller: 'LanguageAllController'
                    })
                    .state('language.edit', {
                        url: '/language/edit/:language_id',
                        templateUrl: 'front/tpl/language/form',
                        resolve: load(['themes/admin/assets/js/controllers/language/form.js']),
                        controller: 'LanguageFormController',
                        params: {action: 'edit'}
                    })
                    .state('language.add', {
                        url: '/language/add',
                        templateUrl: 'front/tpl/language/form',
                        resolve: load(['themes/admin/assets/js/controllers/language/form.js']),
                        controller: 'LanguageFormController',
                        params: {action: 'add'}
                    })






                    //Routers Period
                    .state('period', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('period.all', {
                        url: '/periods',
                        templateUrl: 'front/tpl/period/all',
                        resolve: load(['themes/admin/assets/js/controllers/period/all.js']),
                        controller: 'PeriodAllController'
                    })
                    .state('period.edit', {
                        url: '/period/edit/:period_id',
                        templateUrl: 'front/tpl/period/form',
                        resolve: load(['themes/admin/assets/js/controllers/period/form.js']),
                        controller: 'PeriodFormController',
                        params: {action: 'edit'}
                    })
                    .state('period.add', {
                        url: '/period/add',
                        templateUrl: 'front/tpl/period/form',
                        resolve: load(['themes/admin/assets/js/controllers/period/form.js']),
                        controller: 'PeriodFormController',
                        params: {action: 'add'}
                    })


                    //Routers Gremark
                    .state('gremark', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('gremark.all', {
                        url: '/gremarks',
                        templateUrl: 'front/tpl/gremark/all',
                        resolve: load(['themes/admin/assets/js/controllers/gremark/all.js']),
                        controller: 'GremarkAllController'
                    })
                    .state('gremark.edit', {
                        url: '/gremark/edit/:gremark_id',
                        templateUrl: 'front/tpl/gremark/form',
                        resolve: load(['themes/admin/assets/js/controllers/gremark/form.js']),
                        controller: 'GremarkFormController',
                        params: {action: 'edit'}
                    })
                    .state('gremark.add', {
                        url: '/gremark/add',
                        templateUrl: 'front/tpl/gremark/form',
                        resolve: load(['themes/admin/assets/js/controllers/gremark/form.js']),
                        controller: 'GremarkFormController',
                        params: {action: 'add'}
                    })





                    //Routers Remark
                    .state('remark', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('remark.all', {
                        url: '/remarks',
                        templateUrl: 'front/tpl/remark/all',
                        resolve: load(['themes/admin/assets/js/controllers/remark/all.js']),
                        controller: 'RemarkAllController'
                    })
                    .state('remark.edit', {
                        url: '/remark/edit/:remark_id',
                        templateUrl: 'front/tpl/remark/form',
                        resolve: load(['themes/admin/assets/js/controllers/remark/form.js']),
                        controller: 'RemarkFormController',
                        params: {action: 'edit'}
                    })
                    .state('remark.add', {
                        url: '/remark/add',
                        templateUrl: 'front/tpl/remark/form',
                        resolve: load(['themes/admin/assets/js/controllers/remark/form.js']),
                        controller: 'RemarkFormController',
                        params: {action: 'add'}
                    })



                    //Routers LevelsTemplate
                    .state('levels_template', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('levels_template.all', {
                        url: '/levels_templates',
                        templateUrl: 'front/tpl/levels_template/all',
                        resolve: load(['themes/admin/assets/js/controllers/levels_template/all.js']),
                        controller: 'LevelsTemplateAllController'
                    })
                    .state('levels_template.edit', {
                        url: '/levels_template/edit/:levels_template_id',
                        templateUrl: 'front/tpl/levels_template/form',
                        resolve: load(['themes/admin/assets/js/controllers/levels_template/form.js']),
                        controller: 'LevelsTemplateFormController',
                        params: {action: 'edit'}
                    })
                    .state('levels_template.add', {
                        url: '/levels_template/add',
                        templateUrl: 'front/tpl/levels_template/form',
                        resolve: load(['themes/admin/assets/js/controllers/levels_template/form.js']),
                        controller: 'LevelsTemplateFormController',
                        params: {action: 'add'}
                    })




                    //Routers Country
                    .state('country', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('country.all', {
                        url: '/countries',
                        templateUrl: 'front/tpl/country/all',
                        resolve: load(['themes/admin/assets/js/controllers/country/all.js']),
                        controller: 'CountryAllController'
                    })
                    .state('country.edit', {
                        url: '/country/edit/:country_id',
                        templateUrl: 'front/tpl/country/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/country/form.js']),
                        controller: 'CountryFormController',
                        params: {action: 'edit'}
                    })
                    .state('country.add', {
                        url: '/country/add',
                        templateUrl: 'front/tpl/country/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/country/form.js']),
                        controller: 'CountryFormController',
                        params: {action: 'add'}
                    })



                    //Routers City
                    .state('city', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('city.all', {
                        url: '/cities',
                        templateUrl: 'front/tpl/city/all',
                        resolve: load(['themes/admin/assets/js/controllers/city/all.js']),
                        controller: 'CityAllController'
                    })
                    .state('city.edit', {
                        url: '/city/edit/:city_id',
                        templateUrl: 'front/tpl/city/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/city/form.js']),
                        controller: 'CityFormController',
                        params: {action: 'edit'}
                    })
                    .state('city.add', {
                        url: '/city/add',
                        templateUrl: 'front/tpl/city/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/city/form.js']),
                        controller: 'CityFormController',
                        params: {action: 'add'}
                    })














                    //Routers SchoolYear
                    .state('school_year', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_year.all', {
                        url: '/school_years',
                        templateUrl: 'front/tpl/school_year/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_year/all.js']),
                        controller: 'SchoolYearAllController'
                    })
                    .state('school_year.edit', {
                        url: '/school_year/edit/:school_year_id',
                        templateUrl: 'front/tpl/school_year/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/school_year/form.js']),
                        controller: 'SchoolYearFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_year.add', {
                        url: '/school_year/add',
                        templateUrl: 'front/tpl/school_year/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/school_year/form.js']),
                        controller: 'SchoolYearFormController',
                        params: {action: 'add'}
                    })
                    .state('school_year.infos', {
                        url: '/school_year/infos/:school_year_id',
                        templateUrl: 'front/tpl/school_year/infos',
                        resolve: load(['themes/admin/assets/js/controllers/school_year/infos.js']),
                        controller: 'SchoolYearInfosController',
                    })




                    //Routers SchoolClass
                    .state('school_class', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_class.all', {
                        url: '/school_classes',
                        templateUrl: 'front/tpl/school_class/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_class/all.js']),
                        controller: 'SchoolClassAllController'
                    })
                    .state('school_class.edit', {
                        url: '/school_class/edit/:school_class_id',
                        templateUrl: 'front/tpl/school_class/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/school_class/form.js']),
                        controller: 'SchoolClassFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_class.add', {
                        url: '/school_class/add',
                        templateUrl: 'front/tpl/school_class/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/school_class/form.js']),
                        controller: 'SchoolClassFormController',
                        params: {action: 'add'}
                    })



                    //Routers Sdepartment
                    .state('sdepartment', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('sdepartment.all', {
                        url: '/sdepartments',
                        templateUrl: 'front/tpl/sdepartment/all',
                        resolve: load(['themes/admin/assets/js/controllers/sdepartment/all.js']),
                        controller: 'SdepartmentAllController'
                    })
                    .state('sdepartment.edit', {
                        url: '/sdepartment/edit/:sdepartment_id',
                        templateUrl: 'front/tpl/sdepartment/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/sdepartment/form.js']),
                        controller: 'SdepartmentFormController',
                        params: {action: 'edit'}
                    })
                    .state('sdepartment.add', {
                        url: '/sdepartment/add',
                        templateUrl: 'front/tpl/sdepartment/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/sdepartment/form.js']),
                        controller: 'SdepartmentFormController',
                        params: {action: 'add'}
                    })



                    //Routers TService
                    .state('tservice', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('tservice.all', {
                        url: '/tservices',
                        templateUrl: 'front/tpl/tservice/all',
                        resolve: load(['themes/admin/assets/js/controllers/tservice/all.js']),
                        controller: 'TServiceAllController'
                    })
                    .state('tservice.edit', {
                        url: '/tservice/edit/:tservice_id',
                        templateUrl: 'front/tpl/tservice/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/tservice/form.js']),
                        controller: 'TServiceFormController',
                        params: {action: 'edit'}
                    })
                    .state('tservice.add', {
                        url: '/tservice/add',
                        templateUrl: 'front/tpl/tservice/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/tservice/form.js']),
                        controller: 'TServiceFormController',
                        params: {action: 'add'}
                    })

                        

                    //Routers WeekTemplate
                    .state('week_template', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('week_template.all', {
                        url: '/week_templates',
                        templateUrl: 'front/tpl/week_template/all',
                        resolve: load(['themes/admin/assets/js/controllers/week_template/all.js']),
                        controller: 'WeekTemplateAllController'
                    })
                    .state('week_template.edit', {
                        url: '/week_template/edit/:week_template_id',
                        templateUrl: 'front/tpl/week_template/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/week_template/form.js']),
                        controller: 'WeekTemplateFormController',
                        params: {action: 'edit'}
                    })
                    .state('week_template.add', {
                        url: '/week_template/add',
                        templateUrl: 'front/tpl/week_template/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/week_template/form.js']),
                        controller: 'WeekTemplateFormController',
                        params: {action: 'add'}
                    })
                        

                    //Routers AlertType
                    .state('alert_type', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('alert_type.all', {
                        url: '/AlertTypes',
                        templateUrl: 'front/tpl/alert_type/all',
                        resolve: load(['themes/admin/assets/js/controllers/alert_type/all.js']),
                        controller: 'AlertTypeAllController'
                    })
                    .state('alert_type.edit', {
                        url: '/AlertType/edit/:alert_type_id',
                        templateUrl: 'front/tpl/alert_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/alert_type/form.js']),
                        controller: 'AlertTypeFormController',
                        params: {action: 'edit'}
                    })
                    .state('alert_type.add', {
                        url: '/AlertType/add',
                        templateUrl: 'front/tpl/alert_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/alert_type/form.js']),
                        controller: 'AlertTypeFormController',
                        params: {action: 'add'}
                    })





                    //Routers AlertStatus
                    .state('alert_status', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('alert_status.all', {
                        url: '/alert_status',
                        templateUrl: 'front/tpl/alert_status/all',
                        resolve: load(['themes/admin/assets/js/controllers/alert_status/all.js']),
                        controller: 'AlertStatusAllController'
                    })
                    .state('alert_status.edit', {
                        url: '/AlertStatus/edit/:alert_status_id',
                        templateUrl: 'front/tpl/alert_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/alert_status/form.js']),
                        controller: 'AlertStatusFormController',
                        params: {action: 'edit'}
                    })
                    .state('alert_status.add', {
                        url: '/AlertStatus/add',
                        templateUrl: 'front/tpl/alert_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/alert_status/form.js']),
                        controller: 'AlertStatusFormController',
                        params: {action: 'add'}
                    })


                        
                    //Routers Attendance
                    .state('attendance', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('attendance.all', {
                        url: '/Attendances',
                        templateUrl: 'front/tpl/attendance/all',
                        resolve: load(['themes/admin/assets/js/controllers/attendance/all.js']),
                        controller: 'AttendanceAllController'
                    })
                    .state('attendance.edit', {
                        url: '/Attendance/edit/:attendance_id',
                        templateUrl: 'front/tpl/attendance/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/attendance/form.js']),
                        controller: 'AttendanceFormController',
                        params: {action: 'edit'}
                    })
                    .state('attendance.add', {
                        url: '/Attendance/add',
                        templateUrl: 'front/tpl/attendance/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/attendance/form.js']),
                        controller: 'AttendanceFormController',
                        params: {action: 'add'}
                    })

                    //Routers AttendanceStatus
                    .state('attendance_status', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('attendance_status.all', {
                        url: '/AttendanceStatus',
                        templateUrl: 'front/tpl/attendance_status/all',
                        resolve: load(['themes/admin/assets/js/controllers/attendance_status/all.js']),
                        controller: 'AttendanceStatusAllController'
                    })
                    .state('attendance_status.edit', {
                        url: '/AttendanceStatus/edit/:attendance_status_id',
                        templateUrl: 'front/tpl/attendance_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/attendance_status/form.js']),
                        controller: 'AttendanceStatusFormController',
                        params: {action: 'edit'}
                    })
                    .state('attendance_status.add', {
                        url: '/AttendanceStatus/add',
                        templateUrl: 'front/tpl/attendance_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/attendance_status/form.js']),
                        controller: 'AttendanceStatusFormController',
                        params: {action: 'add'}
                    })

                        
                    //Routers AttendanceType
                    .state('attendance_type', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('attendance_type.all', {
                        url: '/attendance_types',
                        templateUrl: 'front/tpl/attendance_type/all',
                        resolve: load(['themes/admin/assets/js/controllers/attendance_type/all.js']),
                        controller: 'AttendanceTypeAllController'
                    })
                    .state('attendance_type.edit', {
                        url: '/attendance_type/edit/:attendance_type_id',
                        templateUrl: 'front/tpl/attendance_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/attendance_type/form.js']),
                        controller: 'AttendanceTypeFormController',
                        params: {action: 'edit'}
                    })
                    .state('attendance_type.add', {
                        url: '/attendance_type/add',
                        templateUrl: 'front/tpl/attendance_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/attendance_type/form.js']),
                        controller: 'AttendanceTypeFormController',
                        params: {action: 'add'}
                    })
                        
                    //Routers SchoolJob
                    .state('school_job', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_job.all', {
                        url: '/SchoolJobs',
                        templateUrl: 'front/tpl/school_job/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_job/all.js']),
                        controller: 'SchoolJobAllController'
                    })
                    .state('school_job.edit', {
                        url: '/SchoolJob/edit/:school_job_id',
                        templateUrl: 'front/tpl/school_job/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_job/form.js']),
                        controller: 'SchoolJobFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_job.add', {
                        url: '/SchoolJob/add',
                        templateUrl: 'front/tpl/school_job/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_job/form.js']),
                        controller: 'SchoolJobFormController',
                        params: {action: 'add'}
                    })
                        
                        
                    //Routers SchoolType
                    .state('school_type', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_type.all', {
                        url: '/SchoolTypes',
                        templateUrl: 'front/tpl/school_type/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_type/all.js']),
                        controller: 'SchoolTypeAllController'
                    })
                    .state('school_type.edit', {
                        url: '/SchoolType/edit/:school_type_id',
                        templateUrl: 'front/tpl/school_type/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/school_type/form.js']),
                        controller: 'SchoolTypeFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_type.add', {
                        url: '/SchoolType/add',
                        templateUrl: 'front/tpl/school_type/form',
                        resolve: load([ 'moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/school_type/form.js']),
                        controller: 'SchoolTypeFormController',
                        params: {action: 'add'}
                    })




                    //Routers SchoolPeriod
                    .state('school_period', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_period.all', {
                        url: '/school_periods',
                        templateUrl: 'front/tpl/school_period/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_period/all.js']),
                        controller: 'SchoolPeriodAllController'
                    })
                    .state('school_period.edit', {
                        url: '/school_period/edit/:school_period_id',
                        templateUrl: 'front/tpl/school_period/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/school_period/form.js']),
                        controller: 'SchoolPeriodFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_period.add', {
                        url: '/school_period/add',
                        templateUrl: 'front/tpl/school_period/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/school_period/form.js']),
                        controller: 'SchoolPeriodFormController',
                        params: {action: 'add'}
                    })





                    //Routers MobileType
                    .state('mobile_type', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('mobile_type.all', {
                        url: '/mobile_types',
                        templateUrl: 'front/tpl/mobile_type/all',
                        resolve: load(['themes/admin/assets/js/controllers/mobile_type/all.js']),
                        controller: 'MobileTypeAllController'
                    })
                    .state('mobile_type.edit', {
                        url: '/mobile_type/edit/:mobile_type_id',
                        templateUrl: 'front/tpl/mobile_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/mobile_type/form.js']),
                        controller: 'MobileTypeFormController',
                        params: {action: 'edit'}
                    })
                    .state('mobile_type.add', {
                        url: '/mobile_type/add',
                        templateUrl: 'front/tpl/mobile_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/mobile_type/form.js']),
                        controller: 'MobileTypeFormController',
                        params: {action: 'add'}
                    })




                    //Routers DateSystem
                    .state('date_system', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('date_system.all', {
                        url: '/dates_system',
                        templateUrl: 'front/tpl/date_system/all',
                        resolve: load(['themes/admin/assets/js/controllers/date_system/all.js']),
                        controller: 'DateSystemAllController'
                    })
                    .state('date_system.edit', {
                        url: '/DateSystem/edit/:date_system_id',
                        templateUrl: 'front/tpl/date_system/form',
                        resolve: load(['themes/admin/assets/js/controllers/date_system/form.js']),
                        controller: 'DateSystemFormController',
                        params: {action: 'edit'}
                    })
                    .state('date_system.add', {
                        url: '/DateSystem/add',
                        templateUrl: 'front/tpl/date_system/form',
                        resolve: load(['themes/admin/assets/js/controllers/date_system/form.js']),
                        controller: 'DateSystemFormController',
                        params: {action: 'add'}
                    })



                    //Routers Genre
                    .state('genre', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('genre.all', {
                        url: '/genres',
                        templateUrl: 'front/tpl/genre/all',
                        resolve: load(['themes/admin/assets/js/controllers/genre/all.js']),
                        controller: 'GenreAllController'
                    })
                    .state('genre.edit', {
                        url: '/genre/edit/:genre_id',
                        templateUrl: 'front/tpl/genre/form',
                        resolve: load(['themes/admin/assets/js/controllers/genre/form.js']),
                        controller: 'GenreFormController',
                        params: {action: 'edit'}
                    })
                    .state('genre.add', {
                        url: '/genre/add',
                        templateUrl: 'front/tpl/genre/form',
                        resolve: load(['themes/admin/assets/js/controllers/genre/form.js']),
                        controller: 'GenreFormController',
                        params: {action: 'add'}
                    })





                    //Routers IdnType
                    .state('idn_type', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('idn_type.all', {
                        url: '/idn_types',
                        templateUrl: 'front/tpl/idn_type/all',
                        resolve: load(['themes/admin/assets/js/controllers/idn_type/all.js']),
                        controller: 'IdnTypeAllController'
                    })
                    .state('idn_type.edit', {
                        url: '/idn_type/edit/:idn_type_id',
                        templateUrl: 'front/tpl/idn_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/idn_type/form.js']),
                        controller: 'IdnTypeFormController',
                        params: {action: 'edit'}
                    })
                    .state('idn_type.add', {
                        url: '/idn_type/add',
                        templateUrl: 'front/tpl/idn_type/form',
                        resolve: load(['themes/admin/assets/js/controllers/idn_type/form.js']),
                        controller: 'IdnTypeFormController',
                        params: {action: 'add'}
                    })




                    //Routers SessionStatus
                    .state('session_status', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('session_status.all', {
                        url: '/session_statuss',
                        templateUrl: 'front/tpl/session_status/all',
                        resolve: load(['themes/admin/assets/js/controllers/session_status/all.js']),
                        controller: 'SessionStatusAllController'
                    })
                    .state('session_status.edit', {
                        url: '/session_status/edit/:session_status_id',
                        templateUrl: 'front/tpl/session_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/session_status/form.js']),
                        controller: 'SessionStatusFormController',
                        params: {action: 'edit'}
                    })
                    .state('session_status.add', {
                        url: '/session_status/add',
                        templateUrl: 'front/tpl/session_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/session_status/form.js']),
                        controller: 'SessionStatusFormController',
                        params: {action: 'add'}
                    })






                    //Routers Holiday
                    .state('holiday', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('holiday.all', {
                        url: '/holidays',
                        templateUrl: 'front/tpl/holiday/all',
                        resolve: load(['themes/admin/assets/js/controllers/holiday/all.js']),
                        controller: 'HolidayAllController'
                    })
                    .state('holiday.edit', {
                        url: '/holiday/edit/:holiday_id',
                        templateUrl: 'front/tpl/holiday/form',
                        resolve: load(['themes/admin/assets/js/controllers/holiday/form.js']),
                        controller: 'HolidayFormController',
                        params: {action: 'edit'}
                    })
                    .state('holiday.add', {
                        url: '/holiday/add',
                        templateUrl: 'front/tpl/holiday/form',
                        resolve: load(['themes/admin/assets/js/controllers/holiday/form.js']),
                        controller: 'HolidayFormController',
                        params: {action: 'add'}
                    })





                     //Routers SchoolTerm
                    .state('school_term', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_term.all', {
                        url: '/school_terms',
                        templateUrl: 'front/tpl/school_term/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_term/all.js']),
                        controller: 'SchoolTermAllController'
                    })
                    .state('school_term.edit', {
                        url: '/school_term/edit/:school_term_id',
                        templateUrl: 'front/tpl/school_term/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_term/form.js']),
                        controller: 'SchoolTermFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_term.add', {
                        url: '/school_term/add',
                        templateUrl: 'front/tpl/school_term/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_term/form.js']),
                        controller: 'SchoolTermFormController',
                        params: {action: 'add'}
                    })



                    //Routers LevelClass
                    .state('level_class', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('level_class.all', {
                        url: '/level_classes',
                        templateUrl: 'front/tpl/level_class/all',
                        resolve: load(['themes/admin/assets/js/controllers/level_class/all.js']),
                        controller: 'LevelClassAllController'
                    })
                    .state('level_class.edit', {
                        url: '/level_class/edit/:level_class_id',
                        templateUrl: 'front/tpl/level_class/form',
                        resolve: load(['themes/admin/assets/js/controllers/level_class/form.js']),
                        controller: 'LevelClassFormController',
                        params: {action: 'edit'}
                    })
                    .state('level_class.add', {
                        url: '/level_class/add',
                        templateUrl: 'front/tpl/level_class/form',
                        resolve: load(['themes/admin/assets/js/controllers/level_class/form.js']),
                        controller: 'LevelClassFormController',
                        params: {action: 'add'}
                    })









                    //Routers Relship
                    .state('relship', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('relship.all', {
                        url: '/relships',
                        templateUrl: 'front/tpl/relship/all',
                        resolve: load(['themes/admin/assets/js/controllers/relship/all.js']),
                        controller: 'RelshipAllController'
                    })
                    .state('relship.edit', {
                        url: '/relship/edit/:relship_id',
                        templateUrl: 'front/tpl/relship/form',
                        resolve: load(['themes/admin/assets/js/controllers/relship/form.js']),
                        controller: 'RelshipFormController',
                        params: {action: 'edit'}
                    })
                    .state('relship.add', {
                        url: '/relship/add',
                        templateUrl: 'front/tpl/relship/form',
                        resolve: load(['themes/admin/assets/js/controllers/relship/form.js']),
                        controller: 'RelshipFormController',
                        params: {action: 'add'}
                    })





                    //Routers Rating
                    .state('rating', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('rating.all', {
                        url: '/ratings',
                        templateUrl: 'front/tpl/rating/all',
                        resolve: load(['themes/admin/assets/js/controllers/rating/all.js']),
                        controller: 'RatingAllController'
                    })
                    .state('rating.edit', {
                        url: '/rating/edit/:rating_id',
                        templateUrl: 'front/tpl/rating/form',
                        resolve: load(['themes/admin/assets/js/controllers/rating/form.js']),
                        controller: 'RatingFormController',
                        params: {action: 'edit'}
                    })
                    .state('rating.add', {
                        url: '/rating/add',
                        templateUrl: 'front/tpl/rating/form',
                        resolve: load(['themes/admin/assets/js/controllers/rating/form.js']),
                        controller: 'RatingFormController',
                        params: {action: 'add'}
                    })


                    //Routers CoursesTemplate
                    .state('courses_template', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('courses_template.all', {
                        url: '/courses_templates',
                        templateUrl: 'front/tpl/courses_template/all',
                        resolve: load(['themes/admin/assets/js/controllers/courses_template/all.js']),
                        controller: 'CoursesTemplateAllController'
                    })
                    .state('courses_template.edit', {
                        url: '/courses_template/edit/:courses_template_id',
                        templateUrl: 'front/tpl/courses_template/form',
                        resolve: load(['checkbox_helper','themes/admin/assets/js/controllers/courses_template/form.js']),
                        controller: 'CoursesTemplateFormController',
                        params: {action: 'edit'}
                    })
                    .state('courses_template.add', {
                        url: '/courses_template/add',
                        templateUrl: 'front/tpl/courses_template/form',
                        resolve: load(['checkbox_helper','themes/admin/assets/js/controllers/courses_template/form.js']),
                        controller: 'CoursesTemplateFormController',
                        params: {action: 'add'}
                    })




                    //Routers Wday
                    .state('wday', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('wday.all', {
                        url: '/wdays',
                        templateUrl: 'front/tpl/wday/all',
                        resolve: load(['themes/admin/assets/js/controllers/wday/all.js']),
                        controller: 'WdayAllController'
                    })
                    .state('wday.edit', {
                        url: '/wday/edit/:wday_id',
                        templateUrl: 'front/tpl/wday/form',
                        resolve: load(['themes/admin/assets/js/controllers/wday/form.js']),
                        controller: 'WdayFormController',
                        params: {action: 'edit'}
                    })
                    .state('wday.add', {
                        url: '/wday/add',
                        templateUrl: 'front/tpl/wday/form',
                        resolve: load(['themes/admin/assets/js/controllers/wday/form.js']),
                        controller: 'WdayFormController',
                        params: {action: 'add'}
                    })




                     //Routers ExamSession
                    .state('exam_session', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('exam_session.all', {
                        url: '/exam_sessions',
                        templateUrl: 'front/tpl/exam_session/all',
                        resolve: load(['themes/admin/assets/js/controllers/exam_session/all.js']),
                        controller: 'ExamSessionAllController'
                    })
                    .state('exam_session.edit', {
                        url: '/exam_session/edit/:exam_session_id',
                        templateUrl: 'front/tpl/exam_session/form',
                        resolve: load(['themes/admin/assets/js/controllers/exam_session/form.js']),
                        controller: 'ExamSessionFormController',
                        params: {action: 'edit'}
                    })
                    .state('exam_session.add', {
                        url: '/exam_session/add',
                        templateUrl: 'front/tpl/exam_session/form',
                        resolve: load(['themes/admin/assets/js/controllers/exam_session/form.js']),
                        controller: 'ExamSessionFormController',
                        params: {action: 'add'}
                    })




                    //Routers EFile
                    .state('efile', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('efile.all', {
                        url: '/efiles',
                        templateUrl: 'front/tpl/efile/all',
                        resolve: load(['themes/admin/assets/js/controllers/efile/all.js']),
                        controller: 'EFileAllController'
                    })
                    .state('efile.edit', {
                        url: '/efile/edit/:efile_id',
                        templateUrl: 'front/tpl/efile/form',
                        resolve: load(['themes/admin/assets/js/controllers/efile/form.js']),
                        controller: 'EFileFormController',
                        params: {action: 'edit'}
                    })
                    .state('efile.add', {
                        url: '/efile/add',
                        templateUrl: 'front/tpl/efile/form',
                        resolve: load(['themes/admin/assets/js/controllers/efile/form.js']),
                        controller: 'EFileFormController',
                        params: {action: 'add'}
                    })



                    //Routers DayTemplate
                    .state('day_template', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('day_template.all', {
                        url: '/day_templates',
                        templateUrl: 'front/tpl/day_template/all',
                        resolve: load(['themes/admin/assets/js/controllers/day_template/all.js']),
                        controller: 'DayTemplateAllController'
                    })
                    .state('day_template.edit', {
                        url: '/day_template/edit/:day_template_id',
                        templateUrl: 'front/tpl/day_template/form',
                        resolve: load(['themes/admin/assets/js/controllers/day_template/form.js']),
                        controller: 'DayTemplateFormController',
                        params: {action: 'edit'}
                    })
                    .state('day_template.add', {
                        url: '/day_template/add',
                        templateUrl: 'front/tpl/day_template/form',
                        resolve: load(['themes/admin/assets/js/controllers/day_template/form.js']),
                        controller: 'DayTemplateFormController',
                        params: {action: 'add'}
                    })


                    //Routers DayTemplateItem
                    .state('day_template_item', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('day_template_item.all', {
                        url: '/day_template_items',
                        templateUrl: 'front/tpl/day_template_item/all',
                        resolve: load(['themes/admin/assets/js/controllers/day_template_item/all.js']),
                        controller: 'DayTemplateItemAllController'
                    })
                    .state('day_template_item.edit', {
                        url: '/day_template_item/edit/:day_template_item_id',
                        templateUrl: 'front/tpl/day_template_item/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/day_template_item/form.js']),
                        controller: 'DayTemplateItemFormController',
                        params: {action: 'edit'}
                    })
                    .state('day_template_item.add', {
                        url: '/day_template_item/add',
                        templateUrl: 'front/tpl/day_template_item/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/day_template_item/form.js']),
                        controller: 'DayTemplateItemFormController',
                        params: {action: 'add'}
                    })



                    //Routers ModelTerm
                    .state('model_term', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('model_term.all', {
                        url: '/model_terms',
                        templateUrl: 'front/tpl/model_term/all',
                        resolve: load(['themes/admin/assets/js/controllers/model_term/all.js']),
                        controller: 'ModelTermAllController'
                    })
                    .state('model_term.edit', {
                        url: '/model_term/edit/:model_term_id',
                        templateUrl: 'front/tpl/model_term/form',
                        resolve: load(['themes/admin/assets/js/controllers/model_term/form.js']),
                        controller: 'ModelTermFormController',
                        params: {action: 'edit'}
                    })
                    .state('model_term.add', {
                        url: '/model_term/add',
                        templateUrl: 'front/tpl/model_term/form',
                        resolve: load(['themes/admin/assets/js/controllers/model_term/form.js']),
                        controller: 'ModelTermFormController',
                        params: {action: 'add'}
                    })





                    //Routers StudentExam
                    .state('student_exam', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('student_exam.all', {
                        url: '/student_exams',
                        templateUrl: 'front/tpl/student_exam/all',
                        resolve: load(['themes/admin/assets/js/controllers/student_exam/all.js']),
                        controller: 'StudentExamAllController'
                    })
                    .state('student_exam.edit', {
                        url: '/student_exam/edit/:student_exam_id',
                        templateUrl: 'front/tpl/student_exam/form',
                        resolve: load(['themes/admin/assets/js/controllers/student_exam/form.js']),
                        controller: 'StudentExamFormController',
                        params: {action: 'edit'}
                    })
                    .state('student_exam.add', {
                        url: '/student_exam/add',
                        templateUrl: 'front/tpl/student_exam/form',
                        resolve: load(['themes/admin/assets/js/controllers/student_exam/form.js']),
                        controller: 'StudentExamFormController',
                        params: {action: 'add'}
                    })





                    //Routers StudentExam
                    .state('family_relation', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('family_relation.my_relations', {
                        url: '/my_family_relations',
                        templateUrl: 'front/tpl/family_relation/my_relations',
                        resolve: load(['themes/admin/assets/js/controllers/family_relation/my_relations.js']),
                        controller: 'MyRelationsFamilyRelationController'
                    })
                    .state('family_relation.add_new_relation', {
                        url: '/add_new_relation',
                        templateUrl: 'front/tpl/family_relation/add_new_relation',
                        resolve: load(['themes/admin/assets/js/controllers/family_relation/add_new_relation.js']),
                        controller: 'AddNewRelationFamilyRelationController'
                    })
                    .state('family_relation.edit_my_relation', {
                        url: '/edit_my_relation/:family_relation_id',
                        templateUrl: 'front/tpl/family_relation/edit_my_relation',
                        resolve: load(['themes/admin/assets/js/controllers/family_relation/edit_my_relation.js']),
                        controller: 'EditMyRelationFamilyRelationController'
                    })






                    //Routers StudentFileStatus
                    .state('student_file_status', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('student_file_status.all', {
                        url: '/student_file_status',
                        templateUrl: 'front/tpl/student_file_status/all',
                        resolve: load(['themes/admin/assets/js/controllers/student_file_status/all.js']),
                        controller: 'StudentFileStatusAllController'
                    })
                    .state('student_file_status.edit', {
                        url: '/student_file_status/edit/:student_file_status_id',
                        templateUrl: 'front/tpl/student_file_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/student_file_status/form.js']),
                        controller: 'StudentFileStatusFormController',
                        params: {action: 'edit'}
                    })
                    .state('student_file_status.add', {
                        url: '/student_file_status/add',
                        templateUrl: 'front/tpl/student_file_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/student_file_status/form.js']),
                        controller: 'StudentFileStatusFormController',
                        params: {action: 'add'}
                    })





                    //Routers ClassCourseExam
                    .state('class_course_exam', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('class_course_exam.all', {
                        url: '/class_course_exams',
                        templateUrl: 'front/tpl/class_course_exam/all',
                        resolve: load(['themes/admin/assets/js/controllers/class_course_exam/all.js']),
                        controller: 'ClassCourseExamAllController'
                    })
                    .state('class_course_exam.edit', {
                        url: '/class_course_exam/edit/:class_course_exam_id',
                        templateUrl: 'front/tpl/class_course_exam/form',
                        resolve: load(['themes/admin/assets/js/controllers/class_course_exam/form.js']),
                        controller: 'ClassCourseExamFormController',
                        params: {action: 'edit'}
                    })
                    .state('class_course_exam.add', {
                        url: '/class_course_exam/add',
                        templateUrl: 'front/tpl/class_course_exam/form',
                        resolve: load(['themes/admin/assets/js/controllers/class_course_exam/form.js']),
                        controller: 'ClassCourseExamFormController',
                        params: {action: 'add'}
                    })






                    //Routers SchoolEmployee
                    .state('school_employee', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_employee.all', {
                        url: '/school_employees',
                        templateUrl: 'front/tpl/school_employee/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_employee/all.js']),
                        controller: 'SchoolEmployeeAllController'
                    })
                    .state('school_employee.edit', {
                        url: '/school_employee/edit/:school_employee_id',
                        templateUrl: 'front/tpl/school_employee/form',
                        resolve: load(['moment', 'checkbox_helper', 'themes/admin/assets/js/controllers/school_employee/form.js']),
                        controller: 'SchoolEmployeeFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_employee.add', {
                        url: '/school_employee/add',
                        templateUrl: 'front/tpl/school_employee/form',
                        resolve: load(['moment',  'checkbox_helper', 'themes/admin/assets/js/controllers/school_employee/form.js']),
                        controller: 'SchoolEmployeeFormController',
                        params: {action: 'add'}
                    })
                    .state('school_employee.replace', {
                        url: '/school_employees/replace',
                        templateUrl: 'front/tpl/school_employee/replace',
                        resolve: load(['themes/admin/assets/js/controllers/school_employee/replace.js']),
                        controller: 'SchoolEmployeeReplaceController'
                    })
                    .state('school_employee.schedule', {
                        url: '/school_employee/schedule/:school_employee_id',
                        templateUrl: 'front/tpl/school_employee/schedule',
                        resolve: load(['themes/admin/assets/js/controllers/school_employee/schedule.js']),
                        controller: 'SchoolEmployeeScheduleController'
                    })




                    //Routers Student
                    .state('student', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('student.all', {
                        url: '/students',
                        templateUrl: 'front/tpl/student/all',
                        resolve: load(['themes/admin/assets/js/controllers/student/all.js']),
                        controller: 'StudentAllController'
                    })
                    .state('student.edit', {
                        url: '/student/edit/:student_id',
                        templateUrl: 'front/tpl/student/form',
                        resolve: load(['themes/admin/assets/js/controllers/student/form.js']),
                        controller: 'StudentFormController',
                        params: {action: 'edit'}
                    })
                    .state('student.add', {
                        url: '/student/add',
                        templateUrl: 'front/tpl/student/form',
                        resolve: load(['themes/admin/assets/js/controllers/student/form.js']),
                        controller: 'StudentFormController',
                        params: {action: 'add'}
                    })
                    .state('student.add_to_school', {
                        url: '/student/add_to_school',
                        templateUrl: 'front/tpl/student/add_to_school',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/student/add_to_school.js']),
                        controller: 'StudentAddToSchoolController'
                    })
                    .state('student.transfer_seat', {
                        url: '/student/transfer_seat',
                        templateUrl: 'front/tpl/student/transfer_seat',
                        resolve: load(['themes/admin/assets/js/controllers/student/transfer_seat.js']),
                        controller: 'StudentTransferSeatController'
                    })





                    //Routers SchoolRegistration
                    .state('school_registration', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_registration.all', {
                        url: '/school_registrations',
                        templateUrl: 'front/tpl/school_registration/all',
                        resolve: load(['themes/admin/assets/js/controllers/school_registration/all.js']),
                        controller: 'SchoolRegistrationAllController'
                    })
                    .state('school_registration.edit', {
                        url: '/school_registration/edit/:school_registration_id',
                        templateUrl: 'front/tpl/school_registration/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_registration/form.js']),
                        controller: 'SchoolRegistrationFormController',
                        params: {action: 'edit'}
                    })
                    .state('school_registration.add', {
                        url: '/school_registration/add',
                        templateUrl: 'front/tpl/school_registration/form',
                        resolve: load(['themes/admin/assets/js/controllers/school_registration/form.js']),
                        controller: 'SchoolRegistrationFormController',
                        params: {action: 'add'}
                    })






                    //Routers Alert
                    .state('alert', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('alert.send', {
                        url: '/alert/send',
                        templateUrl: 'front/tpl/alert/send',
                        resolve: load(['themes/admin/assets/js/controllers/alert/send.js']),
                        controller: 'AlertSendController'
                    })





                    //Routers CandidateStatus
                    .state('candidate_status', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('candidate_status.all', {
                        url: '/candidate_status',
                        templateUrl: 'front/tpl/candidate_status/all',
                        resolve: load(['themes/admin/assets/js/controllers/candidate_status/all.js']),
                        controller: 'CandidateStatusAllController'
                    })
                    .state('candidate_status.edit', {
                        url: '/candidate_status/edit/:candidate_status_id',
                        templateUrl: 'front/tpl/candidate_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/candidate_status/form.js']),
                        controller: 'CandidateStatusFormController',
                        params: {action: 'edit'}
                    })
                    .state('candidate_status.add', {
                        url: '/candidate_status/add',
                        templateUrl: 'front/tpl/candidate_status/form',
                        resolve: load(['themes/admin/assets/js/controllers/candidate_status/form.js']),
                        controller: 'CandidateStatusFormController',
                        params: {action: 'add'}
                    })




                    
                    



                    //Routers School Class Course
                    .state('school_class_course', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('school_class_course.config_item', {
                        url: '/school_class_course/config_item',
                        templateUrl: 'front/tpl/school_class_course/config_item',
                        resolve: load(['themes/admin/assets/js/controllers/school_class_course/config_item.js']),
                        controller: 'SchoolClassCourseConfigItemController'
                    })
                    


                    //Routers Feed import
                    .state('feed_import', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('feed_import.all', {
                        url: '/feed_import',
                        templateUrl: 'front/tpl/feed_import/all',
                        resolve: load(['angularFileUpload','themes/admin/assets/js/controllers/feed_import/all.js']),
                        controller: 'FeedImportAllController'
                    })
                    .state('feed_import.process', {
                        url: '/feed_import/process/:feed_import_id',
                        templateUrl: 'front/tpl/feed_import/process',
                        resolve: load(['moment',  'themes/admin/assets/js/controllers/feed_import/process.js']),
                        controller: 'FeedImportProcessController'
                    })





                    //Routers CRM
                    .state('crm', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('crm.erequest', {
                        url: '/crm/erequest',
                        template: '<div ui-view class="fade-in-up"></div>'
                    })
                    .state('crm.erequest.submit', {
                        url: '/submit',
                        templateUrl: 'front/tpl/crm/erequest/submit',
                        resolve: load(['angularFileUpload','themes/admin/assets/js/controllers/crm/erequest/submit.js']),
                        controller: 'CrmErequestSubmitController'
                    })
                    .state('crm.erequest.settings', {
                        url: '/settings/:crm_erequest_id',
                        templateUrl: 'front/tpl/crm/erequest/settings',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/crm/erequest/settings.js']),
                        controller: 'CrmErequestSettingsController'
                    })
                    .state('crm.erequest.my_erequests', {
                        url: '/my-erequests/',
                        templateUrl: 'front/tpl/crm/erequest/my_erequests',
                        resolve: load(['moment',  'themes/admin/assets/js/controllers/crm/erequest/my_erequests.js']),
                        controller: 'CrmMyErequestsController'
                    })
                    .state('crm.erequest.follow_up', {
                        url: '/follow_up/:crm_erequest_id',
                        templateUrl: 'front/tpl/crm/erequest/follow_up',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/crm/erequest/follow_up.js']),
                        controller: 'CrmErequestsFollowUpController'
                    })
                    .state('crm.erequest.received_erequests', {
                        url: '/received-erequests/',
                        templateUrl: 'front/tpl/crm/erequest/received_erequests',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/crm/erequest/received_erequests.js']),
                        controller: 'CrmReceivedErequestsController'
                    })
                    .state('crm.erequest.reply_erequest', {
                        url: '/reply-erequest/:crm_erequest_id',
                        templateUrl: 'front/tpl/crm/erequest/reply_erequest',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/crm/erequest/reply_erequest.js']),
                        controller: 'CrmReplyErequestController'
                    })





                    //Routers CPC
                    .state('cpc', {
                        abstract: true,
                        templateUrl: layout
                    })
                    .state('cpc.books', {
                        url: '/cpc/books/',
                        templateUrl: 'front/tpl/cpc/book/all',
                        resolve: load(['themes/admin/assets/js/controllers/cpc/book/all.js']),
                        controller: 'CpcBookAllController'
                    })
                    .state('cpc.edit_book', {
                        url: '/cpc/book/edit/:cpc_book_id',
                        templateUrl: 'front/tpl/cpc/book/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/cpc/book/form.js']),
                        controller: 'CpcBookFormController',
                        params: {action: 'edit'}
                    })
                    .state('cpc.add_book', {
                        url: '/cpc/book/add',
                        templateUrl: 'front/tpl/cpc/book/form',
                        resolve: load(['moment', 'themes/admin/assets/js/controllers/cpc/book/form.js']),
                        controller: 'CpcBookFormController',
                        params: {action: 'add'}
                    })
                    .state('cpc.book_pages', {
                        url: '/cpc/book/:book_id/pages',
                        templateUrl: 'front/tpl/cpc/book/pages',
                        resolve: load(['angularFileUpload','themes/admin/assets/js/controllers/cpc/book/pages.js']),
                        controller: 'CpcBookPagesController'
                    })
                    .state('cpc.edit_book_page', {
                        url: '/cpc/book/:cpc_book_id/page/edit/:cpc_book_page_id',
                        templateUrl: 'front/tpl/cpc/book/page_form',
                        resolve: load(['themes/admin/assets/js/controllers/cpc/book/page_form.js']),
                        controller: 'CpcBookPageFormController',
                        params: {action: 'edit'}
                    })
                    .state('cpc.add_book_page', {
                        url: '/cpc/book/:cpc_book_id/page/add',
                        templateUrl: 'front/tpl/cpc/book/page_form',
                        resolve: load(['themes/admin/assets/js/controllers/cpc/book/page_form.js']),
                        controller: 'CpcBookPageFormController',
                        params: {action: 'add'}
                    })
                    .state('cpc.course_programs', {
                        url: '/cpc/course-programs/',
                        templateUrl: 'front/tpl/cpc/course_program/all',
                        resolve: load(['themes/admin/assets/js/controllers/cpc/course_program/all.js']),
                        controller: 'CpcCourseProgramAllController'
                    })
                    .state('cpc.edit_course_program', {
                        url: '/cpc/course-program/edit/:cpc_course_program_id',
                        templateUrl: 'front/tpl/cpc/course_program/form',
                        resolve: load(['themes/admin/assets/js/controllers/cpc/course_program/form.js']),
                        controller: 'CpcCourseProgramFormController',
                        params: {action: 'edit'}
                    })
                    .state('cpc.add_course_program', {
                        url: '/cpc/course-program/add',
                        templateUrl: 'front/tpl/cpc/course_program/form',
                        resolve: load(['themes/admin/assets/js/controllers/cpc/course_program/form.js']),
                        controller: 'CpcCourseProgramFormController',
                        params: {action: 'add'}
                    })
                    .state('cpc.course_program_books', {
                        url: '/cpc/course-program/:cpc_course_program_id/books',
                        templateUrl: 'front/tpl/cpc/course_program/books',
                        resolve: load(['themes/admin/assets/js/controllers/cpc/course_program/books.js']),
                        controller: 'CpcCourseProgramBooksController'
                    })






                    /* Exemple Theme */
                    .state('app.dashboard-v2', {
                        url: '/dashboard-v2',
                        templateUrl: 'themes/admin/assets/tpl/app_dashboard_v2.html',
                        resolve: load(['themes/admin/assets/js/controllers/chart.js'])
                    })
                    .state('app.dashboard-v3', {
                        url: '/dashboard-v3',
                        templateUrl: 'themes/admin/assets/tpl/app_dashboard_v3.html',
                        resolve: load(['themes/admin/assets/js/controllers/chart.js'])
                    })
                    .state('app.ui', {
                        url: '/ui',
                        template: '<div ui-view class="fade-in-up"></div>'
                    })
                    .state('app.ui.buttons', {
                        url: '/buttons',
                        templateUrl: 'themes/admin/assets/tpl/ui_buttons.html'
                    })
                    .state('app.ui.icons', {
                        url: '/icons',
                        templateUrl: 'themes/admin/assets/tpl/ui_icons.html'
                    })
                    .state('app.ui.grid', {
                        url: '/grid',
                        templateUrl: 'themes/admin/assets/tpl/ui_grid.html'
                    })
                    .state('app.ui.widgets', {
                        url: '/widgets',
                        templateUrl: 'themes/admin/assets/tpl/ui_widgets.html'
                    })
                    .state('app.ui.bootstrap', {
                        url: '/bootstrap',
                        templateUrl: 'themes/admin/assets/tpl/ui_bootstrap.html'
                    })
                    .state('app.ui.sortable', {
                        url: '/sortable',
                        templateUrl: 'themes/admin/assets/tpl/ui_sortable.html'
                    })
                    .state('app.ui.scroll', {
                        url: '/scroll',
                        templateUrl: 'themes/admin/assets/tpl/ui_scroll.html',
                        resolve: load('js/controllers/scroll.js')
                    })
                    .state('app.ui.portlet', {
                        url: '/portlet',
                        templateUrl: 'themes/admin/assets/tpl/ui_portlet.html'
                    })
                    .state('app.ui.timeline', {
                        url: '/timeline',
                        templateUrl: 'themes/admin/assets/tpl/ui_timeline.html'
                    })
                    .state('app.ui.tree', {
                        url: '/tree',
                        templateUrl: 'themes/admin/assets/tpl/ui_tree.html',
                        resolve: load(['angularBootstrapNavTree', 'js/controllers/tree.js'])
                    })
                    .state('app.ui.toaster', {
                        url: '/toaster',
                        templateUrl: 'themes/admin/assets/tpl/ui_toaster.html',
                        resolve: load(['toaster', 'js/controllers/toaster.js'])
                    })
                    .state('app.ui.jvectormap', {
                        url: '/jvectormap',
                        templateUrl: 'themes/admin/assets/tpl/ui_jvectormap.html',
                        resolve: load('js/controllers/vectormap.js')
                    })
                    .state('app.ui.googlemap', {
                        url: '/googlemap',
                        templateUrl: 'themes/admin/assets/tpl/ui_googlemap.html',
                        resolve: load(['themes/admin/assets/js/app/map/load-google-maps.js', 'js/app/map/ui-map.js', 'js/app/map/map.js'], function() {
                            return loadGoogleMaps(); })
                    })
                    .state('app.chart', {
                        url: '/chart',
                        templateUrl: 'themes/admin/assets/tpl/ui_chart.html',
                        resolve: load('js/controllers/chart.js')
                    })
                    // table
                    .state('app.table', {
                        url: '/table',
                        template: '<div ui-view></div>'
                    })
                    .state('app.table.static', {
                        url: '/static',
                        templateUrl: 'themes/admin/assets/tpl/table_static.html'
                    })
                    .state('app.table.datatable', {
                        url: '/datatable',
                        templateUrl: 'themes/admin/assets/tpl/table_datatable.html'
                    })
                    .state('app.table.footable', {
                        url: '/footable',
                        templateUrl: 'themes/admin/assets/tpl/table_footable.html'
                    })
                    .state('app.table.grid', {
                        url: '/grid',
                        templateUrl: 'themes/admin/assets/tpl/table_grid.html',
                        resolve: load(['ngGrid', 'themes/admin/assets/js/controllers/grid.js'])
                    })
                    .state('app.table.uigrid', {
                        url: '/uigrid',
                        templateUrl: 'themes/admin/assets/tpl/table_uigrid.html',
                        resolve: load(['ui.grid', 'themes/admin/assets/js/controllers/uigrid.js'])
                    })
                    .state('app.table.editable', {
                        url: '/editable',
                        templateUrl: 'themes/admin/assets/tpl/table_editable.html',
                        controller: 'XeditableCtrl',
                        resolve: load(['xeditable', 'themes/admin/assets/js/controllers/xeditable.js'])
                    })
                    .state('app.table.smart', {
                        url: '/smart',
                        templateUrl: 'themes/admin/assets/tpl/table_smart.html',
                        resolve: load(['smart-table', 'themes/admin/assets/js/controllers/table.js'])
                    })
                    // form
                    .state('app.form', {
                        url: '/form',
                        template: '<div ui-view class="fade-in"></div>',
                        resolve: load('themes/admin/assets/js/controllers/form.js')
                    })
                    .state('app.form.components', {
                        url: '/components',
                        templateUrl: 'themes/admin/assets/tpl/form_components.html',
                        resolve: load(['ngBootstrap', 'daterangepicker', 'themes/admin/assets/js/controllers/form.components.js'])
                    })
                    .state('app.form.elements', {
                        url: '/elements',
                        templateUrl: 'themes/admin/assets/tpl/form_elements.html'
                    })
                    .state('app.form.validation', {
                        url: '/validation',
                        templateUrl: 'themes/admin/assets/tpl/form_validation.html'
                    })
                    .state('app.form.wizard', {
                        url: '/wizard',
                        templateUrl: 'themes/admin/assets/tpl/form_wizard.html'
                    })
                    .state('app.form.fileupload', {
                        url: '/fileupload',
                        templateUrl: 'themes/admin/assets/tpl/form_fileupload.html',
                        resolve: load(['angularFileUpload', 'themes/admin/assets/js/controllers/file-upload.js'])
                    })
                    .state('app.form.imagecrop', {
                        url: '/imagecrop',
                        templateUrl: 'themes/admin/assets/tpl/form_imagecrop.html',
                        resolve: load(['ngImgCrop', 'themes/admin/assets/js/controllers/imgcrop.js'])
                    })
                    .state('app.form.select', {
                        url: '/select',
                        templateUrl: 'themes/admin/assets/tpl/form_select.html',
                        controller: 'SelectCtrl',
                        resolve: load(['ui.select', 'themes/admin/assets/js/controllers/select.js'])
                    })
                    .state('app.form.slider', {
                        url: '/slider',
                        templateUrl: 'themes/admin/assets/tpl/form_slider.html',
                        controller: 'SliderCtrl',
                        resolve: load(['vr.directives.slider', 'themes/admin/assets/js/controllers/slider.js'])
                    })
                    .state('app.form.editor', {
                        url: '/editor',
                        templateUrl: 'themes/admin/assets/tpl/form_editor.html',
                        controller: 'EditorCtrl',
                        resolve: load(['textAngular', 'themes/admin/assets/js/controllers/editor.js'])
                    })
                    .state('app.form.xeditable', {
                        url: '/xeditable',
                        templateUrl: 'themes/admin/assets/tpl/form_xeditable.html',
                        controller: 'XeditableCtrl',
                        resolve: load(['xeditable', 'themes/admin/assets/js/controllers/xeditable.js'])
                    })
                    // pages
                    .state('app.page', {
                        url: '/page',
                        template: '<div ui-view class="fade-in-down"></div>'
                    })
                    .state('app.page.profile', {
                        url: '/profile',
                        templateUrl: 'themes/admin/assets/tpl/page_profile.html'
                    })
                    .state('app.page.post', {
                        url: '/post',
                        templateUrl: 'themes/admin/assets/tpl/page_post.html'
                    })
                    .state('app.page.search', {
                        url: '/search',
                        templateUrl: 'themes/admin/assets/tpl/page_search.html'
                    })
                    .state('app.page.invoice', {
                        url: '/invoice',
                        templateUrl: 'themes/admin/assets/tpl/page_invoice.html'
                    })
                    .state('app.page.price', {
                        url: '/price',
                        templateUrl: 'themes/admin/assets/tpl/page_price.html'
                    })
                    .state('app.docs', {
                        url: '/docs',
                        templateUrl: 'themes/admin/assets/tpl/docs.html'
                    })
                    // others
                    .state('lockme', {
                        url: '/lockme',
                        templateUrl: 'themes/admin/assets/tpl/page_lockme.html'
                    })
                    .state('access', {
                        url: '/access',
                        template: '<div ui-view class="fade-in-right-big smooth"></div>'
                    })
                    .state('access.signin', {
                        url: '/signin',
                        templateUrl: 'themes/admin/assets/tpl/page_signin.html',
                        resolve: load(['themes/admin/assets/js/controllers/signin.js'])
                    })
                    .state('access.signup', {
                        url: '/signup',
                        templateUrl: 'themes/admin/assets/tpl/page_signup.html',
                        resolve: load(['themes/admin/assets/js/controllers/signup.js'])
                    })
                    .state('access.forgotpwd', {
                        url: '/forgotpwd',
                        templateUrl: 'themes/admin/assets/tpl/page_forgotpwd.html'
                    })
                    .state('access.404', {
                        url: '/404',
                        templateUrl: 'themes/admin/assets/tpl/page_404.html'
                    })

                // fullCalendar
                .state('app.calendar', {
                    url: '/calendar',
                    templateUrl: 'themes/admin/assets/tpl/app_calendar.html',
                    // use resolve to load other dependences
                    resolve: load(['moment',  'fullcalendar', 'ui.calendar', 'themes/admin/assets/js/app/calendar/calendar.js'])
                })

                     // mail
                    .state('app.mail', {
                        abstract: true,
                        url: '/mail',
                        templateUrl: 'themes/admin/assets/tpl/mail.html',
                        // use resolve to load other dependences
                        resolve: load(['themes/admin/assets/js/app/mail/mail.js', 'themes/admin/assets/js/app/mail/mail-service.js', 'moment'])
                    })
                    .state('app.mail.list', {
                        url: '/inbox/{fold}',
                        templateUrl: 'themes/admin/assets/tpl/mail.list.html'
                    })
                    .state('app.mail.detail', {
                        url: '/{mailId:[0-9]{1,4}}',
                        templateUrl: 'themes/admin/assets/tpl/mail.detail.html'
                    })
                    .state('app.mail.compose', {
                        url: '/compose',
                        templateUrl: 'themes/admin/assets/tpl/mail.new.html'
                    })

                .state('layout', {
                        abstract: true,
                        url: '/layout',
                        templateUrl: 'themes/admin/assets/tpl/layout.html'
                    })
                    .state('layout.fullwidth', {
                        url: '/fullwidth',
                        views: {
                            '': {
                                templateUrl: 'themes/admin/assets/tpl/layout_fullwidth.html'
                            },
                            'footer': {
                                templateUrl: 'themes/admin/assets/tpl/layout_footer_fullwidth.html'
                            }
                        },
                        resolve: load(['themes/admin/assets/js/controllers/vectormap.js'])
                    })
                    .state('layout.mobile', {
                        url: '/mobile',
                        views: {
                            '': {
                                templateUrl: 'themes/admin/assets/tpl/layout_mobile.html'
                            },
                            'footer': {
                                templateUrl: 'themes/admin/assets/tpl/layout_footer_mobile.html'
                            }
                        }
                    })
                    .state('layout.app', {
                        url: '/app',
                        views: {
                            '': {
                                templateUrl: 'themes/admin/assets/tpl/layout_app.html'
                            },
                            'footer': {
                                templateUrl: 'themes/admin/assets/tpl/layout_footer_fullwidth.html'
                            }
                        },
                        resolve: load(['themes/admin/assets/js/controllers/tab.js'])
                    })
                    .state('apps', {
                        abstract: true,
                        url: '/apps',
                        templateUrl: 'themes/admin/assets/tpl/layout.html'
                    })
                    .state('apps.note', {
                        url: '/note',
                        templateUrl: 'themes/admin/assets/tpl/apps_note.html',
                        resolve: load(['themes/admin/assets/js/app/note/note.js', 'moment'])
                    })
                    .state('apps.contact', {
                        url: '/contact',
                        templateUrl: 'themes/admin/assets/tpl/apps_contact.html',
                        resolve: load(['themes/admin/assets/js/app/contact/contact.js'])
                    })
                    .state('app.weather', {
                        url: '/weather',
                        templateUrl: 'themes/admin/assets/tpl/apps_weather.html',
                        resolve: load(['themes/admin/assets/js/app/weather/skycons.js', 'angular-skycons', 'js/app/weather/ctrl.js', 'moment'])
                    })
                    .state('app.todo', {
                        url: '/todo',
                        templateUrl: 'themes/admin/assets/tpl/apps_todo.html',
                        resolve: load(['themes/admin/assets/js/app/todo/todo.js', 'moment'])
                    })
                    .state('app.todo.list', {
                        url: '/{fold}'
                    })
                    .state('app.note', {
                        url: '/note',
                        templateUrl: 'themes/admin/assets/tpl/apps_note_material.html',
                        resolve: load(['themes/admin/assets/js/app/note/note.js', 'moment'])
                    })
                    .state('music', {
                        url: '/music',
                        templateUrl: 'themes/admin/assets/tpl/music.html',
                        controller: 'MusicCtrl',
                        resolve: load([
                            'com.2fdevs.videogular',
                            'com.2fdevs.videogular.plugins.controls',
                            'com.2fdevs.videogular.plugins.overlayplay',
                            'com.2fdevs.videogular.plugins.poster',
                            'com.2fdevs.videogular.plugins.buffering',
                            'themes/admin/assets/js/app/music/ctrl.js',
                            'themes/admin/assets/js/app/music/theme.css'
                        ])
                    })
                    .state('music.home', {
                        url: '/home',
                        templateUrl: 'themes/admin/assets/tpl/music.home.html'
                    })
                    .state('music.genres', {
                        url: '/genres',
                        templateUrl: 'themes/admin/assets/tpl/music.genres.html'
                    })
                    .state('music.detail', {
                        url: '/detail',
                        templateUrl: 'themes/admin/assets/tpl/music.detail.html'
                    })
                    .state('music.mtv', {
                        url: '/mtv',
                        templateUrl: 'themes/admin/assets/tpl/music.mtv.html'
                    })
                    .state('music.mtvdetail', {
                        url: '/mtvdetail',
                        templateUrl: 'themes/admin/assets/tpl/music.mtv.detail.html'
                    })
                    .state('music.playlist', {
                        url: '/playlist/{fold}',
                        templateUrl: 'themes/admin/assets/tpl/music.playlist.html'
                    })
                    .state('app.material', {
                        url: '/material',
                        template: '<div ui-view class="wrapper-md"></div>',
                        resolve: load(['themes/admin/assets/js/controllers/material.js'])
                    })
                    .state('app.material.button', {
                        url: '/button',
                        templateUrl: 'themes/admin/assets/tpl/material/button.html'
                    })
                    .state('app.material.color', {
                        url: '/color',
                        templateUrl: 'themes/admin/assets/tpl/material/color.html'
                    })
                    .state('app.material.icon', {
                        url: '/icon',
                        templateUrl: 'themes/admin/assets/tpl/material/icon.html'
                    })
                    .state('app.material.card', {
                        url: '/card',
                        templateUrl: 'themes/admin/assets/tpl/material/card.html'
                    })
                    .state('app.material.form', {
                        url: '/form',
                        templateUrl: 'themes/admin/assets/tpl/material/form.html'
                    })
                    .state('app.material.list', {
                        url: '/list',
                        templateUrl: 'themes/admin/assets/tpl/material/list.html'
                    })
                    .state('app.material.ngmaterial', {
                        url: '/ngmaterial',
                        templateUrl: 'themes/admin/assets/tpl/material/ngmaterial.html'
                    });

                function load(srcs, callback) {
                    return {
                        deps: ['$ocLazyLoad', '$q',
                            function($ocLazyLoad, $q) {
                                var deferred = $q.defer();
                                var promise = false;
                                srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                                if (!promise) {
                                    promise = deferred.promise;
                                }
                                angular.forEach(srcs, function(src) {
                                    promise = promise.then(function() {
                                        if (JQ_CONFIG[src]) {
                                            return $ocLazyLoad.load(JQ_CONFIG[src]);
                                        }
                                        angular.forEach(MODULE_CONFIG, function(module) {
                                            if (module.name == src) {
                                                name = module.name;
                                            } else {
                                                name = src;
                                            }
                                        });
                                        return $ocLazyLoad.load(name);
                                    });
                                });
                                deferred.resolve();
                                return callback ? promise.then(function() {
                                    return callback(); }) : promise;
                            }
                        ]
                    };
                };


            }
        ]
    );