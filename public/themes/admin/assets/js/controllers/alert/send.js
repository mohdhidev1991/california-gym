'use strict';

angular.module('app', ['ngMaterial']);
app.controller('AlertSendController', function($scope, $http, $stateParams, $location, $filter) {

    $scope.alert = {};

    $scope.GetAlertReceivers = function() {
        $scope.alert_receivers = null;
        $http({
            method: 'GET',
            url: '/api/Alert/getReceivers/'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.alert_receivers = response.data.data;
                angular.forEach($scope.alert_receivers,function(obj, key) {
                    if(obj.lookup_code==="attendance"){
                        $scope.alert_receivers.splice(key, 1);
                    }
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetAlertTypes = function() {
        $http({
            method: 'GET',
            url: '/api/AlertType/get/'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.alert_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get_my_schools/'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.schools = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetSchoolYears = function() {
        $scope.school_years = null;
        if($scope.alert.school_id){
            $http({
                method: 'GET',
                url: '/api/SchoolYear/get/school_id='+$scope.alert.school_id
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $scope.school_years = response.data.data;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };

    $scope.GetLevelClasses = function() {
        $scope.level_classes = null;
        if($scope.alert.school_year_id){
            $http({
                method: 'GET',
                url: '/api/LevelClass/get/school_year_id='+$scope.alert.school_year_id
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $scope.level_classes = response.data.data;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.GetSchoolClasses = function() {
        $scope.school_classes = null;
        if($scope.alert.school_year_id && $scope.alert.level_class_id){
            $http({
                method: 'GET',
                url: '/api/SchoolClass/get/school_year_id='+$scope.alert.school_year_id+',level_class_id='+$scope.alert.level_class_id
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $scope.school_classes = response.data.data;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };

    $scope.changed_alert_type = function() {
        $scope.alert.title_ar = $scope.alert.title_en = $scope.alert.message_ar = $scope.alert.message_en = null;
        $scope.selected_type = $filter('filter')($scope.alert_types, { id: $scope.alert.alert_type })[0];

        if ($scope.selected_type) {
            $scope.alert.title_ar = $scope.selected_type.alert_type_name_ar;
            $scope.alert.title_en = $scope.selected_type.alert_type_name_en;
            $scope.alert.message_ar = $scope.selected_type.alert_type_desc_ar;
            $scope.alert.message_en = $scope.selected_type.alert_type_desc_en;
        }
    };

    $scope.changed_alert_receiver = function() {
        $scope.alert.alert_type = $scope.alert.title_ar = $scope.alert.title_en = $scope.alert.description_ar = $scope.alert.description_en = $scope.alert.school_id = $scope.alert.school_year_id = $scope.alert.level_class_id = $scope.alert.school_class_id = null;
    };

    $scope.GetAlertReceivers();
    $scope.GetAlertTypes();
    $scope.GetSchools();
    $scope.GetSchoolYears();



    $scope.selected = undefined;
    $scope.getStudents = function(val) {
        return $http.get('/api/Student/get/limit=10,key_search=' + encodeURI(val)).then(function(response) {
            return response.data.data.map(function(item) {
                item.fullname = item.firstname + ' ' + item.lastname + ' ' + item.f_firstname;
                return item;
            });
        });
    };




    $scope.getTeachers = function(val) {
        return $http.get('/api/SchoolEmployee/get/school_job=teacher,limit=10,key_search=' + encodeURI(val)).then(function(response) {
            return response.data.status.map(function(item) {
                item.fullname = item.firstname + ' ' + item.lastname + ' ' + item.f_firstname;
                return item;
            });
        });
    };



    $scope.send = function() {
        $scope.success_send_alert = null;
        $scope.error_send_alert = null;
        var request_data = {
            receiver : $scope.alert.alert_receiver,
            title : {"ar" : $scope.alert.title_ar, "en" : $scope.alert.title_en},
            message : {"ar" : $scope.alert.message_ar, "en" : $scope.alert.message_en}
        };
        if($scope.alert.student){
            request_data.student_id = $scope.alert.student.id;
        }
        $http({
            method: 'POST',
            data: request_data,
            url: '/api/Alert/sendByReceiver/'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.success_send_alert = true;
            }else{
                $scope.error_send_alert = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

});


