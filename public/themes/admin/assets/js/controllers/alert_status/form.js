'use strict';

app.controller('AlertStatusFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.alert_status = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetAlertStatus = function() {
        $http({
            method: 'GET',
            url: '/api/AlertStatus/get/alert_status_id=' + $stateParams.alert_status_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.alert_status = response.data.data;
                if ($scope.alert_status.active && $scope.alert_status.active === "Y") {
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if ($scope.action === "edit") {
        $scope.GetAlertStatus();
    } else {
        $scope.alert_status = {};
        $scope.alert_status.new = true;
    }


    $scope.pre_save = function() {
        if ($scope.active_switch === true) {
            $scope.alert_status.active = "Y";
        } else {
            $scope.alert_status.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location) {
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { alert_status: $scope.alert_status },
                url: '/api/AlertStatus/save'
            }).then(function successCallback(response) {
                if (back_location) {
                    $state.go('alert_status.all');
                } else {
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function() {
        if ($scope.pre_save()) {
            console.log($scope.alert_status);
            $http({
                method: 'POST',
                data: { alert_status: $scope.alert_status },
                url: '/api/AlertStatus/save'
            }).then(function successCallback(response) {
                console.log(response);
                if (response.data.data.id) {
                    $location.path("/AlertStatus/edit/" + response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});