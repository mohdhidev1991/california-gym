'use strict';

app.controller('AlertTypeAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.alert_types = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetAlertTypes = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/AlertType/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.alert_types) {
                $scope.alert_types = response.data.data.alert_types;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetAlertTypes();
    };



    $scope.delete_alert_type = function(alert_type_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteAlertTypeInstanceCtrl',
            resolve: {
                alert_type_id: function() {
                    return alert_type_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetAlertTypes();
        }, function() {
            
        });
    };


    $scope.GetAlertTypes();


});



app.controller('ModalDeleteAlertTypeInstanceCtrl', function($scope, $uibModalInstance, $http, alert_type_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {alert_type_id : alert_type_id},
            url: '/api/AlertType/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


