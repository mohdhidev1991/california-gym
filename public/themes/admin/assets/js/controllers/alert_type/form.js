'use strict';

app.controller('AlertTypeFormController', function($scope, $http, $stateParams, $location) {

    $scope.alert_type = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetAlertType = function() {
        $http({
            method: 'GET',
            url: '/api/AlertType/get/alert_type_id='+$stateParams.alert_type_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.alert_type = response.data.data;
                if($scope.alert_type.active && $scope.alert_type.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetAlertType();
    }else{
        $scope.alert_type = {};
        $scope.alert_type.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.alert_type.active = "Y";
        }else{
            $scope.alert_type.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {alert_type: $scope.alert_type},
                url: '/api/AlertType/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/AlertTypes");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.alert_type);
            $http({
                method: 'POST',
                data: {alert_type: $scope.alert_type},
                url: '/api/AlertType/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/AlertType/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



