'use strict';

app.controller('AttendanceAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.attendances = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetAttendances = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Attendance/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.attendances) {
                $scope.attendances = response.data.data.attendances;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetAttendances();
    };



    $scope.delete_attendance = function(attendance_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteAttendanceInstanceCtrl',
            resolve: {
                attendance_id: function() {
                    return attendance_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetAttendances();
        }, function() {
            
        });
    };


    $scope.GetAttendances();


});



app.controller('ModalDeleteAttendanceInstanceCtrl', function($scope, $uibModalInstance, $http, attendance_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {attendance_id : attendance_id},
            url: '/api/Attendance/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


