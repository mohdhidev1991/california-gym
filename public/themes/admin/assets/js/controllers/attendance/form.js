'use strict';

app.controller('AttendanceFormController', function($scope, $http, $stateParams, $location) {

    $scope.attendance = null;
    $scope.active_switch = false;
    $scope.rejected_switch = false;
    $scope.attendance_types = null;
    $scope.timer_att_time = null;
    $scope.action = $stateParams.action;


    $scope.GetAttendance = function() {
        $http({
            method: 'GET',
            url: '/api/Attendance/get/attendance_id='+$stateParams.attendance_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.attendance = response.data.data;
                if($scope.attendance.active && $scope.attendance.active==="Y"){
                    $scope.active_switch = true;
                    $scope.rejected_switch = true;
                    $scope.timer_att_time = moment($scope.attendance.att_time,'HH:mm');
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetAttendanceTypes = function() {
        $http({
            method: 'GET',
            url: '/api/AttendanceType/get'
        }).then(function successCallback(response) {
            if (response.data.data.attendance_types.length) {
                $scope.attendance_types = response.data.data.attendance_types;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetAttendance();
    }else{
        $scope.attendance = {};
        $scope.attendance.new = true;
    }
    $scope.GetAttendanceTypes();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.attendance.active = "Y";
        }else{
            $scope.attendance.active = "N";
        }
        if($scope.rejected_switch===true){
            $scope.attendance.rejected = "Y";
        }else{
            $scope.attendance.rejected = "N";
        }
        $scope.attendance.att_time = moment($scope.timer_att_time).format('HH:mm');
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {attendance: $scope.attendance},
                url: '/api/Attendance/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/Attendances");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.attendance);
            $http({
                method: 'POST',
                data: {attendance: $scope.attendance},
                url: '/api/Attendance/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/Attendance/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



