'use strict';

app.controller('AttendanceStatusAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.attendance_statuss = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetAttendanceStatuss = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/AttendanceStatus/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.attendance_statuss) {
                $scope.attendance_statuss = response.data.data.attendance_statuss;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetAttendanceStatuss();
    };



    $scope.delete_attendance_status = function(attendance_status_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteAttendanceStatusInstanceCtrl',
            resolve: {
                attendance_status_id: function() {
                    return attendance_status_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetAttendanceStatuss();
        }, function() {
            
        });
    };


    $scope.GetAttendanceStatuss();


});



app.controller('ModalDeleteAttendanceStatusInstanceCtrl', function($scope, $uibModalInstance, $http, attendance_status_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {attendance_status_id : attendance_status_id},
            url: '/api/AttendanceStatus/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


