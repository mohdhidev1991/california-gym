'use strict';

app.controller('AttendanceStatusFormController', function($scope, $http, $stateParams, $location) {

    $scope.attendance_status = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetAttendanceStatus = function() {
        $http({
            method: 'GET',
            url: '/api/AttendanceStatus/get/attendance_status_id='+$stateParams.attendance_status_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.attendance_status = response.data.data;
                if($scope.attendance_status.active && $scope.attendance_status.active==="Y"){
                    $scope.active_switch = true;
                }
                console.log($scope.attendance_status);
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetAttendanceStatus();
    }else{
        $scope.attendance_status = {};
        $scope.attendance_status.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.attendance_status.active = "Y";
        }else{
            $scope.attendance_status.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {attendance_status: $scope.attendance_status},
                url: '/api/AttendanceStatus/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/AttendanceStatuss");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.attendance_status);
            $http({
                method: 'POST',
                data: {attendance_status: $scope.attendance_status},
                url: '/api/AttendanceStatus/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/AttendanceStatus/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



