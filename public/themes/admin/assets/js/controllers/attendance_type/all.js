'use strict';

app.controller('AttendanceTypeAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.attendance_types = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [10 , 50, 500, 999];

    $scope.GetAttendanceTypes = function() {
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/AttendanceType/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.attendance_types) {
                $scope.attendance_types = response.data.data.attendance_types;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetAttendanceTypes();
    };



    $scope.delete_attendance_type = function(attendance_type_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteAttendanceTypeInstanceCtrl',
            resolve: {
                attendance_type_id: function() {
                    return attendance_type_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetAttendanceTypes();
        }, function() {
            
        });
    };


    $scope.GetAttendanceTypes();


});



app.controller('ModalDeleteAttendanceTypeInstanceCtrl', function($scope, $uibModalInstance, $http, attendance_type_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {attendance_type_id : attendance_type_id},
            url: '/api/AttendanceType/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


