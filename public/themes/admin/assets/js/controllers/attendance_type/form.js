'use strict';

app.controller('AttendanceTypeFormController', function($scope, $http, $stateParams, $location) {

    $scope.attendance_type = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetAttendanceType = function() {
        $http({
            method: 'GET',
            url: '/api/AttendanceType/get/attendance_type_id='+$stateParams.attendance_type_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.attendance_type = response.data.data;
                if($scope.attendance_type.active && $scope.attendance_type.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetAttendanceType();
    }else{
        $scope.attendance_type = {};
        $scope.attendance_type.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.attendance_type.active = "Y";
        }else{
            $scope.attendance_type.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {attendance_type: $scope.attendance_type},
                url: '/api/AttendanceType/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/AttendanceTypes");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.attendance_type);
            $http({
                method: 'POST',
                data: {attendance_type: $scope.attendance_type},
                url: '/api/AttendanceType/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/AttendanceType/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



