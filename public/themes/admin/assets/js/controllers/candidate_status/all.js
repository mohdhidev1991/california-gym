'use strict';

app.controller('CandidateStatusAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.candidate_statuss = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetCandidateStatuss = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/CandidateStatus/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.candidate_statuss) {
                $scope.candidate_statuss = response.data.data.candidate_statuss;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCandidateStatuss();
    };



    $scope.delete_candidate_status = function(candidate_status_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCandidateStatusInstanceCtrl',
            resolve: {
                candidate_status_id: function() {
                    return candidate_status_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCandidateStatuss();
        }, function() {
            
        });
    };


    $scope.GetCandidateStatuss();


});



app.controller('ModalDeleteCandidateStatusInstanceCtrl', function($scope, $uibModalInstance, $http, candidate_status_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {candidate_status_id : candidate_status_id},
            url: '/api/CandidateStatus/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


