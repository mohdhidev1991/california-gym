'use strict';

app.controller('CandidateStatusFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.candidate_status = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetCandidateStatus = function() {
        $http({
            method: 'GET',
            url: '/api/CandidateStatus/get/candidate_status_id=' + $stateParams.candidate_status_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.candidate_status = response.data.data;
                if ($scope.candidate_status.active && $scope.candidate_status.active === "Y") {
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if ($scope.action === "edit") {
        $scope.GetCandidateStatus();
    } else {
        $scope.candidate_status = {};
        $scope.candidate_status.new = true;
    }


    $scope.pre_save = function() {
        if ($scope.active_switch === true) {
            $scope.candidate_status.active = "Y";
        } else {
            $scope.candidate_status.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location) {
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { candidate_status: $scope.candidate_status },
                url: '/api/CandidateStatus/save'
            }).then(function successCallback(response) {
                if (back_location) {
                    $state.go('candidate_status.all');
                } else {
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function() {
        if ($scope.pre_save()) {
            console.log($scope.candidate_status);
            $http({
                method: 'POST',
                data: { candidate_status: $scope.candidate_status },
                url: '/api/CandidateStatus/save'
            }).then(function successCallback(response) {
                console.log(response);
                if (response.data.data.id) {
                    $location.path("/CandidateStatus/edit/" + response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});