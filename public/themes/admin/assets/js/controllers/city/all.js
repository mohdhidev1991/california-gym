'use strict';

app.controller('CityAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.cities = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100,500];

    $scope.GetCities = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/City/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.cities) {
                $scope.cities = response.data.data.cities;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCities();
    };



    $scope.delete_city = function(city_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCityInstanceCtrl',
            resolve: {
                city_id: function() {
                    return city_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCities();
        }, function() {
            
        });
    };


    $scope.GetCities();


});



app.controller('ModalDeleteCityInstanceCtrl', function($scope, $uibModalInstance, $http, city_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {city_id : city_id},
            url: '/api/City/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

