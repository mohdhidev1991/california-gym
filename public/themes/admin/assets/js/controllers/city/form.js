'use strict';

app.controller('CityFormController', function($scope, $http, $stateParams, $location) {

    $scope.city = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.countries  = null;


    $scope.GetCity = function() {
        $http({
            method: 'GET',
            url: '/api/City/get/city_id='+$stateParams.city_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.city = response.data.data;

                if($scope.city.active && $scope.city.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };




    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.data.countries.length) {
                $scope.countries = response.data.data.countries;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if($scope.action==="edit"){
        $scope.GetCity();
    }else{
        $scope.city = {};
        $scope.city.new = true;
    }
    $scope.GetCountries();



    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.city.active = "Y";
        }else{
            $scope.city.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {city: $scope.city},
                url: '/api/City/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/cities");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {city: $scope.city},
                url: '/api/City/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/city/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



