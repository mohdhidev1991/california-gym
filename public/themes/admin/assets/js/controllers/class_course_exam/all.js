'use strict';

app.controller('ClassCourseExamAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.class_course_exams = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetClassCourseExams = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/ClassCourseExam/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.class_course_exams) {
                $scope.class_course_exams = response.data.data.class_course_exams;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetClassCourseExams();
    };



    $scope.delete_class_course_exam = function(class_course_exam_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteClassCourseExamInstanceCtrl',
            resolve: {
                class_course_exam_id: function() {
                    return class_course_exam_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetClassCourseExams();
        }, function() {
            
        });
    };


    $scope.GetClassCourseExams();


});



app.controller('ModalDeleteClassCourseExamInstanceCtrl', function($scope, $uibModalInstance, $http, class_course_exam_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {class_course_exam_id : class_course_exam_id},
            url: '/api/ClassCourseExam/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


