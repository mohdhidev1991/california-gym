'use strict';

app.controller('ClassCourseExamFormController', function($scope, $http, $stateParams, $state, $location) {

    $scope.class_course_exam = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetClassCourseExam = function() {
        $http({
            method: 'GET',
            url: '/api/ClassCourseExam/get/class_course_exam_id='+$stateParams.class_course_exam_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.class_course_exam = response.data.data;
                if($scope.class_course_exam.active && $scope.class_course_exam.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetClassCourseExam();
    }else{
        $scope.class_course_exam = {};
        $scope.class_course_exam.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.class_course_exam.active = "Y";
        }else{
            $scope.class_course_exam.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {class_course_exam: $scope.class_course_exam},
                url: '/api/ClassCourseExam/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $state.go("class_course_exam.all");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.class_course_exam);
            $http({
                method: 'POST',
                data: {class_course_exam: $scope.class_course_exam},
                url: '/api/ClassCourseExam/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $state.go("class_course_exam.edit", {class_course_exam_id: response.data.data.id} );
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



