'use strict';

app.controller('ClubAllController', function($scope, $http, $uibModal, $stateParams, $location, Notification, $filter) {

    $scope.clubs = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100];

    $scope.GetClubs = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Club/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.clubs = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetClubs();
    };


    $scope.GetRoles = function() {
        $http({
            method: 'GET',
            url: '/api/Role/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.roles = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.delete = function(club_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteClubInstanceCtrl',
            resolve: {
                club_id: function() {
                    return club_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetClubs();
        }, function() {
            
        });
    };


    $scope.GetClubs();
    $scope.GetRoles();
});

app.controller('ModalDeleteClubInstanceCtrl', function($scope, $uibModalInstance, $http, club_id, $location, Notification, $filter) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {club_id : club_id},
            url: '/api/Club/delete'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


