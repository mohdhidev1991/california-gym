'use strict';

app.controller('ClubFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.club = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.failed_get_data = false;
    $scope.roles = null;

    $scope.GetClub = function() {
        $http({
            method: 'GET',
            url: '/api/Club/get/club_id=' + $stateParams.club_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.club = response.data.data;
                if ($scope.club.active && $scope.club.active === "Y") {
                    $scope.active_switch = true;
                }
                if($scope.club.opening_hour_1){ $scope.timer_opening_hour_1 = moment($scope.club.opening_hour_1, 'HH:mm'); }
                if($scope.club.opening_hour_2){ $scope.timer_opening_hour_2 = moment($scope.club.opening_hour_2, 'HH:mm'); }
                if($scope.club.opening_hour_3){ $scope.timer_opening_hour_3 = moment($scope.club.opening_hour_3, 'HH:mm'); }
                if($scope.club.opening_hour_4){ $scope.timer_opening_hour_4 = moment($scope.club.opening_hour_4, 'HH:mm'); }
                if($scope.club.opening_hour_5){ $scope.timer_opening_hour_5 = moment($scope.club.opening_hour_5, 'HH:mm'); }
                if($scope.club.opening_hour_6){ $scope.timer_opening_hour_6 = moment($scope.club.opening_hour_6, 'HH:mm'); }
                if($scope.club.opening_hour_7){ $scope.timer_opening_hour_7 = moment($scope.club.opening_hour_7, 'HH:mm'); }

                if($scope.club.closing_hour_1){ $scope.timer_closing_hour_1 = moment($scope.club.closing_hour_1, 'HH:mm'); }
                if($scope.club.closing_hour_2){ $scope.timer_closing_hour_2 = moment($scope.club.closing_hour_2, 'HH:mm'); }
                if($scope.club.closing_hour_3){ $scope.timer_closing_hour_3 = moment($scope.club.closing_hour_3, 'HH:mm'); }
                if($scope.club.closing_hour_4){ $scope.timer_closing_hour_4 = moment($scope.club.closing_hour_4, 'HH:mm'); }
                if($scope.club.closing_hour_5){ $scope.timer_closing_hour_5 = moment($scope.club.closing_hour_5, 'HH:mm'); }
                if($scope.club.closing_hour_6){ $scope.timer_closing_hour_6 = moment($scope.club.closing_hour_6, 'HH:mm'); }
                if($scope.club.closing_hour_7){ $scope.timer_closing_hour_7 = moment($scope.club.closing_hour_7, 'HH:mm'); }

            }else{
                $scope.failed_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if ($scope.action === "edit") {
        $scope.GetClub();
    } else {
        $scope.club = {};
        $scope.club.new = true;
        $scope.club.active = "Y";
        $scope.active_switch = true;
    }

    $scope.pre_save = function() {
        if ($scope.active_switch === true) {
            $scope.club.active = "Y";
        } else {
            $scope.club.active = "N";
        }
        if($scope.timer_opening_hour_1){ $scope.club.opening_hour_1 = moment($scope.timer_opening_hour_1).format('HH:mm'); }else{$scope.club.opening_hour_1=null;}
        if($scope.timer_opening_hour_2){ $scope.club.opening_hour_2 = moment($scope.timer_opening_hour_2).format('HH:mm'); }else{$scope.club.opening_hour_2=null;}
        if($scope.timer_opening_hour_3){ $scope.club.opening_hour_3 = moment($scope.timer_opening_hour_3).format('HH:mm'); }else{$scope.club.opening_hour_3=null;}
        if($scope.timer_opening_hour_4){ $scope.club.opening_hour_4 = moment($scope.timer_opening_hour_4).format('HH:mm'); }else{$scope.club.opening_hour_4=null;}
        if($scope.timer_opening_hour_5){ $scope.club.opening_hour_5 = moment($scope.timer_opening_hour_5).format('HH:mm'); }else{$scope.club.opening_hour_5=null;}
        if($scope.timer_opening_hour_6){ $scope.club.opening_hour_6 = moment($scope.timer_opening_hour_6).format('HH:mm'); }else{$scope.club.opening_hour_6=null;}
        if($scope.timer_opening_hour_7){ $scope.club.opening_hour_7 = moment($scope.timer_opening_hour_7).format('HH:mm'); }else{$scope.club.opening_hour_7=null;}

        if($scope.timer_closing_hour_1){ $scope.club.closing_hour_1 = moment($scope.timer_closing_hour_1).format('HH:mm'); }else{$scope.club.closing_hour_1=null;}
        if($scope.timer_closing_hour_2){ $scope.club.closing_hour_2 = moment($scope.timer_closing_hour_2).format('HH:mm'); }else{$scope.club.closing_hour_2=null;}
        if($scope.timer_closing_hour_3){ $scope.club.closing_hour_3 = moment($scope.timer_closing_hour_3).format('HH:mm'); }else{$scope.club.closing_hour_3=null;}
        if($scope.timer_closing_hour_4){ $scope.club.closing_hour_4 = moment($scope.timer_closing_hour_4).format('HH:mm'); }else{$scope.club.closing_hour_4=null;}
        if($scope.timer_closing_hour_5){ $scope.club.closing_hour_5 = moment($scope.timer_closing_hour_5).format('HH:mm'); }else{$scope.club.closing_hour_5=null;}
        if($scope.timer_closing_hour_6){ $scope.club.closing_hour_6 = moment($scope.timer_closing_hour_6).format('HH:mm'); }else{$scope.club.closing_hour_6=null;}
        if($scope.timer_closing_hour_7){ $scope.club.closing_hour_7 = moment($scope.timer_closing_hour_7).format('HH:mm'); }else{$scope.club.closing_hour_7=null;}

        return true;
    };

    $scope.save = function(back_location) {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { club: $scope.club },
                url: '/api/Club/save'
            }).then(function successCallback(response) {
                if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }
                if (response.data.status && back_location) {
                    $state.go('setting.club.all');
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function() {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { club: $scope.club },
                url: '/api/Club/save'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $state.go('setting.club.edit', { club_id: response.data.data });
                }else{
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});

angular.module('app.directives', [])
    .directive('pwCheck', [function() {
        return {
            require: 'ngModel',
            link: function(scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function() {
                    scope.$apply(function() {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        };
    }]);