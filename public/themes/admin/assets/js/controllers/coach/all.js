'use strict';

app.controller('CoachAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.coachs = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100];
    
    
    
    $scope.GetCoachs = function() {
        var selectCourse ;
        if($scope.course != null){
            selectCourse= $scope.course.id;
        }else{
            selectCourse= 0;
        }
        $scope.inputName=$("#nameCoach").val();
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;
        $scope.Params += ',selectConcept='+selectCourse;
        $scope.Params += ',inputName='+$scope.inputName;
        $scope.url_export_coach = null;

        console.log($scope.Params);
        $http({
            method: 'GET',
            url: '/api/Coach/get/'+$scope.Params
        }).then(function successCallback(response) {
            $scope.url_export_coach = window.config.base_url + '/api/Coach/get/' + $scope.Params + ',export=csv';   
            
            if (response.data.data) {
                $scope.coachs = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCourses = function() {
        $http({
            method: 'GET',
            url: '/api/Course/get/'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.select_cours = function(cours) {
        $scope.course = cours;
        $scope.GetCoachs();
    }

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCoachs();
    };


    $scope.delete = function(coach_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCoachInstanceCtrl',
            resolve: {
                coach_id: function() {
                    return coach_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCoachs();
        }, function() {
            
        });
    };
    
    $scope.search_coach = function() {
        $scope.Params = 'course='+$scope.course.id;
        var name_coach = document.getElementById('input-search-coach').value ;
        $scope.name_coach = name_coach ;
        $scope.Params += ',search_name_coach='+$scope.name_coach;
        console.log($scope.Params);
        $http({
            method: 'GET',
            url: '/api/Coach/search/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.coachs = response.data.data;
                
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    $scope.GetCoachs();

    $scope.GetCourses();
});

app.controller('ModalDeleteCoachInstanceCtrl', function($scope, $uibModalInstance, $http, coach_id, $location, Notification, $filter) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {coach_id : coach_id},
            url: '/api/Coach/delete'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


