'use strict';

app.controller('CoachFormController', function($scope, $http, $stateParams, $location, $state, Notification, $filter , $uibModal) {

    $scope.coach = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.failed_get_data = false;
    $scope.courses = [];
    $scope.clubs = [];

    $scope.GetCoach = function(){

        $http({
            method: 'GET',
            url: '/api/Coach/get/coach_id=' + $stateParams.coach_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.coach = response.data.data;
                if ($scope.coach.active && $scope.coach.active === "Y") {
                    $scope.active_switch = true;
                }

                if ($scope.coach.morning_starting_hour_1) { $scope.morning_starting_hour_1 = moment($scope.coach.morning_starting_hour_1, 'HH:mm'); }
                if ($scope.coach.morning_starting_hour_2) { $scope.morning_starting_hour_2 = moment($scope.coach.morning_starting_hour_2, 'HH:mm'); }
                if ($scope.coach.morning_starting_hour_3) { $scope.morning_starting_hour_3 = moment($scope.coach.morning_starting_hour_3, 'HH:mm'); }
                if ($scope.coach.morning_starting_hour_4) { $scope.morning_starting_hour_4 = moment($scope.coach.morning_starting_hour_4, 'HH:mm'); }
                if ($scope.coach.morning_starting_hour_5) { $scope.morning_starting_hour_5 = moment($scope.coach.morning_starting_hour_5, 'HH:mm'); }
                if ($scope.coach.morning_starting_hour_6) { $scope.morning_starting_hour_6 = moment($scope.coach.morning_starting_hour_6, 'HH:mm'); }
                if ($scope.coach.morning_starting_hour_7) { $scope.morning_starting_hour_7 = moment($scope.coach.morning_starting_hour_7, 'HH:mm'); }


                if ($scope.coach.morning_ending_hour_1) { $scope.morning_ending_hour_1 = moment($scope.coach.morning_ending_hour_1, 'HH:mm'); }
                if ($scope.coach.morning_ending_hour_2) { $scope.morning_ending_hour_2 = moment($scope.coach.morning_ending_hour_2, 'HH:mm'); }
                if ($scope.coach.morning_ending_hour_3) { $scope.morning_ending_hour_3 = moment($scope.coach.morning_ending_hour_3, 'HH:mm'); }
                if ($scope.coach.morning_ending_hour_4) { $scope.morning_ending_hour_4 = moment($scope.coach.morning_ending_hour_4, 'HH:mm'); }
                if ($scope.coach.morning_ending_hour_5) { $scope.morning_ending_hour_5 = moment($scope.coach.morning_ending_hour_5, 'HH:mm'); }
                if ($scope.coach.morning_ending_hour_6) { $scope.morning_ending_hour_6 = moment($scope.coach.morning_ending_hour_6, 'HH:mm'); }
                if ($scope.coach.morning_ending_hour_7) { $scope.morning_ending_hour_7 = moment($scope.coach.morning_ending_hour_7, 'HH:mm'); }



                if ($scope.coach.afternoon_starting_hour_1) { $scope.afternoon_starting_hour_1 = moment($scope.coach.afternoon_starting_hour_1, 'HH:mm'); }
                if ($scope.coach.afternoon_starting_hour_2) { $scope.afternoon_starting_hour_2 = moment($scope.coach.afternoon_starting_hour_2, 'HH:mm'); }
                if ($scope.coach.afternoon_starting_hour_3) { $scope.afternoon_starting_hour_3 = moment($scope.coach.afternoon_starting_hour_3, 'HH:mm'); }
                if ($scope.coach.afternoon_starting_hour_4) { $scope.afternoon_starting_hour_4 = moment($scope.coach.afternoon_starting_hour_4, 'HH:mm'); }
                if ($scope.coach.afternoon_starting_hour_5) { $scope.afternoon_starting_hour_5 = moment($scope.coach.afternoon_starting_hour_5, 'HH:mm'); }
                if ($scope.coach.afternoon_starting_hour_6) { $scope.afternoon_starting_hour_6 = moment($scope.coach.afternoon_starting_hour_6, 'HH:mm'); }
                if ($scope.coach.afternoon_starting_hour_7) { $scope.afternoon_starting_hour_7 = moment($scope.coach.afternoon_starting_hour_7, 'HH:mm'); }


                if ($scope.coach.afternoon_ending_hour_1) { $scope.afternoon_ending_hour_1 = moment($scope.coach.afternoon_ending_hour_1, 'HH:mm'); }
                if ($scope.coach.afternoon_ending_hour_2) { $scope.afternoon_ending_hour_2 = moment($scope.coach.afternoon_ending_hour_2, 'HH:mm'); }
                if ($scope.coach.afternoon_ending_hour_3) { $scope.afternoon_ending_hour_3 = moment($scope.coach.afternoon_ending_hour_3, 'HH:mm'); }
                if ($scope.coach.afternoon_ending_hour_4) { $scope.afternoon_ending_hour_4 = moment($scope.coach.afternoon_ending_hour_4, 'HH:mm'); }
                if ($scope.coach.afternoon_ending_hour_5) { $scope.afternoon_ending_hour_5 = moment($scope.coach.afternoon_ending_hour_5, 'HH:mm'); }
                if ($scope.coach.afternoon_ending_hour_6) { $scope.afternoon_ending_hour_6 = moment($scope.coach.afternoon_ending_hour_6, 'HH:mm'); }
                if ($scope.coach.afternoon_ending_hour_7) { $scope.afternoon_ending_hour_7 = moment($scope.coach.afternoon_ending_hour_7, 'HH:mm'); }
                

            } else {
                $scope.failed_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetUsers = function() {
        $http({
            method: 'GET',
            url: '/api/ReaUser/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.users = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetUsers();


    $scope.GetClubs = function() {
        $http({
            method: 'GET',
            url: '/api/Club/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetClubs();


    $scope.GetCourses = function() {
        $http({
            method: 'GET',
            url: '/api/Course/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCourses();


    $scope.GetWdays = function() {
        $http({
            method: 'GET',
            url: '/api/Wday/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.wdays = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetWdays();



    $scope.check_selected_option = function(id, $model) {
        if (!$model) return false;
        return $model.indexOf(id) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model) {
        var index_to_remove = $model.indexOf(id);
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        } else {
            $model.push(id);
        }
    };


    if ($scope.action === "edit") {
        $scope.GetCoach();
    } else {
        $scope.coach = {};
        $scope.coach.new = true;
        $scope.coach.active = "Y";
        $scope.active_switch = true;
        $scope.coach.clubs = [];
        $scope.coach.courses = [];
        $scope.coach.disponibilities = [];
        
    }

    $scope.pre_save = function() {
        
        
        if ($scope.active_switch === true) {
            $scope.coach.active = "Y";
        } else {
            $scope.coach.active = "N";
        }
        $scope.coach.courses_mfk = null;
        if ($scope.coach.courses) {
            $scope.coach.courses_mfk = ',' + $scope.coach.courses.join(',') + ',';
        }
        $scope.coach.clubs_mfk = null;
        if ($scope.coach.clubs) {
            $scope.coach.clubs_mfk = ',' + $scope.coach.clubs.join(',') + ',';
        }
        $scope.coach.disponibility = null;
        if ($scope.coach.disponibilities) {
            $scope.coach.disponibility = ',' + $scope.coach.disponibilities.join(',') + ',';
        }



        if ($scope.morning_starting_hour_1) { $scope.coach.morning_starting_hour_1 = moment($scope.morning_starting_hour_1).format('HH:mm'); } else { $scope.coach.morning_starting_hour_1 = null; }
        if ($scope.morning_starting_hour_2) { $scope.coach.morning_starting_hour_2 = moment($scope.morning_starting_hour_2).format('HH:mm'); } else { $scope.coach.morning_starting_hour_2 = null; }
        if ($scope.morning_starting_hour_3) { $scope.coach.morning_starting_hour_3 = moment($scope.morning_starting_hour_3).format('HH:mm'); } else { $scope.coach.morning_starting_hour_3 = null; }
        if ($scope.morning_starting_hour_4) { $scope.coach.morning_starting_hour_4 = moment($scope.morning_starting_hour_4).format('HH:mm'); } else { $scope.coach.morning_starting_hour_4 = null; }
        if ($scope.morning_starting_hour_5) { $scope.coach.morning_starting_hour_5 = moment($scope.morning_starting_hour_5).format('HH:mm'); } else { $scope.coach.morning_starting_hour_5 = null; }
        if ($scope.morning_starting_hour_6) { $scope.coach.morning_starting_hour_6 = moment($scope.morning_starting_hour_6).format('HH:mm'); } else { $scope.coach.morning_starting_hour_6 = null; }
        if ($scope.morning_starting_hour_7) { $scope.coach.morning_starting_hour_7 = moment($scope.morning_starting_hour_7).format('HH:mm'); } else { $scope.coach.morning_starting_hour_7 = null; }

        if ($scope.morning_ending_hour_1) { $scope.coach.morning_ending_hour_1 = moment($scope.morning_ending_hour_1).format('HH:mm'); } else { $scope.coach.morning_ending_hour_1 = null; }
        if ($scope.morning_ending_hour_2) { $scope.coach.morning_ending_hour_2 = moment($scope.morning_ending_hour_2).format('HH:mm'); } else { $scope.coach.morning_ending_hour_2 = null; }
        if ($scope.morning_ending_hour_3) { $scope.coach.morning_ending_hour_3 = moment($scope.morning_ending_hour_3).format('HH:mm'); } else { $scope.coach.morning_ending_hour_3 = null; }
        if ($scope.morning_ending_hour_4) { $scope.coach.morning_ending_hour_4 = moment($scope.morning_ending_hour_4).format('HH:mm'); } else { $scope.coach.morning_ending_hour_4 = null; }
        if ($scope.morning_ending_hour_5) { $scope.coach.morning_ending_hour_5 = moment($scope.morning_ending_hour_5).format('HH:mm'); } else { $scope.coach.morning_ending_hour_5 = null; }
        if ($scope.morning_ending_hour_6) { $scope.coach.morning_ending_hour_6 = moment($scope.morning_ending_hour_6).format('HH:mm'); } else { $scope.coach.morning_ending_hour_6 = null; }
        if ($scope.morning_ending_hour_7) { $scope.coach.morning_ending_hour_7 = moment($scope.morning_ending_hour_7).format('HH:mm'); } else { $scope.coach.morning_ending_hour_7 = null; }


        if ($scope.afternoon_starting_hour_1) { $scope.coach.afternoon_starting_hour_1 = moment($scope.afternoon_starting_hour_1).format('HH:mm'); } else { $scope.coach.afternoon_starting_hour_1 = null; }
        if ($scope.afternoon_starting_hour_2) { $scope.coach.afternoon_starting_hour_2 = moment($scope.afternoon_starting_hour_2).format('HH:mm'); } else { $scope.coach.afternoon_starting_hour_2 = null; }
        if ($scope.afternoon_starting_hour_3) { $scope.coach.afternoon_starting_hour_3 = moment($scope.afternoon_starting_hour_3).format('HH:mm'); } else { $scope.coach.afternoon_starting_hour_3 = null; }
        if ($scope.afternoon_starting_hour_4) { $scope.coach.afternoon_starting_hour_4 = moment($scope.afternoon_starting_hour_4).format('HH:mm'); } else { $scope.coach.afternoon_starting_hour_4 = null; }
        if ($scope.afternoon_starting_hour_5) { $scope.coach.afternoon_starting_hour_5 = moment($scope.afternoon_starting_hour_5).format('HH:mm'); } else { $scope.coach.afternoon_starting_hour_5 = null; }
        if ($scope.afternoon_starting_hour_6) { $scope.coach.afternoon_starting_hour_6 = moment($scope.afternoon_starting_hour_6).format('HH:mm'); } else { $scope.coach.afternoon_starting_hour_6 = null; }
        if ($scope.afternoon_starting_hour_7) { $scope.coach.afternoon_starting_hour_7 = moment($scope.afternoon_starting_hour_7).format('HH:mm'); } else { $scope.coach.afternoon_starting_hour_7 = null; }

        if ($scope.afternoon_ending_hour_1) { $scope.coach.afternoon_ending_hour_1 = moment($scope.afternoon_ending_hour_1).format('HH:mm'); } else { $scope.coach.afternoon_ending_hour_1 = null; }
        if ($scope.afternoon_ending_hour_2) { $scope.coach.afternoon_ending_hour_2 = moment($scope.afternoon_ending_hour_2).format('HH:mm'); } else { $scope.coach.afternoon_ending_hour_2 = null; }
        if ($scope.afternoon_ending_hour_3) { $scope.coach.afternoon_ending_hour_3 = moment($scope.afternoon_ending_hour_3).format('HH:mm'); } else { $scope.coach.afternoon_ending_hour_3 = null; }
        if ($scope.afternoon_ending_hour_4) { $scope.coach.afternoon_ending_hour_4 = moment($scope.afternoon_ending_hour_4).format('HH:mm'); } else { $scope.coach.afternoon_ending_hour_4 = null; }
        if ($scope.afternoon_ending_hour_5) { $scope.coach.afternoon_ending_hour_5 = moment($scope.afternoon_ending_hour_5).format('HH:mm'); } else { $scope.coach.afternoon_ending_hour_5 = null; }
        if ($scope.afternoon_ending_hour_6) { $scope.coach.afternoon_ending_hour_6 = moment($scope.afternoon_ending_hour_6).format('HH:mm'); } else { $scope.coach.afternoon_ending_hour_6 = null; }
        if ($scope.afternoon_ending_hour_7) { $scope.coach.afternoon_ending_hour_7 = moment($scope.afternoon_ending_hour_7).format('HH:mm'); } else { $scope.coach.afternoon_ending_hour_7 = null; }

        return true;
    };

    $scope.save = function(back_location) {
        $scope.errors_form = null;
        $scope.success_form = null;
        console.log($scope.coach);
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { coach: $scope.coach },
                url: '/api/Coach/save'
            }).then(function successCallback(response) {
                if (!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
                if (response.data.status && back_location) {
                    $state.go('setting.coach.all');
                } else if (response.data.status) {
                    $scope.success_form = true;
                } else if (!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function() {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { coach: $scope.coach },
                url: '/api/Coach/save'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    Notification.success({ message: $filter('translate')('Coach.NewCoachAddedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('setting.coach.edit', { coach_id: response.data.data });
                } else {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.upload_photo = function(feed_import_id){
        
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalUploadFilePhoto.html',
            controller: 'ModalUploadFilePhotoInstanceCtrl',
            resolve: {}
        });

        modalInstance.result.then(function(files) {
            if(files && files[0]){
                $scope.coach.photo_file_id = files[0].file_id;
            }
        }, function() {

        });
    };

    $scope.photo_src_url = function() {
        if(!$scope.coach || !$scope.coach.photo_file_id){
            return null;
        }
        return window.config.public_file_url+'/'+$scope.coach.photo_file_id;
    };
    
    $scope.remove_photo = function(item) {
        $scope.coach.photo_file_id = null;
    };


});



app.controller('ModalUploadFilePhotoInstanceCtrl', function($scope, $uibModalInstance, $http, FileUploader) {
    
    $scope.cancel_import = function() {
        $uibModalInstance.dismiss('cancel');
    };

    

    var uploader = $scope.uploader = new FileUploader({
        autoUpload: true,
        url: '/api/File/upload',
        formData: { module: 'photo', active: 'Y' },
    });

    // FILTERS
    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            return this.queue.length < 10;
        }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        $scope.success_upload = null;
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };

    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };

    uploader.onProgressItem = function(fileItem, progress) {
        //$scope.errors_upload = null;
        console.info('onProgressItem', fileItem, progress);
    };
    
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        console.info('onSuccessItem', fileItem, response, status, headers);
    };

    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };

    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };

    uploader.onCompleteItem = function(fileItem, response, status, headers) {

        $scope.errors_upload = null;
        if (response.status) {
            $scope.success_upload = true;
            fileItem.file_id = response.data;
        } else {
            $scope.uploader.queue = [];
            fileItem.isError = true;
            fileItem.isSuccess = false;
            fileItem.isUploaded = false;
            $scope.errors_upload = response.message;
        }
        //console.info('onCompleteItem', fileItem, response, status, headers);
    };

    uploader.onCompleteAll = function() {

        console.info('onCompleteAll');
        if($scope.success_upload){
            var files = $scope.uploader.queue;
            $uibModalInstance.close(files);
        }
    
    };

});