'use strict';

app.controller('CountryAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.countries = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100,500];

    $scope.GetCountries = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Country/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.countries) {
                $scope.countries = response.data.data.countries;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCountries();
    };



    $scope.delete_country = function(country_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCountryInstanceCtrl',
            resolve: {
                country_id: function() {
                    return country_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCountries();
        }, function() {
            
        });
    };


    $scope.GetCountries();


});



app.controller('ModalDeleteCountryInstanceCtrl', function($scope, $uibModalInstance, $http, country_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {country_id : country_id},
            url: '/api/Country/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

