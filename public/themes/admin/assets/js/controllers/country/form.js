'use strict';

app.controller('CountryFormController', function($scope, $http, $stateParams, $location) {

    $scope.country = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.timer_maintenance_start_time = null;
    $scope.timer_maintenance_end_time = null;
    $scope.dates_system  = null;
    $scope.wdays  = null;


    $scope.GetCountry = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get/country_id='+$stateParams.country_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.country = response.data.data;

                if($scope.country.maintenance_start_time){
                    $scope.timer_maintenance_start_time = moment($scope.country.maintenance_start_time, 'HH:mm');
                }
                
                if($scope.country.maintenance_end_time){
                    $scope.timer_maintenance_end_time = moment($scope.country.maintenance_end_time, 'HH:mm');    
                }

                if($scope.country.active && $scope.country.active==="Y"){
                    $scope.active_switch = true;
                }

                if(!$scope.country.we_days){
                    $scope.country.we_days = [];
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };




    $scope.GetDatesSystem = function() {
        $http({
            method: 'GET',
            url: '/api/DateSystem/get'
        }).then(function successCallback(response) {
            if (response.data.data.dates_system.length) {
                $scope.dates_system = response.data.data.dates_system;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetWdays = function() {
        $http({
            method: 'GET',
            url: '/api/Wday/get'
        }).then(function successCallback(response) {
            if (response.data.data.wdays.length) {
                $scope.wdays = response.data.data.wdays;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetIdnTypes = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get'
        }).then(function successCallback(response) {
            if (response.data.data.idn_types.length) {
                $scope.idn_types = response.data.data.idn_types;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetCountry();
    }else{
        $scope.country = {};
        $scope.country.new = true;
        $scope.country.we_days = [];
        $scope.country.sa_idn_types = [];
    }
    $scope.GetDatesSystem();
    $scope.GetWdays();
    $scope.GetIdnTypes();


    $scope.CheckboxObjectHelper = CheckboxObject.helper;

    $scope.pre_save = function(){

        if($scope.active_switch===true){
            $scope.country.active = "Y";
        }else{
            $scope.country.active = "N";
        }
        if($scope.timer_maintenance_start_time){
            $scope.country.maintenance_start_time = moment($scope.timer_maintenance_start_time).format('HH:mm');
        }
        if($scope.timer_maintenance_end_time){
            $scope.country.maintenance_end_time = moment($scope.timer_maintenance_end_time).format('HH:mm');
        }
        $scope.country.we_days_mfk = null;
        if($scope.country.we_days){
            $scope.country.we_days_mfk = ','+$scope.country.we_days.join(',')+',';
        }
        $scope.country.sa_idn_type_mfk = null;
        if($scope.country.sa_idn_types){
            $scope.country.sa_idn_type_mfk = ','+$scope.country.sa_idn_types.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {country: $scope.country},
                url: '/api/Country/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/countries");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {country: $scope.country},
                url: '/api/Country/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/country/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



