'use strict';

app.controller('CourseAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.courses = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100];

    $scope.GetCourses = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Course/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCourses();
    };



    $scope.delete = function(course_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseInstanceCtrl',
            resolve: {
                course_id: function() {
                    return course_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCourses();
        }, function() {
            
        });
    };


    $scope.GetCourses();


});



app.controller('ModalDeleteCourseInstanceCtrl', function($scope, $uibModalInstance, $http, course_id, $location, Notification, $filter) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {course_id : course_id},
            url: '/api/Course/delete'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

