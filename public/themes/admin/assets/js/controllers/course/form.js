'use strict';

app.controller('CourseFormController', function($scope, $rootScope, $http, $stateParams, $location, $state, Notification, $filter, $uibModal) {

    $scope.course = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.logos = [];

    
    console.log($stateParams.course_id);
    $scope.GetCourse = function() {
        $http({
            method: 'GET',
            url: '/api/Course/get/course_id='+$stateParams.course_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.course = response.data.data;
                if($scope.course.active && $scope.course.active==="Y"){
                    $scope.active_switch = true;
                }
            }else{
                $scope.failed_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCourseTypes = function() {
        $http({
            method: 'GET',
            url: '/api/CourseType/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.course_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCourseFors = function() {
        $http({
            method: 'GET',
            url: '/api/CourseFor/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.course_fors = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetClubs = function() {
        $http({
            method: 'GET',
            url: '/api/Club/get/join_obj=room'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };



    if($scope.action==="edit"){
        $scope.GetCourse();
    }else{
        $scope.course = {};
        $scope.course.new = true;
        $scope.course.active = "Y";
        $scope.active_switch = true;
        $scope.course.course_fors = [];
        $scope.course.rooms = [];
    }
    $scope.GetCourseFors();
    $scope.GetCourseTypes();
    $scope.GetClubs();

    $scope.CheckboxObjectHelper = CheckboxObject.helper;

    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.course.active = "Y";
        }else{
            $scope.course.active = "N";
        }

        $scope.course.course_for_mfk = null;
        if($scope.course.course_fors){
            $scope.course.course_for_mfk = ','+$scope.course.course_fors.join(',')+',';
        }

        $scope.course.rooms_mfk = null;
        if($scope.course.rooms){
            $scope.course.rooms_mfk = ','+$scope.course.rooms.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location){
        $scope.errors_form = null;
        $scope.success_form = null;
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {course: $scope.course},
                url: '/api/Course/save'
            }).then(function successCallback(response){
                
                if(!response.data.status){
                $scope.errors_form = response.data.message;
                }

                if (response.data.status && back_location){
                    $state.go('setting.course.all');
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }


            
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        $scope.errors_form = null;
        $scope.success_form = null;
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {course: $scope.course},
                url: '/api/Course/save'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    Notification.success({ message: $filter('translate')('Course.NewCourseAddedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('setting.course.edit', { course_id: response.data.data });
                }else{
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.upload_logo = function(feed_import_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalUploadFileLogo.html',
            controller: 'ModalUploadFileLogoInstanceCtrl',
            resolve: {}
        });
        modalInstance.result.then(function(files) {
            if(files && files[0]){
                $scope.course.logo_file_id = files[0].file_id;
            }
        }, function() {

        });
    };

    $scope.logo_src_url = function() {
        if(!$scope.course || !$scope.course.logo_file_id){
            return null;
        }
        return window.config.public_file_url+'/'+$scope.course.logo_file_id;
    };

    $scope.remove_logo = function(item) {
        $scope.course.logo_file_id = null;
    };


});





app.controller('ModalUploadFileLogoInstanceCtrl', function($scope, $uibModalInstance, $http, FileUploader) {

    $scope.cancel_import = function() {
        $uibModalInstance.dismiss('cancel');
    };

    var uploader = $scope.uploader = new FileUploader({
        autoUpload: true,
        url: '/api/File/upload',
        formData: { module: 'logo', active: 'Y' },
    });

    // FILTERS
    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            return this.queue.length < 10;
        }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        $scope.success_upload = null;
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        //$scope.errors_upload = null;
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        if (response.status) {
            $scope.success_upload = true;
            fileItem.file_id = response.data;
        } else {
            $scope.uploader.queue = [];
            fileItem.isError = true;
            fileItem.isSuccess = false;
            fileItem.isUploaded = false;
            $scope.errors_upload = response.message;
        }
        //console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
        if($scope.success_upload){
            var files = $scope.uploader.queue;
            $uibModalInstance.close(files);
        }
    };

    //console.info('uploader', uploader);
});


