'use strict';

app.controller('CourseSessionCurrentController', function($scope, $http, $stateParams, $location) {

    $scope.students = null;
    $scope.current_course_session = null;
    $scope.students = null;
    $scope.total_students = 0;
    $scope.present_students = 0;


    $scope.GetCurrentCourseSession = function() {
        var url_ = '/api/courseSession/current';
        if($stateParams.date){
            url_ = '/api/courseSession/current/' + $stateParams.date;
        }
        $http({
            method: 'GET',
            url: url_
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.current_course_session = response.data.data;
                $scope.LoadListStudents();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.LoadListStudents = function() {
        $http({
            method: 'POST',
            data: $scope.current_course_session,
            url: '/api/CourseSession/listStudentsByCourseSession'
        }).then(function successCallback(response) {
            if (response.data.data.length) {
                $scope.students = response.data.data;
                
            } else {
                $scope.students = false;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCurrentCourseSession();

    $scope.count_total_students = function(){
        if(!$scope.students) return 0;
        return $scope.students.length;
    };

    $scope.count_present_students = function(){
        if(!$scope.students) return 0;
        var count = 0;
        angular.forEach($scope.students, function(student, key) {
            if (student.coming_status_id==="1") {
                count++;
            }
        });
        return count;
    };

    $scope.count_absent_students = function(){
        if(!$scope.students) return 0;
        var count = 0;
        angular.forEach($scope.students, function(student, key) {
            if (student.coming_status_id==="6") {
                count++;
            }
        });
        return count;
    };

    $scope.count_late_students = function(){
        if(!$scope.students) return 0;
        var count = 0;
        angular.forEach($scope.students, function(student, key) {
            if (student.coming_status_id==="4") {
                count++;
            }
        });
        return count;
    };

    $scope.count_waiting_students = function(){
        if(!$scope.students) return 0;
        var count = 0;
        angular.forEach($scope.students, function(student, key) {
            if (student.coming_status_id==="2") {
                count++;
            }
        });
        return count;
    };


    $scope.finish_course_session = function() {
        $http({
            method: 'POST',
            data: {'course_session' : $scope.current_course_session},
            url: '/api/CourseSession/close'
        }).then(function successCallback(response) {
            console.log(response);
            $location.path( "/course_session/timeline");
        }, function errorCallback(response) {
            console.log('error');
        });
    };

});


