'use strict';

app.controller('CourseSessionEditCurrentController', function($scope, $http, $stateParams, $uibModal, $state) {

    $scope.students = null;
    $scope.course_session = null;
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.can_submit_save_edit = false;


    $scope.LoadListStudents = function() {
        $http({
            method: 'POST',
            data: {hday_num:$scope.course_session.hday_num,hmonth:$scope.course_session.hmonth,level_class_id:$scope.course_session.level_class_id,session_order:$scope.course_session.session_order,symbol:$scope.course_session.symbol,year:$scope.course_session.year},
            url: '/api/CourseSession/listStudentsByCourseSession'
        }).then(function successCallback(response) {
            if (response.data.data.length) {
                $scope.students = response.data.data;
                $scope.check_can_submit_save_edit();
            } else {
                $scope.students = false;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.LoadCourseSessionInfos = function() {

        var url_ = '/api/courseSession/current';
        if($stateParams.date){
            url_ = '/api/courseSession/current/' + $stateParams.date;
        }
        $http({
            method: 'GET',
            url: url_
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.course_session = response.data.data;
                $scope.LoadListStudents();
                //$scope.course_session.session_start_time = moment().format('HH:mm');
                $scope.uitime_global_coming_time = moment($scope.course_session.session_start_time, 'HH:mm');
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.LoadCourseSessionInfos();

    $scope.InitStudentPresence = function(student_session) {

        if (!student_session.coming_status_id) {
            student_session.coming_status_id = 1;
        }
        if (!student_session.exit_status_id) {
            student_session.exit_status_id = 1;
        }
        if (!student_session.coming_time && $scope.course_session) {
            student_session.coming_time = $scope.course_session.session_start_time;
            student_session.session_start_time = $scope.course_session.session_start_time;
        }
        if (!student_session.exit_time && $scope.course_session) {
            student_session.exit_time = $scope.course_session.session_end_time;
            student_session.session_end_time = $scope.course_session.session_end_time;
        }

        if (!student_session.timepicker) {
            student_session.timepicker = moment(student_session.coming_time, 'HH:mm');
            student_session.custom_coming_time = false;
        }

        if (student_session.coming_status_id == 4) {
            student_session.custom_coming_time = true;
        }
        return student_session;
    };


    $scope.update_global_coming_time = function() {
        angular.forEach($scope.students, function(student, key) {
            if (student.custom_coming_time===false) {
                student.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
                student.timepicker = $scope.uitime_global_coming_time;
            }
        });
        $scope.check_can_submit_save_edit();
    };


    $scope.update_student_coming_time = function(student) {
        student.student_session.coming_time = moment(student.student_session.timepicker).format('HH:mm');
    };


    $scope.set_coming_status_id = function(student, status_id) {
        student.student_session.coming_status_id = status_id;
        if(student.student_session.coming_status_id!=4){
            student.student_session.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
            student.student_session.custom_coming_time = false;
            student.student_session.timepicker = $scope.uitime_global_coming_time;
        }else{
            student.student_session.custom_coming_time = true;
        }

        $scope.check_can_submit_save_edit();
    };

    $scope.check_can_submit_save_edit = function() {
        var checked = 0;
        angular.forEach($scope.students, function(student, key) {
            if(student.coming_status_id!=3){
                checked++;
            }
        });
        if($scope.students && checked===$scope.students.length){
            $scope.can_submit_save_edit = true;
        }
    };



    

    $scope.submit_save_edit = function() {
        $scope.students_data_to_save = [];
        angular.forEach($scope.students, function(student, key) {
            var student_session_data = {
                student_id: student.student_id,
                attributes: {
                    coming_status_id: student.coming_status_id,
                    coming_time: student.coming_time,
                    comments: student.comments
                }
            };
            $scope.students_data_to_save.push(student_session_data);
        });
        $http({
            method: 'POST',
            data: {'course_session' : $scope.course_session,'students_session': $scope.students_data_to_save},
            url: '/api/CourseSession/StudentsSession/save'
        }).then(function successCallback(response) {
            $state.go( "course_session.current_with_date", {"date": $stateParams.date} );
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.add_comment = function(student_session) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCommentStudentSession.html',
            controller: 'ModalCommentStudentSessionInstanceCtrl',
            resolve: {
                student_session: function() {
                    return student_session;
                },
                course_session: function() {
                    return $scope.course_session;
                }
            }
        });
        modalInstance.result.then(function(student_session) {
            //$scope.TimelineCourseSession.splice(student_session.$index, 1);
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

});






app.controller('ModalCommentStudentSessionInstanceCtrl', function($scope, $uibModalInstance, $http, student_session, course_session) {
    $scope.student_session = student_session;
    $scope.course_session = course_session;
    $scope.submit = function() {
        $uibModalInstance.close(student_session);
    };

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});















