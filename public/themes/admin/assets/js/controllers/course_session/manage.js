'use strict';

app.controller('CourseSessionManageController', function($scope, $http, $stateParams, $location) {

    $scope.filter = {};
    $scope.course_sessions = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = 50;


    $scope.order_by = 'session_hdate';
    $scope.order = 'DESC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetCourseSessions();
    };


    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetCourseSessions();
    };



    $scope.GetCourseSessions = function() {

        if($scope.loading) return;

        $scope.empty_results = null;

        $scope.Params = 'join=all,active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }
        

        angular.forEach([
            'session_status_id',
            'prof_id',
            'course_id',
            'level_class_id',
            'session_hdate'
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });
        if( ($scope.filter.teacher) && (typeof $scope.filter.teacher.id) ){
            $scope.Params += ',prof_id='+$scope.filter.teacher.id;
        }


        $http({
            method: 'GET',
            url: '/api/CourseSession/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status) {
                $scope.course_sessions = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.course_sessions = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.course_sessions = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };



    $scope.GetSessionStatuss = function() {
        $scope.session_statuss = null;
        $http({
            method: 'GET',
            url: '/api/SessionStatus/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.session_statuss = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetSessionStatuss();




    $scope.GetLevelClasses = function() {
        $scope.level_classes = null;
        $http({
            method: 'GET',
            url: '/api/LevelClass/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.level_classes = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetLevelClasses();

    $scope.GetCourses = function() {
        $scope.courses = null;
        $http({
            method: 'GET',
            url: '/api/Course/get/current_school'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCourses();



    $scope.getTeachers = function(val) {
        return $http.get('/api/SchoolEmployee/get/school_job=teacher,join=rea_user,limit=20,key_search=' + encodeURI(val)).then(function(response) {
            return response.data.data.map(function(item) {
                item.fullname = item.firstname + ' ' + item.f_firstname + ' ' + item.lastname;
                return item;
            });
        });
    };




    $scope.GetCourseSessions();


});



