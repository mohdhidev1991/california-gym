'use strict';

app.controller('CourseSessionManageItemController', function($scope, $http, $stateParams, $uibModal, $state) {

    $scope.students = null;
    $scope.course_session = null;
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.can_submit_save_edit = false;
    $scope.alerts = [];


    $scope.LoadListStudents = function() {
        $scope.students = null;
        $scope.empty_students = false;
        $http({
            method: 'POST',
            data: {hday_num:$scope.course_session.hday_num,hmonth:$scope.course_session.hmonth,level_class_id:$scope.course_session.level_class_id,session_order:$scope.course_session.session_order,symbol:$scope.course_session.symbol,year:$scope.course_session.year},
            url: '/api/CourseSession/listStudentsByCourseSession'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.students = response.data.data;
                $scope.check_can_submit_save_edit();
            } else {
                $scope.empty_students = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.switch_edit_coming_status = function() {
        if($scope.edit_coming_status){
            $scope.LoadListStudents();
        }
    };




    $scope.LoadCourseSessionInfos = function() {
        $http({
            method: 'GET',
            url: '/api/CourseSession/get/single,join=all,year='+$stateParams.year+',hmonth='+$stateParams.hmonth+',hday_num='+$stateParams.hday_num+',level_class_id='+$stateParams.level_class_id+',symbol='+$stateParams.symbol+',symbol='+$stateParams.symbol+',session_order='+$stateParams.session_order
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.course_session = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.LoadCourseSessionInfos();

    $scope.InitStudentPresence = function(student_session) {

        if (!student_session.coming_status_id) {
            student_session.coming_status_id = 1;
        }
        if (!student_session.exit_status_id) {
            student_session.exit_status_id = 1;
        }
        if (!student_session.coming_time && $scope.course_session) {
            student_session.coming_time = $scope.course_session.session_start_time;
            student_session.session_start_time = $scope.course_session.session_start_time;
        }
        if (!student_session.exit_time && $scope.course_session) {
            student_session.exit_time = $scope.course_session.session_end_time;
            student_session.session_end_time = $scope.course_session.session_end_time;
        }

        if (!student_session.timepicker) {
            student_session.timepicker = moment(student_session.coming_time, 'HH:mm');
            student_session.custom_coming_time = false;
        }

        if (student_session.coming_status_id == 4) {
            student_session.custom_coming_time = true;
        }
        return student_session;
    };


    $scope.update_global_coming_time = function() {
        angular.forEach($scope.students, function(student, key) {
            if (student.custom_coming_time===false) {
                student.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
                student.timepicker = $scope.uitime_global_coming_time;
            }
        });
        $scope.check_can_submit_save_edit();
    };


    $scope.update_student_coming_time = function(student) {
        student.student_session.coming_time = moment(student.student_session.timepicker).format('HH:mm');
    };


    $scope.set_coming_status_id = function(student, status_id) {
        student.student_session.coming_status_id = status_id;
        if(student.student_session.coming_status_id!=4){
            student.student_session.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
            student.student_session.custom_coming_time = false;
            student.student_session.timepicker = $scope.uitime_global_coming_time;
        }else{
            student.student_session.custom_coming_time = true;
        }

        $scope.check_can_submit_save_edit();
    };

    $scope.check_can_submit_save_edit = function() {
        var checked = 0;
        angular.forEach($scope.students, function(student, key) {
            if(student.coming_status_id!=3){
                checked++;
            }
        });
        if($scope.students && checked===$scope.students.length){
            $scope.can_submit_save_edit = true;
        }
    };



    

    $scope.submit_save_edit = function() {
        $scope.students_data_to_save = [];
        angular.forEach($scope.students, function(student, key) {
            var student_session_data = {
                student_id: student.student_id,
                attributes: {
                    coming_status_id: student.coming_status_id,
                    coming_time: student.coming_time,
                    comments: student.comments
                }
            };
            $scope.students_data_to_save.push(student_session_data);
        });
        $http({
            method: 'POST',
            data: {'course_session' : $scope.course_session,'students_session': $scope.students_data_to_save},
            url: '/api/CourseSession/StudentsSession/save'
        }).then(function successCallback(response) {
            
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.cancel_course_session = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCancelCourseSession.html',
            controller: 'ModalCancelCourseSessionInstanceCtrl',
            resolve: {
                course_session: function() {
                    return $scope.course_session;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.alerts = [];
            $scope.alerts.push({msg: 'CourseSession.CourseSessionCanceledWithSuccess', type: 'success'});
            $scope.LoadCourseSessionInfos();
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };






    $scope.change_teacher_course_session = function(change_per) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalChangeTeacherCourseSession.html',
            controller: 'ModalChangeTeacherCourseSessionInstanceCtrl',
            resolve: {
                course_session: function() {
                    return $scope.course_session;
                },
                change_per: function() {
                    return change_per;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.alerts = [];
            $scope.alerts.push({msg: 'CourseSession.TeacherChangedWithSuccess', type: 'success'});
            $scope.LoadCourseSessionInfos();
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };


    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };


    $scope.add_comment = function(student_session) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCommentStudentSession.html',
            controller: 'ModalCommentStudentSessionInstanceCtrl',
            resolve: {
                student_session: function() {
                    return student_session;
                },
                course_session: function() {
                    return $scope.course_session;
                }
            }
        });
        modalInstance.result.then(function(student_session) {
            //$scope.TimelineCourseSession.splice(student_session.$index, 1);
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

});






app.controller('ModalCancelCourseSessionInstanceCtrl', function($scope, $uibModalInstance, $http, course_session) {
    $scope.course_session = angular.copy(course_session);
    $scope.submit = function() {
        $http({
            method: 'POST',
            data: $scope.course_session,
            url: '/api/CourseSession/admin_cancel'
        }).then(function successCallback(response) {
            if(response.data.status){
                $uibModalInstance.close();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



app.controller('ModalChangeTeacherCourseSessionInstanceCtrl', function($scope, $uibModalInstance, $http, course_session, change_per) {
    $scope.course_session = angular.copy(course_session);
    $scope.teacher = null;
    $scope.change_per = angular.copy(change_per);
    
    if($scope.change_per==='item'){
        $scope.session_status_comment = angular.copy($scope.course_session.session_status_comment);
    }
    if($scope.change_per==='items'){
        $scope.start_hdate = angular.copy($scope.course_session.session_hdate);
        $scope.session_status_comment = null;
    }
    


    $scope.getTeachers = function(val) {
        return $http.get('/api/SchoolEmployee/get/school_job=teacher,join=rea_user,limit=10,key_search=' + encodeURI(val)+',exclude_id='+$scope.course_session.prof_id+',course_id='+course_session.course_id).then(function(response) {
            return response.data.data.map(function(item) {
                item.fullname = item.firstname + ' ' + item.f_firstname + ' ' + item.lastname;
                return item;
            });
        });
    };


    $scope.submit = function() {
        $scope.success = null;
        $scope.errors = null;
        $http({
            method: 'POST',
            data: {course_session: $scope.course_session, teacher: $scope.teacher, change_per: $scope.change_per, start_hdate: $scope.start_hdate, session_status_comment: $scope.session_status_comment},
            url: '/api/CourseSession/change_teacher'
        }).then(function successCallback(response) {
            if(response.data.status){
                $uibModalInstance.close();
            }else{
                $scope.errors = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



app.controller('ModalCommentStudentSessionInstanceCtrl', function($scope, $uibModalInstance, $http, student_session, course_session) {
    $scope.student_session = student_session;
    $scope.course_session = course_session;
    $scope.from_hdate = $scope.course_session.session_hdate;
    $scope.submit = function() {
        $uibModalInstance.close(student_session);
    };

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});















