'use strict';

app.controller('CourseSessionOpenController', function($scope, $http, $stateParams, $location, $state, $uibModal) {

    $scope.course_session_fields = $stateParams;
    $scope.students = null;
    $scope.course_session = null;
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.can_submit_open_session = false;


    $scope.LoadListStudents = function() {
        $http({
            method: 'POST',
            data: $scope.course_session_fields,
            url: '/api/CourseSession/listStudentsByCourseSession'
        }).then(function successCallback(response) {
            if (response.data.data.length) {
                $scope.students = response.data.data;
                $scope.check_can_submit_open_session();
            } else {
                $scope.students = false;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.LoadCourseSessionInfos = function() {
        $http({
            method: 'GET',
            data: $scope.course_session_fields,
            url: '/api/courseSession/infos/' + $scope.course_session_fields.year + '/' + $scope.course_session_fields.hmonth + '/' + $scope.course_session_fields.hday_num+ '/' + $scope.course_session_fields.level_class_id+ '/' + $scope.course_session_fields.symbol+ '/' + $scope.course_session_fields.session_order
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.course_session = response.data.data;
                $scope.LoadListStudents();
                $scope.course_session.real_start_time = moment().format('HH:mm');
                $scope.uitime_global_coming_time = moment($scope.course_session.real_start_time, 'HH:mm');
                $scope.check_can_submit_open_session();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.LoadCourseSessionInfos();

    $scope.InitStudentPresence = function(student_session) {

        if (!student_session.coming_status_id) {
            student_session.coming_status_id = 1;
        }
        if (!student_session.exit_status_id) {
            student_session.exit_status_id = 1;
        }
        if (!student_session.coming_time && $scope.course_session) {
            student_session.coming_time = $scope.course_session.real_start_time;
            student_session.session_start_time = $scope.course_session.real_start_time;
        }
        if (!student_session.exit_time && $scope.course_session) {
            student_session.exit_time = $scope.course_session.session_end_time;
            student_session.session_end_time = $scope.course_session.session_end_time;
        }

        if (!student_session.timepicker) {
            student_session.timepicker = moment($scope.uitime_global_coming_time, 'HH:mm');
            student_session.custom_coming_time = false;
        }

        if (student_session.coming_status_id == 4) {
            student_session.custom_coming_time = true;
        }
        return student_session;
    };


    $scope.update_global_coming_time = function() {
        angular.forEach($scope.students, function(student, key) {
            if (student.custom_coming_time===false) {
                student.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
                student.timepicker = $scope.uitime_global_coming_time;
            }
        });
        $scope.check_can_submit_open_session();
    };


    $scope.update_student_coming_time = function(student) {
        student.student_session.coming_time = moment(student.student_session.timepicker).format('HH:mm');
    };


    $scope.set_coming_status_id = function(student, status_id) {
        
        student.student_session.coming_status_id = status_id;

        if(student.student_session.coming_status_id!=4){
            student.student_session.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
            student.student_session.custom_coming_time = false;
            student.student_session.timepicker = $scope.uitime_global_coming_time;
        }else{
            student.student_session.custom_coming_time = true;
        }

        $scope.check_can_submit_open_session();
    };

    $scope.check_can_submit_open_session = function() {
        var checked = 0;
        angular.forEach($scope.students, function(student, key) {
            if(student.coming_status_id!=3){
                checked++;
            }
        });
        if($scope.students && checked===$scope.students.length){
            $scope.can_submit_open_session = true;
        }
    };

    $scope.set_exit_status_id = function(student, status_id) {
        student.student_session.exit_status_id = status_id;
    };



    $scope.comment_student = function(student_session) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCommentOpenStudentSession.html',
            controller: 'ModalCommentOpenStudentSessionInstanceCtrl',
            resolve: {
                student_session: function() {
                    return student_session;
                },
                course_session: function() {
                    return $scope.course_session;
                }
            }
        });
        modalInstance.result.then(function(student_session) {
            //$scope.TimelineCourseSession.splice(student_session.$index, 1);
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };


    $scope.comment_course_session = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCommentOpenCourseSession.html',
            controller: 'ModalCommentOpenCourseSessionInstanceCtrl',
            resolve: {
                course_session: function() {
                    return $scope.course_session;
                }
            }
        });
        modalInstance.result.then(function(session_status_comment) {
            console.log(session_status_comment);
            $scope.course_session.session_status_comment = session_status_comment;
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };


    $scope.submit_open_session = function() {
        $scope.course_session.real_start_time = moment($scope.uitime_global_coming_time).format('HH:mm');
        $scope.students_data_to_save = [];
        angular.forEach($scope.students, function(student, key) {
            var student_session_student_data = {
                student_id: student.student_id,
                attributes: {
                    coming_status_id: student.coming_status_id,
                    coming_time: student.coming_time,
                    comments: student.comments
                }
            };
            $scope.students_data_to_save.push(student_session_student_data);
        });
        $http({
            method: 'POST',
            data: {  'course_session' : $scope.course_session, 'students_session': $scope.students_data_to_save},
            url: '/api/CourseSession/start'
        }).then(function successCallback(response) {
            if( response.data.data === true ){
                $state.go( "course_session.current", {data: $scope.course_session.session_hdate});
            }
            if( response.data.status === false ){
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'ModalErrorOpenCourseSession.html',
                    controller: 'ModalErrorOpenCourseSessionInstanceCtrl',
                    resolve: { errors: function(){ return response.data.message; } }
                });
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
    };

});







app.controller('ModalCommentOpenStudentSessionInstanceCtrl', function($scope, $uibModalInstance, $http, student_session, course_session) {
    $scope.student_session = student_session;
    $scope.course_session = course_session;
    $scope.submit = function() {
        $uibModalInstance.close($scope.student_session);
    };

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



app.controller('ModalCommentOpenCourseSessionInstanceCtrl', function($scope, $uibModalInstance, $http, course_session) {

    $scope.course_session = course_session;
    $scope.submit = function() {
        $uibModalInstance.close($scope.course_session.session_status_comment);
    };

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



app.controller('ModalErrorOpenCourseSessionInstanceCtrl', function($scope, $uibModalInstance, $state, errors) {
    
    $scope.errors = errors;
    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.go_current_couse_session = function() {
        $uibModalInstance.dismiss('cancel');
        $state.go('course_session.current');
    };
});




