'use strict';

app.controller('CourseSessionOpenController', function($scope, $http, $stateParams, $uibModal, CourseSessionFactory) {

    $scope.coursesession_fields = $stateParams;
    $scope.students = null;
    $scope.coursesession = CourseSessionFactory.get();
    $scope.hstep = 1;
    $scope.mstep = 1;


    $scope.LoadListStudents = function() {
        $http({
            method: 'POST',
            data: $scope.coursesession_fields,
            url: '/api/Student/listStudentsByCourseSession'
        }).then(function successCallback(response) {

            if (response.data.data.length) {
                $scope.students = response.data.data;
            } else {
                $scope.students = false;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if (!$scope.coursesession) {
        $http({
            method: 'GET',
            data: $scope.coursesession_fields,
            url: '/api/courseSession/infos/' + $scope.coursesession_fields.year + '/' + $scope.coursesession_fields.hmonth + '/' + $scope.coursesession_fields.hday_num
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.coursesession = response.data.data;
                $scope.LoadListStudents();
                $scope.uitime_global_coming_time = moment($scope.coursesession.session_start_time, 'hh:mm');
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    } else {
        $scope.LoadListStudents();
        $scope.uitime_global_coming_time = moment($scope.coursesession.session_start_time, 'hh:mm');
    }


    $scope.InitStudentPresence = function(item) {

        if (!item.coming_status_id) {
            item.coming_status_id = 1;
        }
        if (!item.exit_status_id) {
            item.exit_status_id = 1;
        }
        if (!item.coming_time && $scope.coursesession) {
            item.coming_time = $scope.coursesession.session_start_time;
            item.session_start_time = $scope.coursesession.session_start_time;
        }
        if (!item.exit_time && $scope.coursesession) {
            item.exit_time = $scope.coursesession.session_end_time;
            item.session_end_time = $scope.coursesession.session_end_time;
        }

        if (!item.timepicker) {
            item.timepicker = moment($scope.uitime_global_coming_time, 'hh:mm');
            item.custom_coming_time = false;
        }

        return item;
    };


    $scope.update_global_coming_time = function() {
        angular.forEach($scope.students, function(student, key) {
            if (student.custom_coming_time===false) {
                student.coming_time = moment($scope.uitime_global_coming_time).format('hh:mm');
                student.timepicker = $scope.uitime_global_coming_time;
            }
        });
    };


    $scope.update_student_coming_time = function(student) {
        console.log('here: update_student_coming_time');
        student.item.coming_time = moment(student.item.timepicker).format('hh:mm');
        student.item.custom_coming_time = true;
    };



    $scope.set_coming_status_id = function(student, status_id) {
        student.item.coming_status_id = status_id;
    };
    $scope.set_exit_status_id = function(student, status_id) {
        student.item.exit_status_id = status_id;
    };



    $scope.change_student_presence = function(student) {
        event.preventDefault();

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'front/tpl/coursesession/modal_student_presence',
            controller: 'ModalStudentPresenceInstanceCtrl',
            resolve: {
                student_tochange: function() {
                    return student;
                }
            }
        });
        modalInstance.result.then(function(student_tochange) {
            student.item = student_tochange;
            //$scope.TimelineCourseSession.splice(studant.$index, 1);
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };



});




app.controller('ModalStudentPresenceInstanceCtrl', function($scope, $uibModalInstance, $http, student_tochange) {

    $scope.valide = true;
    $scope.student = angular.copy(student_tochange.item);

    $scope.save = function() {
        if (!$scope.student.fullname) {
            $scope.valide = false;
        } else {
            $scope.valide = true;
            $uibModalInstance.close($scope.student);
        }
    };

    $scope.close_modal = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.update_coming_time = function() {
        $scope.student.coming_time = moment($scope.uitime_coming_time).format('hh:mm');

        $scope.student.exit_time = moment($scope.uitime_exit_time).format('hh:mm');
    };


    $scope.set_coming_status_id = function(status_id) {
        $scope.student.coming_status_id = status_id;
    };
    $scope.set_exit_status_id = function(status_id) {
        $scope.student.exit_status_id = status_id;
    };

    $scope.uitime_coming_time = moment($scope.student.coming_time, 'hh:mm');
    $scope.uitime_coming_time_min_time = moment($scope.student.session_start_time, 'hh:mm');
    $scope.uitime_coming_time_max_time = moment($scope.student.session_end_time, 'hh:mm');

    $scope.uitime_exit_time = moment($scope.student.exit_time, 'hh:mm');
    $scope.uitime_exit_time_min_time = moment($scope.student.session_start_time, 'hh:mm');
    $scope.uitime_exit_time_max_time = moment($scope.student.session_end_time, 'hh:mm');

    $scope.hstep = 1;
    $scope.mstep = 1;


});




