'use strict';

app.controller('ReplaceCoachController', function($scope, $http, $uibModal, $stateParams, $location , $filter) {

    $scope.url_export_replace_coach = null;
    moment.locale('fr');
    let DateStart =   $('#startDate').val();
    let EndStart =   $('#endDate').val();

    $scope.dateRange = {
        startDate: moment().format('YYYY-MM-DD'),
        endDate: moment().format('YYYY-MM-DD'),
        date: moment().format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD')
    };


    $scope.daterangepickerOptions = {
        format: 'YYYY-MM-DD',
        locale: {
            applyLabel: $filter('translate')('Global.Apply'),
            fromLabel: $filter('translate')('Global.From'),
            format: "YYYY-MM-DD",
            toLabel: $filter('translate')('Global.To'),
            cancelLabel: $filter('translate')('Global.Cancel')
                //customRangeLabel: 'Custom range'
        }
    };

    $scope.popup_datepicker = {
        date: moment().add(7, 'days').startOf('week').toDate(),
        opened: false,
    };


    $scope.date_format = 'M!/d!/yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: null,
        minDate: null,
        startingDay: 1,
        dateDisabled: function(option) {
            return (option.mode === 'day' && (option.date.getDay() !== 1));
        }
    };

    $scope.open_datepicker = function() {
        
        $('#daterangepicker').first().click();
    };

    $scope.disabledDates = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 1));
    };
    
    $scope.course_session_fins = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.state = "replace";
    $scope.optionsItemsPerPage = [10,50,100];
    $scope.list_export_replace_coach = null;
    $scope.GetCourseSessionFin = function() {

        $scope.dateRange.startDate = moment($scope.dateRange.date.substring(10, 0)).format('YYYY-MM-DD');
        $scope.dateRange.endDate = moment($scope.dateRange.date.substring(23, 13)).format('YYYY-MM-DD');
        $scope.Params = 'startdate=' + $scope.dateRange.startDate +',enddate=' + $scope.dateRange.endDate ;
        $scope.Params += 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;
        $scope.Params += ',state='+$scope.state;
        var club = sessionStorage.getItem("club");
        $scope.Params += ',club='+club;
        $http({
            method: 'GET',
            url: '/api/CourseSessionFin/get/'+$scope.Params
        }).then(function successCallback(response) {
            $scope.url_export_replace_coach = window.config.base_url + '/api/CourseSessionFin/get/' + $scope.Params + ',export=csv';
            console.log($scope.url_export_replace_coach);
            if (response.data.status) {
                $scope.list_export_replace_coach = response.data.data ;
                $scope.course_session_fins = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.list_export_replace_coach = null ;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCourseSessionFin();
    };

    $scope.delete = function(course_session_id){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCourseFinConfirmDelete.html',
            controller: 'ModalDeleteCourseFinInstanceCtrl',
            resolve: {
                course_session_id: function() {
                    return course_session_id;
                }
            }
        });

        modalInstance.result.then(function() {
            $scope.GetCourseSessionFin();
        }, function() {
            
        });
    };

    $scope.validate = function(course_session_id){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCourseFinConfirmValidate.html',
            controller: 'ModalValidateCourseFinInstanceCtrl',
            resolve: {
                course_session_id: function() {
                    return course_session_id;
                }
            }
        });

        modalInstance.result.then(function() {
            $scope.GetCourseSessionFin();
        }, function() {
            
        });
    };


    $scope.GetCourseSessionFin();


});


app.controller('ModalDeleteCourseFinInstanceCtrl', function($scope, $uibModalInstance, $http, course_session_id, $location, Notification, $filter) {
    
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {course_session_id : course_session_id},
            url: '/api/CourseSessionFin/replacerefuse'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataRefusedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                  angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


app.controller('ModalValidateCourseFinInstanceCtrl', function($scope, $uibModalInstance, $http, course_session_id, $location, Notification, $filter) {
    
    $scope.confirm_validate = function() {
        $http({
            method: 'POST',
            data: {course_session_id : course_session_id},
            url: '/api/CourseSessionFin/validreplace'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataValidatedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                  angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.cancel_confirm_validate = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
