'use strict';

app.controller('CourseTypeAllController', function($scope, $http, $uibModal, $stateParams, $location, Notification, $filter) {

    $scope.course_types = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100];

    $scope.GetCourseTypes = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/CourseType/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.course_types = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCourseTypes();
    };


    $scope.delete = function(course_type_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseTypeInstanceCtrl',
            resolve: {
                course_type_id: function() {
                    return course_type_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCourseTypes();
        }, function() {
            
        });
    };


    $scope.GetCourseTypes();
});

app.controller('ModalDeleteCourseTypeInstanceCtrl', function($scope, $uibModalInstance, $http, course_type_id, $location, Notification, $filter) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {course_type_id : course_type_id},
            url: '/api/CourseType/delete'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


