'use strict';

app.controller('CourseTypeFormController', function($scope, $http, $stateParams, $location, $state, Notification, $filter) {

    $scope.course_type = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.failed_get_data = false;
    $scope.roles = null;

    $scope.GetCourseType = function() {
        $http({
            method: 'GET',
            url: '/api/CourseType/get/course_type_id=' + $stateParams.course_type_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.course_type = response.data.data;
                if ($scope.course_type.active && $scope.course_type.active === "Y") {
                    $scope.active_switch = true;
                }
            }else{
                $scope.failed_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if ($scope.action === "edit") {
        $scope.GetCourseType();
    } else {
        $scope.course_type = {};
        $scope.course_type.new = true;
        $scope.course_type.active = "Y";
        $scope.active_switch = true;
    }

    $scope.pre_save = function() {
        if ($scope.active_switch === true) {
            $scope.course_type.active = "Y";
        } else {
            $scope.course_type.active = "N";
        }

        return true;
    };

    $scope.save = function(back_location) {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { course_type: $scope.course_type },
                url: '/api/CourseType/save'
            }).then(function successCallback(response) {
                if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }
                if (response.data.status && back_location) {
                    Notification.success({ message: $filter('translate')('Global.DataSavedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('setting.course_type.all');
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function() {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { course_type: $scope.course_type },
                url: '/api/CourseType/save'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    Notification.success({ message: $filter('translate')('Global.DataAddedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('setting.course_type.edit', { course_type_id: response.data.data });
                }else{
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});
