'use strict';

app.controller('CoursesConfigItemConfigItemController', function($scope, $http, $stateParams, $location) {
    $scope.courses_config_template_id = 0;
    $scope.courses_config_templates = null;
    $scope.school_level_id = 0;
    $scope.school_levels = null;
    $scope.level_class_id = 0;
    $scope.level_classes = null;
    $scope.courses = null;
    $scope.global_session_duration_min = null;
    $scope.success_save = null;
    $scope.error_save = null;


    $scope.GetCoursesConfigTemplates = function() {
        $http({
            method: 'GET',
            //url: '/api/CoursesConfigTemplates/listBySchoolId'
            url: '/api/CoursesConfigTemplate/get/current_user_school=true'
        }).then(function successCallback(response) {
            if (response.data.data.courses_config_templates.length) {
                $scope.courses_config_templates = response.data.data.courses_config_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.changed_courses_config_template_id = function() {
        $scope.school_level_id = 0;
        $scope.school_levels = null;
        $scope.level_class_id = 0;
        $scope.level_classes = null;
        $scope.courses = null;
        $scope.success_save = null;
        $scope.error_save = null;
        $scope.global_session_duration_min = null;

        if($scope.courses_config_template_id){
            angular.forEach($scope.courses_config_templates, function(item, key) {
                if(item.id===$scope.courses_config_template_id){
                    $scope.global_session_duration_min = item.session_duration_min;
                }
            });
            $http({
                method: 'GET',
                //url: '/api/SchoolLevel/listByCoursesConfigTemplateId/'+$scope.courses_config_template_id
                url: '/api/SchoolLevel/get/courses_config_template_id='+$scope.courses_config_template_id
            }).then(function successCallback(response) {
                console.log(response);
                if (response.data.data.school_levels.length) {
                    $scope.school_levels = response.data.data.school_levels;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };



    $scope.changed_school_level_id = function() {

        $scope.level_class_id = 0;
        $scope.level_classes = null;
        $scope.courses = null;
        $scope.success_save = null;
        $scope.error_save = null;

        if($scope.school_level_id){
            $http({
                method: 'GET',
                //url: '/api/LevelClass/getAllLevelClassBySchoolLevelID/'+$scope.school_level_id
                url: '/api/LevelClass/get/school_level_id='+$scope.school_level_id
            }).then(function successCallback(response) {
                console.log(response);
                if (response.data.data.level_classes.length) {
                    $scope.level_classes = response.data.data.level_classes;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };

    $scope.changed_level_class_id = function() {
        $scope.courses = null;
        $scope.success_save = null;
        $scope.error_save = null;
    };


    $scope.count_total_session_nb = function() {
        var total = 0;
        angular.forEach($scope.courses, function(course, key) {
            if(course.session_nb){
                total += course.session_nb;
            }
        });
        return total;
    };

    $scope.count_total_hours_nb = function() {
        var total = 0;
        angular.forEach($scope.courses, function(course, key) {
            if(course.hours_nb){
                total += course.hours_nb;
            }
        });
        return total;
    };

    $scope.count_total_coef = function() {
        var total = 0;
        angular.forEach($scope.courses, function(course, key) {
            if(course.coef){
                total += course.coef;
            }
        });
        return total;
    };


    $scope.pre_get_item_course = function(item) {
        item.balance = 'test balance';
        item.hours_nb = $scope.global_session_duration_min*item.session_nb/60;
        return item;
        //
    };


    $scope.get_courses = function() {
        $scope.courses = null;
        $scope.success_save = null;
        $scope.error_save = null;
        $http({
            method: 'GET',
            url: '/api/CoursesConfigItems/listByCoursesConfigTemplateAndLevelClassId/cct_id/'+$scope.courses_config_template_id+'/lc_id/'+$scope.level_class_id
        }).then(function successCallback(response) {
            console.log(response);
            if (response.data.data.length) {
                $scope.courses = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.save = function() {
        $scope.success_save = null;
        $scope.error_save = null;
        $http({
            method: 'POST',
            data: {courses_config_items:$scope.courses},
            url: '/api/CoursesConfigItem/multisave'
        }).then(function successCallback(response) {
            $scope.success_save = true;
            console.log(response);
        }, function errorCallback(response) {
            $scope.error_save = true;
            console.log('error');
        });
    };


    $scope.GetCoursesConfigTemplates();
    
});


