'use strict';

app.controller('CoursesConfigItemSelectTemplateController', function($scope, $http, $stateParams, $location) {

    $scope.schools = null;
    $scope.selected_school = null;
    $scope.levels_template = null;
    $scope.courses_templates = null;
    $scope.courses_config_template = null;
    


    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
 

    
    $scope.GetLevelsTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/LevelsTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.levels_templates.length) {
                $scope.levels_templates = response.data.data.levels_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetCoursesTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/CoursesTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.courses_templates.length) {
                $scope.courses_templates = response.data.data.courses_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.changed_selected_school = function() {
        $scope.courses_config_template = null;
        if ($scope.selected_school) {
            $http({
                method: 'GET',
                url: '/api/CoursesConfigTemplate/get/school_id='+$scope.selected_school
            }).then(function successCallback(response) {
                if (response.data.data.courses_config_templates.length) {
                    $scope.courses_config_template = response.data.data.courses_config_templates[0];
                }else{
                    $scope.courses_config_template = {};
                    $scope.courses_config_template.new = true;
                    $scope.courses_config_template.school_id = $scope.selected_school;
                    $scope.courses_config_template.active = 'Y';
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };

    $scope.save = function() {
        $http({
            method: 'POST',
            data: {courses_config_template:$scope.courses_config_template},
            url: '/api/CoursesConfigTemplate/save'
        }).then(function successCallback(response) {
            if($scope.courses_config_template.new){
                $scope.changed_selected_school();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetSchools();
    $scope.GetLevelsTemplates();
    $scope.GetCoursesTemplates();
});


