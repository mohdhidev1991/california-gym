'use strict';

app.controller('CoursesTemplateAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.courses_templates = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetCoursesTemplates = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/CoursesTemplate/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.courses_templates) {
                $scope.courses_templates = response.data.data.courses_templates;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetCoursesTemplates();
    };



    $scope.delete_courses_template = function(courses_template_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCoursesTemplateInstanceCtrl',
            resolve: {
                courses_template_id: function() {
                    return courses_template_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCoursesTemplates();
        }, function() {
            
        });
    };


    $scope.GetCoursesTemplates();


});



app.controller('ModalDeleteCoursesTemplateInstanceCtrl', function($scope, $uibModalInstance, $http, courses_template_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {courses_template_id : courses_template_id},
            url: '/api/CoursesTemplate/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


