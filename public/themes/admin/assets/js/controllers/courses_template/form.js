'use strict';

app.controller('CoursesTemplateFormController', function($scope, $http, $stateParams, $location) {

    $scope.courses_template = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetCoursesTemplate = function() {
        $http({
            method: 'GET',
            url: '/api/CoursesTemplate/get/courses_template_id='+$stateParams.courses_template_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.courses_template = response.data.data;
                if($scope.courses_template.active && $scope.courses_template.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCourses = function() {
        $http({
            method: 'GET',
            url: '/api/Course/get'
        }).then(function successCallback(response) {
            if (response.data.data.courses.length) {
                $scope.courses = response.data.data.courses;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetCoursesTemplate();
    }else{
        $scope.courses_template = {};
        $scope.courses_template.new = true;
        $scope.courses_template.courses = [];
        $scope.courses_template.course_mfk = '';
    }
    $scope.GetCourses();



    $scope.CheckboxObjectHelper = CheckboxObject.helper;
    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.courses_template.active = "Y";
        }else{
            $scope.courses_template.active = "N";
        }

        $scope.courses_template.course_mfk = null;
        if($scope.courses_template.courses){
            $scope.courses_template.course_mfk = ','+$scope.courses_template.courses.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {courses_template: $scope.courses_template},
                url: '/api/CoursesTemplate/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/courses_templates");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.courses_template);
            $http({
                method: 'POST',
                data: {courses_template: $scope.courses_template},
                url: '/api/CoursesTemplate/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/courses_template/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



