'use strict';

app.controller('CourseSessionEditCurrentController', function($scope, $http, $stateParams, $location) {

    $scope.students = null;
    $scope.coursesession = null;
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.can_submit_save_edit = false;


    $scope.LoadListStudents = function() {
        $http({
            method: 'POST',
            data: {hday_num:$scope.coursesession.hday_num,hmonth:$scope.coursesession.hmonth,level_class_id:$scope.coursesession.level_class_id,session_order:$scope.coursesession.session_order,symbol:$scope.coursesession.symbol,year:$scope.coursesession.year},
            url: '/api/Student/listStudentsByCourseSession'
        }).then(function successCallback(response) {
            if (response.data.data.length) {
                $scope.students = response.data.data;
                $scope.check_can_submit_save_edit();
            } else {
                $scope.students = false;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.LoadCourseSessionInfos = function() {
        /*$http({
            method: 'GET',
            data: $scope.coursesession_fields,
            url: '/api/courseSession/infos/' + $scope.coursesession_fields.year + '/' + $scope.coursesession_fields.hmonth + '/' + $scope.coursesession_fields.hday_num+ '/' + $scope.coursesession_fields.level_class_id+ '/' + $scope.coursesession_fields.symbol+ '/' + $scope.coursesession_fields.session_order
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.coursesession = response.data.data;
                $scope.LoadListStudents();
                $scope.coursesession.session_start_time = moment().format('HH:mm');
                $scope.uitime_global_coming_time = moment($scope.coursesession.session_start_time, 'HH:mm');
            }
        }, function errorCallback(response) {
            console.log('error');
        });*/

        var url_ = '/api/courseSession/current';
        if($stateParams.date){
            url_ = '/api/courseSession/current/' + $stateParams.date;
        }
        $http({
            method: 'GET',
            url: url_
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.coursesession = response.data.data;
                $scope.LoadListStudents();
                //$scope.coursesession.session_start_time = moment().format('HH:mm');
                $scope.uitime_global_coming_time = moment($scope.coursesession.session_start_time, 'HH:mm');
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.LoadCourseSessionInfos();

    $scope.InitStudentPresence = function(item) {

        if (!item.coming_status_id) {
            item.coming_status_id = 1;
        }
        if (!item.exit_status_id) {
            item.exit_status_id = 1;
        }
        if (!item.coming_time && $scope.coursesession) {
            item.coming_time = $scope.coursesession.session_start_time;
            item.session_start_time = $scope.coursesession.session_start_time;
        }
        if (!item.exit_time && $scope.coursesession) {
            item.exit_time = $scope.coursesession.session_end_time;
            item.session_end_time = $scope.coursesession.session_end_time;
        }

        if (!item.timepicker) {
            item.timepicker = moment(item.coming_time, 'HH:mm');
            item.custom_coming_time = false;
        }

        if (item.coming_status_id == 4) {
            item.custom_coming_time = true;
        }
        return item;
    };


    $scope.update_global_coming_time = function() {
        angular.forEach($scope.students, function(student, key) {
            if (student.custom_coming_time===false) {
                student.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
                student.timepicker = $scope.uitime_global_coming_time;
            }
        });
        $scope.check_can_submit_save_edit();
    };


    $scope.update_student_coming_time = function(student) {
        student.item.coming_time = moment(student.item.timepicker).format('HH:mm');
    };


    $scope.set_coming_status_id = function(student, status_id) {
        student.item.coming_status_id = status_id;
        if(student.item.coming_status_id!=4){
            student.item.coming_time = moment($scope.uitime_global_coming_time).format('HH:mm');
            student.item.custom_coming_time = false;
            student.item.timepicker = $scope.uitime_global_coming_time;
        }else{
            student.item.custom_coming_time = true;
        }

        $scope.check_can_submit_save_edit();
    };

    $scope.check_can_submit_save_edit = function() {
        var checked = 0;
        angular.forEach($scope.students, function(student, key) {
            if(student.coming_status_id!=3){
                checked++;
            }
        });
        if($scope.students && checked===$scope.students.length){
            $scope.can_submit_save_edit = true;
        }
    };

    $scope.submit_save_edit = function() {
        $scope.students_data_to_save = [];
        angular.forEach($scope.students, function(student, key) {
            var item_student_data = {
                student_id: student.student_id,
                attributes: {
                    coming_status_id: student.coming_status_id,
                    coming_time: student.coming_time,
                }
            };
            $scope.students_data_to_save.push(item_student_data);
        });
        $http({
            method: 'POST',
            data: {'course_session' : $scope.coursesession,'students_session': $scope.students_data_to_save},
            url: '/api/CourseSession/StudentsSession/save'
        }).then(function successCallback(response) {
            console.log(response);
            $location.path( "/coursesession/current/"+$stateParams.date );
        }, function errorCallback(response) {
            console.log('error');
        });
    };

});


