'use strict';


angular.module('app', ['ui.bootstrap']);



app.controller('CourseSessionTimelineController', function($scope, $http, $stateParams, $uibModal, CourseSessionFactory) {


    $scope.TimelineCourseSession = null;
    $scope.empty_timeline = false;

    $scope.url_data_timeline = '/api/courseSession/timeline';
    if ($stateParams.date) {
        $scope.url_data_timeline = '/api/courseSession/timeline/' + $stateParams.date;
    }



    $scope.animationsEnabled = true;


    $http({
        method: 'GET',
        url: $scope.url_data_timeline
    }).then(function successCallback(response) {

        $scope.TimelineCourseSession = response.data.data;

        if (!response.data.data) {
            $scope.empty_timeline = response.data.message;
        }

    }, function errorCallback(response) {

    });



    $scope.coursesFilterTimes = function(item) {
        var startTime = moment(item.session_start_time, 'HH:mm');
        var endTime = moment(item.session_end_time, 'HH:mm');
        item.duration = endTime.diff(startTime, 'minutes');

        item.session_start_time_ampm = moment(item.session_start_time, 'HH:mm').format('hh:mm a');
        item.session_end_time_ampm = moment(item.session_end_time, 'HH:mm').format('hh:mm a');

        return item;
    };




    $scope.cancel_course = function(coursesession) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'front/tpl/coursesession/modal_confirm_cancel',
            controller: 'ModalCancelCourseInstanceCtrl',
            resolve: {
                coursesession: function() {
                    return coursesession;
                }
            }
        });
        modalInstance.result.then(function(coursesession) {
            $scope.TimelineCourseSession.splice(coursesession.$index, 1);
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.open_course = function(coursesession) {
        CourseSessionFactory.set(coursesession.item);
    };


});









app.controller('ModalCancelCourseInstanceCtrl', function($scope, $uibModalInstance, $http, coursesession) {

    $scope.no_comment = false;
    $scope.submit_cancelmodal = function() {

        if (!$scope.comment) {
            $scope.no_comment = true;
        } else {
            $scope.no_comment = false;
            coursesession.item.session_status_comment = $scope.comment;

            $http({
                method: 'POST',
                data: coursesession.item,
                url: '/api/CourseSession/cancel'
            }).then(function successCallback(response) {
                $uibModalInstance.close(coursesession);
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };

    $scope.close_cancelmodal = function() {
        $uibModalInstance.dismiss('cancel');
    };
});











