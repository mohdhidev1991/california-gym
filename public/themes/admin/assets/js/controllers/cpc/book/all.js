'use strict';

app.controller('CpcBookAllController', function($scope, $http, $uibModal, $stateParams, $location, $window) {

    $scope.filter = {};
    $scope.books = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];

    $scope.order_by = 'id';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetBooks();
    };


    $scope.GetBooks = function() {
        if($scope.loading) return;

        $scope.empty_results = null;

        $scope.Params = 'active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });


        $http({
            method: 'GET',
            url: '/api/CpcBook/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status) {
                $scope.books = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.books = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.books = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };


    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetBooks();
    };



    



    $scope.delete_book = function(cpc_book_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteBookInstanceCtrl',
            resolve: {
                cpc_book_id: function() {
                    return cpc_book_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetBooks();
        }, function() {

        });
    };


    $scope.GetBooks();


});



app.controller('ModalDeleteBookInstanceCtrl', function($scope, $uibModalInstance, $http, cpc_book_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { cpc_book_id: cpc_book_id },
            url: '/api/CpcBook/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});