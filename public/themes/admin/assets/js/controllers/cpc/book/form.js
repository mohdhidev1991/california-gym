'use strict';

app.controller('CpcBookFormController', function($scope, $http, $stateParams, $location, $state) {


    $scope.cpc_book = null;
    $scope.genres = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetCpcBook = function() {
        $scope.error_get = null;
        $http({
            method: 'GET',
            url: '/api/CpcBook/get/cpc_book_id='+$stateParams.cpc_book_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.cpc_book = response.data.data;
                if($scope.cpc_book.active && $scope.cpc_book.active==="Y"){
                    $scope.active_switch = true;
                }
            }else{
                $scope.error_get = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    
    $scope.GetCpcParentBooks = function() {
        $http({
            method: 'GET',
            url: '/api/CpcBook/get/order_by=book_name,order=ASC,exclude_id='+$stateParams.cpc_book_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.parent_books = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCpcParentBooks();
    

    $scope.GetCpcBookType = function() {
        $http({
            method: 'GET',
            url: '/api/CpcBookType/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.book_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCpcBookCategories = function() {
        $http({
            method: 'GET',
            url: '/api/CpcBookCategory/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.book_categories = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCourses = function() {
        $http({
            method: 'GET',
            url: '/api/Course/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetLevelClasses = function() {
        $http({
            method: 'GET',
            url: '/api/LevelClass/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.level_classes = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if($scope.action==="edit"){
        $scope.GetCpcBook();
    }else{
        $scope.cpc_book = {};
        $scope.cpc_book.new = true;
        $scope.cpc_book.level_classes = [];
    }
    $scope.GetCpcBookType();
    $scope.GetCpcBookCategories();
    $scope.GetCourses();
    $scope.GetLevelClasses();


    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.cpc_book.active = "Y";
        }else{
            $scope.cpc_book.active = "N";
        }
        $scope.cpc_book.level_class_mfk = null;
        if($scope.cpc_book.level_classes){
            $scope.cpc_book.level_class_mfk = ','+$scope.cpc_book.level_classes.join(',')+',';
        }
        return true;
    };


    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {cpc_book: $scope.cpc_book},
                url: '/api/CpcBook/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $state.go("cpc.books");
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {cpc_book: $scope.cpc_book},
                url: '/api/CpcBook/save'
            }).then(function successCallback(response) {
                if(response.data.status){
                    $state.go("cpc.edit_book", {cpc_book_id: response.data.data});
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



