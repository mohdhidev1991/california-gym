'use strict';

app.controller('CpcBookPageFormController', function($scope, $http, $stateParams, $location, $state) {


    $scope.cpc_book_page = null;
    $scope.genres = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.cpc_book_id = $stateParams.cpc_book_id;


    $scope.GetCpcBookPage = function() {
        $scope.error_get = null;
        $http({
            method: 'GET',
            url: '/api/CpcBookPage/get/join=cpc_book,book_id='+$scope.cpc_book_id+',cpc_book_page_id='+$stateParams.cpc_book_page_id
        }).then(function successCallback(response) {
            console.log(response.data);
            if (response.data.status) {
                $scope.cpc_book_page = response.data.data;
                if($scope.cpc_book_page.active && $scope.cpc_book_page.active==="Y"){
                    $scope.active_switch = true;
                }
            }else{
                $scope.error_get = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    



    if($scope.action==="edit"){
        $scope.GetCpcBookPage();
    }else{
        $scope.cpc_book_page = {};
        $scope.cpc_book_page.new = true;
        $scope.cpc_book_page.book_id = $scope.cpc_book_id;
    }

    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.cpc_book_page.active = "Y";
        }else{
            $scope.cpc_book_page.active = "N";
        }
        $scope.cpc_book_page.level_class_mfk = null;
        if($scope.cpc_book_page.level_classes){
            $scope.cpc_book_page.level_class_mfk = ','+$scope.cpc_book_page.level_classes.join(',')+',';
        }
        return true;
    };


    $scope.save = function(back_location){
        if($scope.pre_save()){
            $scope.errors_save = null;
            $scope.success_save = null;
            $http({
                method: 'POST',
                data: {cpc_book_page: $scope.cpc_book_page},
                url: '/api/CpcBookPage/save'
            }).then(function successCallback(response) {
                if(response.data.status){
                    if(back_location){
                        $state.go("cpc.book_pages", {book_id: $scope.cpc_book_id});
                    }else{
                        $scope.success_save = true;
                    }
                }else{
                    $scope.errors_save = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $scope.errors_save = null;
            $scope.success_save = null;
            $http({
                method: 'POST',
                data: {cpc_book_page: $scope.cpc_book_page},
                url: '/api/CpcBookPage/save'
            }).then(function successCallback(response) {
                if(response.data.status){
                    $state.go("cpc.edit_book_page", {cpc_book_id: $scope.cpc_book_id, cpc_book_page_id: response.data.data});
                }else{
                    $scope.errors_save = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



