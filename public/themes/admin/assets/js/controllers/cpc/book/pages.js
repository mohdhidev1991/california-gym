'use strict';

app.controller('CpcBookPagesController', function($scope, $http, $uibModal, $stateParams, $location, $window) {

    $scope.filter = {};
    $scope.book_pages = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];

    $scope.order_by = 'book_page_num';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetBookPages();
    };




    $scope.GetCpcBook = function() {
        $scope.error_get_cpc_book = null;
        $http({
            method: 'GET',
            url: '/api/CpcBook/get/cpc_book_id='+$stateParams.book_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.cpc_book = response.data.data;
            }else{
                $scope.error_get_cpc_book = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCpcBook();

    $scope.GetBookPages = function() {
        if(!$stateParams.book_id) return null;

        if($scope.loading) return;
        $scope.empty_results = null;

        $scope.Params = 'book_id=' + $stateParams.book_id;
        $scope.Params += ',active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });


        $http({
            method: 'GET',
            url: '/api/CpcBookPage/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status) {
                $scope.book_pages = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.book_pages = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.book_pages = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };
    $scope.GetBookPages();

    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetBookPages();
    };



    



    $scope.delete_book_page = function(cpc_book_page_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteBookPageInstanceCtrl',
            resolve: {
                cpc_book_page_id: function() {
                    return cpc_book_page_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetBookPages();
        }, function() {

        });
    };


    $scope.upload_book = function() {
        if($scope.book_pages.length){
            var need_confirmation = true;
        }else{
            var need_confirmation = false;
        }
        
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCpcUploadBook.html',
            controller: 'ModalCpcUploadBookInstanceCtrl',
            keyboard: false,
            backdrop: 'static',
            resolve: {
                cpc_book_id: function() {
                    return $scope.cpc_book.id;
                },
                need_confirmation: function() {
                    return need_confirmation;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetBookPages();
        }, function() {
            $scope.GetBookPages();
        });
    };
});



app.controller('ModalDeleteBookPageInstanceCtrl', function($scope, $uibModalInstance, $http, cpc_book_page_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { cpc_book_page_id: cpc_book_page_id },
            url: '/api/CpcBookPage/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});




app.controller('ModalCpcUploadBookInstanceCtrl', function($scope, $uibModalInstance, $http, FileUploader, cpc_book_id, need_confirmation) {
    $scope.need_confirmation = angular.copy(need_confirmation);

    $scope.finish = function() {
        $uibModalInstance.close();
    };

    $scope.cancel_upload_book = function() {
        $uibModalInstance.dismiss('cancel');
    };


    var uploader = $scope.uploader = new FileUploader({
        url: '/api/CpcBookPage/upload_book',
        formData: [{cpc_book_id: cpc_book_id}],
        autoUpload: false,
    });

    // FILTERS
    uploader.filters.push({
        name: 'queueLimit',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            return this.queue.length < 1;
        }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        $scope.success_upload = null;
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.log(uploader);
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        if (response.status) {
            $scope.success_upload = true;
            fileItem.file_id = response.data;
            //$uibModalInstance.close();
        } else {
            fileItem.isError = true;
            fileItem.isSuccess = false;
            fileItem.isUploaded = false;
            $scope.errors_upload = response.message;
        }
        //console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };
});