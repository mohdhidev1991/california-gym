'use strict';

app.controller('CpcCourseProgramAllController', function($scope, $http, $uibModal, $stateParams, $location, $window) {

    $scope.filter = {};
    $scope.course_programs = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];

    $scope.order_by = 'id';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetCoursePrograms();
    };


    $scope.GetCoursePrograms = function() {
        if($scope.loading) return;

        $scope.empty_results = null;

        $scope.Params = 'active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });


        $http({
            method: 'GET',
            url: '/api/CpcCourseProgram/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status) {
                $scope.course_programs = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.course_programs = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.course_programs = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };


    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetCoursePrograms();
    };



    



    $scope.delete_course_program = function(cpc_course_program_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseProgramInstanceCtrl',
            resolve: {
                cpc_course_program_id: function() {
                    return cpc_course_program_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCoursePrograms();
        }, function() {

        });
    };


    $scope.GetCoursePrograms();


});



app.controller('ModalDeleteCourseProgramInstanceCtrl', function($scope, $uibModalInstance, $http, cpc_course_program_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { cpc_course_program_id: cpc_course_program_id },
            url: '/api/CpcCourseProgram/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});