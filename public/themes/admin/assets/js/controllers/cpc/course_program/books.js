'use strict';

app.controller('CpcCourseProgramBooksController', function($scope, $http, $uibModal, $stateParams, $location, $window) {

    $scope.filter = {};
    $scope.course_program_books = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];
    $scope.course_program_id = $stateParams.cpc_course_program_id;

    $scope.order_by = 'id';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetCourseProgramBooks();
    };


    $scope.GetCourseProgramBooks = function() {
        if($scope.loading) return;

        $scope.empty_results = null;

        $scope.Params = 'course_program_id='+$scope.course_program_id;
        $scope.Params += ',join=cpc_course_program!cpc_book!level_class';
        $scope.Params += ',active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });


        $http({
            method: 'GET',
            url: '/api/CpcCourseProgramBook/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status) {
                $scope.course_program_books = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.course_program_books = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.course_program_books = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };


    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetCourseProgramBooks();
    };



    



    $scope.delete_course_program_book = function(cpc_course_program_book_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseProgramBookInstanceCtrl',
            resolve: {
                cpc_course_program_book_id: function() {
                    return cpc_course_program_book_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCourseProgramBooks();
        }, function() {

        });
    };




    $scope.add_book = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddBookCourseProgram.html',
            controller: 'ModalAddBookCourseProgramInstanceCtrl',
            resolve: {
                course_program_id: function() {
                    return $scope.course_program_id;
                },
                action: function() {
                    return "new";
                },
                course_program_book: function() {
                    return null;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCourseProgramBooks();
        }, function() {

        });
    };

    $scope.edit_book = function(course_program_book) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddBookCourseProgram.html',
            controller: 'ModalAddBookCourseProgramInstanceCtrl',
            resolve: {
                course_program_id: function() {
                    return $scope.course_program_id;
                },
                action: function() {
                    return "edit";
                },
                course_program_book: function() {
                    return course_program_book;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCourseProgramBooks();
        }, function() {

        });
    };


    $scope.GetCourseProgramBooks();


});



app.controller('ModalDeleteCourseProgramBookInstanceCtrl', function($scope, $uibModalInstance, $http, cpc_course_program_book_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { cpc_course_program_book_id: cpc_course_program_book_id },
            url: '/api/CpcCourseProgramBook/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});




app.controller('ModalAddBookCourseProgramInstanceCtrl', function($scope, $uibModalInstance, $http, course_program_id, action, course_program_book) {
    $scope.course_program_plan = null;
    $scope.course_program_book = {};
    $scope.action = action;
    

    $scope.save = function() {
        $scope.errors = null;
        $http({
            method: 'POST',
            data: $scope.course_program_book,
            url: '/api/CpcCourseProgramBook/save_book'
        }).then(function successCallback(response) {
            if(response.data.status){
                $uibModalInstance.close();
            }else{
                $scope.errors = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };

    $scope.GetBooks = function() {
        $http({
            method: 'GET',
            url: '/api/CpcBook/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.books = response.data.data;
				$scope.changed_book();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetBooks();

    $scope.GetLevelClasses = function() {
        $http({
            method: 'GET',
            url: '/api/LevelClass/get/'+'level_class_ids='+$scope.book_level_class_ids+',cpc_course_program_id='+$scope.course_program_book.course_program_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.level_classes = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.changed_book = function() {
		if(!$scope.course_program_book.book_id){
			$scope.course_program_book.level_class_id = null;
			$scope.course_program_book.book_level_class_ids = null;	
		}
		
		console.log($scope.course_program_book);
		console.log($scope.books);
		
        angular.forEach($scope.books, function(book, key) {
          if(book.id===$scope.course_program_book.book_id){
            if(book.level_class_mfk){
                $scope.book_level_class_ids = book.level_class_mfk.substring(1).slice(0,-1).replace(',','!');
            }
          }
        });
        $scope.GetLevelClasses();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };


    if($scope.action==='new'){
        $scope.course_program_book.new = true;
        $scope.course_program_book.course_program_id = course_program_id;
    }
    if($scope.action==='edit'){
        $scope.course_program_book = course_program_book;
		$scope.course_program_book.course_program_id = course_program_id;
    }
});

