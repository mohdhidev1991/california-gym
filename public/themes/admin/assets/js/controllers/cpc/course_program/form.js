'use strict';

app.controller('CpcCourseProgramFormController', function($scope, $http, $stateParams, $location, $state) {


    $scope.cpc_course_program = null;
    $scope.genres = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetCpcCourseProgram = function() {
        $scope.error_get = null;
        $http({
            method: 'GET',
            url: '/api/CpcCourseProgram/get/cpc_course_program_id='+$stateParams.cpc_course_program_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.cpc_course_program = response.data.data;
                if($scope.cpc_course_program.active && $scope.cpc_course_program.active==="Y"){
                    $scope.active_switch = true;
                }
            }else{
                $scope.error_get = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    $scope.GetLevelsTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/LevelsTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.levels_templates = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if($scope.action==="edit"){
        $scope.GetCpcCourseProgram();
    }else{
        $scope.cpc_course_program = {};
        $scope.cpc_course_program.new = true;
    }
    $scope.GetLevelsTemplates();


    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.cpc_course_program.active = "Y";
        }else{
            $scope.cpc_course_program.active = "N";
        }
        return true;
    };


    $scope.save = function(back_location){
        if($scope.pre_save()){
            $scope.errors_save = null;
            $scope.success_save = null;
            $http({
                method: 'POST',
                data: {cpc_course_program: $scope.cpc_course_program},
                url: '/api/CpcCourseProgram/save'
            }).then(function successCallback(response) {
                if(response.data.status){
                    if(back_location){
                        $state.go("cpc.course_programs");
                    }else{
                        $scope.success_save = true;
                    }
                }else{
                    $scope.errors_save = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $scope.errors_save = null;
            $scope.success_save = null;
            $http({
                method: 'POST',
                data: {cpc_course_program: $scope.cpc_course_program},
                url: '/api/CpcCourseProgram/save'
            }).then(function successCallback(response) {
                if(response.data.status){
                    $state.go("cpc.edit_course_program", {cpc_course_program_id: response.data.data});
                }else{
                    $scope.errors_save = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



