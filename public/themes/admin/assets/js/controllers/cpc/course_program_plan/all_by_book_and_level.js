'use strict';

app.controller('CpcCourseProgramPlanAllByBookAndLevelController', function($scope, $http, $uibModal, $stateParams, $location, $window) {

    $scope.filter = {};
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.course_program_id = $stateParams.cpc_course_program_id;
    $scope.book_id = $stateParams.book_id;
    $scope.level_class_id = $stateParams.level_class_id;
    $scope.course_program_book = null;
	
	
	$scope.GetCourseProgramBook = function() {
		$scope.empty_course_program_book = null;
		$http({
            method: 'GET',
            url: '/api/CpcCourseProgramBook/get/join=count_course_plan!level_class!cpc_course_program!cpc_book,single,course_program_id='+$scope.course_program_id+',book_id='+$scope.book_id+',level_class_id='+$scope.level_class_id
        }).then(function successCallback(response) {
            if (response.data.status && response.data.data.id) {
                $scope.course_program_book = response.data.data;
            }else{
				$scope.empty_course_program_book = true;
			}
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCourseProgramBook();


	$scope.order_by = 'course_num';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetCoursePlans();
    };
    $scope.GetCoursePlans = function() {
		
        if($scope.loading) return;

        $scope.empty_results = null;

        $scope.Params = 'course_program_id='+$scope.course_program_id;
        $scope.Params += ',book_id='+$scope.book_id;
        $scope.Params += ',level_class_id='+$scope.level_class_id;
        $scope.Params += ',limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });


        $http({
            method: 'GET',
            url: '/api/CpcCoursePlan/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status && response.data.data.length) {
                $scope.course_plans = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.course_plans = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.course_plans = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };


    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetCoursePlans();
    };



    



    $scope.delete_course_program_book = function(cpc_course_program_book_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseProgramBookInstanceCtrl',
            resolve: {
                cpc_course_program_book_id: function() {
                    return cpc_course_program_book_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCoursePlans();
        }, function() {

        });
    };




    $scope.add_book = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddBookCourseProgram.html',
            controller: 'ModalAddBookCourseProgramInstanceCtrl',
            resolve: {
                course_program_id: function() {
                    return $scope.course_program_id;
                },
                action: function() {
                    return "new";
                },
                course_program_book: function() {
                    return null;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCoursePlans();
        }, function() {

        });
    };

    $scope.edit_book = function(course_program_book) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddBookCourseProgram.html',
            controller: 'ModalAddBookCourseProgramInstanceCtrl',
            resolve: {
                course_program_id: function() {
                    return $scope.course_program_id;
                },
                action: function() {
                    return "edit";
                },
                course_program_book: function() {
                    return course_program_book;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetCoursePlans();
        }, function() {

        });
    };
    $scope.GetCoursePlans();
});



app.controller('ModalDeleteCourseProgramBookInstanceCtrl', function($scope, $uibModalInstance, $http, cpc_course_program_book_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { cpc_course_program_book_id: cpc_course_program_book_id },
            url: '/api/CpcCourseProgramBook/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

