'use strict';

app.controller('CrmErequestsFollowUpController', function($scope, $http, $stateParams, $cookies, $state, Notification, $filter, $uibModal) {
    $scope.erequest = null;
    $scope.rea_user = $cookies.getObject('rea_user');
    $scope.new_follow_up = {};
    $scope.form = {};

    $scope.GetCrmErequestLevels = function() {
        $scope.crm_erequest_levels = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestLevel/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_levels = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestLevels();


    $scope.GetErequest = function() {
        $scope.empty_results = null;
        $scope.Params = 'full_infos,join=count_crm_erequest_fup!count_crm_erequest_fup_not_read!crm_erequest_status!crm_erequest_type!crm_erequest_cat,crm_erequest_id=' + $stateParams.crm_erequest_id;

        $http({
            method: 'GET',
            url: '/api/CrmErequest/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.erequest = response.data.data;
                $scope.new_follow_up.status_id = $scope.erequest.crm_erequest_status_id;
                $scope.new_follow_up.erequest_id = $scope.erequest.id;
                $scope.GetErequestFollowUps();
            } else {
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetErequest();



    $scope.GetErequestFollowUps = function() {
        $scope.empty_follow_ups = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestFup/get/order_by=fup_date,order=DESC,order_by2=fup_time,order2=DESC,crm_erequest_id=' + $stateParams.crm_erequest_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.follow_ups = response.data.data;
            } else {
                $scope.empty_follow_ups = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.toggle_opened_follow_up = function(follow_up) {
        if (follow_up.is_read === 'Y' || $scope.rea_user.id == follow_up.owner_id) return;
        follow_up.is_read = 'Y';
        $http({
            method: 'POST',
            data: { crm_erequest_fup_id: follow_up.id },
            url: '/api/CrmErequestFup/is_read'
        }).then(function successCallback(response) {

        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.unread_follow_up = function(follow_up) {
        console.log(follow_up);
        follow_up.is_read = 'N';
    };



    $scope.GetCrmErequestStatus = function() {
        $scope.new_follow_up_status = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestStatus/get/user'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.new_follow_up_status = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestStatus();




    $scope.count_fup_not_read = function(follow_up) {
        var counter = 0;
        angular.forEach($scope.follow_ups, function(item_fup, key) {
            if (item_fup.is_read === 'N') {
                counter++;
            }
        });
        return counter;
    };

    $scope.reset_form_new_follow_up = function() {
        $scope.new_follow_up = {};
        $scope.new_follow_up.status_id = $scope.erequest.crm_erequest_status_id;
        $scope.new_follow_up.erequest_id = $scope.erequest.id;
        $scope.form.new_follow_up.$setUntouched();
    };



    $scope.submit_follow_up = function() {
        if($scope.new_follow_up.status_id!==$scope.erequest.crm_erequest_status_id){
            $scope.confirm_submit_follow_up();
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmSubmitFollowUp.html',
            controller: 'ModalConfirmSubmitFollowUpInstanceCtrl',
            resolve: {
            }
        });
        modalInstance.result.then(function() {
            $scope.confirm_submit_follow_up();
        }, function() {

        });
    };
    $scope.confirm_submit_follow_up = function() {
        $scope.errors_add_new_follow_up = null;
        $http({
            method: 'POST',
            data: { follow_up: $scope.new_follow_up },
            url: '/api/CrmErequest/submit_follow_up'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('CRM.FollowUpAddedWithSuccess'), delay: 5000, positionX: 'left' });
                $scope.GetErequest();
                $scope.add_new_follow_up = false;
                $scope.reset_form_new_follow_up();
                $('html,body').animate({scrollTop: $('#rows-follow_ups').offset().top-100},'slow');
            } else {
                $scope.errors_add_new_follow_up = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
});




app.controller('ModalConfirmSubmitFollowUpInstanceCtrl', function($scope, $uibModalInstance, $http) {
    $scope.confirm_submit_follow_up = function() {
        $uibModalInstance.close();
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



