'use strict';

app.controller('CrmMyErequestsController', function($scope, $http, $stateParams, $cookies, $state, $uibModal) {
    $scope.erequests = null;

    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];

    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetSchools();
    };


    $scope.GetErequests = function() {
        $scope.empty_results = null;
        $scope.Params = 'limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;
        $scope.Params += ',join=count_crm_erequest_fup!count_crm_erequest_fup_not_read!crm_erequest_status!crm_erequest_type!crm_erequest_cat,order_by=crm_erequest_hdate,order=DESC,order_by2=crm_erequest_time,order2=DESC,include_archived';

        $http({
            method: 'GET',
            url: '/api/CrmErequest/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.erequests = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            } else {
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetErequests();



    $scope.cancel_erequest = function(crm_erequest_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalCancelCrmErequest.html',
            controller: 'ModalCancelCrmErequestInstanceCtrl',
            resolve: {
                crm_erequest_id: function() {
                    return crm_erequest_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetErequests();
        }, function() {

        });
    };



});





app.controller('ModalCancelCrmErequestInstanceCtrl', function($scope, $uibModalInstance, $http, crm_erequest_id) {
    $scope.confirm_cancel = function() {
        $http({
            method: 'POST',
            data: { crm_erequest_id: crm_erequest_id },
            url: '/api/CrmErequest/cancel'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});