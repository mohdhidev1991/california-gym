'use strict';

app.controller('CrmReceivedErequestsController', function($scope, $http, $stateParams, $cookies, $state, $uibModal) {
    $scope.erequests = null;

    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];

    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetSchools();
    };

    
    $scope.GetErequests = function() {
        $scope.empty_results = null;
        $scope.Params = 'limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;
        $scope.Params += ',received';
        $scope.Params += ',join=count_crm_erequest_fup!count_crm_erequest_fup_not_read!crm_erequest_status!crm_erequest_type!crm_erequest_cat,order_by=crm_erequest_hdate,order=DESC,order_by2=crm_erequest_time,order2=DESC,include_archived';

        $http({
            method: 'GET',
            url: '/api/CrmErequest/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.erequests = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            } else {
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetErequests();
});

