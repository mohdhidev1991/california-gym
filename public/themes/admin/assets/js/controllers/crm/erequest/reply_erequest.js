'use strict';

app.controller('CrmReplyErequestController', function($scope, $http, $stateParams, $cookies, $state, Notification, $filter, $uibModal) {
    $scope.erequest = null;
    $scope.rea_user = $cookies.getObject('rea_user');
    $scope.new_follow_up = {};
    $scope.form = {};

    $scope.GetCrmErequestLevels = function() {
        $scope.crm_erequest_levels = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestLevel/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_levels = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestLevels();


    $scope.GetErequest = function() {
        $scope.empty_results = null;
        $scope.Params = 'full_infos,received,join=count_crm_erequest_fup!count_crm_erequest_fup_not_read!crm_erequest_status!crm_erequest_type!crm_erequest_cat,crm_erequest_id=' + $stateParams.crm_erequest_id;

        $http({
            method: 'GET',
            url: '/api/CrmErequest/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.erequest = response.data.data;
                $scope.new_follow_up.status_id = $scope.erequest.crm_erequest_status_id;
                $scope.new_follow_up.erequest_id = $scope.erequest.id;
                $scope.GetErequestFollowUps();
            } else {
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetErequest();



    $scope.GetErequestFollowUps = function() {
        $scope.empty_follow_ups = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestFup/get/order_by=fup_date,order=DESC,order_by2=fup_time,order2=DESC,crm_erequest_id=' + $stateParams.crm_erequest_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.follow_ups = response.data.data;
            } else {
                $scope.empty_follow_ups = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.toggle_opened_follow_up = function(follow_up) {
        if (follow_up.is_read === 'Y' || $scope.rea_user.id == follow_up.owner_id) return;
        follow_up.is_read = 'Y';
        $http({
            method: 'POST',
            data: { crm_erequest_fup_id: follow_up.id },
            url: '/api/CrmErequestFup/is_read'
        }).then(function successCallback(response) {

        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.unread_follow_up = function(follow_up) {
        console.log(follow_up);
        follow_up.is_read = 'N';
    };



    $scope.GetCrmErequestStatus = function() {
        $scope.new_follow_up_status = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestStatus/get/owner'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.new_follow_up_status = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestStatus();




    $scope.count_fup_not_read = function(follow_up) {
        var counter = 0;
        angular.forEach($scope.follow_ups, function(item_fup, key) {
            if (item_fup.is_read === 'N') {
                counter++;
            }
        });
        return counter;
    };

    $scope.reset_form_new_follow_up = function() {
        $scope.new_follow_up = {};
        $scope.new_follow_up.status_id = $scope.erequest.crm_erequest_status_id;
        $scope.new_follow_up.erequest_id = $scope.erequest.id;
        $scope.form.new_follow_up.$setUntouched();
    };



    $scope.submit_reply_follow_up = function() {
        if ($scope.new_follow_up.status_id !== $scope.erequest.crm_erequest_status_id) {
            $scope.confirm_submit_reply_follow_up();
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmSubmitFollowUp.html',
            controller: 'ModalConfirmSubmitFollowUpInstanceCtrl',
            resolve: {}
        });
        modalInstance.result.then(function() {
            $scope.confirm_submit_reply_follow_up();
        }, function() {

        });
    };



    $scope.confirm_submit_reply_follow_up = function() {
        $scope.errors_add_new_follow_up = null;
        $http({
            method: 'POST',
            data: { follow_up: $scope.new_follow_up },
            url: '/api/CrmErequest/submit_reply_follow_up'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('CRM.FollowUpAddedWithSuccess'), delay: 5000, positionX: 'left' });
                $scope.GetErequest();
                $scope.add_new_follow_up = false;
                $scope.reset_form_new_follow_up();
                $('html,body').animate({ scrollTop: $('#rows-follow_ups').offset().top - 100 }, 'slow');
            } else {
                $scope.errors_add_new_follow_up = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.confirm_approve_leave = function(approve) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalApproveLeave.html',
            controller: 'ModalApproveLeaveInstanceCtrl',
            resolve: {
                erequest_id : function(){
                    return $scope.erequest.id;
                },
                approve : function(){
                    return approve;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetErequest();
        });
    };


    $scope.change_erequest_config = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalRedirectErequest.html',
            controller: 'ModalRedirectErequestInstanceCtrl',
            resolve: {
                erequest : function(){
                    return $scope.erequest;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetErequest();
        });
    };


});




app.controller('ModalConfirmSubmitFollowUpInstanceCtrl', function($scope, $uibModalInstance, $http) {
    $scope.confirm_submit_follow_up = function() {
        $uibModalInstance.close();
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});




app.controller('ModalApproveLeaveInstanceCtrl', function($scope, $uibModalInstance, $http, $filter, erequest_id, approve, Notification) {
    $scope.is_approve = approve===true;
    $scope.is_disapprove = approve===false;

    $scope.confirm_approve = function() {
        $http({
            method: 'POST',
            data: { crm_erequest_id: erequest_id},
            url: '/api/CrmErequest/approve_leave'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('CRM.LeaveRequestApproved'), delay: 5000, positionX: 'left' });
                $uibModalInstance.close();
            } else {
                $scope.errors = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };

    $scope.confirm_disapprove = function() {
        $http({
            method: 'POST',
            data: { crm_erequest_id: erequest_id},
            url: '/api/CrmErequest/disapprove_leave'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('CRM.LeaveRequestDisapproved'), delay: 5000, positionX: 'left' });
                $uibModalInstance.close();
            } else {
                $scope.errors = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});










app.controller('ModalRedirectErequestInstanceCtrl', function($scope, $uibModalInstance, $http, $cookies, erequest, Notification, $filter, $state) {
    $scope.erequest = angular.copy(erequest);
    $scope.rea_user = $cookies.getObject('rea_user');


    $scope.GetCrmErequestTypes = function() {
        $scope.crm_erequest_types = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestType/get/domain_ids=1:2'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestTypes();


    $scope.GetCrmErequestCats = function() {
        $scope.crm_erequest_cats = null;
        if (!$scope.erequest.crm_erequest_type_id) return;
        $http({
            method: 'GET',
            url: '/api/CrmErequestCat/get/crm_erequest_type_id=' + $scope.erequest.crm_erequest_type_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_cats = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestCats();


    $scope.GetCrmErequestLevels = function() {
        $scope.crm_erequest_levels = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestLevel/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_levels = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestLevels();


    $scope.confirm_redirect_erequest = function() {
        $http({
            method: 'POST',
            data: { crm_erequest: $scope.erequest},
            url: '/api/CrmErequest/redirect_erequest'
        }).then(function successCallback(response) {
            console.log(response.data);
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('CRM.RequestRedirectedWithSuccess'), delay: 5000, positionX: 'left' });
                if(response.data.attributes && response.data.attributes.owner_changed){
                    $state.go('crm.erequest.received_erequests');
                    $uibModalInstance.close();
                }else{
                    $uibModalInstance.close();
                }
            } else {
                $scope.errors = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

