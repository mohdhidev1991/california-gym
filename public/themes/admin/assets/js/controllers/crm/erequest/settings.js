'use strict';

app.controller('CrmErequestSettingsController', function($scope, $http, $stateParams, $cookies, $state, $filter, Notification) {

    $scope.rea_user = $cookies.getObject('rea_user');
    $scope.erequest = null;

    $scope.GetErequest = function() {
        $scope.errors = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequest/get/join=crm_erequest_type!crm_erequest_cat,crm_erequest_id=' + $stateParams.crm_erequest_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.erequest = response.data.data;
                if (!$scope.erequest.atable_id) {
                    $scope.erequest = null;
                    $scope.errors = ['CRM.ThisRequestDontHavaSettingPage'];
                } else {
                    if ($scope.erequest.atable_id === 3462) {
                        $scope.GetLeaveTypes();
                    }
                    $scope.erequest_settings = {};
                    $scope.erequest_settings.erequest_id = $scope.erequest.id;
                }
            } else {
                $scope.errors = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetErequest();


    $scope.GetLeaveTypes = function() {
        $scope.leave_types = null;
        $scope.selected_leave_type = null;
        if (!$scope.erequest.crm_erequest_type_id || !$scope.erequest.crm_erequest_cat_id) return;
        $http({
            method: 'GET',
            url: '/api/HrmLeaveType/get/crm_erequest_type_id=' + $scope.erequest.crm_erequest_type_id + ',crm_erequest_cat_id=' + $scope.erequest.crm_erequest_cat_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.leave_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.ChangedLeaveType = function() {
        $scope.selected_leave_type = null;
        if (!$scope.erequest_settings.leave_type_id) return;
        for (var i_leave_type in $scope.leave_types) {
            if ($scope.leave_types[i_leave_type].id === $scope.erequest_settings.leave_type_id) {
                $scope.selected_leave_type = $scope.leave_types[i_leave_type];
                break;
            }
        }
    };

    $scope.submit = function() {
        $scope.errors = null;

        if ($scope.erequest_settings.ui_hrm_leave_start_time) {
            var time = moment($scope.erequest_settings.ui_hrm_leave_start_time).format('HH:mm');
            $scope.erequest_settings.hrm_leave_start_time = time.toString();
        }
        if ($scope.erequest_settings.ui_hrm_leave_end_time) {
            var time = moment($scope.erequest_settings.ui_hrm_leave_end_time).format('HH:mm');
            $scope.erequest_settings.hrm_leave_end_time = time.toString();
        }
        $http({
            method: 'POST',
            data: { erequest_settings: $scope.erequest_settings },
            url: '/api/CrmErequest/submit_settings'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('CRM.RequestSubmittedWithSuccess'), delay: 5000, positionX: 'left' });
                $state.go('crm.erequest.my_erequests');
            } else {
                $scope.errors = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

});


