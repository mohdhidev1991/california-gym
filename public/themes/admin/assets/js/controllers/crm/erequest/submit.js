'use strict';

app.controller('CrmErequestSubmitController', function($scope, $http, $stateParams, $location, $cookies, $uibModal, $state, Notification, $filter) {

    $scope.erequest = {};
    $scope.rea_user = $cookies.getObject('rea_user');
    $scope.attachements = [];

      


    $scope.users = [];
    $scope.users = [{
        id: $scope.rea_user.id,
        fullname: $scope.rea_user.firstname + ' ' + $scope.rea_user.f_firstname + ' ' + $scope.rea_user.lastname,
    }];
    $scope.erequest.owner_auser_id = $scope.rea_user.id;

    $scope.GetCrmErequestTypes = function() {
        $scope.crm_erequest_types = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestType/get/school_jobs,domain_ids=1:2'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestTypes();


    $scope.GetCrmErequestLevels = function() {
        $scope.crm_erequest_levels = null;
        $http({
            method: 'GET',
            url: '/api/CrmErequestLevel/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_levels = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCrmErequestLevels();



    $scope.GetCrmErequestCats = function() {
        $scope.crm_erequest_cats = null;
        if (!$scope.erequest.crm_erequest_type_id) return;
        $http({
            method: 'GET',
            url: '/api/CrmErequestCat/get/crm_erequest_type_id=' + $scope.erequest.crm_erequest_type_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.crm_erequest_cats = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.submit = function() {
        $scope.erequest.files = [];
        if ($scope.attachements.length) {
            angular.forEach($scope.attachements, function(file_item, key) {
                $scope.erequest.files.push(file_item.file_id);
            });
        }
        var redirect = true;
        $http({
            method: 'POST',
            data: { crm_erequest: $scope.erequest },
            url: '/api/CrmErequest/submit'
        }).then(function successCallback(response) {
            if (response.data.status) {
                angular.forEach($scope.crm_erequest_types, function(type, key) {
                    if (type.id === $scope.erequest.crm_erequest_type_id && type.atable_id) {
                        $state.go('crm.erequest.settings', { crm_erequest_id: response.data.data });
                        redirect = false;
                    }
                });
                if(redirect){
                    $state.go('crm.erequest.my_erequests');
                    Notification.success({message: $filter('translate')('CRM.RequestSubmittedWithSuccess'), delay: 5000, positionX: 'left'});    
                }
            } else {

            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };




    $scope.upload_attachements = function(feed_import_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalUploadFileSubmitErequest.html',
            controller: 'ModalUploadFileSubmitErequestInstanceCtrl',
            resolve: {}
        });
        modalInstance.result.then(function(files) {
            angular.forEach(files, function(file_item, key) {
                $scope.attachements.push(file_item);
            });
        }, function() {

        });
    };

    $scope.remove_attachement = function(item) {
        var index = $scope.attachements.indexOf(item);
        $scope.attachements.splice(index, 1);
    };

});





app.controller('ModalUploadFileSubmitErequestInstanceCtrl', function($scope, $uibModalInstance, $http, FileUploader) {
    $scope.finish_upload = function() {
        var files = $scope.uploader.queue;
        $uibModalInstance.close(files);
    };
    $scope.cancel_import = function() {
        $uibModalInstance.dismiss('cancel');
    };

    var uploader = $scope.uploader = new FileUploader({
        url: '/api/File/upload',
        formData: { module: 'crm', active: 'N' },
    });

    // FILTERS
    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            return this.queue.length < 5;
        }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        $scope.success_upload = null;
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        //$scope.errors_upload = null;
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        if (response.status) {
            $scope.success_upload = true;
            fileItem.file_id = response.data;
        } else {
            fileItem.isError = true;
            fileItem.isSuccess = false;
            fileItem.isUploaded = false;
            $scope.errors_upload = response.message;
        }
        //console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);
});