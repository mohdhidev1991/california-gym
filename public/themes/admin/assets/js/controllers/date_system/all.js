'use strict';

app.controller('DateSystemAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.dates_system = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetDateSystems = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/DateSystem/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.dates_system) {
                $scope.dates_system = response.data.data.dates_system;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetDateSystems();
    };



    $scope.delete_date_system = function(date_system_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteDateSystemInstanceCtrl',
            resolve: {
                date_system_id: function() {
                    return date_system_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetDateSystems();
        }, function() {
            
        });
    };


    $scope.GetDateSystems();


});



app.controller('ModalDeleteDateSystemInstanceCtrl', function($scope, $uibModalInstance, $http, date_system_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {date_system_id : date_system_id},
            url: '/api/DateSystem/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

