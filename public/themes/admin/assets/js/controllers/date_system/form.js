'use strict';

app.controller('DateSystemFormController', function($scope, $http, $stateParams, $location) {

    $scope.date_system = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.schools  = null;


    $scope.GetDateSystem = function() {
        $http({
            method: 'GET',
            url: '/api/DateSystem/get/date_system_id='+$stateParams.date_system_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.date_system = response.data.data;

                if($scope.date_system.maintenance_start_time){
                    $scope.timer_maintenance_start_time = moment($scope.date_system.maintenance_start_time, 'HH:mm');
                }
                
                if($scope.date_system.maintenance_end_time){
                    $scope.timer_maintenance_end_time = moment($scope.date_system.maintenance_end_time, 'HH:mm');    
                }

                if($scope.date_system.active && $scope.date_system.active==="Y"){
                    $scope.active_switch = true;
                }

                if(!$scope.date_system.we_days){
                    $scope.date_system.we_days = [];
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };





    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetDateSystem();
    }else{
        $scope.date_system = {};
        $scope.date_system.new = true;
    }
    $scope.GetSchools();

    $scope.pre_save = function(){

        if($scope.active_switch===true){
            $scope.date_system.active = "Y";
        }else{
            $scope.date_system.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {date_system: $scope.date_system},
                url: '/api/DateSystem/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/dates_system");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {date_system: $scope.date_system},
                url: '/api/DateSystem/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/date_system/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



