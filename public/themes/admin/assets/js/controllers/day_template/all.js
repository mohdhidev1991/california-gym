'use strict';

app.controller('DayTemplateAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.day_templates = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetDayTemplates = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/DayTemplate/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.day_templates) {
                $scope.day_templates = response.data.data.day_templates;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetDayTemplates();
    };



    $scope.delete_day_template = function(day_template_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteDayTemplateInstanceCtrl',
            resolve: {
                day_template_id: function() {
                    return day_template_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetDayTemplates();
        }, function() {
            
        });
    };


    $scope.GetDayTemplates();


});



app.controller('ModalDeleteDayTemplateInstanceCtrl', function($scope, $uibModalInstance, $http, day_template_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {day_template_id : day_template_id},
            url: '/api/DayTemplate/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


