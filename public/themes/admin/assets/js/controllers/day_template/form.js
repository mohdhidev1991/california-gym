'use strict';

app.controller('DayTemplateFormController', function($scope, $http, $stateParams, $location) {

    $scope.day_template = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetDayTemplate = function() {
        $http({
            method: 'GET',
            url: '/api/DayTemplate/get/day_template_id='+$stateParams.day_template_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.day_template = response.data.data;
                if($scope.day_template.active && $scope.day_template.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


     $scope.GetLevelClasses = function() {
        $http({
            method: 'GET',
            url: '/api/LevelClass/get'
        }).then(function successCallback(response) {
            if (response.data.data.level_classes.length) {
                $scope.level_classes = response.data.data.level_classes;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetWdays = function() {
        $http({
            method: 'GET',
            url: '/api/Wday/get'
        }).then(function successCallback(response) {
            if (response.data.data.wdays.length) {
                $scope.wdays = response.data.data.wdays;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    if($scope.action==="edit"){
        $scope.GetDayTemplate();
    }else{
        $scope.day_template = {};
        $scope.day_template.new = true;
    }
    $scope.GetSchools();
    $scope.GetLevelClasses();
    $scope.GetWdays();

    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.day_template.active = "Y";
        }else{
            $scope.day_template.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {day_template: $scope.day_template},
                url: '/api/DayTemplate/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/day_templates");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.day_template);
            $http({
                method: 'POST',
                data: {day_template: $scope.day_template},
                url: '/api/DayTemplate/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/day_template/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



