'use strict';

app.controller('DayTemplateItemAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.day_template_items = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetDayTemplateItems = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/DayTemplateItem/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.day_template_items) {
                $scope.day_template_items = response.data.data.day_template_items;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetDayTemplateItems();
    };



    $scope.delete_day_template_item = function(day_template_item_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteDayTemplateItemInstanceCtrl',
            resolve: {
                day_template_item_id: function() {
                    return day_template_item_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetDayTemplateItems();
        }, function() {
            
        });
    };


    $scope.GetDayTemplateItems();


});



app.controller('ModalDeleteDayTemplateItemInstanceCtrl', function($scope, $uibModalInstance, $http, day_template_item_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {day_template_item_id : day_template_item_id},
            url: '/api/DayTemplateItem/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


