'use strict';

app.controller('DayTemplateItemFormController', function($scope, $http, $stateParams, $location) {

    $scope.day_template_item = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetDayTemplateItem = function() {
        $http({
            method: 'GET',
            url: '/api/DayTemplateItem/get/day_template_item_id='+$stateParams.day_template_item_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.day_template_item = response.data.data;
                if($scope.day_template_item.active && $scope.day_template_item.active==="Y"){
                    $scope.active_switch = true;
                }
                if($scope.day_template_item.session_start_time){
                    $scope.timer_session_start_time = moment($scope.day_template_item.session_start_time,'HH:mm');
                }
                if($scope.day_template_item.session_end_time){
                    $scope.timer_session_end_time = moment($scope.day_template_item.session_end_time,'HH:mm');
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetDayTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/DayTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.day_templates.length) {
                $scope.day_templates = response.data.data.day_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };




    if($scope.action==="edit"){
        $scope.GetDayTemplateItem();
    }else{
        $scope.day_template_item = {};
        $scope.day_template_item.new = true;
    }

    $scope.GetDayTemplates();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.day_template_item.active = "Y";
        }else{
            $scope.day_template_item.active = "N";
        }
        $scope.day_template_item.session_start_time = null;
        if($scope.timer_session_start_time){
         $scope.day_template_item.session_start_time = moment($scope.timer_session_start_time).format('HH:mm');
        }
        $scope.day_template_item.session_end_time = null;
        if($scope.timer_session_end_time){
         $scope.day_template_item.session_end_time = moment($scope.timer_session_end_time).format('HH:mm');
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {day_template_item: $scope.day_template_item},
                url: '/api/DayTemplateItem/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/day_template_items");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.day_template_item);
            $http({
                method: 'POST',
                data: {day_template_item: $scope.day_template_item},
                url: '/api/DayTemplateItem/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/day_template_item/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



