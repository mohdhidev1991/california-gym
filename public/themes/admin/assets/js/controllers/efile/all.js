'use strict';

app.controller('EFileAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.efiles = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetEFiles = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/EFile/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.efiles) {
                $scope.efiles = response.data.data.efiles;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetEFiles();
    };



    $scope.delete_efile = function(efile_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteEFileInstanceCtrl',
            resolve: {
                efile_id: function() {
                    return efile_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetEFiles();
        }, function() {
            
        });
    };


    $scope.GetEFiles();


});



app.controller('ModalDeleteEFileInstanceCtrl', function($scope, $uibModalInstance, $http, efile_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {efile_id : efile_id},
            url: '/api/EFile/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


