'use strict';

app.controller('EFileFormController', function($scope, $http, $stateParams, $location) {

    $scope.efile = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetEFile = function() {
        $http({
            method: 'GET',
            url: '/api/EFile/get/efile_id='+$stateParams.efile_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.efile = response.data.data;
                if($scope.efile.active && $scope.efile.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    if($scope.action==="edit"){
        $scope.GetEFile();
    }else{
        $scope.efile = {};
        $scope.efile.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.efile.active = "Y";
        }else{
            $scope.efile.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {efile: $scope.efile},
                url: '/api/EFile/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/efiles");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.efile);
            $http({
                method: 'POST',
                data: {efile: $scope.efile},
                url: '/api/EFile/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/efile/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



