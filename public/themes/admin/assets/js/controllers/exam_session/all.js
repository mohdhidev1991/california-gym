'use strict';

app.controller('ExamSessionAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.exam_sessions = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetExamSessions = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/ExamSession/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.exam_sessions) {
                $scope.exam_sessions = response.data.data.exam_sessions;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetExamSessions();
    };



    $scope.delete_exam_session = function(exam_session_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteExamSessionInstanceCtrl',
            resolve: {
                exam_session_id: function() {
                    return exam_session_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetExamSessions();
        }, function() {
            
        });
    };


    $scope.GetExamSessions();


});



app.controller('ModalDeleteExamSessionInstanceCtrl', function($scope, $uibModalInstance, $http, exam_session_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {exam_session_id : exam_session_id},
            url: '/api/ExamSession/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


