'use strict';

app.controller('ExamSessionFormController', function($scope, $http, $stateParams, $location) {

    $scope.exam_session = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetExamSession = function() {
        $http({
            method: 'GET',
            url: '/api/ExamSession/get/exam_session_id='+$stateParams.exam_session_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.exam_session = response.data.data;
                if($scope.exam_session.active && $scope.exam_session.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    if($scope.action==="edit"){
        $scope.GetExamSession();
    }else{
        $scope.exam_session = {};
        $scope.exam_session.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.exam_session.active = "Y";
        }else{
            $scope.exam_session.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {exam_session: $scope.exam_session},
                url: '/api/ExamSession/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/exam_sessions");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.exam_session);
            $http({
                method: 'POST',
                data: {exam_session: $scope.exam_session},
                url: '/api/ExamSession/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/exam_session/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



