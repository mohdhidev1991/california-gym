'use strict';

app.controller('AddNewRelationFamilyRelationController', function($scope, $http, $stateParams, $location) {
	$scope.family_relation = {};

	
    $scope.GetRelships = function() {
        $http({
            method: 'GET',
            url: '/api/Relship/get'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.relships = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetRelships();



    $scope.GetIDNTypes = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get/'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.idn_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetIDNTypes();


    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.countries = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCountries();


    $scope.GetCities = function() {
        $scope.cities = null;
        if($scope.new_student.country_id){
            $http({
                method: 'GET',
                url: '/api/City/get/country_id='+$scope.new_student.country_id
            }).then(function successCallback(response) {
                if(response.data.status){
                    $scope.cities = response.data.data;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
    //$scope.GetCities();


    $scope.find = function(){
        $scope.not_founded_student_idn = null;
        if ($scope.family_relation.student_idn) {
            $http({
                method: 'GET',
                url: '/api/Student/get/idn=' + $scope.family_relation.student_idn
            }).then(function successCallback(response) {
                if(response.data.data){
                    $scope.student = response.data.data;
                }else{
                    $scope.not_founded_student_idn = true;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.confirm_add_relation_from_idn = function(){
        $scope.success_add_relation = null;
        $scope.errors_add_relation = null;
        if ($scope.student) {
            $http({
                method: 'POST',
                url: '/api/FamilyRelation/add_relation_from_idn/',
                data: {student:$scope.student}
            }).then(function successCallback(response) {
                if(response.data.status){
                    $scope.success_add_relation = true;
                }else{
                    $scope.errors_add_relation = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.save_new_student = function(){
        $scope.success_save_new_student = null;
        $scope.errors_save_new_student = null;
        if ($scope.new_student) {
            $scope.new_student.idn = $scope.family_relation.student_idn;
            $http({
                method: 'POST',
                url: '/api/FamilyRelation/save_new_student/',
                data: {student:$scope.new_student}
            }).then(function successCallback(response) {
                if(response.data.status){
                    $scope.success_save_new_student = true;
                }else{
                    $scope.errors_save_new_student = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.reset = function(){
        $scope.student = null;
        $scope.add_new_student =null;
        $scope.new_student =null;
        $scope.family_relation = null;
        $scope.errors_add_relation = null;
        $scope.success_add_relation = null;
        $scope.not_founded_student_idn = null;
        $scope.success_save_new_student = null;
        $scope.errors_save_new_student = null;
    };


});
