'use strict';

app.controller('EditMyRelationFamilyRelationController', function($scope, $http, $stateParams, $location) {

    $scope.family_relation = null;


    $scope.GetRelships = function() {
        $http({
            method: 'GET',
            url: '/api/Relship/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.relships = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetRelships();


    $scope.GetMyFamilyRelation = function() {

        $http({
            method: 'GET',
            url: '/api/FamilyRelation/get_my_relation/' + $stateParams.family_relation_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.family_relation = response.data.data;
            }else{
                $scope.error_family_relation = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetMyFamilyRelation();


    $scope.save = function() {
        $scope.success_edit_relation = null;
        $scope.errors_edit_relation = null;
        $http({
            method: 'POST',
            url: '/api/FamilyRelation/save_my_family_relation/',
            data: { family_relation: $scope.family_relation }
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.success_edit_relation = true;
            } else {
                $scope.errors_edit_relation = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



});