'use strict';

app.controller('MyRelationsFamilyRelationController', function($scope, $http, $stateParams, $location, $uibModal) {


    $scope.family_relations = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10,50,100];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[1];

    $scope.GetFamilyRelations = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/FamilyRelation/get_my_relations'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.family_relations = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetFamilyRelations();
    };



    $scope.delete_family_relation = function(family_relation_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteFamilyRelationInstanceCtrl',
            resolve: {
                family_relation_id: function() {
                    return family_relation_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetFamilyRelations();
        }, function() {
            
        });
    };


    $scope.GetFamilyRelations();


});



app.controller('ModalDeleteFamilyRelationInstanceCtrl', function($scope, $uibModalInstance, $http, family_relation_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {family_relation_id : family_relation_id},
            url: '/api/FamilyRelation/delete_my_relation'
        }).then(function successCallback(response) {
            if(response.data.status){
                $uibModalInstance.close();    
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

