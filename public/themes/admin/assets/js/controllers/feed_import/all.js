'use strict';

app.controller('FeedImportAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.filter = {};
    $scope.feed_imports = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];

    $scope.order_by = 'id';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetFeedImports();
    };


    $scope.GetFeedImports = function() {
        if($scope.loading) return;

        $scope.empty_results = null;

        $scope.Params = 'active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });


        $http({
            method: 'GET',
            url: '/api/FeedImport/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status) {
                $scope.feed_imports = response.data.data;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.total;
                }
            }else{
                $scope.feed_imports = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.feed_imports = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };




    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetFeedImports();
    };




    $scope.delete_feed_import = function(feed_import_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteFeedImportInstanceCtrl',
            resolve: {
                feed_import_id: function() {
                    return feed_import_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetFeedImports();
        }, function() {

        });
    };


    $scope.log_item = function(feed_import) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalLogItem.html',
            controller: 'ModalLogItemInstanceCtrl',
            resolve: {
                feed_import: function() {
                    return feed_import;
                }
            }
        });
        modalInstance.result.then(function() {
        }, function() {

        });
    };

    $scope.import_new_file = function(feed_import_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalFeedImportNewFile.html',
            controller: 'ModalFeedImportNewFileInstanceCtrl',
            resolve: {}
        });
        modalInstance.result.then(function() {
            $scope.GetFeedImports();
        }, function() {
            $scope.GetFeedImports();
        });
    };


    $scope.GetFeedImports();


});



app.controller('ModalDeleteFeedImportInstanceCtrl', function($scope, $uibModalInstance, $http, feed_import_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { feed_import_id: feed_import_id },
            url: '/api/FeedImport/delete'
        }).then(function successCallback(response) {
            if(response.data.status){
                $uibModalInstance.close();
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


app.controller('ModalLogItemInstanceCtrl', function($scope, $uibModalInstance, feed_import) {
    $scope.feed_import = angular.copy(feed_import);
    $scope.feed_import.log = angular.fromJson(feed_import.log);
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('ModalFeedImportNewFileInstanceCtrl', function($scope, $uibModalInstance, $http, FileUploader) {
    $scope.import = function() {
        
    };
    $scope.cancel_import = function() {
        $uibModalInstance.dismiss('cancel');
    };

    var uploader = $scope.uploader = new FileUploader({
        url: '/api/FeedImport/upload'
    });

    // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 1;
        }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        $scope.success_upload = null;
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        //$scope.errors_upload = null;
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        $scope.errors_upload = null;
        if(response.status){
            $scope.success_upload = true;
        }else{
            fileItem.isError = true;
            fileItem.isSuccess = false;
            fileItem.isUploaded = false;
            $scope.errors_upload = response.message;
        }
        //console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);
});

