'use strict';

app.controller('FeedImportProcessController', function($scope, $http, $stateParams, $location, $timeout, $filter, $translate, $uibModal) {

    $scope.processors = [
        {
            lookup_code: "students_parent",
            processor_name: "FeedImport.ImportStudentsWithParent",
        },
        {
            lookup_code: "school_employees",
            processor_name: "FeedImport.ImportSchoolEmployees",
        }
    ];
    $scope.GetFeedImport = function(){
        $scope.error_get = null;
        $scope.feed_import = {};
        $http({
            method: 'GET',
            url: '/api/FeedImport/get/feed_import_id=' + $stateParams.feed_import_id
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.feed_import = response.data.data;
            }else{
                $scope.error_get = true;
            }
        }, function errorCallback(response) {
            $scope.error_get = true;
            console.log('error');
        });
    };
    $scope.GetFeedImport();


    $scope.GetSchoolYears = function(){
        $scope.school_years = null;
        $http({
            method: 'GET',
            url: '/api/SchoolYear/get/current_school_id'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.school_years = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetSchoolYears();


    $scope.GetSchoolDepartments = function(){
        $scope.sdepartments = null;
        $http({
            method: 'GET',
            url: '/api/Sdepartment/get/current_school'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.sdepartments = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetSchoolDepartments();


    $scope.start_process = function(){
        if(!$scope.feed_import){
            return;
        }
        $scope.total = 0;
        $scope.item = 0;
        $scope.pourcent = "0%";
        $scope.processing = false;
        $scope.finish_process = false;
        $scope.errors_process = null;
        $scope.messages = null;

        $http({
            method: 'POST',
            data: {feed_import_id: $scope.feed_import.id, processor: $scope.feed_import.processor},
            url: '/api/FeedImport/process'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.total = response.data.total;
                $scope.item = 1;
                $scope.messages = [];
                $scope.pourcent = "0%";
                $scope.processing = true;
                $scope.process_item(1);
            }else{
                $scope.errors_process = response.data.message;
                $scope.processing = false;
            }
        }, function errorCallback(response) {
            $scope.processing = false;
            console.log('error');
        });
    };

    $scope.process_item = function(item){
        if(!$scope.feed_import){
            return;
        }
        $http({
            method: 'POST',
            data: {feed_import_id: $scope.feed_import.id, processor: $scope.feed_import.processor, item: $scope.item, options: $scope.options},
            url: '/api/FeedImport/process'
        }).then(function successCallback(response) {
            if(response.data.status){
                if(response.data.attributes && response.data.attributes.finish){
                    $scope.finish_process = true;
                    $scope.processing = false;
                }else{
                    $timeout(function () {
                        $scope.item++;
                        $scope.pourcent = $filter('number')(($scope.item-1)/$scope.total * 100, 0) + '%';
                        if(response.data.attributes && response.data.attributes.message){
                            if( typeof response.data.attributes.message==='object' ){
                                angular.forEach(response.data.attributes.message, function(item_msg, key_item_msg) {
                                    var msg_t = $filter('translate')(item_msg);
                                    if(response.data.attributes.success){
                                        $scope.messages.push('<span class="text-success" >'+msg_t+'<span>');
                                    }else{
                                        $scope.messages.push('<span class="text-danger" >'+msg_t+'<span>');
                                    }
                                });
                            }else{
                                var msg_t = $filter('translate')(response.data.attributes.message);
                                if(response.data.attributes.success){
                                    $scope.messages.push('<span class="text-success" >'+msg_t+'<span>');
                                }else{
                                    $scope.messages.push('<span class="text-danger" >'+msg_t+'<span>');
                                }
                            }
                        }
                        $scope.process_item($scope.item);
                    }, 1000);
                }
            }else{
                $scope.errors_process = response.data.message;
                $scope.processing = false;
            }
            
        }, function errorCallback(response) {
            $scope.processing = false;
            console.log('error');
        });
    };

    $scope.$watch('options.school_year_id', function(new_val, old_val, scope){
        if(new_val){
            $scope.school_year_started = null;
            angular.forEach($scope.school_years, function(school_year, key) {
              if(school_year.id===new_val){
                var school_year_start_hdate = moment(school_year.school_year_start_hdate.toString(),'iYYYYiMiD').format('iYYYYiMiD');
                var current_hdate = moment().format('iYYYYiMiD');
                if(current_hdate>school_year_start_hdate){
                    $scope.school_year_started = true;
                }
              }
            });
        }
    });




    $scope.log_item = function(feed_import) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalLogItemInProcess.html',
            controller: 'ModalLogItemInProcessInstanceCtrl',
            resolve: {
                feed_import: function() {
                    return feed_import;
                }
            }
        });
        modalInstance.result.then(function() {
        }, function() {

        });
    };

});




app.controller('ModalLogItemInProcessInstanceCtrl', function($scope, $uibModalInstance, feed_import) {
    $scope.feed_import = angular.copy(feed_import);
    $scope.feed_import.log = angular.fromJson(feed_import.log);
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



