'use strict';

app.controller('GenreAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.genres = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetGenres = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Genre/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.genres) {
                $scope.genres = response.data.data.genres;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetGenres();
    };



    $scope.delete_genre = function(genre_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteGenreInstanceCtrl',
            resolve: {
                genre_id: function() {
                    return genre_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetGenres();
        }, function() {
            
        });
    };


    $scope.GetGenres();


});



app.controller('ModalDeleteGenreInstanceCtrl', function($scope, $uibModalInstance, $http, genre_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {genre_id : genre_id},
            url: '/api/Genre/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


