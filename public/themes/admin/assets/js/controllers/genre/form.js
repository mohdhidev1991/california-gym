'use strict';

app.controller('GenreFormController', function($scope, $http, $stateParams, $location) {

    $scope.genre = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetGenre = function() {
        $http({
            method: 'GET',
            url: '/api/Genre/get/genre_id='+$stateParams.genre_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.genre = response.data.data;
                if($scope.genre.active && $scope.genre.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetGenre();
    }else{
        $scope.genre = {};
        $scope.genre.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.genre.active = "Y";
        }else{
            $scope.genre.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {genre: $scope.genre},
                url: '/api/Genre/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/genres");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.genre);
            $http({
                method: 'POST',
                data: {genre: $scope.genre},
                url: '/api/Genre/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/genre/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



