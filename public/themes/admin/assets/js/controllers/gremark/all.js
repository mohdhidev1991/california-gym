'use strict';

app.controller('GremarkAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.gremarks = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetGremarks = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Gremark/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.gremarks) {
                $scope.gremarks = response.data.data.gremarks;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetGremarks();
    };



    $scope.delete_gremark = function(gremark_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteGremarkInstanceCtrl',
            resolve: {
                gremark_id: function() {
                    return gremark_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetGremarks();
        }, function() {
            
        });
    };


    $scope.GetGremarks();


});



app.controller('ModalDeleteGremarkInstanceCtrl', function($scope, $uibModalInstance, $http, gremark_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {gremark_id : gremark_id},
            url: '/api/Gremark/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


