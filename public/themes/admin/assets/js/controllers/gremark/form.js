'use strict';

app.controller('GremarkFormController', function($scope, $http, $stateParams, $location) {

    $scope.gremark = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetGremark = function() {
        $http({
            method: 'GET',
            url: '/api/Gremark/get/gremark_id='+$stateParams.gremark_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.gremark = response.data.data;
                if($scope.gremark.active && $scope.gremark.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetGremark();
    }else{
        $scope.gremark = {};
        $scope.gremark.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.gremark.active = "Y";
        }else{
            $scope.gremark.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {gremark: $scope.gremark},
                url: '/api/Gremark/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/gremarks");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.gremark);
            $http({
                method: 'POST',
                data: {gremark: $scope.gremark},
                url: '/api/Gremark/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/gremark/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



