'use strict';

app.controller('HolidayAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.holidays = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetHolidays = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Holiday/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.holidays) {
                $scope.holidays = response.data.data.holidays;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetHolidays();
    };



    $scope.delete_holiday = function(holiday_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteHolidayInstanceCtrl',
            resolve: {
                holiday_id: function() {
                    return holiday_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetHolidays();
        }, function() {
            
        });
    };


    $scope.GetHolidays();


});



app.controller('ModalDeleteHolidayInstanceCtrl', function($scope, $uibModalInstance, $http, holiday_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {holiday_id : holiday_id},
            url: '/api/Holiday/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


