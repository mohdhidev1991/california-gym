'use strict';

app.controller('HolidayFormController', function($scope, $http, $stateParams, $location) {

    $scope.holiday = null;
    $scope.schools = null;
    $scope.countries = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetHoliday = function() {
        $http({
            method: 'GET',
            url: '/api/Holiday/get/holiday_id='+$stateParams.holiday_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.holiday = response.data.data;
                if($scope.holiday.active && $scope.holiday.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.data.countries.length) {
                $scope.countries = response.data.data.countries;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetHoliday();
    }else{
        $scope.holiday = {};
        $scope.holiday.new = true;
    }
    $scope.GetCountries();
    $scope.GetSchools();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.holiday.active = "Y";
        }else{
            $scope.holiday.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {holiday: $scope.holiday},
                url: '/api/Holiday/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/holidays");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.holiday);
            $http({
                method: 'POST',
                data: {holiday: $scope.holiday},
                url: '/api/Holiday/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/holiday/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



