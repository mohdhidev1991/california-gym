'use strict';

app.controller('HolidayManageController', function($scope, $http, $uibModal, $stateParams, $location, $rootScope, $translate, uiCalendarConfig, $filter) {

    $scope.events = [];
    $scope.date_format = 'dd-MM-yyyy';


    $scope.format_date = function(date_, format_) {
        return moment(date_).format(format_);
    };

    $scope.yearWeek = function(date) {
        if (!date) { date = moment(); }
        return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 1) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
    };

    $scope.GetHolidays = function() {
        $scope.holidays = [];
        $('.calendar').fullCalendar('removeEvents');
        $('.calendar').addClass('loading');
        $scope.Params = '';
        $http({
            method: 'GET',
            url: '/api/Holiday/get/' + $scope.Params
        }).then(function successCallback(response) {
            $('.calendar').fullCalendar('removeEvents');
            $('.calendar').removeClass('loading');
            if (response.data.status) {
                $scope.holidays = response.data.data;
                angular.forEach($scope.holidays, function(obj, key) {
                    $scope.events.push({
                        data: obj,
                        title: obj.description,
                        start: moment(obj.date),
                        end: moment(obj.date),
                        allDay: true,
                        color: "#23b7e5"
                    });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.clean_hours = function(elements, deleteValue) {
        for (var i = 0; i < elements.length; i++) {
            if (elements[i] == deleteValue) {
                elements.splice(i, 1);
                i--;
            }
        }
        return elements;
    };


    /* alert on dayClick */
    $scope.precision = 400;
    $scope.lastClickTime = 0;
    $scope.alertOnDayClick = function(date_event, jsEvent, view) {
        var time = new Date().getTime();
        if (time - $scope.lastClickTime <= $scope.precision) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ModalAddHoliday.html',
                controller: 'ModalAddHolidayInstanceCtrl',
                resolve: {
                    date_event: function() {
                        return date_event;
                    }
                }
            });
            modalInstance.result.then(function(data) {
                $('.calendar').fullCalendar('refetchEvents');
            }, function() {

            });

        }
        $scope.lastClickTime = time;
    };


    $scope.overlay = $('.fc-overlay');
    $scope.alertOnMouseOver = function(event, jsEvent, view) {
        $scope.event = event;
        $scope.overlay.removeClass('left right top').find('.arrow').removeClass('left right top pull-up');
        var wrap = $(jsEvent.target).closest('.fc-event');
        var cal = wrap.closest('.calendar');
        var left = wrap.offset().left - cal.offset().left;
        var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
        var top = cal.height() - (wrap.offset().top - cal.offset().top + wrap.height());
        if (right > $scope.overlay.width()) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up');
        } else if (left > $scope.overlay.width()) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
        } else {
            $scope.overlay.find('.arrow').addClass('top');
        }
        if (top < $scope.overlay.height()) {
            $scope.overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass('pull-down')
        }
        (wrap.find('.fc-overlay').length === 0) && wrap.append($scope.overlay);
    };




    $scope.alertOnEventClick = function(event, jsEvent, view) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddHoliday.html',
            controller: 'ModalEditHolidayInstanceCtrl',
            resolve: {
                holiday: function() {
                    return event.data;
                }
            }
        });
        modalInstance.result.then(function(data) {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };

    /* config object */
    $scope.uiConfig = {
        calendar: {
            defaultView: 'month',
            allDaySlot: false,
            firstDay: 1,
            timezone: 'local',
            lang: $rootScope.app.settings.selectLang,
            height: 650,
            editable: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            dayClick: $scope.alertOnDayClick,
            eventClick: $scope.alertOnEventClick,
            events: function(start, end, timezone, callback) {
                $scope.GetHolidays();
            },
            eventAfterAllRender: function(view) {
                
            },
        }
    };

    /* add custom event*/
    $scope.addEvent = function() {
        $scope.events.push({
            title: 'New Event',
            start: new Date(y, m, d),
            className: ['b-l b-2x b-info']
        });
    };

    /* remove event */
    $scope.remove = function(index) {
        $scope.events.splice(index, 1);
    };

    /* Change View */
    $scope.changeView = function(view, calendar) {
        $('.calendar').fullCalendar('changeView', view);
    };



    $scope.today = function(calendar) {
        $('.calendar').fullCalendar('today');
    };


    $scope.change_datepicker = function() {
        $('.calendar').fullCalendar('gotoDate', $scope.popup_datepicker.date);
    };

    $scope.open_datepicker = function() {
        $scope.popup_datepicker.opened = true;
    };
    $scope.popup_datepicker = {
        date: moment().toDate(),
        opened: false,
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: moment().add(1, 'years').toDate(),
        minDate: null,
        startingDay: 1
    };

    /* event sources array*/
    $scope.eventSources = [$scope.events];

});










app.controller('ModalAddHolidayInstanceCtrl', function($scope, $uibModalInstance, $http, date_event, $location, Notification) {

    $scope.holiday = {};
    $scope.is_new = true;
    $scope.holiday.new = true;
    $scope.holiday.active = "Y";
    $scope.date_format = 'dd-MM-yyyy';
    $scope.dateOptions = {
        //minDate: moment().add(7, 'days').startOf('week').toDate(),
        //maxDate: moment().add(7, 'days').endOf('week').toDate()
    };

    $scope.holiday.date = moment(date_event).toDate();
    $scope.add = function() {
        $http({
            method: 'POST',
            data: { holiday: $scope.holiday },
            url: '/api/Holiday/save'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $uibModalInstance.close();
            } else {
                $scope.errors_form = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });


    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});









app.controller('ModalEditHolidayInstanceCtrl', function($scope, $uibModalInstance, $http, holiday, $location, Notification, $uibModal) {
    
    $scope.holiday = holiday;

    $scope.format_date = function(date_, format_) {
        return moment(date_).format(format_);
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.delete = function(holiday_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalDeleteHoliday.html',
            controller: 'ModalDeleteHolidayInstanceCtrl',
            resolve: {
                holiday_id: function() {
                    return holiday_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
        $uibModalInstance.dismiss('cancel');
    };
});





app.controller('ModalDeleteHolidayInstanceCtrl', function($scope, $uibModalInstance, $http, holiday_id, $location, Notification, $filter) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { holiday_id: holiday_id },
            url: '/api/Holiday/delete'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            } else {
                angular.forEach(response.data.message, function(value, key) {
                    Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

