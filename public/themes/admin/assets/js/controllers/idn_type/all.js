'use strict';

app.controller('IdnTypeAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.idn_types = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetIdnTypes = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/IdnType/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.idn_types) {
                $scope.idn_types = response.data.data.idn_types;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetIdnTypes();
    };



    $scope.delete_idn_type = function(idn_type_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteIdnTypeInstanceCtrl',
            resolve: {
                idn_type_id: function() {
                    return idn_type_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetIdnTypes();
        }, function() {
            
        });
    };


    $scope.GetIdnTypes();


});



app.controller('ModalDeleteIdnTypeInstanceCtrl', function($scope, $uibModalInstance, $http, idn_type_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {idn_type_id : idn_type_id},
            url: '/api/IdnType/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


