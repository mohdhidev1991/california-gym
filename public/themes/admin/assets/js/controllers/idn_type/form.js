'use strict';

app.controller('IdnTypeFormController', function($scope, $http, $stateParams, $location) {

    $scope.idn_type = null;
    $scope.active_switch = false;
    $scope.countries = null;
    $scope.action = $stateParams.action;


    $scope.GetIdnType = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get/idn_type_id='+$stateParams.idn_type_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.idn_type = response.data.data;
                if($scope.idn_type.active && $scope.idn_type.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.data.countries.length) {
                $scope.countries = response.data.data.countries;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetIdnType();
    }else{
        $scope.idn_type = {};
        $scope.idn_type.new = true;
    }
    $scope.GetCountries();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.idn_type.active = "Y";
        }else{
            $scope.idn_type.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {idn_type: $scope.idn_type},
                url: '/api/IdnType/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/idn_types");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.idn_type);
            $http({
                method: 'POST',
                data: {idn_type: $scope.idn_type},
                url: '/api/IdnType/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/idn_type/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



