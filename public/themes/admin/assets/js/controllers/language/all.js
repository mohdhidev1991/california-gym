'use strict';

app.controller('LanguageAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.languages = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetLanguages = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Language/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.languages) {
                $scope.languages = response.data.data.languages;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetLanguages();
    };



    $scope.delete_language = function(language_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteLanguageInstanceCtrl',
            resolve: {
                language_id: function() {
                    return language_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetLanguages();
        }, function() {
            
        });
    };


    $scope.GetLanguages();


});



app.controller('ModalDeleteLanguageInstanceCtrl', function($scope, $uibModalInstance, $http, language_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {language_id : language_id},
            url: '/api/Language/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


