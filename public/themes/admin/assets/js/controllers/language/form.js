'use strict';

app.controller('LanguageFormController', function($scope, $http, $stateParams, $location) {

    $scope.language = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetLanguage = function() {
        $http({
            method: 'GET',
            url: '/api/Language/get/language_id='+$stateParams.language_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.language = response.data.data;
                if($scope.language.active && $scope.language.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetLanguage();
    }else{
        $scope.language = {};
        $scope.language.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.language.active = "Y";
        }else{
            $scope.language.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {language: $scope.language},
                url: '/api/Language/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/languages");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.language);
            $http({
                method: 'POST',
                data: {language: $scope.language},
                url: '/api/Language/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/language/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



