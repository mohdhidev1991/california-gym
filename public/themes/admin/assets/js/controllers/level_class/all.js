'use strict';

app.controller('LevelClassAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetLevelClasss = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/LevelClass/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.level_classes) {
                $scope.level_classes = response.data.data.level_classes;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetLevelClasss();
    };



    $scope.delete_level_class = function(level_class_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteLevelClassInstanceCtrl',
            resolve: {
                level_class_id: function() {
                    return level_class_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetLevelClasss();
        }, function() {
            
        });
    };


    $scope.GetLevelClasss();


});



app.controller('ModalDeleteLevelClassInstanceCtrl', function($scope, $uibModalInstance, $http, level_class_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {level_class_id : level_class_id},
            url: '/api/LevelClass/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


