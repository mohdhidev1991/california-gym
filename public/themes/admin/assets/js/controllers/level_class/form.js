'use strict';

app.controller('LevelClassFormController', function($scope, $http, $stateParams, $location) {

    $scope.level_class = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetLevelClass = function() {
        $http({
            method: 'GET',
            url: '/api/LevelClass/get/level_class_id='+$stateParams.level_class_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.level_class = response.data.data;
                if($scope.level_class.active && $scope.level_class.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetSchoolLevels = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolLevel/get'
        }).then(function successCallback(response) {
            if (response.data.data.school_levels) {
                $scope.school_levels = response.data.data.school_levels;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetLevelClass();
    }else{
        $scope.level_class = {};
        $scope.level_class.new = true;
    }
    $scope.GetSchoolLevels();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.level_class.active = "Y";
        }else{
            $scope.level_class.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {level_class: $scope.level_class},
                url: '/api/LevelClass/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/level_classes");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.level_class);
            $http({
                method: 'POST',
                data: {level_class: $scope.level_class},
                url: '/api/LevelClass/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/level_class/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



