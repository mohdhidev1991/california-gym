'use strict';

app.controller('LevelsTemplateAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.levels_templates = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetLevelsTemplates = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/LevelsTemplate/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.levels_templates) {
                $scope.levels_templates = response.data.data.levels_templates;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetLevelsTemplates();
    };



    $scope.delete_levels_template = function(levels_template_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteLevelsTemplateInstanceCtrl',
            resolve: {
                levels_template_id: function() {
                    return levels_template_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetLevelsTemplates();
        }, function() {
            
        });
    };


    $scope.GetLevelsTemplates();


});



app.controller('ModalDeleteLevelsTemplateInstanceCtrl', function($scope, $uibModalInstance, $http, levels_template_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {levels_template_id : levels_template_id},
            url: '/api/LevelsTemplate/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

