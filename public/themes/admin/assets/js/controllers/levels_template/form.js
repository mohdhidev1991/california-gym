'use strict';

app.controller('LevelsTemplateFormController', function($scope, $http, $stateParams, $location) {

    $scope.levels_template = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetLevelsTemplate = function() {
        $http({
            method: 'GET',
            url: '/api/LevelsTemplate/get/levels_template_id='+$stateParams.levels_template_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.levels_template = response.data.data;
                if($scope.levels_template.active && $scope.levels_template.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetLevelsTemplate();
    }else{
        $scope.levels_template = {};
        $scope.levels_template.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.levels_template.active = "Y";
        }else{
            $scope.levels_template.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {levels_template: $scope.levels_template},
                url: '/api/LevelsTemplate/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/levels_templates");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.levels_template);
            $http({
                method: 'POST',
                data: {levels_template: $scope.levels_template},
                url: '/api/LevelsTemplate/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/levels_template/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



