'use strict';

app.controller('MobileTypeAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.mobile_types = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetMobileTypes = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/MobileType/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.mobile_types) {
                $scope.mobile_types = response.data.data.mobile_types;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetMobileTypes();
    };



    $scope.delete_mobile_type = function(mobile_type_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteMobileTypeInstanceCtrl',
            resolve: {
                mobile_type_id: function() {
                    return mobile_type_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetMobileTypes();
        }, function() {
            
        });
    };


    $scope.GetMobileTypes();


});



app.controller('ModalDeleteMobileTypeInstanceCtrl', function($scope, $uibModalInstance, $http, mobile_type_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {mobile_type_id : mobile_type_id},
            url: '/api/MobileType/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


