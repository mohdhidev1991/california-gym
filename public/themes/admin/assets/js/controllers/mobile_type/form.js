'use strict';

app.controller('MobileTypeFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.mobile_type = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetMobileType = function() {
        $http({
            method: 'GET',
            url: '/api/MobileType/get/mobile_type_id='+$stateParams.mobile_type_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.mobile_type = response.data.data;
                if($scope.mobile_type.active && $scope.mobile_type.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetMobileType();
    }else{
        $scope.mobile_type = {};
        $scope.mobile_type.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.mobile_type.active = "Y";
        }else{
            $scope.mobile_type.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {mobile_type: $scope.mobile_type},
                url: '/api/MobileType/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $state.go("mobile_type.all");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.mobile_type);
            $http({
                method: 'POST',
                data: {mobile_type: $scope.mobile_type},
                url: '/api/MobileType/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/MobileType/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



