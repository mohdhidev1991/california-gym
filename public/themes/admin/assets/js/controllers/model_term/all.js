'use strict';

app.controller('ModelTermAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.model_terms = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetModelTerms = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/ModelTerm/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.model_terms) {
                $scope.model_terms = response.data.data.model_terms;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetModelTerms();
    };



    $scope.delete_model_term = function(model_term_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteModelTermInstanceCtrl',
            resolve: {
                model_term_id: function() {
                    return model_term_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetModelTerms();
        }, function() {
            
        });
    };


    $scope.GetModelTerms();


});



app.controller('ModalDeleteModelTermInstanceCtrl', function($scope, $uibModalInstance, $http, model_term_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {model_term_id : model_term_id},
            url: '/api/ModelTerm/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


