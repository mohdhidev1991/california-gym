'use strict';

app.controller('ModelTermFormController', function($scope, $http, $stateParams, $location) {

    $scope.model_term = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetModelTerm = function() {
        $http({
            method: 'GET',
            url: '/api/ModelTerm/get/model_term_id='+$stateParams.model_term_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.model_term = response.data.data;
                if($scope.model_term.active && $scope.model_term.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    if($scope.action==="edit"){
        $scope.GetModelTerm();
    }else{
        $scope.model_term = {};
        $scope.model_term.new = true;
    }
    $scope.GetSchools();

    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.model_term.active = "Y";
        }else{
            $scope.model_term.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {model_term: $scope.model_term},
                url: '/api/ModelTerm/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/model_terms");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.model_term);
            $http({
                method: 'POST',
                data: {model_term: $scope.model_term},
                url: '/api/ModelTerm/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/model_term/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



