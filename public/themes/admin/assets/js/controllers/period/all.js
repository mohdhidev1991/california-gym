'use strict';

app.controller('PeriodAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.periods = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 10;
    $scope.optionsItemsPerPage = [10, 50, 100, 500];

    $scope.GetPeriods = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Period/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.periods) {
                $scope.periods = response.data.data.periods;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetPeriods();
    };



    $scope.delete_period = function(period_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeletePeriodInstanceCtrl',
            resolve: {
                period_id: function() {
                    return period_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetPeriods();
        }, function() {
            
        });
    };


    $scope.GetPeriods();


});



app.controller('ModalDeletePeriodInstanceCtrl', function($scope, $uibModalInstance, $http, period_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {period_id : period_id},
            url: '/api/Period/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

