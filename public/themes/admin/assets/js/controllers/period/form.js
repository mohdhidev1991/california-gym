'use strict';

app.controller('PeriodFormController', function($scope, $http, $stateParams, $location) {

    $scope.period = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetPeriod = function() {
        $http({
            method: 'GET',
            url: '/api/Period/get/period_id='+$stateParams.period_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.period = response.data.data;
                if($scope.period.active && $scope.period.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetPeriod();
    }else{
        $scope.period = {};
        $scope.period.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.period.active = "Y";
        }else{
            $scope.period.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {period: $scope.period},
                url: '/api/Period/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/periods");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.period);
            $http({
                method: 'POST',
                data: {period: $scope.period},
                url: '/api/Period/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/period/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



