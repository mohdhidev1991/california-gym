'use strict';

app.controller('PlanningExportGRHController', function($scope, $http, $uibModal, $stateParams, $location, $filter) {

    $scope.url_export_grh = null;
    moment.locale('fr');
      let DateStart =   $('#startDate').val();
      let EndStart =   $('#endDate').val();

     $scope.dateRange = {
        startDate: moment().format('YYYY-MM-DD'),
        endDate: moment().format('YYYY-MM-DD'),
        date: moment().format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD')
    };


    $scope.daterangepickerOptions = {
        format: 'YYYY-MM-DD',
        locale: {
            applyLabel: $filter('translate')('Global.Apply'),
            fromLabel: $filter('translate')('Global.From'),
            format: "YYYY-MM-DD",
            toLabel: $filter('translate')('Global.To'),
            cancelLabel: $filter('translate')('Global.Cancel')
                //customRangeLabel: 'Custom range'
        }
    };

    $scope.popup_datepicker = {
        date: moment().add(7, 'days').startOf('week').toDate(),
        opened: false,
    };


    $scope.date_format = 'M!/d!/yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: null,
        minDate: null,
        startingDay: 1,
        dateDisabled: function(option) {
            return (option.mode === 'day' && (option.date.getDay() !== 1));
        }
    };

    $scope.open_datepicker = function() {
        //$scope.popup_datepicker.opened = true;
        $('#daterangepicker').first().click();
    };

    $scope.disabledDates = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 1));
    };
    $scope.Params = 'startdate=' + $scope.dateRange.startDate +',enddate=' + $scope.dateRange.endDate ;
    $scope.list_export_grh = null;
    $http({
        method: 'GET',
        url: '/api/CourseSession/exportgrh/' + $scope.Params
    }).then(function successCallback(response){

        $scope.url_export_grh = window.config.base_url + '/api/CourseSession/exportgrh/' + $scope.Params + ',export=csv';
        
        console.log($scope.url_export_grh);
        
        
        if (response.data.status && response.data.data.length) {
            $scope.list_export_grh = response.data.data ;
        } else {
            $scope.url_export_grh = null;
        }
    
    }, function errorCallback(response) {
        console.log('error');
    });


    $scope.get_export = function() {

                $scope.dateRange.startDate = moment($scope.dateRange.date.substring(10, 0)).format('YYYY-MM-DD');
                $scope.dateRange.endDate = moment($scope.dateRange.date.substring(23, 13)).format('YYYY-MM-DD');
                $scope.Params = 'startdate=' + $scope.dateRange.startDate +',enddate=' + $scope.dateRange.endDate ;
                console.log($scope.Params);
            
                
                $scope.list_export_grh = null;
                $http({
                    method: 'GET',
                    url: '/api/CourseSession/exportgrh/' + $scope.Params
                }).then(function successCallback(response){

                    $scope.url_export_grh = window.config.base_url + '/api/CourseSession/exportgrh/' + $scope.Params + ',export=csv';
                    
                    console.log($scope.url_export_grh);
                    
                    
                    if (response.data.status && response.data.data.length) {
                        $scope.list_export_grh = response.data.data ;
                    } else {
                        $scope.url_export_grh = null;
                    }
                
                }, function errorCallback(response) {
                    console.log('error');
                });


    }
    
});