'use strict';

app.controller('PlanningHistoryController', function($scope, $http, $uibModal, $stateParams, $location, $rootScope, $translate, uiCalendarConfig, $filter) {

    $scope.club = null;
    $scope.room = null;
    $scope.course_sessions = [];
    $scope.events = [];
    $scope.cost_per_room = 0;
    $scope.cost_per_club = 0;
    $scope.url_print_pdf = null;
    $scope.url_print_all_pdf = null;

    $scope.date_format = 'dd-MM-yyyy';

    $scope.GetClubs = function() {
        $('.calendar').hide();
        $scope.Params = null;
        $http({
            method: 'GET',
            url: '/api/Club/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetClubs();

    $scope.GetRooms = function() {
        $scope.rooms = false;
        if (!$scope.club) {
            return;
        }
        $scope.Params = 'club_id=' + $scope.club.id;
        $http({
            method: 'GET',
            url: '/api/Room/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.length) {
                $scope.rooms = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.format_date = function(date_, format_) {
        return moment(date_).format(format_);
    };

    $scope.yearWeek = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('Y'));
        // return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 10) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
    };

    $scope.weekForDate = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('W'));
    };

    $scope.GetCourseSessions = function() {
        $scope.course_sessions = [];
        $scope.cost_per_room = 0;
        $scope.cost_per_club = 0;
        $scope.url_print_pdf = null;
        $scope.url_print_all_pdf = null;
        $scope.empty_course_sessions = false;
        $('.calendar').fullCalendar('removeEvents');
        $('.calendar').addClass('loading');
        if (!$scope.club || !$scope.room) {
            return;
        }

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        var year_num = this.yearWeek($('.calendar').fullCalendar('getDate'));


        $scope.url_print_pdf = window.config.base_url + '/planning/' + $scope.club.id + '/' + $scope.room.id + '/' + year_num+ '/' + week_num + '/print_pdf';
        $scope.url_print_all_pdf = window.config.base_url + '/planning/' + $scope.club.id + '/all_rooms/' + year_num + '/' + week_num + '/print_pdf';
        console.log($scope.url_print_all_pdf);
        $scope.Params = 'join_obj=all,club_id=' + $scope.club.id + ',room_id=' + $scope.room.id + ',week_num=' + week_num + ',week_year=' + year_num;
        $http({
            method: 'GET',
            url: '/api/CourseSession/get/' + $scope.Params
        }).then(function successCallback(response) {
            $('.calendar').fullCalendar('removeEvents');
            $('.calendar').removeClass('loading');
            if (response.data.status) {
                $scope.course_sessions = response.data.data;
                if(!$scope.course_sessions.length){
                    $scope.empty_course_sessions = true;
                }
                angular.forEach($scope.course_sessions, function(obj, key) {
                    if(typeof(obj.coach)==='undefined' || !obj.coach){
                        obj.coach = {};
                    }
                    $scope.events.push({
                        data: obj,
                        title: obj.course.course_name + ' - ' + obj.coach.firstname+' '+ obj.coach.lastname + ' #' + obj.coach.registration_number,
                        start: moment(obj.course_session_date + ' ' + obj.course_start_time),
                        end: moment(obj.course_session_date + ' ' + obj.course_end_time),
                        color: obj.course.color,
                    });
                });
                if (response.data.attributes.cost_per_club) {
                    $scope.cost_per_club = response.data.attributes.cost_per_club;
                }
                if (response.data.attributes.cost_per_room) {
                    $scope.cost_per_room = response.data.attributes.cost_per_room;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.clean_hours = function(elements, deleteValue) {
        for (var i = 0; i < elements.length; i++) {
            if (elements[i] == deleteValue) {
                elements.splice(i, 1);
                i--;
            }
        }
        return elements;
    };

    $scope.select_club = function(club) {
        $scope.club = club;
        $scope.room = null;
        $scope.GetRooms();
        var opening_hours = [$scope.club.opening_hour_1, $scope.club.opening_hour_2, $scope.club.opening_hour_3, $scope.club.opening_hour_4, $scope.club.opening_hour_5, $scope.club.opening_hour_6, $scope.club.opening_hour_7];
        opening_hours = $scope.clean_hours(opening_hours, undefined);
        var closing_hours = [$scope.club.closing_hour_1, $scope.club.closing_hour_2, $scope.club.closing_hour_3, $scope.club.closing_hour_4, $scope.club.closing_hour_5, $scope.club.closing_hour_6, $scope.club.closing_hour_7];
        closing_hours = $scope.clean_hours(closing_hours, undefined);
        $scope.uiConfig.calendar.minTime = $filter('orderBy')(opening_hours)[0];
        if( parseInt(moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').format('HH')) == 23 ){
            $scope.uiConfig.calendar.maxTime = '23:59';
        }else{
         $scope.uiConfig.calendar.maxTime = moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').add(1, 'hours').format('HH:mm');   
        }
        $('.calendar').hide();
    };

    $scope.select_room = function(room) {
        $('.calendar').show();
        if ((moment().isoWeek() !== $('.calendar').fullCalendar('getDate').isoWeek()) || !$scope.room) {
            $('.calendar').fullCalendar('today');
        }
        $scope.room = room;
        $('.calendar').fullCalendar('refetchEvents');
    };



    /* alert on dayClick */
    $scope.precision = 400;
    $scope.lastClickTime = 0;


    $scope.overlay = $('.fc-overlay');
    $scope.alertOnMouseOver = function(event, jsEvent, view) {
        $scope.event = event;
        $scope.overlay.removeClass('left right top').find('.arrow').removeClass('left right top pull-up');
        var wrap = $(jsEvent.target).closest('.fc-event');
        var cal = wrap.closest('.calendar');
        var left = wrap.offset().left - cal.offset().left;
        var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
        var top = cal.height() - (wrap.offset().top - cal.offset().top + wrap.height());
        if (right > $scope.overlay.width()) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up');
        } else if (left > $scope.overlay.width()) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
        } else {
            $scope.overlay.find('.arrow').addClass('top');
        }
        if (top < $scope.overlay.height()) {
            $scope.overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass('pull-down')
        }
        (wrap.find('.fc-overlay').length === 0) && wrap.append($scope.overlay);
    };

    /* config object */
    $scope.uiConfig = {
        calendar: {
            defaultView: 'agendaWeek',
            allDaySlot: false,
            firstDay: 1,
            timezone: 'local',
            lang: $rootScope.app.settings.selectLang,
            height: 650,
            editable: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            events: function(start, end, timezone, callback) {
                $scope.GetCourseSessions();
            },
            eventAfterAllRender: function(view) {
                if (!$('.calendar').fullCalendar('getDate').isSame($scope.popup_datepicker.date, 'day')) {
                    $scope.popup_datepicker.date = $('.calendar').fullCalendar('getDate').toDate();
                }
            },
        }
    };

    /* add custom event*/
    $scope.addEvent = function() {
        $scope.events.push({
            title: 'New Event',
            start: new Date(y, m, d),
            className: ['b-l b-2x b-info']
        });
    };

    /* remove event */
    $scope.remove = function(index) {
        $scope.events.splice(index, 1);
    };

    /* Change View */
    $scope.changeView = function(view, calendar) {
        $('.calendar').fullCalendar('changeView', view);
    };



    $scope.today = function(calendar) {
        $('.calendar').fullCalendar('today');
    };


    $scope.change_datepicker = function() {
        $('.calendar').fullCalendar('gotoDate', $scope.popup_datepicker.date);
    };

    $scope.open_datepicker = function() {
        $scope.popup_datepicker.opened = true;
    };
    $scope.popup_datepicker = {
        date: moment().toDate(),
        opened: false,
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: moment().add(1, 'years').toDate(),
        minDate: null,
        startingDay: 1
    };

    /* event sources array*/
    $scope.eventSources = [$scope.events];




    $scope.duplicate_course_sessions = function(course_session_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalDuplicateCourseSessions.html',
            controller: 'ModalDuplicateCourseSessionsInstanceCtrl',
            resolve: {
                week_num: function() {
                    return $('.calendar').fullCalendar('getDate').isoWeek();
                },
                year: function() {
                    return $('.calendar').fullCalendar('getDate').format('YYYY');
                },
                room_id: function() {
                    return $scope.room.id;
                },
                club_id: function() {
                    return $scope.club.id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };

});








app.controller('ModalDuplicateCourseSessionsInstanceCtrl', function($scope, $uibModalInstance, $http, week_num, year, room_id, club_id, $location, Notification, $filter) {



    $scope.popup_datepicker = {
        date: moment().add(7, 'days').startOf('week').toDate(),
        opened: false,
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: moment().add(1, 'years').toDate(),
        minDate: null,
        startingDay: 1,
        dateDisabled: function(option) {
            return (option.mode === 'day' && (option.date.getDay() !== 1));
        }
    };
    $scope.confirm = function() {
        $scope.errors_form = null;
        $http({
            method: 'POST',
            data: { week_num: week_num, week_year: year, room_id: room_id, club_id: club_id, date: moment($scope.popup_datepicker.date).format('YYYY-MM-DD') },
            url: '/api/CourseSession/duplicate_planning'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('Planning.PlanningDuplicatedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            } else {
                $scope.errors_form = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.open_datepicker = function() {
        $scope.popup_datepicker.opened = true;
    };
    $scope.disabledDates = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 1));
    };

});