'use strict';
app.controller('PlanningManageController', function($scope, $http, $uibModal, $stateParams, $location, $rootScope, $translate, uiCalendarConfig, Notification , $filter) {

    $scope.club = null;
    $scope.room = null;
    $scope.course_sessions = [];
    $scope.events = [];
    $scope.eventsAllRoomsHome = {e0:[[]], e1:[[]], e2:[[]], e3:[[]], e4:[[]]};
    $scope.eventsAllRoomsHome0 = [];
    $scope.eventsAllRoomsHome1 = [];
    $scope.eventsAllRoomsHome2 = [];
    $scope.eventsAllRoomsHome3 = [];
    $scope.eventsAllRoomsHome4 = [];
    $scope.eventsAllRoomsHome5 = [];
    $scope.eventsAllRoomsHome6 = [];
    $scope.eventsAllRoomsHome7 = [];
    $scope.eventsAllRoomsHome8 = [];
    $scope.eventsAllRoomsHome9 = [];
    $scope.display_type_cours = true;
    $scope.cost_per_room = 0 ;
    $scope.cost_per_club = 0;
    $scope.url_print_pdf = null;
    $scope.url_print_all_pdf = null;
    $scope.is_manage_home = false;
    
    var permissions = sessionStorage.getItem("permissions");
    var pos_manage_home = permissions.indexOf("manage_home");
    var pos_manage_controller = permissions.indexOf("validate_canceled_courses");
    var pos_manage_admin = permissions.indexOf("manage_settings");

    $(document).ready(function() {
        
        $(function(){
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        
        var maxDate = year + '-' + month + '-' + day;

        if(pos_manage_controller == -1){
        $('#date_planing_home').attr('min', maxDate);
        }


       });
    
    });

    function disable_passed_date(){

        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        var date_selected = document.getElementById("date_planing_home").value;
        if(date_selected < maxDate){
            document.getElementById("date_planing_home").value = maxDate ;
        }
    
    }
    
    if(pos_manage_controller != -1){

        var ladate=new Date(sessionStorage.getItem("date_calendar"));
        var tab_jour=new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
        var jour_mois = ladate.getDate();
        var jour_lettre = tab_jour[ladate.getDay()];
        if(jour_lettre == "Dimanche" || jour_mois == 20){
            $scope.display_button_valid_hebdomadaire = true;
        }else{
            $scope.display_button_valid_hebdomadaire = false;
        }
    
    }

    

    if(pos_manage_home != -1){
        if(pos_manage_admin != -1){
            $scope.is_manage_home = false;
        }else{
            $scope.is_manage_home = true;
        }
        
        var date_calendar = sessionStorage.getItem("date_calendar");
        if(date_calendar == null){
            var to_day = new Date();
            var d = to_day.getDate();
            var m = to_day.getMonth() + 1; //Month from 0 to 11
            var y = to_day.getFullYear();
            var dd= '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
            sessionStorage.setItem("date_calendar",dd);
            document.getElementById("date_planing_home").value == dd ;
        }
    }
    else {
        $scope.is_manage_home = false;
    }

    console.log('$scope.is_manage_home:'+$scope.is_manage_home);
    
    $http({
        method: 'GET',
        url: '/api/Coach/getAll',
    }).then(function successCallback(response) {
        if (response.data.status) {
            $scope.ListCoachs = response.data.data;
            
        }
    }, function errorCallback(response) {
        console.log('error');
    });

    $scope.GetClubs = function() {
        $('.calendar').hide();
        $scope.Params = null;
        $http({
            method: 'GET',
            url: '/api/Club/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetClubs();

    $scope.GetRoomsRoleHome = function(){
        $scope.roomsRoleHome = false;
        var club = sessionStorage.getItem("club");
        $scope.Params = 'club_id='+club ;
        $http({
            method: 'GET',
            url: '/api/Room/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.length) {
                $scope.roomsRoleHome = response.data.data;
                var date_planing_home = sessionStorage.getItem("date_calendar");

                document.getElementById("date_planing_home").value = date_planing_home;
                $scope.roomsRoleHome.forEach(room_item => {
                    $scope.GetCourseSessionsAllRoomsHome(room_item, date_planing_home);
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    if(pos_manage_home !== -1){
        $scope.GetRoomsRoleHome(); 
    }

    $scope.select_calendar_date_by_home = function() {
        var date_calendar = document.getElementById("date_planing_home").value ;
        sessionStorage.setItem("date_calendar",date_calendar);
        if(pos_manage_controller != -1){
             var ladate=new Date(sessionStorage.getItem("date_calendar"));
             var tab_jour=new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
             var jour_mois = ladate.getDate();
             var jour_lettre = tab_jour[ladate.getDay()];
             if(jour_lettre == "Dimanche" || jour_mois == 20){
                 $scope.display_button_valid_hebdomadaire = true;
             }else{
                 $scope.display_button_valid_hebdomadaire = false;
             }
        }
        if(pos_manage_home != -1){
            $scope.GetRoomsRoleHome(); 
        }
   };

   $scope.valid_hebdomadaire = function() {
    var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
    var minDate = moment().startOf('week').toDate(); 
    var maxDate =moment().endOf('week').toDate();
    var month = minDate.getMonth() + 1;
    var month_max = maxDate.getMonth() + 1;
    var club = sessionStorage.getItem("club");
    
        var day_min = minDate.getDate();
        var day_max = maxDate.getDate();
        var year = minDate.getFullYear();
        if(month < 10)
            month = '0' + month.toString();

        if(month_max < 10)
           month_max = '0' + month_max.toString();
        
        if(day_min < 10)
            day_min = '0' + day_min.toString();
            
        if(day_max < 10)
           day_max = '0' + day_max.toString();
     
        minDate = day_min + '-' + month + '-' + year;
        maxDate = day_max + '-' + month_max + '-' + year;
        $scope.Params = 'week_num=' + week_num + ',minDate=' + minDate + ',maxDate=' + maxDate+',club=' + club;
        $http({
            method: 'get',
            url: '/api/CourseSessionFin/valid_hebdomadaire/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {

                Notification.success({ message: $filter('translate')('Global.valid_hebdomadaire_succees'), delay: 5000, positionX: 'right' });
                Notification.success({ message: $filter('translate')('Global.email_send_grh'), delay: 5000, positionX: 'right' });
                
            }
        }, function errorCallback(response) {
            console.log('error');
        });
   };

    $scope.GetRooms = function() {
        $scope.rooms = false;
        if (!$scope.club) {
            return;
        }
        $scope.Params = 'club_id=' + $scope.club.id;
        $http({
            method: 'GET',
            url: '/api/Room/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.length) {
                $scope.rooms = response.data.data;
            
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.format_date = function(date_, format_) {
        return moment(date_).format(format_);
    };
    
    $scope.yearWeek = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('Y'));
        // return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 10) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
    };

    $scope.weekForDate = function(date) {
        if (!date) { date = moment(); 
        }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('W'));
    };

    $scope.GetCourseSessions = function() {
        if(pos_manage_home != -1){
           $scope.display_buttton = false;
        }
        else{
            $scope.display_buttton = true;
        }

        

        if(pos_manage_home != -1){
            
            var dt_e = $('.calendar').fullCalendar('getDate');
            dt_e = new Date(dt_e._d);
            var dt_j = new Date();
            var date_j = dt_j.getFullYear() + '-' + ('0' + (dt_j.getMonth() + 1)).slice(-2) + '-' + ('0' + dt_j.getDate()).slice(-2); 
            var date_j_e = dt_e.getFullYear() + '-' + ('0' + (dt_e.getMonth() + 1)).slice(-2) + '-' + ('0' + dt_e.getDate()).slice(-2); 
            if(date_j_e <= date_j){

                      const iconRight = document.querySelector('.fc-icon-left-single-arrow');
                      iconRight.classList.add('hidden');    
            
            }else{
                    const iconRight = document.querySelector('.fc-icon-left-single-arrow');
                    iconRight.classList.add('block');   
                    iconRight.classList.remove('hidden');   
            }
        
        }

        $scope.course_sessions = [];
        $scope.cost_per_room = 0;
        $scope.cost_per_club = 0;
        $scope.have_course_sessions_invalide = false;
        $scope.empty_course_sessions = false;
        $scope.url_print_pdf = null;
        $scope.url_print_all_pdf = null;
        $('.calendar').fullCalendar('removeEvents');
        $('.calendar').addClass('loading');
        if (!$scope.club || !$scope.room) {
            return;
        }

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        
                $scope.Params = 'join_obj=all,actives=I!Y,club_id=' + $scope.club.id + ',room_id=' + $scope.room.id + ',week_num=' + week_num + ',week_year=' + $scope.yearWeek($('.calendar').fullCalendar('getDate'));
                $scope.url_print_pdf = window.config.base_url + '/planning/' + $scope.club.id + '/' + $scope.room.id + '/' + $scope.yearWeek($('.calendar').fullCalendar('getDate')) + '/' + week_num + '/print_pdf';
                $scope.url_print_all_pdf = window.config.base_url + '/planning/' + $scope.club.id + '/all_rooms/' + $scope.yearWeek($('.calendar').fullCalendar('getDate')) + '/' + week_num + '/print_pdf';
                
                $http({
                    method: 'GET',
                    url: '/api/CourseSession/get/' + $scope.Params
                }).then(function successCallback(response) {

                    $('.calendar').fullCalendar('removeEvents');
                    $('.calendar').removeClass('loading');

                    if (response.data.status) {
                        $scope.course_sessions = response.data.data;
                        if(!$scope.course_sessions.length){
                        $scope.empty_course_sessions = true;
                        }

                        angular.forEach($scope.course_sessions, function(obj, key) {
                            if(typeof(obj.coach)==='undefined' || !obj.coach){
                                obj.coach = {};
                            }
                            
                            
                            

                            if (obj.active === 'I') { $scope.have_course_sessions_invalide = true; }
                            var icon_invalid = (obj.active === 'Y') ? '' : '<i class="fa fa-info-circle alert-event text-danger" aria-hidden="true"></i>';
                            var icon_remplacant = (obj.secondary_coach_id === obj.coach_id) ? '<i class="fa fa-retweet alert-coach_id_secondary text-danger" aria-hidden="true"></i>' : '';
                            var icon_notif_number_participants = (obj.notif_time_set_nbr_participants === 'notif_active') ? '<i class="fa fa-bell alert-event text-warning" style="background:none;border:none;font-size:11px;" aria-hidden="true"></i>' : '';
                            var icon_set_nbr_participants = (obj.set_nbr_participants  >= 0) ? '<span class="alert-event" style="border:none;color:#fff;background:none;padding-top:7px;padding-right:40px;font-size:11px;">'+obj.set_nbr_participants+'</span>' : '';
                            var icon_check_validate_remplissage = (obj.check_validate_remplissage == 'true') ? '<span class="fa fa-check alert-event" style="background:none;border:none;padding-top:7px;padding-right:20px;color:#fff;font-size:11px;" aria-hidden="true"></span>' : '';
                            var icon_review = (obj.review != '') ? '<span class="fa fa-comments alert-event" style="background:none;border:none;padding-top:7px;padding-right:60px;color:#fff;font-size:11px;" aria-hidden="true"></span>' : '';
                            var icon_replace_coach = (obj.replace_coach == true) ? '<span class="fa fa-exchange alert-event" style="background:none;color:red;font-size:11px;" aria-hidden="true"></span>' : '';
                         
                            
                            
                            if(obj.color_cancel_cours){
                                var color = obj.color_cancel_cours;
                                var course_name = obj.course.course_name+'<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>'; 
                                if(obj.type_course == 'Autre'){
                                    var description_course = obj.description_course;
                                    
                                    if(description_course.length < 20){
                                        var course_name = description_course.substring(0,20);
                                        course_name = course_name+'<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>' ;
                                        
                                    }else{
                                        var course_name = description_course.substring(0,20);
                                        course_name = course_name+'<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>' ;
                                    }
                                }
        
                            }else{

                                if(obj.type_course == 'Cours'){
                                    var color = obj.course.color;  
                                    var course_name = obj.course.course_name; 
                                }else{
                                    var color = '#00ff00';
                                    var description_course = obj.description_course;
                                    if(description_course.length < 20){
                                        var course_name = description_course.substring(0,20);
                                    }else{
                                        var course_name = description_course.substring(0,20);
                                        course_name = course_name+'...' ;
                                    }
                                
                                }
                            }
                            
                            var list_coachs = '';
                            if(obj.primary_coach_name != null){
                               list_coachs = list_coachs +'1/'+ obj.primary_coach_name ;
                            }

                            if(obj.secondary_coach_name != null){
                               list_coachs =  list_coachs +'2/'+ obj.secondary_coach_name ;
                            }

                            if(obj.triple_coach_name != null){
                               list_coachs =  list_coachs +'3/'+ obj.triple_coach_name ;
                            }


                            if(obj.quad_coach_name != null){
                              
                                list_coachs =  list_coachs +'4/'+ obj.quad_coach_name ;

                            }
                            if(obj.five_coach_name != null){
                              
                                list_coachs = list_coachs +'5/'+ obj.five_coach_name ;

                            }

                            $scope.events.push({
                                id: obj.id,
                                data: obj,
                                title: icon_review + icon_set_nbr_participants
                                + icon_replace_coach
                                + icon_check_validate_remplissage
                                + icon_notif_number_participants
                                + icon_invalid
                                + icon_remplacant + course_name + ' - ' + list_coachs ,
                                start: moment(obj.course_session_date + ' ' + obj.course_start_time),
                                end: moment(obj.course_session_date + ' ' + obj.course_end_time),
                                color: color,
                            });
                        
                        
                        });

                        if (response.data.attributes.cost_per_club) {
                            $scope.cost_per_club = response.data.attributes.cost_per_club;
                        }
                        if (response.data.attributes.cost_per_room) {
                            $scope.cost_per_room = response.data.attributes.cost_per_room;
                        }
                    }
                }, function errorCallback(response) {
                    console.log('error');
                });
    };

    
    $scope.GetCourseSessions = function() {
       
        if(pos_manage_home != -1) {
            $scope.display_buttton = false;
        } else {
             $scope.display_buttton = true;
        }

        if(pos_manage_home != -1){
            
            var dt_e = $('.calendar').fullCalendar('getDate');
            dt_e = new Date(dt_e._d);
            var dt_j = new Date();
            var date_j = dt_j.getFullYear() + '-' + ('0' + (dt_j.getMonth() + 1)).slice(-2) + '-' + ('0' + dt_j.getDate()).slice(-2); 
            var date_j_e = dt_e.getFullYear() + '-' + ('0' + (dt_e.getMonth() + 1)).slice(-2) + '-' + ('0' + dt_e.getDate()).slice(-2); 
            if(date_j_e <= date_j){
                const iconRight = document.querySelector('.fc-icon-left-single-arrow');
                    iconRight.classList.add('hidden');    
            }else{
                const iconRight = document.querySelector('.fc-icon-left-single-arrow');
                    iconRight.classList.add('block');   
                    iconRight.classList.remove('hidden');   
            }
        }

        $scope.course_sessions = [];
        $scope.cost_per_room = 0;
        $scope.cost_per_club = 0;
        $scope.have_course_sessions_invalide = false;
        $scope.empty_course_sessions = false;
        $scope.url_print_pdf = null;
        $scope.url_print_all_pdf = null;
        $('.calendar').fullCalendar('removeEvents');
        $('.calendar').addClass('loading');
        if (!$scope.club || !$scope.room) {
            return;
        }

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
    
        $scope.Params = 'join_obj=all,actives=I!Y,club_id=' + $scope.club.id + ',room_id=' + $scope.room.id + ',week_num=' + week_num + ',week_year=' + $scope.yearWeek($('.calendar').fullCalendar('getDate'));
        
        $scope.url_print_pdf = window.config.base_url + '/planning/' + $scope.club.id + '/' + $scope.room.id + '/' + $scope.yearWeek($('.calendar').fullCalendar('getDate')) + '/' + week_num + '/print_pdf';
        $scope.url_print_all_pdf = window.config.base_url + '/planning/' + $scope.club.id + '/all_rooms/' + $scope.yearWeek($('.calendar').fullCalendar('getDate')) + '/' + week_num + '/print_pdf';
        
        $http({
            method: 'GET',
            url: '/api/CourseSession/get/' + $scope.Params
        }).then(function successCallback(response) {
            $('.calendar').fullCalendar('removeEvents');
            $('.calendar').removeClass('loading');

            if (response.data.status) {
                $scope.course_sessions = response.data.data;

                if(!$scope.course_sessions.length){
                    $scope.empty_course_sessions = true;
                }
                

              

                angular.forEach($scope.course_sessions, function(obj, key) {
                    if(typeof(obj.coach)==='undefined' || !obj.coach){
                        obj.coach = {};
                    }

                    
                    
                    //console.log(obj.check_validate_remplissage);

                    if (obj.active === 'I') { $scope.have_course_sessions_invalide = true; }
                    var icon_invalid = (obj.active === 'Y') ? '' : '<i class="fa fa-info-circle alert-event text-danger" aria-hidden="true"></i>';
                    var icon_remplacant = (obj.secondary_coach_id === obj.coach_id) ? '<i class="fa fa-retweet alert-coach_id_secondary text-danger" aria-hidden="true"></i>' : '';
                    var icon_notif_number_participants = (obj.notif_time_set_nbr_participants === 'notif_active') ? '<i class="fa fa-bell alert-event text-warning" style="background:none;border:none;font-size:11px;" aria-hidden="true"></i>' : '';
                    var icon_set_nbr_participants = (obj.set_nbr_participants  > 0) ? '<span class="alert-event" style="border:none;color:#fff;background:none;padding-top:7px;padding-right:40px;font-size:11px;">'+obj.set_nbr_participants+'</span>' : '';
                    var icon_check_validate_remplissage = (obj.check_validate_remplissage == 'true') ? '<span class="fa fa-check alert-event" style="background:none;border:none;padding-top:7px;padding-right:20px;color:#fff;font-size:11px;" aria-hidden="true"></span>' : '';
                    var icon_review = (obj.review != '') ? '<span class="fa fa-comments alert-event" style="background:none;border:none;padding-top:7px;padding-right:60px;color:#fff;font-size:11px;" aria-hidden="true"></span>' : '';
                    var icon_replace_coach = (obj.replace_coach == true) ? '<span class="fa fa-exchange alert-event" style="background:none;color:red;font-size:11px;" aria-hidden="true"></span>' : '';
                         
                            if(obj.color_cancel_cours){

                                var color = obj.color_cancel_cours;
                                var course_name = obj.course.course_name+'<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>'; 
                                if(obj.type_course == 'Autre'){

                                    var description_course = obj.description_course;
                                    
                                    if(description_course.length < 20){
                                        var course_name = description_course.substring(0,20);
                                        course_name = course_name+'<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>' ;
                                    }else{
                                        var course_name = description_course.substring(0,20);
                                        course_name = course_name+'...<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>' ;
                                    }
                                
                                }
        
                            }else{

                                if(obj.type_course == 'Cours'){
                                    
                                    if(obj.course.color != null){
                                        var color = obj.course.color; 
                                    }else{
                                        var color = "#00ff00"; 
                                    }
                                     
                                    var course_name = obj.course.course_name; 
                                }else{
                                    var color = '#00ff00';
                                    var description_course = obj.description_course;
                                    
                                    if(description_course.length < 20){
                                        var course_name = description_course.substring(0,20);
                                    }else{
                                        var course_name = description_course.substring(0,20);
                                        course_name = course_name+'...' ;
                                    }
                                }
                            }

                            var list_coachs = '';
                            if(obj.primary_coach_name != null){
                              
                                list_coachs = list_coachs +'1/'+ obj.primary_coach_name ;

                            }
                            if(obj.secondary_coach_name != null){
                              
                                list_coachs =  list_coachs +'2/'+ obj.secondary_coach_name ;

                            }
                            if(obj.triple_coach_name != null){
                              
                                list_coachs =  list_coachs +'3/'+ obj.triple_coach_name ;

                            }
                            if(obj.quad_coach_name != null){
                              
                                list_coachs =  list_coachs +'4/'+ obj.quad_coach_name ;

                            }
                            if(obj.five_coach_name != null){
                              
                                list_coachs = list_coachs +'5/'+ obj.five_coach_name ;

                            }

                            $scope.events.push({
                                id: obj.id,
                                data: obj,
                                title: icon_review + icon_set_nbr_participants
                                + icon_replace_coach
                                + icon_check_validate_remplissage
                                + icon_notif_number_participants
                                + icon_invalid
                                + icon_remplacant + course_name + ' - ' + list_coachs ,
                                start: moment(obj.course_session_date + ' ' + obj.course_start_time),
                                end: moment(obj.course_session_date + ' ' + obj.course_end_time),
                                color: color,
                            });
                        
                });

                if (response.data.attributes.cost_per_club) {
                    $scope.cost_per_club = response.data.attributes.cost_per_club;
                }
                if (response.data.attributes.cost_per_room) {
                    $scope.cost_per_room = response.data.attributes.cost_per_room;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };
   

    $scope.RoomItemSetCalendarConfig = function(room_item, date_planing_home) {
        var room_id = room_item.id;
        room_item.calendarConfig = {
            room_id: room_id,
            date_planing_home: date_planing_home,
            minTime : '07:00' ,
            defaultView: 'agendaDay',
            defaultDate : sessionStorage.getItem("date_calendar"), 
            allDaySlot: false,
            firstDay: 1,
            timezone: 'local',
            lang: $rootScope.app.settings.selectLang,
            height: 1150,
            editable: false,
            eventDurationEditable: false,
            eventStartEditable: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            eventClick: function(event, jsEvent, view) {
                $scope.alertOnEventClick(event, jsEvent, view, room_item, date_planing_home);
            },
            dayClick: $scope.alertOnDayClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventAfterAllRender: function(view) {},
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
            }
        };
    }
    

    $scope.GetCourseSessionsAllRoomsHome = function(room_item, date_planing_home) {
        
        var room_id = room_item.id;
        
        if (!room_item.calendarConfig){
        $scope.RoomItemSetCalendarConfig(room_item, date_planing_home);
        }
        room_item.events = [];

        $scope.course_sessions_all_rooms_home = [];
        $scope.cost_per_room_all_rooms_home = 0;
        $scope.cost_per_club_all_rooms_home = 0;
        $scope.have_course_sessions_invalide_all_rooms_home = false;
        $scope.empty_course_sessions_all_rooms_home = false;

        var club_id = sessionStorage.getItem("club");
        var week_num = this.weekForDate($('.calendar' + room_id).fullCalendar('getDate'));
        $scope.Params = 'join_obj=all,actives=I!Y,club_id=' + club_id + ',date_planing_home=' + date_planing_home + ',room_id=' + room_id + ',week_num=' + week_num + ',week_year=' + $scope.yearWeek($('.calendar' + room_id).fullCalendar('getDate'));
        
        console.log($scope.Params);
        $http({
            method: 'GET',
            url: '/api/CourseSession/getCoursesAllRoomsHome/' + $scope.Params
        }).then(function successCallback(response) {

            // $('.calendar' + room_id).fullCalendar('removeEvents');
            $('.calendar' + room_id).removeClass('loading');
            if (response.data.status) {
                $scope.course_sessions_all_rooms_home = response.data.data;
                if(!$scope.course_sessions_all_rooms_home.length){
                    $scope.empty_course_sessions_all_rooms_home = true;
                }

                $scope.course_sessions_all_rooms_home.forEach(item_session => {
                    
                    if(typeof(item_session.coach)==='undefined' || !item_session.coach){
                        item_session.coach = {};
                    }
      
                    if (item_session.active === 'I') { $scope.have_course_sessions_invalide_all_rooms_home = true; }
                    
                    var icon_invalid = (item_session.active === 'Y') ? '' : '<i class="fa fa-info-circle alert-event text-danger" aria-hidden="true"></i>';
                    var icon_remplacant = (item_session.secondary_coach_id === item_session.coach_id) ? '<i class="fa fa-retweet alert-coach_id_secondary text-danger" aria-hidden="true"></i>' : '';
                    var icon_notif_number_participants = (item_session.notif_time_set_nbr_participants === 'notif_active') ? '<i class="fa fa-bell alert-event text-warning" style="background:none;border:none;font-size:11px;" aria-hidden="true"></i>' : '';
                    var icon_set_nbr_participants = (item_session.set_nbr_participants  >= 0) ? '<span class="alert-event" style="border:none;color:#fff;background:none;padding-top:7px;padding-right:40px;font-size:11px;">'+item_session.set_nbr_participants+'</span>' : '';
                    var icon_check_validate_remplissage = (item_session.check_validate_remplissage == 'true') ? '<span class="fa fa-check alert-event" style="background:none;border:none;padding-top:7px;padding-right:20px;color:#fff;font-size:11px;" aria-hidden="true"></span>' : '';
                    var icon_review = (item_session.review != '') ? '<span class="fa fa-comments alert-event" style="background:none;border:none;padding-top:7px;padding-right:60px;color:#fff;font-size:11px;" aria-hidden="true"></span>' : '';
                    var icon_replace_coach = (item_session.replace_coach == true) ? '<span class="fa fa-exchange alert-event" style="background:none;color:red;font-size:11px;" aria-hidden="true"></span>' : '';
                    
                    if(item_session.color_cancel_cours){

                        var color = item_session.color_cancel_cours;
                        var course_name = item_session.course.course_name+'<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>'; ; 
                        if(item_session.type_course == 'Autre'){

                            var description_course = item_session.description_course;
                            
                            if(description_course.length < 20){
                                var course_name = description_course.substring(0,20);
                                course_name = course_name+'<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>' ;
                            }else{
                                var course_name = description_course.substring(0,20);
                                course_name = course_name+'...<i class="fa fa-times alert-event text-danger" aria-hidden="true"></i>' ;
                            }
                        
                        }

                    }else{

                        if(item_session.type_course == 'Cours'){
                            var color = item_session.course.color;  
                            var course_name = item_session.course.course_name; 
                        }else{
                            var color = '#00ff00';
                            var description_course = item_session.description_course;
                            
                            if(description_course.length < 20){
                                var course_name = description_course.substring(0,20);
                            }else{
                                var course_name = description_course.substring(0,20);
                                course_name = course_name+'...' ;
                            }
                            
                        }
                    }
                            var list_coachs = '';
                            if(item_session.primary_coach_name != null){
                              
                                list_coachs = list_coachs +'1/'+ item_session.primary_coach_name ;

                            }
                            if(item_session.secondary_coach_name != null){
                              
                                list_coachs =  list_coachs +'2/'+ item_session.secondary_coach_name ;

                            }
                            if(item_session.triple_coach_name != null){
                              
                                list_coachs =  list_coachs +'3/'+ item_session.triple_coach_name ;

                            }
                            if(item_session.quad_coach_name != null){
                              
                                list_coachs =  list_coachs +'4/'+ item_session.quad_coach_name ;

                            }
                            if(item_session.five_coach_name != null){
                              
                                list_coachs = list_coachs +'5/'+ item_session.five_coach_name ;

                            }


                        let myData = [{

                        id: item_session.id,
                        data: item_session,
                        title: icon_review + icon_set_nbr_participants
                        + icon_replace_coach
                        + icon_check_validate_remplissage
                        + icon_notif_number_participants
                        + icon_invalid
                        + icon_remplacant + course_name + ' - ' + list_coachs ,
                        start: moment(item_session.course_session_date + ' ' + item_session.course_start_time).toDate(),
                        end: moment(item_session.course_session_date + ' ' + item_session.course_end_time).toDate(),
                        color: color,
                       
                        }];


                    room_item.events.push(myData);
                
                });

                  //console.log('room_item.events', room_item.events);
                $('.calendar' + room_id).fullCalendar('refetchEvents');
                
                if (response.data.attributes.cost_per_club) {
                    $scope.cost_per_club_all_rooms_home = response.data.attributes.cost_per_club;
                }
                if (response.data.attributes.cost_per_room) {
                    $scope.cost_per_room_all_rooms_home = response.data.attributes.cost_per_room;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCourseSessionsAllRoomsHomeCallApi = function() {

    }

   
    $scope.clean_hours = function(elements, deleteValue) {
        for (var i = 0; i < elements.length; i++) {
            if (elements[i] == deleteValue) {
                elements.splice(i, 1);
                i--;
            }
        }
        return elements;
    };

    $scope.select_club = function(club) {
        $scope.club = club;
        $scope.room = null;
        $scope.GetRooms();
        var opening_hours = [$scope.club.opening_hour_1, $scope.club.opening_hour_2, $scope.club.opening_hour_3, $scope.club.opening_hour_4, $scope.club.opening_hour_5, $scope.club.opening_hour_6, $scope.club.opening_hour_7];
        opening_hours = $scope.clean_hours(opening_hours, undefined);
        var closing_hours = [$scope.club.closing_hour_1, $scope.club.closing_hour_2, $scope.club.closing_hour_3, $scope.club.closing_hour_4, $scope.club.closing_hour_5, $scope.club.closing_hour_6, $scope.club.closing_hour_7];
        closing_hours = $scope.clean_hours(closing_hours, undefined);
        $scope.uiConfig.calendar.minTime = $filter('orderBy')(opening_hours)[0];
        if( parseInt(moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').format('HH')) == 23 ){
            $scope.uiConfig.calendar.maxTime = '23:59';
        }else{
         $scope.uiConfig.calendar.maxTime = moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').add(1, 'hours').format('HH:mm');   
        }
        //if($scope.uiConfig.calendar.maxTime) $scope.uiConfig.calendar.maxTime = moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').add(1, 'hours').format('HH:mm');
        //$scope.uiConfig.calendar.minTime = '00:00';
        //$scope.uiConfig.calendar.maxTime = '23:59';*/
        $('.calendar').hide();
    };

    $scope.select_room = function(room) {
        $('.calendar').show();

        if (pos_manage_home != -1){
           $('.calendar').fullCalendar('gotoDate', moment().add(0, 'weeks').toDate());
         }else{
            $('.calendar').fullCalendar('gotoDate', moment().add(1, 'weeks').toDate());
         }

        $scope.room = room;
        $('.calendar').fullCalendar('refetchEvents');
    };

    $scope.select_coach = function(coach){
        $('.calendar').show();
        $('.calendar').fullCalendar('gotoDate', moment().add(1, 'weeks').toDate());
        $scope.coach = coach;
        $('.calendar').fullCalendar('refetchEvents'); 
    };



    /* alert on dayClick */
    $scope.precision = 400;
    $scope.lastClickTime = 0;
    $scope.alertOnDayClick = function(date_event, jsEvent, view) {
        var time = new Date().getTime();
        if (time - $scope.lastClickTime <= $scope.precision) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ModalAddCourseSession.html',
                controller: 'ModalAddCourseSessionInstanceCtrl',
                resolve: {
                    club_id: function() {
                        return $scope.club.id;
                    },
                    room_id: function() {
                        return $scope.room.id;
                    },
                    date_event: function() {
                        return date_event;
                    }
                }
            });
            modalInstance.result.then(function(data) {
                $('.calendar').fullCalendar('refetchEvents');
            }, function() {

            });

        }
        $scope.lastClickTime = time;
    };
    /* alert on Drop */
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = event;
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = event;
    };

    $scope.overlay = $('.fc-overlay');
    $scope.alertOnMouseOver = function(event, jsEvent, view) {
        $scope.event = event;
        $scope.overlay.removeClass('left right top').find('.arrow').removeClass('left right top pull-up');
        var wrap = $(jsEvent.target).closest('.fc-event');
        var cal = wrap.closest('.calendar');
        var left = wrap.offset().left - cal.offset().left;
        var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
        var top = cal.height() - (wrap.offset().top - cal.offset().top + wrap.height());
        if (right > $scope.overlay.width()) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up');
        } else if (left > $scope.overlay.width()) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
        } else {
            $scope.overlay.find('.arrow').addClass('top');
        }
        if (top < $scope.overlay.height()) {
            $scope.overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass('pull-down');
        }
        (wrap.find('.fc-overlay').length === 0) && wrap.append($scope.overlay);
    };



    $scope.alertOnEventClick = function(event, jsEvent, view, room_item, date_planing_home) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddCourseSession.html',
            controller: 'ModalEditCourseSessionInstanceCtrl',
            resolve: {
                course_session: function() {
                    return event.data;
                }
            }
        });
        modalInstance.result.then(function(data) {
            // $('.calendar').fullCalendar('refetchEvents');
            if(pos_manage_home !== -1){
                $scope.GetRoomsRoleHome(); 
            }
        }, function() {

        });
    };



    $scope.delete_course_session = function(course_session_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseSessionInstanceCtrl',
            resolve: {
                course_session_id: function() {
                    return course_session_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };



    $scope.validate_course_sessions = function(course_session_id) {

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        var year_num = this.yearWeek($('.calendar').fullCalendar('getDate'));

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalValidateCourseSessions.html',
            controller: 'ModalValidateCourseSessionsInstanceCtrl',
            resolve: {
                week_num: function() {
                    return week_num;
                },
                year: function() {
                    return year_num;
                },
                room_id: function() {
                    return $scope.room.id;
                },
                club_id: function() {
                    return $scope.club.id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };

    
    if (pos_manage_home != -1){
        
        $scope.uiConfig = {
        calendar: {
            defaultView: 'agendaDay',
            minTime : '07:00',
            defaultDate : sessionStorage.getItem("date_calendar"), 
            allDaySlot: false,
            firstDay: 1,
            timezone: 'local',
            lang: $rootScope.app.settings.selectLang,
            height: 1150,
            editable: false,
            eventDurationEditable: false,
            eventStartEditable: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            eventClick: $scope.alertOnEventClick,
            dayClick: $scope.alertOnDayClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            events: function(start, end, timezone, callback) {
                $scope.GetCourseSessions();
            },
            eventAfterAllRender: function(view) {},
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
            }
            
        }
    };
    
    }else{

    /* config object */
    
    $scope.uiConfig = {
        calendar: {
            defaultView: 'agendaWeek',
            allDaySlot: false,
            firstDay: 1,
            timezone: 'local',
            lang: $rootScope.app.settings.selectLang,
            height: 650,
            editable: false,
            eventDurationEditable: false,
            eventStartEditable: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            eventClick: $scope.alertOnEventClick,
            dayClick: $scope.alertOnDayClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            //eventMouseover: $scope.alertOnMouseOver,
            events: function(start, end, timezone, callback) {
                $scope.GetCourseSessions();
            },
            eventAfterAllRender: function(view) {},
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
            }
        
        }
    };

    }

    /* add custom event*/
    $scope.addEvent = function() {
        $scope.events.push({
            title: 'New Event',
            start: new Date(y, m, d),
            className: ['b-l b-2x b-info']
        });
    };

    /* remove event */
    $scope.remove = function(index) {
        $scope.events.splice(index, 1);
    };

    /* Change View */
    $scope.changeView = function(view, calendar) {
        $('.calendar').fullCalendar('changeView', view);
    };

    $scope.today = function(calendar) {
        $('.calendar').fullCalendar('today');
    };

    /* event sources array*/
    $scope.eventSources = [$scope.events];
    $scope.eventSourcesAllRoomsHome = $scope.eventsAllRoomsHome;
    //$scope.eventSourcesAllRoomsHome[0] = [$scope.eventsAllRoomsHome0];

    $scope.eventSourcesAllRoomsHome0 = [$scope.eventsAllRoomsHome0];
    $scope.eventSourcesAllRoomsHome1 = [$scope.eventsAllRoomsHome1];
    $scope.eventSourcesAllRoomsHome2 = [$scope.eventsAllRoomsHome2];
    $scope.eventSourcesAllRoomsHome3 = [$scope.eventsAllRoomsHome3];
    $scope.eventSourcesAllRoomsHome4 = [$scope.eventsAllRoomsHome4];
    $scope.eventSourcesAllRoomsHome5 = [$scope.eventsAllRoomsHome5];
    $scope.eventSourcesAllRoomsHome6 = [$scope.eventsAllRoomsHome6];
    $scope.eventSourcesAllRoomsHome7 = [$scope.eventsAllRoomsHome7];
    $scope.eventSourcesAllRoomsHome8 = [$scope.eventsAllRoomsHome8];
    $scope.eventSourcesAllRoomsHome9 = [$scope.eventsAllRoomsHome9];
   $scope.duplicate_course_sessions = function(course_session_id) {
        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        var year_num = this.yearWeek($('.calendar').fullCalendar('getDate'));
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalDuplicateCourseSessions.html',
            controller: 'ModalDuplicateCourseSessionsInstanceCtrl',
            resolve: {
                week_num: function() {
                    return week_num;
                },
                year: function() {
                    return year_num;
                },
                room_id: function() {
                    return $scope.room.id;
                },
                club_id: function() {
                    return $scope.club.id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };




    $scope.send_by_email = function(course_session_id) {

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        var year_num = this.yearWeek($('.calendar').fullCalendar('getDate'));

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalSendByEmail.html',
            controller: 'ModalSendByEmailInstanceCtrl',
            resolve: {
                week_num: function() {
                    return week_num;
                },
                year: function() {
                    return year_num;
                },
                room_id: function() {
                    return $scope.room.id;
                },
                club_id: function() {
                    return $scope.club.id;
                },
                empty_course_sessions: function() {
                    return $scope.empty_course_sessions;
                },
                have_course_sessions_invalide: function() {
                    return $scope.have_course_sessions_invalide;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };

    /*debut modif calendar 03/01/2019*/

    $scope.iframeLoaded = function(){   ;
        console.log($scope)
        $scope.showloader = false;     
    } 
   
    $scope.Table = createTable();
    $scope.class = "nothing";
    $scope.changeClass = function(){
        console.log('test');
    };
    
    function createTable() {
        var Table = {};
        Table.Lines = [];
         
        for (var i = 0; i < 4; i++) {
            var Line = {};
            Line.Cases = [];
             
            for (var j = 0; j < 10; j++) {
                var Points = {};
                Line.Cases.push(Points);
            }
            Table.Lines.push(Line);
        }
        return Table;
    }
    /*fin modif calendar 03/01/2019*/



});





app.controller('ModalAddCourseSessionInstanceCtrl', function($scope, $uibModalInstance, $http, club_id, room_id, date_event, $location, Notification) {

    $scope.is_new = true;
    $scope.is_manage_old_days = true;
    $scope.is_cours = true ;
    $scope.is_others = false ;
    $scope.display_type_cours = true ;
    /* begin multiple select */
    
    $http({
        method: 'GET',
        url: '/api/Coach/getAll',
    }).then(function successCallback(response) {
        if (response.data.status) {
            $scope.coachsList = response.data.data;
            $scope.coachs_lst = [];


            angular.forEach($scope.coachsList, function(obj, key) {
                 $scope.coachs_lst.push({
                    id: obj.id,
                    name: obj.firstname+' '+obj.lastname
                 });
            });
        }
    }, function errorCallback(response) {
        console.log('error');
    });

    $scope.typecourse = function(){
        var type_course = $scope.course_session.type_course ;//document.getElementById('type_course').value ;
        if(type_course == 'Cours'){
            $scope.is_cours = true ;
            $scope.is_others = false ;
        }else{
            $scope.is_cours = false ;
            $scope.is_others = true ;

            $http({
                method: 'GET',
                url: '/api/Coach/getAll',
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $scope.coaches = response.data.data;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
           
      
    $scope.onSubmit = function() {
        
        if ($scope.multipleSelectForm.$invalid) {
          if ($scope.multipleSelectForm.$error.required != null) {
            $scope.multipleSelectForm.$error.required.forEach(function(element) {
              element.$setDirty();
            });
          }
          return null;
        }
        alert("Field is Valid");

    };
  
  // Init
      $scope.testVal = ''; // Test any any text 
  
      $scope.beforeSelectItem = function(item) {
        // perform operation on this item before selecting it.
        $scope.testVal = 'Passed - Before Select Item';
      };
  
      $scope.afterSelectItem = function(item) {
        // perform operation on this item after selecting it.
        $scope.testVal = 'Passed - After Select Item';
      };
  
      $scope.beforeRemoveItem = function(item) {
        // perform operation on this item before removing it.
        $scope.testVal = 'Passed - Before Remove Item';
      };
  
      $scope.afterRemoveItem = function(item) {
        // perform operation on this item after removing it.
        $scope.testVal = 'Passed - After Remove Item';
      };
    /* end multiple select  */
    
    $scope.date_format = 'dd-MM-yyyy';
    $scope.dateOptions = {
        //minDate: moment().add(7, 'days').startOf('week').toDate(),
        //maxDate: moment().add(7, 'days').endOf('week').toDate()
    };

    $scope.course_session = {};
    $scope.course_session.club_id = club_id;
    $scope.course_session.room_id = room_id;
    $scope.course_session.date = moment(date_event).toDate();
    $scope.course_session.time = moment(date_event);
    $scope.courses = null;
    var DateObj = new Date();
    var date_jour = DateObj.getFullYear() + '-' + ('0' + (DateObj.getMonth() + 1)).slice(-2) + '-' + ('0' + DateObj.getDate()).slice(-2); 
    var permissions = sessionStorage.getItem("permissions");
    var pos_manage_home = permissions.indexOf("manage_home");
    var pos_edit_old_day = permissions.indexOf("edit_old_day_planning ");
    var dd_event = date_event.toDate();
    var date_jour_event = dd_event.getFullYear() + '-' + ('0' + (dd_event.getMonth() + 1)).slice(-2) + '-' + ('0' + dd_event.getDate()).slice(-2); 
    if(pos_edit_old_day != -1){
        if(pos_manage_home != -1){
            $scope.is_manage_old_days = false;
            }else{
            $scope.is_manage_old_days = true;
        }
    }
    else{
        if(date_jour_event >= date_jour){
            if(pos_manage_home != -1){
            $scope.is_manage_old_days = false;
            }else{
            $scope.is_manage_old_days = true;
            }
        }else{
            $scope.is_manage_old_days = false;
        }
    }
    

    $scope.GetCourses = function() {
        
        $scope.Params = 'room_id=' + $scope.course_session.room_id;
     
        $http({
            method: 'GET',
            url: '/api/Course/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCourses();


    $scope.yearWeek = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('Y'));
        // return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 10) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
    };

    $scope.weekForDate = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('W'));
    };


    $scope.EmptyCoaches = function() {
        $scope.list_coachs = [];
    };
    $scope.GetCoaches = function() {

        $scope.errors_max_five_coachs = false ;
        $scope.need_force_validation = null;
        $scope.errors_form = null;
        $scope.course_session.coach_id = null;
        $scope.coaches = null;
        if(!$scope.course_session.date || !$scope.course_session.time){
            return;
        }
        $scope.Params = 'exclude_coach_id='+$scope.course_session.primary_coach_id+',course_session_id=' + $scope.course_session.id + ',course_id=' + $scope.course_session.course_id + ',club_id=' + $scope.course_session.club_id + ',date_disponibility=' + moment($scope.course_session.date).format('YYYY-MM-DD') + ',time_disponibility=' + moment($scope.course_session.time).format('HH:mm')+',selectConcept=0,inputName=';
        $http({
            method: 'GET',
            url: '/api/Coach/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.coaches = response.data.data;
                $scope.coachsList = response.data.data;
                $scope.coachs_lst = [];
                angular.forEach($scope.coachsList, function(obj, key) {
                    $scope.coachs_lst.push({
                        id: obj.id,
                        name: obj.firstname+' '+obj.lastname
                    });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.apply_force_validation = null;
    $scope.force_validation = function(){
        $scope.apply_force_validation = true;
        $scope.add();
    };

    $scope.add = function() {

        $scope.course_session.course_session_date = moment($scope.course_session.date).format('YYYY-MM-DD');
        var type_course = document.getElementById('type_course').value ;
        if(type_course == 'Cours'){
        $scope.course_session.course_start_time = moment($scope.course_session.time).format('HH:mm');
        }else{
        $scope.course_session.time_others_begin = moment($scope.course_session.time_others_begin).format('HH:mm');
        $scope.course_session.time_others_stop = moment($scope.course_session.time_others_stop).format('HH:mm');
        }
        
        var items_coachs    = ["primary_coach_id", "secondary_coach_id", "triple_coach_id" , "quad_coach_id" , "five_coach_id"];
        var number_coach = 0;


        $scope.list_coachs.forEach(function (coach_item, i) {
            number_coach = i ;
            if(i == 0){
                $scope.course_session.primary_coach_id = coach_item.id;
                $scope.course_session.primary_coach_name = coach_item.name;
            }

            if(i == 1){
                $scope.course_session.secondary_coach_id = coach_item.id;
                $scope.course_session.secondary_coach_name = coach_item.name;
            }

            if(i == 2){
                $scope.course_session.triple_coach_id = coach_item.id;
                $scope.course_session.triple_coach_name = coach_item.name;
            }

            if(i == 3){
                $scope.course_session.quad_coach_id = coach_item.id;
                $scope.course_session.quad_coach_name = coach_item.name;
            }
            if(i == 4){
                $scope.course_session.five_coach_id = coach_item.id;
                $scope.course_session.five_coach_name = coach_item.name;
            }
        });

        $scope.course_session.number_coach = number_coach + 1 ;
        
        if($scope.course_session.number_coach <= 5){
            $http({
                method: 'POST',
                data: { course_session: $scope.course_session, force_validation: $scope.apply_force_validation },
                url: '/api/CourseSession/add_course_session'
            }).then(function successCallback(response) {
                $scope.need_force_validation = null;
                $scope.apply_force_validation = null;
                if (response.data.status) {
                    $uibModalInstance.close();
                } else {
                    if(response.data.attributes && response.data.attributes.need_force_validation){
                        $scope.need_force_validation = true;
                    }
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }else{
          $scope.errors_max_five_coachs = true ;
        }

    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('ModalEditCourseSessionInstanceCtrl', function($scope, $uibModalInstance, $http, course_session, $location, Notification, $uibModal) {

    $scope.is_new = false;
    $scope.date_format = 'dd-MM-yyyy';
    $scope.is_cours = true ;
    $scope.is_others = false ;
    $scope.dateOptions = {
        //minDate: moment().add(7, 'days').startOf('week').toDate(),
        //maxDate: moment().add(7, 'days').endOf('week').toDate()
    };

    $scope.msg_min_one_coach = false ;


    
    $scope.list_coachs = [];
    if(course_session.primary_coach_id != null){
    $scope.list_coachs.push({
        id : course_session.primary_coach_id ,
        name : course_session.primary_coach_name
    });
    }
    
    if(course_session.secondary_coach_id != null){
    $scope.list_coachs.push({
        id : course_session.secondary_coach_id ,
        name : course_session.secondary_coach_name
    });
    }


    if(course_session.triple_coach_id != null){
    $scope.list_coachs.push({
        id : course_session.triple_coach_id ,
        name : course_session.triple_coach_name
    });
    }

    if(course_session.quad_coach_id != null){
    $scope.list_coachs.push({
        id : course_session.quad_coach_id ,
        name : course_session.quad_coach_name
    });
    }

    if(course_session.five_coach_id != null){
    $scope.list_coachs.push({
        id : course_session.five_coach_id ,
        name : course_session.five_coach_name
    });
    }
    
   

    $scope.display_type_cours = false ;
    
    /* begin multiple select */
    $http({
        method: 'GET',
        url: '/api/Coach/getAll',
    }).then(function successCallback(response) {


        if (response.data.status) {
            $scope.coachsList = response.data.data;
            $scope.coachs_lst = [];


            angular.forEach($scope.coachsList, function(obj, key) {
                 $scope.coachs_lst.push({
                    id: obj.id,
                    name: obj.firstname+' '+obj.lastname
                 });
            });

        }
    
    }, function errorCallback(response) {
        console.log('error');
    });

    
    $scope.onSubmit = function(){
        if ($scope.multipleSelectForm.$invalid) {
          if ($scope.multipleSelectForm.$error.required != null) {
            $scope.multipleSelectForm.$error.required.forEach(function(element) {
              element.$setDirty();
            });
          }
          return null;
        }
        alert("Field is Valid");
    };
  
      // Init
      $scope.testVal = ''; // Test any any text 
  
      $scope.beforeSelectItem = function(item) {
        // perform operation on this item before selecting it.
        $scope.testVal = 'Passed - Before Select Item';
      };
  
      $scope.afterSelectItem = function(item) {
        // perform operation on this item after selecting it.
        $scope.testVal = 'Passed - After Select Item';
      };
  
      $scope.beforeRemoveItem = function(item) {
        // perform operation on this item before removing it.
        $scope.testVal = 'Passed - Before Remove Item';
      };
  
      $scope.afterRemoveItem = function(item) {
        // perform operation on this item after removing it.
        $scope.testVal = 'Passed - After Remove Item';
      };
    
    /* end multiple select  */
    

    $scope.course_session = course_session;
    $scope.course_session.date = moment(course_session.course_session_date, 'YYYY-MM-DD').toDate();
    var DateObj = new Date();
    var dd = DateObj.getFullYear() + '-' + ('0' + (DateObj.getMonth() + 1)).slice(-2) + '-' + ('0' + DateObj.getDate()).slice(-2); 
    var permissions = sessionStorage.getItem("permissions");


    var pos_manage_controller = permissions.indexOf("validate_canceled_courses");
    if(pos_manage_controller != -1){
        $scope.is_manage_controller = true;
    }else{
        $scope.is_manage_controller = false;
    }

    var pos_edit_old_day = permissions.indexOf("edit_old_day_planning");
    if(pos_edit_old_day != -1){
        $scope.is_manage_old_days = true;
    }
    else{
        if(course_session.course_session_date >= dd)
        {
            $scope.is_manage_old_days = true;

        }else{
            $scope.is_manage_old_days = false;
        }
    }

    
    var pos_manage_home = permissions.indexOf("manage_home");
    if(pos_manage_home != -1){
    
       $scope.is_manage_home = true;
       $scope.is_manage_old_days = false;
    }
    else{
        $scope.is_manage_home = false;
    }
    

    
    $scope.course_session.time = moment(course_session.course_start_time, 'HH:mm');
    $scope.courses = null;

    $scope.GetCourses = function() {
        $scope.Params = 'room_id=' + $scope.course_session.room_id;
        console.log($scope.Params);
        $http({
            method: 'GET',
            url: '/api/Course/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
            }
            $scope.course_session.course_id = course_session.course_id;
            $scope.GetCoaches();
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCourses();

    $scope.EmptyCoaches = function() {
        $scope.list_coachs = [];
    };

    $scope.GetCoaches = function() {

        $scope.coaches = null;
        $scope.coaches_secondary = null;
        if(!$scope.course_session.date || !$scope.course_session.time){
            return;
        }
        $scope.Params = 'course_session_id=' + $scope.course_session.id + ',course_id=' + $scope.course_session.course_id + ',club_id=' + $scope.course_session.club_id + ',selectConcept=0,inputName=';
        $http({
            method: 'GET',
            url: '/api/Coach/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.coaches = response.data.data;
                $scope.coachsList = response.data.data;
                $scope.coachs_lst = [];
                angular.forEach($scope.coachsList, function(obj, key) {
                    $scope.coachs_lst.push({
                        id: obj.id,
                        name: obj.firstname+' '+obj.lastname
                    });
                });

                
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCoachesSecondary = function() {
        $scope.coaches_secondary = null;
        if(!$scope.course_session.primary_coach_id){
            return;
        }
        $scope.Params = 'exclude_coach_id='+$scope.course_session.primary_coach_id+',course_session_id=' + $scope.course_session.id + ',course_id=' + $scope.course_session.course_id + ',club_id=' + $scope.course_session.club_id + ',date_disponibility=' + moment($scope.course_session.date).format('YYYY-MM-DD') + ',time_disponibility=' + moment($scope.course_session.time).format('HH:mm');
        $http({
            method: 'GET',
            url: '/api/Coach/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.coaches_secondary = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.apply_force_validation = null;
    $scope.force_validation = function(){
        $scope.apply_force_validation = true;
        $scope.save();
    };

    $scope.save = function() {
        
        $scope.errors_max_five_coachs = false ;
        $scope.course_session.course_session_date = moment($scope.course_session.date).format('YYYY-MM-DD');
        $scope.course_session.course_start_time = moment($scope.course_session.time).format('HH:mm');
        
        $scope.course_session.primary_coach_id = ($scope.list_coachs && $scope.list_coachs[0]) ? $scope.list_coachs[0].id : null;
        $scope.course_session.primary_coach_name = ($scope.list_coachs && $scope.list_coachs[0]) ? $scope.list_coachs[0].name : null;
        
        $scope.course_session.secondary_coach_id = ($scope.list_coachs && $scope.list_coachs[1]) ? $scope.list_coachs[1].id : null;
        $scope.course_session.secondary_coach_name = ($scope.list_coachs && $scope.list_coachs[1]) ? $scope.list_coachs[1].name : null;
        
        $scope.course_session.triple_coach_id = ($scope.list_coachs && $scope.list_coachs[2]) ? $scope.list_coachs[2].id : null;
        $scope.course_session.triple_coach_name = ($scope.list_coachs && $scope.list_coachs[2]) ? $scope.list_coachs[2].name : null;
        
        $scope.course_session.quad_coach_id = ($scope.list_coachs && $scope.list_coachs[3]) ? $scope.list_coachs[3].id : null;
        $scope.course_session.quad_coach_name = ($scope.list_coachs && $scope.list_coachs[3]) ? $scope.list_coachs[3].name : null;
        
        $scope.course_session.five_coach_id = ($scope.list_coachs && $scope.list_coachs[4]) ? $scope.list_coachs[4].id : null;
        $scope.course_session.five_coach_name = ($scope.list_coachs && $scope.list_coachs[4]) ? $scope.list_coachs[4].name : null;
        
        $scope.course_session.number_coach = ($scope.list_coachs && $scope.list_coachs.length) ? $scope.list_coachs.length : 0;
        
        
        if($scope.course_session.number_coach <= 5){
        $http({
            method: 'POST',
            data: { course_session: $scope.course_session, force_validation: $scope.apply_force_validation },
            url: '/api/CourseSession/update'
        }).then(function successCallback(response) {
            $scope.need_force_validation = null;
            $scope.apply_force_validation = null;
            if (response.data.status) {
                $uibModalInstance.close();
                $('.calendar').fullCalendar('refetchEvents');
            } else {
                if(response.data.attributes && response.data.attributes.need_force_validation){
                    $scope.need_force_validation = true;
                }
                $scope.errors_form = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });

        }else{
        $scope.errors_max_five_coachs = true ;
        }

    };

    
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };



    $scope.delete = function(course_session_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseSessionInstanceCtrl',
            resolve: {
                course_session_id: function() {
                    return course_session_id;
                }
            }
        });

        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {
        });
        $uibModalInstance.dismiss('cancel');
    };

    $scope.request_cancel_cours = function(course_session_id){
            $scope.active = "Y";
            $scope.coach_session_id = course_session_id;
            $scope.state = "annuler";
            $scope.nbr_paticipants = 0;
            $scope.coach = "";
            $scope.Params = 'active=' + $scope.active + ',coach_session_id=' +$scope.coach_session_id + ',state=' + $scope.state + ',nbr_paticipants=' + $scope.nbr_paticipants + ',coach=' + $scope.coach;
            $http({
                method: 'GET',
                url: '/api/CourseSession/request_cancel_cours/' + $scope.Params
            }).then(function successCallback(response) {
                
                if (response.data.status) {
                   $uibModalInstance.close();
                
                } else if(response.data.status) {
                    $scope.success_form = true;
                }else if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }
               
            }, function errorCallback(response) {
                console.log('error');
            });
    };

    
    $scope.add_review = function(course_session_id) {
        $scope.coach_session_id = course_session_id;
        var review = document.getElementById('form-add-review').value ;
        $scope.Params = 'coach_session_id=' +$scope.coach_session_id + ',review=' + review ; 
        $http({
            method: 'GET',
            url: '/api/CourseSession/add_review/' + $scope.Params
        }).then(function successCallback(response) {
            
            if (response.data.status) {
               $uibModalInstance.close({

            });
           
            }else if(response.data.status){
                $scope.success_form = true;
            }else if(!response.data.status) {
                $scope.errors_form = response.data.message;
            }
           
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    
    $scope.msg_set_nbr_participants = false;

    
    $scope.validate_objectif_remplissage = function(course_session_id){

        $scope.active = "Y";
        $scope.coach_session_id = course_session_id;
        var check_validate_remplissage = $('#check_validate_remplissage').prop('checked');
        if(course_session.set_nbr_participants != null){
        $scope.Params = 'active=' + $scope.active + ',coach_session_id=' +$scope.coach_session_id + ',check_validate=' + check_validate_remplissage ; 
        $http({
            method: 'GET',
            url: '/api/CourseSession/validate_objectif_remplissage/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
               $uibModalInstance.close();

            }else if(response.data.status){
                $scope.success_form = true;
            }else if(!response.data.status) {
                $scope.errors_form = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        }else{
          $scope.msg_set_nbr_participants = true;
        }
    
    };

    $scope.display_alert_coach = false ;
    $scope.replace_coach = function(course_session_id){
        $scope.active = "Y";
        $scope.coach_session_id = course_session_id;
        $scope.state = "replace";
        $scope.nbr_paticipants = 0;
        $scope.coach = $scope.course_session.coach_id;
        $scope.Params = 'active=' + $scope.active + ',coach_session_id=' +$scope.coach_session_id + ',state=' + $scope.state + ',nbr_paticipants=' + $scope.nbr_paticipants + ',coach=' + $scope.coach; 
        $http({
            method: 'GET',
            url: '/api/CourseSession/replace_coach/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $uibModalInstance.close();
            }else if(response.data.status){
                $scope.success_form = true;
            }else if(!response.data.status) {
                $scope.errors_form = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.replace_cours = function(course_session_id){

        $scope.active = "Y";
        $scope.coach_session_id = course_session_id;
        $scope.state = "replace";
        $scope.nbr_paticipants = 0;
        

        $scope.errors_max_five_coachs = false ;
        $scope.errors_update_cours_day_after = false ;
        
        $scope.course_session.primary_coach_id = ($scope.list_coachs && $scope.list_coachs[0]) ? $scope.list_coachs[0].id : null;
        $scope.course_session.primary_coach_name = ($scope.list_coachs && $scope.list_coachs[0]) ? $scope.list_coachs[0].name : null;
        
        $scope.course_session.secondary_coach_id = ($scope.list_coachs && $scope.list_coachs[1]) ? $scope.list_coachs[1].id : null;
        $scope.course_session.secondary_coach_name = ($scope.list_coachs && $scope.list_coachs[1]) ? $scope.list_coachs[1].name : null;
        
        $scope.course_session.triple_coach_id = ($scope.list_coachs && $scope.list_coachs[2]) ? $scope.list_coachs[2].id : null;
        $scope.course_session.triple_coach_name = ($scope.list_coachs && $scope.list_coachs[2]) ? $scope.list_coachs[2].name : null;
        
        $scope.course_session.quad_coach_id = ($scope.list_coachs && $scope.list_coachs[3]) ? $scope.list_coachs[3].id : null;
        $scope.course_session.quad_coach_name = ($scope.list_coachs && $scope.list_coachs[3]) ? $scope.list_coachs[3].name : null;
        
        $scope.course_session.five_coach_id = ($scope.list_coachs && $scope.list_coachs[4]) ? $scope.list_coachs[4].id : null;
        $scope.course_session.five_coach_name = ($scope.list_coachs && $scope.list_coachs[4]) ? $scope.list_coachs[4].name : null;
        
        $scope.course_session.number_coach = ($scope.list_coachs && $scope.list_coachs.length) ? $scope.list_coachs.length : 0;
        
        $scope.cours = $scope.course_session.course_id;
        $scope.coach  = $scope.course_session.primary_coach_id;

        
        var DateObj = new Date();
        var date_jour = DateObj.getFullYear() + '-' + ('0' + (DateObj.getMonth() + 1)).slice(-2) + '-' + ('0' + DateObj.getDate()).slice(-2); 
        var date_jour_event = document.getElementById('date_planing_home').value;


        if(date_jour_event ==  date_jour || pos_manage_controller != -1){

        $scope.errors_update_cours_day_after = false ;
        if($scope.list_coachs.length != 0){
        $scope.errors_max_five_coachs = false ;
        if($scope.course_session.number_coach <= 5){
            $scope.Params = 'active=' + $scope.active + ',coach_session_id=' +$scope.coach_session_id + ',state=' + $scope.state + ',nbr_paticipants=' + $scope.nbr_paticipants + ',course=' + $scope.cours + ',coach=' +$scope.coach +',coach_primary_id=' +$scope.course_session.primary_coach_id +',coach_secondary_id=' +$scope.course_session.secondary_coach_id+',coach_triple_id=' +$scope.course_session.triple_coach_id+',coach_quad_id=' +$scope.course_session.quad_coach_id+',coach_five_id=' +$scope.course_session.five_coach_id+',coach_primary_name=' +$scope.course_session.primary_coach_name +',coach_secondary_name=' +$scope.course_session.secondary_coach_name+',coach_triple_name=' +$scope.course_session.triple_coach_name+',coach_quad_name=' +$scope.course_session.quad_coach_name+',coach_five_name=' +$scope.course_session.five_coach_name; 
            $http({
                method: 'GET',
                url: '/api/CourseSession/replace_course/' + $scope.Params
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $uibModalInstance.close();
                   
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }else{
           $scope.errors_max_five_coachs = true ;
        }

        }else{
           $scope.msg_min_one_coach = true ; 
        }

        }else{
           
            $scope.errors_update_cours_day_after = true ;
    
        }
        
    };

    $scope.nbr_paritcipants_cours = function(course_session_id) {

        $scope.active = "Y";
        $scope.coach_session_id = course_session_id;
        $scope.state = "participe";

        var val_part = $scope.course_session.set_nbr_participants;

        $scope.nbr_paticipants = val_part;
        $scope.coach = "";
        $scope.Params = 'active=' + $scope.active + ',coach_session_id=' +$scope.coach_session_id + ',state=' + $scope.state + ',nbr_paticipants=' + $scope.nbr_paticipants + ',coach=' + $scope.coach; 
        
        $http({
            method: 'GET',
            url: '/api/CourseSession/nbr_paritcipants_cours/' + $scope.Params
        }).then(function successCallback(response) {
            
            if (response.data.status) {
                $uibModalInstance.close();
            
            }else if(response.data.status){
                $scope.success_form = true;
            }else if(!response.data.status) {
                $scope.errors_form = response.data.message;
            }
           
        }, function errorCallback(response) {
            console.log('error');
        });
    
    };
    
});






app.controller('ModalDeleteCourseSessionInstanceCtrl', function($scope, $uibModalInstance, $http, course_session_id, $location, Notification, $filter) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { course_session_id: course_session_id },
            url: '/api/CourseSession/delete'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            } else {
                angular.forEach(response.data.message, function(value, key) {
                    Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


app.controller('ModalValidateCourseSessionsInstanceCtrl', function($scope, $uibModalInstance, $http, week_num, year, room_id, club_id, $location, Notification, $filter) {
    $scope.analyzing = true;
    $scope.coaches = null;
    
    $scope.analyse = function() {
        $scope.analyzing = true;
        $http({
            method: 'POST',
            data: { week_num: week_num, week_year: year, room_id: room_id, club_id: club_id },
            url: '/api/CourseSession/analyse'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.analyzing = false;
                if(response.data.attributes && response.data.attributes.coaches){
                    $scope.coaches = response.data.attributes.coaches;
                }
            } else {
                angular.forEach(response.data.message, function(value, key) {
                    Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.analyse();



    $scope.confirm = function() {
        $http({
            method: 'POST',
            data: { week_num: week_num, week_year: year, room_id: room_id, club_id: club_id },
            url: '/api/CourseSession/validate_planning'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('Planning.PlanningValidatedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            } else {
                angular.forEach(response.data.message, function(value, key) {
                    Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});











app.controller('ModalDuplicateCourseSessionsInstanceCtrl', function($scope, $uibModalInstance, $http, week_num, year, room_id, club_id, $location, Notification, $filter) {

    $scope.popup_datepicker = {
        date: moment().add(7, 'days').startOf('week').toDate(),
        opened: false,
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: moment().add(1, 'years').toDate(),
        minDate: null,
        startingDay: 1,
        dateDisabled: function(option) {
            return (option.mode === 'day' && (option.date.getDay() !== 1));
        }
    };

    $scope.yearWeek = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('Y'));
        // return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 10) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
    };

    $scope.weekForDate = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('W'));
    };

    $scope.confirm = function() {
        $scope.errors_form = null;
        $http({
            method: 'POST',
            data: { week_num: week_num, week_year: year, room_id: room_id, club_id: club_id, date: moment($scope.popup_datepicker.date).format('YYYY-MM-DD') },
            url: '/api/CourseSession/duplicate_planning'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('Planning.PlanningDuplicatedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            } else {
                $scope.errors_form = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.open_datepicker = function() {
        $scope.popup_datepicker.opened = true;
    };

    $scope.disabledDates = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 1));
    };

});




app.controller('ModalSendByEmailInstanceCtrl', function($scope, $uibModalInstance, $http, week_num, year, room_id, club_id, empty_course_sessions, have_course_sessions_invalide, $location, Notification, $filter) {

    $scope.errors_form = [];
    if(empty_course_sessions){
        $scope.errors_form.push('Planning.EmptyPlanning');
    }
    if(have_course_sessions_invalide && !empty_course_sessions){
        $scope.errors_form.push('Planning.TheScheduleHasNotYetBeenValidated');
    }
    if(!empty_course_sessions && !have_course_sessions_invalide){
        $scope.errors_form.push('Planning.TheMailServerIsNotConfiguredYet');
    }

    $scope.confirm = function() {
        $scope.errors_form = null;
        $http({
            method: 'POST',
            data: { week_num: week_num, week_year: year, room_id: room_id, club_id: club_id, date: moment($scope.popup_datepicker.date).format('YYYY-MM-DD') },
            url: '/api/CourseSession/send_by_email'
        }).then(function successCallback(response) {
            if (response.data.status) {
                Notification.success({ message: $filter('translate')('Planning.PlanningDuplicatedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            } else {
                $scope.errors_form = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.open_datepicker = function() {
        $scope.popup_datepicker.opened = true;
    };
    $scope.disabledDates = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 1));
    };

});