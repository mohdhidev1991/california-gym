'use strict';
app.controller('PlanningManageCoachController', function($scope, $http, $uibModal, $stateParams, $location, $rootScope, $translate, uiCalendarConfig, $filter) {
    
    $scope.club = null;
    $scope.room = null;
    $scope.course_sessions = [];
    $scope.events = [];
    $scope.eventsAllRoomsHome = {e0:[[]], e1:[[]], e2:[[]], e3:[[]], e4:[[]]};
    $scope.eventsAllRoomsHome0 = [];
    $scope.eventsAllRoomsHome1 = [];
    $scope.eventsAllRoomsHome2 = [];
    $scope.eventsAllRoomsHome3 = [];
    $scope.eventsAllRoomsHome4 = [];
    $scope.eventsAllRoomsHome5 = [];
    $scope.eventsAllRoomsHome6 = [];
    $scope.eventsAllRoomsHome7 = [];
    $scope.eventsAllRoomsHome8 = [];
    $scope.eventsAllRoomsHome9 = [];
    $scope.cost_per_room = 0;
    $scope.cost_per_club = 0;
    $scope.url_print_pdf = null;
    $scope.url_print_all_pdf = null;
    var permissions = sessionStorage.getItem("permissions");
    var pos_manage_home = permissions.indexOf("manage_home");
    var pos_manage_controller = permissions.indexOf("validate_canceled_courses");
    if(pos_manage_controller != -1){
       var ladate=new Date()
        var tab_jour=new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
        var jour_mois = ladate.getDate();
        var jour_lettre = tab_jour[ladate.getDay()];
        if(jour_lettre == "Dimanche" || jour_mois == 20){
            $scope.display_button_valid_hebdomadaire = true;
        }else{
            $scope.display_button_valid_hebdomadaire = false;
        }
    }

    
    

    
    $http({
        method: 'GET',
        url: '/api/Coach/getAll',
    }).then(function successCallback(response) {
        if (response.data.status) {
            $scope.ListCoachs = response.data.data;
        }
    }, function errorCallback(response) {
        console.log('error');
    });

    

    $scope.GetClubs = function() {
        $('.calendar').hide();
        $scope.Params = null;
        $http({
            method: 'GET',
            url: '/api/Club/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetClubs();

    

    



    $scope.format_date = function(date_, format_) {
        return moment(date_).format(format_);
    };

    
    $scope.yearWeek = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('Y'));
        // return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 10) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
    };

    $scope.weekForDate = function(date) {
        if (!date) { date = moment(); 
        }
        var begin = moment(date).startOf('week').isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('W'));
    };

    $scope.GetCourseSessions = function() {
        
        $scope.course_sessions = [];
        $scope.cost_per_room = 0;
        $scope.cost_per_club = 0;
        $scope.have_course_sessions_invalide = false;
        $scope.empty_course_sessions = false;
        $scope.url_print_pdf_coach = null;
       


        $('.calendar').fullCalendar('removeEvents');
        $('.calendar').addClass('loading');
        
        if (!$scope.coach) {
            return;
        }

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        
        if($scope.club){
        $scope.Params = 'join_obj=all,actives=I!Y,club_id=' + $scope.club.id + ',coach_id=' + $scope.coach.id + ',week_num=' + week_num + ',week_year=' + $scope.yearWeek($('.calendar').fullCalendar('getDate'));
        $scope.url_print_pdf_coach = window.config.base_url + '/planning/coach/' + $scope.club.id + '/' + $scope.coach.id + '/' + $scope.yearWeek($('.calendar').fullCalendar('getDate')) + '/' + week_num + '/print_pdf';
        }else{
        $scope.Params = 'join_obj=all,actives=I!Y,club_id=null,coach_id=' + $scope.coach.id + ',week_num=' + week_num + ',week_year=' + $scope.yearWeek($('.calendar').fullCalendar('getDate'));
        $scope.url_print_pdf_coach = window.config.base_url + '/planning/coach/0/' + $scope.coach.id + '/' + $scope.yearWeek($('.calendar').fullCalendar('getDate')) + '/' + week_num + '/print_pdf';
        
        }

        console.log($scope.Params);
        
                $http({
                    method: 'GET',
                    url: '/api/CourseSession/getbycoach/' + $scope.Params
                }).then(function successCallback(response) {

                    $('.calendar').fullCalendar('removeEvents');
                    $('.calendar').removeClass('loading');
                    if (response.data.status) {

                        $scope.course_sessions = response.data.data;
                        if(!$scope.course_sessions.length){
                        $scope.empty_course_sessions = true;
                        }

                        angular.forEach($scope.course_sessions, function(obj, key) {
                            if(typeof(obj.coach)==='undefined' || !obj.coach){
                                obj.coach = {};
                            }
        
                            if (obj.active === 'I') { $scope.have_course_sessions_invalide = true; }
                            var icon_invalid = (obj.active === 'Y') ? '' : '<i class="fa fa-info-circle alert-event text-danger" aria-hidden="true"></i>';
                                    var icon_remplacant = (obj.secondary_coach_id === obj.coach_id) ? '<i class="fa fa-retweet alert-coach_id_secondary text-danger" aria-hidden="true"></i>' : '';
                                    var icon_notif_number_participants = (obj.notif_time_set_nbr_participants === 'notif_active') ? '<i class="fa fa-bell alert-event text-warning" style="background:none;border:none;" aria-hidden="true"></i>' : '';
                                    var icon_set_nbr_participants = (obj.set_nbr_participants  > 0) ? '<span class="alert-event" style="border:none;color:#fff;background:none;padding-top:7px;padding-right:40px;">'+obj.set_nbr_participants+'</span>' : '';
                                    var icon_check_validate_remplissage = (obj.check_validate_remplissage == 'on') ? '<span class="fa fa-check alert-event" style="background:none;border:none;padding-top:7px;padding-right:20px;color:#fff;" aria-hidden="true"></span>' : '';
                                    var icon_review = (obj.review != '') ? '<span class="fa fa-comments alert-event" style="background:none;border:none;padding-top:7px;padding-right:60px;color:#fff;" aria-hidden="true"></span>' : '';
                            
                                    if(obj.color_cancel_cours){
                                        var color = obj.color_cancel_cours;
                                        var course_name = obj.course.course_name; 
                                        if(obj.type_course == 'Autre'){
                                            var description_course = obj.description_course;
                                            
                                            if(description_course.length < 20){
                                                var course_name = description_course.substring(0,20);
                                            }else{
                                                var course_name = description_course.substring(0,20);
                                                course_name = course_name+'...' ;
                                            }
                                        }
                
                                    }else{
                                        if(obj.type_course == 'Cours'){
                                            var color = obj.course.color;  
                                            var course_name = obj.course.course_name; 
                                        }else{
                                            var color = '#00ff00';
                                            var description_course = obj.description_course;
                                            
                                            if(description_course.length < 20){
                                                var course_name = description_course.substring(0,20);
                                            }else{
                                                var course_name = description_course.substring(0,20);
                                                course_name = course_name+'...' ;
                                            }
                                            
                                        }
                                    }
        
                                    $scope.events.push({
                                        id: obj.id,
                                        data: obj,
                                        title: icon_review + icon_set_nbr_participants
                                        + icon_check_validate_remplissage
                                        + icon_notif_number_participants
                                        + icon_invalid
                                        + icon_remplacant + course_name + ' - ' + obj.coach.firstname +' '+ obj.coach.lastname+''+ obj.club.club_name ,
                                        start: moment(obj.course_session_date + ' ' + obj.course_start_time),
                                        end: moment(obj.course_session_date + ' ' + obj.course_end_time),
                                        color: color,
                                    });
                                
                        });


                        if (response.data.attributes.cost_per_club) {
                            $scope.cost_per_club = response.data.attributes.cost_per_club;
                        }
                        if (response.data.attributes.cost_per_room) {
                            $scope.cost_per_room = response.data.attributes.cost_per_room;
                        }
                    
                    }
                
                }, function errorCallback(response) {
                    console.log('error');
                });

        };


    
   
    $scope.clean_hours = function(elements, deleteValue) {
        for (var i = 0; i < elements.length; i++) {
            if (elements[i] == deleteValue) {
                elements.splice(i, 1);
                i--;
            }
        }
        return elements;
    };

    $scope.select_club = function(club){
        $scope.club = club;
        $scope.room = null;
        var opening_hours = [$scope.club.opening_hour_1, $scope.club.opening_hour_2, $scope.club.opening_hour_3, $scope.club.opening_hour_4, $scope.club.opening_hour_5, $scope.club.opening_hour_6, $scope.club.opening_hour_7];
        opening_hours = $scope.clean_hours(opening_hours, undefined);
        var closing_hours = [$scope.club.closing_hour_1, $scope.club.closing_hour_2, $scope.club.closing_hour_3, $scope.club.closing_hour_4, $scope.club.closing_hour_5, $scope.club.closing_hour_6, $scope.club.closing_hour_7];
        closing_hours = $scope.clean_hours(closing_hours, undefined);
        $scope.uiConfig.calendar.minTime = $filter('orderBy')(opening_hours)[0];
        if( parseInt(moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').format('HH')) == 23 ){
            $scope.uiConfig.calendar.maxTime = '23:59';
        }else{
         $scope.uiConfig.calendar.maxTime = moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').add(1, 'hours').format('HH:mm');   
        }
        $('.calendar').hide();
    };


    $scope.select_all_club = function(){
        $scope.all = 1;
        $scope.club = null;
        $scope.room = null;
        var opening_hours = [$scope.club.opening_hour_1, $scope.club.opening_hour_2, $scope.club.opening_hour_3, $scope.club.opening_hour_4, $scope.club.opening_hour_5, $scope.club.opening_hour_6, $scope.club.opening_hour_7];
        opening_hours = $scope.clean_hours(opening_hours, undefined);
        var closing_hours = [$scope.club.closing_hour_1, $scope.club.closing_hour_2, $scope.club.closing_hour_3, $scope.club.closing_hour_4, $scope.club.closing_hour_5, $scope.club.closing_hour_6, $scope.club.closing_hour_7];
        closing_hours = $scope.clean_hours(closing_hours, undefined);
        $scope.uiConfig.calendar.minTime = $filter('orderBy')(opening_hours)[0];
        if( parseInt(moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').format('HH')) == 23 ){
            $scope.uiConfig.calendar.maxTime = '23:59';
        }else{
         $scope.uiConfig.calendar.maxTime = moment($filter('orderBy')(closing_hours, '-')[0], 'HH:mm').add(1, 'hours').format('HH:mm');   
        }
        $('.calendar').hide();
    };


    $scope.select_coach = function(coach){
        $('.calendar').show();
        $('.calendar').fullCalendar('gotoDate', moment().add(1, 'weeks').toDate());
        $scope.coach = coach;
        $('.calendar').fullCalendar('refetchEvents'); 
    };

    
    $scope.precision = 400;
    $scope.lastClickTime = 0;
    $scope.alertOnDayClick = function(date_event, jsEvent, view) {
        var time = new Date().getTime();
        if (time - $scope.lastClickTime <= $scope.precision) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ModalAddCourseSession.html',
                controller: 'ModalAddCourseSessionInstanceCtrl',
                resolve: {
                    club_id: function() {
                        return $scope.club.id;
                    },
                    room_id: function() {
                        return $scope.room.id;
                    },
                    date_event: function() {
                        return date_event;
                    }
                }
            });
            modalInstance.result.then(function(data) {
                $('.calendar').fullCalendar('refetchEvents');
            }, function() {

            });

        }
        $scope.lastClickTime = time;
    };
    /* alert on Drop */
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = event;
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = event;
    };

    $scope.overlay = $('.fc-overlay');
    $scope.alertOnMouseOver = function(event, jsEvent, view) {
        $scope.event = event;
        $scope.overlay.removeClass('left right top').find('.arrow').removeClass('left right top pull-up');
        var wrap = $(jsEvent.target).closest('.fc-event');
        var cal = wrap.closest('.calendar');
        var left = wrap.offset().left - cal.offset().left;
        var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
        var top = cal.height() - (wrap.offset().top - cal.offset().top + wrap.height());
        if (right > $scope.overlay.width()) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up');
        } else if (left > $scope.overlay.width()) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
        } else {
            $scope.overlay.find('.arrow').addClass('top');
        }
        if (top < $scope.overlay.height()) {
            $scope.overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass('pull-down');
        }
        (wrap.find('.fc-overlay').length === 0) && wrap.append($scope.overlay);
    };



    $scope.alertOnEventClick = function(event, jsEvent, view) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddCourseSession.html',
            controller: 'ModalEditCourseSessionInstanceCtrl',
            resolve: {
                course_session: function() {
                    return event.data;
                }
            }
        });
        modalInstance.result.then(function(data) {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };



    $scope.delete_course_session = function(course_session_id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteCourseSessionInstanceCtrl',
            resolve: {
                course_session_id: function() {
                    return course_session_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };



    $scope.validate_course_sessions = function(course_session_id) {

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        var year_num = this.yearWeek($('.calendar').fullCalendar('getDate'));

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalValidateCourseSessions.html',
            controller: 'ModalValidateCourseSessionsInstanceCtrl',
            resolve: {
                week_num: function() {
                    return week_num;
                },
                year: function() {
                    return year_num;
                },
                room_id: function() {
                    return $scope.room.id;
                },
                club_id: function() {
                    return $scope.club.id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };

    
    if (pos_manage_home != -1){
        
        $scope.uiConfig = {
        calendar: {
            defaultView: 'agendaDay',
            defaultDate : sessionStorage.getItem("date_calendar"), 
            allDaySlot: false,
            firstDay: 1,
            timezone: 'local',
            lang: $rootScope.app.settings.selectLang,
            height: 1150,
            
            editable: false,
            eventDurationEditable: false,
            eventStartEditable: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            
            eventClick: $scope.alertOnEventClick,
            dayClick: $scope.alertOnDayClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            events: function(start, end, timezone, callback) {
                $scope.GetCourseSessions();
            },
            eventAfterAllRender: function(view) {},
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
            }
            
        }
    };
    
    }else{

    /* config object */
    
    $scope.uiConfig = {
        calendar: {
            defaultView: 'agendaWeek',
            allDaySlot: false,
            firstDay: 1,
            timezone: 'local',
            lang: $rootScope.app.settings.selectLang,
            height: 650,
            editable: false,
            eventDurationEditable: false,
            eventStartEditable: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            eventClick: $scope.alertOnEventClick,
            dayClick: $scope.alertOnDayClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            //eventMouseover: $scope.alertOnMouseOver,
            events: function(start, end, timezone, callback) {
                $scope.GetCourseSessions();
            },
            eventAfterAllRender: function(view) {},
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
            }
        
        }
    };

    }

    /* add custom event*/
    $scope.addEvent = function() {
        $scope.events.push({
            title: 'New Event',
            start: new Date(y, m, d),
            className: ['b-l b-2x b-info']
        });
    };

    /* remove event */
    $scope.remove = function(index) {
        $scope.events.splice(index, 1);
    };

    /* Change View */
    $scope.changeView = function(view, calendar) {
        $('.calendar').fullCalendar('changeView', view);
    };

    $scope.today = function(calendar) {
        $('.calendar').fullCalendar('today');
    };

    /* event sources array*/
    $scope.eventSources = [$scope.events];
    $scope.eventSourcesAllRoomsHome = $scope.eventsAllRoomsHome;
    //$scope.eventSourcesAllRoomsHome[0] = [$scope.eventsAllRoomsHome0];
    //window.location.reload();
    $scope.eventSourcesAllRoomsHome0 = [$scope.eventsAllRoomsHome0];
    $scope.eventSourcesAllRoomsHome1 = [$scope.eventsAllRoomsHome1];
    $scope.eventSourcesAllRoomsHome2 = [$scope.eventsAllRoomsHome2];
    $scope.eventSourcesAllRoomsHome3 = [$scope.eventsAllRoomsHome3];
    $scope.eventSourcesAllRoomsHome4 = [$scope.eventsAllRoomsHome4];
    $scope.eventSourcesAllRoomsHome5 = [$scope.eventsAllRoomsHome5];
    $scope.eventSourcesAllRoomsHome6 = [$scope.eventsAllRoomsHome6];
    $scope.eventSourcesAllRoomsHome7 = [$scope.eventsAllRoomsHome7];
    $scope.eventSourcesAllRoomsHome8 = [$scope.eventsAllRoomsHome8];
    $scope.eventSourcesAllRoomsHome9 = [$scope.eventsAllRoomsHome9];
   $scope.duplicate_course_sessions = function(course_session_id) {
        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        var year_num = this.yearWeek($('.calendar').fullCalendar('getDate'));
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalDuplicateCourseSessions.html',
            controller: 'ModalDuplicateCourseSessionsInstanceCtrl',
            resolve: {
                week_num: function() {
                    return week_num;
                },
                year: function() {
                    return year_num;
                },
                room_id: function() {
                    return $scope.room.id;
                },
                club_id: function() {
                    return $scope.club.id;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };




    $scope.send_by_email = function(course_session_id) {

        var week_num = this.weekForDate($('.calendar').fullCalendar('getDate'));
        var year_num = this.yearWeek($('.calendar').fullCalendar('getDate'));

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalSendByEmail.html',
            controller: 'ModalSendByEmailInstanceCtrl',
            resolve: {
                week_num: function() {
                    return week_num;
                },
                year: function() {
                    return year_num;
                },
                room_id: function() {
                    return $scope.room.id;
                },
                club_id: function() {
                    return $scope.club.id;
                },
                empty_course_sessions: function() {
                    return $scope.empty_course_sessions;
                },
                have_course_sessions_invalide: function() {
                    return $scope.have_course_sessions_invalide;
                }
            }
        });
        modalInstance.result.then(function() {
            $('.calendar').fullCalendar('refetchEvents');
        }, function() {

        });
    };

    /*debut modif calendar 03/01/2019*/

    $scope.iframeLoaded = function(){   ;
        console.log($scope)
        $scope.showloader = false;     
    } 
   
    $scope.Table = createTable();
    $scope.class = "nothing";
    $scope.changeClass = function(){
        console.log('test');
    };
    
    function createTable() {
        var Table = {};
        Table.Lines = [];
         
        for (var i = 0; i < 4; i++) {
            var Line = {};
            Line.Cases = [];
             
            for (var j = 0; j < 10; j++) {
                var Points = {};
                Line.Cases.push(Points);
            }
            Table.Lines.push(Line);
        }
        return Table;
    }
    /*fin modif calendar 03/01/2019*/



});



































