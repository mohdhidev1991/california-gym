'use strict';
app.controller('PlanningListRoomManageController', function($scope, $compile, $http, $uibModal, $stateParams, $location, $rootScope, $translate, uiCalendarConfig, $filter) {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $scope.is_manage_home =  true;

    $scope.events = [
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d, 10, 0), end: new Date(y, m, d, 16, 0)}
    ];

    /* config object */
    $scope.uiConfig = {
      calendar:{
        defaultView: 'agendaDay',
        height: 450,
        editable: true,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        }
        // eventClick: $scope.alertOnEventClick,
        // eventDrop: $scope.alertOnDrop,
        // eventResize: $scope.alertOnResize,
        // eventRender: $scope.eventRender
      }
    };

    $scope.roomsRoleHome = [];
    $scope.GetRoomsRoleHome = function(){
      $scope.roomsRoleHome = [];
      var club = sessionStorage.getItem("club");
      club = 9;
      $scope.Params = 'club_id='+club ;
      $http({
          method: 'GET',
          url: '/api/Room/get/' + $scope.Params
      }).then(function successCallback(response) {
          if (response.data.data.length) {
              $scope.roomsRoleHome = response.data.data;
              var date_planing_home = sessionStorage.getItem("date_calendar");
              // document.getElementById("date_planing_home").value = date_planing_home ;
              for(var i = 0 ; i < response.data.data.length ; i++){

                // $scope.GetCourseSessionsAllRoomsHome(i,response.data.data[i],date_planing_home);
              }
              $scope.roomsRoleHome.forEach(room_item => {
                var config_copie = JSON.parse(JSON.stringify($scope.uiConfig.calendar));
                room_item.configCalendar = config_copie;
                date_planing_home = '2020-02-18';
                $scope.GetCourseSessionsAllRoomsHome(room_item, date_planing_home);
                // room_item.events = JSON.parse(JSON.stringify([$scope.events]));
              });
              console.log('$scope.roomsRoleHome', $scope.roomsRoleHome)
          }
          console.log('here')
      }, function errorCallback(response) {
          console.log('error');
      });
  };
  $scope.GetRoomsRoleHome();


  $scope.yearWeek = function(date) {
      if (!date) { date = moment(); }
      var begin = moment(date).startOf('week').isoWeekday(1);
      return parseInt(begin.add(6, 'day').format('Y'));
      // return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 10) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
  };

  $scope.weekForDate = function(date) {
      if (!date) { date = moment(); }
      var begin = moment(date).startOf('week').isoWeekday(1);
      return parseInt(begin.add(6, 'day').format('W'));
  };

  $scope.GetCourseSessionsAllRoomsHome = function(room_item, date_planing_home) {
    var room_id = room_item.id;
    room_item.events = [];

    room_item.calendarConfig = JSON.parse(JSON.stringify($scope.uiConfig.calendar));
    $scope.course_sessions_all_rooms_home = [];
    $scope.cost_per_room_all_rooms_home = 0;
    $scope.cost_per_club_all_rooms_home = 0;
    $scope.have_course_sessions_invalide_all_rooms_home = false;
    $scope.empty_course_sessions_all_rooms_home = false;

    var club_id = sessionStorage.getItem("club");
    club_id = 9; 
    var week_num = this.weekForDate($('.calendar' + room_id).fullCalendar('getDate'));
    $scope.Params = 'join_obj=all,actives=I!Y,club_id=' + club_id + ',date_planing_home=' + date_planing_home + ',room_id=' + room_id + ',week_num=' + week_num + ',week_year=' + $scope.yearWeek($('.calendar' + room_id).fullCalendar('getDate'));
    $http({
        method: 'GET',
        url: '/api/CourseSession/getCoursesAllRoomsHome/' + $scope.Params
    }).then(function successCallback(response) {

        $('.calendar' + room_id).fullCalendar('removeEvents');
        $('.calendar' + room_id).removeClass('loading');
        if (response.data.status) {
            $scope.course_sessions_all_rooms_home = response.data.data;
            if(!$scope.course_sessions_all_rooms_home.length){
                $scope.empty_course_sessions_all_rooms_home = true;
            }

            $scope.course_sessions_all_rooms_home.forEach(item_session => {
              if(typeof(item_session.coach)==='undefined' || !item_session.coach){
                  item_session.coach = {};
              }

              if (item_session.active === 'I') { $scope.have_course_sessions_invalide_all_rooms_home = true; }
              
              var icon_invalid = (item_session.active === 'Y') ? '' : '<i class="fa fa-info-circle alert-event text-danger" aria-hidden="true"></i>';
              var icon_remplacant = (item_session.secondary_coach_id === item_session.coach_id) ? '<i class="fa fa-retweet alert-coach_id_secondary text-danger" aria-hidden="true"></i>' : '';
              var icon_notif_number_participants = (item_session.notif_time_set_nbr_participants === 'notif_active') ? '<i class="fa fa-bell alert-event text-warning" style="background:none;border:none;" aria-hidden="true"></i>' : '';
              var icon_set_nbr_participants = (item_session.set_nbr_participants  > 0) ? '<span class="alert-event" style="border:none;color:#fff;background:none;padding-top:7px;padding-right:40px;">'+item_session.set_nbr_participants+'</span>' : '';
              var icon_check_validate_remplissage = (item_session.check_validate_remplissage == 'on') ? '<span class="fa fa-check alert-event" style="background:none;border:none;padding-top:7px;padding-right:20px;color:#fff;" aria-hidden="true"></span>' : '';
              var icon_review = (item_session.review != '') ? '<span class="fa fa-comments alert-event" style="background:none;border:none;padding-top:7px;padding-right:60px;color:#fff;" aria-hidden="true"></span>' : '';
              
              if(item_session.color_cancel_cours){
                  var color = item_session.color_cancel_cours ;
              }else{
                  var color = item_session.course.color ;    
              }

              if(item_session.color_cancel_cours){
                  var color = item_session.color_cancel_cours ;
              }else{
                  var color = item_session.course.color ;    
              }

              let myData = [{
                id: item_session.id,
                data: item_session,
                title: icon_review + icon_set_nbr_participants + icon_check_validate_remplissage + icon_notif_number_participants + icon_invalid + icon_remplacant + item_session.course.course_name + ' - ' + item_session.coach.firstname +' '+ item_session.coach.lastname ,
                start: moment(item_session.course_session_date + ' ' + item_session.course_start_time).toDate(),
                end: moment(item_session.course_session_date + ' ' + item_session.course_end_time).toDate(),
                color: color,
            }];
              room_item.events.push(myData);
            });

            console.log(room_item.events)
            
            if (response.data.attributes.cost_per_club) {
                $scope.cost_per_club_all_rooms_home = response.data.attributes.cost_per_club;
            }
            if (response.data.attributes.cost_per_room) {
                $scope.cost_per_room_all_rooms_home = response.data.attributes.cost_per_room;
            }
            
        }

          // room_item.events = JSON.parse(JSON.stringify([$scope.events]));
          return;
      }, function errorCallback(response) {
          console.log('error');
      });
    };

});
