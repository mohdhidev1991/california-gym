'use strict';

app.controller('PlanningStatsController', function($scope, $http, $uibModal, $stateParams, $location, $filter) {

    $scope.club = null;
    $scope.url_export_stats = null;

    moment.locale('fr');

    $scope.dateRange = {
        startDate: moment().add(-1, 'months').format('YYYY-MM-DD'),
        endDate: moment().format('YYYY-MM-DD'),
        date: moment().add(-1, 'months').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD')
    };
    $scope.daterangepickerOptions = {
        format: 'YYYY-MM-DD',
        //startDate: moment().format('YYYY-MM-DD'),
        //endDate: moment().add(-1,'months').format('YYYY-MM-DD'),
        locale: {
            applyLabel: $filter('translate')('Global.Apply'),
            fromLabel: $filter('translate')('Global.From'),
            format: "YYYY-MM-DD",
            toLabel: $filter('translate')('Global.To'),
            cancelLabel: $filter('translate')('Global.Cancel')
                //customRangeLabel: 'Custom range'
        }
    };

    $scope.GetClubs = function() {
        $('.calendar').hide();
        $scope.Params = null;
        $http({
            method: 'GET',
            url: '/api/Club/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    
    $scope.GetClubs();
    $scope.select_club = function(club) {
        $scope.club = club;
        $scope.get_stats();
    };


    $scope.GetCoachs = function(){
        $('.calendar').hide();
        $scope.Params = 'selectConcept=0';
        $scope.Params += ',inputName=';
        $http({
            method: 'GET',
            url: '/api/Coach/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.coachs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    
    $scope.GetCoachs();
    
    $scope.select_coach = function(coach){
        $scope.coach = coach;
    };


    $scope.get_stats = function() {
        
        $scope.dateRange.startDate = moment($scope.dateRange.date.substring(10, 0)).format('YYYY-MM-DD');
        $scope.dateRange.endDate = moment($scope.dateRange.date.substring(23, 13)).format('YYYY-MM-DD');
        $scope.price_paid_by_course = null;
        $scope.price_per_course_by_coach = null;
        $scope.nmber_cour_per_coach_by_club = null;
        $scope.cost_per_club = null;
        $scope.total_cost_per_coach = null;
        $scope.number_of_courses_per_concept = null;
        $scope.number_of_courses_per_coach = null;
        $scope.number_of_courses_by_coach = null;
        $scope.number_of_classes_per_club = null;
        $scope.number_of_hours_worked_per_course = null;
        $scope.replacement_number_per_coach = null;
        $scope.number_of_replaced_by_coach = null;
        $scope.number_of_substitutes_per_club = null;
        $scope.number_of_courses_per_concept_per_club = null;
        $scope.number_of_courses_per_coach_per_club = null;
        $scope.empty_stats = null;
        
        //$scope.Params = 'date=' + moment($scope.popup_datepicker.date).unix() + ',stats_type=' + $scope.stats_type + ',stats_by=' + $scope.stats_by;
        $scope.Params = 'startdate=' + moment($scope.dateRange.startDate).unix() +',enddate=' + moment($scope.dateRange.endDate).unix() + ',stats_type=' + $scope.stats_type;
        
        if ($scope.club) {
            $scope.Params += ',club_id=' + $scope.club.id;
        }

        if ($scope.coach) {
            $scope.Params += ',coach_id=' + $scope.coach.id;
        }

        
               
        console.log($scope.Params);
        
        $scope.url_export_stats = null;
        $http({
            method: 'GET',
            url: '/api/CourseSession/stats/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.url_export_stats = window.config.base_url + '/api/CourseSession/stats/' + $scope.Params + ',export=csv';
            if (response.data.status && response.data.data.length) {
                switch ($scope.stats_type) {
                    case 'price_per_course_by_coach':
                        $scope.price_per_course_by_coach = {};
                        $scope.price_per_course_by_coach.series = [$filter('translate')('Statistics.PricePerCoursByCoach')];
                        $scope.price_per_course_by_coach.labels = [];
                        $scope.price_per_course_by_coach.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.price_per_course_by_coach.labels.push(obj.club_name+' ('+obj.nbr+' Cours)');
                            $scope.price_per_course_by_coach.data.push(obj.course_pricing * obj.nbr) ;
                        });
                    break;
                    case 'nmber_cour_per_coach_by_club':
                        $scope.nmber_cour_per_coach_by_club = {};
                        $scope.nmber_cour_per_coach_by_club.series = [$filter('translate')('Statistics.NmberCoursPerCoachByClub')];
                        $scope.nmber_cour_per_coach_by_club.labels = [];
                        $scope.nmber_cour_per_coach_by_club.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.nmber_cour_per_coach_by_club.labels.push(obj.club_name);
                            $scope.nmber_cour_per_coach_by_club.data.push(obj.nbr);
                        });
                    break;
                    case 'price_paid_by_course':
                        $scope.price_paid_by_course = {};
                        $scope.price_paid_by_course.series = [$filter('translate')('Statistics.PricePaidByCourse')];
                        $scope.price_paid_by_course.labels = [];
                        $scope.price_paid_by_course.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.price_paid_by_course.labels.push(obj.course_name+' ('+obj.price_paid+')');
                            $scope.price_paid_by_course.data.push(obj.price_paid);
                        });
                        break;
                    case 'cost_per_club':
                        $scope.cost_per_club = {};
                        $scope.cost_per_club.series = [$filter('translate')('Statistics.CostPerClub')];
                        $scope.cost_per_club.labels = [];
                        $scope.cost_per_club.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.cost_per_club.labels.push(obj.club_name+' ('+obj.price_paid+')');
                            $scope.cost_per_club.data.push(obj.price_paid);
                        });
                        break;
                    case 'number_of_courses_per_coach':
                        $scope.number_of_courses_per_coach = {};
                        $scope.number_of_courses_per_coach.series = [$filter('translate')('Statistics.NumberOfCoursesPerCoach')];
                        $scope.number_of_courses_per_coach.labels = [];
                        $scope.number_of_courses_per_coach.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.number_of_courses_per_coach.labels.push(obj.firstname +' '+obj.lastname +' #' + obj.registration_number+' ('+obj.courses+')');
                            $scope.number_of_courses_per_coach.data.push(obj.courses);
                        });
                        break;
                    case 'number_of_courses_per_concept':
                        $scope.number_of_courses_per_concept = {};
                        $scope.number_of_courses_per_concept.series = [$filter('translate')('Statistics.NumberOfCoursesPerConcept')];
                        $scope.number_of_courses_per_concept.labels = [];
                        $scope.number_of_courses_per_concept.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.number_of_courses_per_concept.labels.push(obj.course_name+' ('+obj.courses+')');
                            $scope.number_of_courses_per_concept.data.push(obj.courses);
                        });
                        break;
                    case 'total_cost_per_coach':
                        $scope.total_cost_per_coach = {};
                        $scope.total_cost_per_coach.series = [$filter('translate')('Statistics.TotalCostPerCoach')];
                        $scope.total_cost_per_coach.labels = [];
                        $scope.total_cost_per_coach.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.total_cost_per_coach.labels.push(obj.firstname +' '+obj.lastname +' #' + obj.registration_number+' ('+obj.price_paid+')');
                            $scope.total_cost_per_coach.data.push(obj.price_paid);
                        });
                        break;
                    case 'number_of_courses_by_coach':
                        $scope.number_of_courses_by_coach = {};
                        $scope.number_of_courses_by_coach.series = [$filter('translate')('Statistics.NumberOfCoursesByCoach')];
                        $scope.number_of_courses_by_coach.labels = [];
                        $scope.number_of_courses_by_coach.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.number_of_courses_by_coach.labels.push(obj.firstname +' '+obj.lastname +' #' + obj.registration_number+' ('+obj.courses+')');
                            $scope.number_of_courses_by_coach.data.push(obj.courses);
                        });
                        break;
                    case 'number_of_classes_per_club':
                        $scope.number_of_classes_per_club = {};
                        $scope.number_of_classes_per_club.series = [$filter('translate')('Statistics.NumberOfClassesPerClub')];
                        $scope.number_of_classes_per_club.labels = [];
                        $scope.number_of_classes_per_club.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.number_of_classes_per_club.labels.push(obj.club_name+' ('+obj.courses+')');
                            $scope.number_of_classes_per_club.data.push(obj.courses);
                        });
                        break;
                    case 'number_of_hours_worked_per_course':
                        $scope.number_of_hours_worked_per_course = {};
                        $scope.number_of_hours_worked_per_course.series = [$filter('translate')('Statistics.NumberOfHoursWorkedPerCourse')];
                        $scope.number_of_hours_worked_per_course.labels = [];
                        $scope.number_of_hours_worked_per_course.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            if(parseInt(obj.during_hours)==obj.during_hours){
                                obj.during_hours = parseInt(obj.during_hours);
                            }
                            $scope.number_of_hours_worked_per_course.labels.push(obj.course_name+' ('+obj.during_hours+')');
                            $scope.number_of_hours_worked_per_course.data.push(obj.during_hours);
                        });
                        break;
                    
                    case 'replacement_number_per_coach':
                        $scope.replacement_number_per_coach = {};
                        $scope.replacement_number_per_coach.series = [$filter('translate')('Statistics.ReplacementNumberPerCoach')];
                        $scope.replacement_number_per_coach.labels = [];
                        $scope.replacement_number_per_coach.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.replacement_number_per_coach.labels.push(obj.firstname + ' ' + obj.lastname+' #' + obj.registration_number+' ('+obj.courses+')');
                            $scope.replacement_number_per_coach.data.push(obj.courses);
                        });
                        break;

                    case 'number_of_replaced_by_coach':
                        $scope.number_of_replaced_by_coach = {};
                        $scope.number_of_replaced_by_coach.series = [$filter('translate')('Statistics.NumberOfReplacedByCoach')];
                        $scope.number_of_replaced_by_coach.labels = [];
                        $scope.number_of_replaced_by_coach.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.number_of_replaced_by_coach.labels.push(obj.firstname + ' ' + obj.lastname+' #' + obj.registration_number+' ('+obj.courses+')');
                            $scope.number_of_replaced_by_coach.data.push(obj.courses);
                        });
                        break;
                    case 'number_of_substitutes_per_club':
                        $scope.number_of_substitutes_per_club = {};
                        $scope.number_of_substitutes_per_club.series = [$filter('translate')('Statistics.NumberOfSubstitutesPerClub')];
                        $scope.number_of_substitutes_per_club.labels = [];
                        $scope.number_of_substitutes_per_club.data = [];
                        angular.forEach(response.data.data, function(obj, key) {
                            $scope.number_of_substitutes_per_club.labels.push(obj.club_name+' ('+obj.courses+')');
                            $scope.number_of_substitutes_per_club.data.push(obj.courses);
                        });
                        break;
                }
            } else {
                $scope.empty_stats = true;
                $scope.url_export_stats = null;
            }
        }, function errorCallback(response) {
            console.log('error');
        });

    };


    $scope.popup_datepicker = {
        date: moment().add(7, 'days').startOf('week').toDate(),
        opened: false,
    };
    $scope.date_format = 'M!/d!/yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: null,
        minDate: null,
        startingDay: 1,
        dateDisabled: function(option) {
            return (option.mode === 'day' && (option.date.getDay() !== 1));
        }
    };
    $scope.open_datepicker = function() {
        //$scope.popup_datepicker.opened = true;
        $('#daterangepicker').first().click();
    };
    $scope.disabledDates = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 1));
    };




    $scope.stats_type_list = {
        
        price_paid_by_course: 'Statistics.PricePaidByCourse',
        price_per_course_by_coach: 'Statistics.PricePerCoursByCoach',
        nmber_cour_per_coach_by_club: 'Statistics.NmberCoursPerCoachByClub',
        cost_per_club: 'Statistics.CostPerClub',
        total_cost_per_coach: 'Statistics.TotalCostPerCoach',
        number_of_courses_per_concept: 'Statistics.NumberOfCoursesPerConcept',
        //number_of_courses_per_coach: 'Statistics.NumberOfCoursesPerCoach',
        number_of_courses_by_coach: 'Statistics.NumberOfCoursesByCoach',
        //number_of_classes_per_club: 'Statistics.NumberOfClassesPerClub',
        number_of_hours_worked_per_course: 'Statistics.NumberOfHoursWorkedPerCourse',
        replacement_number_per_coach: 'Statistics.ReplacementNumberPerCoach',
        number_of_replaced_by_coach: 'Statistics.NumberOfReplacedByCoach',
        number_of_substitutes_per_club: 'Statistics.NumberOfSubstitutesPerClub',
        //number_of_courses_per_concept_per_club: 'Statistics.NumberOfCoursesPerConceptPerClub',
        //number_of_courses_per_coach_per_club: 'Statistics.NumberOfCoursesPerCoachPerClub'
    
    };


    $scope.stats_type = 'price_paid_by_course';
    $scope.select_stats_type = function(key) {
        $scope.stats_type = key;
        $scope.club = null;
        $scope.get_stats();
    };




    $scope.stats_by_list = { by_year: 'Global.Year', by_month: 'Global.Month', by_week: 'Global.Week' };
    $scope.select_stats_by = function(key) {
        $scope.stats_by = key;
        $scope.popup_datepicker.date = moment().startOf('week').add(1, 'days').toDate();
        switch (key) {
            case 'by_year':
                $scope.date_format = 'yyyy';
                $scope.dateOptions.datepickerMode = 'year';
                $scope.dateOptions.yearColumns = 3;
                $scope.dateOptions.yearRows = 1;
                $scope.dateOptions.minMode = 'year';
                $scope.dateOptions.maxMode = 'year';
                break;
            case 'by_month':
                $scope.date_format = 'yyyy-M!';
                $scope.dateOptions.datepickerMode = 'month';
                $scope.dateOptions.minMode = 'month';
                $scope.dateOptions.maxMode = 'year';
                break;
            case 'by_week':
                $scope.date_format = 'yyyy-M!-d!';
                $scope.dateOptions.datepickerMode = 'day';
                $scope.dateOptions.minMode = 'day';
                $scope.dateOptions.maxMode = 'year';
                break;
        }
        $scope.get_stats();
    };
    $scope.select_stats_by('by_month');


    //$scope.get_stats();
});