'use strict';

app.controller('RatingAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.ratings = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetRatings = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Rating/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.ratings) {
                $scope.ratings = response.data.data.ratings;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetRatings();
    };



    $scope.delete_rating = function(rating_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteRatingInstanceCtrl',
            resolve: {
                rating_id: function() {
                    return rating_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetRatings();
        }, function() {
            
        });
    };


    $scope.GetRatings();


});



app.controller('ModalDeleteRatingInstanceCtrl', function($scope, $uibModalInstance, $http, rating_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {rating_id : rating_id},
            url: '/api/Rating/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


