'use strict';

app.controller('RatingFormController', function($scope, $http, $stateParams, $location) {

    $scope.rating = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetRating = function() {
        $http({
            method: 'GET',
            url: '/api/Rating/get/rating_id='+$stateParams.rating_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.rating = response.data.data;
                if($scope.rating.active && $scope.rating.active==="Y"){
                    $scope.active_switch = true;
                }
                $scope.rating.max_pct = parseFloat($scope.rating.max_pct);
                $scope.rating.min_pct = parseFloat($scope.rating.min_pct);
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetRating();
    }else{
        $scope.rating = {};
        $scope.rating.new = true;
    }
    $scope.GetSchools();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.rating.active = "Y";
        }else{
            $scope.rating.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {rating: $scope.rating},
                url: '/api/Rating/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/ratings");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.rating);
            $http({
                method: 'POST',
                data: {rating: $scope.rating},
                url: '/api/Rating/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/rating/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



