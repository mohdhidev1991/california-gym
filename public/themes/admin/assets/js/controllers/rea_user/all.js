'use strict';

app.controller('ReaUserAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.rea_users = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100];

    $scope.GetReaUsers = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/ReaUser/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.rea_users = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetReaUsers();
    };


    $scope.GetRoles = function() {
        $http({
            method: 'GET',
            url: '/api/Role/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.roles = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.delete_user = function(user_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteReaUserInstanceCtrl',
            resolve: {
                user_id: function() {
                    return user_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetReaUsers();
        }, function() {
            
        });
    };


    $scope.GetReaUsers();
    $scope.GetRoles();
});

app.controller('ModalDeleteReaUserInstanceCtrl', function($scope, $uibModalInstance, $http, user_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {user_id : user_id},
            url: '/api/ReaUser/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


