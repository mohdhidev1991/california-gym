'use strict';

app.controller('ReaUserFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.rea_user = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.failed_get_data = false;
    $scope.roles = null;

    $scope.GetReaUser = function() {
        $http({
            method: 'GET',
            url: '/api/ReaUser/get/rea_user_id=' + $stateParams.rea_user_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.rea_user = response.data.data;
                if ($scope.rea_user.active && $scope.rea_user.active === "Y") {
                    $scope.active_switch = true;
                }
            }else{
                $scope.failed_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetRoles = function() {
        $http({
            method: 'GET',
            url: '/api/Role/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.roles = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetClubs = function(){
        $scope.Params = null;
        $http({
            method: 'GET',
            url: '/api/Club/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    if ($scope.action === "edit") {
        $scope.GetReaUser();
        $scope.GetClubs();
    } else {
        $scope.rea_user = {};
        $scope.rea_user.new = true;
        $scope.rea_user.roles = [];
        $scope.GetClubs();
    }

    $scope.GetRoles();
    $scope.pwCheck = function() {
        var valide = ($scope.rea_user.newpwd === $scope.rea_user.newpwd2);
        return valide;
    };
    
    
    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };

    $scope.pre_save = function() {
        if ($scope.active_switch === true) {
            $scope.rea_user.active = "Y";
        } else {
            $scope.rea_user.active = "N";
        }
        $scope.rea_user.roles_mfk = null;
        if($scope.rea_user.roles){
            $scope.rea_user.roles_mfk = ','+$scope.rea_user.roles.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location) {
        $scope.errors_form = null;
        $scope.success_form = null;
        console.log($scope.rea_user);
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { rea_user: $scope.rea_user },
                url: '/api/ReaUser/save'
            }).then(function successCallback(response) {
                if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }
                if (response.data.status && back_location) {
                    $state.go('rea_user.all');
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function() {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { rea_user: $scope.rea_user },
                url: '/api/ReaUser/save'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $state.go('rea_user.edit', { rea_user_id: response.data.data });
                }else{
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };

    
    $scope.selectclubs = function(){

       var club_id = document.getElementById('select-club').value;
       var club_name = $('#select-club option:selected').text();
       $scope.rea_user.club = club_id ;
       $scope.rea_user.club_name = club_name ;
    
    };
    
});

angular.module('app.directives', [])
    .directive('pwCheck', [function() {
        return {
            require: 'ngModel',
            link: function(scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function() {
                    scope.$apply(function() {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        };
    }]);