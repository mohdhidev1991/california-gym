'use strict';

app.controller('LoginFormController', function ($scope, $http, $state, $location) {

    $scope.failed_auth = null;
    
    $scope.login = function () {
        $scope.failed_auth = null;
        $http({
            method: 'POST',
            data: {rea_user: $scope.rea_user},
            url: '/api/weblogin'
        }).then(function successCallback(response) {
            if(response.data.status){
                sessionStorage.setItem('permissions', response.data.data.permissions);
                sessionStorage.setItem('club', response.data.data.club);
                var to_day = new Date();
                var d = to_day.getDate();
                var m = to_day.getMonth() + 1; //Month from 0 to 11
                var y = to_day.getFullYear();
                var dd= '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
                sessionStorage.setItem("date_calendar",dd);
                window.location = "";
            }else{
                $scope.failed_auth = true;
                $scope.failed_auth_message = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    
});
