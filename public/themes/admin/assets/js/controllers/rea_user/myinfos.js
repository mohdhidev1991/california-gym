'use strict';

app.controller('ReaUserMyInfosController', function($scope, $http, $stateParams, $location, $state, notifications) {

    $scope.rea_user = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetMyInfos = function() {
        $scope.error_get = null;
        $http({
            method: 'GET',
            url: '/api/ReaUser/get/myinfos'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.rea_user = response.data.data;
            }else{
                $scope.error_get = true;
            }
        }, function errorCallback(response) {
            $scope.error_get = true;
            console.log('error');
        });
    };



    $scope.GetGenres = function() {
        $http({
            method: 'GET',
            url: '/api/Genre/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.genres = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetCities = function() {
        $http({
            method: 'GET',
            url: '/api/City/get/country=local'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.cities = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.countries = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.schools = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetIdnTypes = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.idn_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetLanguages = function() {
        $http({
            method: 'GET',
            url: '/api/Language/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.languages = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetMyInfos();
    $scope.GetGenres();
    $scope.GetCities();
    $scope.GetCountries();
    $scope.GetSchools();
    $scope.GetIdnTypes();
    $scope.GetLanguages();



    $scope.pre_save = function() {
        return true;
    };

    $scope.save = function(back_location) {
        $scope.success_update = null;
        $scope.error_update = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { rea_user: $scope.rea_user },
                url: '/api/ReaUser/save_myinfos'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $scope.success_update = true;
                    $scope.rea_user.newpwd = null;
                    $scope.rea_user.newpwd2 = null;
                } else {
                    $scope.success_update = null;
                    $scope.error_update = true;
                    $scope.error_update_message = response.data.message;
                }

                notifications.get_notifications();
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.remove_connexion = function(social) {
        $http({
            method: 'POST',
            data: { social: social },
            url: '/api/ReaUser/remove_connexion'
        }).then(function successCallback(response) {
            $state.reload();
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.send_active_email = function() {
        $scope.success_send_active_email = null;
        $http({
            method: 'POST',
            url: '/api/ReaUser/send_active_email'
        }).then(function successCallback(response) {
            $scope.success_send_active_email = true;
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.send_active_mobile = function() {
        $scope.success_send_active_mobile = null;
        $scope.errors_send_active_mobile = null;
        $http({
            method: 'POST',
            data: { mobile: $scope.rea_user.mobile },
            url: '/api/ReaUser/send_active_mobile'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.success_send_active_mobile = true;
            } else {
                $scope.errors_send_active_mobile = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.submit_activation_mobile_code = function() {
        $scope.success_submit_activation_mobile_code = null;
        $scope.error_submit_activation_mobile_code = null;
        $http({
            method: 'POST',
            data: { 'mobile_activation_code': $scope.mobile_activation_code },
            url: '/api/ReaUser/submit_activation_mobile_code'
        }).then(function successCallback(response) {
            if (response.data.status) {
                notifications.get_notifications();
                $scope.success_submit_activation_mobile_code = true;
            } else {
                $scope.error_submit_activation_mobile_code = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


});