'use strict';

app.controller('NewPasswordFormController', function($scope, $http, vcRecaptchaService) {

    $scope.rea_user = {};
    $scope.failed_change_password = null;
    $scope.failed_message = null;
    $scope.success_change_password = null;
    
    

    $scope.change_password = function(){

        $scope.errors = null;
        $scope.success_change_password = null;
        $scope.failed_change_password = null;

        $http({
            method: 'POST',
            data: {rea_user: $scope.rea_user},
            url: '/api/ReaUser/change_password'
        }).then(function successCallback(response) {
            if(response.data.data.success===true){
                $scope.success_change_password = true;
            }else{
                $scope.failed_change_password = true;
                $scope.errors = response.data.data.errors;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

});