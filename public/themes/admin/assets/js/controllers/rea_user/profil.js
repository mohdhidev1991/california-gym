'use strict';

app.controller('ReaUserProfilController', function($scope, $http, $stateParams, $location) {

    $scope.rea_user = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetMyProfil = function() {
        $http({
            method: 'GET',
            url: '/api/ReaUser/me'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.rea_user = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCities = function() {
        $http({
            method: 'GET',
            url: '/api/City/get'
        }).then(function successCallback(response) {
            if (response.data.data.cities) {
                $scope.cities = response.data.data.cities;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.data.countries) {
                $scope.countries = response.data.data.countries;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetIdnTypes = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get'
        }).then(function successCallback(response) {
            if (response.data.data.idn_types) {
                $scope.idn_types = response.data.data.idn_types;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetLanguages = function() {
        $http({
            method: 'GET',
            url: '/api/Language/get'
        }).then(function successCallback(response) {
            if (response.data.data.languages) {
                $scope.languages = response.data.data.languages;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetReaUser();
    }else{
        $scope.rea_user = {};
        $scope.rea_user.new = true;
    }
    $scope.GetCities();
    $scope.GetCountries();
    $scope.GetSchools();
    $scope.GetIdnTypes();
    $scope.GetLanguages();


    $scope.pwCheck = function(){
        var valide = ($scope.rea_user.newpwd===$scope.rea_user.newpwd2);
        return valide;
    };


    $scope.pre_save = function(){
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {rea_user: $scope.rea_user},
                url: '/api/ReaUser/save_profil'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/rea_users");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.rea_user);
            $http({
                method: 'POST',
                data: {rea_user: $scope.rea_user},
                url: '/api/ReaUser/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/rea_user/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});


