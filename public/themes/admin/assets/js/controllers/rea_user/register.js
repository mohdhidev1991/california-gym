'use strict';

app.controller('RegisterFormController', function($scope, $http, vcRecaptchaService) {

    $scope.rea_user = {};
    $scope.failed_register = null;
    $scope.widgetId = null;
    $scope.failed_recaptcha = null;
    $scope.success_registration = null;
    $scope.model = {
        key: '6LebaCATAAAAAA_s4YzMoLezmhGQYOhUdWpsn6ZU'
    };
    $scope.setResponse = function (response) {
        console.info('response %s', response);
        $scope.valide_recaptcha = response;
        $scope.failed_recaptcha = null;
    };

    $scope.setWidgetId = function (widgetId) {
        console.info('Created widget ID: %s', widgetId);
        $scope.widgetId = widgetId;
    };

    $scope.cbExpiration = function() {
        console.info('expired google recaptcha');
        vcRecaptchaService.reload($scope.widgetId);
        $scope.valide_recaptcha = null;
     };

    $scope.pwCheck = function(){
        var valide = ($scope.rea_user.pwd===$scope.rea_user.pwd2);
        return valide;
    };

    $scope.pre_save = function(){
        return true;
    };

    $scope.register = function(){
        $scope.failed_register = null;
        $scope.failed_recaptcha = null;
        $scope.errors = null;
        $scope.success_registration = null;

        if($scope.valide_recaptcha){
            $http({
                method: 'POST',
                data: {rea_user: $scope.rea_user},
                url: '/api/webregister'
            }).then(function successCallback(response) {
                if(response.data.data.success===true){
                    //window.location = '/#/myinfos';
                    $scope.success_registration = true;
                }else{
                    $scope.failed_register = true;
                    $scope.failed_message = response.data.message;
                    $scope.errors = response.data.data.errors;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }else{
            $scope.failed_recaptcha = true;
            console.log('failed_recaptcha');
        }
    };

});