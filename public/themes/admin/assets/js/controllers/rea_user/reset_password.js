'use strict';

//angular.module('app', ['vcRecaptcha']);
app.controller('ResetPasswordFormController', function ($scope, vcRecaptchaService, $http) {

    $scope.valide_recaptcha = null;
    $scope.widgetId = null;
    $scope.failed_recaptcha = null;
    $scope.model = {
        key: '6LebaCATAAAAAA_s4YzMoLezmhGQYOhUdWpsn6ZU'
    };

    $scope.setResponse = function (response) {
        console.info('response %s', response);
        $scope.valide_recaptcha = response;
        $scope.failed_recaptcha = null;
    };

    $scope.setWidgetId = function (widgetId) {
        console.info('Created widget ID: %s', widgetId);
        $scope.widgetId = widgetId;
    };

    $scope.cbExpiration = function() {
        console.info('expired google recaptcha');
        vcRecaptchaService.reload($scope.widgetId);
        $scope.valide_recaptcha = null;
     };

    $scope.reset_password = function () {
        
        $scope.errors = null;
        $scope.failed_recaptcha = null;
        $scope.success_send_reset_password = null;

        if($scope.valide_recaptcha){
            $http({
                method: 'POST',
                data: {rea_user: $scope.rea_user},
                url: '/api/reset_password'
            }).then(function successCallback(response) {
                if(response.data.data.success){
                    $scope.success_send_reset_password = true;
                }else{
                    $scope.failed_send_reset = true;
                    $scope.errors = response.data.data.errors;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }else{
            $scope.failed_recaptcha = true;
            console.log('failed_recaptcha');
        }
        
    };
});
