'use strict';

app.controller('RelshipAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.relships = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetRelships = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Relship/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.relships) {
                $scope.relships = response.data.data.relships;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetRelships();
    };



    $scope.delete_relship = function(relship_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteRelshipInstanceCtrl',
            resolve: {
                relship_id: function() {
                    return relship_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetRelships();
        }, function() {
            
        });
    };


    $scope.GetRelships();


});



app.controller('ModalDeleteRelshipInstanceCtrl', function($scope, $uibModalInstance, $http, relship_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {relship_id : relship_id},
            url: '/api/Relship/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


