'use strict';

app.controller('RelshipFormController', function($scope, $http, $stateParams, $location) {

    $scope.relship = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetRelship = function() {
        $http({
            method: 'GET',
            url: '/api/Relship/get/relship_id='+$stateParams.relship_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.relship = response.data.data;
                if($scope.relship.active && $scope.relship.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };




    if($scope.action==="edit"){
        $scope.GetRelship();
    }else{
        $scope.relship = {};
        $scope.relship.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.relship.active = "Y";
        }else{
            $scope.relship.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {relship: $scope.relship},
                url: '/api/Relship/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/relships");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.relship);
            $http({
                method: 'POST',
                data: {relship: $scope.relship},
                url: '/api/Relship/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/relship/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



