'use strict';

app.controller('RemarkAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.remarks = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetRemarks = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Remark/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.remarks) {
                $scope.remarks = response.data.data.remarks;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetRemarks();
    };



    $scope.delete_remark = function(remark_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteRemarkInstanceCtrl',
            resolve: {
                remark_id: function() {
                    return remark_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetRemarks();
        }, function() {
            
        });
    };


    $scope.GetRemarks();


});



app.controller('ModalDeleteRemarkInstanceCtrl', function($scope, $uibModalInstance, $http, remark_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {remark_id : remark_id},
            url: '/api/Remark/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


