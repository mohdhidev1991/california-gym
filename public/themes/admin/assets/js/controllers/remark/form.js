'use strict';

app.controller('RemarkFormController', function($scope, $http, $stateParams, $location) {

    $scope.remark = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetRemark = function() {
        $http({
            method: 'GET',
            url: '/api/Remark/get/remark_id='+$stateParams.remark_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.remark = response.data.data;
                if($scope.remark.active && $scope.remark.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetGremarks = function() {
        $http({
            method: 'GET',
            url: '/api/Gremark/get'
        }).then(function successCallback(response) {
            if (response.data.data.gremarks) {
                $scope.gremarks = response.data.data.gremarks;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetRemark();
    }else{
        $scope.remark = {};
        $scope.remark.new = true;
    }
    $scope.GetGremarks();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.remark.active = "Y";
        }else{
            $scope.remark.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {remark: $scope.remark},
                url: '/api/Remark/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/remarks");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.remark);
            $http({
                method: 'POST',
                data: {remark: $scope.remark},
                url: '/api/Remark/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/remark/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



