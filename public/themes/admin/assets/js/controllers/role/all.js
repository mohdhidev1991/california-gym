'use strict';

app.controller('RoleAllController', function($scope, $http, $uibModal, $stateParams, $location, Notification, $filter) {

    $scope.roles = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100];

    $scope.GetRoles = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Role/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.roles = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetRoles();
    };


    $scope.delete = function(role_id) {

        var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'ModalConfirmDelete.html',
        controller: 'ModalDeleteRoleInstanceCtrl',
        resolve: {
            role_id: function() {
                return role_id;
            }
        }
        });
        modalInstance.result.then(function() {
            $scope.GetRoles();
        }, function() {
            
        });
    };

    $scope.GetRoles();
});

app.controller('ModalDeleteRoleInstanceCtrl', function($scope, $uibModalInstance, $http, role_id, $location, Notification, $filter) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {role_id : role_id},
            url: '/api/Role/delete'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


