'use strict';

app.controller('RoleFormController', function($scope, $http, $stateParams, $location, $state, Notification, $filter) {

    $scope.role = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.failed_get_data = false;
    $scope.roles = null;
    
    $scope.GetRole = function(){
        $http({
            method: 'GET',
            url: '/api/Role/get/role_id=' + $stateParams.role_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.role = response.data.data;
                if ($scope.role.active && $scope.role.active === "Y") {
                    $scope.active_switch = true;
                }
            }else{
                $scope.failed_get_data = true;
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    
    

    $scope.GetPermissions = function() {
        $http({
            method: 'GET',
            url: '/api/Permission/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.permissions = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if ($scope.action === "edit") {
        $scope.GetRole();

    } else{

        $scope.role = {};
        $scope.role.new = true;
        $scope.role.active = "Y";
        $scope.role.permissions = [];
        $scope.active_switch = true;
    }


    $scope.GetPermissions();


   
    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };

    $scope.pre_save = function() {
        if ($scope.active_switch === true) {
            $scope.role.active = "Y";
        } else {
            $scope.role.active = "N";
        }

        $scope.role.permissions_mfk = null;
        if($scope.role.permissions){
            $scope.role.permissions_mfk = ','+$scope.role.permissions.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location) {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { role: $scope.role },
                url: '/api/Role/save'
            }).then(function successCallback(response) {
                if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }
                if (response.data.status && back_location) {
                    Notification.success({ message: $filter('translate')('Global.DataSavedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('role.all');
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function() {
        $scope.errors_form = null;
        $scope.success_form = null;
        if ($scope.pre_save()) {
            $http({
                method: 'POST',
                data: { role: $scope.role },
                url: '/api/Role/save'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    Notification.success({ message: $filter('translate')('Global.DataAddedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('role.edit', { role_id: response.data.data });
                }else{
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});
