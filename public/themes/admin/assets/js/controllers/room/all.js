'use strict';

app.controller('RoomAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.rooms = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10, 50, 100, 500];



    $scope.GetClub = function() {
        $http({
            method: 'GET',
            url: '/api/Club/get/club_id='+$stateParams.club_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.club = response.data.data;
                $scope.GetRooms();
            }else{
                $scope.failed_club = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetRooms = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;
        $scope.Params += ',club_id='+$stateParams.club_id;

        $http({
            method: 'GET',
            url: '/api/Room/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.rooms = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetRooms();
    };



    $scope.delete = function(room_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteRoomInstanceCtrl',
            resolve: {
                room_id: function() {
                    return room_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetRooms();
        }, function() {
            
        });
    };


    $scope.GetClub();
});



app.controller('ModalDeleteRoomInstanceCtrl', function($scope, $uibModalInstance, $http, room_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {room_id : room_id},
            url: '/api/Room/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

