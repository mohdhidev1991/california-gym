'use strict';

app.controller('RoomFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.room = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.club_id = $stateParams.club_id;
    $scope.clubs  = null;


    $scope.GetRoom = function() {
        $http({
            method: 'GET',
            url: '/api/Room/get/room_id='+$stateParams.room_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.room = response.data.data;
                if($scope.room.active && $scope.room.active==="Y"){
                    $scope.active_switch = true;
                }
            }else{
                $scope.failed_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetClub = function() {
        $http({
            method: 'GET',
            url: '/api/Club/get/club_id='+$stateParams.club_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.club = response.data.data;
            }else{
                $scope.failed_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    
    $scope.GetClubs = function() {
        $http({
            method: 'GET',
            url: '/api/Club/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.clubs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    
    $scope.GetWdays = function() {
        $http({
            method: 'GET',
            url: '/api/Wday/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.wdays = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetWdays();



    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };

    

    if($scope.action==="edit"){
        $scope.GetRoom();
    }else{
        $scope.room = {};
        $scope.room.new = true;
        $scope.room.active = "Y";
        $scope.active_switch = true;
        $scope.room.club_id = parseInt($stateParams.club_id);
        $scope.room.disponibilities = [];
    }
    $scope.GetClub();
    $scope.GetClubs();

    $scope.pre_save = function(){

        if($scope.active_switch===true){
            $scope.room.active = "Y";
        }else{
            $scope.room.active = "N";
        }

        $scope.room.disponibility = null;
        if($scope.room.disponibilities){
            $scope.room.disponibility = ','+$scope.room.disponibilities.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location){
        $scope.errors_form = null;
        $scope.success_form = null;
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {room: $scope.room},
                url: '/api/Room/save'
            }).then(function successCallback(response) {
                if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }
                if (response.data.status && back_location) {
                    $state.go('setting.club.rooms', {club_id: $scope.room.club_id});
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        $scope.errors_form = null;
        $scope.success_form = null;
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {room: $scope.room},
                url: '/api/Room/save'
            }).then(function successCallback(response) {
                if(response.data.status){
                    $state.go('setting.club.room.edit', {club_id: $scope.room.club_id, room_id: response.data.data});
                }else{
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



