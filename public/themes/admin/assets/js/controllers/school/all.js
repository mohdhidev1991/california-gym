'use strict';

app.controller('SchoolAllController', function($scope, $http, $uibModal, $stateParams, $location, $window) {

    $scope.filter = {};
    $scope.schools = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];

    $scope.order_by = 'id';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetSchools();
    };


    $scope.GetSchools = function() {
        if($scope.loading) return;

        $scope.empty_results = null;

        $scope.Params = 'active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'group_num',
            'genre_id',
            'lang_id',
            'name_or_id',
            'levels_template_id',
            'courses_template_id',
            'country_id',
            'city_id',
            'status',
            'school_type_id',
            'period_id',
            'school_level_id',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });


        $http({
            method: 'GET',
            url: '/api/School/get/' + $scope.Params
        }).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.data.total;
                }
            }else{
                $scope.schools = null;
                $scope.empty_results = true;
            }
        }, function errorCallback(response) {
            $scope.schools = null;
            $scope.empty_results = true;
            console.log('error');
        });
    };


    $scope.export_xls = function() {
        $window.location.href = '/api/School/get/export=xls,join=all,' +$scope.Params;
    };



    $scope.GetSchoolTypes = function() {
        $scope.school_types = null;
        $http({
            method: 'GET',
            url: '/api/SchoolType/get'
        }).then(function successCallback(response) {
            if (response.data.data.school_types.length) {
                $scope.school_types = response.data.data.school_types;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetSchoolTypes();

    $scope.GetGenre = function() {
        $scope.genres = null;
        $http({
            method: 'GET',
            url: '/api/Genre/get'
        }).then(function successCallback(response) {
            if (response.data.data.genres.length) {
                $scope.genres = response.data.data.genres;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetGenre();


    $scope.GetLanguages = function() {
        $scope.genres = null;
        $http({
            method: 'GET',
            url: '/api/Language/get'
        }).then(function successCallback(response) {
            if (response.data.data.languages.length) {
                $scope.languages = response.data.data.languages;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetLanguages();


    $scope.GetLevelsTemplates = function() {
        $scope.genres = null;
        $http({
            method: 'GET',
            url: '/api/LevelsTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.levels_templates) {
                $scope.levels_templates = response.data.data.levels_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetLevelsTemplates();

    $scope.GetCoursesTemplates = function() {
        $scope.genres = null;
        $http({
            method: 'GET',
            url: '/api/CoursesTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.courses_templates) {
                $scope.courses_templates = response.data.data.courses_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCoursesTemplates();

    $scope.GetCountries = function() {
        $scope.countries = null;
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.data.countries.length) {
                $scope.countries = response.data.data.countries;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetCountries();


    $scope.GetCities = function() {
        $scope.cities = null;
        if(!$scope.filter.country_id) return;

        $http({
            method: 'GET',
            url: '/api/City/get/country_id='+$scope.filter.country_id
        }).then(function successCallback(response) {
            if (response.data.data.cities.length) {
                $scope.cities = response.data.data.cities;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetPeriods = function() {
        $scope.periods = null;
        $http({
            method: 'GET',
            url: '/api/Period/get'
        }).then(function successCallback(response) {
            if (response.data.data.periods.length) {
                $scope.periods = response.data.data.periods;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetPeriods();


    $scope.GetSchoolLevels = function() {
        $scope.periods = null;
        $http({
            method: 'GET',
            url: '/api/SchoolLevel/get'
        }).then(function successCallback(response) {
            if (response.data.data.school_levels.length) {
                $scope.school_levels = response.data.data.school_levels;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetSchoolLevels();


    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetSchools();
    };



    



    $scope.delete_school = function(school_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolInstanceCtrl',
            resolve: {
                school_id: function() {
                    return school_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchools();
        }, function() {

        });
    };


    $scope.GetSchools();


});



app.controller('ModalDeleteSchoolInstanceCtrl', function($scope, $uibModalInstance, $http, school_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { school_id: school_id },
            url: '/api/School/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});