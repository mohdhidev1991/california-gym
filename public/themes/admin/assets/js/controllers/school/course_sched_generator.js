'use strict';

app.controller('SchoolCourseSchedGeneratorController', function($scope, $http, $stateParams, $location) {
    $scope.selected_school_id = null;
    $scope.selected_school_year_id = null;
    $scope.selected_level_class_id = null;
    $scope.selected_school_class_id = null;
    $scope.schools = null;
    $scope.school_years = null;
    $scope.level_classes = null;
    $scope.week_templates = null;
    $scope.school_classes = null;
    $scope.course_sched_items = null;
    $scope.submited_find_course_sched_items = false;
    $scope.new_template = false;
    $scope.total_session_order = 0;
    $scope.total_days = 7;
    $scope.days = [];


    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get/current_user_school=true'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolYears = function() {
        if($scope.selected_school_id){
            $http({
                method: 'GET',
                url: '/api/SchoolYear/get/school_id='+$scope.selected_school_id
            }).then(function successCallback(response) {
                if (response.data.data.school_years.length) {
                    $scope.school_years = response.data.data.school_years;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.GetLevelClasses = function() {
        if($scope.selected_school_year_id){
            $http({
                method: 'GET',
                url: '/api/LevelClass/get/school_year_id='+$scope.selected_school_year_id
            }).then(function successCallback(response) {
                if (response.data.data.level_classes.length) {
                    $scope.level_classes = response.data.data.level_classes;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };

    $scope.GetSchoolClasses = function() {
        if($scope.selected_school_year_id && $scope.selected_level_class_id){
            $http({
                method: 'GET',
                url: '/api/SchoolClass/get/level_class_id='+$scope.selected_level_class_id+',school_year_id='+$scope.selected_school_year_id
            }).then(function successCallback(response) {
                if (response.data.data.school_classes.length) {
                    $scope.school_classes = response.data.data.school_classes;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.GetCourseSchedItems = function() {
        $scope.course_sched_items = null;
        $scope.new_template = false;
        if($scope.selected_school_year_id && $scope.selected_level_class_id){
            $http({
                method: 'GET',
                url: '/api/CourseSchedItem/get/school_year_id='+$scope.selected_school_year_id+',level_class_id='+$scope.selected_level_class_id+',symbol='+$scope.selected_school_class_id
            }).then(function successCallback(response) {
                $scope.submited_find_course_sched_items = true;
                if (response.data.data.course_sched_items.length) {
                    $scope.course_sched_items = response.data.data.course_sched_items;
                    $scope.init_table_course_sched_items();
                }else{
                    $scope.new_template = true;
                    $scope.GetWeekTemplates();
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.GetCourses = function() {
        $scope.courses = null;
        $http({
            method: 'GET',
            url: '/api/Course/get'
        }).then(function successCallback(response) {
            if (response.data.data.courses.length) {
                $scope.courses = response.data.data.courses;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetWeekTemplates = function() {
        $scope.week_templates = null;
        $http({
            method: 'GET',
            url: '/api/WeekTemplate/get/school_year_id='+$scope.selected_school_year_id+',level_class_id='+$scope.selected_level_class_id
        }).then(function successCallback(response) {
            if (response.data.data.week_templates.length) {
                $scope.week_templates = response.data.data.week_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.changed_selected_school_id = function() {
        $scope.school_years = null;
        $scope.level_classes = false;
        $scope.school_classes = false;
        $scope.submited_find_course_sched_items = false;
        $scope.course_sched_items = null;
        $scope.new_template = false;
        $scope.GetSchoolYears();
    };
    $scope.changed_selected_school_year_id = function() {
        $scope.level_classes = false;
        $scope.school_classes = false;
        $scope.submited_find_course_sched_items = false;
        $scope.course_sched_items = null;
        $scope.new_template = false;
        $scope.GetLevelClasses();
    };
    $scope.changed_selected_level_class_id = function() {
        $scope.school_classes = false;
        $scope.submited_find_course_sched_items = false;
        $scope.course_sched_items = null;
        $scope.new_template = false;
        $scope.GetSchoolClasses();
    };

    $scope.find_course_sched_items = function() {
        $scope.submited_find_course_sched_items = false;
        $scope.course_sched_items = null;
        $scope.new_template = false;
        $scope.success_update = false;
        $scope.GetCourseSchedItems();
    };


    $scope.init_table_course_sched_items = function() {
        $scope.GetCourses();

        $scope.days = [];
        $scope.sessions = [];
        $scope.data_course_sched_items = [];
        angular.forEach($scope.course_sched_items, function(item, key){
            item.symbol = parseInt(item.symbol);
            if(!$scope.days[item.wday_id]){
                $scope.days[item.wday_id] = [];
            }
            if(!$scope.sessions[item.session_order]){
                $scope.sessions[item.session_order] = [];
            }
            $scope.days[item.wday_id][item.session_order] = item;
            $scope.sessions[item.session_order][item.wday_id] = item;
            $scope.data_course_sched_items[item.id] = item;
        });
        $scope.total_session_order = $scope.sessions.length;
    };


    $scope.save = function() {

        var data_to_save = [];
        angular.forEach($scope.sessions,function(session, key_session){
            angular.forEach(session,function(item_, key){
                data_to_save.push(item_);
            });
        });
        $scope.success_update = false;
        $http({
            method: 'POST',
            data: {course_sched_items: data_to_save},
            url: '/api/CourseSchedItem/multisave'
        }).then(function successCallback(response) {
            $scope.success_update = true;
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.creat_from_template = function() {
        $http({
            method: 'POST',
            data: {
                week_template_id: $scope.selected_week_template_id,
                school_year_id: $scope.selected_school_year_id,
                level_class_id: $scope.selected_level_class_id,
                school_class_id: $scope.selected_school_class_id
            },
            url: '/api/CourseSchedItem/CreatFromWeekTemplate'
        }).then(function successCallback(response) {
            $scope.find_course_sched_items();
        }, function errorCallback(response) {
            console.log('error');
        });
    };




    $scope.GetSchools();

});


