'use strict';

angular.module('app',['checklist-model']);

app.controller('SchoolEditController', function($scope, $http, $stateParams, $location) {

    $scope.school = null;
    $scope.genres = null;
    $scope.cities = null;
    $scope.school_types = null;
    $scope.school_levels = null;
    $scope.group_schools = null;
    $scope.periods = null;
    $scope.languages = null;
    $scope.levels_templates = null;
    $scope.courses_templates = null;
    $scope.courses_config_templates = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchool = function() {
        $http({
            method: 'GET',
            url: '/api/School/get/school_id='+$stateParams.school_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school = response.data.data;
                if($scope.school.active && $scope.school.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    $scope.GetGenre = function() {
        $http({
            method: 'GET',
            url: '/api/Genre/get'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.genres = response.data.data.genres;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetCities = function() {
        $http({
            method: 'GET',
            url: '/api/City/get'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.cities = response.data.data.cities;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolTypes = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolType/get'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_types = response.data.data.school_types;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolLevels = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolLevel/get'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_levels = response.data.data.school_levels;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetGroupSchools = function() {
        $scope.Params = 'school_type_id=2,exclude_id='+$stateParams.school_id;
        $http({
            method: 'GET',
            url: '/api/School/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.schools) {
                $scope.group_schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetPeriods = function() {
        $http({
            method: 'GET',
            url: '/api/Period/get'
        }).then(function successCallback(response) {
            if (response.data.data.periods) {
                $scope.periods = response.data.data.periods;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetLanguages = function() {
        $http({
            method: 'GET',
            url: '/api/Language/get'
        }).then(function successCallback(response) {
            if (response.data.data.languages) {
                $scope.languages = response.data.data.languages;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.LevelsTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/LevelsTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.levels_templates) {
                $scope.levels_templates = response.data.data.levels_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.CoursesTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/CoursesTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.courses_templates) {
                $scope.courses_templates = response.data.data.courses_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.CoursesConfigTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/CoursesConfigTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.courses_config_templates) {
                $scope.courses_config_templates = response.data.data.courses_config_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    if($scope.action==="edit"){
        $scope.GetSchool();
    }else{
        $scope.school = {};
        $scope.school.new = true;
        $scope.school.periods = [];
        $scope.school.school_levels = [];
    }
    $scope.GetGenre();
    $scope.GetCities();
    $scope.GetSchoolTypes();
    $scope.GetSchoolLevels();
    $scope.GetGroupSchools();
    $scope.GetPeriods();
    $scope.GetLanguages();
    $scope.LevelsTemplates();
    $scope.CoursesTemplates();
    $scope.CoursesConfigTemplates();



    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };

    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school.active = "Y";
        }else{
            $scope.school.active = "N";
        }

        $scope.school.school_level_mfk = null;
        if($scope.school.school_levels){
            $scope.school.school_level_mfk = ','+$scope.school.school_levels.join(',')+',';
        }

        $scope.school.period_mfk = null;
        if($scope.school.periods){
            $scope.school.period_mfk = ','+$scope.school.periods.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school: $scope.school},
                url: '/api/School/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/schools");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.school);
            $http({
                method: 'POST',
                data: {school: $scope.school},
                url: '/api/School/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/school/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



