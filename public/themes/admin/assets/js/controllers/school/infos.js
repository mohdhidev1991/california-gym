'use strict';

app.controller('SchoolInfosController', function($scope, $http, $stateParams, $location) {

    $scope.school = null;

    $scope.GetSchoolInfos = function() {
        $http({
            method: 'GET',
            url: '/api/School/get/school_id=' + $stateParams.school_id + ',fullinfos'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school = response.data.data;
            } else {
                $scope.can_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolInfos();

});