'use strict';

app.controller('RegisterSchoolFormController', function ($scope, $http, vcRecaptchaService) {

    $scope.response = null;
    $scope.widgetId = null;
    $scope.model = {
        key: '6LebaCATAAAAAA_s4YzMoLezmhGQYOhUdWpsn6ZU'
    };

    $scope.setResponse = function (response) {
        console.info('valide google recaptcha');
        $scope.valide_captcha = response;
        $scope.failed_recaptcha = null;
    };

    $scope.setWidgetId = function (widgetId) {
        $scope.widgetId = widgetId;
    };

    $scope.cbExpiration = function() {
        console.info('expired google recaptcha');
        vcRecaptchaService.reload($scope.widgetId);
        $scope.valide_captcha = null;
        $scope.failed_recaptcha = true;
     };

    $scope.register_school = function () {
        $scope.failed_school_registration = null;
        $scope.success_school_registration = null;
        $scope.errors = null;
        $scope.failed_recaptcha = null;

        if ( $scope.valide_captcha) {
            $http({
                method: 'POST',
                data: {school_registration: $scope.school_registration},
                url: '/api/SchoolRegistration/submit'
            }).then(function successCallback(response) {
                if(response.data.data.success===true){
                    $scope.success_school_registration = true;
                }else{
                    $scope.failed_school_registration = true;
                    $scope.errors = response.data.data.errors;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        } else {
            $scope.failed_recaptcha = true;
            console.log('Failed validation');
        }
    };
});
