'use strict';

app.controller('SchoolClassAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.school_classes = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetSchoolClasses = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolClass/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_classes) {
                $scope.school_classes = response.data.data.school_classes;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolClasses();
    };



    $scope.delete_school_class = function(school_class_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolClassInstanceCtrl',
            resolve: {
                school_class_id: function() {
                    return school_class_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolClasses();
        }, function() {
            
        });
    };


    $scope.GetSchoolClasses();


});



app.controller('ModalDeleteSchoolClassInstanceCtrl', function($scope, $uibModalInstance, $http, school_class_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_class_id : school_class_id},
            url: '/api/SchoolClass/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

