'use strict';

app.controller('SchoolClassFormController', function($scope, $http, $stateParams, $location) {

    $scope.school_class = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.level_classes = null;
    $scope.school_years = null;
    $scope.rooms = null;


    $scope.GetSchoolClass = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolClass/get/school_class_id='+$stateParams.school_class_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_class = response.data.data;
                if($scope.school_class.active && $scope.school_class.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };





    $scope.GetLevelClasses = function() {
        $http({
            method: 'GET',
            url: '/api/LevelClass/get'
        }).then(function successCallback(response) {
            if (response.data.data.level_classes.length) {
                $scope.level_classes = response.data.data.level_classes;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolYears = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolYear/get'
        }).then(function successCallback(response) {
            if (response.data.data.school_years.length) {
                $scope.school_years = response.data.data.school_years;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetRooms = function() {
        $http({
            method: 'GET',
            url: '/api/Room/get'
        }).then(function successCallback(response) {
            if (response.data.data.rooms.length) {
                $scope.rooms = response.data.data.rooms;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSchoolClass();
    }else{
        $scope.school_class = {};
        $scope.school_class.new = true;
    }
    $scope.GetLevelClasses();
    $scope.GetSchoolYears();
    $scope.GetRooms();

    $scope.pre_save = function(){

        if($scope.active_switch===true){
            $scope.school_class.active = "Y";
        }else{
            $scope.school_class.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_class: $scope.school_class},
                url: '/api/SchoolClass/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/school_classes");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_class: $scope.school_class},
                url: '/api/SchoolClass/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/school_class/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



