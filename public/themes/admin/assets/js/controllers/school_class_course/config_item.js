'use strict';

app.controller('SchoolClassCourseConfigItemController', function($scope, $http, $stateParams, $location) {

    $scope.GetMySchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get_my_schools'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolYears = function() {
        $scope.school_years = null;
        if($scope.school_id){
            $http({
                method: 'GET',
                url: '/api/SchoolYear/get/school_id='+$scope.school_id
            }).then(function successCallback(response) {
                if (response.data.data.school_years.length) {
                    $scope.school_years = response.data.data.school_years;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.GetSchoolLevels = function() {
        $scope.school_levels = null;
        if($scope.school_id){
            $http({
                method: 'GET',
                url: '/api/SchoolLevel/get/school_id='+$scope.school_id
            }).then(function successCallback(response) {
                if (response.data.data.school_levels.length) {
                    $scope.school_levels = response.data.data.school_levels;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.GetLevelClasses = function() {
        $scope.level_classes = null;
        if($scope.school_level_id){
            $http({
                method: 'GET',
                url: '/api/LevelClass/get/school_level_id='+$scope.school_level_id
            }).then(function successCallback(response) {
                if (response.data.data.level_classes.length) {
                    $scope.level_classes = response.data.data.level_classes;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };



    $scope.GetSchoolEmployees = function() {
        $scope.profs = null;
        if($scope.school_id){
            $http({
                method: 'GET',
                url: '/api/SchoolEmployee/get/school_id='+$scope.school_id+',school_job=teacher'
            }).then(function successCallback(response) {
                if (response.data.data.school_employees.length) {
                    $scope.profs = response.data.data.school_employees;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };



    $scope.get_school_classes_courses = function() {
        $scope.school_classes_courses = [];
        $scope.success_get = null;
        $scope.error_get = null;
        $http({
            method: 'POST',
            data: {school_id: $scope.school_id, school_year_id: $scope.school_year_id, level_class_id: $scope.level_class_id},
            url: '/api/SchoolClassCourse/generate'
        }).then(function successCallback(response) {
            if (response.data.data.school_classes_courses.length) {
                $scope.prepare_school_classes_courses(response.data.data.school_classes_courses);
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.clean = function(actual) {
      var newArray = new Array();
      for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
          newArray.push(actual[i]);
        }
      }
      return newArray;
    };

    $scope.reset_datas = function(){
        $scope.school_classes_courses = [];
        $scope.school_classes_courses_keys = [];
        $scope.symboles = [];
        $scope.symboles_keys = [];
        $scope.courses = [];
        $scope.courses_keys = [];
    };

    $scope.prepare_school_classes_courses = function(school_classes_courses) {
        $scope.reset_datas();

        angular.forEach(school_classes_courses, function(school_class_course, key) {

            if($scope.symboles_keys.indexOf(school_class_course.symbol) === -1) {
                var __item_symbol = {
                    symbol: school_class_course.symbol,
                    level_class_id: school_class_course.level_class_id,
                    level_class_name_ar: school_class_course.level_class_name_ar,
                    level_class_name_en: school_class_course.level_class_name_en,
                    school_year_id: school_class_course.school_year_id
                };
                $scope.symboles[school_class_course.symbol] = __item_symbol;
                $scope.symboles_keys.push(school_class_course.symbol);
            }


            if($scope.courses_keys.indexOf(school_class_course.course_id) === -1) {
                var __item_course = {
                    course_id: school_class_course.course_id,
                    course_name_ar: school_class_course.course_name_ar,
                    course_name_en: school_class_course.course_name_en,
                    level_class_id: school_class_course.level_class_id,
                    level_class_name_ar: school_class_course.level_class_name_ar,
                    level_class_name_en: school_class_course.level_class_name_en,
                    school_year_id: school_class_course.school_year_id
                };
                $scope.courses[school_class_course.course_id] = __item_course;
                $scope.courses_keys.push(school_class_course.course_id);
            }

            if(!$scope.school_classes_courses[school_class_course.symbol]){
                $scope.school_classes_courses[school_class_course.symbol] = [];
            }
            var profs = [];
            angular.forEach($scope.profs, function(prof, key_prof) {
                if(prof.courses.indexOf(school_class_course.course_id) !== -1) {
                    profs.push(angular.copy(prof));
                }
            });
            school_class_course.profs = angular.copy(profs);
            if( !$scope.school_classes_courses[school_class_course.symbol] ){
                $scope.school_classes_courses[school_class_course.symbol] = [];
            }
            $scope.school_classes_courses[school_class_course.symbol][school_class_course.course_id] = angular.copy(school_class_course);
        });
    };



    $scope.save = function() {
        var data = [];
        angular.forEach($scope.symboles_keys, function(symbol, key_symbol) {
            angular.forEach($scope.school_classes_courses[symbol], function(school_classe_course, key) {
                data.push(school_classe_course);
            });
        });
        $scope.success_save = null;
        $scope.error_save = null;
        $http({
            method: 'POST',
            data: {school_classes_courses: data},
            url: '/api/SchoolClassCourse/multisave'
        }).then(function successCallback(response) {
            console.log(response);
            $scope.success_save = true;
        }, function errorCallback(response) {
            $scope.error_save = true;
            console.log('error');
        });
    };


    $scope.GetMySchools();
    
});


