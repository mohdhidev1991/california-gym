'use strict';

app.controller('SchoolEmployeeAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.school_employees = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100,500,999];

    $scope.GetSchoolEmployees = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolEmployee/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.school_employees = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolEmployees();
    };



    $scope.delete_school_employee = function(school_employee_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolEmployeeInstanceCtrl',
            resolve: {
                school_employee_id: function() {
                    return school_employee_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolEmployees();
        }, function() {
            
        });
    };


    $scope.GetSchoolEmployees();


});



app.controller('ModalDeleteSchoolEmployeeInstanceCtrl', function($scope, $uibModalInstance, $http, school_employee_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_employee_id : school_employee_id},
            url: '/api/SchoolEmployee/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


