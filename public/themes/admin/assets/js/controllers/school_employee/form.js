'use strict';

app.controller('SchoolEmployeeFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.school_employee = null;
    $scope.active_switch = false;
    $scope.valide_email_switch = false;
    $scope.valide_mobile_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchoolEmployee = function() {
        $scope.error_get = null;
        $http({
            method: 'GET',
            url: '/api/SchoolEmployee/get/school_employee_id='+$stateParams.school_employee_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_employee = response.data.data;
                if($scope.school_employee.active && $scope.school_employee.active==="Y"){
                    $scope.active_switch = true;
                }
                if($scope.school_employee.valide_email && $scope.school_employee.valide_email==="Y"){
                    $scope.valide_email_switch = true;
                }
                if($scope.school_employee.valide_mobile && $scope.school_employee.valide_mobile==="Y"){
                    $scope.valide_mobile_switch = true;
                }
                if($scope.school_employee.country_id){
                    $scope.GetCities();
                }
            }else{
                $scope.error_get = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCities = function() {
        if(!$scope.school_employee.country_id){
            $scope.cities = null;
            return;
        }
        $http({
            method: 'GET',
            url: '/api/City/get/country_id='+$scope.school_employee.country_id
        }).then(function successCallback(response) {
            if (response.data.data.cities) {
                $scope.cities = response.data.data.cities;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.data.countries) {
                $scope.countries = response.data.data.countries;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetGenres = function() {
        $scope.genres = null;
        $http({
            method: 'GET',
            url: '/api/Genre/get'
        }).then(function successCallback(response) {
            if (response.data.data.genres) {
                $scope.genres = response.data.data.genres;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    




    $scope.GetSchoolDepartments = function() {
        $scope.sdepartments = null;
        $http({
            method: 'GET',
            url: '/api/Sdepartment/get/current_school'
        }).then(function successCallback(response) {
            if (response.data.data.sdepartments) {
                $scope.sdepartments = response.data.data.sdepartments;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.check_selected_option = function(id, $model){
        if(!$model) return false;
        return $model.indexOf( id ) !== -1;
    };
    $scope.toggle_checkbox_option = function(id, $model){
        var index_to_remove = $model.indexOf( id );
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        }else{
            $model.push(id);
        }
    };


    $scope.GetCourses = function() {
        $scope.courses = null;
        $http({
            method: 'GET',
            url: '/api/Course/get/'
        }).then(function successCallback(response) {
            if (response.data.data.courses) {
                $scope.courses = response.data.data.courses;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetSchoolJobs = function() {
        $scope.school_jobs = null;
        $http({
            method: 'GET',
            url: '/api/SchoolJob/get/'
        }).then(function successCallback(response) {
            if (response.data.data.school_jobs) {
                $scope.school_jobs = response.data.data.school_jobs;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetWdays = function() {
        $scope.wdays = null;
        $http({
            method: 'GET',
            url: '/api/Wday/get/'
        }).then(function successCallback(response) {
            if (response.data.data.wdays) {
                $scope.wdays = response.data.data.wdays;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetIdnTypes = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get'
        }).then(function successCallback(response) {
            if (response.data.data.idn_types) {
                $scope.idn_types = response.data.data.idn_types;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetLanguages = function() {
        $http({
            method: 'GET',
            url: '/api/Language/get'
        }).then(function successCallback(response) {
            if (response.data.data.languages) {
                $scope.languages = response.data.data.languages;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSchoolEmployee();
    }else{
        $scope.school_employee = {};
        $scope.GetSchoolDepartments();
        $scope.school_employee.new = true;
        $scope.school_employee.courses = [];
        $scope.school_employee.wdays = [];
        $scope.school_employee.school_jobs = [];
    }
    
    $scope.GetCountries();
    $scope.GetGenres();
    $scope.GetSchoolJobs();
    $scope.GetCourses();
    $scope.GetWdays();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_employee.active = "Y";
        }else{
            $scope.school_employee.active = "N";
        }

        $scope.school_employee.school_job_mfk = null;
        if($scope.school_employee.school_jobs){
            $scope.school_employee.school_job_mfk = ','+$scope.school_employee.school_jobs.join(',')+',';
        }

        $scope.school_employee.course_mfk = null;
        if($scope.school_employee.courses){
            $scope.school_employee.course_mfk = ','+$scope.school_employee.courses.join(',')+',';
        }

        $scope.school_employee.wday_mfk = null;
        if($scope.school_employee.wdays){
            $scope.school_employee.wday_mfk = ','+$scope.school_employee.wdays.join(',')+',';
        }
        return true;
    };

    $scope.save = function(back_location){
        $scope.success_save = null;
        $scope.alerts = {};
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_employee: $scope.school_employee},
                url: '/api/SchoolEmployee/save'
            }).then(function successCallback(response) {
                if(response.data.status){

                    if(back_location){
                        $state.go('school_employee.all');
                    }else{
                        $scope.alerts.messages = response.data.message;
                        $scope.alerts.type = 'success';
                        $scope.success_save = true;
                    }
                }else{
                    $scope.alerts.messages = response.data.message;
                    $scope.alerts.type = 'error';
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_employee: $scope.school_employee},
                url: '/api/SchoolEmployee/save'
            }).then(function successCallback(response) {
                if(response.data.status){
                    $state.go('school_employee.edit',{school_employee_id: response.data.data.id});
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



