'use strict';

app.controller('SchoolEmployeeReplaceController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.school_employees = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100,500,999];

    $scope.GetSchoolEmployees = function() {
        
        $scope.Params = 'limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolEmployee/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.school_employees = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolEmployees();
    };



    $scope.replace_employee = function(school_employee) {
        $scope.success_replace_employee = null;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalReplaceEmployee.html',
            controller: 'ModalReplaceEmployeeInstanceCtrl',
            resolve: {
                school_employee: function() {
                    return school_employee;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.success_replace_employee = true;
            $scope.GetSchoolEmployees();
        }, function() {
            
        });
    };


    $scope.GetSchoolEmployees();


});



app.controller('ModalReplaceEmployeeInstanceCtrl', function($scope, $uibModalInstance, $http, school_employee) {
    $scope.school_employee = angular.copy(school_employee);
    console.log(school_employee);
    $scope.school_employee_to_replace = null;

    $scope.GetSchoolEmployees = function(val) {
        return $http.get('/api/SchoolEmployee/get/school_job=teacher,join=rea_user,limit=10,key_search=' + encodeURI(val)+',exclude_id='+$scope.school_employee.id).then(function(response) {
            return response.data.data.map(function(item) {
                item.fullname = item.firstname + ' ' + item.f_firstname + ' ' + item.lastname;
                return item;
            });
        });
    };

    $scope.submit = function() {
        $http({
            method: 'POST',
            data: {school_employee_id : $scope.school_employee.id, school_employee_to_replace_id : $scope.school_employee_to_replace.id, school_employee_to_replace_id : $scope.school_employee_to_replace.id, school_employee_to_replace_id : $scope.school_employee_to_replace.id, cancel_employee_account: $scope.cancel_employee_account },
            url: '/api/SchoolEmployee/replace'
        }).then(function successCallback(response) {
            if(response.data.status){
                $uibModalInstance.close();
            }else{
                $scope.errors = response.data.message.errors;
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


