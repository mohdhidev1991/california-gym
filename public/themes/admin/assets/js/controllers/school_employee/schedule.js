'use strict';

app.controller('SchoolEmployeeScheduleController', function($scope, $http, $stateParams, $location, $filter) {

    $scope.school_employee = null;
    $scope.error_get = null;
    $scope.school_employee_id = $stateParams.school_employee_id;


    $scope.GetSchoolEmployee = function() {
        
        var url_api = '/api/SchoolEmployee/fullinfos/';
        if($scope.school_employee_id){
            url_api = '/api/SchoolEmployee/fullinfos/'+$scope.school_employee_id;
        }

        $scope.error_get = null;
        $http({
            method: 'GET',
            url: url_api
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.school_employee = response.data.data;
            }else{
                $scope.error_get = response.data.message;
            }
        }, function errorCallback(response) {
            $scope.error_get = true;
            console.log('error');
        });
    };


    $scope.Courses = function() {
        $http({
            method: 'GET',
            url: '/api/Course/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.courses = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.Courses();


    $scope.SchoolJobs = function() {
        $scope.school_jobs = null;
        $http({
            method: 'GET',
            url: '/api/SchoolJob/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.school_jobs = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.SchoolJobs();


    $scope.Wdays = function() {
        $scope.school_jobs = null;
        $http({
            method: 'GET',
            url: '/api/Wday/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.wdays = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.Wdays();


    $scope.LevelClassCurrentSchoolYear = function() {
        $scope.school_jobs = null;
        $http({
            method: 'GET',
            url: '/api/LevelClass/get/current_school_year'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.level_classes = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.LevelClassCurrentSchoolYear();



    $scope.GetSchoolEmployee();
    
});

