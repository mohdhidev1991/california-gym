'use strict';

app.controller('SchoolJobAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.school_jobs = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10, 50, 100, 500];

    $scope.GetSchoolJobs = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolJob/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_jobs) {
                $scope.school_jobs = response.data.data.school_jobs;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolJobs();
    };



    $scope.delete_school_job = function(school_job_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolJobInstanceCtrl',
            resolve: {
                school_job_id: function() {
                    return school_job_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolJobs();
        }, function() {
            
        });
    };


    $scope.GetSchoolJobs();


});



app.controller('ModalDeleteSchoolJobInstanceCtrl', function($scope, $uibModalInstance, $http, school_job_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_job_id : school_job_id},
            url: '/api/SchoolJob/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


