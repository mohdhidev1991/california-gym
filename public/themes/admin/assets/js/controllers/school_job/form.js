'use strict';

app.controller('SchoolJobFormController', function($scope, $http, $stateParams, $location) {

    $scope.school_job = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchoolJob = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolJob/get/school_job_id='+$stateParams.school_job_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_job = response.data.data;
                if($scope.school_job.active && $scope.school_job.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSchoolJob();
    }else{
        $scope.school_job = {};
        $scope.school_job.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_job.active = "Y";
        }else{
            $scope.school_job.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_job: $scope.school_job},
                url: '/api/SchoolJob/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/SchoolJobs");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.school_job);
            $http({
                method: 'POST',
                data: {school_job: $scope.school_job},
                url: '/api/SchoolJob/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/SchoolJob/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



