'use strict';

app.controller('SchoolLevelAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.school_levels = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetSchoolLevels = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolLevel/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_levels) {
                $scope.school_levels = response.data.data.school_levels;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolLevels();
    };



    $scope.delete_school_level = function(school_level_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolLevelInstanceCtrl',
            resolve: {
                school_level_id: function() {
                    return school_level_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolLevels();
        }, function() {
            
        });
    };


    $scope.GetSchoolLevels();


});



app.controller('ModalDeleteSchoolLevelInstanceCtrl', function($scope, $uibModalInstance, $http, school_level_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_level_id : school_level_id},
            url: '/api/SchoolLevel/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


