'use strict';

app.controller('SchoolLevelConfigController', function($scope, $http, $stateParams, $location) {

    $scope.schools = null;
    $scope.school_years = null;
    $scope.school_levels_config = null;
    $scope.selected_school = null;
    $scope.selected_school_year = null;
    $scope.config_mode = false;


    $scope.GetMySchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get_my_schools'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetSchoolYears = function() {
        $scope.school_years = null;
        $scope.selected_school_year = null;
        if($scope.selected_school){
            $http({
                method: 'GET',
                url: '/api/SchoolYear/getAllSchoolYearsBySchoolID/'+$scope.selected_school
            }).then(function successCallback(response) {
                if (response.data.data.length) {
                    $scope.school_years = response.data.data;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };



    $scope.GetConfigLevelClass = function() {
        $scope.school_levels_config = null;
        $scope.error_save = null;
        $scope.success_save = null;
        if($scope.selected_school_year){
            $http({
                method: 'GET',
                url: '/api/LevelClass/getAllBySchoolIdAndYearIdAndDomaineMfk/school_id/'+$scope.selected_school+'/school_year_id/'+$scope.selected_school_year
            }).then(function successCallback(response) {
                console.log(response);
                if (response.data.data.length) {
                    $scope.school_levels_config = response.data.data;
                    $scope.config_mode = true;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.GetSchoolSdepartment = function() {
        $scope.sdepartments = null;
        if($scope.selected_school){
            $http({
                method: 'GET',
                url: '/api/Sdepartment/get/school_id='+$scope.selected_school
            }).then(function successCallback(response) {
                console.log(response.data.data.sdepartments);
                if (response.data.data.sdepartments.length) {
                    $scope.sdepartments = response.data.data.sdepartments;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.save = function() {
        $scope.error_save = null;
        $scope.success_save = null;
        if($scope.school_levels_config){
            $http({
                method: 'POST',
                data: {school_levels_config : $scope.school_levels_config},
                url: '/api/LevelClass/SaveScopeSchoolLevelClass'
            }).then(function successCallback(response) {
                $scope.success_save = true;
                console.log(response);
            }, function errorCallback(response) {
                $scope.error_save = true;
                console.log('error');
            });
        }
    };


    $scope.config_school_level = function(){
        $scope.GetConfigLevelClass();
        console.log('config_school_level');
    };


    $scope.cancel_config_school_level = function(){
        $scope.config_mode = false;
    };

    $scope.GetMySchools();
});


