'use strict';

app.controller('SchoolLevelFormController', function($scope, $http, $stateParams, $location) {

    $scope.school_level = null;
    $scope.levels_templates = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchoolLevel = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolLevel/get/school_level_id='+$stateParams.school_level_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_level = response.data.data;
                if($scope.school_level.active && $scope.school_level.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.LevelsTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/LevelsTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.levels_templates) {
                $scope.levels_templates = response.data.data.levels_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSchoolLevel();
    }else{
        $scope.school_level = {};
        $scope.school_level.new = true;
    }
    $scope.LevelsTemplates();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_level.active = "Y";
        }else{
            $scope.school_level.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_level: $scope.school_level},
                url: '/api/SchoolLevel/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/school_levels");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.school_level);
            $http({
                method: 'POST',
                data: {school_level: $scope.school_level},
                url: '/api/SchoolLevel/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/school_level/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



