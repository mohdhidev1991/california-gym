'use strict';

app.controller('SchoolPeriodAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.school_periods = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetSchoolPeriods = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolPeriod/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_periods) {
                $scope.school_periods = response.data.data.school_periods;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolPeriods();
    };



    $scope.delete_school_period = function(school_period_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolPeriodInstanceCtrl',
            resolve: {
                school_period_id: function() {
                    return school_period_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolPeriods();
        }, function() {
            
        });
    };


    $scope.GetSchoolPeriods();


});



app.controller('ModalDeleteSchoolPeriodInstanceCtrl', function($scope, $uibModalInstance, $http, school_period_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_period_id : school_period_id},
            url: '/api/SchoolPeriod/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


