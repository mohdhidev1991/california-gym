'use strict';

app.controller('SchoolPeriodFormController', function($scope, $http, $stateParams, $location) {

    $scope.school_period = null;
    $scope.schools = null;
    $scope.periods = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchoolPeriod = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolPeriod/get/school_period_id='+$stateParams.school_period_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_period = response.data.data;
                if($scope.school_period.active && $scope.school_period.active==="Y"){
                    $scope.active_switch = true;
                }
                $scope.timer_door_open_time = moment($scope.school_period.door_open_time,'HH:mm');
                $scope.timer_door_close_time = moment($scope.school_period.door_close_time,'HH:mm');
                $scope.timer_end_coming_time = moment($scope.school_period.end_coming_time,'HH:mm');
                $scope.timer_start_exit_time = moment($scope.school_period.start_exit_time,'HH:mm');
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetPeriods = function() {
        $http({
            method: 'GET',
            url: '/api/Period/get'
        }).then(function successCallback(response) {
            if (response.data.data.periods) {
                $scope.periods = response.data.data.periods;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSchoolPeriod();
    }else{
        $scope.school_period = {};
        $scope.school_period.new = true;
    }
    $scope.GetSchools();
    $scope.GetPeriods();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_period.active = "Y";
        }else{
            $scope.school_period.active = "N";
        }
        $scope.school_period.door_open_time = moment($scope.timer_door_open_time).format('HH:mm');
        $scope.school_period.door_close_time = moment($scope.timer_door_close_time).format('HH:mm');
        $scope.school_period.end_coming_time = moment($scope.timer_end_coming_time).format('HH:mm');
        $scope.school_period.start_exit_time = moment($scope.timer_start_exit_time).format('HH:mm');
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_period: $scope.school_period},
                url: '/api/SchoolPeriod/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/school_periods");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.school_period);
            $http({
                method: 'POST',
                data: {school_period: $scope.school_period},
                url: '/api/SchoolPeriod/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/school_period/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



