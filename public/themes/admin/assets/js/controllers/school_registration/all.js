'use strict';

app.controller('SchoolRegistrationAllController', function($scope, $http, $uibModal, $stateParams) {

    $scope.school_registrations = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100,500,999];

    $scope.GetSchoolRegistrations = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolRegistration/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_registrations) {
                $scope.school_registrations = response.data.data.school_registrations;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolRegistrations();
    };



    $scope.delete_school_registration = function(school_registration_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolRegistrationInstanceCtrl',
            resolve: {
                school_registration_id: function() {
                    return school_registration_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolRegistrations();
        }, function() {
            
        });
    };


    $scope.GetSchoolRegistrations();


});



app.controller('ModalDeleteSchoolRegistrationInstanceCtrl', function($scope, $uibModalInstance, $http, school_registration_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_registration_id : school_registration_id},
            url: '/api/SchoolRegistration/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


