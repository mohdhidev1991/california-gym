'use strict';

app.controller('SchoolRegistrationFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.school_registration = null;
    $scope.active_switch = false;
    $scope.valide_email_switch = false;
    $scope.valide_mobile_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchoolRegistration = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolRegistration/get/school_registration_id='+$stateParams.school_registration_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_registration = response.data.data;
                if($scope.school_registration.active && $scope.school_registration.active==="Y"){
                    $scope.active_switch = true;
                }
                if($scope.school_registration.country_id){
                    $scope.GetCities();
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCities = function() {
        $http({
            method: 'GET',
            url: '/api/City/get/country=local'
        }).then(function successCallback(response) {
            if (response.data.data.cities) {
                $scope.cities = response.data.data.cities;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    

    if($scope.action==="edit"){
        $scope.GetSchoolRegistration();
    }else{
        $scope.school_registration = {};
        $scope.school_registration.new = true;
    }

    $scope.GetCities();




    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_registration.active = "Y";
        }else{
            $scope.school_registration.active = "N";
        }

        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_registration: $scope.school_registration},
                url: '/api/SchoolRegistration/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $state.go("school_registration.all");
                    return;
                }
                if($scope.school_registration.id){
                    $scope.GetSchoolRegistration();
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.school_registration);
            $http({
                method: 'POST',
                data: {school_registration: $scope.school_registration},
                url: '/api/SchoolRegistration/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $state.go("school_registration.edit",{school_registration_id: response.data.data.id});
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.convert_to_school = function(){
        $scope.errors_convert_to_school = false;
        $scope.success_convert_to_school = false;
        $http({
            method: 'POST',
            data: {school_registration_id: $scope.school_registration.id},
            url: '/api/SchoolRegistration/convert_to_school'
        }).then(function successCallback(response) {
            if(response.data.data.success){
                $scope.success_convert_to_school = true;
            }else{
                $scope.errors_convert_to_school = response.data.data.errors;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
});





