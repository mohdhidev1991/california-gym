'use strict';

app.controller('SchoolTermAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.school_terms = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetSchoolTerms = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolTerm/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_terms) {
                $scope.school_terms = response.data.data.school_terms;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolTerms();
    };



    $scope.delete_school_term = function(school_term_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolTermInstanceCtrl',
            resolve: {
                school_term_id: function() {
                    return school_term_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolTerms();
        }, function() {
            
        });
    };


    $scope.GetSchoolTerms();


});



app.controller('ModalDeleteSchoolTermInstanceCtrl', function($scope, $uibModalInstance, $http, school_term_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_term_id : school_term_id},
            url: '/api/SchoolTerm/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


