'use strict';

app.controller('SchoolTermFormController', function($scope, $http, $stateParams, $location) {

    $scope.school_term = null;
    $scope.levels_templates = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchoolTerm = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolTerm/get/school_term_id='+$stateParams.school_term_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_term = response.data.data;
                if($scope.school_term.active && $scope.school_term.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSchoolTerm();
    }else{
        $scope.school_term = {};
        $scope.school_term.new = true;
    }
    $scope.GetSchools();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_term.active = "Y";
        }else{
            $scope.school_term.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_term: $scope.school_term},
                url: '/api/SchoolTerm/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/school_terms");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.school_term);
            $http({
                method: 'POST',
                data: {school_term: $scope.school_term},
                url: '/api/SchoolTerm/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/school_term/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



