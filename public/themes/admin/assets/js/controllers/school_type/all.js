'use strict';

app.controller('SchoolTypeAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.school_types = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetSchoolTypes = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SchoolType/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_types) {
                $scope.school_types = response.data.data.school_types;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolTypes();
    };



    $scope.delete_school_type = function(school_type_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolTypeInstanceCtrl',
            resolve: {
                school_type_id: function() {
                    return school_type_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolTypes();
        }, function() {
            
        });
    };


    $scope.GetSchoolTypes();


});



app.controller('ModalDeleteSchoolTypeInstanceCtrl', function($scope, $uibModalInstance, $http, school_type_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_type_id : school_type_id},
            url: '/api/SchoolType/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

