'use strict';

app.controller('SchoolTypeFormController', function($scope, $http, $stateParams, $location) {

    $scope.school_type = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.tservices  = null;


    $scope.GetSchoolType = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolType/get/school_type_id='+$stateParams.school_type_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_type = response.data.data;

                if($scope.school_type.active && $scope.school_type.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };




    $scope.GetTservices = function() {
        $http({
            method: 'GET',
            url: '/api/TService/get'
        }).then(function successCallback(response) {
            if (response.data.data.tservices.length) {
                $scope.tservices = response.data.data.tservices;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if($scope.action==="edit"){
        $scope.GetSchoolType();
    }else{
        $scope.school_type = {};
        $scope.school_type.new = true;
    }
    $scope.GetTservices();



    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_type.active = "Y";
        }else{
            $scope.school_type.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_type: $scope.school_type},
                url: '/api/SchoolType/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/SchoolTypes");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_type: $scope.school_type},
                url: '/api/SchoolType/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/SchoolType/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



