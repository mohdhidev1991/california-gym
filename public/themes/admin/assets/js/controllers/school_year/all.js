'use strict';

app.controller('SchoolYearAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.school_years = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];
    $scope.filter = {};



    $scope.order_by = 'id';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetSchoolYears();
    };

    $scope.GetSchoolYears = function() {
        $scope.empty_results = null;
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'school_id',
            'name_or_id',
            'week_template_id',
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });

        $http({
            method: 'GET',
            url: '/api/SchoolYear/get/join=school,'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.school_years.length) {
                $scope.school_years = response.data.data.school_years;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }else{
                $scope.empty_results = true;
                $scope.school_years = null;
                $scope.totalItems = 0;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSchoolYears();
    };



    $scope.delete_school_year = function(school_year_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSchoolYearInstanceCtrl',
            resolve: {
                school_year_id: function() {
                    return school_year_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSchoolYears();
        }, function() {
            
        });
    };


    $scope.GetSchoolYears();


});



app.controller('ModalDeleteSchoolYearInstanceCtrl', function($scope, $uibModalInstance, $http, school_year_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {school_year_id : school_year_id},
            url: '/api/SchoolYear/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


