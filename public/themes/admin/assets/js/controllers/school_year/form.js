'use strict';

app.controller('SchoolYearFormController', function($scope, $http, $stateParams, $location) {

    $scope.school_year = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSchoolYear = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolYear/get/school_year_id='+$stateParams.school_year_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_year = response.data.data;
                if($scope.school_year.active && $scope.school_year.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if($scope.action==="edit"){
        $scope.GetSchoolYear();
    }else{
        $scope.school_year = {};
        $scope.school_year.new = true;
    }
    $scope.GetSchools();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.school_year.active = "Y";
        }else{
            $scope.school_year.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {school_year: $scope.school_year},
                url: '/api/SchoolYear/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/school_years");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.school_year);
            $http({
                method: 'POST',
                data: {school_year: $scope.school_year},
                url: '/api/SchoolYear/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/school_year/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



