'use strict';

app.controller('SchoolYearInfosController', function($scope, $http, $stateParams, $location) {

    $scope.school_year = null;

    $scope.GetSchoolYearInfos = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolYear/get/school_year_id=' + $stateParams.school_year_id + ',fullinfos'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_year = response.data.data;
            } else {
                $scope.can_get_data = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolYearInfos();

});