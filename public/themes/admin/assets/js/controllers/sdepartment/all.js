'use strict';

app.controller('SdepartmentAllController', function($scope, $http, $uibModal, $stateParams, $window) {

    $scope.sdepartments = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.optionsItemsPerPage = [10, 50, 100, 999];
    $scope.itemsPerPage = $scope.optionsItemsPerPage[0];
    $scope.active_status = [{id: 'Y', label: 'Global.Yes'}, {id: 'N', label: 'Global.No'}];
    $scope.filter = {};

    $scope.order_by = 'id';
    $scope.order = 'ASC';
    $scope.set_order = function(order_by, order, call_back) {
        var switche = false;

        if($scope.order_by!==order_by){
            switche = true;
        }
        $scope.order_by = order_by;
        if(!switche){
            if (order) {
                $scope.order = order;
            }else{
                if ($scope.order === 'ASC') {
                    $scope.order = 'DESC';
                }else{
                    $scope.order = 'ASC';
                }
            }
        }
        $scope.GetSdepartments();
    };


    $scope.GetSdepartments = function() {
        $scope.empty_results = null;
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        if($scope.order_by){
            $scope.Params += ',order_by='+$scope.order_by;
        }
        if($scope.order){
            $scope.Params += ',order='+$scope.order;
        }

        angular.forEach([
            'school_id',
            'name_or_id',
            'week_template_id',
            'status',
            ], function(filter_item, key) {
            if($scope.filter[filter_item]){
                $scope.Params += ','+filter_item+'='+encodeURIComponent($scope.filter[filter_item]);
            }
        });

        $http({
            method: 'GET',
            url: '/api/Sdepartment/get/join=school,'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.sdepartments.length) {
                $scope.sdepartments = response.data.data.sdepartments;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }else{
                $scope.empty_results = true;
                $scope.sdepartments = null;
                $scope.totalItems = 0;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.export_xls = function() {
        $window.location.href = '/api/Sdepartment/get/export=xls,join=all,' +$scope.Params;
    };


    $scope.GetSchools = function() {
        $scope.schools = null;
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetSchools();


    $scope.GetWeekTemplates = function() {
        $scope.week_templates = null;
        $http({
            method: 'GET',
            url: '/api/WeekTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.week_templates.length) {
                $scope.week_templates = response.data.data.week_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetWeekTemplates();


    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSdepartments();
    };



    $scope.delete_sdepartment = function(sdepartment_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSdepartmentInstanceCtrl',
            resolve: {
                sdepartment_id: function() {
                    return sdepartment_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSdepartments();
        }, function() {
            
        });
    };


    $scope.GetSdepartments();


});



app.controller('ModalDeleteSdepartmentInstanceCtrl', function($scope, $uibModalInstance, $http, sdepartment_id) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {sdepartment_id : sdepartment_id},
            url: '/api/Sdepartment/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

