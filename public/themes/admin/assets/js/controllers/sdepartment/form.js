'use strict';

app.controller('SdepartmentFormController', function($scope, $http, $stateParams, $location) {

    $scope.sdepartment = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.schools  = null;


    $scope.GetSdepartment = function() {
        $http({
            method: 'GET',
            url: '/api/Sdepartment/get/sdepartment_id='+$stateParams.sdepartment_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.sdepartment = response.data.data;

                if($scope.sdepartment.maintenance_start_time){
                    $scope.timer_maintenance_start_time = moment($scope.sdepartment.maintenance_start_time, 'HH:mm');
                }
                
                if($scope.sdepartment.maintenance_end_time){
                    $scope.timer_maintenance_end_time = moment($scope.sdepartment.maintenance_end_time, 'HH:mm');    
                }

                if($scope.sdepartment.active && $scope.sdepartment.active==="Y"){
                    $scope.active_switch = true;
                }

                if(!$scope.sdepartment.we_days){
                    $scope.sdepartment.we_days = [];
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };





    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetWeekTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/WeekTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.week_templates.length) {
                $scope.week_templates = response.data.data.week_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSdepartment();
    }else{
        $scope.sdepartment = {};
        $scope.sdepartment.new = true;
    }
    $scope.GetSchools();
    $scope.GetWeekTemplates();

    $scope.pre_save = function(){

        if($scope.active_switch===true){
            $scope.sdepartment.active = "Y";
        }else{
            $scope.sdepartment.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {sdepartment: $scope.sdepartment},
                url: '/api/Sdepartment/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/sdepartments");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {sdepartment: $scope.sdepartment},
                url: '/api/Sdepartment/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/sdepartment/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



