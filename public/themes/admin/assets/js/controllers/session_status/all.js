'use strict';

app.controller('SessionStatusAllController', function($scope, $http, $uibModal, $stateParams, $location) {
    $scope.session_statuss = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetSessionStatuss = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/SessionStatus/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.session_statuss) {
                $scope.session_statuss = response.data.data.session_statuss;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetSessionStatuss();
    };



    $scope.delete_session_status = function(session_status_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteSessionStatusInstanceCtrl',
            resolve: {
                session_status_id: function() {
                    return session_status_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetSessionStatuss();
        }, function() {
            
        });
    };


    $scope.GetSessionStatuss();


});



app.controller('ModalDeleteSessionStatusInstanceCtrl', function($scope, $uibModalInstance, $http, session_status_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {session_status_id : session_status_id},
            url: '/api/SessionStatus/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


