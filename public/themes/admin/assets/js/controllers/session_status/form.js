'use strict';

app.controller('SessionStatusFormController', function($scope, $http, $stateParams, $location) {

    $scope.session_status = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetSessionStatus = function() {
        $http({
            method: 'GET',
            url: '/api/SessionStatus/get/session_status_id='+$stateParams.session_status_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.session_status = response.data.data;
                if($scope.session_status.active && $scope.session_status.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetSessionStatus();
    }else{
        $scope.session_status = {};
        $scope.session_status.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.session_status.active = "Y";
        }else{
            $scope.session_status.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {session_status: $scope.session_status},
                url: '/api/SessionStatus/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/session_statuss");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.session_status);
            $http({
                method: 'POST',
                data: {session_status: $scope.session_status},
                url: '/api/SessionStatus/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/session_status/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



