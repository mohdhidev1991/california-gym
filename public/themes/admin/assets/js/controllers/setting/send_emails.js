'use strict';

app.controller('SettingSendEmailsManageController', function($scope, $http, $uibModal, $stateParams, $location) {


    $scope.yearWeek = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('Y'));
        // return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 10) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
    };

    $scope.weekForDate = function(date) {
        if (!date) { date = moment(); }
        var begin = moment(date).isoWeekday(1);
        return parseInt(begin.add(6, 'day').format('W'));
    };

    //$scope.week_num = moment().add(7, 'days').format('W');
    $scope.week_num = 20;
    $scope.course_session_year = $scope.yearWeek();
    $scope.week_num = $scope.weekForDate();



    $scope.send_by_email = function() {

        var week_num = this.weekForDate($scope.popup_datepicker.date);
        var year_num = this.yearWeek($scope.popup_datepicker.date);
        $scope.course_session_year = year_num;
        $scope.week_num = week_num + 1;
        $scope.finish = false;
        $scope.processing = true;
        $scope.pourcent = 0;
        $scope.current_step = 0;
        $scope.request_processor();
    
    };

    $scope.request_processor = function(){
        $http({
            method: 'GET',
            url: '/api/CourseSession/send_week_email/'+$scope.course_session_year+'/'+$scope.week_num+'/0/0/0/'+$scope.current_step
        }).then(function successCallback(response) {
            $scope.current_step++;
            if(response.data.attributes.finish){
                $scope.pourcent = 100;
                $scope.finish = true;
            }else{
                $scope.pourcent = parseInt($scope.current_step/response.data.data.steps*100);
                $scope.request_processor();
            }
            $scope.request_processor();
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.popup_datepicker = {
        date: moment().add(7, 'days').startOf('isoweek').toDate(),
        opened: false,
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: moment().add(1, 'years').toDate(),
        minDate: null,
        startingDay: 1,
        dateDisabled: function(option) {
            return (option.mode === 'day' && (option.date.getDay() !== 1));
        }
    };
    $scope.open_datepicker = function() {
        $scope.popup_datepicker.opened = true;
    };
    $scope.disabledDates = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 1));
    };

});


