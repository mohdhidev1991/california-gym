'use strict';

app.controller('StudentAddToSchoolController', function($scope, $http, $stateParams, $location, $state, $filter, $anchorScroll) {

    $scope.parent_user = {};
    $scope.student = {};
    $scope.family_relation = {};
    $scope.student_file = {};
    $scope.step = 'find_parent';

    $scope.reset_all = function() {
        $scope.parent_user = {};
        $scope.step = 'find_parent';
        $scope.student = {};
        $scope.family_relation = {};
        $scope.errors_save_parent = null;
    };

    $scope.find_parent = function() {
        $scope.step = 'parent';
        var clone_parent = angular.copy($scope.parent_user);
        $scope.student = {};
        $scope.family_relation = {};

        $http({
            method: 'GET',
            url: '/api/ReaUser/get/idn='+$scope.parent_user.idn+',idn_type_id='+$scope.parent_user.idn_type_id
        }).then(function successCallback(response) {
            $scope.step = 'parent';
            if (response.data.status) {
                $scope.parent_user.id = response.data.data.id;
                $scope.parent_user.country_id = response.data.data.country_id;
                $scope.parent_user.firstname = response.data.data.firstname;
                $scope.parent_user.lastname = response.data.data.lastname;
                $scope.parent_user.f_firstname = response.data.data.f_firstname;
                $scope.parent_user.mobile = response.data.data.mobile;
                $scope.parent_user.genre_id = response.data.data.genre_id;
                $scope.parent_user.email = response.data.data.email;
            }else{
                $scope.parent_user.id = null;
                $scope.parent_user.new = true;
                $scope.parent_user.idn = clone_parent.idn;
                $scope.parent_user.idn_type_id = clone_parent.idn_type_id;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetStudent = function() {
        $http({
            method: 'GET',
            url: '/api/Student/get/student_id='+$stateParams.student_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.student = response.data.data;
                if($scope.student.active && $scope.student.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCities = function() {
        if(!$scope.student.country_id){
            $scope.cities = null;
            return;
        }
        $http({
            method: 'GET',
            url: '/api/City/get/country_id='+$scope.student.country_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.cities = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.countries = response.data.data;
                $scope.GetCities();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetGenres = function() {
        $scope.genres = null;
        $http({
            method: 'GET',
            url: '/api/Genre/get'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.genres = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetIdnTypes = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.idn_types = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolLevels = function() {
        $http({
            method: 'GET',
            url: '/api/SchoolLevel/get/current_school_levels'
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.school_levels = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetLevelClasses = function(school_level_id, model_source) {
        $scope.level_classes = null;
        if(!school_level_id){
            return;
        }
        if(model_source){
            var copy_model_source = angular.copy(model_source);
        }

        $http({
            method: 'GET',
            url: '/api/LevelClass/get/school_level_id='+school_level_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.level_classes = response.data.data;
            }
            if(model_source){
                model_source = copy_model_source;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetSchoolClasses = function(level_class_id, model_source) {
        $scope.school_classes = null;
        if(!level_class_id){
            return;
        }

        if(model_source){
            var copy_model_source = angular.copy(model_source);
        }

        $http({
            method: 'GET',
            url: '/api/SchoolClass/get/level_class_id='+level_class_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.school_classes = response.data.data;
            }
            if(model_source){
                model_source = copy_model_source;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetRelships = function() {
        $http({
            method: 'GET',
            url: '/api/Relship/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.relships = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    $scope.GetCountries();
    $scope.GetIdnTypes();
    $scope.GetGenres();
    $scope.GetRelships();
    $scope.GetSchoolLevels();


    $scope.$watch('family_relation.relship_id', function(newValue, oldValue) {

        // if is child son
        if($scope.family_relation.relship_id===1 && $scope.student.genre_id===1 && !$scope.family_relation.resp_relationship){
            $scope.family_relation.resp_relationship = $filter('translate')('Field.Son');
        }
        // if is child girl
        if($scope.family_relation.relship_id===1 && $scope.student.genre_id===2 && !$scope.family_relation.resp_relationship){
            $scope.family_relation.resp_relationship = $filter('translate')('Field.Girl');
        }

        // if is parent man
        if($scope.family_relation.relship_id===1 && $scope.parent_user.genre_id===1 && $scope.parent_user.lastname && !$scope.student.lastname){
            $scope.student.lastname = $scope.parent_user.lastname;
        }
        // if is parent man
        if($scope.family_relation.relship_id===1 && $scope.parent_user.genre_id===1 && $scope.parent_user.firstname && !$scope.student.f_firstname){
            $scope.student.f_firstname = $scope.parent_user.firstname;
        }
        // if is parent man
        if($scope.family_relation.relship_id===1 && $scope.parent_user.genre_id===1 && $scope.parent_user.idn_type_id && !$scope.student.idn_type_id){
            $scope.student.idn_type_id = $scope.parent_user.idn_type_id;
        }
        // if is parent man
        if($scope.family_relation.relship_id===1 && $scope.parent_user.genre_id===1 && $scope.parent_user.country_id && !$scope.student.country_id){
            $scope.student.country_id = $scope.parent_user.country_id;
        }
    });
    $scope.$watch('student.genre_id', function(newValue, oldValue) {
        if($scope.family_relation.relship_id===1 && $scope.student.genre_id===1 && !$scope.family_relation.resp_relationship){
            $scope.family_relation.resp_relationship = $filter('translate')('Field.Son');
        }
        if($scope.family_relation.relship_id===1 && $scope.student.genre_id===2 && !$scope.family_relation.resp_relationship){
            $scope.family_relation.resp_relationship = $filter('translate')('Field.Girl');
        }
    });

    $scope.$watch('student.current_school_level_id', function(newValue, oldValue) {
        $scope.GetLevelClasses($scope.student.current_school_level_id, $scope.student.current_level_class_id);
    });


    $scope.$watch('step', function(newValue, oldValue) {
        $anchorScroll();
        if(newValue==='students'){
            $scope.reset_students_step();
        }

        if($scope.form_student_file){
            $scope.form_student_file.$setPristine();
            $scope.form_student_file.$setUntouched();
        }
        if($scope.form_student){
            $scope.form_student.$setPristine();
            $scope.form_student.$setUntouched();
        }
        if($scope.form_parent){
            $scope.form_parent.$setPristine();
            $scope.form_parent.$setUntouched();
        }
    });


    $scope.get_related_students = function(){
        $scope.related_students = null;
        $scope.empty_related_students = null;
        if(typeof $scope.parent_user.id === 'undefined'){
            return;
        }
        $http({
            method: 'GET',
            url: '/api/FamilyRelation/get/join=student!student_file,resp_rea_user_id='+$scope.parent_user.id
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.related_students = response.data.data;
            }else{
                $scope.empty_related_students = true;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.save_parent = function(){
        $scope.errors_save_parent = null;
        $http({
            method: 'POST',
            data: {parent_user: $scope.parent_user},
            url: '/api/FamilyRelation/save_parent'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.parent_user.id = response.data.data;
                $scope.step = 'students';
            }else{
                $scope.errors_save_parent = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.save_student = function(back){

        $scope.success_save_student = null;
        $scope.errors_save_student = null;

        if($scope.family_relation.active_account_checkbox){
            $scope.family_relation.active_account = 'Y';
        }else{
            $scope.family_relation.active_account = 'N';
        }
        $http({
            method: 'POST',
            data: {student: $scope.student, family_relation: $scope.family_relation, parent_user: $scope.parent_user},
            url: '/api/FamilyRelation/save_student_for_parent'
        }).then(function successCallback(response) {
            if(response.data.status===true){
                $scope.student.id = response.data.data.student_id;
                $scope.family_relation.id = response.data.data.family_relation_id;
                if(back){
                    $scope.step = 'students';
                }else{
                    $scope.success_save_student = true;
                }
            }else{
                $scope.errors_save_student = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };





    $scope.remove_student = function(){
        alert('we can add this');
        return;
    };
    $scope.reset_students_step = function(){
        $scope.step = 'students';
        $scope.get_related_students();

    };
    $scope.add_student = function(){
        $scope.step = 'add_student';
        $scope.student = {};
        $scope.student.new = true;
        $scope.family_relation = {};
        $scope.family_relation.new = true;
        return;
    };
    $scope.edit_student = function(student_id){
        $scope.step = 'edit_student';
        $scope.student = {};
        $scope.family_relation = {};
        $scope.success_save_student = null;
        $scope.errors_save_student = null;

        $http({
            method: 'GET',
            url: '/api/FamilyRelation/get/join=student!student_file,resp_rea_user_id='+$scope.parent_user.id+',student_id='+student_id
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.student.id = response.data.data.student_id;
                $scope.student.firstname = response.data.data.student_firstname;
                $scope.student.lastname = response.data.data.student_lastname;
                $scope.student.f_firstname = response.data.data.student_f_firstname;
                $scope.student.genre_id = response.data.data.student_genre_id;
                $scope.student.idn = response.data.data.student_idn;
                $scope.student.idn_type_id = response.data.data.student_idn_type_id;
                $scope.student.country_id = response.data.data.student_country_id;
                $scope.student.birth_date = response.data.data.student_birth_date;

                $scope.student.current_school_level_id = response.data.data.current_school_level_id;
                $scope.student.current_level_class_id = response.data.data.current_level_class_id;


                $scope.family_relation.id = response.data.data.id;
                
                if(response.data.data.active_account==='Y'){
                    $scope.family_relation.active_account_checkbox = true;
                }else{
                    $scope.family_relation.active_account_checkbox = false;
                }
                
                $scope.family_relation.resp_relationship = response.data.data.resp_relationship;
                $scope.family_relation.relship_id = response.data.data.relship_id;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
        return;
    };



    $scope.edit_student_file = function(student_id){
        $scope.step = 'edit_student_file';

        $scope.student_file = {};

        $http({
            method: 'GET',
            url: '/api/StudentFile/get/first,join=level_class,current_school_year,student_id='+student_id
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.student_file.id = response.data.data.id;
                $scope.student_file.school_level_id = response.data.data.school_level_id;
                $scope.student_file.level_class_id = response.data.data.level_class_id;
                $scope.student_file.school_class_id = response.data.data.school_class_id;
                $scope.student_file.symbol = response.data.data.symbol;
                $scope.student_file.paid_amount = response.data.data.paid_amount;
                $scope.student_file.paid_hdate = response.data.data.paid_hdate;
                $scope.student_file.paid_comment = response.data.data.paid_comment;
                $scope.student_file.student_place = response.data.data.student_place;
                $scope.student_file.student_num = response.data.data.student_num;
                $scope.student_file.student_id = student_id;
            }else{
                $scope.student_file.new = true;
                $scope.student_file.student_id = student_id;
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
        
        return;
    };
    $scope.$watch('student_file.school_level_id', function(newValue, oldValue) {
        $scope.GetLevelClasses($scope.student_file.school_level_id, $scope.student_file.level_class_id);
    });
    $scope.$watch('student_file.level_class_id', function(newValue, oldValue) {
        $scope.GetSchoolClasses($scope.student_file.level_class_id, $scope.student_file.school_class_id);
    });



    $scope.save_student_file = function(back){

        $scope.success_save_student_file = null;
        $scope.errors_save_student_file = null;

        angular.forEach($scope.school_classes, function(item_school_class, key) {
            if(item_school_class.id===$scope.student_file.school_class_id){
                $scope.student_file.symbol = item_school_class.symbol;
            }
        });

        $http({
            method: 'POST',
            data: {student_file: $scope.student_file},
            url: '/api/StudentFile/save_student'
        }).then(function successCallback(response) {
            if(response.data.data.success===true){
                if(back){
                    $scope.step = 'students';
                }else{
                    $scope.success_save_student_file = true;
                    $scope.edit_student_file($scope.student_file.student_id);
                }
            }else{
                $scope.errors_save_student_file = response.data.data.errors;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

});



