'use strict';

app.controller('StudentAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.students = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100,500,999];

    $scope.GetStudents = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Student/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.students) {
                $scope.students = response.data.data.students;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetStudents();
    };



    $scope.delete_student = function(student_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteStudentInstanceCtrl',
            resolve: {
                student_id: function() {
                    return student_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetStudents();
        }, function() {
            
        });
    };


    $scope.GetStudents();


});



app.controller('ModalDeleteStudentInstanceCtrl', function($scope, $uibModalInstance, $http, student_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {student_id : student_id},
            url: '/api/Student/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


