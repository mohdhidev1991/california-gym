'use strict';

app.controller('StudentFormController', function($scope, $http, $stateParams, $location, $state) {

    $scope.student = null;
    $scope.active_switch = false;
    $scope.valide_email_switch = false;
    $scope.valide_mobile_switch = false;
    $scope.action = $stateParams.action;
    $scope.student = {};


    $scope.GetStudent = function() {
        $http({
            method: 'GET',
            url: '/api/Student/get/student_id='+$stateParams.student_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.student = response.data.data;
                if($scope.student.active && $scope.student.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetCities = function() {
        if(!$scope.student.country_id){
            $scope.cities = null;
            return;
        }
        $http({
            method: 'GET',
            url: '/api/City/get/country_id='+$scope.student.country_id
        }).then(function successCallback(response) {
            if (response.data.data.cities) {
                $scope.cities = response.data.data.cities;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetCountries = function() {
        $http({
            method: 'GET',
            url: '/api/Country/get'
        }).then(function successCallback(response) {
            if (response.data.data.countries) {
                $scope.countries = response.data.data.countries;
                $scope.GetCities();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetGenres = function() {
        $scope.genres = null;
        $http({
            method: 'GET',
            url: '/api/Genre/get'
        }).then(function successCallback(response) {
            if (response.data.data.genres) {
                $scope.genres = response.data.data.genres;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetIdnTypes = function() {
        $http({
            method: 'GET',
            url: '/api/IdnType/get'
        }).then(function successCallback(response) {
            if (response.data.data.idn_types) {
                $scope.idn_types = response.data.data.idn_types;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    if($scope.action==="edit"){
        $scope.GetStudent();
    }else{
        $scope.student = {};
        $scope.student.new = true;
    }
    $scope.GetCountries();
    $scope.GetIdnTypes();
    $scope.GetGenres();


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.student.active = "Y";
        }else{
            $scope.student.active = "N";
        }

        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {student: $scope.student},
                url: '/api/Student/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $state.go("student.all");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.student);
            $http({
                method: 'POST',
                data: {student: $scope.student},
                url: '/api/Student/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/student/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



