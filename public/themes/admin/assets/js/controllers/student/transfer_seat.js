'use strict';

app.controller('StudentTransferSeatController', function($scope, $http, $stateParams, $location, $state, $uibModal) {


    $scope.GetSchoolLevels = function() {
        $scope.reset_bf_transfer();
        $http({
            method: 'GET',
            url: '/api/SchoolLevel/get/current_school_levels'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.school_levels = response.data.data;
            }else{
                $scope.school_levels = null;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    $scope.GetLevelClasses = function() {
        $scope.level_classes = null;
        $scope.reset_bf_transfer();
        if(!$scope.school_level_id) return;
        $http({
            method: 'GET',
            url: '/api/LevelClass/get/school_level_id='+$scope.school_level_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.level_classes = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetSchoolClasses = function() {
        $scope.school_classes = null;
        $scope.reset_bf_transfer();
        if(!$scope.level_class_id) return;
        $http({
            method: 'GET',
            url: '/api/SchoolClass/get/join=room,level_class_id='+$scope.level_class_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.school_classes = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.get_capacity = function() {
        if(!$scope.school_class_id) return null;
        var capacity = 0;
        angular.forEach($scope.school_classes, function(obj, key) {
            if(obj.id===$scope.school_class_id){
                capacity = parseInt(obj.capacity);
            }
        });
        return capacity;
    };

    

    $scope.reset_bf_transfer = function() {
        $scope.students_place = null;
        $scope.students_source = null;
        $scope.school_class_fullof = null;
        $scope.capacity = null;
        $scope.errors_get_students = null;
        $scope.bad_order = null;
    };
    $scope.get_students = function() {
        $scope.reset_bf_transfer();

        if(!$scope.school_level_id || !$scope.level_class_id || !$scope.school_class_id) return;
        $http({
            method: 'GET',
            url: '/api/StudentFile/get/order_by=student_place,order=ASC,current_school_year,join=student,school_class_id='+$scope.school_class_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.capacity = $scope.get_capacity();
                $scope.students_source = response.data.data;
                if($scope.students_source.length>$scope.capacity){
                    $scope.school_class_fullof = true;
                    $scope.errors_get_students = ['SchoolClass.ThisSchoolClassIsFullOfPleaseReviewTheSchoolClassRoomCapacity'];
                }else{
                    $scope.order_students();
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.order_students = function() {
        $scope.students_place = [];
        for (var i = 1; i <= $scope.capacity; i++) {
            var item = $scope.students_source.filter(function(obj) {
              if(obj.student_place === i){
                return obj;
              }
            });
            if(typeof item[0]==='object'){
                $scope.students_place[item[0].student_place-1] = item[0];
            }else{
                $scope.students_place[i-1] = null;
            }
        };
        angular.forEach($scope.students_source, function(obj, key) {
          if(!obj.student_place || !$scope.students_place[obj.student_place-1]){
            $scope.students_place.push(obj);
          }
        });
        if($scope.students_place.length>$scope.capacity){
            $scope.students_place = null;
            $scope.bad_order = true;
            $scope.errors_get_students = ['SchoolClass.SameStudentsDontHavePlaceInThisSchoolClassPleaseReorderNow'];
        }
    };
    

    $scope.reorder_school_class = function() {
        $http({
            method: 'POST',
            data: {school_class_id: $scope.school_class_id},
            url: '/api/StudentFile/reorder_student_places'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.get_students();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_student_place = function(student) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalChangeStudentPlace.html',
            controller: 'ModalChangeStudentPlaceInstanceCtrl',
            resolve: {
                student: function() {
                    return student;
                },
                school_classes: function() {
                    return $scope.school_classes;
                },
                level_classes: function() {
                    return $scope.level_classes;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.get_students();
        }, function() {

        });
    };


    $scope.GetSchoolLevels();


});




app.controller('ModalChangeStudentPlaceInstanceCtrl', function($scope, $uibModalInstance, $http, student, level_classes, school_classes) {

    $scope.level_classes = angular.copy(level_classes);
    $scope.school_classes = angular.copy(school_classes);
    $scope.student = angular.copy(student);
    
    $scope.get_capacity = function() {
        if(!$scope.new_school_class_id) return null;
        var capacity = 0;
        angular.forEach($scope.school_classes, function(obj, key) {
            if(obj.id===$scope.new_school_class_id){
                capacity = parseInt(obj.capacity);
            }
        });
        return capacity;
    };

    

    $scope.reset_bf_transfer = function() {
        $scope.students_place = null;
        $scope.students_source = null;
        $scope.school_class_fullof = null;
        $scope.capacity = null;
        $scope.errors_get_students = null;
        $scope.bad_order = null;
        $scope.new_student_place = {};
    };
    $scope.get_students = function() {
        $scope.reset_bf_transfer();

        if(!$scope.new_school_class_id) return;
        $http({
            method: 'GET',
            url: '/api/StudentFile/get/order_by=student_place,order=ASC,current_school_year,join=student,school_class_id='+$scope.new_school_class_id
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.capacity = $scope.get_capacity();
                $scope.students_source = response.data.data;
                if($scope.students_source.length>$scope.capacity){
                    $scope.school_class_fullof = true;
                    $scope.errors_get_students = ['SchoolClass.ThisSchoolClassIsFullOfPleaseReviewTheSchoolClassRoomCapacity'];
                }else{
                    $scope.order_students();
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.order_students = function() {
        $scope.students_place = [];
        for (var i = 1; i <= $scope.capacity; i++) {
            var item = $scope.students_source.filter(function(obj) {
              if(obj.student_place === i){
                return obj;
              }
            });
            if(typeof item[0]==='object'){
                $scope.students_place[item[0].student_place-1] = item[0];
            }else{
                $scope.students_place[i-1] = null;
            }
        };
        angular.forEach($scope.students_source, function(obj, key) {
          if(!obj.student_place || !$scope.students_place[obj.student_place-1]){
            $scope.students_place.push(obj);
          }
        });
        if($scope.students_place.length>$scope.capacity){
            $scope.students_place = null;
            $scope.bad_order = true;
            $scope.errors_get_students = ['SchoolClass.SameStudentsDontHavePlaceInThisSchoolClassPleaseReorderNow'];
        }
    };
    



    $scope.confirm_replace = function() {
        if(!$scope.new_student_place.place_number) return;

        $http({
            method: 'POST',
            data: {school_class_id: $scope.student.school_class_id, student_id: $scope.student.student_id, new_school_class_id: $scope.new_school_class_id, new_student_place: $scope.new_student_place.place_number},
            url: '/api/StudentFile/replace_student_place/'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $uibModalInstance.close();
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



