'use strict';

app.controller('StudentExamAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.student_exams = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetStudentExams = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/StudentExam/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.student_exams) {
                $scope.student_exams = response.data.data.student_exams;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetStudentExams();
    };



    $scope.delete_student_exam = function(student_exam_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteStudentExamInstanceCtrl',
            resolve: {
                student_exam_id: function() {
                    return student_exam_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetStudentExams();
        }, function() {
            
        });
    };


    $scope.GetStudentExams();


});



app.controller('ModalDeleteStudentExamInstanceCtrl', function($scope, $uibModalInstance, $http, student_exam_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {student_exam_id : student_exam_id},
            url: '/api/StudentExam/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


