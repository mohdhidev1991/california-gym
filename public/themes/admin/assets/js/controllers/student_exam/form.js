'use strict';

app.controller('StudentExamFormController', function($scope, $http, $stateParams, $location) {

    $scope.student_exam = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetStudentExam = function() {
        $http({
            method: 'GET',
            url: '/api/StudentExam/get/student_exam_id='+$stateParams.student_exam_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.student_exam = response.data.data;
                if($scope.student_exam.active && $scope.student_exam.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    if($scope.action==="edit"){
        $scope.GetStudentExam();
    }else{
        $scope.student_exam = {};
        $scope.student_exam.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.student_exam.active = "Y";
        }else{
            $scope.student_exam.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {student_exam: $scope.student_exam},
                url: '/api/StudentExam/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/student_exams");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.student_exam);
            $http({
                method: 'POST',
                data: {student_exam: $scope.student_exam},
                url: '/api/StudentExam/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/student_exam/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



