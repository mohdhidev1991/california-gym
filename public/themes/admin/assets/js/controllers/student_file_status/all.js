'use strict';

app.controller('StudentFileStatusAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.student_file_statuss = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetStudentFileStatuss = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/StudentFileStatus/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.student_file_statuss) {
                $scope.student_file_statuss = response.data.data.student_file_statuss;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetStudentFileStatuss();
    };



    $scope.delete_student_file_status = function(student_file_status_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteStudentFileStatusInstanceCtrl',
            resolve: {
                student_file_status_id: function() {
                    return student_file_status_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetStudentFileStatuss();
        }, function() {
            
        });
    };


    $scope.GetStudentFileStatuss();


});



app.controller('ModalDeleteStudentFileStatusInstanceCtrl', function($scope, $uibModalInstance, $http, student_file_status_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {student_file_status_id : student_file_status_id},
            url: '/api/StudentFileStatus/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


