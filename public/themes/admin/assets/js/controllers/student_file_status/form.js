'use strict';

app.controller('StudentFileStatusFormController', function($scope, $http, $stateParams, $location) {

    $scope.student_file_status = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetStudentFileStatus = function() {
        $http({
            method: 'GET',
            url: '/api/StudentFileStatus/get/student_file_status_id='+$stateParams.student_file_status_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.student_file_status = response.data.data;
                if($scope.student_file_status.active && $scope.student_file_status.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    if($scope.action==="edit"){
        $scope.GetStudentFileStatus();
    }else{
        $scope.student_file_status = {};
        $scope.student_file_status.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.student_file_status.active = "Y";
        }else{
            $scope.student_file_status.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {student_file_status: $scope.student_file_status},
                url: '/api/StudentFileStatus/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/student_file_status");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.student_file_status);
            $http({
                method: 'POST',
                data: {student_file_status: $scope.student_file_status},
                url: '/api/StudentFileStatus/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/student_file_status/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



