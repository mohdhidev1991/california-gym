'use strict';

app.controller('TestAllController', function($scope, $http, $uibModal, $stateParams, $location, Notification, $filter) {
    
    
    $scope.tests = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100];
    $scope.GetTests = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;
        $http({
            method: 'GET',
            url: '/api/Test/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.tests = response.data.data;
                
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }

            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetTests();
    };


    $scope.delete = function(test_id){
        
        var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'ModalConfirmDelete.html',
        controller: 'ModalDeleteTestInstanceCtrl',
        resolve: {

            test_id: function() {
                return test_id;
            }
        
        }
        });
        modalInstance.result.then(function() {
            $scope.GetTests();
        }, function() {
            
        });
    
    };
    $scope.GetTests();
});



app.controller('ModalDeleteTestInstanceCtrl', function($scope, $uibModalInstance, $http, test_id, $location, Notification, $filter) {
    
    
    $scope.confirm_delete = function() {


        
        $http({
            method: 'POST',
            data: {test_id : test_id},
            url: '/api/Test/delete'
        }).then(function successCallback(response) {
            if(response.data.status){
                Notification.success({ message: $filter('translate')('Global.DataDeletedWithSuccess'), delay: 5000, positionX: 'right' });
                $uibModalInstance.close();
            }else{
                  angular.forEach(response.data.message, function(value, key) {
                  Notification.error({ message: $filter('translate')(value), delay: 5000, positionX: 'right' });
                });
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});





