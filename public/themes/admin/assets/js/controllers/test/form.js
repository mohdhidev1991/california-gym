'use strict';

app.controller('TestFormController', function($scope, $http, $stateParams, $location, $state, Notification, $filter) {

    $scope.test = null;
    $scope.action = $stateParams.action;
    $scope.failed_get_data = false;
    $scope.GetTest = function(){
        $http({
            method: 'GET',
            url: '/api/Test/get/Test_id=' + $stateParams.test_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.test = response.data.data;
            }else{
                $scope.failed_get_data = true;
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    
    
    if ($scope.action === "edit"){
        console.log('test');
        $scope.GetTest();
    } else{
        $scope.test = {};
        $scope.test.new = true;
    }

    $scope.save = function(back_location) {
        $scope.errors_form = null;
        $scope.success_form = null;
        
            $http({
                method: 'POST',
                data: { test: $scope.test },
                url: '/api/Test/save'
            }).then(function successCallback(response) {
                if(!response.data.status){
                    $scope.errors_form = response.data.message;
                }
                if (response.data.status && back_location) {
                    Notification.success({ message: $filter('translate')('Global.DataSavedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('test.all');
                }else if(response.data.status){
                    $scope.success_form = true;
                }else if(!response.data.status) {
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
    };
  
    

    $scope.add = function() {
        $scope.errors_form = null;
        $scope.success_form = null;

        console.log($scope.test);

            $http({
                method: 'POST',
                data: { test : $scope.test },
                url: '/api/Test/save'
            }).then(function successCallback(response) {
                if (response.data.status) {
                    Notification.success({ message: $filter('translate')('Global.DataAddedWithSuccess'), delay: 5000, positionX: 'right' });
                    $state.go('test.edit', { test_id: response.data.data });
                }else{
                    $scope.errors_form = response.data.message;
                }
            }, function errorCallback(response) {
                console.log('error');
            });
            
    };
    
    
});
