'use strict';

app.controller('TranslateAllController', function($scope, $http, $uibModal, $stateParams) {

    $scope.translates = null;
    $scope.filter_translate_module_id = null;
    $scope.success_update = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 50;
    $scope.optionsItemsPerPage = [10,50,100,500];

    $scope.GetTranslates = function() {
        $scope.success_update = null;

        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        if($scope.filter_translate_module_id){
            $scope.Params += ',translate_module_id='+$scope.filter_translate_module_id;
        }
        if($scope.key_search){
            $scope.Params += ',key_search='+encodeURI($scope.key_search);
        }
        
        $http({
            method: 'GET',
            url: '/api/Translate/multiget/'+$scope.Params
        }).then(function successCallback(response) {
            if(!response.data.status){
                $scope.totalItems = 0;
                $scope.no_results = true;
                $scope.translates = null;
            }else{
                $scope.no_results = null;
                $scope.translates = response.data.data;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.total;
                }
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetLanguages = function() {
        $http({
            method: 'GET',
            url: '/api/Language/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.languages = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    $scope.GetTranslateModules = function() {
        $http({
            method: 'GET',
            url: '/api/TranslateModule/get'
        }).then(function successCallback(response) {
            if (response.data.status) {
                $scope.translate_modules = response.data.data;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetLanguages();
    $scope.GetTranslateModules();

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetTranslates();
    };



    $scope.delete_translate = function(translate_module_id, meta_key) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteTranslateInstanceCtrl',
            resolve: {
                translate_module_id: function() {
                    return translate_module_id;
                },
                meta_key: function() {
                    return meta_key;
                },
            }
        });
        modalInstance.result.then(function() {
            $scope.GetTranslates();
        }, function() {
            
        });
    };


    $scope.multisave = function() {
        $scope.success_update = null;
        $scope.alerts = null;
        $http({
            method: 'POST',
            data: {translates: $scope.translates},
            url: '/api/Translate/multisave'
        }).then(function successCallback(response) {
            if(response.data.status){
                $scope.alerts = {};
                $scope.alerts.type = 'success';
                $scope.alerts.messages = response.data.message;
            }else{
                $scope.alerts = {};
                $scope.alerts.type = 'error';
                $scope.alerts.messages = response.data.message;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.add_translate_module = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddTranslateModule.html',
            controller: 'ModalAddTranslateModuleInstanceCtrl',
            resolve: {
                
            }
        });
        modalInstance.result.then(function() {
            $scope.GetTranslateModules();
            $scope.GetTranslates();
        }, function() {
            
        });
    };



    $scope.add_translate = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalAddTranslate.html',
            controller: 'ModalAddTranslateInstanceCtrl',
            resolve: {
                languages: function() { return $scope.languages; },
                translate_modules: function() { return $scope.translate_modules; },
                filter_translate_module_id: function() { return $scope.filter_translate_module_id; }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetTranslates();
        }, function() {
            
        });
    };


    $scope.GetTranslates();


});



app.controller('ModalAddTranslateModuleInstanceCtrl', function($scope, $uibModalInstance, $http) {
    $scope.translate_module = {new: true};
    $scope.add = function() {
        $scope.errors = null;
        $http({
            method: 'POST',
            data: {translate_module : $scope.translate_module},
            url: '/api/TranslateModule/save'
        }).then(function successCallback(response) {
            if(response.data.status){
                $uibModalInstance.close();
            }else{
                $scope.errors = response.data.message;
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



app.controller('ModalAddTranslateInstanceCtrl', function($scope, $uibModalInstance, $http, $location, languages, translate_modules, filter_translate_module_id) {
    
    $scope.languages = languages;
    $scope.translate_modules = translate_modules;

    $scope.translate = {new: true, translate_module_id: filter_translate_module_id};

    $scope.add = function(close_modal) {
        $scope.alerts = null;
        $http({
            method: 'POST',
            data: {translate : $scope.translate},
            url: '/api/Translate/save'
        }).then(function successCallback(response) {
            $scope.alerts = {};
            if(response.data.status){
                if(close_modal){
                    $uibModalInstance.close();
                }else{
                    $scope.alerts.messages = response.data.message;
                    $scope.alerts.messages.push('Translate.YouCanAddAnotherTranslation');
                    $scope.alerts.type = 'success';
                    $scope.success_add = true;
                    $scope.translate = {new: true, translate_module_id: filter_translate_module_id};
                }
            }else{
                $scope.alerts.messages = response.data.message;
                $scope.alerts.type = 'error';
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



app.controller('ModalDeleteTranslateInstanceCtrl', function($scope, $uibModalInstance, $http, $location, translate_module_id, meta_key) {
    

    $scope.confirm_delete = function() {
        $scope.alerts = null;
        $http({
            method: 'POST',
            data: {translate_module_id : translate_module_id, meta_key: meta_key },
            url: '/api/Translate/delete'
        }).then(function successCallback(response) {
            $scope.alerts = {};
            if(response.data.status){
                $scope.alerts.messages = response.data.message;
                $scope.alerts.type = 'success';
                $uibModalInstance.close();
            }else{
                $scope.alerts.messages = response.data.message;
                $scope.alerts.type = 'error';
            }
            
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


