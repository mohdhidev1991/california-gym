'use strict';

app.controller('TServiceAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.tservices = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetTServices = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/TService/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.tservices) {
                $scope.tservices = response.data.data.tservices;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetTServices();
    };



    $scope.delete_tservice = function(tservice_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteTServiceInstanceCtrl',
            resolve: {
                tservice_id: function() {
                    return tservice_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetTServices();
        }, function() {
            
        });
    };


    $scope.GetTServices();


});



app.controller('ModalDeleteTServiceInstanceCtrl', function($scope, $uibModalInstance, $http, tservice_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {tservice_id : tservice_id},
            url: '/api/TService/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

