'use strict';

app.controller('TServiceFormController', function($scope, $http, $stateParams, $location) {

    $scope.tservice = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.schools  = null;


    $scope.GetTService = function() {
        $http({
            method: 'GET',
            url: '/api/TService/get/tservice_id='+$stateParams.tservice_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.tservice = response.data.data;

                if($scope.tservice.active && $scope.tservice.active==="Y"){
                    $scope.active_switch = true;
                }


            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };





    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.GetWeekTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/WeekTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.week_templates.length) {
                $scope.week_templates = response.data.data.week_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetTService();
    }else{
        $scope.tservice = {};
        $scope.tservice.new = true;
    }
    $scope.GetSchools();
    $scope.GetWeekTemplates();

    $scope.pre_save = function(){

        if($scope.active_switch===true){
            $scope.tservice.active = "Y";
        }else{
            $scope.tservice.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {tservice: $scope.tservice},
                url: '/api/TService/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/tservices");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {tservice: $scope.tservice},
                url: '/api/TService/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/tservice/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



