'use strict';

app.controller('WdayAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.wdays = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 10;
    $scope.optionsItemsPerPage = [10, 50, 100, 500];

    $scope.GetWdays = function() {

        $scope.Params = 'active=all,limit=' + $scope.itemsPerPage;
        $scope.Params += ',page=' + $scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/Wday/get/' + $scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.wdays) {
                $scope.wdays = response.data.data.wdays;
                if ($scope.currentPage === 1) {
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function() {
        $scope.currentPage = 1;
        $scope.GetWdays();
    };



    $scope.delete_wday = function(wday_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteWdayInstanceCtrl',
            resolve: {
                wday_id: function() {
                    return wday_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetWdays();
        }, function() {

        });
    };


    $scope.GetWdays();


});



app.controller('ModalDeleteWdayInstanceCtrl', function($scope, $uibModalInstance, $http, wday_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: { wday_id: wday_id },
            url: '/api/Wday/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });

    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});