'use strict';

app.controller('WdayFormController', function($scope, $http, $stateParams, $location) {

    $scope.wday = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;


    $scope.GetWday = function() {
        $http({
            method: 'GET',
            url: '/api/Wday/get/wday_id='+$stateParams.wday_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.wday = response.data.data;
                if($scope.wday.active && $scope.wday.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };


    

    if($scope.action==="edit"){
        $scope.GetWday();
    }else{
        $scope.wday = {};
        $scope.wday.new = true;
    }


    $scope.pre_save = function(){
        if($scope.active_switch===true){
            $scope.wday.active = "Y";
        }else{
            $scope.wday.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {wday: $scope.wday},
                url: '/api/Wday/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/wdays");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            console.log($scope.wday);
            $http({
                method: 'POST',
                data: {wday: $scope.wday},
                url: '/api/Wday/save'
            }).then(function successCallback(response) {
                console.log(response);
                if(response.data.data.id){
                    $location.path("/wday/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



