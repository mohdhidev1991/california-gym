'use strict';

app.controller('WeekTemplateAllController', function($scope, $http, $uibModal, $stateParams, $location) {

    $scope.week_templates = null;
    $scope.totalItems = null;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5;
    $scope.optionsItemsPerPage = [2,5,10,50,100];

    $scope.GetWeekTemplates = function() {
        
        $scope.Params = 'active=all,limit='+$scope.itemsPerPage;
        $scope.Params += ',page='+$scope.currentPage;

        $http({
            method: 'GET',
            url: '/api/WeekTemplate/get/'+$scope.Params
        }).then(function successCallback(response) {
            if (response.data.data.week_templates) {
                $scope.week_templates = response.data.data.week_templates;
                if($scope.currentPage===1){
                    $scope.totalItems = response.data.data.total;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };

    $scope.change_itemsPerPage = function(){
        $scope.currentPage = 1;
        $scope.GetWeekTemplates();
    };



    $scope.delete_week_template = function(week_template_id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalConfirmDelete.html',
            controller: 'ModalDeleteWeekTemplateInstanceCtrl',
            resolve: {
                week_template_id: function() {
                    return week_template_id;
                }
            }
        });
        modalInstance.result.then(function() {
            $scope.GetWeekTemplates();
        }, function() {
            
        });
    };


    $scope.GetWeekTemplates();


});



app.controller('ModalDeleteWeekTemplateInstanceCtrl', function($scope, $uibModalInstance, $http, week_template_id, $location) {
    $scope.confirm_delete = function() {
        $http({
            method: 'POST',
            data: {week_template_id : week_template_id},
            url: '/api/WeekTemplate/delete'
        }).then(function successCallback(response) {
            $uibModalInstance.close();
        }, function errorCallback(response) {
            console.log('error');
        });
        
    };
    $scope.cancel_confirm_delete = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

