'use strict';

app.controller('WeekTemplateFormController', function($scope, $http, $stateParams, $location) {

    $scope.week_template = null;
    $scope.active_switch = false;
    $scope.action = $stateParams.action;
    $scope.schools = null;
    $scope.level_classes = null;
    $scope.day_templates = null;


    $scope.GetWeekTemplate = function() {
        $http({
            method: 'GET',
            url: '/api/WeekTemplate/get/week_template_id='+$stateParams.week_template_id
        }).then(function successCallback(response) {
            if (response.data.data) {
                $scope.week_template = response.data.data;
                if($scope.week_template.active && $scope.week_template.active==="Y"){
                    $scope.active_switch = true;
                }
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };





    $scope.GetSchools = function() {
        $http({
            method: 'GET',
            url: '/api/School/get'
        }).then(function successCallback(response) {
            if (response.data.data.schools.length) {
                $scope.schools = response.data.data.schools;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetLevelClasses = function() {
        $http({
            method: 'GET',
            url: '/api/LevelClass/get'
        }).then(function successCallback(response) {
            if (response.data.data.level_classes.length) {
                $scope.level_classes = response.data.data.level_classes;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };



    $scope.GetDayTemplates = function() {
        $http({
            method: 'GET',
            url: '/api/DayTemplate/get'
        }).then(function successCallback(response) {
            if (response.data.data.day_templates.length) {
                $scope.day_templates = response.data.data.day_templates;
            }
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    

    if($scope.action==="edit"){
        $scope.GetWeekTemplate();
    }else{
        $scope.week_template = {};
        $scope.week_template.new = true;
    }
    $scope.GetSchools();
    $scope.GetLevelClasses();
    $scope.GetDayTemplates();

    $scope.pre_save = function(){

        if($scope.active_switch===true){
            $scope.week_template.active = "Y";
        }else{
            $scope.week_template.active = "N";
        }
        return true;
    };

    $scope.save = function(back_location){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {week_template: $scope.week_template},
                url: '/api/WeekTemplate/save'
            }).then(function successCallback(response) {
                if(back_location){
                    $location.path("/week_templates");
                }else{
                    console.log(response);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };


    $scope.add = function(){
        if($scope.pre_save()){
            $http({
                method: 'POST',
                data: {week_template: $scope.week_template},
                url: '/api/WeekTemplate/save'
            }).then(function successCallback(response) {
                if(response.data.data.id){
                    $location.path("/week_template/edit/"+response.data.data.id);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        }
    };
});



