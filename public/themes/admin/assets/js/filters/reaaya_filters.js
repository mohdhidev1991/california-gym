'use strict';

/* Filters */
// need load the moment.js to use this filter. 
angular.module('app')
    .filter('translate_symbol', function($filter) {
        return function(symbol) {
            return $filter('translate')('Symbol.' + symbol);
        };
    });



// need load the moment.js to use this filter. 
angular.module('app')
.filter('filter_name_by_id', function() {
    return function(id_, objs, field) {
        if(!objs){
          return null;
        }
        for (var i = 0; i < objs.length; i++) {
            if (!objs[i]['id'] || !objs[i][field]) {
                break;
            }
            if (id_ === objs[i]['id']) {
                id_ = objs[i][field];
                break;
            }
        }
        return id_;
    };
});




/* used in ng-loop */
angular.module('app').filter('range', function() {
    return function(input, start, end) {
        if(!start) return [];
        if(!end) return [];

        start = parseInt(start);
        end = parseInt(end);

        for (var i = start; i <= end; i++) {
            input.push(i);
        }

        return input;
    };
});




angular.module('app').filter("dateformat", function() {
    return function(date, format) {
        if(!date) return null;
        return moment(date.toString(), 'YYYYMMDD').format(format);
    };
});

