'use strict';

var CheckboxObject = CheckboxObject || {};

CheckboxObject.helper = {
    check_selected_option : function(id, $model) {
        if (!$model) return false;
        return $model.indexOf(id) !== -1;
    },
    toggle_checkbox_option : function(id, $model) {
    	if( !$model ) $model = [];
        var index_to_remove = $model.indexOf(id);
        if (index_to_remove > -1) {
            $model.splice(index_to_remove, 1);
        } else {
            $model.push(id);
        }
    }
};