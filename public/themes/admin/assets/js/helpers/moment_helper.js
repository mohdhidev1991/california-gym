'use strict';


moment.__proto__.yearWeek = function(date) {
    if (!date) { date = moment(); }
    
    return (parseInt(moment(date).format('M')) === 1 && moment(date).isoWeek() > 1) ? parseInt(moment(date).format('YYYY')) - 1 : parseInt(moment(date).format('YYYY'));
};