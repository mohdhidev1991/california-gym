'use strict';

/* Controllers */



angular.module('app')
    .controller('AppCtrl', function($scope, $translate, $localStorage, $window, $css, $rootScope, $http, $cookies, $state, permissions, $location) {
        // add 'ie' classes to html
        var isIE = !!navigator.userAgent.match(/MSIE/i);
        if (isIE) {
            angular.element($window.document.body).addClass('ie');
        }
        if (isSmartDevice($window)) {
            angular.element($window.document.body).addClass('smart');
        }

        // config
        $rootScope.app = {
            name: 'California Gym',
            version: '1.0.1',
            // for chart colors
            color: {
                primary: '#7266ba',
                info: '#23b7e5',
                success: '#27c24c',
                warning: '#fad733',
                danger: '#f05050',
                light: '#e8eff0',
                dark: '#3a3f51',
                black: '#1c2b36'
            },
            settings: {
                themeID: 1,
                navbarHeaderColor: 'bg-black',
                navbarCollapseColor: 'bg-white-only',
                asideColor: 'bg-black',
                headerFixed: true,
                asideFixed: false,
                asideFolded: false,
                asideDock: false,
                container: false
            }
        };

        // save settings to local storage
        if (angular.isDefined($localStorage.settings)) {
            $rootScope.app.settings = $localStorage.settings;
        } else {
            $localStorage.settings = $rootScope.app.settings;
        }
        $scope.$watch('app.settings', function() {
            if ($rootScope.app.settings.asideDock && $rootScope.app.settings.asideFixed) {
                // aside dock and fixed must set the header fixed.
                $rootScope.app.settings.headerFixed = true;
            }
            // for box layout, add background image
            $rootScope.app.settings.container ? angular.element('html').addClass('bg') : angular.element('html').removeClass('bg');
            // save to local storage
            $localStorage.settings = $rootScope.app.settings;
        }, true);

        // angular translate
        $scope.lang = {
            isopen: false
        };
        $scope.langs = {
            en: 'English',
            fr: 'Français',
            //ar: 'العربية',
        };
        $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || $scope.langs['ar'];
        $scope.selectLangKey = $translate.proposedLanguage();
        $rootScope.app.settings.selectLang = $translate.proposedLanguage();

        if ($scope.selectLangKey == 'ar') {
            $css.add(['/themes/admin/assets/libs/assets/bootstrap-rtl/dist/css/bootstrap-rtl.min.css', '/themes/admin/assets/css/app.rtl.css', '/themes/admin/assets/css/admin.reaaya.rtl.css']);
        } else {
            $css.remove(['/themes/admin/assets/libs/assets/bootstrap-rtl/dist/css/bootstrap-rtl.min.css', '/themes/admin/assets/css/app.rtl.css', '/themes/admin/assets/css/admin.reaaya.rtl.css']);
        }

        $scope.setLang = function(langKey, $event) {
            // set the current lang
            $scope.selectLang = $scope.langs[langKey];
            $scope.selectLangKey = langKey;
            // You can change the language during runtime
            $translate.use(langKey);
            $scope.lang.isopen = !$scope.lang.isopen;
            $rootScope.app.settings.selectLang = $translate.proposedLanguage();

            if (langKey === 'ar') {
                $scope.is_rtl = true;
                $css.add(['/themes/admin/assets/libs/assets/bootstrap-rtl/dist/css/bootstrap-rtl.min.css', '/themes/admin/assets/css/app.rtl.css', '/themes/admin/assets/css/admin.reaaya.rtl.css']);
            } else {
                $scope.is_rtl = false;
                $css.remove(['/themes/admin/assets/libs/assets/bootstrap-rtl/dist/css/bootstrap-rtl.min.css', '/themes/admin/assets/css/app.rtl.css', '/themes/admin/assets/css/admin.reaaya.rtl.css']);
            }
        };

        function isSmartDevice($window) {
            // Adapted from http://www.detectmobilebrowsers.com
            var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
            // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
            return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        }




        $scope.init_rea_user_data = function() {
            $http({
                method: 'GET',
                url: '/api/ReaUser/me/'
            }).then(function successCallback(response) {
                if (response.data.data) {
                    $scope.rea_user = response.data.data;
                    var rea_user = $cookies.getObject('rea_user');
                    $cookies.putObject('rea_user', $scope.rea_user);
                }
            }, function errorCallback(response) {
                console.log('error');
            });
        };
        $scope.init_rea_user_data();



    });










