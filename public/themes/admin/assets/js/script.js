(function() {
  //declare all modules and their dependencies.
  angular.module('multipleSelectDemo', [
    'ui.router',
    'multipleSelect'
  ]).config(function() {

  })
  .controller('controller', function($scope) {

    $scope.skillsList = [{
      id: 1,
      name: "Java"
    }, {
      id: 2,
      name: "C"
    }, {
      id: 3,
      name: "C++"
    }, {
      id: 4,
      name: "Core Java"
    }, {
      id: 5,
      name: "JavaScript"
    }, {
      id: 6,
      name: "PHP"
    }, {
      id: 7,
      name: "HTML"
    }, {
      id: 8,
      name: "CSS"
    }, {
      id: 9,
      name: "Angular Js"
    }, {
      id: 10,
      name: "Bootstrap"
    }];

    $scope.skillsList1 = [
      "Java",
      "C",
      "C++",
      "Core Java",
      "Javascript",
      "PHP",
      "MySql",
      "Hibernate",
      "Spring",
      "AngularJs",
      "BackboneJs",
      "Sencha Touch",
      "ExtJs"
    ];
    
    $scope.onSubmit = function() {
      console.log("submit");
      if ($scope.multipleSelectForm.$invalid) {
        if ($scope.multipleSelectForm.$error.required != null) {
          $scope.multipleSelectForm.$error.required.forEach(function(element) {
            element.$setDirty();
          });
        }
        return null;
      }
      alert("Field is Valid");
    };

// Init
$scope.testVal = ''; // Test any any text 

    $scope.beforeSelectItem = function(item) {

      // perform operation on this item before selecting it.
      $scope.testVal = 'Passed - Before Select Item';
    };

    $scope.afterSelectItem = function(item) {
      // perform operation on this item after selecting it.
      $scope.testVal = 'Passed - After Select Item';
    };

    $scope.beforeRemoveItem = function(item) {
      // perform operation on this item before removing it.
      $scope.testVal = 'Passed - Before Remove Item';
    };

    $scope.afterRemoveItem = function(item) {
      // perform operation on this item after removing it.
      $scope.testVal = 'Passed - After Remove Item';
    };

  });

})();



