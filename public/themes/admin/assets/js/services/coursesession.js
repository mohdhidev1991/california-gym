'use strict';


app.factory('CourseSessionFactory', function () {
    var data_ = null;
    return {
        get: function () {
            return data_;
        },
        set: function (coursesession) {
            data_ = coursesession;
        }
    };
});


