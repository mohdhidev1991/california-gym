'use strict';


angular.module('app').service('notifications', function($http, $rootScope) {
    this.get_notifications = function(){
        $http({
            method: 'GET',
            url: '/api/ReaUser/notifications'
        }).then(function successCallback(response) {
            $rootScope.notifications = null;
            if (response.data.data.notifications) {
                $rootScope.notifications = response.data.data.notifications;
            }
            return null;
        }, function errorCallback(response) {
            console.log('error');
        });
    };
    return this;
});
