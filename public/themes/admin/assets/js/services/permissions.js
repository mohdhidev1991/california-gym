angular.module('app').directive('hasPermission', function(permissions) {
    return {
        link: function(scope, element, attrs) {
            if (!_.isString(attrs.hasPermission)) {
                throw 'hasPermission value must be a string';
            }
            var value = attrs.hasPermission.trim();
            var notPermissionFlag = value[0] === '!';
            if (notPermissionFlag) {
                value = value.slice(1).trim();
            }

            function toggleVisibilityBasedOnPermission() {
                var hasPermission = permissions.hasPermission(value);
                if (hasPermission && !notPermissionFlag || !hasPermission && notPermissionFlag) {
                    element.show();
                } else {
                    element.hide();
                }
            }
            toggleVisibilityBasedOnPermission();
            scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
        }
    };
});

angular.module('app')
    .factory('permissions', function($rootScope) {
        var permissionList;
        return {
            setPermissions: function(permissions) {
                permissionList = permissions;
                $rootScope.$broadcast('permissionsChanged');
            },
            hasPermission: function(permission) {
                permission = permission.trim();
                return _.some(permissionList, function(item) {
                    if (_.isString(item)) {
                        return item.trim() === permission;
                    }
                });
            }
        };
    });

app.run(function(permissions, $http, $rootScope, $location) {
    $http({
        method: 'GET',
        url: '/api/ReaUser/me/'
    }).then(function successCallback(response) {
        if (response.data.data && response.data.data.permissions) {
            permissions.setPermissions(response.data.data.permissions);
        }
        $rootScope.$on('$stateChangeStart', function(scope, next, current) {
            var permission = next.permission;
            if (_.isString(permission) && !permissions.hasPermission(permission)) {
                $location.path('/unauthorized');
            }
        });
    }, function errorCallback(response) {
        console.log('error');
    });

});