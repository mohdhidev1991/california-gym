'use strict';
(function(ng) {
    'use strict';

    var app = ng.module('islamicDatepicker', []);

    app.directive('islamicDatepicker', function($parse, $cookies, DateSystem) {


        return {
            restrict: "E",
            replace: true,
            transclude: false,
            compile: function(element, attrs) {
                var modelAccessor = $parse(attrs.ngModel);
                var ngModelHdate = $parse(attrs.ngModelHdate);
                var required = $parse(attrs.ngRequired);
                var IsHijri = DateSystem.isHijriCalendar();



                var html = '<input type="text" ng-model="' + modelAccessor + '" id="' + attrs.id + '" class="form-control" />';

                var newElem = $(html);
                element.replaceWith(newElem);

                if (required) {
                    element.attr('ng-required', true);
                }

                return function(scope, element, attrs, controller) {
                    var calendar = null;
                    if (IsHijri) {
                        calendar = $.calendars.instance('ummalqura', 'ar');
                    } else {
                        calendar = $.calendars.instance('gregorian', 'ar');
                    }

                    element.calendarsPicker({
                        calendar: calendar,
                        dateFormat: 'yyyy/mm/dd',
                        minDay: '+10',
                        onSelect: function(dates) {
                            scope.$apply(function(scope) {
                                var new_hdate = null;
                                if (IsHijri) {
                                    var hdate = dates[0].toString();
                                    var hyear = number_formatat(moment(hdate, 'iYYYY/iM/iD').format('iYYYY'));
                                    var hmonth = number_formatat(moment(hdate, 'iYYYY/iM/iD').format('iM'));
                                    var hday = number_formatat(moment(hdate, 'iYYYY/iM/iD').format('iD'));
                                    new_hdate = hyear + '' + hmonth + '' + hday;
                                } else {
                                    var gdate = dates[0].toString();
                                    var hyear = number_formatat(moment(gdate, 'YYYY/MM/DD').format('iYYYY'));
                                    var hmonth = number_formatat(moment(gdate, 'YYYY/MM/DD').format('iM'));
                                    var hday = number_formatat(moment(gdate, 'YYYY/MM/DD').format('iD'));
                                    new_hdate = hyear + '' + hmonth + '' + hday;
                                }
                                ngModelHdate.assign(scope, new_hdate);
                            });
                        },
                        onChange: function(dates) {
                            console.log(dates);
                        }
                    });
                    element.on('change', function() {
                        if (!element.val() || element.val() === '') {
                            scope.$apply(function(scope) {
                                ngModelHdate.assign(scope, null);
                            });
                        }
                    });

                    function number_formatat(myNumber) {
                        return (myNumber.toString().length < 2) ? "0" + myNumber.toString() : myNumber.toString();
                    }

                    scope.$watch(modelAccessor, function(val) {
                        if (val) {
                            var data_ = val.toString().slice(0, 4) + '/' + val.toString().slice(4, 6) + '/' + val.toString().slice(6, 8);
                            if (!IsHijri) {
                                data_ = moment(data_, 'iYYYY-iM-iD').format('YYYY/MM/DD');
                            }
                            element.val(data_);
                        } else {
                            element.val("");
                        }
                    });

                };

            }
        };
    });


}(angular));