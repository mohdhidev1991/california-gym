﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <title>jQuery Calendars Datepicker</title>
    
    <link rel="stylesheet" href="jquery.calendars.picker.css">
    <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../bootstrap/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script src="jquery.plugin.js"></script>
    <script src="jquery.calendars.js"></script>
    <script src="jquery.calendars.plus.js"></script>
    <script src="jquery.calendars.picker.js"></script>
    <script src="jquery.calendars.islamic.js"></script>
    <script src="jquery.calendars.islamic-ar.js"></script>

    

    <style type="text/css">
    .calendars-month {
	    float: none;
	    width: auto;
	}
	.calendars {
		max-width: 250px;
	}
</style>
</head>

<body>

    <div ng-app="docsSimpleDirective" ng-controller="Controller">
        <my-datepicker type="text" ng-model="user.dateOfBirth" />
    </div>


    <script>


    var app = angular.module('docsSimpleDirective', []);
    app.controller('Controller', ['$scope', function($scope) {
        $scope.customer = {
        name: 'Naomi',
            address: '1600 Amphitheatre'
        };

        $scope.user = {
            dateOfBirth: new Date(1970, 0, 1)
        }
    }]);
    app.directive('myDatepicker', function ($parse) {
           return {
              restrict: "E",
              replace: true,
              transclude: false,
              compile: function (element, attrs) {
                 var modelAccessor = $parse(attrs.ngModel);

                 var html = "<input type='text' id='" + attrs.id + "' >" +
                    "</input>";

                 var newElem = $(html);
                 element.replaceWith(newElem);

                 return function (scope, element, attrs, controller) {
                        var calendar = $.calendars.instance('islamic','ar');
                        element.calendarsPicker({
                            calendar: calendar
                        });
                    return;


                    var processChange = function () {
                       var date = new Date(element.datepicker("getDate"));

                       scope.$apply(function (scope) {
                          // Change bound variable
                          modelAccessor.assign(scope, date);
                       });
                    };
                    element.addClass('xx');
                    console.log(element);
                    return;
                    element.datepicker({
                       inline: true,
                       onClose: processChange,
                       onSelect: processChange
                    });

                    scope.$watch(modelAccessor, function (val) {
                       var date = new Date(val);
                       element.datepicker("setDate", date);
                    });

                 };

              }
           };
        });

    </script>


</body>

</html>