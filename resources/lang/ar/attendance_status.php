<?php
return [
    'good_quitted' => 'انصراف في الموعد',
    'early_quitted' => 'انصراف مبكر',
    'on_time' => 'حضور في الموعد',
    'on_late' => 'حضور متأخر',
    'absent' => 'غياب',
    'not_called_yet' => 'لم يتم نداءه',
    'waiting_he_come' => 'لم يحضر بعد',
];
