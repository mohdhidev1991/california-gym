<?php
return [
    'morning_period' => 'فترة صباحية',
    'night_period' => 'فترة ليلية',
    'evening_period' => 'فترة مسائية',
];
