<?php
return [
    'primary' => 'ابتدائي',
    'secondary' => 'ثانوي',
    'university' => 'جامعي',
    'middle' => 'متوسط',
];
