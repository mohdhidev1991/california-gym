<?php
return [
    'coming_session' => 'حصة قادمة',
    'closed_session' => 'حصة مغلقة',
    'current_session' => 'حصة مفتوحة',
    'canceled_session' => 'حصة ملغاة ',
];
