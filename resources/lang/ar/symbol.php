<?php
/**
 * Created by PhpStorm.
 * User: Aziz Trabelsi
 * Date: 8/12/2016
 * Time: 11:11 AM
 */

return [
    'a' => 'أ',
    'b' => 'ب',
    'c' => 'ت',
    'd' => 'ث',
    'e' => 'ج',
    'f' => 'ح',
    'i' => 'خ',
    'j' => 'د',
    'k' => 'ذ',
    'l' => 'ر',
];