<?php
return [
    'failed' => 'لم يجتز',
    'excellent' => 'متفوق',
    'advanced' => 'متقدم',
    'skilled' => 'متمكن',
];
