<?php
/**
 * Created by PhpStorm.
 * User: Aziz Trabelsi
 * Date: 8/12/2016
 * Time: 11:10 AM
 */

return [
    'a' => 'A',
    'b' => 'B',
    'c' => 'C',
    'd' => 'D',
    'e' => 'E',
    'f' => 'F',
    'i' => 'I',
    'j' => 'J',
    'k' => 'K',
    'l' => 'L',
];