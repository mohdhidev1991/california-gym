@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'alerts.store']) !!}

        @include('alerts.fields')

    {!! Form::close() !!}
</div>
@endsection
