@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($alert, ['route' => ['alerts.update', $alert->id], 'method' => 'patch']) !!}

        @include('alerts.fields')

    {!! Form::close() !!}
</div>
@endsection
