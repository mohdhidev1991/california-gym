@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Alerts</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('alerts.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($alerts->isEmpty())
                <div class="well text-center">No Alerts found.</div>
            @else
                @include('alerts.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $alerts])


    </div>
@endsection
