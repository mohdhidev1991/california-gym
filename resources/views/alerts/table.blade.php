<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Alert Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($alerts as $alert)
        <tr>
            <td>{!! $alert->id !!}</td>
			<td>{!! $alert->creation_user_id !!}</td>
			<td>{!! $alert->creation_date !!}</td>
			<td>{!! $alert->update_user_id !!}</td>
			<td>{!! $alert->update_date !!}</td>
			<td>{!! $alert->validation_user_id !!}</td>
			<td>{!! $alert->validation_date !!}</td>
			<td>{!! $alert->active !!}</td>
			<td>{!! $alert->version !!}</td>
			<td>{!! $alert->update_groups_mfk !!}</td>
			<td>{!! $alert->delete_groups_mfk !!}</td>
			<td>{!! $alert->display_groups_mfk !!}</td>
			<td>{!! $alert->sci_id !!}</td>
			<td>{!! $alert->alert_name !!}</td>
            <td>
                <a href="{!! route('alerts.edit', [$alert->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('alerts.delete', [$alert->id]) !!}" onclick="return confirm('Are you sure wants to delete this Alert?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
