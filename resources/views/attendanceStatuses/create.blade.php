@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'attendanceStatuses.store']) !!}

        @include('attendanceStatuses.fields')

    {!! Form::close() !!}
</div>
@endsection
