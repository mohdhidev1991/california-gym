@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($attendanceStatus, ['route' => ['attendanceStatuses.update', $attendanceStatus->id], 'method' => 'patch']) !!}

        @include('attendanceStatuses.fields')

    {!! Form::close() !!}
</div>
@endsection
