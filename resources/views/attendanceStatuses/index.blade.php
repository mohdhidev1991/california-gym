@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">AttendanceStatuses</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('attendanceStatuses.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($attendanceStatuses->isEmpty())
                <div class="well text-center">No AttendanceStatuses found.</div>
            @else
                @include('attendanceStatuses.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $attendanceStatuses])


    </div>
@endsection
