@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'attendanceTypes.store']) !!}

        @include('attendanceTypes.fields')

    {!! Form::close() !!}
</div>
@endsection
