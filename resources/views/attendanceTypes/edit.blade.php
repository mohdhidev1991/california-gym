@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($attendanceType, ['route' => ['attendanceTypes.update', $attendanceType->id], 'method' => 'patch']) !!}

        @include('attendanceTypes.fields')

    {!! Form::close() !!}
</div>
@endsection
