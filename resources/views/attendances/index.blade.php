@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Attendances</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('attendances.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($attendances->isEmpty())
                <div class="well text-center">No Attendances found.</div>
            @else
                @include('attendances.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $attendances])


    </div>
@endsection
