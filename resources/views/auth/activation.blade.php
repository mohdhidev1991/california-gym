@extends('layouts.app_no_container_js')

@section('js_footer')
    @parent
@endsection


@section('content')
    <div class="wrapper" >
        @if ($invalide)
            <div class="alert alert-danger" translate="User.TokenInvalideOrExipred" ></div>
        @endif

        @if ($exipred)
            <div class="alert alert-danger" translate="User.TokenInvalideOrExipred" ></div>
        @endif

        @if ($already_active)
            <div class="alert alert-warning" translate="User.YourAccountIsAlreadyActive" ></div>
        @endif

        @if ($activation)
            <div class="alert alert-success" translate="User.YourAccountIsActiveNow" ></div>
        @endif


        <div class="text-center" >
            <a href="{!! url('/') !!}" class="btn btn-primary" <i class="fa fa-btn fa-home"></i><span translate="Global.BackToHome" ></span></a>
        </div>
    </div>
@endsection
