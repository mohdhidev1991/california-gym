@extends('layouts.app_no_container_js')


@section('js_footer')
    @parent
    <script src="{!! URL::asset("themes/admin/assets/js/controllers/rea_user/login.js") !!}"></script>
@endsection



@section('content')
    <div class="wrapper text-center">
      <strong translate="Global.ManagementPlanning" ></strong>
    </div>

    <div class="m-b-lg">
        <form class="form-register form-validation form-login ng-pristine ng-valid" name="form" role="form" ng-controller="LoginFormController" novalidate>
            
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">


            <div class="list-group list-group-sm">
                <div class="list-group-item {!! isset($errors) && $errors->has('email') ? ' has-error' : '' !!}">
                    <input type="text" placeholder="{{ 'Field.Login' | translate }}" ng-required="true" class="form-control no-border" name="username" value="" ng-model="rea_user.username" />
                </div>
                <div class="list-group-item" >
                    <input type="password" ng-required="true" placeholder="{{ 'Field.Password' | translate }}" class="form-control no-border" name="pwd" ng-model="rea_user.pwd" />
                </div>
            </div>
            <div class="checkbox m-b-md m-t-none">
                <label class="i-checks">
                    <input type="checkbox" ng-model="rea_user.remember" name="remember" ><i></i> <span translate="User.RememberMe" ></span></a>
                </label>
            </div>


            <div class="alert alert-danger" ng-show="failed_auth" >
                <span translate="User.FailedLoginCheckUsernameAndPasswordOrContactTheApplicationAdministrator" ng-show="failed_auth" ></span>
            </div>

            <div class="list-group" >
                <span uib-tooltip="{{ 'User.PleaseEnterYourLoginAndPassword' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid" type="submit" ng-click="login()" class="btn btn-lg btn-primary btn-block" >
                        <i class="fa fa-btn fa-sign-in"></i>
                        <span translate="User.SignIn" ></span>
                    </button>
                </span>
            </div>
        </form>
    </div>
@endsection