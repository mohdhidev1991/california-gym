@extends('layouts.app_no_container_js')

@section('js_footer')
    @parent
    <script src="{!! URL::asset("themes/admin/assets/js/controllers/rea_user/reset_password.js") !!}"></script>
@endsection


@section('content')
    <div class="m-b-lg" ng-controller="ResetPasswordFormController"  >
        
        <form class="form-reset-password" role="form" name="form" ng-hide="success_send_reset_password" >

            <div class="wrapper text-center">
                <strong translate="User.InputYourEmailToResetYourPassword" ></strong>
            </div>

            <div class="list-group list-group-sm">
                <div class="list-group-item" >
                    <input name="email" type="email" placeholder="{{User.Email}}" value="" ng-model="rea_user.email" class="form-control no-border" required>
                </div>
            </div>


            <div class="list-group" >
                <div
                    vc-recaptcha
                    theme="'light'"
                    key="model.key"
                    on-create="setWidgetId(widgetId)"
                    on-success="setResponse(response)"
                    on-expire="cbExpiration()"
                ></div>
            </div>

            <div class="alert alert-danger" ng-show="errors" >
                <ul ng-repeat="error in errors" >
                    <li><span translate="{{error}}" ></span></li>
                </ul>
                
            </div>

            
            <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                <button type="submit" ng-click="reset_password()" ng-disabled="form.$invalid" class="btn btn-lg btn-primary btn-block" >
                    <i class="fa fa-btn fa-envelope"></i>
                    <span translate="User.SendPasswordResetLink" ></span>
                </button>
            </span>
            
            <div class="text-center m-t m-b">
                <a href="{!! url('/login') !!}" >
                    <i class="fa fa-btn fa-sign-in"></i>
                    <span translate="User.SignIn" ></span>
                </a>
            </div>

            {!! csrf_field() !!}
        </form>

        <div class="wrapper" ng-show="success_send_reset_password" >
            <div class="alert alert-success" >
                <span translate="User.YouWillReceiveALinkInYourEmailAddressToResetPasswordCheckItNow" ></span>
            </div>
        </div>

        
    </div>
@endsection