@extends('layouts.app_no_container_js')

@section('js_footer')
    @parent
    <script src="{!! URL::asset("themes/admin/assets/js/controllers/rea_user/register.js") !!}"></script>
@endsection


@section('content')

<div class="wrapper text-center">
  <strong translate="User.CreateAnAccount" ></strong>
</div>

<div ng-controller="RegisterFormController" >
    <form class="form-register" name="form" role="form" ng-show="!success_registration" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="text-danger wrapper text-center" ng-show="authError">
        </div>
        <div class="list-group list-group-sm">
            
            <div class="list-group-item" ng-class="{'has-error': !rea_user.email}" >
                <input ng-model="rea_user.email" ng-required="true" type="email" class="form-control no-border" placeholder="{{ 'Field.Email' | translate }} *" >
            </div>


            <div class="list-group-item" ng-class="{'has-error': !rea_user.mobile}" >
                <input ng-model="rea_user.mobile" ng-required="true" type="text" class="form-control no-border" placeholder="{{ 'Field.Mobile' | translate }} *" >
            </div>


            <div class="list-group-item" ng-class="{'has-error': !rea_user.pwd}" >
                <input ng-model="rea_user.pwd" name="pwd" ng-required="true" type="password" class="form-control no-border" placeholder="{{ 'Field.Password' | translate }} *" >
            </div>

            <div class="list-group-item" ng-class="{'has-error': !rea_user.pwd2}" >
                <input ng-model="rea_user.pwd2" name="pwd2" ng-required="rea_user.pwd" ui-validate=" ' ($value==rea_user.pwd) || (!rea_user.pwd && !rea_user.pwd2)' " type="password" class="form-control no-border" placeholder="{{ 'Field.RepeatPassword' | translate }} *" >
            </div>

        </div>

        <div class="alert alert-warning" ng-show="rea_user.pwd.length<6" >
                <span translate="Form.PleaseEnterStrongPasswordMin6AndRepeatTheNewPassword" ></span>
        </div>
        
        <div class="alert alert-danger" ng-show="form.pwd2.$error.validator" >
            <span translate="Form.PasswordsNotTheSame" ></span>
        </div>

        <div class="checkbox m-b-md m-t-none">
            <label class="i-checks">
                <input type="checkbox" ng-model="agree" id="agree" required><i></i><label for="agree"><a href="terms-and-policy" target="_blank" translate="Form.IAgreeToTheReaayaTermsAndConditions" ></a></label>
            </label>
        </div>


        <div class="list-group" >
            <div
                vc-recaptcha
                theme="'light'"
                key="model.key"
                on-create="setWidgetId(widgetId)"
                on-success="setResponse(response)"
                on-expire="cbExpiration()"
            ></div>
        </div>
        
        <div class="alert alert-danger" ng-show="failed_register || failed_recaptcha" >
            <span translate="ReaUser.PleaseValideYourAreNotARobot" ng-show="failed_recaptcha" ></span>
            <ul ng-show="errors" >
                <li ng-repeat="error in errors" >
                    <span ng-repeat="item_error in error" >
                        {{item_error}}<br>
                    </span>
                </li>
            </ul>
        </div>


        <div class="list-group social-btns" >
            <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid || !pwCheck()" tooltip-class="tooltip-danger zindex9999" >
                <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="register()" ng-disabled="!form.$valid || !pwCheck()" translate="User.SignUp" ></button>
            </span>
        </div>
        <div class="line line-dashed"></div>


        <div class="list-group social-btns" >
            <a href="{!! url('/api/fb_redirect') !!}" class="btn btn-lg btn-primary btn-block btn-facebook" >
                <i class="fa fa-btn fa-facebook"></i>
                <span translate="User.LoginWithFacebook" ></span>
            </a>
            <a href="{!! url('/api/google_redirect') !!}" class="btn btn-lg btn-primary btn-block btn-google-plus" >
                <i class="fa fa-btn fa-google-plus"></i>
                <span translate="User.LoginWithGoogle" ></span>
            </a>
            <a href="{!! url('/api/twitter_redirect') !!}" class="btn btn-lg btn-primary btn-block btn-twitter" >
                <i class="fa fa-btn fa-twitter"></i>
                <span translate="User.LoginWithTwitter" ></span>
            </a>
        </div>
        <div class="line line-dashed"></div>

        
        <p class="text-center"><small translate="User.AlreadyHaveAnAccount" ></small></p>
        <div class="form-group">
            <a href="{!! url('/login') !!}" class="btn btn-lg btn-default btn-block" >
                <i class="fa fa-btn fa-rea_user"></i>
                <span translate="User.Login" ></span>
            </a>
        </div>
    </form>

    <div class="alert alert-success" ng-show="success_registration" >
        <p translate="ReaUser.SuccessRegistrationPleaseCheckMailToActiveYourAdress" ></p>
        <br>
        <div >
            <label class="i-checks">
                <input type="checkbox" ng-model="iwillcheck" id="iwillcheck" required><i></i><label for="iwillcheck" translate="ReaUser.OkIWillCheck" ></label>
            </label>
        </div>
        <br>
        <div class="text-center" >
            <a ng-disabled="!iwillcheck" href="{!! url('/') !!}#/myinfos" class="btn btn-primary" ><span translate="ReaUser.StartSiteSite" ></span></a>
        </div>
    </div>
</div>
@endsection
