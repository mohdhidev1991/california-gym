@extends('layouts.app_no_container_js')

@section('js_footer')
    @parent
    @if ($valide)
        <script src="{!! URL::asset("themes/admin/assets/js/controllers/rea_user/new_password.js") !!}"></script>
    @endif
@endsection


@section('content')
    <div class="wrapper" >
    @if ($invalide)
        <div class="alert alert-danger" translate="User.TokenInvalideOrExipred" ></div>
        <div class="text-center" >
            <a href="{!! url('/') !!}" class="btn btn-primary" ><span translate="Global.BackToHome" ></span></a>
        </div>
    @endif

    @if ($exipred)
        <div class="alert alert-danger" translate="User.TokenInvalideOrExipred" ></div>
        <div class="text-center" >
            <a href="{!! url('/') !!}" class="btn btn-primary" ><span translate="Global.BackToHome" ></span></a>
        </div>
    @endif

    @if ($valide)
        
            
            <div class="m-b-lg" ng-controller="NewPasswordFormController" >
                <form class="form-reset-password form-validation form-login" name="form" role="form" ng-hide="success_change_password" >
                    <div class="alert alert-success" translate="User.TokenValideChangeYourPasswordNow" ></div>        
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">


                    <div class="list-group list-group-sm">
                        <div class="list-group-item" >
                            <input type="password" ng-required="true" placeholder="{{ 'Field.NewPassword '| translate}}" class="form-control no-border" name="pwd" ng-model="rea_user.pwd" >
                        </div>
                    </div>
                    <div class="list-group list-group-sm">
                        <div class="list-group-item" >
                            <input type="password" ng-required="true" placeholder="{{ 'Field.RepeatPassword' | translate}}" class="form-control no-border" name="pwd2" ng-model="rea_user.pwd2" ui-validate=" ' ($value==rea_user.pwd) || (!rea_user.pwd && !rea_user.pwd2)' " >
                        </div>
                    </div>

                    <div class="alert alert-warning" ng-show="rea_user.pwd.length<6" >
                            <span translate="Form.PleaseEnterStrongPasswordMin6AndRepeatTheNewPassword" ></span>
                    </div>

                    <div class="alert alert-danger" ng-show="form.pwd2.$error.validator" >
                        <span translate="Form.PasswordsNotTheSame" ng-show="form.pwd2.$error.validator" ></span>
                    </div>

                    <div class="alert alert-danger" ng-show="failed_change_password" >
                        <ul ng-show="errors" >
                            <li ng-repeat="error in errors" >
                                <span ng-repeat="item_error in error" >
                                    {{item_error}}<br>
                                </span>
                            </li>
                        </ul>
                    </div>

                    
                    
                    <div class="list-group" >
                        <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                            <button ng-disabled="!form.$valid" type="submit" ng-click="change_password()" class="btn btn-lg btn-primary btn-block" >
                                <i class="fa fa-btn fa-unlock-alt" aria-hidden="true"></i>
                                <span translate="User.SaveNewPassword" ></span>
                            </button>
                        </span>
                    </div>
                </form>

                <div class="alert alert-success text-center" ng-show="success_change_password" >
                    <span translate="User.NewPasswordSavedWithSuccess" ></span>
                </div>
                <div class="text-center" >
                    <a href="{!! url('/') !!}" class="btn btn-primary" ><span translate="Global.BackToHome" ></span></a>
                </div>
            </div>
        
        
    @endif
    </div>

@endsection
