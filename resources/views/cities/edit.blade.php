@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($city, ['route' => ['cities.update', $city->id], 'method' => 'patch']) !!}

        @include('cities.fields')

    {!! Form::close() !!}
</div>
@endsection
