<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Country Id</th>
			<th>City Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($cities as $city)
        <tr>
            <td>{!! $city->id !!}</td>
			<td>{!! $city->creation_user_id !!}</td>
			<td>{!! $city->creation_date !!}</td>
			<td>{!! $city->update_user_id !!}</td>
			<td>{!! $city->update_date !!}</td>
			<td>{!! $city->validation_user_id !!}</td>
			<td>{!! $city->validation_date !!}</td>
			<td>{!! $city->active !!}</td>
			<td>{!! $city->version !!}</td>
			<td>{!! $city->update_groups_mfk !!}</td>
			<td>{!! $city->delete_groups_mfk !!}</td>
			<td>{!! $city->display_groups_mfk !!}</td>
			<td>{!! $city->sci_id !!}</td>
			<td>{!! $city->country_id !!}</td>
			<td>{!! $city->city_name !!}</td>
            <td>
                <a href="{!! route('cities.edit', [$city->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('cities.delete', [$city->id]) !!}" onclick="return confirm('Are you sure wants to delete this City?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
