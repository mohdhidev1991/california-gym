@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'classCourseExams.store']) !!}

        @include('classCourseExams.fields')

    {!! Form::close() !!}
</div>
@endsection
