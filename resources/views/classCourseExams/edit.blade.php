@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($classCourseExam, ['route' => ['classCourseExams.update', $classCourseExam->id], 'method' => 'patch']) !!}

        @include('classCourseExams.fields')

    {!! Form::close() !!}
</div>
@endsection
