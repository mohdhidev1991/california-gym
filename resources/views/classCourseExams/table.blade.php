<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Class Course Exam Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($classCourseExams as $classCourseExam)
        <tr>
            <td>{!! $classCourseExam->id !!}</td>
			<td>{!! $classCourseExam->creation_user_id !!}</td>
			<td>{!! $classCourseExam->creation_date !!}</td>
			<td>{!! $classCourseExam->update_user_id !!}</td>
			<td>{!! $classCourseExam->update_date !!}</td>
			<td>{!! $classCourseExam->validation_user_id !!}</td>
			<td>{!! $classCourseExam->validation_date !!}</td>
			<td>{!! $classCourseExam->active !!}</td>
			<td>{!! $classCourseExam->version !!}</td>
			<td>{!! $classCourseExam->update_groups_mfk !!}</td>
			<td>{!! $classCourseExam->delete_groups_mfk !!}</td>
			<td>{!! $classCourseExam->display_groups_mfk !!}</td>
			<td>{!! $classCourseExam->sci_id !!}</td>
			<td>{!! $classCourseExam->class_course_exam_name !!}</td>
            <td>
                <a href="{!! route('classCourseExams.edit', [$classCourseExam->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('classCourseExams.delete', [$classCourseExam->id]) !!}" onclick="return confirm('Are you sure wants to delete this ClassCourseExam?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
