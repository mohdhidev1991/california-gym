@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($classCourse, ['route' => ['classCourses.update', $classCourse->id], 'method' => 'patch']) !!}

        @include('classCourses.fields')

    {!! Form::close() !!}
</div>
@endsection
