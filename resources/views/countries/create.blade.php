@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'countries.store']) !!}

        @include('countries.fields')

    {!! Form::close() !!}
</div>
@endsection
