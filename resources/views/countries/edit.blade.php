@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($country, ['route' => ['countries.update', $country->id], 'method' => 'patch']) !!}

        @include('countries.fields')

    {!! Form::close() !!}
</div>
@endsection
