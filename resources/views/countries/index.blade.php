@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Countries</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('countries.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($countries->isEmpty())
                <div class="well text-center">No Countries found.</div>
            @else
                @include('countries.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $countries])


    </div>
@endsection
