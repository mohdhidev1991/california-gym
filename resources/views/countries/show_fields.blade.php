<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $country->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $country->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $country->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $country->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $country->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $country->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $country->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $country->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $country->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $country->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $country->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $country->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $country->sci_id !!}</p>
</div>

<!-- Time Offset Field -->
<div class="form-group">
    {!! Form::label('time_offset', 'Time Offset:') !!}
    <p>{!! $country->time_offset !!}</p>
</div>

<!-- Maintenance End Time Field -->
<div class="form-group">
    {!! Form::label('maintenance_end_time', 'Maintenance End Time:') !!}
    <p>{!! $country->maintenance_end_time !!}</p>
</div>

<!-- Maintenance Start Time Field -->
<div class="form-group">
    {!! Form::label('maintenance_start_time', 'Maintenance Start Time:') !!}
    <p>{!! $country->maintenance_start_time !!}</p>
</div>

<!-- Date System Id Field -->
<div class="form-group">
    {!! Form::label('date_system_id', 'Date System Id:') !!}
    <p>{!! $country->date_system_id !!}</p>
</div>

<!-- Abrev Field -->
<div class="form-group">
    {!! Form::label('abrev', 'Abrev:') !!}
    <p>{!! $country->abrev !!}</p>
</div>

<!-- Country Name Field -->
<div class="form-group">
    {!! Form::label('country_name', 'Country Name:') !!}
    <p>{!! $country->country_name !!}</p>
</div>

