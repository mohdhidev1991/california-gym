@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($courseSchedItem, ['route' => ['courseSchedItems.update', $courseSchedItem->id], 'method' => 'patch']) !!}

        @include('courseSchedItems.fields')

    {!! Form::close() !!}
</div>
@endsection
