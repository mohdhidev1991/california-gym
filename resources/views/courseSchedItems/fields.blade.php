<!-- Session End Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_end_time', 'Session End Time:') !!}
	{!! Form::text('session_end_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Session Start Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_start_time', 'Session Start Time:') !!}
	{!! Form::text('session_start_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('course_id', 'Course Id:') !!}
	{!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Level Class Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('level_class_id', 'Level Class Id:') !!}
	{!! Form::number('level_class_id', null, ['class' => 'form-control']) !!}
</div>

<!-- School Year Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_year_id', 'School Year Id:') !!}
	{!! Form::number('school_year_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Symbol Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('symbol', 'Symbol:') !!}
	{!! Form::text('symbol', null, ['class' => 'form-control']) !!}
</div>

<!-- Session Order Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_order', 'Session Order:') !!}
	{!! Form::number('session_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Wday Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('wday_id', 'Wday Id:') !!}
	{!! Form::number('wday_id', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
