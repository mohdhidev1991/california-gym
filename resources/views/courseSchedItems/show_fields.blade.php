<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $courseSchedItem->id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $courseSchedItem->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $courseSchedItem->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $courseSchedItem->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $courseSchedItem->updated_at !!}</p>
</div>

<!-- Validated By Field -->
<div class="form-group">
    {!! Form::label('validated_by', 'Validated By:') !!}
    <p>{!! $courseSchedItem->validated_by !!}</p>
</div>

<!-- Validated At Field -->
<div class="form-group">
    {!! Form::label('validated_at', 'Validated At:') !!}
    <p>{!! $courseSchedItem->validated_at !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $courseSchedItem->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $courseSchedItem->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $courseSchedItem->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $courseSchedItem->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $courseSchedItem->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $courseSchedItem->sci_id !!}</p>
</div>

<!-- Session End Time Field -->
<div class="form-group">
    {!! Form::label('session_end_time', 'Session End Time:') !!}
    <p>{!! $courseSchedItem->session_end_time !!}</p>
</div>

<!-- Session Start Time Field -->
<div class="form-group">
    {!! Form::label('session_start_time', 'Session Start Time:') !!}
    <p>{!! $courseSchedItem->session_start_time !!}</p>
</div>

<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{!! $courseSchedItem->course_id !!}</p>
</div>

<!-- Level Class Id Field -->
<div class="form-group">
    {!! Form::label('level_class_id', 'Level Class Id:') !!}
    <p>{!! $courseSchedItem->level_class_id !!}</p>
</div>

<!-- School Year Id Field -->
<div class="form-group">
    {!! Form::label('school_year_id', 'School Year Id:') !!}
    <p>{!! $courseSchedItem->school_year_id !!}</p>
</div>

<!-- Symbol Field -->
<div class="form-group">
    {!! Form::label('symbol', 'Symbol:') !!}
    <p>{!! $courseSchedItem->symbol !!}</p>
</div>

<!-- Session Order Field -->
<div class="form-group">
    {!! Form::label('session_order', 'Session Order:') !!}
    <p>{!! $courseSchedItem->session_order !!}</p>
</div>

<!-- Wday Id Field -->
<div class="form-group">
    {!! Form::label('wday_id', 'Wday Id:') !!}
    <p>{!! $courseSchedItem->wday_id !!}</p>
</div>

