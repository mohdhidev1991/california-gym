@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'courseSessions.store']) !!}

        @include('courseSessions.fields')

    {!! Form::close() !!}
</div>
@endsection
