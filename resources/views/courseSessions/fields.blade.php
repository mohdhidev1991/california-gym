<!-- Session Hdate Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_hdate', 'Session Hdate:') !!}
	{!! Form::text('session_hdate', null, ['class' => 'form-control']) !!}
</div>

<!-- Session End Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_end_time', 'Session End Time:') !!}
	{!! Form::text('session_end_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Session Start Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_start_time', 'Session Start Time:') !!}
	{!! Form::text('session_start_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('course_id', 'Course Id:') !!}
	{!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Level Class Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('level_class_id', 'Level Class Id:') !!}
	{!! Form::number('level_class_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Prof Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('prof_id', 'Prof Id:') !!}
	{!! Form::number('prof_id', null, ['class' => 'form-control']) !!}
</div>

<!-- School Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_id', 'School Id:') !!}
	{!! Form::number('school_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Session Status Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_status_id', 'Session Status Id:') !!}
	{!! Form::number('session_status_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Session Status Comment Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_status_comment', 'Session Status Comment:') !!}
	{!! Form::text('session_status_comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Symbol Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('symbol', 'Symbol:') !!}
	{!! Form::text('symbol', null, ['class' => 'form-control']) !!}
</div>

<!-- Year Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('year', 'Year:') !!}
	{!! Form::text('year', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
