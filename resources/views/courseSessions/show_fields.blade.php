<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $courseSession->id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $courseSession->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $courseSession->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $courseSession->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $courseSession->updated_at !!}</p>
</div>

<!-- Validated By Field -->
<div class="form-group">
    {!! Form::label('validated_by', 'Validated By:') !!}
    <p>{!! $courseSession->validated_by !!}</p>
</div>

<!-- Validated At Field -->
<div class="form-group">
    {!! Form::label('validated_at', 'Validated At:') !!}
    <p>{!! $courseSession->validated_at !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $courseSession->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $courseSession->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $courseSession->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $courseSession->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $courseSession->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $courseSession->sci_id !!}</p>
</div>

<!-- Session Hdate Field -->
<div class="form-group">
    {!! Form::label('session_hdate', 'Session Hdate:') !!}
    <p>{!! $courseSession->session_hdate !!}</p>
</div>

<!-- Session End Time Field -->
<div class="form-group">
    {!! Form::label('session_end_time', 'Session End Time:') !!}
    <p>{!! $courseSession->session_end_time !!}</p>
</div>

<!-- Session Start Time Field -->
<div class="form-group">
    {!! Form::label('session_start_time', 'Session Start Time:') !!}
    <p>{!! $courseSession->session_start_time !!}</p>
</div>

<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{!! $courseSession->course_id !!}</p>
</div>

<!-- Level Class Id Field -->
<div class="form-group">
    {!! Form::label('level_class_id', 'Level Class Id:') !!}
    <p>{!! $courseSession->level_class_id !!}</p>
</div>

<!-- Prof Id Field -->
<div class="form-group">
    {!! Form::label('prof_id', 'Prof Id:') !!}
    <p>{!! $courseSession->prof_id !!}</p>
</div>

<!-- School Id Field -->
<div class="form-group">
    {!! Form::label('school_id', 'School Id:') !!}
    <p>{!! $courseSession->school_id !!}</p>
</div>

<!-- Session Status Id Field -->
<div class="form-group">
    {!! Form::label('session_status_id', 'Session Status Id:') !!}
    <p>{!! $courseSession->session_status_id !!}</p>
</div>

<!-- Session Status Comment Field -->
<div class="form-group">
    {!! Form::label('session_status_comment', 'Session Status Comment:') !!}
    <p>{!! $courseSession->session_status_comment !!}</p>
</div>

<!-- Symbol Field -->
<div class="form-group">
    {!! Form::label('symbol', 'Symbol:') !!}
    <p>{!! $courseSession->symbol !!}</p>
</div>

<!-- Year Field -->
<div class="form-group">
    {!! Form::label('year', 'Year:') !!}
    <p>{!! $courseSession->year !!}</p>
</div>

