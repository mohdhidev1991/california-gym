<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Session Hdate</th>
			<th>Session End Time</th>
			<th>Session Start Time</th>
			<th>Course Id</th>
			<th>Level Class Id</th>
			<th>Prof Id</th>
			<th>School Id</th>
			<th>Session Status Id</th>
			<th>Session Status Comment</th>
			<th>Symbol</th>
			<th>Year</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($courseSessions as $courseSession)
        <tr>
            <td>{!! $courseSession->id !!}</td>
			<td>{!! $courseSession->created_by !!}</td>
			<td>{!! $courseSession->created_at !!}</td>
			<td>{!! $courseSession->updated_by !!}</td>
			<td>{!! $courseSession->updated_at !!}</td>
			<td>{!! $courseSession->validated_by !!}</td>
			<td>{!! $courseSession->validated_at !!}</td>
			<td>{!! $courseSession->active !!}</td>
			<td>{!! $courseSession->version !!}</td>
			<td>{!! $courseSession->update_groups_mfk !!}</td>
			<td>{!! $courseSession->delete_groups_mfk !!}</td>
			<td>{!! $courseSession->display_groups_mfk !!}</td>
			<td>{!! $courseSession->sci_id !!}</td>
			<td>{!! $courseSession->session_hdate !!}</td>
			<td>{!! $courseSession->session_end_time !!}</td>
			<td>{!! $courseSession->session_start_time !!}</td>
			<td>{!! $courseSession->course_id !!}</td>
			<td>{!! $courseSession->level_class_id !!}</td>
			<td>{!! $courseSession->prof_id !!}</td>
			<td>{!! $courseSession->school_id !!}</td>
			<td>{!! $courseSession->session_status_id !!}</td>
			<td>{!! $courseSession->session_status_comment !!}</td>
			<td>{!! $courseSession->symbol !!}</td>
			<td>{!! $courseSession->year !!}</td>
            <td>
                <a href="{!! route('courseSessions.edit', [$courseSession->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('courseSessions.delete', [$courseSession->id]) !!}" onclick="return confirm('Are you sure wants to delete this CourseSession?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
