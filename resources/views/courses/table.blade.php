<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Course Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($courses as $course)
        <tr>
            <td>{!! $course->id !!}</td>
			<td>{!! $course->creation_user_id !!}</td>
			<td>{!! $course->creation_date !!}</td>
			<td>{!! $course->update_user_id !!}</td>
			<td>{!! $course->update_date !!}</td>
			<td>{!! $course->validation_user_id !!}</td>
			<td>{!! $course->validation_date !!}</td>
			<td>{!! $course->active !!}</td>
			<td>{!! $course->version !!}</td>
			<td>{!! $course->update_groups_mfk !!}</td>
			<td>{!! $course->delete_groups_mfk !!}</td>
			<td>{!! $course->display_groups_mfk !!}</td>
			<td>{!! $course->sci_id !!}</td>
			<td>{!! $course->course_name !!}</td>
            <td>
                <a href="{!! route('courses.edit', [$course->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('courses.delete', [$course->id]) !!}" onclick="return confirm('Are you sure wants to delete this Course?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
