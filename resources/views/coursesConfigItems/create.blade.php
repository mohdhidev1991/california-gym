@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'coursesConfigItems.store']) !!}

        @include('coursesConfigItems.fields')

    {!! Form::close() !!}
</div>
@endsection
