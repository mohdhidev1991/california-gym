@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($coursesConfigItem, ['route' => ['coursesConfigItems.update', $coursesConfigItem->id], 'method' => 'patch']) !!}

        @include('coursesConfigItems.fields')

    {!! Form::close() !!}
</div>
@endsection
