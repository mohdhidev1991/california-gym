<!-- Courses Config Template Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('courses_config_template_id', 'Courses Config Template Id:') !!}
	{!! Form::number('courses_config_template_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('course_id', 'Course Id:') !!}
	{!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Level Class Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('level_class_id', 'Level Class Id:') !!}
	{!! Form::number('level_class_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Session Nb Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_nb', 'Session Nb:') !!}
	{!! Form::number('session_nb', null, ['class' => 'form-control']) !!}
</div>

<!-- Coef Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('coef', 'Coef:') !!}
	{!! Form::number('coef', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
