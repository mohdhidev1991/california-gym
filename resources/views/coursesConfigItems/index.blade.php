@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">CoursesConfigItems</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('coursesConfigItems.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($coursesConfigItems->isEmpty())
                <div class="well text-center">No CoursesConfigItems found.</div>
            @else
                @include('coursesConfigItems.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $coursesConfigItems])


    </div>
@endsection
