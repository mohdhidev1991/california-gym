<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $coursesConfigItem->id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $coursesConfigItem->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $coursesConfigItem->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $coursesConfigItem->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $coursesConfigItem->updated_at !!}</p>
</div>

<!-- Validated By Field -->
<div class="form-group">
    {!! Form::label('validated_by', 'Validated By:') !!}
    <p>{!! $coursesConfigItem->validated_by !!}</p>
</div>

<!-- Validated At Field -->
<div class="form-group">
    {!! Form::label('validated_at', 'Validated At:') !!}
    <p>{!! $coursesConfigItem->validated_at !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $coursesConfigItem->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $coursesConfigItem->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $coursesConfigItem->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $coursesConfigItem->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $coursesConfigItem->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $coursesConfigItem->sci_id !!}</p>
</div>

<!-- Courses Config Template Id Field -->
<div class="form-group">
    {!! Form::label('courses_config_template_id', 'Courses Config Template Id:') !!}
    <p>{!! $coursesConfigItem->courses_config_template_id !!}</p>
</div>

<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{!! $coursesConfigItem->course_id !!}</p>
</div>

<!-- Level Class Id Field -->
<div class="form-group">
    {!! Form::label('level_class_id', 'Level Class Id:') !!}
    <p>{!! $coursesConfigItem->level_class_id !!}</p>
</div>

<!-- Session Nb Field -->
<div class="form-group">
    {!! Form::label('session_nb', 'Session Nb:') !!}
    <p>{!! $coursesConfigItem->session_nb !!}</p>
</div>

<!-- Coef Field -->
<div class="form-group">
    {!! Form::label('coef', 'Coef:') !!}
    <p>{!! $coursesConfigItem->coef !!}</p>
</div>

