<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Courses Config Template Id</th>
			<th>Course Id</th>
			<th>Level Class Id</th>
			<th>Session Nb</th>
			<th>Coef</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($coursesConfigItems as $coursesConfigItem)
        <tr>
            <td>{!! $coursesConfigItem->id !!}</td>
			<td>{!! $coursesConfigItem->created_by !!}</td>
			<td>{!! $coursesConfigItem->created_at !!}</td>
			<td>{!! $coursesConfigItem->updated_by !!}</td>
			<td>{!! $coursesConfigItem->updated_at !!}</td>
			<td>{!! $coursesConfigItem->validated_by !!}</td>
			<td>{!! $coursesConfigItem->validated_at !!}</td>
			<td>{!! $coursesConfigItem->active !!}</td>
			<td>{!! $coursesConfigItem->version !!}</td>
			<td>{!! $coursesConfigItem->update_groups_mfk !!}</td>
			<td>{!! $coursesConfigItem->delete_groups_mfk !!}</td>
			<td>{!! $coursesConfigItem->display_groups_mfk !!}</td>
			<td>{!! $coursesConfigItem->sci_id !!}</td>
			<td>{!! $coursesConfigItem->courses_config_template_id !!}</td>
			<td>{!! $coursesConfigItem->course_id !!}</td>
			<td>{!! $coursesConfigItem->level_class_id !!}</td>
			<td>{!! $coursesConfigItem->session_nb !!}</td>
			<td>{!! $coursesConfigItem->coef !!}</td>
            <td>
                <a href="{!! route('coursesConfigItems.edit', [$coursesConfigItem->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('coursesConfigItems.delete', [$coursesConfigItem->id]) !!}" onclick="return confirm('Are you sure wants to delete this CoursesConfigItem?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
