@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'coursesConfigTemplates.store']) !!}

        @include('coursesConfigTemplates.fields')

    {!! Form::close() !!}
</div>
@endsection
