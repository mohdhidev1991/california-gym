@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($coursesConfigTemplate, ['route' => ['coursesConfigTemplates.update', $coursesConfigTemplate->id], 'method' => 'patch']) !!}

        @include('coursesConfigTemplates.fields')

    {!! Form::close() !!}
</div>
@endsection
