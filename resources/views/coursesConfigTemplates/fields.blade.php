<!-- Courses Config Template Name Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('courses_config_template_name', 'Courses Config Template Name:') !!}
	{!! Form::text('courses_config_template_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Levels Template Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('levels_template_id', 'Levels Template Id:') !!}
	{!! Form::number('levels_template_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Courses Template Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('courses_template_id', 'Courses Template Id:') !!}
	{!! Form::number('courses_template_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Session Duration Min Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('session_duration_min', 'Session Duration Min:') !!}
	{!! Form::number('session_duration_min', null, ['class' => 'form-control']) !!}
</div>

<!-- School Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_id', 'School Id:') !!}
	{!! Form::number('school_id', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
