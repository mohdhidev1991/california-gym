@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">CoursesConfigTemplates</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('coursesConfigTemplates.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($coursesConfigTemplates->isEmpty())
                <div class="well text-center">No CoursesConfigTemplates found.</div>
            @else
                @include('coursesConfigTemplates.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $coursesConfigTemplates])


    </div>
@endsection
