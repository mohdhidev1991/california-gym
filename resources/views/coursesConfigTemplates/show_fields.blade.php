<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $coursesConfigTemplate->id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $coursesConfigTemplate->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $coursesConfigTemplate->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $coursesConfigTemplate->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $coursesConfigTemplate->updated_at !!}</p>
</div>

<!-- Validated By Field -->
<div class="form-group">
    {!! Form::label('validated_by', 'Validated By:') !!}
    <p>{!! $coursesConfigTemplate->validated_by !!}</p>
</div>

<!-- Validated At Field -->
<div class="form-group">
    {!! Form::label('validated_at', 'Validated At:') !!}
    <p>{!! $coursesConfigTemplate->validated_at !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $coursesConfigTemplate->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $coursesConfigTemplate->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $coursesConfigTemplate->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $coursesConfigTemplate->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $coursesConfigTemplate->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $coursesConfigTemplate->sci_id !!}</p>
</div>

<!-- Courses Config Template Name Field -->
<div class="form-group">
    {!! Form::label('courses_config_template_name', 'Courses Config Template Name:') !!}
    <p>{!! $coursesConfigTemplate->courses_config_template_name !!}</p>
</div>

<!-- Levels Template Id Field -->
<div class="form-group">
    {!! Form::label('levels_template_id', 'Levels Template Id:') !!}
    <p>{!! $coursesConfigTemplate->levels_template_id !!}</p>
</div>

<!-- Courses Template Id Field -->
<div class="form-group">
    {!! Form::label('courses_template_id', 'Courses Template Id:') !!}
    <p>{!! $coursesConfigTemplate->courses_template_id !!}</p>
</div>

<!-- Session Duration Min Field -->
<div class="form-group">
    {!! Form::label('session_duration_min', 'Session Duration Min:') !!}
    <p>{!! $coursesConfigTemplate->session_duration_min !!}</p>
</div>

<!-- School Id Field -->
<div class="form-group">
    {!! Form::label('school_id', 'School Id:') !!}
    <p>{!! $coursesConfigTemplate->school_id !!}</p>
</div>

