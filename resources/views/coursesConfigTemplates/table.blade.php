<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Courses Config Template Name</th>
			<th>Levels Template Id</th>
			<th>Courses Template Id</th>
			<th>Session Duration Min</th>
			<th>School Id</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($coursesConfigTemplates as $coursesConfigTemplate)
        <tr>
            <td>{!! $coursesConfigTemplate->id !!}</td>
			<td>{!! $coursesConfigTemplate->created_by !!}</td>
			<td>{!! $coursesConfigTemplate->created_at !!}</td>
			<td>{!! $coursesConfigTemplate->updated_by !!}</td>
			<td>{!! $coursesConfigTemplate->updated_at !!}</td>
			<td>{!! $coursesConfigTemplate->validated_by !!}</td>
			<td>{!! $coursesConfigTemplate->validated_at !!}</td>
			<td>{!! $coursesConfigTemplate->active !!}</td>
			<td>{!! $coursesConfigTemplate->version !!}</td>
			<td>{!! $coursesConfigTemplate->update_groups_mfk !!}</td>
			<td>{!! $coursesConfigTemplate->delete_groups_mfk !!}</td>
			<td>{!! $coursesConfigTemplate->display_groups_mfk !!}</td>
			<td>{!! $coursesConfigTemplate->sci_id !!}</td>
			<td>{!! $coursesConfigTemplate->courses_config_template_name !!}</td>
			<td>{!! $coursesConfigTemplate->levels_template_id !!}</td>
			<td>{!! $coursesConfigTemplate->courses_template_id !!}</td>
			<td>{!! $coursesConfigTemplate->session_duration_min !!}</td>
			<td>{!! $coursesConfigTemplate->school_id !!}</td>
            <td>
                <a href="{!! route('coursesConfigTemplates.edit', [$coursesConfigTemplate->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('coursesConfigTemplates.delete', [$coursesConfigTemplate->id]) !!}" onclick="return confirm('Are you sure wants to delete this CoursesConfigTemplate?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
