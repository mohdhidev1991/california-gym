@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($dateSystem, ['route' => ['dateSystems.update', $dateSystem->id], 'method' => 'patch']) !!}

        @include('dateSystems.fields')

    {!! Form::close() !!}
</div>
@endsection
