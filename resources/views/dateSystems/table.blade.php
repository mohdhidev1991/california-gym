<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Date System Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($dateSystems as $dateSystem)
        <tr>
            <td>{!! $dateSystem->id !!}</td>
			<td>{!! $dateSystem->creation_user_id !!}</td>
			<td>{!! $dateSystem->creation_date !!}</td>
			<td>{!! $dateSystem->update_user_id !!}</td>
			<td>{!! $dateSystem->update_date !!}</td>
			<td>{!! $dateSystem->validation_user_id !!}</td>
			<td>{!! $dateSystem->validation_date !!}</td>
			<td>{!! $dateSystem->active !!}</td>
			<td>{!! $dateSystem->version !!}</td>
			<td>{!! $dateSystem->update_groups_mfk !!}</td>
			<td>{!! $dateSystem->delete_groups_mfk !!}</td>
			<td>{!! $dateSystem->display_groups_mfk !!}</td>
			<td>{!! $dateSystem->sci_id !!}</td>
			<td>{!! $dateSystem->date_system_name !!}</td>
            <td>
                <a href="{!! route('dateSystems.edit', [$dateSystem->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('dateSystems.delete', [$dateSystem->id]) !!}" onclick="return confirm('Are you sure wants to delete this DateSystem?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
