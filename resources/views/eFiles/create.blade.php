@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'eFiles.store']) !!}

        @include('eFiles.fields')

    {!! Form::close() !!}
</div>
@endsection
