@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($eFile, ['route' => ['eFiles.update', $eFile->id], 'method' => 'patch']) !!}

        @include('eFiles.fields')

    {!! Form::close() !!}
</div>
@endsection
