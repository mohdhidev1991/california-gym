<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Efile Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($eFiles as $eFile)
        <tr>
            <td>{!! $eFile->id !!}</td>
			<td>{!! $eFile->creation_user_id !!}</td>
			<td>{!! $eFile->creation_date !!}</td>
			<td>{!! $eFile->update_user_id !!}</td>
			<td>{!! $eFile->update_date !!}</td>
			<td>{!! $eFile->validation_user_id !!}</td>
			<td>{!! $eFile->validation_date !!}</td>
			<td>{!! $eFile->active !!}</td>
			<td>{!! $eFile->version !!}</td>
			<td>{!! $eFile->update_groups_mfk !!}</td>
			<td>{!! $eFile->delete_groups_mfk !!}</td>
			<td>{!! $eFile->display_groups_mfk !!}</td>
			<td>{!! $eFile->sci_id !!}</td>
			<td>{!! $eFile->efile_name !!}</td>
            <td>
                <a href="{!! route('eFiles.edit', [$eFile->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('eFiles.delete', [$eFile->id]) !!}" onclick="return confirm('Are you sure wants to delete this EFile?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
