<html>

<head></head>

<body style="background: #F6F8F7; color: #333; padding: 15px;">
    <h1 style="color:red">Données de test</h1>
	<h1>Cours annulé aprés la validation du controleur</h1>
    <p>La session du cours N° ={!! $courseSession->id !!} a été annulé par le controleur </p>
    <div style="background: #ddd;padding:15px;color:#333;">
       <h3>Détails</h3>
       <span>Date :  {!! $courseSession->course_session_date !!}</span><br>
       <span>Heure Début :  {!! $courseSession->course_start_time !!}</span><br>
       <span>Heure Fin :  {!! $courseSession->course_end_time !!}</span><br>
    </div>
</body>

</html>