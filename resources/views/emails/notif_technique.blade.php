<html>
<head></head>
<body style="background: #F6F8F7; color: #333; padding: 15px;">
    <h1 style="color:red">Données de test</h1>
    <h1>Notification de remplacement du coach</h1>
    <p>La session du cours N°{!! $course_session->id !!} a été affectée au coach ({!! $nameCoach !!}) N° #{!! $numCoach !!} </p>
    <div style="background: #ddd;padding:15px;color:#333;">
       <h3>Détails</h3>
       <span>Nom cours :  {!! $nameCourse !!}</span><br>
       <span>Club / Salle :  {!! $nameClub !!} / {!! $nameRoom !!}</span><br>
       <span>Date :  {!! $course_session->course_session_date !!}</span><br>
       <span>Heure Début :  {!! $course_session->course_start_time !!}</span><br>
       <span>Heure Fin :  {!! $course_session->course_end_time !!}</span><br>
    </div>
</body>
</html>