@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'examSessions.store']) !!}

        @include('examSessions.fields')

    {!! Form::close() !!}
</div>
@endsection
