@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">ExamSessions</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('examSessions.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($examSessions->isEmpty())
                <div class="well text-center">No ExamSessions found.</div>
            @else
                @include('examSessions.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $examSessions])


    </div>
@endsection
