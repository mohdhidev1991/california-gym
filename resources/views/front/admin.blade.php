  <!-- navbar -->
  <div data-ng-include=" 'front/tpl/admin/blocks/header' " class="app-header navbar">
  </div>
  <!-- / navbar -->

  <!-- menu -->
  <div data-ng-include=" 'front/tpl/admin/blocks/aside' " class="app-aside hidden-xs {{app.settings.asideColor}}">
  </div>
  <!-- / menu -->

  <!-- content -->
  <div class="app-content">
    <div ui-butterbar></div>
    <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
    <div class="app-content-body fade-in-up_" ui-view></div>
  </div>
  <!-- /content -->

  <!-- footer -->
  <div class="app-footer wrapper b-t bg-light">
    <span class="pull-right">{{app.version}} <a href ui-scroll-to="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
    <span>&copy; 2019</span> - <span>Powered by</span> <a href="http://www.digit-u.com/" target="_blank" >Digi-u</a>.
  </div>
  <!-- / footer -->