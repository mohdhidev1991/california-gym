<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Alert.SendAlert" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default" >
            <div class="panel-heading font-bold" translate="Alert.SendAlert" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate >


                <div class="form-group" >
                    <label for="alert_type" ><span translate="Field.AlertReceiver" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-change="changed_alert_receiver()" ng-model="alert.alert_receiver" ng-options="obj.lookup_code as obj.receiver_name | translate for obj in alert_receivers" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div ng-show="alert.alert_receiver" >

                    <div ng-show="alert.alert_receiver==='student'" >
                        <div class="form-group"  >
                            <label for="student_idn" ><span translate="Student.SelectStudent" ></span><sup class="required" >*</sup></label>                        
                            
                            <input type="text" ng-model="alert.student" uib-typeahead="student as student.fullname for student in getStudents($viewValue)" typeahead-loading="loadingLocations" typeahead-no-results="noResults" class="form-control" placeholder="{{ 'Alert.EnterStudentIdnOrNameOrMobile' | translate }}" >
                            <i ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></i>
                            <div ng-show="noResults">
                              <i class="glyphicon glyphicon-remove"></i> <span translate="Global.NoResultsFound" ></span>
                            </div>
                        </div>
                        <div class="form-group" ng-show="alert.student" >
                            <pre>{{alert.student.fullname}} [IDN: {{alert.student.idn}}]</pre>
                        </div>
                        <input type="hidden" ng-model="alert.student.id" ng-required="alert.alert_receiver==='student'" />
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>



                    <div ng-show="alert.alert_receiver==='teacher'" >
                        <div class="form-group"  >
                            <label for="student_idn" ><span translate="Alert.SelectTeacher" ></span><sup class="required" >*</sup></label>                        
                            
                            <input type="text" ng-model="alert.teacher" uib-typeahead="teacher as teacher.fullname for teacher in getTeachers($viewValue)" typeahead-loading="loadingTeachers" typeahead-no-results="noTeachers" class="form-control" placeholder="{{ 'Alert.EnterTeacherIdnOrNameOrMobile' | translate }}" >
                            <i ng-show="loadingTeachers" class="glyphicon glyphicon-refresh"></i>
                            <div ng-show="noTeachers">
                              <i class="glyphicon glyphicon-remove"></i> <span translate="Global.NoResultsFound" ></span>
                            </div>
                        </div>
                        <div class="form-group" ng-show="alert.teacher" >
                            <pre>{{alert.teacher.fullname}} [IDN: {{alert.teacher.idn}}]</pre>
                        </div>
                        <input type="hidden" ng-model="alert.teacher.id" ng-required="alert.alert_receiver==='teacher'" />
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>



                    <div ng-show="alert.alert_receiver==='school' || alert.alert_receiver==='levelClass' || alert.alert_receiver==='levelClassAndSymbol'" >
                        <div class="form-group"  >
                            <label for="school" >
                                <span translate="Field.School" ></span><sup class="required" >*</sup>
                            </label>
                            <select class="select-form-control" ng-change="GetSchoolYears()" ng-model="alert.school_id" ng-options="obj.id as obj.school_name_ar | translate for obj in schools" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                    </div>

                    <div ng-show="alert.alert_receiver==='levelClass' || alert.alert_receiver==='levelClassAndSymbol'" >
                        

                        <div class="form-group"  >
                            <label for="school" >
                                <span translate="Field.SchoolYear" ></span><sup class="required" >*</sup>
                            </label>
                            <select class="select-form-control" ng-disabled="!school_years" ng-change="GetLevelClasses()" ng-model="alert.school_year_id" ng-options="obj.id as obj.school_year_name_ar | translate for obj in school_years" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>


                        <div class="form-group"  >
                            <label for="school" >
                                <span translate="Field.LevelClass" ></span><sup class="required" >*</sup>
                            </label>
                            <select class="select-form-control" ng-disabled="!level_classes" ng-change="GetSchoolClasses()" ng-model="alert.level_class_id" ng-options="obj.id as obj.level_class_name_ar | translate for obj in level_classes" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>



                        <div class="form-group" ng-show="alert.alert_receiver==='levelClassAndSymbol'" >
                            <label for="school" >
                                <span translate="Field.SchoolClass" ></span><sup class="required" >*</sup>
                            </label>
                            <select class="select-form-control" ng-disabled="!school_classes" ng-model="alert.school_class_id" ng-options="obj.id as obj.symbol | translate for obj in school_classes" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>


                        <input type="hidden" ng-model="alert.school_id" ng-required="alert.alert_receiver==='levelClass'" />
                        <input type="hidden" ng-model="alert.school_year_id" ng-required="alert.alert_receiver==='levelClass'" />
                        <input type="hidden" ng-model="alert.level_class_id" ng-required="alert.alert_receiver==='levelClass'" />
                        <input type="hidden" ng-model="alert.school_class_id" ng-required="alert.alert_receiver==='levelClassAndSymbol'" />
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>




                    <div class="form-group" >
                        <label for="alert_type" ><span translate="Field.AlertType" ></span><sup class="required" >*</sup></label>
                        <select ng-change="changed_alert_type()" class="select-form-control" ng-model="alert.alert_type" ng-options="obj.id as obj.alert_type_name_ar for obj in alert_types" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                      <label for="alert_title_ar" ><span translate="Alert.ArabicTitle" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-required="true" required="" id="alert_title_ar" ng-model="alert.title_ar" class="form-control" placeholder="*" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                      <label for="alert_title_en" ><span translate="Alert.EnglishTitle" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-required="true" required="" id="alert_title_en" ng-model="alert.title_en" class="form-control" placeholder="*" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                      <label for="alert_message_ar" ><span translate="Alert.ArabicMessage" ></span><sup class="required" >*</sup></label>
                      <textarea ng-required="true" required="" id="alert_message_ar" ng-model="alert.message_ar" class="form-control" placeholder="*" ></textarea>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                      <label for="alert_message_en" ><span translate="Alert.EnglishMessage" ></span><sup class="required" >*</sup></label>
                      <textarea ng-required="true" required="" id="alert_message_en" ng-model="alert.message_en" class="form-control" placeholder="*" ></textarea>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
                
              </form>
            </div>


            <div class="panel-footer" >

                <div class="alert alert-danger" ng-show="error_send_alert" >
                    <span translate="{{error_send_alert}}" ></span>
                </div>
                <div class="alert alert-success" ng-show="success_send_alert" >
                    <span translate="{{success_send_alert}}" ></span>
                </div>
                
                <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="send()"
                        >
                        <i class="fa fa-send" ></i><span translate="Form.Send" ></span>
                    </button>
                </span>
            </div>


      </div>
    </div>
</div>