<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="AlertStatus.EditAlertStatus" translate-values="{alert_status_id: alert_status.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="AlertStatus.AddNewAlertStatus" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="AlertStatus.CantGetAlertStatus" ng-show="!alert_status.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="alert_status.id || action==='add'" >
            <div class="panel-heading font-bold" translate="AlertStatus.AlertStatusInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !alert_status.id}" >
                      <label for="alert_status_id" ><span translate="AlertStatus.AlertStatusID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="alert_status_id" ng-model="alert_status.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !alert_status.lookup_code}" >
                  <label for="lookup_code" ><span translate="AlertStatus.AlertStatusLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="alert_status.lookup_code" class="form-control" placeholder="{{ 'AlertStatus.PleaseEnterTheAlertStatusLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !alert_status.alert_status_name_ar}" >
                  <label for="alert_status_name_ar" ><span translate="AlertStatus.AlertStatusNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="alert_status_name_ar" ng-model="alert_status.alert_status_name_ar" class="form-control" placeholder="{{ 'AlertStatus.PleaseEnterTheAlertStatusNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !alert_status.alert_status_name_en}" >
                  <label for="alert_status_name_en" ><span translate="AlertStatus.AlertStatusNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="alert_status_name_en" ng-model="alert_status.alert_status_name_en" class="form-control" placeholder="{{ 'AlertStatus.PleaseEnterTheAlertStatusNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="AlertStatus.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="AlertStatus.AddNewAlertStatus" ></span>
                    </button>
                </span>

                <a ui-sref="alert_status.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>