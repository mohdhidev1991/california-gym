<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="AlertType.EditAlertType" translate-values="{alert_type_id: alert_type.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="AlertType.AddNewAlertType" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="AlertType.CantGetAlertType" ng-show="!alert_type.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="alert_type.id || action==='add'" >
            <div class="panel-heading font-bold" translate="AlertType.AlertTypeInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !alert_type.id}" >
                      <label for="alert_type_id" ><span translate="AlertType.AlertTypeID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="alert_type_id" ng-model="alert_type.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !alert_type.lookup_code}" >
                  <label for="lookup_code" ><span translate="AlertType.AlertTypeLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="alert_type.lookup_code" class="form-control" placeholder="{{ 'AlertType.PleaseEnterTheAlertTypeLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !alert_type.alert_type_name_ar}" >
                  <label for="alert_type_name_ar" ><span translate="AlertType.AlertTypeNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="alert_type_name_ar" ng-model="alert_type.alert_type_name_ar" class="form-control" placeholder="{{ 'AlertType.PleaseEnterTheAlertTypeNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !alert_type.alert_type_name_en}" >
                  <label for="alert_type_name_en" ><span translate="AlertType.AlertTypeNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="alert_type_name_en" ng-model="alert_type.alert_type_name_en" class="form-control" placeholder="{{ 'AlertType.PleaseEnterTheAlertTypeNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="AlertType.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="AlertType.AddNewAlertType" ></span>
                    </button>
                </span>

                <a ui-sref="alert_type.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>