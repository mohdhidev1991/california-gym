<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Attendance.EditAttendance" translate-values="{attendance_id: attendance.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Attendance.AddNewAttendance" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Attendance.CantGetAttendance" ng-show="!attendance.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="attendance.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Attendance.AttendanceInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !attendance.id}" >
                      <label for="attendance_id" ><span translate="Attendance.AttendanceID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="attendance_id" ng-model="attendance.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>



                <div class="form-group" ng-class="{'has-error': !attendance.att_comments}" >
                  <label for="att_comments" ><span translate="Attendance.AttendanceComment" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-max="128" ng-required="true" required="" id="att_comments" ng-model="attendance.att_comments" class="form-control" placeholder="{{ 'Attendance.PleaseEnterTheAttendanceComment' | translate }}">
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !attendance.att_hdate}" >
                  <label for="att_hdate" ><span translate="Attendance.AttendanceHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-max="128" ng-required="true" required="" id="att_hdate" ng-model="attendance.att_hdate" class="form-control" placeholder="{{ 'Attendance.PleaseEnterTheAttendanceHDate' | translate }}">
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="timer_att_time" ><span translate="Attendance.AttendanceTime" ></span></label>
                    <uib-timepicker ng-model="timer_att_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !attendance.attendance_type_id}" >
                    <label for="attendance_type_id" ><span translate="Attendance.AttendanceType" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="attendance.attendance_type_id" ng-options="obj.id as obj.attendance_type_name for obj in attendance_types" >
                        <option value="" >-- {{ "School.SelectTheAttendanceType" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="rejected" ><span translate="Attendance.Rejected" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="rejected" ng-model="rejected_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="Attendance.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Attendance.AddNewAttendance" ></span>
                    </button>
                </span>

                <a ui-sref="attendance.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>