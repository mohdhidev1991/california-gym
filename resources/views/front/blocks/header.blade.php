      <!-- navbar header -->
      <div class="navbar-header {{app.settings.navbarHeaderColor}}">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll-to="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#/" class="navbar-brand text-lt">
          <!--<i class="fa fa-btc"></i>-->
          <img src="{!! URL::asset("themes/admin/assets/img/logo.png") !!}"  alt="." >
          <!--<span class="hidden-folded m-l-xs">{{app.name}}</span>-->
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse box-shadow {{app.settings.navbarCollapseColor}}">
        <!-- buttons -->
        <div class="nav navbar-nav hidden-xs" ng-hide="true" >
          <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
            <i class="fa {{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}} fa-fw"></i>
          </a>
          <!--<a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="fa fa-user fa-fw"></i>
          </a>-->
        </div>
        <!-- / buttons -->

    
        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
          @if(0)
          <li class="dropdown hidden-sm" is-open="lang.isopen" dropdown>
            <a href class="dropdown-toggle" dropdown-toggle data-toggle="dropdown">
              {{selectLang}} <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInLeft w">
              <li ng-repeat="(langKey, label) in langs">
                <a ng-click="setLang(langKey, $event)" href>{{label}}</a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
          @endif
          <li class="hidden-xs">
            <a ui-fullscreen></a>
          </li>
          <li class="dropdown" dropdown>
            <a href class="dropdown-toggle clear" dropdown-toggle data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img src="{!! URL::asset("themes/admin/assets/img/avatar.jpg") !!}" alt="...">
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md">{!! Auth::user()->firstname !!} {!! Auth::user()->lastname !!}</span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu MenuProfile animated fadeInRight w">
              <!--<li class="divider"></li>-->
              <li>
                <a href="{!! url('/logout') !!}" translate="User.Logout" ></a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->

      </div>
      <!-- / navbar collapse -->
