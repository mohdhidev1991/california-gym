<!-- list -->
<ul class="nav">
  
  <li has-permission="manage_planning" ng-class="{active:$state.includes('planning.manage')}" >
    
    <a href="" class="auto" ng-class="{active:$state.includes('planning.manage')}">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-calendar icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.ManagePlanning" ></span>
    </a>

    <ul class="nav nav-sub dk">
      
      <li has-permission="manage_planning" ng-class="{active:$state.includes('planning.manage')}" >
        <a ui-sref="planning.manage" >
         <span  translate="Global.ManagePlanningRoom"></span>
        </a>
      </li>

      <li has-permission="read_stats" >
        <a ui-sref="planning.manage_coach" class="auto" ng-class="{active:$state.includes('planning.manage')}" >
          <span translate="Global.ManagePlanningCoach"></span>
        </a>
      </li>

    </ul>
  </li>

  <li has-permission="manage_home">
    <a ui-sref="course_session_fin.request_cancel_cours" class="auto" ng-class="{active:$state.includes('course_session_fin.request_cancel_cours')}" >
      <i class="glyphicon glyphicon-calendar icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.AllCoursSessionsCancels"></span>
    </a>
  </li>

  <li has-permission="manage_home">
    <a ui-sref="course_session_fin.replace_coach" class="auto" ng-class="{active:$state.includes('course_session_fin.replace_coach')}" >
      <i class="glyphicon glyphicon-calendar icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.AllCoursCoachReplace"></span>
    </a>
  </li>

  <li has-permission="manage_home">
    <a ui-sref="course_session_fin.nbr_paritcipants_cours" class="auto" ng-class="{active:$state.includes('course_session_fin.nbr_paritcipants_cours')}" >
      <i class="glyphicon glyphicon-calendar icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.AllPariticipantsByCoursSession"></span>
    </a>
  </li>

  <li has-permission="read_stats">
    <a ui-sref="planning.stats" class="auto" ng-class="{active:$state.includes('planning.stats')}" >
      <i class="glyphicon glyphicon-stats icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.Statistics"></span>
    </a>
  </li>

  <li has-permission="read_planning">
    <a ui-sref="planning.history" class="auto" ng-class="{active:$state.includes('planning.history')}" >
      <i class="glyphicon glyphicon-calendar icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.PlanningHistory"></span>
    </a>
  </li>


  <li has-permission="manage_users" >
    <a ui-sref="rea_user.all()" class="auto" ng-class="{active:$state.includes('rea_user')}" >
      <i class="glyphicon glyphicon-user icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.ManageUsers" ></span>
    </a>
  </li>

  <li has-permission="manage_roles" >
    <a ui-sref="role.all()" class="auto" ng-class="{active:$state.includes('role')}" >
      <i class="fa fa-key icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.ManageRoles" ></span>
    </a>
  </li>

  <li has-permission="manage_settings" ng-class="{active:$state.includes('setting')}" >
    <a href="" class="auto" ng-class="{active:$state.includes('setting')}" >
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-wrench icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.Setting" ></span>
    </a>
    <ul class="nav nav-sub dk">
      <li has-permission="manage_coaches" ng-class="{active:$state.includes('setting.coach')}" >
        <a ui-sref="setting.coach.all()" >
          <span translate="Global.ManageCoaches" ></span>
        </a>
      </li>
      <li has-permission="manage_cours" ng-class="{active:$state.includes('setting.course')}" >
        <a ui-sref="setting.course.all()" >
          <span translate="Global.ManageCourses" ></span>
        </a>
      </li>
      <li has-permission="manage_cours_type" ng-class="{active:$state.includes('setting.course_type')}" >
        <a ui-sref="setting.course_type.all()" >
          <span translate="Global.ManageCourseTypes" ></span>
        </a>
      </li>
      <li has-permission="manage_club" ng-class="{active:$state.includes('setting.club')}" >
        <a ui-sref="setting.club.all()" >
          <span translate="Global.ManageClubs" ></span>
        </a>
      </li>
      <li has-permission="manage_california_gym" ng-class="{active:$state.includes('setting.holidays')}" >
        <a ui-sref="setting.holidays()" >
          <span translate="Global.ManageCaliforniaGym" ></span>
        </a>
      </li>

      <li has-permission="manage_settings" ng-class="{active:$state.includes('setting.send_emails')}" >
        <a ui-sref="setting.send_emails()" >
          <span translate="Planning.SendByEmail" ></span>
        </a>
      </li>
      
    </ul>
  </li>

  <li has-permission="export_grh">
    <a ui-sref="planning.exportgrh" class="auto"  >
      <i class="glyphicon glyphicon-user icon text-primary-bleu"></i>
      <span class="font-bold" translate="Global.ExportGRH"></span>
    </a>
  </li>



  <li>
    <a href="{!! url('/logout') !!}" >
      <i class="glyphicon glyphicon-log-out icon text-primary-bleu"></i>
      <span class="font-bold" translate="User.Logout" ></span>
    </a>
  </li>

</ul>
<!-- / list -->


