<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="CandidateStatus.EditCandidateStatus" translate-values="{candidate_status_id: candidate_status.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="CandidateStatus.AddNewCandidateStatus" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="CandidateStatus.CantGetCandidateStatus" ng-show="!candidate_status.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="candidate_status.id || action==='add'" >
            <div class="panel-heading font-bold" translate="CandidateStatus.CandidateStatusInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !candidate_status.id}" >
                      <label for="candidate_status_id" ><span translate="CandidateStatus.CandidateStatusID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="candidate_status_id" ng-model="candidate_status.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !candidate_status.lookup_code}" >
                  <label for="lookup_code" ><span translate="CandidateStatus.CandidateStatusLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="candidate_status.lookup_code" class="form-control" placeholder="{{ 'CandidateStatus.PleaseEnterTheCandidateStatusLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !candidate_status.candidate_status_name_ar}" >
                  <label for="candidate_status_name_ar" ><span translate="CandidateStatus.CandidateStatusNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="candidate_status_name_ar" ng-model="candidate_status.candidate_status_name_ar" class="form-control" placeholder="{{ 'CandidateStatus.PleaseEnterTheCandidateStatusNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !candidate_status.candidate_status_name_en}" >
                  <label for="candidate_status_name_en" ><span translate="CandidateStatus.CandidateStatusNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="candidate_status_name_en" ng-model="candidate_status.candidate_status_name_en" class="form-control" placeholder="{{ 'CandidateStatus.PleaseEnterTheCandidateStatusNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="CandidateStatus.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="CandidateStatus.AddNewCandidateStatus" ></span>
                    </button>
                </span>

                <a ui-sref="candidate_status.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>