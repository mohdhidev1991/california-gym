<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="City.EditCity" translate-values="{city_id: city.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="City.AddNewCity" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="City.CantGetCity" ng-show="!city.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="city.id || action==='add'" >
            <div class="panel-heading font-bold" translate="City.CityInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !city.id}" >
                      <label for="city_id" ><span translate="City.CityID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="city_id" ng-model="city.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !city.city_name_ar}" >
                  <label for="city_name_ar" ><span translate="City.CityNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="city_name_ar" ng-model="city.city_name_ar" class="form-control" placeholder="{{ 'City.PleaseEnterTheCityNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !city.city_name_en}" >
                  <label for="city_name_en" ><span translate="City.CityNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="city_name_en" ng-model="city.city_name_en" class="form-control" placeholder="{{ 'City.PleaseEnterTheCityNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !city.country_id}" >
                    <label for="country_id" ><span translate="City.CityCountry" ></span><sup class="required" >*</sup></label>
                    <select id="country_id" class="select-form-control" ng-required="true" ng-model="city.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                        <option value="" >-- {{ "School.SelectTheCountry" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="City.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="City.AddNewCity" ></span>
                    </button>
                </span>

                <a ui-sref="city.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>