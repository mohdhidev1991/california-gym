<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="ClassCourseExam.EditClassCourseExam" translate-values="{class_course_exam_id: class_course_exam.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="ClassCourseExam.AddNewClassCourseExam" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="ClassCourseExam.CantGetClassCourseExam" ng-show="!class_course_exam.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="class_course_exam.id || action==='add'" >
            <div class="panel-heading font-bold" translate="ClassCourseExam.ClassCourseExamInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !class_course_exam.id}" >
                      <label for="class_course_exam_id" ><span translate="ClassCourseExam.ClassCourseExamID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="class_course_exam_id" ng-model="class_course_exam.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !class_course_exam.class_course_exam_name_ar}" >
                  <label for="class_course_exam_name_ar" ><span translate="ClassCourseExam.ClassCourseExamNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="class_course_exam_name_ar" ng-model="class_course_exam.class_course_exam_name_ar" class="form-control" placeholder="{{ 'ClassCourseExam.PleaseEnterTheClassCourseExamNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !class_course_exam.class_course_exam_name_en}" >
                  <label for="class_course_exam_name_en" ><span translate="ClassCourseExam.ClassCourseExamNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="class_course_exam_name_en" ng-model="class_course_exam.class_course_exam_name_en" class="form-control" placeholder="{{ 'ClassCourseExam.PleaseEnterTheClassCourseExamNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="ClassCourseExam.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="ClassCourseExam.AddNewClassCourseExam" ></span>
                    </button>
                </span>

                <a ui-sref="class_course_exam.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>