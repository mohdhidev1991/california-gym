<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Club.EditClub" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Club.AddNewClub" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default" ng-show="failed_get_data" >
          <div class="panel-body">
              <div class="alert alert-danger" translate="Global.InvalidData" ></div>
          </div>
          <div class="panel-footer" >
                <a ui-sref="club.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
        </div>

        <div class="panel panel-default" ng-show="club.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Club.ClubInformations" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="" >
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" >
                      <label for="club_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="club_id" ng-model="club.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
                
                <div class="row">
                <div class="form-group">

                <div class="col-md-9">
                  <label for="club_name" ><span translate="Field.ClubName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="true" id="club_name" ng-model="club.club_name" class="form-control" />
                </div>
                
                <div class="col-md-3">
                      <label for="code_club" ><span translate="Field.CodeClub" ></span></label>
                      <input type="text" id="code_club" ng-model="club.code_club" class="form-control" />
                </div>


                </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="phone" ><span translate="Field.Phone" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="phone" ng-model="club.phone" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="address" ><span translate="Field.Address" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="address" ng-model="club.address" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="email_1" ><span translate="Field.FirstEmail" ></span><sup class="required" >*</sup></label>
                  <input type="email" ng-required="true" required="" id="email_1" ng-model="club.email_1" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="email_2" ><span translate="Field.SecondEmail" ></span><sup class="required" >*</sup></label>
                  <input type="email" ng-required="true" required="" id="email_1" ng-model="club.email_2" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                          <tr>
                            <th class="text-left" ></th>
                            <th class="text-center" ><span translate="Days.Monday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="timer_opening_hour_1 || timer_closing_hour_1" ng-click="timer_opening_hour_1=null; timer_closing_hour_1=null" ><i class="fa fa-times" ></i></button></th>
                            <th class="text-center" ><span translate="Days.Tuesday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="timer_opening_hour_2 || timer_closing_hour_2" ng-click="timer_opening_hour_2=null; timer_closing_hour_2=null" ><i class="fa fa-times" ></i></button></th>
                            <th class="text-center" ><span translate="Days.Wednesday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="timer_opening_hour_3 || timer_closing_hour_3" ng-click="timer_opening_hour_3=null; timer_closing_hour_3=null" ><i class="fa fa-times" ></i></button></th>
                            <th class="text-center" ><span translate="Days.Thursday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="timer_opening_hour_4 || timer_closing_hour_4" ng-click="timer_opening_hour_4=null; timer_closing_hour_4=null" ><i class="fa fa-times" ></i></button></th>
                            <th class="text-center" ><span translate="Days.Friday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="timer_opening_hour_5 || timer_closing_hour_5" ng-click="timer_opening_hour_5=null; timer_closing_hour_5=null" ><i class="fa fa-times" ></i></button></th>
                            <th class="text-center" ><span translate="Days.Saturday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="timer_opening_hour_6 || timer_closing_hour_6" ng-click="timer_opening_hour_6=null; timer_closing_hour_6=null" ><i class="fa fa-times" ></i></button></th>
                            <th class="text-center" ><span translate="Days.Sunday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="timer_opening_hour_7 || timer_closing_hour_7" ng-click="timer_opening_hour_7=null; timer_closing_hour_7=null" ><i class="fa fa-times" ></i></button></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-left" ><span class="font-bold" translate="Field.OpeningHour" ></span></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_opening_hour_1" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_opening_hour_2" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_opening_hour_3" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_opening_hour_4" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_opening_hour_5" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_opening_hour_6" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_opening_hour_7" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                          </tr>

                          <tr>
                            <td class="text-left" ><span class="font-bold" translate="Field.ClosingHour" ></span></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_closing_hour_1" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="timer_closing_hour_2" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="timer_closing_hour_3" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="timer_closing_hour_4" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_closing_hour_5" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_closing_hour_6" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="timer_closing_hour_7" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                          </tr>
                        </tbody>
                    </table>
                </div>
             
              </form>


              <div class="alert alert-danger" ng-show="errors_form" >
                  <ul>
                      <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                  </ul>
              </div>

              <div class="alert alert-success" ng-show="success_form" >
                  <span translate="Form.DataSavedWithSuccess" ></span>
              </div>

            </div>

            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled=" (!form.$valid)"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Add" ></span>
                    </button>
                </span>

                <a ui-sref="setting.club.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
      </div>
    </div>
</div>