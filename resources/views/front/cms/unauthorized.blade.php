

  <div class="text-center m-b-lg">
    <h1 class="text-shadow text-center text-white"><span translate="Global.UnauthorizedPage" ></span></h1>
  </div>
 <div class="container w-xxl w-auto-xs" >
  <div class="list-group bg-info auto m-b-sm m-b-lg">
    <a href="#/" class="list-group-item">
      <i class="fa fa-chevron-right text-muted"></i>
      <i class="fa fa-fw fa-mail-forward m-r-xs"></i> <span translate="Global.BackToHome" ></span>
    </a>
  </div>
</div>