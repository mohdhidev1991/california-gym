<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.ManageCoaches" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="Coach.AllCoaches" ></div>

            

            <div class="panel-header panel-header with-padding">
                <div class="header-tools header-btns" >
                    <a class="btn btn-default" ui-sref="setting.coach.add({add: true})" ><i class="fa fa-plus" ></i><span translate="Coach.AddNewCoach" ></span></a>
                </div>
            </div>
            <div class="row">

            <div class="col-md-4">
            <div class="panel-header panel-header with-padding">
            <div class="btn-group-dropdown" uib-dropdown>
                            
                            <button id="cour-button" type="button" class="btn btn-block"  ng-class="{'btn-primary': course, 'btn-default': !course}" uib-dropdown-toggle ng-disabled="disabled">
                                <span translate="Form.SelectCours" ng-show="!course" ></span>
                                <span ng-show="course" >{{course.course_name}}</span>
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="cour-dropdown">
                                <li ng-repeat="obj in courses">
                                    <a ng-click="select_cours(obj)" ng-class="{active: course.id===obj.id}" >{{obj.course_name}}</a>
                                </li>
                            </ul>
            </div>
            </div>
            </div>
            
            <div class="col-md-4">
            <div class="panel-header panel-header with-padding">
                   <input class="form-control" placeholder="Nom Prénom ..." type="text"  id="nameCoach"  />
            </div>
            </div>

            <div class="col-md-4">
            <div class="panel-header panel-header with-padding">
                   <button ng-click="GetCoachs()" class="btn btn-primary"  >Rechercher</button>
            </div>
            </div>
            

            </div>
            <div class="row">
            <div class="panel-body">
                <div class="table-responsive">

                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:20px;" >
                                    <label translate="Field.Active" ></label>
                                </th>
                                <th style="width:20px;" >
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th>
                                    <label translate="Field.CoachName" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="coach in coachs" >
                                <td>
                                    <span >
                                        <i ng-show="coach.active==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="coach.active==='N'" class="fa fa-times text-danger text"></i>
                                    </span>
                                </td>
                                <td>
                                    <span>{{coach.id}}</span>
                                </td>
                                <td>
                                    <span>{{coach.firstname}} {{coach.lastname}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" ui-sref="setting.coach.edit({coach_id: coach.id})" ><i class="fa fa-edit"></i><span translate="Global.Edit" ></span></a>
                                    <a class="btn btn-xs btn-danger" ng-click="delete(coach.id)" ><i class="fa fa-remove"></i><span translate="Global.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    
                </div>
            </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center"></div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetCoachs()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
            <br>
            <div ng-show="url_export_coach">
                <div class="row" >
                    <div class="col-sm-12 text-right" >
                        <a class="btn btn-sm btn-default" ng-href="{{url_export_coach}}" target="_blank" ><i class="fa fa-file-excel-o" ></i> <span translate="Global.ExportData" ></span></a>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Coach.AreYouSureToDeleteThisCoach' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>