<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Coach.EditCoach" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Coach.AddNewCoach" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default" ng-show="failed_get_data" >
          <div class="panel-body">
              <div class="alert alert-danger" translate="Global.InvalidData" ></div>
          </div>
          <div class="panel-footer" >
                <a ui-sref="coach.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
        </div>

        <div class="panel panel-default" ng-show="coach.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Coach.CoachInformations" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="" >
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" >
                      <label for="coach_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="coach_id" ng-model="coach.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                    <label for="user_id" ><span translate="Field.User" ></span></label>
                    <select class="select-form-control" ng-model="coach.user_id" ng-options="obj.id as (obj.firstname+' '+obj.lastname) for obj in users" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                

                <div class="form-group" >
                  <label for="firstname" ><span translate="Field.FirstName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="true" id="firstname" ng-model="coach.firstname" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="lastname" ><span translate="Field.LastName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="true" id="lastname" ng-model="coach.lastname" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                
                
                <div class="form-group">
                    <label for="logo" >Photo<sup class="required" >*</sup></label>
                    <button ng-hide="coach.photo_file_id" class="btn btn-default" ng-click="upload_photo()" ><i class="fa fa-upload" ></i><span translate="Field.Logo" ></span></button>
                    <div ng-show="coach.photo_file_id" >
                      <img ng-src="{{photo_src_url()}}" class="img img-max-100 img-logo" />
                      <button type="button" class="btn btn-danger btn-xs" ng-click="remove_photo(item)" ><i class="fa fa-times" ></i><span translate="Global.Remove" ></span></button>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" >
                  <label for="email" ><span translate="Field.Email" ></span></label>
                  <input type="email" id="email" ng-model="coach.email" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="mobile" ><span translate="Field.Phone" ></span></label>
                  <input type="text" id="mobile" ng-model="coach.mobile" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" >
                  <label for="registration_number" ><span translate="Field.RegistrationNumber" ></span></label>
                  <input type="text" id="registration_number" ng-model="coach.registration_number" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="course_pricing" ><span translate="Field.CoursePricing" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="true" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" step="1" id="course_pricing" ng-model="coach.course_pricing" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="week_hours_min" ><span translate="Field.MinimumNumberOfHoursPerWeek" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="true" ng-pattern="/^[0-9]{1,7}$/" id="week_hours_min" ng-model="coach.week_hours_min" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="week_hours_max" ><span translate="Field.MaximumNumberOfHoursPerWeek" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="true" ng-pattern="/^[0-9]{1,7}$/" id="week_hours_max" ng-model="coach.week_hours_max" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="day_hours_max" ><span translate="Field.MaximumNumberOfCoachCoursesPerDay" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="true" ng-pattern="/^[0-9]{1,7}$/" id="day_hours_max" ng-model="coach.day_hours_max" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="week_max_price" ><span translate="Field.MaximumPricePerWeek" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="true" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" step="1" id="week_max_price" ng-model="coach.week_max_price" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="clubs" ><span translate="Field.Club" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in clubs" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, coach.clubs)" ng-click="toggle_checkbox_option(obj.id, coach.clubs)" ><i></i>{{obj.club_name}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="courses" ><span translate="Field.Courses" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in courses" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, coach.courses)" ng-click="toggle_checkbox_option(obj.id, coach.courses)" ><i></i>{{obj.course_name}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="Disponibility" class="font-bold" ><span translate="Field.Disponibility" ></span></label>
                  <div class="table-responsive">
                      <table class="table table-striped b-t b-light">
                          <thead>
                            <tr>
                              <th class="text-left" ><span translate="Global.Morning" ></span></th>
                              <th class="text-center" ><span translate="Days.Monday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="morning_starting_hour_1 || morning_ending_hour_1" ng-click="morning_starting_hour_1=null; morning_ending_hour_1=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Tuesday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="morning_starting_hour_2 || morning_ending_hour_2" ng-click="morning_starting_hour_2=null; morning_ending_hour_2=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Wednesday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="morning_starting_hour_3 || morning_ending_hour_3" ng-click="morning_starting_hour_3=null; morning_ending_hour_3=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Thursday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="morning_starting_hour_4 || morning_ending_hour_4" ng-click="morning_starting_hour_4=null; morning_ending_hour_4=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Friday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="morning_starting_hour_5 || morning_ending_hour_5" ng-click="morning_starting_hour_5=null; morning_ending_hour_5=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Saturday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="morning_starting_hour_6 || morning_ending_hour_6" ng-click="morning_starting_hour_6=null; morning_ending_hour_6=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Sunday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="morning_starting_hour_7 || morning_ending_hour_7" ng-click="morning_starting_hour_7=null; morning_ending_hour_7=null" ><i class="fa fa-times" ></i></button></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="text-left" ><span class="font-bold" translate="Field.StartingHour" ></span></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_starting_hour_1" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_starting_hour_2" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_starting_hour_3" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_starting_hour_4" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_starting_hour_5" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_starting_hour_6" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_starting_hour_7" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            </tr>

                            <tr>
                              <td class="text-left" ><span class="font-bold" translate="Field.EndingHour" ></span></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_ending_hour_1" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="morning_ending_hour_2" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="morning_ending_hour_3" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="morning_ending_hour_4" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_ending_hour_5" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_ending_hour_6" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="morning_ending_hour_7" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            </tr>
                          </tbody>
                       </table>
                  </div>
                  <div class="table-responsive">
                      <table class="table table-striped b-t b-light">
                          <thead>
                            <tr>
                              <th class="text-left" ><span class="font-bold" translate="Global.Afternoon" ></span></th>
                              <th class="text-center" ><span translate="Days.Monday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="afternoon_starting_hour_1 || afternoon_ending_hour_1" ng-click="afternoon_starting_hour_1=null; afternoon_ending_hour_1=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Tuesday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="afternoon_starting_hour_2 || afternoon_ending_hour_2" ng-click="afternoon_starting_hour_2=null; afternoon_ending_hour_2=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Wednesday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="afternoon_starting_hour_3 || afternoon_ending_hour_3" ng-click="afternoon_starting_hour_3=null; afternoon_ending_hour_3=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Thursday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="afternoon_starting_hour_4 || afternoon_ending_hour_4" ng-click="afternoon_starting_hour_4=null; afternoon_ending_hour_4=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Friday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="afternoon_starting_hour_5 || afternoon_ending_hour_5" ng-click="afternoon_starting_hour_5=null; afternoon_ending_hour_5=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Saturday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="afternoon_starting_hour_6 || afternoon_ending_hour_6" ng-click="afternoon_starting_hour_6=null; afternoon_ending_hour_6=null" ><i class="fa fa-times" ></i></button></th>
                              <th class="text-center" ><span translate="Days.Sunday" ></span> <button class="btn btn-xs btn-icon btn-danger" ng-show="afternoon_starting_hour_7 || afternoon_ending_hour_7" ng-click="afternoon_starting_hour_7=null; afternoon_ending_hour_7=null" ><i class="fa fa-times" ></i></button></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="text-left" ><span class="font-bold" translate="Field.StartingHour" ></span></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_starting_hour_1" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_starting_hour_2" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_starting_hour_3" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_starting_hour_4" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_starting_hour_5" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_starting_hour_6" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_starting_hour_7" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            </tr>

                            <tr>
                              <td class="text-left" ><span class="font-bold" translate="Field.EndingHour" ></span></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_ending_hour_1" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="afternoon_ending_hour_2" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="afternoon_ending_hour_3" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block " ><uib-timepicker ng-model="afternoon_ending_hour_4" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_ending_hour_5" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_ending_hour_6" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                              <td class="text-center" ><div class="form-group display-inline-block" ><uib-timepicker ng-model="afternoon_ending_hour_7" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false"></uib-timepicker></div></td>
                            </tr>
                          </tbody>
                       </table>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                
                
                

                <div class="form-group">
                    <label for="description" ><span translate="Field.Description" ></span></label>
                     <text-angular ng-model="coach.descenrichi"></text-angular>
                    </label>
                </div>


                <div class="form-group">
                    <label for="note" ><span translate="Field.Note" ></span></label>
                     <textarea class="form-control" ng-model="coach.note" row="3"></textarea>
                    </label>
                </div>

                
                <div class="line line-dashed b-b line-lg pull-in"></div>
                

                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
              
              </form>


              <div class="alert alert-danger" ng-show="errors_form" >
                  <ul>
                      <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                  </ul>
              </div>

              <div class="alert alert-success" ng-show="success_form" >
                  <span translate="Form.DataSavedWithSuccess" ></span>
              </div>

            </div>

            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled=" (!form.$valid)"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Add" ></span>
                    </button>
                </span>

                <a ui-sref="setting.coach.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
      </div>
    </div>
</div>

<script type="text/ng-template" id="ModalUploadFilePhoto.html">
    <div class="modal-header">
      <h3 class="modal-title" translate="File.UploadLogo"></h3>
    </div>
    <div class="modal-body">
      <div nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
          <div class="form-group" >
              <label for="upload_file" translate="Form.SelectFile"></label>
              <input type="file" id="upload_file" nv-file-select="" uploader="uploader" multiple="">
          </div>
          <div>
              <div class="progress bg-light dker" style="" ng-show="uploader.isUploading" >
                  <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
              </div>
          </div>
          <div class="alert alert-success" ng-show="success_upload" >
              <span translate="Form.SuccessUpload" ></span>
          </div>
          <div class="alert alert-danger" ng-show="errors_upload">
              <ul>
                  <li ng-repeat="error in errors_upload">
                      <span>{{ error | translate }}</span>
                  </li>
              </ul>
          </div>
      </div>
    </div>
    <div class="modal-footer">
        <!--<button class="btn btn-primary" type="button" ng-click="finish_upload()" translate="Form.Finish" ></button>-->
        <button class="btn btn-danger" type="button" ng-click="cancel_import()" translate="Global.Close" ></button>
    </div>
</script>


