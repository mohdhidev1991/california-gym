<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Country.EditCountry" translate-values="{country_id: country.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Country.AddNewCountry" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Country.CantGetCountry" ng-show="!country.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="country.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Country.CountryInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !country.id}" >
                      <label for="country_id" ><span translate="Country.CountryID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="country_id" ng-model="country.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !country.country_name_ar}" >
                  <label for="country_name_ar" ><span translate="Country.CountryNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="country_name_ar" ng-model="country.country_name_ar" class="form-control" placeholder="{{ 'Country.PleaseEnterTheCountryNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" ng-class="{'has-error': !country.country_name_en}" >
                  <label for="country_name_en" ><span translate="Country.CountryNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="country_name_en" ng-model="country.country_name_en" class="form-control" placeholder="{{ 'Country.PleaseEnterTheCountryNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="time_offset" ><span translate="Country.CountryTimeOffset" ></span></label>
                    <input type="number" id="time_offset" ng-model="country.time_offset" class="form-control" placeholder="{{ 'Country.PleaseEnterTheCountryTimeOffset' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="maintenance_start_time" ><span translate="Country.CountryMaintenanceStartTime" ></span></label>
                    <uib-timepicker ng-model="timer_maintenance_start_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="maintenance_end_time" ><span translate="Country.CountryMaintenanceEndTime" ></span></label>
                  <uib-timepicker ng-model="timer_maintenance_end_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="abrev" ><span translate="Country.CountryAbrev" ></span></label>
                  <input type="text" id="abrev" ng-model="country.abrev" class="form-control" placeholder="{{ 'Country.PleaseEnterTheCountryAbrev' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="nationalty_name_ar" ><span translate="Country.NationaltyNameAr" ></span></label>
                  <input type="text" id="nationalty_name_ar" ng-model="country.nationalty_name_ar" class="form-control" placeholder="{{ 'Country.PleaseEnterTheCountryNationaltyNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="nationalty_name_en" ><span translate="Country.NationaltyNameEN" ></span></label>
                  <input type="text" id="nationalty_name_en" ng-model="country.nationalty_name_en" class="form-control" placeholder="{{ 'Country.PleaseEnterTheCountryNationaltyNameEN' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="date_system_id" ><span translate="Country.CountryDateSystem" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-model="country.date_system_id" ng-options="obj.id as obj.date_system_name for obj in dates_system" >
                        <option value="" >-- {{ "School.SelectTheCountryDateSystem" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="wdays" ><span translate="School.WDays" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in wdays" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="CheckboxObjectHelper.check_selected_option(obj.id, country.we_days)" ng-click="CheckboxObjectHelper.toggle_checkbox_option(obj.id, country.we_days)" ><i></i>{{obj.wday_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="idntype" ><span translate="Country.IdnType" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in idn_types" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="CheckboxObjectHelper.check_selected_option(obj.id, country.sa_idn_types)" ng-click="CheckboxObjectHelper.toggle_checkbox_option(obj.id, country.sa_idn_types)" ><i></i>{{obj.idn_type_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="Country.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Country.AddNewCountry" ></span>
                    </button>
                </span>

                <a ui-sref="country.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>