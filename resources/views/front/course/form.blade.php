<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Course.EditCourse" translate-values="{course_id: course.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Course.AddNewCourse" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default" ng-show="failed_get_data" >
          <div class="panel-body">
              <div class="alert alert-danger" translate="Global.InvalidData" ></div>
          </div>
          <div class="panel-footer" >
                <a ui-sref="setting.course.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
        </div>

        <div class="panel panel-default" ng-show="course.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Course.CourseInformations" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" >
                      <label for="course_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="course_id" ng-model="course.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                    <div class="row">

                    <div class="col-md-6">
                        <button ng-hide="course.logo_file_id" class="btn btn-default" ng-click="upload_logo()" ><i class="fa fa-upload" ></i><span translate="Field.Logo" ></span></button>
                        <div ng-show="course.logo_file_id" >
                        <img ng-src="{{logo_src_url()}}" class="img img-max-100 img-logo" />
                        <button type="button" class="btn btn-danger btn-xs" ng-click="remove_logo(item)" ><i class="fa fa-times" ></i><span translate="Global.Remove" ></span></button>
                        </div>
                    </div>

                    <div class="col-md-4">
                      <label for="course_code" ><span translate="Field.CourseCode" ></span><sup class="required" >*</sup></label>
                      <input type="text"  id="course_code" ng-required="true" ng-model="course.course_code" class="form-control" />
                    </div>
                    
                    <div class="col-md-2">
                      <label for="nbr_hours" ><span translate="Field.NbrHours" ></span><sup class="required" >*</sup></label>
                      <input type="number" id="nbr_hours" ng-required="true"  step="any" min="0" max="100"   ng-model="course.nbr_hours" class="form-control" />
                    </div>

                    </div>
                </div>

                <div class="form-group" >
                    <label for="color" ><span translate="Field.Color" ></span><sup class="required" >*</sup></label>
                    <input class="form-control" colorpicker ng-required="true" type="text" ng-model="course.color" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" >
                  <label for="course_name" ><span translate="Field.CourseName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="course_name" ng-model="course.course_name" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="during" ><span translate="Field.During" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="" ng-pattern="/^[0-9]{1,7}$/" id="during" ng-model="course.during" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="max_courses" ><span translate="Field.MaximumNumberOfCoursesPerDayPerCoach" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="" ng-pattern="/^[0-9]{1,7}$/" id="max_courses" ng-model="course.max_courses" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="course_type_id" ><span translate="Field.CourseType" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="course.course_type_id" ng-options="obj.id as obj.course_type_name for obj in course_types" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="course_for" ><span translate="Field.CourseFor" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in course_fors" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, course.course_fors)" ng-click="toggle_checkbox_option(obj.id, course.course_fors)" ><i></i>{{obj.course_for_name}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="rooms" ><span translate="Field.RoomClubAssociated" ></span></label>
                    <div ng-repeat="club in clubs" class="box-bordered-left margin-bottom-15" >
                        <label for="rooms" class="font-bold" ><span>{{club.club_name}}</span></label>
                        <div ng-show="!club.rooms.length" class="alert alert-warning" ><span translate="Club.NoRoomsAddedForThisClub" ></span></div>
                        <div class="checkbox" ng-show="club.rooms" >
                          <label class="i-checks" ng-repeat="obj in club.rooms" >
                              <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, course.rooms)" ng-click="toggle_checkbox_option(obj.id, course.rooms)" ><i></i>{{obj.room_name}}
                          </label>
                        </div>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>


                <div class="alert alert-danger" ng-show="errors_form" >
                    <ul>
                        <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                    </ul>
                </div>

                <div class="alert alert-success" ng-show="success_form" >
                    <span translate="Form.DataSavedWithSuccess" ></span>
                </div>
                
              </form>
            </div>


            <div class="panel-footer" >
                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>

                <a ui-sref="setting.course.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
      </div>
    </div>
</div>


<script type="text/ng-template" id="ModalUploadFileLogo.html">
    <div class="modal-header">
      <h3 class="modal-title" translate="File.UploadLogo"></h3>
    </div>
    <div class="modal-body">
      <div nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
          <div class="form-group" >
              <label for="upload_file" translate="Form.SelectFile"></label>
              <input type="file" id="upload_file" nv-file-select="" uploader="uploader" multiple="">
          </div>
          <div>
              <div class="progress bg-light dker" style="" ng-show="uploader.isUploading" >
                  <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
              </div>
          </div>
          <div class="alert alert-success" ng-show="success_upload" >
              <span translate="Form.SuccessUpload" ></span>
          </div>
          <div class="alert alert-danger" ng-show="errors_upload">
              <ul>
                  <li ng-repeat="error in errors_upload">
                      <span>{{ error | translate }}</span>
                  </li>
              </ul>
          </div>
      </div>
    </div>
    <div class="modal-footer">
        <!--<button class="btn btn-primary" type="button" ng-click="finish_upload()" translate="Form.Finish" ></button>-->
        <button class="btn btn-danger" type="button" ng-click="cancel_import()" translate="Global.Close" ></button>
    </div>
</script>



