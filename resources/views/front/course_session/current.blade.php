<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="CourseSession.CurrentCourseSessionLabel" translate-values="{label: current_course_session.course_name_ar}" ></h1>
    </div>
    <div class="wrapper-md">

        <div ng-show="!current_course_session" class="alert alert-warning" ><span translate="CourseSession.NoCourseSessionOpenedCurrently" ></span></div>

        <div ng-show="!students && current_course_session" class="alert alert-danger" translate="CourseSession.EmptyStudents" ></div>
        
        <div class="BoxsCurrentCourseSession" ng-show="students && current_course_session" >
            <div class="row" >
                <div class="col-sm-12 col-md-9 col-lg-9" >
                    <div class="box_presence_students box_presence_students_present" >
                        <span class="icon_presence_student icon_status_on_time" ></span>
                        <strong><span translate="CourseSession.PresentStudents" ></span></strong>
                        <span class="counter counter_present" ><span class="inner_counter" >{{count_present_students()}}/{{count_total_students()}}</span></span>
                    </div>
                    <div class="box_presence_students box_presence_students_absent" >
                        <span class="icon_presence_student icon_status_absent" ></span>
                        <strong><span translate="CourseSession.AbsentStudents" ></span></strong>
                        <span class="counter counter_absent" ><span class="inner_counter" >{{count_absent_students()}}/{{count_total_students()}}</span></span>
                    </div>
                    <div class="box_presence_students box_presence_students_late" >
                        <span class="icon_presence_student icon_status_on_late" ></span>
                        <strong><span translate="CourseSession.LateStudents" ></span></strong>
                        <span class="counter counter_late" ><span class="inner_counter" >{{count_late_students()}}/{{count_total_students()}}</span></span>
                    </div>
                    <div class="box_presence_students box_presence_students_late" >
                        <span class="icon_presence_student icon_status_waiting_he_come" ></span>
                        <strong><span translate="CourseSession.WaitingStudents" ></span></strong>
                        <span class="counter counter_waiting" ><span class="inner_counter" >{{count_waiting_students()}}/{{count_total_students()}}</span></span>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3" >
                    <div class="btns_edit_current_session" >
                        <div class="form-group" >
                            <a class="btn btn-primary btn-block" ui-sref="course_session.edit_current({date:current_course_session.session_hdate})" ><i class="fa fa-pencil" ></i><span translate="CourseSession.EditPresence" ></span></a>
                        </div>
                        <div class="form-group" >
                            <a class="btn btn-danger btn-block" ng-click="finish_course_session()" ><i class="fa fa-times"></i><span translate="CourseSession.FinishSession" ></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>