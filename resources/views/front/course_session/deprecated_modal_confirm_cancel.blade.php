<div class="modal-header">
    <h3 class="modal-title" >{{ 'CourseSession.AreYouSureToCancelThisCourse' | translate }}</h3></div>
	<div class="modal-body">
		<div class="modal-body-form form-group" ng-class="{'has-error': no_comment}" >
			<label for="comment">{{ 'CourseSession.TheReason' | translate }} :</label>
			<textarea  ng-model="comment" class="form-control" id="comment" required="true" ></textarea>
		</div>
		<div class="alert alert-danger" ng-model="alert_comment" ng-show="no_comment" >{{ 'CourseSession.PleaseEnterTheReasonToCancelThisCourseSession' | translate }}</div>
	</div>
<div class="modal-footer">
	<button class="btn btn-default" ng-click="close_cancelmodal()">{{ 'Global.No' | translate }}</button>
    <button class="btn btn-primary" ng-click="submit_cancelmodal()">{{ 'Global.Yes' | translate }}</button>
</div>