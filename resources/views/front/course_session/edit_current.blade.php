<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="CourseSession.EditPresenceTimeCourseSessionNumberLabel" translate-values="{ordre: course_session.session_order, label: course_session.course_name_ar}" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="liste-students-course-session">
            <ul class="list-unstyled row">
                <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item_student item_student_coming_status_id_{{student_session.coming_status_id}}" ng-repeat="student_session in students | filter: InitStudentPresence" ng-click_="change_student_presence(this)" data-student-id="{{student_session.student_id}}" data-coming_status_id="{{student_session.coming_status_id}}" data-exit_status_id="{{student_session.exit_status_id}}">
                    <div class="box-student-presence">
                        <h4 class="font-bold">{{student_session.fullname}}</h4>
                        <!--<h5 class="font-bold">coming_status_id: {{student_session.coming_status_id}}</h5>
                        <h5 class="font-bold">coming_time: {{student_session.coming_time}}</h5>-->
                        <div class="icons_presence_student">
                            <!--<span class="icon_status_not_called_yet" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_Not_called_yet !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_Not_called_yet !!})">Not_called_yet</span>-->
                            <span uib-tooltip="{{ 'AttendanceStatus.OnTime' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_on_time" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_On_time !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_On_time !!})">On_time</span>
                            <span uib-tooltip="{{ 'AttendanceStatus.OnLate' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_on_late" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_On_late !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_On_late !!})">On_late</span>
                            <span uib-tooltip="{{ 'AttendanceStatus.WaitingHeCome' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_waiting_he_come" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_Waiting_he_come !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_Waiting_he_come !!})">Waiting_he_come</span>
                            <span uib-tooltip="{{ 'AttendanceStatus.Absent' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_absent" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_Absent !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_Absent !!})">Absent</span>
                            <!--<span class="icon_status_early_quitted" ng-class="{'active': student_session.exit_status_id=={!! Attendance_status_Early_quitted !!}}" ng-click="set_exit_status_id({!! Attendance_status_Early_quitted !!})">Early_quitted</span>-->
                        </div>

                        <div class="form-group text-center" >
                            <div class="input-append form-group-uib-timepicker">
                                <i class="fa fa-clock-o"></i>
                                <uib-timepicker ng-disabled="student_session.coming_status_id!={!! Attendance_status_On_late !!}" ng-click="update_student_coming_time(this)" min="uitime_global_coming_time" ng-model="student_session.timepicker" minute-step="mstep" hour-step="hstep" show-meridian="false"></uib-timepicker>
                            </div>
                        </div>
                        <button ng-class="{'btn-primary': student_session.comments}" class="btn btn-default btn-icon btn-icon-no-margin btn-min btn-comment-course-session" ng-click="add_comment(student_session)" ><i class="fa fa-commenting" aria-hidden="true"></i></button>
                    </div>
                </li>
            </ul>
        </div>
        <div ng-show="!students" class="alert alert-warning">{{ 'CourseSession.EmptyStudents' | translate }}</div>

        <div ng-show="students" class="btns_submit_open_session zindex999" uib-tooltip="{{ 'CourseSession.PleaseEnterAllStudentCommingStatus' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!can_submit_save_edit" tooltip-class="tooltip-danger zindex9999" >
            <a ng-click="submit_save_edit()" class="btn btn-primary"  ng-class="{'btn-disabled': !can_submit_save_edit}" ng-disabled="!can_submit_save_edit" ><i class="fa fa-floppy-o"></i><span>{{ 'CourseSession.SaveData' | translate }}</span></a>
        </div>
    </div>
</div>







<script type="text/ng-template" id="ModalCommentStudentSession.html" >
    <div class="modal-header">
        <h3 class="modal-title" translate="CourseSession.CommentStudentSession" ></h3></div>
        <div class="modal-body">
            <div class="modal-body-form form-group" >
                <label for="comments">{{ 'CourseSession.Comments' | translate }} :</label>
                <textarea  ng-model="student_session.comments" class="form-control" id="comments" ></textarea>
            </div>
            <div class="alert alert-success" ng-show="success" >{{ 'CourseSession.SuccessUpdateStudentSession' | translate }}</div>
        </div>
    <div class="modal-footer">
        <button class="btn btn-primary" ng-click="submit()">{{ 'Global.Update' | translate }}</button>
        <button class="btn btn-danger" ng-click="close()">{{ 'Global.Cancel' | translate }}</button>
    </div>
</script>