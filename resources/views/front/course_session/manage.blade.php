<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Administration.ManageCoursesSession" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="CourseSession.AllCoursesSessions" ></div>
            <div class="panel-header panel-header with-padding">

                <div class="filter filter-table" >
                    <form name="filter" ng-class="{}">
                        <div class="header-filter" >
                        </div>
                        <div class="body-filter" >


                            <div class="form-group" >
                                <label for="filter_session_status_id" ><span translate="Field.CourseSessionStatus" ></span></label>
                                <select id="filter_session_status_id" class="select-form-control" ng-model="filter.session_status_id" ng-options="obj.id as obj.session_status_name_ar | translate for obj in session_statuss" >
                                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                </select>
                            </div>

                            <div class="form-group"  >
                                <label for="filter_teacher_name_or_idn" ><span translate="Field.Teacher" ></span></label>
                                <input type="text" ng-model="filter.teacher" uib-typeahead="teacher as teacher.fullname for teacher in getTeachers($viewValue)" typeahead-loading="loadingTeachers" typeahead-no-results="noTeachers" class="form-control" placeholder="{{ 'Form.EnterTeacherIdnOrNameOrMobileOrSchoolJob' | translate }}" >
                                <i ng-show="loadingTeachers" class="glyphicon glyphicon-refresh"></i>
                                <div ng-show="noTeachers">
                                  <i class="glyphicon glyphicon-remove"></i> <span translate="Global.NoResultsFound" ></span>
                                </div>
                                <div class="form-group" ng-show="filter.teacher.id" >
                                    <pre>{{filter.teacher.fullname}} [IDN: {{filter.teacher.idn}}]</pre>
                                </div>
                            </div>

                            <div class="form-group" >
                              <label for="filter_session_hdate" ><span translate="Field.Date" ></span></label>
                              <islamic-datepicker id="filter_session_hdate" ng-model="filter.session_hdate" ng-model-hdate="filter.session_hdate" />
                            </div>

                            <div ng-show="show_all_filters" >
                                <div class="form-group" >
                                    <label for="filter_course_id" ><span translate="Field.Course" ></span></label>
                                    <select id="filter_course_id" class="select-form-control" ng-model="filter.course_id" ng-options="obj.id as obj.course_name_ar for obj in courses" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                            </div>

                            <div ng-show="show_all_filters" >
                                <div class="form-group" >
                                    <label for="filter_level_class_id" ><span translate="Field.LevelClass" ></span></label>
                                    <select id="filter_level_class_id" class="select-form-control" ng-model="filter.level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="footer-filtrer" >
                            <span class="checkbox">
                                <label class="i-checks" >
                                    <input type="checkbox" ng-model="show_all_filters" ng-change="filter.teacher = filter.course_id = filter.hdate = filter.level_class_id = filter.session_hdate = null;" ><i></i> <span translate="Form.ShowAllFilters" ></span>
                                </label>
                            </span>
                            <button class="btn btn-primary" ng-click="currentPage=1; GetCourseSessions();" ><i class="fa fa-filter"></i><span translate="Form.Search" ></span></button>
                            <button class="btn btn-danger" ng-show="filter.teacher.id || filter.session_status_id || filter.course_id || filter.hdate || filter.level_class_id || filter.session_hdate" ng-click="filter={}; currentPage=1; GetCourseSessions();" ><i class="fa fa-times" ></i><span translate="Form.Reset" ></span></button>
                        </div>
                    </form>
                    
                </div>
                <div class="line line-dashed b-b line-lg"></div>
            </div>



            <div class="panel-body">
                <div class="alert alert-danger" ng-show="empty_results" >
                    <span translate="Global.NoResultsFound" ></span>
                </div>

                <div class="table-responsive" ng-show="course_sessions" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='session_status_name_ar', th_sorting_asc: order_by==='session_status_name_ar' && order==='DESC', th_sorting_desc: order_by==='session_status_name_ar' && order==='ASC' }" ng-click="set_order('session_status_name_ar', null )" >
                                    <label translate="Field.CourseSessionStatus" ></label>
                                </th>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='session_hdate', th_sorting_asc: order_by==='session_hdate' && order==='DESC', th_sorting_desc: order_by==='session_hdate' && order==='ASC' }" ng-click="set_order('session_hdate', null )" >
                                    <label translate="Field.Date" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='level_class_order', th_sorting_asc: order_by==='level_class_order' && order==='DESC', th_sorting_desc: order_by==='level_class_order' && order==='ASC' }" ng-click="set_order('level_class_order', null )" >
                                    <label translate="Field.LevelClass" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='course_name_ar', th_sorting_asc: order_by==='course_name_ar' && order==='DESC', th_sorting_desc: order_by==='course_name_ar' && order==='ASC' }" ng-click="set_order('course_name_ar', null )" >
                                    <label translate="Field.Course" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='prof_firstname', th_sorting_asc: order_by==='prof_firstname' && order==='DESC', th_sorting_desc: order_by==='prof_firstname' && order==='ASC' }" ng-click="set_order('prof_firstname', null )" >
                                    <label translate="Field.TeacherName" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="course_session in course_sessions" >
                                <td>
                                    <span>{{course_session.session_status_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{course_session.session_hdate}} - {{course_session.session_start_time}}</span>
                                </td>
                                <td>
                                    <span>{{course_session.level_class_name_ar}} - {{course_session.symbol | translate_symbol}}</span>
                                </td>
                                <td>
                                    <span>{{course_session.course_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{course_session.prof_firstname + ' ' +course_session.prof_f_firstname+' '+course_session.prof_lastname}}</span>
                                </td>
                                
                                <td>
                                    <a class="btn btn-sm btn-primary btn-xs" ui-sref="course_session.manage_item({year: course_session.year, hmonth: course_session.hmonth, hday_num: course_session.hday_num, level_class_id: course_session.level_class_id, symbol: course_session.symbol, session_order: course_session.session_order})" ><i class="fa fa-edit"></i><span translate="CourseSession.ManageCourseSession" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer" ng-hide="empty_results" >

                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetCourseSessions()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" max-size="5" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Form.AreYouSureToDeleteThisData' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>