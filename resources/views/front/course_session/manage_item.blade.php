<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ><span translate="CourseSession.ManageCourseSession" ></span></h1>
    </div>
    <div class="wrapper-md">
        
        <div ng-hide="edit_coming_status" >
            <div class="panel panel-default" >
                <div class="panel-heading font-bold" translate="CourseSession.CourseSessionInformations" ></div>
                <div class="panel-body">
                    <p>
                        <label class="font-bold" translate="Field.Date" ></label>: <span>{{course_session.session_hdate}}</span>
                    </p>
                    <p>
                        <label class="font-bold" translate="Field.Course" ></label>: <span>{{course_session.course_name_ar}}</span>
                    </p>
                    <p>
                        <label class="font-bold" translate="Field.LevelClass" ></label>: <span>{{course_session.level_class_name_ar}}</span>
                    </p>
                    <p>
                        <label class="font-bold" translate="Field.Symbol" ></label>: <span>{{course_session.symbol | translate_symbol}}</span>
                    </p>
                    <p>
                        <label class="font-bold" translate="Field.TeacherName" ></label>: <span>{{course_session.prof_firstname + ' ' +course_session.prof_f_firstname+' '+course_session.prof_lastname}}</span>
                    </p>
                    <p>
                        <label class="font-bold" translate="Field.CourseSessionStatus" ></label>: <span>{{course_session.session_status_name_ar}}</span>
                    </p>
                    <p ng-show="course_session.session_status_comment" >
                        <label class="font-bold" translate="Field.Comment" ></label>: <span>{{course_session.session_status_comment}}</span>
                    </p>
                </div>
                <div class="panel-footer" >

                    <div uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg | translate}}</div>

                    <button ng-show="course_session.session_status_id===1 || course_session.session_status_id===5" type="submit" class="btn btn-sm btn-danger" ng-click="cancel_course_session()" >
                        <i class="fa fa-times" ></i><span translate="CourseSession.CancelCourseSession" ></span>
                    </button>
                    <button ng-show="course_session.session_status_id===1 || course_session.session_status_id===5" type="submit" class="btn btn-sm btn-primary" ng-click="change_teacher_course_session('item')" >
                        <i class="fa fa-exchange" ></i><span translate="CourseSession.ChangingTeacherForThisCourseSession" ></span>
                    </button>
                    <button ng-show="course_session.session_status_id===1 || course_session.session_status_id===5" type="submit" class="btn btn-sm btn-primary" ng-click="change_teacher_course_session('items')" >
                        <i class="fa fa-exchange" ></i><span translate="CourseSession.ChangingTeacherForThisCourseSessionStartingFromCertainDate" ></span>
                    </button>
                    <a ui-sref="course_session.manage()" class="btn btn-sm btn-default" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i><span translate="Global.Back" ></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="form-group" ng-show="course_session.session_status_id===2 || course_session.session_status_id===3 || course_session.session_status_id===5" >
            <label for="edit_coming_status" ><span translate="CourseSession.EditPresence" ></span></label>
            <label class="i-switch m-t-xs m-r">
              <input type="checkbox" ng-change="switch_edit_coming_status()" id="edit_coming_status" ng-change="" ng-model="edit_coming_status" >
              <i></i>
            </label>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>

        
        <div class="wrapper-edit-coming_status" ng-show="edit_coming_status" >
            <div class="liste-students-course-session">
                <ul class="list-unstyled row">
                    <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item_student item_student_coming_status_id_{{student_session.coming_status_id}}" ng-repeat="student_session in students | filter: InitStudentPresence" ng-click_="change_student_presence(this)" data-student-id="{{student_session.student_id}}" data-coming_status_id="{{student_session.coming_status_id}}" data-exit_status_id="{{student_session.exit_status_id}}">
                        <div class="box-student-presence">
                            <h4 class="font-bold">{{student_session.fullname}}</h4>
                            <!--<h5 class="font-bold">coming_status_id: {{student_session.coming_status_id}}</h5>
                            <h5 class="font-bold">coming_time: {{student_session.coming_time}}</h5>-->
                            <div class="icons_presence_student">
                                <!--<span class="icon_status_not_called_yet" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_Not_called_yet !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_Not_called_yet !!})">Not_called_yet</span>-->
                                <span uib-tooltip="{{ 'AttendanceStatus.OnTime' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_on_time" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_On_time !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_On_time !!})">On_time</span>
                                <span uib-tooltip="{{ 'AttendanceStatus.OnLate' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_on_late" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_On_late !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_On_late !!})">On_late</span>
                                <span uib-tooltip="{{ 'AttendanceStatus.WaitingHeCome' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_waiting_he_come" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_Waiting_he_come !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_Waiting_he_come !!})">Waiting_he_come</span>
                                <span uib-tooltip="{{ 'AttendanceStatus.Absent' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" class="icon_presence_student icon_status_absent" ng-class="{'active': student_session.coming_status_id=={!! Attendance_status_Absent !!}}" ng-click="set_coming_status_id(this,{!! Attendance_status_Absent !!})">Absent</span>
                                <!--<span class="icon_status_early_quitted" ng-class="{'active': student_session.exit_status_id=={!! Attendance_status_Early_quitted !!}}" ng-click="set_exit_status_id({!! Attendance_status_Early_quitted !!})">Early_quitted</span>-->
                            </div>

                            <div class="form-group text-center" >
                                <div class="input-append form-group-uib-timepicker">
                                    <i class="fa fa-clock-o"></i>
                                    <uib-timepicker ng-disabled="student_session.coming_status_id!={!! Attendance_status_On_late !!}" ng-click="update_student_coming_time(this)" min="uitime_global_coming_time" ng-model="student_session.timepicker" minute-step="mstep" hour-step="hstep" show-meridian="false"></uib-timepicker>
                                </div>
                            </div>
                            <button ng-class="{'btn-primary': student_session.comments}" class="btn btn-default btn-icon btn-icon-no-margin btn-min btn-comment-course-session" ng-click="add_comment(student_session)" ><i class="fa fa-commenting" aria-hidden="true"></i></button>
                        </div>
                    </li>
                </ul>
            </div>
            <div ng-show="empty_students" class="alert alert-warning">{{ 'CourseSession.NoStudentsForThisCourseSession' | translate }}</div>

            <div ng-show="students" class="btns_submit_open_session zindex999" uib-tooltip="{{ 'CourseSession.PleaseEnterAllStudentCommingStatus' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!can_submit_save_edit" tooltip-class="tooltip-danger zindex9999" >
                <a ng-click="submit_save_edit()" class="btn btn-primary"  ng-class="{'btn-disabled': !can_submit_save_edit}" ng-disabled="!can_submit_save_edit" ><i class="fa fa-floppy-o"></i><span>{{ 'Form.SaveData' | translate }}</span></a>
                <a ng-click="edit_coming_status=false" class="btn btn-default"  ><span>{{ 'Form.Back' | translate }}</span></a>
            </div>
        </div>
    </div>
</div>







<script type="text/ng-template" id="ModalCommentStudentSession.html" >
    <div class="modal-header">
        <h3 class="modal-title" translate="Panel.AddComment" ></h3></div>
        <div class="modal-body">
            <div class="modal-body-form form-group" >
                <label for="comments">{{ 'Field.Comment' | translate }} :</label>
                <textarea  ng-model="student_session.comments" class="form-control" id="comments" ></textarea>
            </div>
            <div class="alert alert-success" ng-show="success" >{{ 'CourseSession.SuccessUpdateStudentSession' | translate }}</div>
        </div>
    <div class="modal-footer">
        <button class="btn btn-primary" ng-click="submit()">{{ 'Form.Update' | translate }}</button>
        <button class="btn btn-danger" ng-click="close()">{{ 'Form.Cancel' | translate }}</button>
    </div>
</script>




<script type="text/ng-template" id="ModalCancelCourseSession.html" >
    <div class="modal-header">
        <h3 class="modal-title" translate="CourseSession.CancelCourseSession" ></h3></div>
        <div class="modal-body">
            <div class="modal-body-form form-group" >
                <label for="session_status_comment">{{ 'Form.YouCanAddAComment' | translate }} :</label>
                <textarea  ng-model="course_session.session_status_comment" class="form-control" id="session_status_comment" ></textarea>
            </div>
            <div class="alert alert-success" ng-show="success" >{{ 'CourseSession.SuccessUpdateStudentSession' | translate }}</div>
        </div>
    <div class="modal-footer">
        <button class="btn btn-danger" ng-click="submit()"><span translate="CourseSession.CancelCourseSession" ></span></button>
        <button class="btn btn-default" ng-click="close()">{{ 'Form.Cancel' | translate }}</button>
    </div>
</script>


<script type="text/ng-template" id="ModalChangeTeacherCourseSession.html" >
    <div class="modal-header">
        <h3 class="modal-title" ng-show="change_per==='item'" translate="CourseSession.ChangingTeacherForThisCourseSession" ></h3>
        <h3 class="modal-title" ng-show="change_per==='items'" translate="CourseSession.ChangingTeacherForThisCourseSessionStartingFromCertainDate" ></h3>
    </div>
    <div class="modal-body">
        <form name="form" >
            <div class="modal-body-form form-group" >
                <label for="old_teacher" ><span translate="CourseSession.CurrentTeacher" ></span></label>
                <input class="form-control" ng-disabled="true" value="{{course_session.prof_firstname + ' ' +course_session.prof_f_firstname+' '+course_session.prof_lastname}}" />
            </div>
            <div class="form-group"  >
                <label for="teacher_name_or_idn" ><span translate="CourseSession.SubstituteTeacher" ></span></label>
                <input type="text" ng-model="teacher" uib-typeahead="teacher as teacher.fullname for teacher in getTeachers($viewValue)" typeahead-loading="loadingTeachers" typeahead-no-results="noTeachers" class="form-control" placeholder="{{ 'Form.EnterTeacherIdnOrNameOrMobileOrSchoolJob' | translate }}" >
                <i ng-show="loadingTeachers" class="glyphicon glyphicon-refresh"></i>
                <div ng-show="noTeachers">
                  <i class="glyphicon glyphicon-remove"></i> <span translate="Global.NoResultsFound" ></span>
                </div>
                <div class="form-group" ng-show="teacher.id" >
                    <pre>{{teacher.fullname}} [IDN: {{teacher.idn}}]</pre>
                </div>
            </div>
            <div class="modal-body-form form-group" ng-show="change_per==='items'" >
              <label for="start_hdate" ><span translate="Field.FromDate" ></span></label>
              <islamic-datepicker id="start_hdate" ng-model="start_hdate" ng-model-hdate="start_hdate" />
            </div>
            <div class="modal-body-form form-group" >
              <label for="session_status_comment" ><span translate="Field.Comment" ></span></label>
              <textarea ng-model="session_status_comment" class="form-control" placeholder="{{ 'Form.YouCanAddAComment' | translate}}" ></textarea>
            </div>
            <div class="alert alert-success" ng-show="success" >{{ 'CourseSession.TeacherChangedWithSuccess' | translate }}</div>
        </form>

        <div class="alert alert-danger" ng-show="errors" >
            <ul>
                <li ng-repeat="error in errors" ><span translate="{{error}}" ></span></li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" ng-disabled="!teacher.id" ng-click="submit()"><span translate="CourseSession.ChangeTeacher" ></span></button>
        <button class="btn btn-default" ng-click="close()">{{ 'Form.Close' | translate }}</button>
    </div>
</script>


