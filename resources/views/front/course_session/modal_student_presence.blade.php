<div class="modal-header">
    <h3 class="modal-title">Presence student {{student.fullname}}</h3></div>
<div class="modal-body">
    <div class="form-group">
        <label for="coming_status_id">coming_status_id:</label>
        <div class="icons_presence_student">
            <span class="icon_status_on_time" ng-class="{'active': student.coming_status_id=={!! Attendance_status_On_time !!}}" ng-click="set_coming_status_id({!! Attendance_status_On_time !!})">On_time</span>
            <span class="icon_status_waiting_he_come" ng-class="{'active': student.coming_status_id=={!! Attendance_status_Waiting_he_come !!}}" ng-click="set_coming_status_id({!! Attendance_status_Waiting_he_come !!})">Waiting_he_come</span>
            <span class="icon_status_on_late" ng-class="{'active': student.coming_status_id=={!! Attendance_status_On_late !!}}" ng-click="set_coming_status_id({!! Attendance_status_On_late !!})">On_late</span>
            <span class="icon_status_absent" ng-class="{'active': student.coming_status_id=={!! Attendance_status_Absent !!}}" ng-click="set_coming_status_id({!! Attendance_status_Absent !!})">Absent</span>
            <span class="icon_status_not_called_yet" ng-class="{'active': student.coming_status_id=={!! Attendance_status_Not_called_yet !!}}" ng-click="set_coming_status_id({!! Attendance_status_Not_called_yet !!})">Not_called_yet</span>
        </div>
        <!--<select ui-jq="chosen" ng-model="student.coming_status_id" id="coming_status_id" class="w-full">
                    <option value="{!! Attendance_status_On_time !!}">On_time</option>
                    <option value="{!! Attendance_status_Waiting_he_come !!}">Waiting_he_come</option>
                    <option value="{!! Attendance_status_On_late !!}">On_late</option>
                    <option value="{!! Attendance_status_Absent !!}">Absent</option>
                    <option value="{!! Attendance_status_Not_called_yet !!}">Not_called_yet</option>
            </select>-->
    </div>
    <div class="form-group" ng-hide="student.coming_status_id=={!! Attendance_status_Absent !!}">
        <label for="exit_status_id">exit_status_id:</label>
        <div class="icons_presence_student">
            <span class="icon_status_good_quitted" ng-class="{'active': student.exit_status_id=={!! Attendance_status_Good_quitted !!}}" ng-click="set_exit_status_id({!! Attendance_status_Good_quitted !!})">Good_quitted</span>
            <span class="icon_status_early_quitted" ng-class="{'active': student.exit_status_id=={!! Attendance_status_Early_quitted !!}}" ng-click="set_exit_status_id({!! Attendance_status_Early_quitted !!})">Early_quitted</span>
        </div>
        <!--<select ui-jq="chosen" ng-model="student.exit_status_id" id="exit_status_id" class="w-full">
                    <option value="{!! Attendance_status_Early_quitted !!}">Early_quitted</option>
                    <option value="{!! Attendance_status_Good_quitted !!}">Good_quitted</option>
            </select>-->
    </div>
    <div class="form-group" ng-hide="student.coming_status_id=={!! Attendance_status_Absent !!}">
        <label for="exit_status_id">coming_time:</label>
        <div id="coming_time" class="input-append">
            <uib-timepicker ng-change="update_coming_time()" min="uitime_coming_time_min_time" max="uitime_coming_time_max_time" ng-model="uitime_coming_time" minute-step="mstep" hour-step="hstep" show-meridian="false"></uib-timepicker>
        </div>
    </div>
    <div class="form-group" ng-hide="student.coming_status_id=={!! Attendance_status_Absent !!}">
        <label for="exit_status_id">exit_time:</label>
        <div id="exit_time" class="input-append">
            <uib-timepicker ng-change="update_exit_time()" min="uitime_exit_time_min_time" max="uitime_exit_time_max_time" ng-model="uitime_exit_time" minute-step="mstep" hour-step="hstep" show-meridian="false"></uib-timepicker>
        </div>
    </div>
    <div class="alert alert-danger" ng-show="!student.fullname">Please fill all the inputs</div>
</div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="close_modal()">Close</button>

    <button class="btn btn-primary" ng-click="save()">Save</button>
</div>