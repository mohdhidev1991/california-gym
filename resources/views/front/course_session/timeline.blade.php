<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3">{{ 'CourseSession.CourseSessionProgram' | translate }}</h1>
    </div>
    <div class="wrapper-md">
        
        <div ng-show="empty_timeline" class="alert alert-warning" ><span translate="CourseSession.NoCourseSessionProgramCurrently" ></span></div>

        <div class="" >
            <ul class="liste-timeline-course-session" >
                <li ng-repeat="item in TimelineCourseSession | filter: coursesFilterTimes" data-id="session_order_{{item.session_order}}" >
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            <h4 class="m-n h4 text-center" translate="CourseSession.SessionOrdre" translate-values="{ ordre: item.session_order }" ></h4>
                            <h5 class="m-n font-thin h5 text-center">{{item.course_name_ar}}</h5>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <span class="circle-time circle-start-time">
                                    <i class="fa fa-clock-o"></i>
                                    <span>{{item.session_start_time_ampm}}</span>
                                </span>
                                <span class="circle-difference-time">
                                    <i translate="CourseSession.NumberMinutes" translate-values="{ minutes: item.duration }" ></i>
                                </span>
                                <span class="circle-time circle-end-time">
                                    <i class="fa fa-clock-o"></i>
                                    <span>{{item.session_end_time_ampm}}</span>
                                </span>
                            </div>

                        </div>
                        <div class="panel-footer text-center footer-btns-timeline">
                            <a class="btn btn-danger" ng-click="cancel_course(this)" ><i class="fa fa-times"></i><span>{{ 'CourseSession.CancelCourseSession' | translate }}</span></a>
                            <a class="btn btn-primary" ui-sref="course_session.open({year:item.year,hmonth:item.hmonth,hday_num:item.hday_num,level_class_id:item.level_class_id,symbol:item.symbol,session_order:item.session_order})" ><i class="fa fa-folder-open"></i><span>{{ 'CourseSession.OpenCourseSession' | translate }}</span></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        
    </div>
</div>



<script type="text/ng-template" id="ModalConfirmCancelCourseSession.html" >
    <div class="modal-header">
        <h3 class="modal-title" >{{ 'CourseSession.AreYouSureToCancelThisCourse' | translate }}</h3></div>
        <div class="modal-body">
            <div class="modal-body-form form-group" ng-class="{'has-error': no_comment}" >
                <label for="comment">{{ 'CourseSession.TheReasonToCancelThisCourseSession' | translate }} :</label>
                <textarea  ng-model="comment" class="form-control" id="comment" required="true" ></textarea>
            </div>
            <div class="alert alert-danger" ng-model="alert_comment" ng-show="no_comment" >{{ 'CourseSession.PleaseEnterTheReasonToCancelThisCourseSession' | translate }}</div>
        </div>
    <div class="modal-footer">
        <button class="btn btn-default" ng-click="close_cancelmodal()">{{ 'Global.No' | translate }}</button>
        <button class="btn btn-primary" ng-click="submit_cancelmodal()">{{ 'Global.Yes' | translate }}</button>
    </div>
</script>