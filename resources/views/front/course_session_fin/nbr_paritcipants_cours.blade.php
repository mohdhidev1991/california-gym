<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.ManagePariticipantsByCoursSession" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="Global.AllPariticipantsByCoursSession" ></div>

            <div class="panel-body">
                
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <div class="input-group input-group-datepicker-no-clear-button">
                            <input ui-jq="daterangepicker" ui-options="daterangepickerOptions" is-open="popup_datepicker.opened" ng-change="GetCourseSessionFin()"  ng-model="dateRange.date" class="form-control w-md" id="daterangepicker" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open_datepicker();"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group">
                    <a class="btn btn-sm btn-default"  ng-href="{{url_export_nbr_participants_cours}}" target="_blank" ><i class="fa fa-file-excel-o" ></i> <span translate="Global.ExportTable" ></span></a>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:20px;" >
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Date" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Time_Start" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Time_End" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Course_name" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Coach_name" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Room_name" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Club_name" ></label>
                                </th>
                                <th>
                                    <label translate="Field.CoursSessionNbrParticipants" ></label>
                                </th>
                                <!--
                                <th has-permission="validate_canceled_courses" >
                                    <label translate="Global.Actions" ></label>
                                </th>
                                -->
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="course_session_fin in course_session_fins" >
                                <td>
                                    <span>{{course_session_fin.Id}}</span>
                                </td>
                                <td>
                                   <span>{{course_session_fin.Date}}</span>
                                </td>

                                <td>
                                   <span>{{course_session_fin.Heure_debut}}</span>
                                </td>

                                <td>
                                   <span>{{course_session_fin.Heure_fin}}</span>
                                </td>

                                <td>
                                   <span>{{course_session_fin.Cours}}</span>
                                </td>
                                <td>
                                   <span>{{course_session_fin.Nom_Coach}} {{course_session_fin.Prenom_Coach}}</span>
                                </td>
                                <td>
                                   <span>{{course_session_fin.Salle}}</span>
                                </td>
                                <td>
                                   <span>{{course_session_fin.Club}}</span>
                                </td>
                                <td>
                                    <span><span class="badge badge-warning">{{course_session_fin.nbr_paticipants}}</span></span>
                                </td>

                                <!--
                                <td has-permission="validate_canceled_courses">
                                    <a ng-show="course_session_fin.state != 'validerpart'" class="btn btn-xs btn-success" ng-click="validate(course_session_fin.id)" ><i class="fa fa-edit"></i><span translate="Global.Validate" ></span></a>
                                    <a ng-show="course_session_fin.state != 'validerpart'" class="btn btn-xs btn-danger" ng-click="delete(course_session_fin.id)" ><i class="fa fa-remove"></i><span translate="Global.refuse" ></span></a>
                                </td>
                                -->
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center"></div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetRoles()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<script type="text/ng-template" id="ModalCourseFinConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'CourseSessionFin.AreYouSureToDeleteThisCourseSessionFin' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>


<script type="text/ng-template" id="ModalCourseFinConfirmValidate.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'CourseSessionFin.AreYouSureToValidateThisNbr' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_validate()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_validate()" translate="Global.No" ></button>
    </div>
</script>
