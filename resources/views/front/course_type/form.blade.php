<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="CourseType.EditCourseType" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="CourseType.AddNewCourseType" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default" ng-show="failed_get_data" >
          <div class="panel-body">
              <div class="alert alert-danger" translate="Global.InvalidData" ></div>
          </div>
          <div class="panel-footer" >
                <a ui-sref="course_type.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
        </div>

        <div class="panel panel-default" ng-show="course_type.id || action==='add'" >
            <div class="panel-heading font-bold" translate="CourseType.CourseTypeInformations" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="" >
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" >
                      <label for="course_type_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="course_type_id" ng-model="course_type.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                  <label for="course_type_name" ><span translate="Field.CourseTypeName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="true" id="course_type_name" ng-model="course_type.course_type_name" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
             
              </form>


              <div class="alert alert-danger" ng-show="errors_form" >
                  <ul>
                      <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                  </ul>
              </div>

              <div class="alert alert-success" ng-show="success_form" >
                  <span translate="Form.DataSavedWithSuccess" ></span>
              </div>

            </div>

            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled=" (!form.$valid)"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Add" ></span>
                    </button>
                </span>

                <a ui-sref="setting.course_type.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
      </div>
    </div>
</div>