<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="CoursesConfigItem.CourseMaterialsSettings"></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default">
            <div class="panel-heading" translate="CoursesConfigItem.CourseMaterialsSettings" ></div>

            <header class="panel-header with-padding">
                <div class="row">
                    <div class="col-md-1">
                        <label translate="CoursesConfigItem.Template" ></label>
                    </div>
                    <div class="col-md-3">
                        <select class="select-form-control" ng-model="courses_config_template_id" ng-options="obj.id as obj.courses_config_template_name_ar for obj in courses_config_templates" ng-change="changed_courses_config_template_id()" ng-disabled="!courses_config_templates" >
                            <option value="" >-- {{ "CoursesConfigItem.SelectTemplate" | translate }} --</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <label translate="CoursesConfigItem.Level" ></label>
                    </div>
                    <div class="col-md-3">
                        <select class="select-form-control" ng-model="school_level_id" ng-options="obj.id as obj.school_level_name_ar for obj in school_levels" ng-change="changed_school_level_id()" ng-disabled="!courses_config_template_id || !courses_config_templates || !school_levels" >
                            <option value="" >-- {{ "CoursesConfigItem.SelectLevel" | translate }} --</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <label translate="CoursesConfigItem.LevelClass" ></label>
                    </div>
                    <div class="col-md-2">
                        <select class="select-form-control" ng-model="level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" ng-change="changed_level_class_id()" ng-disabled="!school_level_id || !courses_config_templates || !school_levels || !level_classes" >
                            <option value="" >-- {{ "CoursesConfigItem.SelectLevelClass" | translate }} --</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button ng-click="get_courses()" ng-disabled="!courses_config_template_id || !school_level_id || !level_class_id" ng-changed="changed_level_class_id()" class="btn btn-sm btn-primary btn-default" translate="Global.Manage" ></button>
                    </div>
                </div>
            </header>

            <form class="" name="form" >
                <div class="table-responsive" ng-show= "courses" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th>
                                    <label translate="Course.Name" ></label>
                                </th>
                                <th>
                                    <label translate="Course.SessionNb" ></label>
                                </th>
                                <th>
                                    <label translate="Course.HoursNb" ></label>
                                </th>
                                <th>
                                    <label translate="Course.Coef" ></label>
                                </th>
                                <th>
                                    <label translate="Course.Balance" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="course in courses | filter:pre_get_item_course" >
                                <td>
                                    <span>{{course.course_name_ar}}</span>
                                </td>
                                <td>
                                    <input type="number" ng-pattern="/^[0-9]{1,7}$/" class="form-control" ng-required="true" ng-model="course.session_nb" >
                                </td>
                                <td>
                                    <input type="text" class="form-control" ng-disabled="true" ng-model="course.hours_nb" >
                                </td>
                                <td>
                                    <input type="number" ng-pattern="/^[0-9]{1,7}$/" class="form-control" ng-required="true" ng-model="course.coef" >
                                </td>
                                <td>
                                    {{course.balance}}
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                    <span translate="Course.Total" ></span>
                                </td>
                                <td>
                                    <span>{{count_total_session_nb()}}</span>
                                </td>
                                <td>
                                    <span>{{count_total_hours_nb()}}</span>
                                </td>
                                <td>
                                    <span>{{count_total_coef()}}</span>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        <tfoot>
                    </table>
                </div>
            </form>


            <footer class="panel-footer" ng-show= "courses" >
                <div class="row">
                    <div class="col-md-12 text-center">

                        <div class="alert alert-success" ng-show="success_save" translate="Form.SuccessSaveData" aria-hidden="false" style="">Form.SuccessSaveData</div>
                        <div class="alert alert-danger" ng-show="error_save" translate="Form.ErrorSaveData" aria-hidden="true">Form.ErrorSaveData</div>

                        <span uib-tooltip="Form.PleaseFillAllRequiredFields" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" aria-hidden="false" class="">
                            <button ng-click="save()" ng-disabled="!form.$valid" class="btn btn-sm btn-primary btn-default" ><i class="fa fa-floppy-o" ></i><span translate="Global.SaveChanges" ></span></button>
                        </span>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>



