<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="CoursesConfigItem.ClassesSettingsLabel"></h1>
    </div>
    <div class="wrapper-md">
        <form name="form" >
            <div class="panel panel-default">
                <div class="panel-heading" translate="CoursesConfigItem.CourseSessionConfigLabel" ></div>
                <div class="table-responsive">
                    <table class="table table-striped table-striped b-t b-light">
                        <tbody>
                            <tr>
                                <td width="10%"><label translate="CoursesConfigItem.ID" ></label></td>
                                <td><label translate="CoursesConfigItem.TemplateName" ></label></td>
                                <td><label translate="CoursesConfigItem.School" ></label></td>
                            </tr>
                            <tr>
                                <td>
                                    <input class="form-control" ng-model="courses_config_template.id" ng-disabled="true" />
                                </td>
                                <td>
                                    <input ng-required="$ng" class="form-control" ng-model="courses_config_template.courses_config_template_name_ar" ng-disabled="!selected_school" />
                                    <input ng-required="$ng" class="form-control" ng-model="courses_config_template.courses_config_template_name_en" ng-disabled="!selected_school" />
                                </td>
                                <td>
                                    <select ng-required="true" class="form-control" ng-model="selected_school" ng-change="changed_selected_school()" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                                        <option value="" >-- {{ "CoursesConfigItem.SelectSchool" | translate }} --</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label translate="CoursesConfigItem.TimeCourseSession" ></label></td>
                                <td><label translate="CoursesConfigItem.ModelCourses" ></label></td>
                                <td><label translate="CoursesConfigItem.ModelStudyLevels" ></label></td>
                            </tr>
                            <tr>
                                <td>
                                    <input ng-required="true" class="form-control" ng-pattern="/^[0-9]{1,7}$/" type="number" ng-model="courses_config_template.session_duration_min" ng-disabled="!selected_school" />
                                </td>
                                <td>
                                    <select ng-required="true" class="form-control" ng-model="courses_config_template.courses_template_id" ng-options="obj.id as obj.courses_template_name_ar for obj in courses_templates" ng-disabled="!selected_school" >
                                        <option value="" >-- {{ "CoursesConfigItem.SelectCoursesTemplate" | translate }} --</option>
                                    </select>
                                </td>
                                <td>
                                    <select ng-required="true" class="form-control" ng-model="courses_config_template.levels_template_id" ng-options="obj.id as obj.levels_template_name_ar for obj in levels_templates" ng-disabled="!selected_school" >
                                        <option value="" >-- {{ "CoursesConfigItem.SelectLevelsTemplate" | translate }} --</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <span uib-tooltip="Form.PleaseFillAllRequiredFields" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" aria-hidden="false" class="">
                                <button ng-click="save()" ng-disabled="!form.$valid" class="btn btn-sm btn-primary btn-default" ><i class="fa fa-floppy-o" ></i><span translate="Global.SaveChanges" ></span></button>
                            </span>
                            <a ng-show="courses_config_template.id" ui-sref="courses_config_item.config_item()" ng-disabled="!form.$valid" class="btn btn-sm btn-primary btn-default" translate="CoursesConfigItem.CourseMaterialsSettings" ></a>
                        </div>
                    </div>
                </footer>
            </div>
        </form>
    </div>
</div>



