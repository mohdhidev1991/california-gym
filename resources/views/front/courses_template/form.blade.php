<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="CoursesTemplate.EditCoursesTemplate" translate-values="{courses_template_id: courses_template.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="CoursesTemplate.AddNewCoursesTemplate" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="CoursesTemplate.CantGetCoursesTemplate" ng-show="!courses_template.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="courses_template.id || action==='add'" >
            <div class="panel-heading font-bold" translate="CoursesTemplate.CoursesTemplateInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !courses_template.id}" >
                      <label for="courses_template_id" ><span translate="CoursesTemplate.CoursesTemplateID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="courses_template_id" ng-model="courses_template.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !courses_template.courses_template_name_ar}" >
                  <label for="courses_template_name_ar" ><span translate="CoursesTemplate.CoursesTemplateNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="courses_template_name_ar" ng-model="courses_template.courses_template_name_ar" class="form-control" placeholder="{{ 'CoursesTemplate.PleaseEnterTheCoursesTemplateNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !courses_template.courses_template_name_en}" >
                  <label for="courses_template_name_en" ><span translate="CoursesTemplate.CoursesTemplateNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="courses_template_name_en" ng-model="courses_template.courses_template_name_en" class="form-control" placeholder="{{ 'CoursesTemplate.PleaseEnterTheCoursesTemplateNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="courses" ><span translate="CoursesTemplate.Courses" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in courses" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="CheckboxObjectHelper.check_selected_option(obj.id, courses_template.courses)" ng-click="CheckboxObjectHelper.toggle_checkbox_option(obj.id, courses_template.courses)" ><i></i>{{obj.course_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="CoursesTemplate.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="CoursesTemplate.AddNewCoursesTemplate" ></span>
                    </button>
                </span>

                <a ui-sref="courses_template.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>