<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3">{{ 'CourseSession.CoursesSessionsLabel' | translate }}</h1>
    </div>
    <div class="wrapper-md">

        <div class="" >
            <ul class="liste-timeline-course-session" >
                <li ng-repeat="item in TimelineCourseSession | filter: coursesFilterTimes" data-id="session_order_{{item.session_order}}" >
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            <h4 class="m-n h4 text-center" translate="CourseSession.SessionOrdre" translate-values="{ ordre: item.session_order }" ></h4>
                            <h5 class="m-n font-thin h5 text-center">{{item.course}}</h5>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <span class="circle-time circle-start-time">
                                    <i class="fa fa-clock-o"></i>
                                    <span>{{item.session_start_time_ampm}}</span>
                                </span>
                                <span class="circle-difference-time">
                                    <i translate="CourseSession.NumberMinutes" translate-values="{ minutes: item.duration }" ></i>
                                </span>
                                <span class="circle-time circle-end-time">
                                    <i class="fa fa-clock-o"></i>
                                    <span>{{item.session_end_time_ampm}}</span>
                                </span>
                            </div>

                        </div>
                        <div class="panel-footer text-center footer-btns-timeline">
                            <a class="btn btn-danger" ng-click="cancel_course(this)" ><i class="fa fa-times"></i><span>{{ 'Global.Cancel' | translate }}</span></a>
                            <a class="btn btn-primary" ng-click="open_course(this)" ui-sref="coursesession.open({year:item.year,hmonth:item.hmonth,hday_num:item.hday_num,level_class_id:item.level_class_id,symbol:item.symbol,session_order:item.session_order})" ><i class="fa fa-folder-open"></i><span>{{ 'Global.Open' | translate }}</span></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div ng-show="empty_timeline" class="alert alert-warning" >{{empty_timeline}}</div>
    </div>
</div>
