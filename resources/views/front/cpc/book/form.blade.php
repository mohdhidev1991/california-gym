<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="CPC.EditBook" translate-values="{cpc_book_id: cpc_book.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="CPC.AddNewBook" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="CPC.CantGetBook" ng-show="error_get" ></div>


        <div class="panel panel-default" ng-show="cpc_book.id || action==='add'" >
            <div class="panel-heading font-bold" translate="CPC.BookInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" >
                      <label for="cpc_book_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="cpc_book_id" ng-model="cpc_book.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" >
                    <label for="parent_book_id" ><span translate="CPC.ParentBook" ></span></label>
                    <select id="parent_book_id" class="select-form-control" ng-model="cpc_book.parent_book_id" ng-options="obj.id as obj.book_name for obj in parent_books" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="cpc_book_name" ><span translate="Field.BookName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="cpc_book_name" ng-model="cpc_book.book_name" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="book_nb_pages" ><span translate="Field.BookNumberPages" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="" id="book_nb_pages" ng-model="cpc_book.book_nb_pages" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="expiring_hdate" ><span translate="Field.ExpiringDate" ></span></label>
                  <islamic-datepicker id="expiring_hdate" ng-model="cpc_book.expiring_hdate" ng-model-hdate="cpc_book.expiring_hdate" ng-required="true" required="" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="book_type_id" ><span translate="CPC.BookType" ></span><sup class="required" >*</sup></label>
                    <select id="book_type_id" class="select-form-control" ng-required="true" ng-model="cpc_book.book_type_id" ng-options="obj.id as obj.book_type_name_ar for obj in book_types" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="book_category_id" ><span translate="CPC.BookCategory" ></span><sup class="required" >*</sup></label>
                    <select id="book_category_id" class="select-form-control" ng-required="true" ng-model="cpc_book.book_category_id" ng-options="obj.id as obj.book_category_name_ar for obj in book_categories" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="course_id" ><span translate="Field.Course" ></span><sup class="required" >*</sup></label>
                    <select id="course_id" class="select-form-control" ng-required="true" ng-model="cpc_book.course_id" ng-options="obj.id as obj.course_name_ar for obj in courses" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="levels" ><span translate="Field.LevelClass" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in level_classes" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, cpc_book.level_classes)" ng-click="toggle_checkbox_option(obj.id, cpc_book.level_classes)" ><i></i>{{obj.level_class_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="CPC.AddNewBook" ></span>
                    </button>
                </span>
                
                <a ng-show="action==='edit'" class="btn btn-sm btn-primary" ui-sref="cpc.book_pages({book_id: cpc_book.id})" >
                    <i class="fa fa-book" ></i><span translate="CPC.ManageBookPages" ></span>
                </a>

                <a ui-sref="cpc.books()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>

      </div>
    </div>
</div>