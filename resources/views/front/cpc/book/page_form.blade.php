<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="CPC.EditBookPage" translate-values="{cpc_book_page_id: cpc_book_page.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="CPC.AddNewBookPage" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="CPC.CantGetBookPage" ng-show="error_get" ></div>

        <div class="panel panel-default" ng-show="cpc_book_page.id || action==='add'" >
            <div class="panel-heading font-bold" translate="CPC.BookPageInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !cpc_book_page.id}" >
                      <label for="cpc_book_page_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="cpc_book_page_id" ng-model="cpc_book_page.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                  <label for="book_page_num" ><span translate="Field.BookPageNumber" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="" id="book_page_num" ng-model="cpc_book_page.book_page_num" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="book_page_name" ><span translate="Field.BookPageName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="book_page_name" ng-model="cpc_book_page.book_page_name" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="book_page_content" ><span translate="Field.BookPageContent" ></span><sup class="required" >*</sup></label>
                  <textarea type="text" ng-required="true" required="" id="book_page_content" ng-model="cpc_book_page.book_page_content" class="form-control"></textarea>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="alert alert-danger" ng-show="errors_save" >
                    <ul>
                        <li ng-repeat="error in errors_save" >
                            <span>{{ error | translate }}</span>
                        </li>
                    </ul>
                </div><!-- errors_save -->
                <div class="alert alert-success" ng-show="success_save" >
                    <span translate="Form.DataSavedWithSuccess" ></span>
                </div>
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="CPC.AddNewBookPage" ></span>
                    </button>
                </span>

                <a ui-sref="cpc.book_pages({book_id: cpc_book_id})" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>

      </div>
    </div>
</div>