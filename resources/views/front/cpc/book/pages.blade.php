<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="CPC.BookPages" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="alert alert-danger" ng-show="error_get_cpc_book" >
            <span translate="Global.NoResultsFound" ></span>
        </div>
        

        <div class="panel panel-default" ng-hide="error_get_cpc_book" >
            <div class="panel-heading font-bold" translate="CPC.AllBookPages" ></div>
            <div class="panel-header panel-header with-padding">
                
                <div class="header-tools header-btns margin-bottom-15" >
                    <a class="btn btn-default" ui-sref="cpc.add_book_page({cpc_book_id: cpc_book.id, add: true})" ><i class="fa fa-plus" ></i><span translate="CPC.AddNewBookPage" ></span></a>
                    <a class="btn btn-default" ng-click="upload_book()" ><i class="fa fa-upload" ></i><span translate="CPC.UploadBook" ></span></a>
                </div>
            </div>



            <div class="panel-body" >
                <div class="alert alert-warning" ng-show="book_pages && !book_pages.length" >
                    <span translate="CPC.NoBookPagesAddedForThisBook" ></span>
                </div>

                <div class="table-responsive" ng-show="book_pages.length" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='book_page_num', th_sorting_asc: order_by==='book_page_num' && order==='DESC', th_sorting_desc: order_by==='book_page_num' && order==='ASC' }" ng-click="set_order('book_page_num', null )" >
                                    <label translate="Field.BookPageNumber" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='book_page_name', th_sorting_asc: order_by==='book_page_name' && order==='DESC', th_sorting_desc: order_by==='book_page_name' && order==='ASC' }" ng-click="set_order('book_page_name', null )" >
                                    <label translate="Field.BookName" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="book_page in book_pages" >
                                <td>
                                    <span>{{book_page.book_page_num}}</span>
                                </td>
                                <td>
                                    <span>{{book_page.book_page_name}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-primary btn-xs" ui-sref="cpc.edit_book_page({cpc_book_id: book_page.book_id,cpc_book_page_id: book_page.id})" ><i class="fa fa-edit"></i><span translate="Form.Edit" ></span></a>
                                    <a target="_blank" href="/file/book/page/{{book_page.book_page_url}}" class="btn btn-default btn-xs" ng-disabled="!book_page.book_page_url" ><i class="fa fa-search"></i><span translate="CPC.ViewBookPageImage" ></span></a>
                                    <a class="btn btn-sm btn-danger btn-xs" ng-click="delete_book_page(book_page.id)" ><i class="fa fa-remove"></i><span translate="Form.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer" ng-hide="empty_results" >
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs" >
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetBookPages()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>



<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Form.AreYouSureToDeleteThisData' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>







<script type="text/ng-template" id="ModalCpcUploadBook.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="CPC.UploadBook" ></h3>
    </div>
    <div class="modal-body">
        <div ng-show="need_confirmation" >
          <div>
            <span class="checkbox">
                <label class="i-checks">
                    <input type="checkbox" ng-click="need_confirmation=false" ><i></i> <span translate="CPC.ThisBookAlreadyHavePagesTheFileToUploadWillBeInsertedOrReplaceThePageBookImages" ></span>
                </label>
            </span>
          </div>
        </div>
        <div nv-file-drop="" uploader="uploader" filters="queueLimit" ng-hide="need_confirmation" >
            <div ng-hide="uploader.queue.length" >
                <div class="form-group">
                    <label for="select_file" translate="Form.SelectFile" ></label>
                    <input type="file" id="upload_file" nv-file-select="" uploader="uploader" >
                </div>
            </div>

            <div ng-show="uploader.queue.length" >
                <table class="table bg-white-only b-a">
                    <thead>
                      <tr>
                          <th width="50%"><span translate="FeedImport.FileName" ></span></th>
                          <th ng-show="uploader.isHTML5" ><span transalte="FeedImport.Size" ></span></th>
                          <th ng-show="uploader.isHTML5" ><span transalte="FeedImport.Progress" ></span></th>
                          <th><span translate="FeedImport.Status" ></span></th>
                          <th><span translate="Global.Actions" ></span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="item in uploader.queue">
                          <td><strong>{{ item.file.name }}</strong></td>
                          <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
                          <td ng-show="uploader.isHTML5">
                              <div class="progress progress-sm m-b-none m-t-xs">
                                  <div class="progress-bar bg-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                              </div>
                          </td>
                          <td class="text-center">
                              <span ng-show="item.isSuccess" class="text-success"><i class="glyphicon glyphicon-ok"></i></span>
                              <span ng-show="item.isCancel" class="text-warning"><i class="glyphicon glyphicon-ban-circle"></i></span>
                              <span ng-show="item.isError" class="text-danger"><i class="glyphicon glyphicon-remove"></i></span>
                          </td>
                          <td nowrap>
                              <button type="button" class="btn btn-primary btn-xs" ng-click="errors_upload = null; item.upload();" ng-disabled="item.isReady || item.isUploading || item.isSuccess" translate="Form.Upload" ></button>
                              <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.cancel()" ng-disabled="!item.isUploading" translate="Form.Cancel" ></button>
                              <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.remove()" ng-disabled="!item.isReady" translate="Form.Remove" ></button>
                          </td>
                      </tr>
                    </tbody>
                </table>
                <div>
                  <p><span><span translate="FeedImport.QueueProgress" ></span>:</p>
                  <div class="progress bg-light dker" style="">
                      <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                  </div>
                </div>
            </div>
            <div class="alert alert-danger" ng-show="errors_upload" >
                <ul>
                    <li ng-repeat="error in errors_upload" >
                        <span>{{ error | translate }}</span>
                    </li>
                </ul>
            </div><!-- errors_upload -->
            <div class="alert alert-success" ng-show="success_upload" >
              <span translate="CPC.BookUploadedAndBookPagesCreatedWithSuccess" ></span>
            </div>
        </div>
    </div>
    <div class="modal-footer" >
        <button class="btn btn-danger" ng-hide="uploader.isUploading" type="button" ng-click="cancel_upload_book()" translate="Form.Close" ></button>
        <!--<button class="btn btn-primary" ng-show="success_upload" type="button" ng-click="finish()" translate="Form.Finish" ></button>-->
    </div>
</script>
