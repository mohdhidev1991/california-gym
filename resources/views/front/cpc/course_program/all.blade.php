<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="CPC.CoursePrograms" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="CPC.AllCoursePrograms" ></div>
            <div class="panel-header panel-header with-padding">
                
                <div class="header-tools header-btns margin-bottom-15" >
                    <a class="btn btn-default" ui-sref="cpc.add_course_program({add: true})" ><i class="fa fa-plus" ></i><span translate="CPC.AddNewCourseProgram" ></span></a>
                </div>
            </div>



            <div class="panel-body">
                <div class="alert alert-danger" ng-show="empty_results" >
                    <span translate="Global.NoResultsFound" ></span>
                </div>

                <div class="table-responsive" ng-show="course_programs" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='active', th_sorting_asc: order_by==='active' && order==='DESC', th_sorting_desc: order_by==='active' && order==='ASC' }" ng-click="set_order('active', null )" >
                                    <label translate="Field.Active" ></label>
                                </th>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='id', th_sorting_asc: order_by==='id' && order==='DESC', th_sorting_desc: order_by==='id' && order==='ASC' }" ng-click="set_order('id', null )" >
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='course_program_name_ar', th_sorting_asc: order_by==='course_program_name_ar' && order==='DESC', th_sorting_desc: order_by==='course_program_name_ar' && order==='ASC' }" ng-click="set_order('course_program_name_ar', null )" >
                                    <label translate="Field.ArabicName" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='course_program_name_en', th_sorting_asc: order_by==='course_program_name_en' && order==='DESC', th_sorting_desc: order_by==='course_program_name_en' && order==='ASC' }" ng-click="set_order('course_program_name_en', null )" >
                                    <label translate="Field.EnglishName" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="course_program in course_programs" >
                                <td>
                                    <span >
                                        <i ng-show="course_program.active==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="course_program.active==='N'" class="fa fa-times text-danger text"></i>
                                    </span>
                                </td>
                                <td>
                                    <span>{{course_program.id}}</span>
                                </td>
                                <td>
                                    <span>{{course_program.course_program_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{course_program.course_program_name_en}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" ui-sref="cpc.edit_course_program({cpc_course_program_id: course_program.id})" ><i class="fa fa-edit"></i><span translate="Form.Edit" ></span></a>
                                    <a class="btn btn-xs btn-primary" ui-sref="cpc.course_program_books({cpc_course_program_id: course_program.id})" ><i class="fa fa-book"></i><span translate="CPC.CourseProgramBooks" ></span></a>
                                    <a class="btn btn-xs btn-danger" ng-click="delete_course_program(course_program.id)" ><i class="fa fa-remove"></i><span translate="Form.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer" ng-hide="empty_results" >
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetCoursePrograms()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Form.AreYouSureToDeleteThisData' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>