<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="CPC.EditCourseProgram" translate-values="{cpc_course_program_id: cpc_course_program.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="CPC.AddNewCourseProgram" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="CPC.CantGetCourseProgram" ng-show="error_get" ></div>


        <div class="panel panel-default" ng-show="cpc_course_program.id || action==='add'" >
            <div class="panel-heading font-bold" translate="CPC.CourseProgramInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" >
                      <label for="cpc_course_program_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="cpc_course_program_id" ng-model="cpc_course_program.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" >
                    <label for="levels_template_id" ><span translate="Field.LevelsTemplate" ></span></label>
                    <select id="levels_template_id" class="select-form-control" ng-model="cpc_course_program.levels_template_id" ng-options="obj.id as obj.levels_template_name_ar for obj in levels_templates" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="cpc_course_program_name_ar" ><span translate="Field.ArabicName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="cpc_course_program_name_ar" ng-model="cpc_course_program.course_program_name_ar" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="cpc_course_program_name_en" ><span translate="Field.EnglishName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="cpc_course_program_name_en" ng-model="cpc_course_program.course_program_name_en" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="alert alert-danger" ng-show="errors_save" >
                    <ul>
                        <li ng-repeat="error in errors_save" >
                            <span>{{ error | translate }}</span>
                        </li>
                    </ul>
                </div><!-- errors_save -->
                <div class="alert alert-success" ng-show="success_save" >
                    <span translate="Form.DataSavedWithSuccess" ></span>
                </div>

              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="CPC.AddNewCourseProgram" ></span>
                    </button>
                </span>

                <a ui-sref="cpc.course_programs()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>

      </div>
    </div>
</div>