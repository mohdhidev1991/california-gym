<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ><span translate="CRM.FollowUpRequest" ></span></h1>
    </div>

    <div class="wrapper-md">
        <div class="alert alert-danger" ng-show="errors" >
            <ul>
            <li ng-repeat="error in errors" >
                <span>{{ error | translate }}</span>
            </li>
            </ul>
        </div>
        <div class="alert alert-danger" ng-show="empty_results" >
            <span translate="Global.NoResultsFound" ></span>
        </div>

            <div ng-show="erequest" >
                <uib-accordion close-others="oneAtATime">

                    <div uib-accordion-group class="panel-default" is-open="general_is_open" >
                        <uib-accordion-heading>
                            <label>{{'CRM.GeneralRequestData' | translate}}</label><i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': general_is_open, 'glyphicon-chevron-right': !general_is_open}"></i>
                        </uib-accordion-heading>

                        <p>
                            <label class="font-bold" translate="CRM.RequestDate"></label>: <span>{{erequest.crm_erequest_hdate}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestTime"></label>: <span>{{erequest.crm_erequest_time}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestType"></label>: <span>{{erequest.crm_erequest_type_name_ar}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestCategory"></label>: <span>{{erequest.crm_erequest_cat_name_ar}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestLevel"></label>: <span>{{erequest.crm_erequest_level_enum| filter_name_by_id:crm_erequest_levels:'erequest_level_name_ar' | translate}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestName"></label>: <span>{{erequest.crm_erequest_name}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestDetails"></label>:<br><span>{{erequest.crm_erequest_details}}</span>
                        </p>

                        <div ng-show="erequest.attachements" >
                            <label class="font-bold" translate="File.Attachements" ></label>
                            <ul>
                                <li ng-repeat="attachement in erequest.attachements" >
                                    <a href="/file/private/{{attachement.file_name}}" target="_blank" >{{attachement.original_name}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div uib-accordion-group class="panel-default" ng-show="erequest.atable_id" is-open="settings_is_open" >
                        <uib-accordion-heading>
                            <label>{{'CRM.RequestSettings' | translate}}</label><i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': settings_is_open, 'glyphicon-chevron-right': !settings_is_open}"></i>
                        </uib-accordion-heading>
                        
                    </div>


                    <div uib-accordion-group class="panel-default" is-open="follow_ups_is_open" >
                        <uib-accordion-heading>
                            <label>{{'CRM.FollowUps' | translate}}</label>
                            <span ng-show="count_fup_not_read()" aria-hidden="false" class=""><b class="badge bg-info">{{count_fup_not_read()}}</b></span>
                            <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': follow_ups_is_open, 'glyphicon-chevron-right': !follow_ups_is_open}"></i>
                        </uib-accordion-heading>

                        <div class="alert alert-warning" ng-show="empty_follow_ups" >
                            <span translate="CRM.EmptyFollowUps" ></span>
                        </div>

                        <uib-accordion close-others="oneAtATime" ng-show="follow_ups" >

                            <div uib-accordion-group class="panel-default" ng-repeat="follow_up in follow_ups" is-open="follow_up_is_open[follow_up.id]" >
                                <uib-accordion-heading >
                                    <div ng-click="toggle_opened_follow_up(follow_up)" >
                                        <label ng-class="{'font-bold': follow_up.is_read==='N'}">{{follow_up.fup_changes}}</label>
                                        <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': follow_up_is_open[follow_up.id], 'glyphicon-chevron-right': !follow_up_is_open[follow_up.id]}"></i>
                                        <i class="fa fa-eye pull-right" role="button" ng-click="unread_follow_up(follow_up)" aria-hidden="true" ng-show="follow_up.is_read==='Y'" ></i>
                                        <i class="fa fa-eye-slash pull-right" aria-hidden="true" ng-show="follow_up.is_read==='N'" ></i>
                                        <div class="clear" ></div>
                                    </div>
                                </uib-accordion-heading>
                                <div>
                                    <p>
                                        <label class="font-bold" translate="CRM.FollowUpDate"></label>: <span>{{follow_up.fup_hdate}}</span>
                                    </p>
                                    <p>
                                        <label class="font-bold" translate="CRM.FollowUpTime"></label>: <span>{{follow_up.fup_time}}</span>
                                    </p>
                                    {{follow_up.fup_comments}}
                                </div>
                            </div>
                        </uib-accordion>

                    </div>


                </uib-accordion>
            </div>



    </div>
</div>
