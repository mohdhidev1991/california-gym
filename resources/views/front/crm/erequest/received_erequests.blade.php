<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ><span translate="CRM.AnswerCustomersRequests" ></span></h1>
    </div>

    <div class="wrapper-md">
        <div class="alert alert-danger" ng-show="errors" >
            <ul>
            <li ng-repeat="error in errors" >
                <span>{{ error | translate }}</span>
            </li>
            </ul>
        </div>
        <div class="panel panel-default" >
            <div class="panel-heading font-bold" ><span translate="CRM.Requests" ></span></div>

            <div class="panel-body">
                <div class="alert alert-danger" ng-show="empty_results" >
                    <span translate="Global.NoResultsFound" ></span>
                </div>

                <div class="alert alert-warning" ng-show="erequests.length===0" >
                    <span translate="CRM.YouDontHaveCustomersRequests" ></span>
                </div>

                <div class="table-responsive" ng-show="erequests.length>0" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th>
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th>
                                    <label translate="CRM.RequestDate" ></label>
                                </th>
                                <th>
                                    <label translate="CRM.RequestType" ></label>
                                </th>
                                <th>
                                    <label translate="CRM.RequestCategory" ></label>
                                </th>
                                <th>
                                    <label translate="CRM.RequestName" ></label>
                                </th>
                                <th>
                                    <label translate="CRM.RequestStatus" ></label>
                                </th>
                                <th>
                                    <label translate="CRM.RequestNews" ></label>
                                </th>
                                <th>
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="erequest in erequests" >
                                <td>
                                    <span>{{erequest.id}}</span>
                                </td>
                                <td>
                                    <span>{{erequest.crm_erequest_hdate}}</span>
                                </td>
                                <td>
                                    <span>{{erequest.crm_erequest_type_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{erequest.crm_erequest_cat_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{erequest.crm_erequest_name}}</span>
                                </td>
                                <td>
                                    <span>{{erequest.crm_erequest_status_name_ar}}</span>
                                </td>
                                <td>
                                    <span ng-show="erequest.count_fup_not_read" >
                                        <b class="badge bg-info">{{erequest.count_fup_not_read}}</b>
                                    </span>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-primary btn-xs" ui-sref="crm.erequest.reply_erequest({crm_erequest_id: erequest.id})" ><i class="fa fa-search"></i><span translate="CRM.FollowUp" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <footer class="panel-footer" ng-hide="empty_results" >
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" max-size="5" ng-change="GetErequests()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>



<script type="text/ng-template" id="ModalCancelCrmErequest.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'CRM.AreYouSureToCancelThisRequest' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_cancel()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_cancel()" translate="Global.No" ></button>
    </div>
</script>