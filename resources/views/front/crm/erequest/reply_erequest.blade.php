<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ><span translate="CRM.FollowUpRequest" ></span></h1>
    </div>

    <div class="wrapper-md">
        <div class="alert alert-danger" ng-show="errors" >
            <ul>
            <li ng-repeat="error in errors" >
                <span>{{ error | translate }}</span>
            </li>
            </ul>
        </div>
        <div class="alert alert-danger" ng-show="empty_results" >
            <span translate="Global.NoResultsFound" ></span>
        </div>

            <div ng-show="erequest" >
                <uib-accordion close-others="oneAtATime">

                    <div uib-accordion-group class="panel-default" is-open="general_is_open" >
                        <uib-accordion-heading>
                            <label>{{'CRM.GeneralRequestData' | translate}}</label><i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': general_is_open, 'glyphicon-chevron-right': !general_is_open}"></i>
                        </uib-accordion-heading>

                        <p>
                            <label class="font-bold" translate="CRM.RequestDate"></label>: <span>{{erequest.crm_erequest_hdate | dateformat:'YYYY/MM/D'}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestTime"></label>: <span>{{erequest.crm_erequest_time}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestType"></label>: <span>{{erequest.crm_erequest_type_name_ar}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestCategory"></label>: <span>{{erequest.crm_erequest_cat_name_ar}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestLevel"></label>: <span>{{erequest.crm_erequest_level_enum| filter_name_by_id:crm_erequest_levels:'erequest_level_name_ar' | translate}}</span>
                        </p>
                        <p ng-show="erequest.auser" >
                            <label class="font-bold" translate="CRM.RequestOwner"></label>: <span>{{erequest.auser.firstname+' '+erequest.auser.f_firstname+' '+erequest.auser.lastname}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestName"></label>: <span>{{erequest.crm_erequest_name}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestDetails"></label>:<br><span>{{erequest.crm_erequest_details}}</span>
                        </p>
                        <p>
                            <label class="font-bold" translate="CRM.RequestStatus"></label>: <span>{{erequest.crm_erequest_status_name_ar}}</span>
                        </p>

                        <div ng-show="erequest.attachements" >
                            <label class="font-bold" translate="File.Attachements" ></label>
                            <ul>
                                <li ng-repeat="attachement in erequest.attachements" >
                                    <a href="/file/private/{{attachement.file_name}}" target="_blank" >{{attachement.original_name}}</a>
                                </li>
                            </ul>
                        </div>

                        <a ng-show=" erequest.crm_erequest_status_id === 1 ||  erequest.crm_erequest_status_id === 2 ||  erequest.crm_erequest_status_id === 3 " class="btn btn-primary" ng-click="change_erequest_config()" ><i class="fa fa-exchange" aria-hidden="true"></i><span translate="CRM.RedirectTheRequest" ></span></a>
                    </div>


                    <div uib-accordion-group class="panel-default" ng-show="erequest.atable_id" is-open="settings_is_open" >
                        <uib-accordion-heading>
                            <label>{{'CRM.RequestSettings' | translate}}</label><i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': settings_is_open, 'glyphicon-chevron-right': !settings_is_open}"></i>
                        </uib-accordion-heading>
                        <div class="alert alert-warning" ng-show="!erequest.settings && erequest.crm_erequest_status_id===1" >
                            <span translate="CRM.PleaseWaitUserToCompleteTheRequestSettings" ></span>
                        </div>
                        <div ng-show="erequest.settings && erequest.atable_id===3462" >
                            <p>
                                <label class="font-bold" translate="HRM.LeaveType"></label>: <span>{{erequest.settings.hrm_leave_type_name_ar}}</span>
                            </p>
                            <p>
                                <label class="font-bold" translate="HRM.LeaveStartDate"></label>: <span>{{erequest.settings.hrm_leave_start_hdate | dateformat:'YYYY/MM/D'}}</span>
                            </p>
                            <p ng-show="erequest.settings.free_time==='Y'" >
                                <label class="font-bold" translate="HRM.LeaveStartTime"></label>: <span>{{erequest.settings.hrm_leave_start_time}}</span>
                            </p>
                            <p>
                                <label class="font-bold" translate="HRM.LeaveEndDate"></label>: <span>{{erequest.settings.hrm_leave_end_hdate | dateformat:'YYYY/MM/D'}}</span>
                            </p>
                            <p ng-show="erequest.settings.free_time==='Y'" >
                                <label class="font-bold" translate="HRM.LeaveEndTime"></label>: <span>{{erequest.settings.hrm_leave_end_time}}</span>
                            </p>
                            <p>
                                <label class="font-bold" translate="HRM.LeaveName"></label>: <span>{{erequest.settings.hrm_leave_name}}</span>
                            </p>
                            <p>
                                <label class="font-bold" translate="HRM.LeaveReason"></label>: <span>{{erequest.settings.hrm_leave_reason}}</span>
                            </p>
                            <p>
                                <label ng-hide="erequest.settings.approved===null" class="font-bold" translate="HRM.IsApproved"></label><span ng-hide="erequest.settings.approved===null" >: </span>
                                <span ng-show="erequest.settings.approved==='Y'" translate="Global.Yes" ></span>
                                <span ng-show="erequest.settings.approved==='N'" translate="Global.Yes" ></span>
                                <a ng-show="erequest.settings.approved===null" class="btn btn-primary" ng-click="confirm_approve_leave(true)" ><i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span translate="CRM.Approve" ></span></a>
                                <a ng-show="erequest.settings.approved===null" class="btn btn-danger" ng-click="confirm_approve_leave(false)" ><i class="fa fa-thumbs-o-down" aria-hidden="true"></i><span translate="CRM.Disapprove" ></span></a>
                            </p>

                        </div>
                    </div>


                    <div uib-accordion-group class="panel-default" is-open="follow_ups_is_open" >
                        <uib-accordion-heading>
                            <label>{{'CRM.FollowUps' | translate}}</label>
                            <span ng-show="count_fup_not_read()" aria-hidden="false" class=""><b class="badge bg-info">{{count_fup_not_read()}}</b></span>
                            <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': follow_ups_is_open, 'glyphicon-chevron-right': !follow_ups_is_open}"></i>
                        </uib-accordion-heading>

                        <div class="alert alert-warning" ng-show="empty_follow_ups" >
                            <span translate="CRM.EmptyFollowUps" ></span>
                        </div>

                        <div id="rows-follow_ups" ng-show="follow_ups" >
                            <uib-accordion close-others="oneAtATime" >
                                <div ng-repeat="follow_up in follow_ups" class="follow-up-item" ng-class="{'my-follow-up': rea_user.id===follow_up.owner_id, 'other-follow-up': rea_user.id!==follow_up.owner_id }" >
                                    <div uib-accordion-group class="panel-default" is-open="follow_up_is_open[follow_up.id]" >
                                        <uib-accordion-heading >
                                            <div ng-click="toggle_opened_follow_up(follow_up)" >
                                                <label ng-class="{'font-bold': follow_up.is_read==='N'}">
                                                    <span translate="CRM.FollowUpDate"></span>: <span>{{follow_up.fup_hdate | dateformat:'YYYY/MM/D'}}</span> <span>{{follow_up.fup_time}}</span> - <span>{{follow_up.fup_comments | words:5}}...</span>
                                                </label>
                                                <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': follow_up_is_open[follow_up.id], 'glyphicon-chevron-right': !follow_up_is_open[follow_up.id]}"></i>
                                                <span class="fa-stack fa-lg text-success pull-right" ng-show="follow_up.is_read==='Y' && rea_user.id!==follow_up.owner_id" >
                                                    <i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>
                                                    <i class="fa fa-check fa-inverse fa-stack-1x" style="margin-left:-3px;"></i>
                                                    <i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i>
                                                </span>
                                                <span class="fa-stack fa-lg text-gris pull-right" ng-hide="follow_up.is_read==='Y' || rea_user.id===follow_up.owner_id" ng-click="unread_follow_up(follow_up)" >
                                                    <i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>
                                                    <i class="fa fa-check fa-inverse fa-stack-1x" style="margin-left:-3px;"></i>
                                                    <i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i>
                                                </span>
                                                <div class="clear" ></div>
                                            </div>
                                        </uib-accordion-heading>
                                        <div>
                                            <div class="alert alert-warning" ng-show="follow_up.fup_changes" >
                                                <span>{{follow_up.fup_changes}}</span>
                                            </div>
                                            <p>
                                                {{follow_up.fup_comments}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </uib-accordion>
                        </div>

                        <div class="clearfix" ></div>
                        <div class="container-add-new-follow" ng-show="[1,2,3].indexOf(erequest.crm_erequest_status_id)!=-1" >
                            <div class="form-group text-left" ng-show="!add_new_follow_up" >
                                <a class="btn btn-primary btn-with-icon" ng-click="add_new_follow_up=true;" ><i class="fa fa-plus" ></i> <span translate="CRM.AddNewFollowUp" ></span></a>
                            </div>
                            <form role="form" name="form.new_follow_up" class="ng-pristine ng-valid" novalidate ng-show="add_new_follow_up" >
                                <div class="panel panel-default" >
                                    <div class="panel-heading" >
                                        <label class="font-bold" translate="CRM.AddNewFollowUp" ></label>
                                    </div>
                                    <div class="panel-body" >
                                        <div class="form-group" >
                                          <label for="now_follow_up_comment" ><span translate="CRM.FollowUpComment" ></span><sup class="required" >*</sup></label>
                                          <textarea ng-required="true" id="hrm_leave_reason" ng-model="new_follow_up.fup_comments" class="form-control"></textarea>
                                        </div>
                                        <div class="line line-dashed b-b line-lg pull-in"></div>

                                        <div class="form-group" >
                                            <label for="now_follow_up_status_id" ><span translate="CRM.RequestStatus" ></span><sup class="required" >*</sup></label>
                                            <select class="select-form-control" ng-required="true" ng-model="new_follow_up.status_id" ng-options="obj.id as obj.crm_erequest_status_name_ar for obj in new_follow_up_status" >
                                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                            </select>
                                        </div>

                                        <div class="alert alert-danger" ng-show="errors_add_new_follow_up" >
                                            <ul>
                                                <li ng-repeat="error in errors_add_new_follow_up" ><span>{{error | translate}}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel-footer" >
                                        <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.new_follow_up.$valid" tooltip-class="tooltip-danger zindex9999" >
                                            <button ng-disabled="!form.new_follow_up.$valid "
                                                type="submit" class="btn btn-sm btn-primary" ng-click="submit_reply_follow_up()"
                                                >
                                                <span translate="CRM.SaveFollowUp" ></span>
                                            </button>
                                        </span>
                                        <a class="btn btn-sm btn-danger" ng-click="add_new_follow_up=false; reset_form_new_follow_up()" >
                                            <i class="fa fa-times" ></i><span translate="Form.Cancel" ></span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>


                </uib-accordion>
            </div>



    </div>
</div>



<script type="text/ng-template" id="ModalConfirmSubmitFollowUp.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Warning" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'CRM.RequestStatusNotChangedAreYouSure' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_submit_follow_up()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>

<script type="text/ng-template" id="ModalApproveLeave.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p ng-show="is_approve" >{{ 'CRM.AreYouSureToApproveThisRequest' | translate}}</p>
        <p ng-show="is_disapprove" >{{ 'CRM.AreYouSureToDisapproveThisRequest' | translate}}</p>
        <div class="alert alert-danger" ng-show="errors" >
            <ul>
            <li ng-repeat="error in errors" >
                <span>{{ error | translate }}</span>
            </li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button ng-show="is_approve" class="btn btn-primary" type="button" ng-click="confirm_approve()" translate="Global.Yes" ></button>
        <button ng-show="is_disapprove" class="btn btn-primary" type="button" ng-click="confirm_disapprove()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>



<script type="text/ng-template" id="ModalApproveLeave.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p ng-show="is_approve" >{{ 'CRM.AreYouSureToApproveThisRequest' | translate}}</p>
        <p ng-show="is_disapprove" >{{ 'CRM.AreYouSureToDisapproveThisRequest' | translate}}</p>
        <div class="alert alert-danger" ng-show="errors" >
            <ul>
            <li ng-repeat="error in errors" >
                <span>{{ error | translate }}</span>
            </li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button ng-show="is_approve" class="btn btn-primary" type="button" ng-click="confirm_approve()" translate="Global.Yes" ></button>
        <button ng-show="is_disapprove" class="btn btn-primary" type="button" ng-click="confirm_disapprove()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>







<script type="text/ng-template" id="ModalRedirectErequest.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="CRM.RedirectTheRequest" ></h3>
    </div>
    <form role="form" name="form" class="ng-pristine ng-valid" novalidate >
        <div class="modal-body">
            <div class="form-group" ng-class="{'has-error': !erequest.crm_erequest_type_id}" >
                <label for="crm_erequest_type_id" ><span translate="CRM.RequestType" ></span><sup class="required" >*</sup></label>
                <select class="select-form-control" ng-change="GetCrmErequestCats()" ng-required="true" ng-model="erequest.crm_erequest_type_id" ng-options="obj.id as obj.crm_erequest_type_name_ar for obj in crm_erequest_types" >
                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                </select>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group" ng-class="{'has-error': !erequest.crm_erequest_cat_id}" >
                <label for="crm_erequest_cat_id" ><span translate="CRM.RequestCategory" ></span><sup class="required" >*</sup></label>
                <select class="select-form-control" ng-disabled="!erequest.crm_erequest_type_id" ng-required="true" ng-model="erequest.crm_erequest_cat_id" ng-options="obj.id as obj.crm_erequest_cat_name_ar for obj in crm_erequest_cats" >
                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                </select>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>


            <div class="form-group" >
                <label for="crm_erequest_level_enum" ><span translate="CRM.RequestLevel" ></span><sup class="required" >*</sup></label>
                <select class="select-form-control" ng-required="true" ng-model="erequest.crm_erequest_level_enum" ng-options="obj.id as obj.erequest_level_name_ar | translate for obj in crm_erequest_levels" >
                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                </select>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>


            <div class="form-group" >
              <label for="redirect_erequest_comment" ><span translate="CRM.ReasonToRedirect" ></span><sup class="required" >*</sup></label>
              <textarea ng-required="true" id="redirect_erequest_comment" ng-model="erequest.comment" class="form-control"></textarea>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>


            <div class="alert alert-danger" ng-show="errors" >
                <ul>
                <li ng-repeat="error in errors" >
                    <span>{{ error | translate }}</span>
                </li>
                </ul>
            </div>
        </div>

        <div class="modal-footer">
            <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                <button ng-disabled="!form.$valid" class="btn btn-primary" type="button" ng-click="confirm_redirect_erequest()" translate="CRM.RedirectTheRequest" ></button>
            </span>
            <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.Cancel" ></button>
        </div>
    </form>
</script>


