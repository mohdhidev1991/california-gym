<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ><span translate="CRM.RequestSettings" ></span></h1>
    </div>

    <div class="wrapper-md">


        <div class="alert alert-danger" ng-show="errors" >
            <ul>
            <li ng-repeat="error in errors" >
                <span>{{ error | translate }}</span>
            </li>
            </ul>
        </div>

        <div class="panel panel-default" ng-show="erequest" >
            <div class="panel-heading font-bold" ><span translate="CRM.RequestSettings" ></span></div>

            <div class="panel-body">
                <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                    <div class="form-group" ng-show="erequest.atable_id===3462" >
                        <label for="hrm_leave" ><span translate="HRM.LeaveType" ></span><sup class="required" >*</sup></label>
                        <select class="select-form-control" ng-change="ChangedLeaveType()" ng-required="erequest.atable_id===3462" ng-model="erequest_settings.leave_type_id" ng-options="obj.id as obj.hrm_leave_type_name_ar for obj in leave_types" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>

                    <div class="form-group" ng-show="erequest.atable_id===3462" >
                      <label for="hrm_leave_start_hdate" ><span translate="HRM.LeaveStartDate" ></span></label>
                      <islamic-datepicker id="hrm_leave_start_hdate" ng-required="erequest.atable_id===3462" ng-model="erequest_settings.hrm_leave_start_hdate" ng-model-hdate="erequest_settings.hrm_leave_start_hdate" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-show="erequest.atable_id===3462 && selected_leave_type.free_time==='Y'" >
                      <label for="ui_hrm_leave_start_time" ><span translate="HRM.LeaveStartTime" ></span></label>
                        <div uib-timepicker ng-required="erequest.atable_id===3462 && selected_leave_type.free_time==='Y'" ng-model="erequest_settings.ui_hrm_leave_start_time" hour-step="1" minute-step="15" show-meridian="ismeridian "></div>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-show="erequest.atable_id===3462" >
                      <label for="hrm_leave_end_hdate" ><span translate="HRM.LeaveEndDate" ></span></label>
                      <islamic-datepicker id="hrm_leave_end_hdate" ng-required="erequest.atable_id===3462" ng-model="erequest_settings.hrm_leave_end_hdate" ng-model-hdate="erequest_settings.hrm_leave_end_hdate" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-show="erequest.atable_id===3462 && selected_leave_type.free_time==='Y'" >
                      <label for="ui_hrm_leave_end_time" ><span translate="HRM.LeaveEndTime" ></span></label>
                        <div uib-timepicker ng-required="erequest.atable_id===3462 && selected_leave_type.free_time==='Y'" ng-model="erequest_settings.ui_hrm_leave_end_time" hour-step="1" minute-step="15" show-meridian="ismeridian "></div>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-show="erequest.atable_id===3462" >
                      <label for="hrm_leave_name" ><span translate="HRM.LeaveName" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-required="erequest.atable_id===3462" ng-max="48" id="hrm_leave_name" ng-model="erequest_settings.hrm_leave_name" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-show="erequest.atable_id===3462" >
                      <label for="hrm_leave_reason" ><span translate="HRM.LeaveReason" ></span><sup class="required" >*</sup></label>
                      <textarea ng-required="erequest.atable_id===3462" id="hrm_leave_reason" ng-model="erequest_settings.hrm_leave_reason" class="form-control"></textarea>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="alert alert-danger" ng-show="errors" >
                        <ul>
                            <li ng-repeat="error in errors" ><span>{{error | translate}}</span></li>
                        </ul>
                    </div>


                </form>

            </div>

            <div class="panel-footer" >
                <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid "
                        type="submit" class="btn btn-sm btn-primary" ng-click="submit()"
                        >
                        <span translate="Form.SubmitRequest" ></span>
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>