<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ><span translate="CRM.AddNewElectronicRequest" ></span></h1>
    </div>

    <div class="wrapper-md">

        <div class="panel panel-default" >
            <div class="panel-heading font-bold" ><span translate="CRM.GeneralRequestData" ></span></div>

            <div class="panel-body">
                <form role="form" name="form" class="ng-pristine ng-valid" novalidate>

                    <div class="form-group" ng-class="{'has-error': !erequest.owner_auser_id}" >
                        <label for="owner_auser_id" ><span translate="CRM.RequestOwner" ></span><sup class="required" >*</sup></label>
                        <select class="select-form-control" ng-disabled="true" ng-required="true" ng-model="erequest.owner_auser_id" ng-options="obj.id as obj.fullname for obj in users" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-class="{'has-error': !erequest.crm_erequest_type_id}" >
                        <label for="crm_erequest_type_id" ><span translate="CRM.RequestType" ></span><sup class="required" >*</sup></label>
                        <select class="select-form-control" ng-change="GetCrmErequestCats()" ng-disabled="!erequest.owner_auser_id" ng-required="true" ng-model="erequest.crm_erequest_type_id" ng-options="obj.id as obj.crm_erequest_type_name_ar for obj in crm_erequest_types" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-class="{'has-error': !erequest.crm_erequest_cat_id}" >
                        <label for="crm_erequest_cat_id" ><span translate="CRM.RequestCategory" ></span><sup class="required" >*</sup></label>
                        <select class="select-form-control" ng-disabled="!erequest.crm_erequest_type_id" ng-required="true" ng-model="erequest.crm_erequest_cat_id" ng-options="obj.id as obj.crm_erequest_cat_name_ar for obj in crm_erequest_cats" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                        <label for="crm_erequest_level_enum" ><span translate="CRM.RequestLevel" ></span><sup class="required" >*</sup></label>
                        <select class="select-form-control" ng-required="true" ng-model="erequest.crm_erequest_level_enum" ng-options="obj.id as obj.erequest_level_name_ar | translate for obj in crm_erequest_levels" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                      <label for="crm_erequest_name" ><span translate="CRM.RequestName" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-required="true" ng-max="48" required="" id="crm_erequest_name" ng-model="erequest.crm_erequest_name" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                      <label for="crm_erequest_details" ><span translate="CRM.RequestDetails" ></span><sup class="required" >*</sup></label>
                      <textarea ng-required="true" required="" id="crm_erequest_details" ng-model="erequest.crm_erequest_details" class="form-control"></textarea>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                        <label ><span translate="File.Attachements" ></span></label>
                        <ul class="list-attachements" ng-show="attachements" >
                            <li ng-repeat="item in attachements">
                                <label><strong>{{ item.file.name }}</strong></label>
                                <span>
                                  <button type="button" class="btn btn-danger btn-xs" ng-click="remove_attachement(item)" ><i class="fa fa-times" ></i><span translate="Form.Remove" ></span></button>
                                </span>
                            </li>
                        </ul>
                    </div>

                    <div class="form-group" >
                      <button class="btn btn-default" ng-click="upload_attachements()" ><i class="fa fa-upload" ></i><span translate="File.UploadNewAttachement" ></span></button>
                    </div>

                </form>
            </div>

            <div class="panel-footer" >
                <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-hide="erequest.crm_erequest_type_id===1" ng-disabled="!form.$valid "
                        type="submit" class="btn btn-sm btn-primary" ng-click="submit()"
                        >
                        <span translate="Form.SubmitRequest" ></span>
                    </button>
                    <button ng-show="erequest.crm_erequest_type_id===1" ng-disabled="!form.$valid "
                        type="submit" class="btn btn-sm btn-primary" ng-click="submit()"
                        >
                        <span translate="Form.Next" ></span>
                        <i class="fa fa-in-left fa-arrow-left" ></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>




<script type="text/ng-template" id="ModalUploadFileSubmitErequest.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="File.UploadNewAttachement" ></h3>
    </div>
    <div class="modal-body">
        <div nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
              <div>
                <div class="form-group">
                  <label for="upload_file" translate="FeedImport.SelectFile" ></label>
                  <input type="file" id="upload_file" nv-file-select="" uploader="uploader" multiple="">
                </div>
              </div>
              <div class="col">
                <div class="wrapper-md bg-light dk b-b">
                  <span class="pull-right m-t-xs"><span translate="FeedImport.QueueLength" ></span>: <b class="badge bg-info">{{ uploader.queue.length }}</b></span>
                  <h3 class="m-n font-thin"><span translate="FeedImport.UploadQueue" ></span></h3>      
                </div>
                <div class="">
                  <table class="table bg-white-only b-a">
                      <thead>
                          <tr>
                              <th width="50%"><span translate="FeedImport.FileName" ></span></th>
                              <th ng-show="uploader.isHTML5" ><span transalte="FeedImport.Size" ></span></th>
                              <th ng-show="uploader.isHTML5" ><span transalte="FeedImport.Progress" ></span></th>
                              <th><span translate="FeedImport.Status" ></span></th>
                              <th><span translate="Global.Actions" ></span></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="item in uploader.queue">
                              <td><strong>{{ item.file.name }}</strong></td>
                              <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
                              <td ng-show="uploader.isHTML5">
                                  <div class="progress progress-sm m-b-none m-t-xs">
                                      <div class="progress-bar bg-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                  </div>
                              </td>
                              <td class="text-center">
                                  <span ng-show="item.isSuccess" class="text-success"><i class="glyphicon glyphicon-ok"></i></span>
                                  <span ng-show="item.isCancel" class="text-warning"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                  <span ng-show="item.isError" class="text-danger"><i class="glyphicon glyphicon-remove"></i></span>
                              </td>
                              <td nowrap>
                                  <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.upload();" ng-disabled="item.isReady || item.isUploading || item.isSuccess" translate="Form.Upload" ></button>
                                  <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.cancel()" ng-disabled="!item.isUploading" translate="Form.Cancel" ></button>
                                  <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.remove()" translate="Form.Remove" ></button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  <div>
                    <div>
                      <p><span><span translate="FeedImport.QueueProgress" ></span>:</p>
                      <div class="progress bg-light dker" style="">
                          <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                      </div>
                    </div>
                    <div class="alert alert-success" ng-show="success_upload" >
                      <span translate="Form.SuccessUpload" ></span>
                    </div>
                    <div class="alert alert-danger" ng-show="errors_upload" >
                        <ul>
                        <li ng-repeat="error in errors_upload" >
                            <span>{{ error | translate }}</span>
                        </li>
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="finish_upload()" translate="Form.Finish" ></button>
        <!--<button class="btn btn-danger" type="button" ng-click="cancel_import()" translate="Form.Close" ></button>-->
    </div>
</script>

