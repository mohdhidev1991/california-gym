<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="DateSystem.ManageDateSystems" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="DateSystem.AllDateSystems" ></div>
            <div class="panel-header panel-header with-padding">
                <div class="header-tools header-btns" >
                    <a class="btn btn-default" ui-sref="date_system.add({add: true})" ><i class="fa fa-plus" ></i><span translate="DateSystem.AddNewDateSystem" ></span></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:20px;" >
                                    <label translate="DateSystem.Active" ></label>
                                </th>
                                <th style="width:20px;" >
                                    <label translate="Global.ID" ></label>
                                </th>
                                <th>
                                    <label translate="DateSystem.NameAr" ></label>
                                </th>
                                <th>
                                    <label translate="DateSystem.NameEn" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="date_system in dates_system" >
                                <td>
                                    <span >
                                        <i ng-show="date_system.active==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="date_system.active==='N'" class="fa fa-times text-danger text"></i>
                                    </span>
                                </td>
                                <td>
                                    <span>{{date_system.id}}</span>
                                </td>
                                <td>
                                    <span>{{date_system.date_system_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{date_system.date_system_name_en}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-primary" ui-sref="date_system.edit({date_system_id: date_system.id})" ><i class="fa fa-edit"></i><span translate="Global.Edit" ></span></a>
                                    <a class="btn btn-sm btn-danger" ng-click="delete_date_system(date_system.id)" ><i class="fa fa-remove"></i><span translate="Global.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetDateSystems()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'DateSystem.AreYouSureToDeleteThisDateSystem' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>