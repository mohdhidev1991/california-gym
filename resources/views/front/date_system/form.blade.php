<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="DateSystem.EditDateSystem" translate-values="{date_system_id: date_system.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="DateSystem.AddNewDateSystem" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="DateSystem.CantGetDateSystem" ng-show="!date_system.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="date_system.id || action==='add'" >
            <div class="panel-heading font-bold" translate="DateSystem.DateSystemInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !date_system.id}" >
                      <label for="date_system_id" ><span translate="DateSystem.DateSystemID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="date_system_id" ng-model="date_system.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>



                <div class="form-group" ng-class="{'has-error': !date_system.lookup_code}" >
                  <label for="lookup_code" ><span translate="DateSystem.DateSystemLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="date_system.lookup_code" class="form-control" placeholder="{{ 'DateSystem.PleaseEnterTheDateSystemLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                
                <div class="form-group" ng-class="{'has-error': !date_system.date_system_name_ar}" >
                  <label for="date_system_name_ar" ><span translate="DateSystem.DateSystemNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="date_system_name_ar" ng-model="date_system.date_system_name_ar" class="form-control" placeholder="{{ 'DateSystem.PleaseEnterTheDateSystemNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" ng-class="{'has-error': !date_system.date_system_name_en}" >
                  <label for="date_system_name_en" ><span translate="DateSystem.DateSystemNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="date_system_name_en" ng-model="date_system.date_system_name_en" class="form-control" placeholder="{{ 'DateSystem.PleaseEnterTheDateSystemNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


            
                <div class="form-group" >
                    <label for="active" ><span translate="DateSystem.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="DateSystem.AddNewDateSystem" ></span>
                    </button>
                </span>

                <a ui-sref="date_system.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>