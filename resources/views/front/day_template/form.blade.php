<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="DayTemplate.EditDayTemplate" translate-values="{day_template_id: day_template.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="DayTemplate.AddNewDayTemplate" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="DayTemplate.CantGetDayTemplate" ng-show="!day_template.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="day_template.id || action==='add'" >
            <div class="panel-heading font-bold" translate="DayTemplate.DayTemplateInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !day_template.id}" >
                      <label for="day_template_id" ><span translate="DayTemplate.DayTemplateID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="day_template_id" ng-model="day_template.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !day_template.day_template_name_ar}" >
                  <label for="day_template_name_ar" ><span translate="DayTemplate.DayTemplateNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="day_template_name_ar" ng-model="day_template.day_template_name_ar" class="form-control" placeholder="{{ 'DayTemplate.PleaseEnterTheDayTemplateNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !day_template.day_template_name_en}" >
                  <label for="day_template_name_en" ><span translate="DayTemplate.DayTemplateNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="day_template_name_en" ng-model="day_template.day_template_name_en" class="form-control" placeholder="{{ 'DayTemplate.PleaseEnterTheDayTemplateNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !day_template.school_id}" >
                    <label for="school_id" ><span translate="DayTemplate.School" ></span></label>
                    <select class="select-form-control" ng-model="day_template.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "DayTemplate.SelectSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !day_template.level_class_id}" >
                    <label for="level_class_id" ><span translate="DayTemplate.LevelClass" ></span></label>
                    <select class="select-form-control" ng-model="day_template.level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" >
                        <option value="" >-- {{ "DayTemplate.SelectLevelClass" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="wday_id" ><span translate="DayTemplate.Wday" ></span></label>
                    <select class="select-form-control" ng-model="day_template.wday_id" ng-options="obj.id as obj.wday_name_ar for obj in wdays" >
                        <option value="" >-- {{ "DayTemplate.SelectWday" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="DayTemplate.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="DayTemplate.AddNewDayTemplate" ></span>
                    </button>
                </span>

                <a ui-sref="day_template.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>