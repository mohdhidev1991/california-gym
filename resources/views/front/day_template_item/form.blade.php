<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="DayTemplateItem.EditDayTemplateItem" translate-values="{day_template_item_id: day_template_item.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="DayTemplateItem.AddNewDayTemplateItem" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="DayTemplateItem.CantGetDayTemplateItem" ng-show="!day_template_item.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="day_template_item.id || action==='add'" >
            <div class="panel-heading font-bold" translate="DayTemplateItem.DayTemplateItemInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !day_template_item.id}" >
                      <label for="day_template_item_id" ><span translate="DayTemplateItem.DayTemplateItemID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="day_template_item_id" ng-model="day_template_item.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                    <label for="maintenance_start_time" ><span translate="DayTemplateItem.SessionStartTime" ></span></label>
                    <uib-timepicker ng-model="timer_session_start_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" >
                    <label for="maintenance_start_time" ><span translate="DayTemplateItem.SessionEndTime" ></span></label>
                    <uib-timepicker ng-model="timer_session_end_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="session_order" ><span translate="DayTemplateItem.SessionOrdre" ></span><sup class="required" >*</sup></label>
                    <input type="number" ng-pattern="/^[0-9]{1,7}$/" ng-required="true" id="session_order" ng-model="day_template_item.session_order" class="form-control" placeholder="{{ 'DayTemplateItem.PleaseEnterTheSessionOrdre' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>





                <div class="form-group" ng-class="{'has-error': !day_template_item.day_template_id}" >
                    <label for="day_template_id" ><span translate="DayTemplateItem.DayTemplate" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="day_template_item.day_template_id" ng-options="obj.id as obj.day_template_name_ar for obj in day_templates" >
                        <option value="" >-- {{ "DayTemplateItem.SelectDayTemplate" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="DayTemplateItem.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="DayTemplateItem.AddNewDayTemplateItem" ></span>
                    </button>
                </span>

                <a ui-sref="day_template_item.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>