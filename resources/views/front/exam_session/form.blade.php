<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="ExamSession.EditExamSession" translate-values="{exam_session_id: exam_session.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="ExamSession.AddNewExamSession" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="ExamSession.CantGetExamSession" ng-show="!exam_session.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="exam_session.id || action==='add'" >
            <div class="panel-heading font-bold" translate="ExamSession.ExamSessionInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !exam_session.id}" >
                      <label for="exam_session_id" ><span translate="ExamSession.ExamSessionID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="exam_session_id" ng-model="exam_session.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !exam_session.exam_session_name_ar}" >
                  <label for="exam_session_name_ar" ><span translate="ExamSession.ExamSessionNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="exam_session_name_ar" ng-model="exam_session.exam_session_name_ar" class="form-control" placeholder="{{ 'ExamSession.PleaseEnterTheExamSessionNameAR' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !exam_session.exam_session_name_en}" >
                  <label for="exam_session_name_en" ><span translate="ExamSession.ExamSessionNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="exam_session_name_en" ng-model="exam_session.exam_session_name_en" class="form-control" placeholder="{{ 'ExamSession.PleaseEnterTheExamSessionNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="ExamSession.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="ExamSession.AddNewExamSession" ></span>
                    </button>
                </span>

                <a ui-sref="exam_session.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>