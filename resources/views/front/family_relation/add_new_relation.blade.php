<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="User.AddNewFamilyRelationship" ></h1>
    </div>

    <div class="wrapper-md">


        <div class="panel panel-default" >
            <div class="panel-heading font-bold" translate="User.AddNewFamilyRelationship" ></div>

            <div class="panel-body">
              
                <form role="form" name="form_search_by_id" class="ng-pristine ng-valid" novalidate>
                    <div class="form-group" >
                        <label for="student_idn" ><span translate="Field.IDN" ></span><sup class="required" >*</sup></label>
                        <div class="input-group m-b">
                            <input ng-disabled="student || add_new_student" type="number" ng-required="true" required="" id="student_idn" name="student_idn" ng-model="family_relation.student_idn" class="form-control" placeholder="{{ 'Form.EnterIdn' | translate }}" />
                            <span class="input-group-addon input-group-addon-for-btn" >
                                <button ng-disabled="!family_relation.student_idn || !form_search_by_id.student_idn.$valid || student || add_new_student" type="submit" class="btn btn-sm btn-primary" ng-click="find()"
                                    >
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    <span translate="Student.FindStudent" ></span>
                                </button>
                            </span>
                        </div>

                        <div class="alert alert-warning" ng-show="not_founded_student_idn" >
                            <span translate="FamilyRelation.CantFoundThisStudent" ></span>
                        </div>
                        <div ng-show="not_founded_student_idn" >
                            <span translate="FamilyRelation.YouNeedToAddThisStudent" ></span>
                            <button ng-click="add_new_student = true; not_founded_student_idn = false" class="btn btn-sm btn-default" ><span translate="FamilyRelation.AddStudent" ></span></button>
                        </div>
                    </div>
                </form>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <form ng-show="student" role="form" name="form_add_from_id" class="ng-pristine ng-valid" novalidate>

                    <div ng-hide="success_add_relation" >
                        <div class="form-group" >
                            <label for="student_name" ><span translate="Student.StudentName" ></span></label>
                            <span>{{student.f_firstname}} {{student.firstname}} {{student.lastname}}</span>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group" ng-class="{'has-error': !student.relship_id}" >
                            <label for="student_relship_id" ><span translate="Field.FamilyRelationship" ></span><sup class="required" >*</sup></label>
                            <select class="form-control" id="student_relship_id" ng-required="true" ng-model="student.relship_id" ng-options="obj.id as obj.relship_name_ar for obj in relships" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>


                        <div class="form-group" >
                            <label for="resp_relationship" ><span translate="Field.RelationshipDescription" ></span><sup class="required" >*</sup></label>
                            <input type="text" id="resp_relationship" ng-model="student.resp_relationship" ng-required="true" class="form-control" />
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>


                        <div class="alert alert-danger" ng-show="errors_add_relation" >
                            <ul>
                                <li ng-repeat="error in errors_add_relation" ><span translate="{{error}}" ></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="alert alert-success" ng-show="success_add_relation" >
                        <span translate="Form.DataInsertedWithSuccess" ></span>
                    </div>
                    
                    <div class="form-group" ng-show="success_add_relation" >
                        <button ng-click="reset()" class="btn btn-sm btn-default" ><span translate="User.AddAnotherFamilyRelation" ></span></button>
                    </div>
                    
                    <div ng-hide="success_add_relation"  >
                        <div class="form-group" >
                            <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!student.relship_id || !student.resp_relationship" tooltip-class="tooltip-danger zindex9999" >
                                <button ng-disabled="!student.relship_id || !student.resp_relationship" ng-click="confirm_add_relation_from_idn()" class="btn btn-sm btn-primary" ><span translate="Form.SaveData" ></span></button>
                            </span>
                            <button ng-click="reset()" class="btn btn-sm btn-default" ><span translate="Form.Reset" ></span></button>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>
                </form>



                <form ng-show="add_new_student" role="form" name="form_add_new_student" class="ng-pristine ng-valid" novalidate>

                    <div ng-hide="success_save_new_student"  >
                        <div class="form-group"  ng-class="{'has-error': !new_student.firstname}" >
                            <label for="firstname" ><span translate="Field.FirstName" ></span><sup class="required" >*</sup></label>
                            <input type="text" ng-required="true" id="firstname" ng-model="new_student.firstname" class="form-control" />
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group"  ng-class="{'has-error': !new_student.lastname}" >
                            <label for="lastname" ><span translate="Field.LastName" ></span><sup class="required" >*</sup></label>
                            <input type="text" ng-required="true" id="lastname" ng-model="new_student.lastname" class="form-control" />
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group"  ng-class="{'has-error': !new_student.f_firstname}" >
                            <label for="f_firstname" ><span translate="Field.FamilyName" ></span><sup class="required" >*</sup></label>
                            <input type="text" ng-required="true" id="f_firstname" ng-model="new_student.f_firstname" class="form-control" />
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group" ng-class="{'has-error': !new_student.idn_type_id}" >
                            <label for="idn_type_id" ><span translate="Field.IdnType" ></span><sup class="required" >*</sup></label>
                            <select class="form-control" ng-required="true" ng-model="new_student.idn_type_id" ng-options="obj.id as obj.idn_type_name_ar for obj in idn_types" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>


                        <div class="form-group" ng-class="{'has-error': !new_student.country_id}" >
                            <label for="country_id" ><span translate="Field.Country" ></span><sup class="required" >*</sup></label>
                            <select class="form-control" ng-change="GetCities()" ng-required="true" ng-model="new_student.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>


                        <div class="form-group" ng-class="{'has-error': !new_student.city_id}" >
                            <label for="city_id" ><span translate="Field.City" ></span><sup class="required" >*</sup></label>
                            <select class="form-control" ng-disabled="!new_student.country_id || !cities" ng-required="true" ng-model="new_student.city_id" ng-options="obj.id as obj.city_name_ar for obj in cities" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>


                        <div class="form-group" ng-class="{'has-error': !new_student.relship_id}" >
                            <label for="relship_id" ><span translate="Field.FamilyRelationship" ></span><sup class="required" >*</sup></label>
                            <select class="form-control" ng-required="true" ng-model="new_student.relship_id" ng-options="obj.id as obj.relship_name_ar for obj in relships" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>



                        <div class="form-group" >
                            <label for="resp_relationship" ><span translate="Field.RelationshipDescription" ></span><sup class="required" >*</sup></label>
                            <input type="text" id="resp_relationship" ng-model="new_student.resp_relationship" ng-required="true" class="form-control" />
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>


                        <div class="alert alert-danger" ng-show="errors_save_new_student" >
                            <ul>
                                <li ng-repeat="error in errors_save_new_student" ><span translate="{{error}}" ></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="alert alert-success" ng-show="success_save_new_student" >
                        <span translate="Form.DataInsertedWithSuccess" ></span>
                    </div>

                    <div class="form-group" ng-show="success_save_new_student" >
                        <button ng-click="reset()" class="btn btn-sm btn-default" ><span translate="User.AddAnotherFamilyRelation" ></span></button>
                    </div>


                    <div ng-hide="success_save_new_student" >
                        <div class="form-group" >
                            <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="form_add_new_student.$invalid" tooltip-class="tooltip-danger zindex9999" >
                                <button ng-disabled="form_add_new_student.$invalid" ng-click="save_new_student()" class="btn btn-sm btn-primary" ><span translate="Form.SaveData" ></span></button>
                            </span>
                            <button ng-click="reset()" class="btn btn-sm btn-default" ><span translate="Form.Reset" ></span></button>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>
                </form>

            </div>
            <div class="panel-footer" >
                <button ui-sref="family_relation.my_relations()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </button>
            </div>


      </div>
    </div>
</div>