<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="FamilyRelation.EditMyRelation" ></h1>
    </div>

    <div class="wrapper-md">

        <div class="alert alert-danger" ng-show="error_family_relation" >
            <span translate="FamilyRelation.CantGetFamilyRelation" ></span>
        </div>
        <div class="panel panel-default" ng-show="family_relation" >
            <div class="panel-heading font-bold" translate="FamilyRelation.EditMyRelation" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate >

                <div class="form-group" >
                    <label for="student_idn" ><span translate="Field.IDN" ></span><sup class="required" >*</sup></label>
                    <input ng-disabled="true" ng-required="true" required="" id="student_idn" name="student_idn" ng-model="family_relation.idn" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div ng-show="family_relation" >
                    <div class="form-group" >
                        <label for="student_name" ><span translate="Student.StudentName" ></span><sup class="required" >*</sup></label>
                        <input ng-disabled="true" value="{{family_relation.student_firstname}} {{family_relation.student_lastname}} {{family_relation.student_f_firstname}}" class="form-control" />
                        <span></span>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" ng-class="{'has-error': !family_relation.relship_id}" >
                        <label for="student_relship_id" ><span translate="Field.FamilyRelationship" ></span><sup class="required" >*</sup></label>
                        <select class="form-control" id="student_relship_id" ng-required="true" ng-model="family_relation.relship_id" ng-options="obj.id as obj.relship_name_ar for obj in relships" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="form-group" >
                        <label for="resp_relationship" ><span translate="Field.RelationshipDescription" ></span><sup class="required" >*</sup></label>
                        <input type="text" id="resp_relationship" ng-required="true" ng-model="family_relation.resp_relationship" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>


                    <div class="alert alert-danger" ng-show="errors_edit_relation" >
                        <ul>
                            <li ng-repeat="error in errors_edit_relation" ><span translate="{{error}}" ></span></li>
                        </ul>
                    </div>

                    <div class="alert alert-success" ng-show="success_edit_relation" >
                        <span translate="Form.SuccessUpdate" ></span>
                    </div>
                </div>

              </form>
            </div>
            <div class="panel-footer" >
                <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!family_relation.relship_id || !family_relation.resp_relationship" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!family_relation.relship_id || !family_relation.resp_relationship" ng-click="save()" class="btn btn-sm btn-primary" ><span translate="Form.SaveData" ></span></button>
                </span>
                <button ui-sref="family_relation.my_relations()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </button>
            </div>
      </div>
    </div>
</div>