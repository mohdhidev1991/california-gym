<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="User.FamilyRelationships" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="User.FamilyRelationships" ></div>
            <div class="panel-header panel-header with-padding">
                <div class="header-tools header-btns" >
                    <a class="btn btn-default" ui-sref="family_relation.add_new_relation()" ><i class="fa fa-plus" ></i><span translate="User.AddNewFamilyRelationship" ></span></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th>
                                    <label translate="Field.TypeOfRelationship" ></label>
                                </th>
                                <th>
                                    <label translate="Field.Name" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="family_relation in family_relations" >
                                <td>
                                    <span>{{family_relation.relship_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{family_relation.student_f_firstname + ' ' + family_relation.student_firstname + ' ' + family_relation.student_lastname }}</span>
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" ui-sref="family_relation.edit_my_relation({family_relation_id: family_relation.id})" ><i class="fa fa-edit"></i><span translate="Form.Edit" ></span></a>
                                    <a class="btn btn-xs btn-danger" ng-click="delete_family_relation(family_relation.id)" ><i class="fa fa-remove"></i><span translate="Form.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetFamilyRelations()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Form.AreYouSureToDeleteThisData' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>