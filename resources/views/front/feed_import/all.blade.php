<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Administration.ManageFeedImport" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="FeedImport.AllFiles" ></div>
            <div class="panel-header panel-header with-padding">
                <div class="header-tools header-btns margin-bottom-15" >
                    

                    <div class="btn-group dropdown">
                      <button class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-file-excel-o"></i><span translate="FeedImport.DownloadTemplate" ></span>
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a href="download/feed_import_template_students_parents.xlsx" ><span translate="FeedImport.ImportStudentsWithParent" ></span></a></li>
                        <li><a href="download/feed_import_template_school_employees.xlsx" ><span translate="FeedImport.ImportSchoolEmployees" ></span></a></li>
                      </ul>
                    </div>

                    <a class="btn btn-default" ng-click="import_new_file()" ><i class="fa fa-plus" ></i><span translate="FeedImport.UploadNewFile" ></span></a>

                </div>
            </div>



            <div class="panel-body">
                <div class="alert alert-danger" ng-show="empty_results" >
                    <span translate="Global.NoResultsFound" ></span>
                </div>

                <div class="table-responsive" ng-show="feed_imports" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:10%;" >
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th>
                                    <label translate="FeedImport.FileName" ></label>
                                </th>
                                <th>
                                    <label translate="FeedImport.Processed" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="feed_import in feed_imports" >
                                <td>
                                    <span>{{feed_import.id}}</span>
                                </td>
                                <td>
                                    <span>{{feed_import.feed_name}}</span>
                                </td>
                                <td>
                                    <span >
                                        <i ng-show="feed_import.processed==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="feed_import.processed!=='Y'" class="fa fa-times text-danger text"></i>
                                        <span ng-show="processed_at" >{{feed_import.processed_at}}</span>
                                    </span>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-primary btn-xs" ui-sref="feed_import.process({feed_import_id: feed_import.id})" ><i class="fa fa-cog"></i><span translate="FeedImport.Processing" ></span></a>
                                    <a class="btn btn-sm btn-default btn-xs" ng-click="log_item(feed_import)" ng-show="feed_import.log" ><i class="fa fa-exclamation-circle"></i><span translate="FeedImport.Log" ></span></a>
                                    <a class="btn btn-sm btn-danger btn-xs" ng-click="delete_feed_import(feed_import.id)" ><i class="fa fa-remove"></i><span translate="Form.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer" ng-hide="empty_results" >
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetSchools()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Form.AreYouSureToDeleteThisData' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>

<script type="text/ng-template" id="ModalLogItem.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="FeedImport.Log" ></h3>
    </div>
    <div class="modal-body">
      <ul>
        <li>
          <label class="font-bold" translate="FeedImport.Total" ></label>: <span>{{feed_import.log.total}}</span>
        </li>

        <li>
          <label class="font-bold" translate="FeedImport.Processed" ></label>: <span>{{feed_import.log.processed}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Inserted" ></label>: <span>{{feed_import.log.inserted}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Failed" ></label>: <span>{{feed_import.log.failed}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Skipped" ></label>: <span>{{feed_import.log.skipped}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Updated" ></label>: <span>{{feed_import.log.updated}}</span>
        </li>
      </ul>

      <div class="alert alert-danger" ng-show="feed_import.log.messages.length" >
        <span ng-repeat="message in feed_import.log.messages" >{{message[0]}}<br></span>
      </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.Close" ></button>
    </div>
</script>


<script type="text/ng-template" id="ModalFeedImportNewFile.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="FeedImport.UploadNewFile" ></h3>
    </div>
    <div class="modal-body">
        <div nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
              <div>
                <div class="form-group">
                  <label for="upload_file" translate="FeedImport.SelectFile" ></label>
                  <input type="file" id="upload_file" nv-file-select="" uploader="uploader" />
                </div>
              </div>
              <div class="col">
                <div class="wrapper-md bg-light dk b-b">
                  <span class="pull-right m-t-xs"><span translate="FeedImport.QueueLength" ></span>: <b class="badge bg-info">{{ uploader.queue.length }}</b></span>
                  <h3 class="m-n font-thin"><span translate="FeedImport.UploadQueue" ></span></h3>      
                </div>
                <div class="">
                  <table class="table bg-white-only b-a">
                      <thead>
                          <tr>
                              <th width="50%"><span translate="FeedImport.FileName" ></span></th>
                              <th ng-show="uploader.isHTML5" ><span transalte="FeedImport.Size" ></span></th>
                              <th ng-show="uploader.isHTML5" ><span transalte="FeedImport.Progress" ></span></th>
                              <th><span translate="FeedImport.Status" ></span></th>
                              <th><span translate="Global.Actions" ></span></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="item in uploader.queue">
                              <td><strong>{{ item.file.name }}</strong></td>
                              <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
                              <td ng-show="uploader.isHTML5">
                                  <div class="progress progress-sm m-b-none m-t-xs">
                                      <div class="progress-bar bg-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                  </div>
                              </td>
                              <td class="text-center">
                                  <span ng-show="item.isSuccess" class="text-success"><i class="glyphicon glyphicon-ok"></i></span>
                                  <span ng-show="item.isCancel" class="text-warning"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                  <span ng-show="item.isError" class="text-danger"><i class="glyphicon glyphicon-remove"></i></span>
                              </td>
                              <td nowrap>
                                  <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.upload();" ng-disabled="item.isReady || item.isUploading || item.isSuccess" translate="Form.Upload" ></button>
                                  <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.cancel()" ng-disabled="!item.isUploading" translate="Form.Cancel" ></button>
                                  <button type="button" class="btn btn-default btn-xs" ng-click="errors_upload = null; item.remove()" translate="Form.Remove" ></button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  <div>
                    <div>
                      <p><span><span translate="FeedImport.QueueProgress" ></span>:</p>
                      <div class="progress bg-light dker" style="">
                          <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                      </div>
                    </div>
                    <div class="alert alert-success" ng-show="success_upload" >
                      <span translate="Form.SuccessUpload" ></span>
                    </div>
                    <div class="alert alert-danger" ng-show="errors_upload" >
                        <ul>
                        <li ng-repeat="error in errors_upload" >
                            <span>{{ error | translate }}</span>
                        </li>
                        </ul>
                    </div>
                    <div ng-hide="true" >
                        <button type="button" class="btn btn-addon btn-success" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
                          <i class="fa fa-arrow-circle-o-up"></i> Upload all
                        </button>
                        <button type="button" class="btn btn-addon btn-warning" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
                          <i class="fa fa-ban"></i> Cancel all
                        </button>
                        <button type="button" class="btn btn-addon btn-danger" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
                            <i class="fa fa-trash-o"></i> Remove all
                        </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </div>
    <div class="modal-footer">
        <!--<button class="btn btn-primary" type="button" ng-click="import()" translate="Form.Import" ></button>-->
        <button class="btn btn-danger" type="button" ng-click="cancel_import()" translate="Form.Close" ></button>
    </div>
</script>