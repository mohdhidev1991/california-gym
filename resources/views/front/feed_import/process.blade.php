<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ><span translate="FeedImport.ProcessFile" translate-values="{file_name: feed_import.feed_name}" ></span></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="FeedImport.CantGetFile" ng-show="error_get" ></div>


        <div class="panel panel-default" ng-show="feed_import.id" >
            <div class="panel-heading font-bold" ><span translate="FeedImport.ProcessFile" translate-values="{file_name: feed_import.feed_name}" ></span></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>


                <div class="form-group">
                  <label for="feed_name" ><span translate="Form.FileName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-disabled="true" ng-required="true" required="" id="feed_name" ng-model="feed_import.feed_name" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !feed_import.lookup_code}" >
                    <label for="processor" ><span translate="FeedImport.ProcessorScript" ></span><sup class="required" >*</sup></label>
                    <select id="processor" ng-disabled="processing" class="select-form-control" ng-required="true" ng-model="feed_import.processor" ng-options="obj.lookup_code as obj.processor_name | translate for obj in processors" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div ng-show="feed_import.processor==='students_parent'" >
                    <div class="form-group" >
                        <label for="school_year" ><span translate="Field.SchoolYear" ></span><sup class="required" >*</sup></label>
                        <select id="school_year" ng-disabled="processing" ng-required="feed_import.processor==='students_parent'" class="select-form-control" ng-model="options.school_year_id" ng-options="obj.id as obj.school_year_name_ar for obj in school_years" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div ng-show="school_year_started && feed_import.processor==='students_parent'" >
                    <div class="form-group" >
                        <label for="school_year_started" class="i-checks m-t-xs m-r">
                          <input type="checkbox" ng-disabled="processing" id="school_year_started" ng-required="school_year_started" ng-model="options.school_year_started" >
                          <i></i>
                            <span translate="FeedImport.AreYouSureThisSchoolYearIsStarted" ></span><sup class="required" >*</sup>
                        </label>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                    <label for="overwrite" ><span translate="FeedImport.OverwriteExistingData" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" ng-disabled="processing" id="overwrite" ng-model="options.overwrite" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div ng-show="feed_import.processor==='students_parent'"  >
                    <div class="form-group" >
                        <label for="maintain_students" ><span translate="FeedImport.TryingToMaintainTheSameStudentsEachClass" ></span></label>
                        <label class="i-switch m-t-xs m-r">
                          <input type="checkbox" ng-disabled="processing" id="maintain_students" ng-model="options.maintain_students" >
                          <i></i>
                        </label>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

              </form>

                    <div class="infos-processor-feed-import" ng-show="processing" >
                        <h5>{{item+'/'+total}}: <span>{{pourcent}}</span></h5>
                        <div class="progress progress-sm progress-striped active m-t-xs m-b-none" ng-show="processing" >
                          <div class="progress-bar bg-success" data-toggle="tooltip" data-original-title="{{pourcent}}" style="width: {{pourcent}}"></div>
                        </div>
                    </div>
                    
                    <div class="clear" ng-show="messages.length" >
                        <div class="log-viewer" >
                            <span ng-repeat="message in messages track by $index" ng-bind-html="message" ></span>
                        </div>
                    </div>

                    <div class="alert alert-success" ng-show="finish_process" >
                        <span translate="FeedImport.FinishProcessing" ></span>
                    </div>

                    <div class="alert alert-danger" ng-show="errors_process" >
                        <ul>
                        <li ng-repeat="error in errors_process" >
                            <span>{{ error | translate }}</span>
                        </li>
                        </ul>
                    </div>
            </div>

            <div class="panel-footer" >

                <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid || processing "
                        type="submit" class="btn btn-sm btn-primary" ng-click="start_process()"
                        >
                        <i class="fa fa-cog" ></i><span translate="FeedImport.Processing" ></span>
                    </button>
                </span>

                <a class="btn btn-sm btn-default" ng-click="log_item(feed_import)" ng-show="feed_import.log" ><i class="fa fa-exclamation-circle"></i><span translate="FeedImport.Log" ></span></a>

                <a ui-sref="feed_import.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>




<script type="text/ng-template" id="ModalLogItemInProcess.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="FeedImport.Log" ></h3>
    </div>
    <div class="modal-body">
      <ul>
        <li>
          <label class="font-bold" translate="FeedImport.Total" ></label>: <span>{{feed_import.log.total}}</span>
        </li>

        <li>
          <label class="font-bold" translate="FeedImport.Processed" ></label>: <span>{{feed_import.log.processed}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Inserted" ></label>: <span>{{feed_import.log.inserted}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Failed" ></label>: <span>{{feed_import.log.failed}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Skipped" ></label>: <span>{{feed_import.log.skipped}}</span>
        </li>
        <li>
          <label class="font-bold" translate="FeedImport.Updated" ></label>: <span>{{feed_import.log.updated}}</span>
        </li>
      </ul>

      <div class="alert alert-danger" ng-show="feed_import.log.messages.length" >
        <span ng-repeat="message in feed_import.log.messages" >{{message[0]}}<br></span>
      </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.Close" ></button>
    </div>
</script>