<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Gremark.EditGremark" translate-values="{gremark_id: gremark.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Gremark.AddNewGremark" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Gremark.CantGetGremark" ng-show="!gremark.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="gremark.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Gremark.GremarkInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !gremark.id}" >
                      <label for="gremark_id" ><span translate="Gremark.GremarkID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="gremark_id" ng-model="gremark.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !gremark.lookup_code}" >
                  <label for="lookup_code" ><span translate="Gremark.GremarkLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="gremark.lookup_code" class="form-control" placeholder="{{ 'Gremark.PleaseEnterTheGremarkLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !gremark.gremark_name_ar}" >
                  <label for="gremark_name_ar" ><span translate="Gremark.GremarkNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="gremark_name_ar" ng-model="gremark.gremark_name_ar" class="form-control" placeholder="{{ 'Gremark.PleaseEnterTheGremarkLangNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !gremark.gremark_name_en}" >
                  <label for="gremark_name_en" ><span translate="Gremark.GremarkNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="gremark_name_en" ng-model="gremark.gremark_name_en" class="form-control" placeholder="{{ 'Gremark.PleaseEnterTheGremarkLangNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="Gremark.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Gremark.AddNewGremark" ></span>
                    </button>
                </span>

                <a ui-sref="gremark.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>