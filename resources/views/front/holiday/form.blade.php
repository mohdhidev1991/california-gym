<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Holiday.EditHoliday" translate-values="{holiday_id: holiday.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Holiday.AddNewHoliday" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Holiday.CantGetHoliday" ng-show="!holiday.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="holiday.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Holiday.HolidayInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !holiday.id}" >
                      <label for="holiday_id" ><span translate="Holiday.HolidayID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="holiday_id" ng-model="holiday.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>



                <div class="form-group" ng-class="{'has-error': !holiday.holiday_name_ar}" >
                  <label for="holiday_name_ar" ><span translate="Holiday.HolidayNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="holiday_name_ar" ng-model="holiday.holiday_name_ar" class="form-control" placeholder="{{ 'Holiday.PleaseEnterTheHolidayNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !holiday.holiday_name_en}" >
                  <label for="holiday_name_en" ><span translate="Holiday.HolidayNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="holiday_name_en" ng-model="holiday.holiday_name_en" class="form-control" placeholder="{{ 'Holiday.PleaseEnterTheHolidayNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !holiday.holiday_start_hdate}" >
                  <label for="holiday_start_hdate" ><span translate="Holiday.HolidayStartHDdate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="holiday_start_hdate" ng-model="holiday.holiday_start_hdate" class="form-control" placeholder="{{ 'Holiday.PleaseEnterTheHolidayStartHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !holiday.holiday_end_hdate}" >
                  <label for="holiday_end_hdate" ><span translate="Holiday.HolidayEndHDdate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="holiday_end_hdate" ng-model="holiday.holiday_end_hdate" class="form-control" placeholder="{{ 'Holiday.PleaseEnterTheHolidayEndHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="country_id" ><span translate="Holiday.HolidayCountry" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-model="holiday.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                        <option value="" >-- {{ "Holiday.SelectTheCountry" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="school_id" ><span translate="Holiday.HolidaySchool" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-model="holiday.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "Holiday.SelectTheSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="Holiday.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Holiday.AddNewHoliday" ></span>
                    </button>
                </span>

                <a ui-sref="holiday.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>