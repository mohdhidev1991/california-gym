<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.ManageCaliforniaGym"></h1>
    </div>
    <div class="wrapper-md">
        <div>
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <div class="col">
                    <div class="clearfix m-b">
                    </div>
                    <div class="pos-rlt">
                        <div class="fc-overlay">
                            <div class="panel bg-white b-a pos-rlt">
                                <span class="arrow"></span>
                                <div class="h4 font-thin m-b-sm">{{event.data.course.course_name}}</div>
                                <div class="line b-b b-light"></div>
                                <div ><i class="fa fa-clock-o text-muted m-r-xs"></i> <span ng-show='event.start' >{{format_date(event.start,'HH:mm')}}</span> - <span ng-show='event.end' >{{format_date(event.end,'HH:mm')}}</span></div>
                                <div ><i class="fa fa-user text-muted m-r-xs"></i> {{event.data.coach.firstname+' '+event.data.coach.lastname}}</div>
                                <div class="m-t-sm"></div>
                            </div>
                        </div>
                        <div class="calendar_wrapper"><div class="calendar" ng-model="eventSources" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>









<script type="text/ng-template" id="ModalAddHoliday.html">
    <div class="modal-header">
        <h3 ng-if="is_new" class="modal-title" translate="Holiday.AddNewHoliday" ></h3>
        <h3 ng-if="!is_new" class="modal-title" translate="Holiday.HolidayInformations" ></h3>
    </div>
    <div class="modal-body">
        <form role="form" name="form" class="" >
               <div class="form-group" ng-if="is_new" >
                    <label for="date" ><span translate="Field.Date" ></span><sup class="required" >*</sup></label>
                    <div class="input-group input-group-datepicker input-group-datepicker-no-clear-button">
                      <input type="text" class="form-control" uib-datepicker-popup="{{date_format}}" maxDate="{{maxDate}}" ng-click="holiday_date_opened=true" ng-model="holiday.date" is-open="holiday_date_opened" datepicker-options="dateOptions" current-text="{{'Global.Today'|translate}}" close-text="{{'Global.Close'|translate}}" ng-required="true" alt-input-formats="altInputFormats" />
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-default" ng-click="holiday_date_opened=true"><i class="glyphicon glyphicon-calendar"></i></button>
                      </span>
                    </div>
                </div>
                <div ng-if="is_new" class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-if="is_new" >
                    <label for="description" ><span translate="Field.Description" ></span><sup class="required" >*</sup></label>
                    <input type="text" ng-required="true" class="form-control" ng-model="holiday.description" />
                </div>

                <div class="alert alert-danger" ng-show="errors_form" >
                  <ul>
                      <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                  </ul>
                </div>

                <div class="form-group" ng-if="holiday.id" >
                    <p><label class="font-bold" translate="Field.Description" ></label>: <span>{{holiday.description}}</span></p>
                    <p><label class="font-bold" translate="Field.Date" ></label>: <span>{{format_date(holiday.date,'DD-MM-YYYY')}}</span></p>
                </div>

        </form>
    </div>
    <div class="modal-footer">
        <button ng-show="holiday.id" type="submit" class="btn btn-sm btn-danger pull-left"" ng-click="delete(holiday.id)" >
            <i class="fa fa-trash" ></i><span translate="Global.Delete" ></span>
        </button>

        <span ng-show="is_new" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
            <button ng-show="is_new" ng-disabled=" (!form.$valid)"
                type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                >
                <i class="fa fa-floppy-o" ></i><span translate="Global.Add" ></span>
            </button>
            <button ng-show="!is_new" ng-disabled=" (!form.$valid)"
                type="submit" class="btn btn-sm btn-primary" ng-click="save()"
                >
                <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
            </button>
        </span>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.Cancel" ></button>
    </div>
</script>






<script type="text/ng-template" id="ModalDeleteHoliday.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Planning.AreYouSureToDeleteThisHoliday' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>



