<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="IdnType.EditIdnType" translate-values="{idn_type_id: idn_type.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="IdnType.AddNewIdnType" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="IdnType.CantGetIdnType" ng-show="!idn_type.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="idn_type.id || action==='add'" >
            <div class="panel-heading font-bold" translate="IdnType.IdnTypeInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !idn_type.id}" >
                      <label for="idn_type_id" ><span translate="IdnType.IdnTypeID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="idn_type_id" ng-model="idn_type.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !idn_type.lookup_code}" >
                  <label for="lookup_code" ><span translate="IdnType.IdnTypeLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="idn_type.lookup_code" class="form-control" placeholder="{{ 'IdnType.PleaseEnterTheIdnTypeLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !idn_type.idn_type_name_ar}" >
                  <label for="idn_type_name_ar" ><span translate="IdnType.IdnTypeNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="idn_type_name_ar" ng-model="idn_type.idn_type_name_ar" class="form-control" placeholder="{{ 'IdnType.PleaseEnterTheIdnTypeNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !idn_type.idn_type_name_en}" >
                  <label for="idn_type_name_en" ><span translate="IdnType.IdnTypeNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="idn_type_name_en" ng-model="idn_type.idn_type_name_en" class="form-control" placeholder="{{ 'IdnType.PleaseEnterTheIdnTypeNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                

                
                <div class="form-group" ng-class="{'has-error': !idn_type.country_id}" >
                    <label for="country_id" ><span translate="IdnType.IdnTypeCountry" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="idn_type.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                        <option value="" >-- {{ "IdnType.SelectTheCountry" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="IdnType.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="IdnType.AddNewIdnType" ></span>
                    </button>
                </span>

                <a ui-sref="idn_type.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>