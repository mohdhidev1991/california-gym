<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Language.EditLanguage" translate-values="{language_id: language.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Language.AddNewLanguage" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Language.CantGetLanguage" ng-show="!language.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="language.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Language.LanguageInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !language.id}" >
                      <label for="language_id" ><span translate="Language.LanguageID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="language_id" ng-model="language.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !language.lookup_code}" >
                  <label for="lookup_code" ><span translate="Language.LanguageLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="language.lookup_code" class="form-control" placeholder="{{ 'Language.PleaseEnterTheLanguageLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !language.lang_name_ar}" >
                  <label for="lang_name_ar" ><span translate="Language.LanguageLangNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lang_name_ar" ng-model="language.lang_name_ar" class="form-control" placeholder="{{ 'Language.PleaseEnterTheLanguageLangNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !language.lang_name_en}" >
                  <label for="lang_name_en" ><span translate="Language.LanguageLangNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lang_name_en" ng-model="language.lang_name_en" class="form-control" placeholder="{{ 'Language.PleaseEnterTheLanguageLangNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="Language.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Language.AddNewLanguage" ></span>
                    </button>
                </span>

                <a ui-sref="language.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>