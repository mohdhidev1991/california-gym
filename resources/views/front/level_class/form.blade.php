<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="LevelClass.EditLevelClass" translate-values="{level_class_id: level_class.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="LevelClass.AddNewLevelClass" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="LevelClass.CantGetLevelClass" ng-show="!level_class.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="level_class.id || action==='add'" >
            <div class="panel-heading font-bold" translate="LevelClass.LevelClassInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !level_class.id}" >
                      <label for="level_class_id" ><span translate="LevelClass.LevelClassID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="level_class_id" ng-model="level_class.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !level_class.lookup_code}" >
                  <label for="lookup_code" ><span translate="LevelClass.LevelClassLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="level_class.lookup_code" class="form-control" placeholder="{{ 'LevelClass.PleaseEnterTheLevelClassLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !level_class.level_class_name_ar}" >
                  <label for="level_class_name_ar" ><span translate="LevelClass.LevelClassNameAR" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="level_class_name_ar" ng-model="level_class.level_class_name_ar" class="form-control" placeholder="{{ 'LevelClass.PleaseEnterTheLevelClassNameAR' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !level_class.level_class_name_en}" >
                  <label for="level_class_name_en" ><span translate="LevelClass.LevelClassNameEN" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="level_class_name_en" ng-model="level_class.level_class_name_en" class="form-control" placeholder="{{ 'LevelClass.PleaseEnterTheLevelClassNameEN' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                
                <div class="form-group" ng-class="{'has-error': !level_class.school_level_id}" >
                    <label for="school_level_id" ><span translate="LevelClass.SchoolLevel" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="level_class.school_level_id" ng-options="obj.id as obj.school_level_name_ar for obj in school_levels" >
                        <option value="" >-- {{ "School.SelectSchoolLevel" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="LevelClass.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="LevelClass.AddNewLevelClass" ></span>
                    </button>
                </span>

                <a ui-sref="level_class.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>