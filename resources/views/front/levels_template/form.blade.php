<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="LevelsTemplate.EditLevelsTemplate" translate-values="{levels_template_id: levels_template.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="LevelsTemplate.AddNewLevelsTemplate" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="LevelsTemplate.CantGetLevelsTemplate" ng-show="!levels_template.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="levels_template.id || action==='add'" >
            <div class="panel-heading font-bold" translate="LevelsTemplate.LevelsTemplateInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !levels_template.id}" >
                      <label for="levels_template_id" ><span translate="LevelsTemplate.LevelsTemplateID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="levels_template_id" ng-model="levels_template.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !levels_template.levels_template_name_ar}" >
                  <label for="levels_template_name_ar" ><span translate="LevelsTemplate.LevelsTemplateNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="levels_template_name_ar" ng-model="levels_template.levels_template_name_ar" class="form-control" placeholder="{{ 'LevelsTemplate.PleaseEnterTheLevelsTemplateNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !levels_template.levels_template_name_en}" >
                  <label for="levels_template_name_en" ><span translate="LevelsTemplate.LevelsTemplateNameEN" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="levels_template_name_en" ng-model="levels_template.levels_template_name_en" class="form-control" placeholder="{{ 'LevelsTemplate.PleaseEnterTheLevelsTemplateNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="LevelsTemplate.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="LevelsTemplate.AddNewLevelsTemplate" ></span>
                    </button>
                </span>

                <a ui-sref="levels_template.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>