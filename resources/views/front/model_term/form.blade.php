<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="ModelTerm.EditModelTerm" translate-values="{model_term_id: model_term.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="ModelTerm.AddNewModelTerm" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="ModelTerm.CantGetModelTerm" ng-show="!model_term.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="model_term.id || action==='add'" >
            <div class="panel-heading font-bold" translate="ModelTerm.ModelTermInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !model_term.id}" >
                      <label for="model_term_id" ><span translate="ModelTerm.ModelTermID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="model_term_id" ng-model="model_term.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>



                <div class="form-group" ng-class="{'has-error': !model_term.model_term_name_ar}" >
                  <label for="model_term_name_ar" ><span translate="ModelTerm.ModelTermNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="model_term_name_ar" ng-model="model_term.model_term_name_ar" class="form-control" placeholder="{{ 'ModelTerm.PleaseEnterTheModelTermNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !model_term.model_term_name_en}" >
                  <label for="model_term_name_en" ><span translate="ModelTerm.ModelTermNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="model_term_name_en" ng-model="model_term.model_term_name_en" class="form-control" placeholder="{{ 'ModelTerm.PleaseEnterTheModelTermNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                  <label for="start_vacancy_hdate" ><span translate="ModelTerm.StartVacancyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" id="start_vacancy_hdate" ng-model="model_term.start_vacancy_hdate" class="form-control" placeholder="{{ 'ModelTerm.PleaseEnterTheModelStartVacancyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="end_vacancy_hdate" ><span translate="ModelTerm.EndVacancyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" id="end_vacancy_hdate" ng-model="model_term.end_vacancy_hdate" class="form-control" placeholder="{{ 'ModelTerm.PleaseEnterTheModelEndVacancyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="start_study_hdate" ><span translate="ModelTerm.StartStudyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" id="start_study_hdate" ng-model="model_term.start_study_hdate" class="form-control" placeholder="{{ 'ModelTerm.PleaseEnterTheModelStartVacancyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="end_study_hdate" ><span translate="ModelTerm.EndStudyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" id="end_study_hdate" ng-model="model_term.end_study_hdate" class="form-control" placeholder="{{ 'ModelTerm.PleaseEnterTheModelEndStudyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                




                <div class="form-group" ng-class="{'has-error': !school.school_id}" >
                    <label for="school_id" ><span translate="ModelTerm.School" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="model_term.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "ModelTerm.SelectSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="ModelTerm.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="ModelTerm.AddNewModelTerm" ></span>
                    </button>
                </span>

                <a ui-sref="model_term.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>