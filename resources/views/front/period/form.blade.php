<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Period.EditPeriod" translate-values="{period_id: period.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Period.AddNewPeriod" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Period.CantGetPeriod" ng-show="!period.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="period.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Period.PeriodInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !period.id}" >
                      <label for="period_id" ><span translate="Period.PeriodID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="period_id" ng-model="period.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !period.lookup_code}" >
                  <label for="lookup_code" ><span translate="Period.PeriodLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="period.lookup_code" class="form-control" placeholder="{{ 'Period.PleaseEnterThePeriodLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !period.period_name_ar}" >
                  <label for="period_name_ar" ><span translate="Period.PeriodNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="period_name_ar" ng-model="period.period_name_ar" class="form-control" placeholder="{{ 'Period.PleaseEnterThePeriodNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !period.period_name_en}" >
                  <label for="period_name_en" ><span translate="Period.PeriodNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="period_name_en" ng-model="period.period_name_en" class="form-control" placeholder="{{ 'Period.PleaseEnterThePeriodNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="active" ><span translate="Period.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Period.AddNewPeriod" ></span>
                    </button>
                </span>

                <a ui-sref="period.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>