
<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.ExportGRH"></h1>
    </div>
    <div class="wrapper-md">
        <div class="form-group">
            <div class="row">
                
              
                <div class="col-sm-6 form-group">
                    
                    <div class="input-group input-group-datepicker-no-clear-button">
                        <input ui-jq="daterangepicker" ui-options="daterangepickerOptions" is-open="popup_datepicker.opened" ng-change="get_export()" ng-model="dateRange.date" class="form-control w-md" id="daterangepicker" />
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open_datepicker();"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                    </div>
                    
                   
                </div>
              
                <div class="col-sm-6 form-group">
                   
                   <a class="btn btn-sm btn-default"  ng-href="{{url_export_grh}}" target="_blank" ><i class="fa fa-file-excel-o" ></i> <span translate="Global.ExportTable" ></span></a>
                
                
                </div>
                

                &nbsp;&nbsp;&nbsp;
              
                <div class="row">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                            <thead>
                                <tr>
                                    <th>
                                        <label translate="Field.ID" ></label>
                                    </th>
                                    <th>
                                        <label>Salles</label>
                                    </th>
                                    <th>
                                        <label translate="Field.ClubCode" ></label>
                                    </th>
                                    <th >
                                        <label translate="Field.Day" ></label>
                                    </th>
                                    <th>
                                        <label translate="Field.Date" ></label>
                                    </th>
                                    <th>
                                        <label translate="Field.Month" ></label>
                                    </th>
                                    <th>
                                        <label>Mois de paie</label>
                                    </th>
                                    <th>
                                        <label translate="Field.Matricule" ></label>
                                    </th>
                                    <th>
                                        <label translate="Field.Coach" ></label>
                                    </th>
                                    <th>
                                        <label translate="Field.Course" ></label>
                                    </th>
                                    <th>
                                        <label translate="Field.CodeCourse" ></label>
                                    </th>
                                    <th>
                                        <label translate="Field.NbrHours" ></label>
                                    </th>
                                    <th>
                                        <label translate="Field.Remplissage" ></label>
                                    </th>
                                    <th>
                                       <label>Commentaire</label>
                                    </th>

                                    <th>
                                       <label>Participants</label>
                                    </th>

                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="obj in list_export_grh" >
                                    
                                    <td>
                                        <a href="">{{obj.ID}}</a>
                                    </td>
                                    <td>
                                        <span>{{obj.Salle}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Code_analytique_salle}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Jour}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Date}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Mois}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.MoisPaie}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Matricule}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Coach}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Cours}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Code_du_cours}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Nbr_heure}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.remplissage}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Commentaire}}</span>
                                    </td>
                                    <td>
                                        <span>{{obj.Participants}}</span>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>

                
            
            </div>

        </div>
        
        
        
                
            
    </div>
</div>