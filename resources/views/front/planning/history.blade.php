<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.PlanningHistory"></h1>
    </div>
    <div class="wrapper-md">
        <div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <div class="btn-group-dropdown" uib-dropdown >
                            <button id="club-button" type="button" class="btn btn-block" ng-class="{'btn-primary': club, 'btn-default': !club}" uib-dropdown-toggle ng-disabled="disabled">
                                <span translate="Form.SelectClub" ng-show="!club" ></span>
                                <span ng-show="club" translate="Field.Club" ></span><span ng-show="club" >: </span><span ng-show="club" >{{club.club_name}}</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="club-dropdown">
                                <li ng-repeat="obj in clubs" ng-class="{active: club.id===obj.id}" >
                                    <a ng-click="select_club(obj)" >{{obj.club_name}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group">
                        <div class="btn-group-dropdown" uib-dropdown >
                            <button id="room-button" ng-disabled="!club" type="button" class="btn btn-block" ng-class="{'btn-primary': room, 'btn-default': !room}" uib-dropdown-toggle ng-disabled="disabled" >
                                <span translate="Form.SelectRoom" ng-show="!room" ></span>
                                <span ng-show="room" translate="Field.Room" ></span><span ng-show="room" >: </span><span ng-show="room" >{{room.room_name}}</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="room-dropdown">
                                <li ng-repeat="obj in rooms" ng-class="{active: rom.id===obj.id}" >
                                    <a ng-click="select_room(obj)">{{obj.room_name}}</a>
                                </li>
                                <li>
                                    <a href ng-show="!rooms" ><span class="text-danger" translate="Club.NoRoomAddedForThisClub"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-group-calender-history" ng-show="club && room" >
                <div class="form-group" >
                    <div class="input-group">
                      <input type="text" class="form-control" uib-datepicker-popup="{{date_format}}" ng-click="open_datepicker()" ng-change="change_datepicker()" ng-model="popup_datepicker.date" is-open="popup_datepicker.opened" datepicker-options="dateOptions" ng-required="true" alt-input-formats="altInputFormats" current-text="{{'Global.Today'|translate}}" close-text="{{'Global.Close'|translate}}" />
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-default" ng-click="open_datepicker()"><i class="glyphicon glyphicon-calendar"></i></button>
                      </span>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="form-group h4 text-right" ng-show="club && room" has-permission="manage_planning" >
                <span class="label bg-primary label-cost label-cost-per-room" ><label class="font-bold" translate="Planning.CostPerRoom" ></label> <span>{{cost_per_room}}</span></span>
                <span class="label bg-primary dk label-cost label-cost-per-club" ><label class="font-bold" translate="Planning.CostPerClub" ></label> <span>{{cost_per_club}}</span></span>
            </div>
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <div class="col">
                    <div class="clearfix m-b">
                    </div>
                    <div class="pos-rlt">
                        <div class="fc-overlay">
                            <div class="panel bg-white b-a pos-rlt">
                                <span class="arrow"></span>
                                <div class="h4 font-thin m-b-sm">{{event.data.course.course_name}}</div>
                                <div class="line b-b b-light"></div>
                                <div ><i class="fa fa-clock-o text-muted m-r-xs"></i> <span ng-show='event.start' >{{format_date(event.start,'HH:mm')}}</span> - <span ng-show='event.end' >{{format_date(event.end,'HH:mm')}}</span></div>
                                <div ><i class="fa fa-user text-muted m-r-xs"></i> {{event.data.coach.firstname+' '+event.data.coach.firstname+' #'+event.data.coach.registration_number}}</div>
                                <div class="m-t-sm"></div>
                            </div>
                        </div>
                        <div class="calendar_wrapper"><div class="calendar" ng-model="eventSources" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div></div>
                    </div>
                </div>
                <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
                    <div class="wrapper">
                        <div ng-repeat="e in events" class="bg-white-only r r-2x m-b-xs wrapper-sm {{e.className[0]}}">
                            <input ng-model="e.title" class="form-control m-t-n-xs no-border no-padder no-bg">
                            <a class="pull-right text-xs text-muted" ng-click="remove($index)"><i class="fa fa-trash-o"></i></a>
                            <div class="text-xs text-muted">{{e.start | date:"MMM dd"}} - {{e.end | date:"MMM dd"}}</div>
                            <div class="text-xs text-muted">{{e.data.coach.firstname+' '+e.data.coach.lastname+' #'+e.data.coach.registration_number}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div ng-show="club && room && !empty_course_sessions" class="footer-container-calender" >
                <div class="row" >
                    <div class="col-sm-12 text-right" >
                        <button has-permission="manage_planning" class="btn btn-sm btn-primary" type="button" ng-click="duplicate_course_sessions()" ><i class="fa fa-clone" ></i> <span translate="Planning.DuplicatePlanning" ></span></button>
                        <a class="btn btn-sm btn-default" ng-href="{{url_print_pdf}}" target="_blank" ><i class="fa fa-print" ></i> <span translate="Planning.PrintPlanning" ></span></a>
                        <a class="btn btn-sm btn-default" ng-href="{{url_print_all_pdf}}" target="_blank" ><i class="fa fa-print" ></i> <span translate="Planning.PrintPlanningForAllRooms" ></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<script type="text/ng-template" id="ModalDuplicateCourseSessions.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Planning.DuplicatePlanning" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Planning.SelectAStartDateAndConfirm' | translate}}</p>
        <div class="form-group" >
            <div class="input-group input-group-datepicker-no-clear-button">
              <input type="text" class="form-control" uib-datepicker-popup="{{format}}" date-disabled="disabledDates(date,mode)" ng-click="open_datepicker()" ng-change="change_datepicker()" ng-model="popup_datepicker.date" is-open="popup_datepicker.opened" datepicker-options="dateOptions" current-text="{{'Global.Today'|translate}}" close-text="{{'Global.Close'|translate}}" ng-required="true" alt-input-formats="altInputFormats" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="open_datepicker()"><i class="glyphicon glyphicon-calendar"></i></button>
              </span>
            </div>
        </div>

        <div class="alert alert-danger" ng-show="errors_form" >
            <ul>
                <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>
