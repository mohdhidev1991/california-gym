<style>
    .hidden {
        display: none;
    }
    .block {
        display: block;
    }
    .calendar-table-home .fc-toolbar{

        display: none;
    }
    .calendar-table-home .fc-widget-header{
        display: none;
    }
</style>

<!-- fin modif 03 / 01 / 2020 -->


<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.ManagePlanning"></h1>
    </div>
    
    <div class="wrapper-md"  >
        <div>
            <div class="form-group" ng-hide="is_manage_home" >
                
                <div class="row">

                    <div class="col-sm-6 form-group">
                        <div class="btn-group-dropdown" uib-dropdown >
                            <button id="club-button" type="button" class="btn btn-block" ng-class="{'btn-primary': club, 'btn-default': !club}" uib-dropdown-toggle ng-disabled="disabled">
                                <span translate="Form.SelectClub" ng-show="!club" ></span>
                                <span ng-show="club" translate="Field.Club" ></span><span ng-show="club" >: </span><span ng-show="club" >{{club.club_name}}</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="club-dropdown">
                                <li ng-repeat="obj in clubs">
                                    <a ng-click="select_club(obj)" ng-class="{active: club.id===obj.id}" >{{obj.club_name}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 form-group">
                        <div class="btn-group-dropdown" uib-dropdown >
                            <button id="room-button" ng-disabled="!club" type="button" class="btn btn-block" ng-class="{'btn-primary': room, 'btn-default': !room}" uib-dropdown-toggle ng-disabled="disabled" >
                                <span translate="Form.SelectRoom" ng-show="!room" ></span>
                                <span ng-show="room" translate="Field.Room" ></span><span ng-show="room" >: </span><span ng-show="room" >{{room.room_name}}</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="room-dropdown">
                                <li ng-repeat="obj in rooms">
                                    <a ng-click="select_room(obj)" ng-class="{active: room.id===obj.id}" >{{obj.room_name}}</a>
                                </li>
                                <li>
                                    <a href ng-show="!rooms" ><span class="text-danger" translate="Club.NoRoomAddedForThisClub"></span></a>
                                </li>
                            </ul>

                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
        
        <div>

          <div class="row" ng-show="is_manage_home">

             <div class="col-sm-8">
             </div>
             <div class="col-sm-4 form-group">

              <div class="col-sm-8">
              <input class="form-control" type="date" name="date_planing_home" id="date_planing_home" onchange="disable_passed_date()" >
              </div>

              <div class="col-sm-4">
              <a ng-click="select_calendar_date_by_home()" class="btn btn-primary">Afficher</a>
              </div>

          </div>

          <div class="row" ng-show="display_button_valid_hebdomadaire">

            <div class="col-sm-8">
            </div>

            <div class="col-sm-4">
              <a ng-click="valid_hebdomadaire()" class="btn btn-primary">Valider le remplisssage hebdomadaire et envoyer au GRH</a>
            </div>

          
          </div>
          </div>
           <br>
           <table border="1" class="calendar-table-home" ng-show="is_manage_home" >
               <thead>
                  <tr>
                     <td ng-repeat="objRoleHome in roomsRoleHome">
                     {{objRoleHome.room_name}}
                     </td>
                  </tr>
               </thead>
               <tbody>
                <tr>
                    <td ng-repeat="objRoleHome in roomsRoleHome" >
                        <div>
                            <div class="calendar{{objRoleHome.id}}" id="calendar{{objRoleHome.id}}"
                            ng-model="objRoleHome.events"
                            ui-calendar="objRoleHome.calendarConfig" style="height:1200px;"></div>
                        </div>
                    </td>
                </tr>
               </tbody>
            </table>

            <div class="form-group h4 text-right" ng-show="club && room">
                <span class="label bg-primary label-cost label-cost-per-room" ><label class="font-bold" translate="Planning.CostPerRoom" ></label> <span>{{cost_per_room}}</span></span>
                <span class="label bg-primary dk label-cost label-cost-per-club" ><label class="font-bold" translate="Planning.CostPerClub" ></label> <span>{{cost_per_club}}</span></span>
            </div>
            <div ng-show="club && room" class="footer-container-calender">
                <div class="row" ng-show="have_course_sessions_invalide" >
                    <div class="col-sm-6" >
                        <p ng-show="display_buttton" ><i class="fa fa-info-circle alert-event text-danger" aria-hidden="true"></i> <span translate="Planning.NotValidatedCourseSession"></span></p>
                    </div>
                    <div class="col-sm-6 text-right" ng-show="display_buttton">
                         <button class="btn btn-sm btn-primary"  type="button" ng-click="validate_course_sessions()" translate="Global.Validate" ></button>
                    </div>
                </div>
            </div>
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <div class="col">
                    <div class="clearfix m-b">
                    </div>
                    <div class="pos-rlt">
                        <div class="fc-overlay">
                            <div class="panel bg-white b-a pos-rlt">
                                <span class="arrow"></span>
                                <div class="h4 font-thin m-b-sm">{{event.data.course.course_name}}</div>
                                <div class="line b-b b-light"></div>
                                <div ><i class="fa fa-clock-o text-muted m-r-xs"></i> <span ng-show='event.start' >{{format_date(event.start,'HH:mm')}}</span> - <span ng-show='event.end' >{{format_date(event.end,'HH:mm')}}</span></div>
                                <div ><i class="fa fa-user text-muted m-r-xs"></i> {{event.data.coach.firstname+' '+event.data.coach.lastname+' #'+event.data.coach.registration_number}}</div>
                                <div class="m-t-sm"></div>
                            </div>
                        </div>
                        <div class="calendar_wrapper"><div class="calendar" ng-model="eventSources" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div></div>
                    </div>
                </div>
                <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
                    <div class="wrapper">
                        <div ng-repeat="e in events" class="bg-white-only r r-2x m-b-xs wrapper-sm {{e.className[0]}}">
                            <input ng-model="e.title" class="form-control m-t-n-xs no-border no-padder no-bg">
                            <a class="pull-right text-xs text-muted" ng-click="remove($index)"><i class="fa fa-trash-o"></i></a>
                            <div class="text-xs text-muted">{{e.start | date:"MMM dd"}} - {{e.end | date:"MMM dd"}}</div>
                            <div class="text-xs text-muted">{{e.data.coach.firstname+' '+e.data.coach.lastname+' #'+e.data.coach.registration_number}}</div>
                        </div>
                    </div>
                </div>
            </div>

           
            <div ng-show="club && room && events && !empty_course_sessions" class="footer-container-calender" >
                <div class="row" >
                    <div class="col-sm-12 text-right" >
                        <button ng-show="display_buttton" class="btn btn-sm btn-primary" type="button" ng-click="duplicate_course_sessions()" ><i class="fa fa-clone" ></i> <span translate="Planning.DuplicatePlanning" ></span></button>
                        <button ng-show="display_buttton" class="btn btn-sm btn-primary" type="button" ng-click="send_by_email()" ><i class="fa fa-paper-plane" ></i> <span translate="Planning.SendByEmail" ></span></button>
                        <a ng-show="display_buttton" class="btn btn-sm btn-default" ng-href="{{url_print_pdf}}" target="_blank" ><i class="fa fa-print" ></i> <span translate="Planning.PrintPlanning" ></span></a>
                        <a ng-show="display_buttton" class="btn btn-sm btn-default" ng-href="{{url_print_all_pdf}}" target="_blank" ><i class="fa fa-print" ></i> <span translate="Planning.PrintPlanningForAllRooms" ></span></a>
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</div>


<script type="text/ng-template" id="ModalAddCourseSession.html" >
   
    <div class="modal-header" ng-show="is_manage_old_days">
        <h3 ng-show="is_new" class="modal-title" translate="Planning.AddNewCourseSession" ></h3>
        <h3 ng-show="!is_new" class="modal-title" translate="Planning.EditCourseSession" ></h3>
    </div>
    
    <div class="modal-header" ng-hide="is_manage_old_days">
        <div ng-hide="is_manage_home">
            <h3 class="modal-title" translate="Planning.not_permission_manage_old_days" ></h3>
        </div>
        <div ng-show="is_manage_home  && !is_manage_controller">
            <h3 class="modal-title" translate="Planning.title_manage_home" ></h3>
        </div>

        <div ng-show="is_manage_home  && is_manage_controller">
            <h3 class="modal-title" translate="Planning.title_manage_controlleur" ></h3>
        </div>
    
    </div>

    <div class="modal-body" ng-show="is_manage_old_days"> 

            <form role="form" name="form" class="">
                        

                        <div ng-show="display_type_cours">
                        <div class="form-group">
                            <label for="type_course" ><span translate="Field.TypeCourse" ></span><sup class="required" >*</sup></label>
                            <select class="select-form-control" ng-change="typecourse()"  ng-model="course_session.type_course" id="type_course" aria-label="ngSelected demo">
                                <option value="Cours" >Cours</option>
                                <option value="Autre" >Autre</option>
                            </select>
                        </div>
                         
                        <div class="form-group" ng-show="is_others">
                            <label for="description_course" ><span translate="Field.DescriptionCourse" ></span></label>
                            <textarea class="form-control" ng-model="course_session.description_course" id="description_course" rows="5"></textarea>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        </div>
                        
                        <div class="form-group" >
                            <label for="course_session_date" ><span translate="Field.Date" ></span><sup class="required" >*</sup></label>
                            <div class="input-group input-group-datepicker input-group-datepicker-no-clear-button">
                            <input type="text" ng-disabled="!course_session.type_course" class="form-control" ng-change="GetCoaches()" uib-datepicker-popup="{{date_format}}" maxDate="{{maxDate}}" ng-click="course_session_date_opened=true" ng-change="change_datepicker()" ng-model="course_session.date" is-open="course_session_date_opened" datepicker-options="dateOptions" current-text="{{'Global.Today'|translate}}" close-text="{{'Global.Close'|translate}}" ng-required="true" alt-input-formats="altInputFormats" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="course_session_date_opened=true"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="row" ng-show="is_others">
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label for="course_start_time" ><span translate="Field.TimeBegin" ></span><sup class="required" >*</sup></label>
                                    <uib-timepicker  ng-disabled="!course_session.type_course" ng-model="course_session.time_others_begin" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label for="course_end_time" ><span translate="Field.TimeStop" ></span><sup class="required" >*</sup></label>
                                    <uib-timepicker ng-disabled="!course_session.type_course" ng-model="course_session.time_others_stop" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false" />
                                </div>
                            </div>
                        </div>

                        
                            <div class="form-group" ng-show="is_cours">
                                <label for="course_start_time" ><span translate="Field.Time" ></span><sup class="required" >*</sup></label>
                                <uib-timepicker ng-change="GetCoaches()" ng-disabled="!course_session.type_course" ng-model="course_session.time" minute-step="5" hour-step="1" mousewheel="false" show-meridian="false" />
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in" ng-show="is_cours"></div>
                        

                       
                      
                            <div class="form-group" ng-show="is_cours">
                                <label for="club_id" ><span translate="Field.Course" ></span><sup class="required" >*</sup></label>
                                <select class="select-form-control" ng-disabled="!course_session.type_course" ng-change="GetCoaches();EmptyCoaches();"  ng-model="course_session.course_id" ng-options="obj.id as obj.course_name for obj in courses" >
                                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                </select>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            

                            <!--
                            <div class="form-group">
                                <label for="primary_coach_id" ><span translate="Field.Coach" ></span><sup class="required" >*</sup></label>
                                <select class="select-form-control" ng-disabled="!course_session.type_course" ng-change="need_force_validation = null; GetCoachesSecondary()" ng-required="true"  ng-model="course_session.primary_coach_id" ng-options="obj.id as (obj.firstname+' '+obj.lastname+' #'+obj.registration_number) for obj in coaches" >
                                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                </select>
                            </div>
                            -->
                            
                            <div class="form-group">
                                    <label for="primary_coach_id" ><span translate="Field.Coach" ></span><sup class="required" >*</sup></label>
                                    <multiple-autocomplete
                                        ng-disabled="!course_session.type_course"
                                        before-select-item="beforeSelectItem"
                                        after-select-item="afterSelectItem"
                                        before-remove-item="beforeRemoveItem"
                                        after-remove-item="afterRemoveItem"
                                        ng-model="list_coachs"
                                        object-property="name"
                                        suggestions-arr="coachs_lst">
                                    </multiple-autocomplete>
                            </div>
                           
                        

                       
                        <div class="alert alert-danger" ng-show="errors_form" >
                        <ul>
                            <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                        </ul>
                        </div>

                        <div class="alert alert-danger" ng-show="errors_max_five_coachs" >
                              <span translate="Global.max_five_coachs" ></span>
                        </div>


                        
                        

                    <div class="form-group text-center" ng-show="need_force_validation" >
                        <button type="submit" class="btn btn-sm btn-primary" ng-click="force_validation()" >
                            <i class="fa fa-floppy-o" ></i><span translate="Global.ForceRegistration" ></span>
                        </button>
                    </div>
            </form>
    </div>

    <div class="modal-footer" ng-show="is_manage_old_days">
           <button ng-show="course_session.id" type="submit" class="btn btn-sm btn-danger pull-left"" ng-click="delete(course_session.id)" >
                <i class="fa fa-trash" ></i><span translate="Global.Delete" ></span>
            </button>

            <span uib-tooltip="{{'Form.PleaseFillInAllRequiredFields'| translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                <button ng-show="is_new" ng-disabled=" (!form.$valid)"
                    type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                    >
                    <i class="fa fa-floppy-o" ></i><span translate="Global.Add" ></span>
                </button>
                <button ng-show="!is_new" ng-disabled=" (!form.$valid)"
                    type="submit" class="btn btn-sm btn-primary" ng-click="save()"
                    >
                    <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                </button>
            </span>
        
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.Cancel" ></button>
    </div>

    
    <div class="modal-body" ng-show="is_manage_home">
        
        <div class="row" style="text-align:center;">
            <h3 class="modal-title" translate="Planning.title-number-participants" ></h3>
            
            <form role="form-number-participants" name="form-number-participants" >
                <div class="form-group" >
                    <label for="number-participants" ><span translate="Field.number-participants" ></span><sup class="required" >*</sup></label>
                    <input type="number" class="form-control" ng-model="course_session.set_nbr_participants"   min="0" />
                </div>
                <button type="submit" class="btn btn-sm btn-primary" ng-click="nbr_paritcipants_cours(course_session.id)">
                    <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                </button>           
            </form>

        </div>
        <hr>
        <div class="row" style="text-align:center;">
            <h3 class="modal-title" translate="Planning.title-choice-coach" ></h3>
            <form role="form-replace-course" name="form-replace-course" class="">
                <div class="form-group" >
                    <select class="select-form-control" ng-change="EmptyCoaches()" ng-model="course_session.course_id" ng-options="course.id as course.course_name | translate for course in courses" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
            </form>
        </div>
        
        <div class="alert alert-danger" role="alert" ng-show="display_alert_coach" >
            Veuillez choisir un nouveau coach
        </div>

        <div class="row" style="text-align:center;">

            <form role="form-replace-coach" name="form-replace-coach" >
                
                <!--
                <div class="form-group">
                    <select class="select-form-control" ng-model="course_session.coach_id"
                        ng-options="coach.id as (coach.firstname+' ' + coach.lastname+' #'+coach.registration_number) | translate for coach in coaches" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                -->

                <div class="form-group" style="text-align:left;"> 
                        <multiple-autocomplete
                            before-select-item="beforeSelectItem"
                            after-select-item="afterSelectItem"
                            before-remove-item="beforeRemoveItem"
                            after-remove-item="afterRemoveItem"
                            ng-model="list_coachs"
                            object-property="name"
                            suggestions-arr="coachs_lst">
                        </multiple-autocomplete>
                </div>

                <div class="alert alert-danger" ng-show="errors_max_five_coachs" >
                              <span translate="Global.max_five_coachs" ></span>
                </div>

                  <div class="alert alert-danger" ng-show="msg_min_one_coach" >
                             Minimum 1 coach
                   </div>

                    <div class="alert alert-danger" ng-show="errors_update_cours_day_after" >
                            Vous ne pouvez pas modifier le cours pour la date (J+1)
                    </div>

                    <button type="submit" class="btn btn-sm btn-primary" ng-click="replace_cours(course_session.id)" >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
               
            </form>
        </div>
        
        <hr>

        <div class="row" style="text-align:center;">
            <h3 class="modal-title" translate="Planning.title-request_cancel_cours" ></h3>
            <form role="form-request-cancel" name="form-request-cancel" class="">
                <button  type="submit" class="btn btn-sm btn-danger" ng-click="request_cancel_cours(course_session.id)" >
                    <i class="fa fa-trash" ></i><span translate="Global.request_cancel_cours" ></span>
                </button>
            </form>
        </div>

        <hr>

        <div class="row" style="text-align:center;">
            <h3 class="modal-title" translate="Planning.title-review" ></h3>
            <form role="form-request-cancel" name="form-request-cancel">
                <textarea class="form-control" name="form-add-review" id="form-add-review" rows="1">{{course_session.review}}</textarea>
                <br>
                <button  type="submit" class="btn btn-sm btn-primary" ng-click="add_review(course_session.id)" >
                    <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                </button>
            </form>
        </div>


        <div ng-show="is_manage_controller">
            <hr>
            <form role="form-request-cancel"  name="form-request-cancel"  class="">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="modal-title" translate="Planning.validate_objectif_remplissage"  ></h3>
                    </div>
                    <div class="col-md-1">
                        <input type="checkbox" class="form-control" id="check_validate_remplissage" ng-model="check_validate_remplissage"/>
                    </div>
                    <div class="col-md-3">
                        <button  type="submit" class="btn btn-sm btn-primary" ng-click="validate_objectif_remplissage(course_session.id)" >
                            <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                        </button>
                    </div>

                    
                </div>
            </form>
        </div>
        <br>
        <div class="alert alert-danger" ng-show="msg_set_nbr_participants" >
                        <span>Veuillez saisir le nombre des participants</span>
        </div>
    </div>
</script>




<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Planning.AreYouSureToDeleteThisCourseSession' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>

<script type="text/ng-template" id="ModalValidateCourseSessions.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        
        <div class="progress" ng-show="analyzing" >
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span translate="Planning.Analyzing" ></span>
            </div>
        </div>

        <div ng-show="!analyzing">
            <div class="alert alert-warning" ng-show="coaches" >
                <span translate="Planning.ACoachDidNotHaveHisMinimumWeeklySchedule" ng-show="coaches.length===1" ></span>
                <span translate="Planning.CoachesDidNotHaveTheirMinimumWeeklySchedule" ng-show="coaches.length>1" ></span>:<br>
                <div class="multi_label_values" ><span class="cochs" ng-repeat="coach in coaches" >{{coach.firstname+' '+coach.lastname+' #'+coach.registration_number}}</span></div>
            </div>
            <p>{{ 'Planning.AreYouSureToValidateThisPlanning' | translate}}</p>
        </div>

    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" ng-show="!analyzing" type="button" ng-click="confirm()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>

<script type="text/ng-template" id="ModalDuplicateCourseSessions.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Planning.DuplicatePlanning" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Planning.SelectAStartDateAndConfirm' | translate}}</p>
        <div class="form-group" >
            <div class="input-group input-group-datepicker-no-clear-button">
              <input type="text" class="form-control" uib-datepicker-popup="{{format}}" date-disabled="disabledDates(date,mode)" ng-click="open_datepicker()" ng-change="change_datepicker()" ng-model="popup_datepicker.date" is-open="popup_datepicker.opened" datepicker-options="dateOptions" current-text="{{'Global.Today'|translate}}" close-text="{{'Global.Close'|translate}}" ng-required="true" alt-input-formats="altInputFormats" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="open_datepicker()"><i class="glyphicon glyphicon-calendar"></i></button>
              </span>
            </div>
        </div>

        <div class="alert alert-danger" ng-show="errors_form" >
            <ul>
                <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.No" ></button>
    </div>
</script>

<script type="text/ng-template" id="ModalSendByEmail.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Planning.SendByEmail" ></h3>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger" ng-show="errors_form" >
            <ul>
                <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-hide="true" ng-click="confirm()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Global.Cancel" ></button>
    </div>
</script>


