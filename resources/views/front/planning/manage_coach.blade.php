




<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.ManagePlanning"></h1>
    </div>
    
    <div class="wrapper-md"  >
        <div>
            <div class="form-group" ng-hide="is_manage_home" >
                
                <div class="row">
                    <div class="col-sm-6 form-group">
                        
                        <div class="btn-group-dropdown" uib-dropdown>
                            
                            <button id="club-button" type="button" class="btn btn-block" ng-class="{'btn-primary': club, 'btn-default': !club}" uib-dropdown-toggle ng-disabled="disabled">
                                <span ng-show="!club && all != 1" >Séléctionner un club</span>
                                <span ng-show="all === 1 && !club" >Tous les clubs</span>
                                <span ng-show="club" translate="Field.Club" ></span><span ng-show="club" >: </span><span ng-show="club" >{{club.club_name}}</span>
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="club-dropdown">
                               <li>
                                 <a ng-click="select_all_club()" ng-class="{active: all===1}" >Tous les clubs</a>
                               </li>
                               <li ng-repeat="obj in clubs">
                                    <a ng-click="select_club(obj)" ng-class="{active: club.id===obj.id}" >{{obj.club_name}}</a>
                                </li>
                            </ul>
                        
                        </div>

                    </div>
                    <div class="col-sm-6 form-group">
                        <div class="btn-group-dropdown" uib-dropdown >
                            <button id="coach-button" type="button" class="btn btn-block" ng-class="{'btn-primary': coach, 'btn-default': !coach}" uib-dropdown-toggle ng-disabled="disabled">
                                <span translate="Coach.SelectCoaches" ng-show="!coach" ></span>
                                <span ng-show="coach">{{coach.firstname}} {{coach.lastname}} #{{coach.registration_number}}</span>
                                <span class="caret"></span>
                            </button>
                            
                            <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="coach-dropdown">
                                <li ng-repeat="obj in ListCoachs" ng-class="{active: coach.id===obj.id}">
                                    <a ng-click="select_coach(obj);" >{{obj.firstname}} {{obj.lastname}} #{{obj.registration_number}}</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        
         
          

            <div class="form-group h4 text-right" ng-show="club && room">
                <span class="label bg-primary label-cost label-cost-per-room" ><label class="font-bold" translate="Planning.CostPerRoom" ></label> <span>{{cost_per_room}}</span></span>
                <span class="label bg-primary dk label-cost label-cost-per-club" ><label class="font-bold" translate="Planning.CostPerClub" ></label> <span>{{cost_per_club}}</span></span>
            </div>
            <div ng-show="club && room" class="footer-container-calender">
                <div class="row" ng-show="have_course_sessions_invalide" >
                    <div class="col-sm-6" >
                        <p ng-show="display_buttton" ><i class="fa fa-info-circle alert-event text-danger" aria-hidden="true"></i> <span translate="Planning.NotValidatedCourseSession"></span></p>
                    </div>
                    <div class="col-sm-6 text-right" ng-show="display_buttton">
                        <button class="btn btn-sm btn-primary"  type="button" ng-click="validate_course_sessions()" translate="Global.Validate" ></button>
                    </div>
                </div>
            </div>
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <div class="col">
                    <div class="clearfix m-b">
                    </div>
                    <div class="pos-rlt">
                        <div class="fc-overlay">
                            <div class="panel bg-white b-a pos-rlt">
                                <span class="arrow"></span>
                                <div class="h4 font-thin m-b-sm">{{event.data.course.course_name}}</div>
                                <div class="line b-b b-light"></div>
                                <div ><i class="fa fa-clock-o text-muted m-r-xs"></i> <span ng-show='event.start' >{{format_date(event.start,'HH:mm')}}</span> - <span ng-show='event.end' >{{format_date(event.end,'HH:mm')}}</span></div>
                                <div ><i class="fa fa-user text-muted m-r-xs"></i> {{event.data.coach.firstname+' '+event.data.coach.lastname+' #'+event.data.coach.registration_number}}</div>
                                <div class="m-t-sm"></div>
                            </div>
                        </div>
                        <div class="calendar_wrapper"><div class="calendar" ng-model="eventSources" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div></div>
                    </div>
                </div>
                <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
                    <div class="wrapper">
                        <div ng-repeat="e in events" class="bg-white-only r r-2x m-b-xs wrapper-sm {{e.className[0]}}">
                            <input ng-model="e.title" class="form-control m-t-n-xs no-border no-padder no-bg">
                            <a class="pull-right text-xs text-muted" ng-click="remove($index)"><i class="fa fa-trash-o"></i></a>
                            <div class="text-xs text-muted">{{e.start | date:"MMM dd"}} - {{e.end | date:"MMM dd"}}</div>
                            <div class="text-xs text-muted">{{e.data.coach.firstname+' '+e.data.coach.lastname+' #'+e.data.coach.registration_number}}</div>
                        </div>
                    </div>
                </div>
            </div>

            <div ng-show="coach && events && !empty_course_sessions" class="footer-container-calender" >
            <div class="row">
                    <div class="col-sm-12 text-right" >
                        <a  class="btn btn-sm btn-default" ng-href="{{url_print_pdf_coach}}" target="_blank" ><i class="fa fa-print" ></i> <span translate="Planning.PrintPlanning" ></span></a>
                    </div>
            </div>
            </div>

             

                
            
            
        </div>
    </div>
</div>









