
<table border="1" class="calendar-table-home" ng-show="is_manage_home" >
    <thead>
        <tr>
            <td ng-repeat="objRoleHome in roomsRoleHome">
                {{objRoleHome.room_name}} <span ng-if="objRoleHome.events">{{objRoleHome.events.length}}</span>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr >
            <td ng-repeat="objRoleHome in roomsRoleHome" >
                <div>
                    <div ui-calendar="objRoleHome.configCalendar"
                    ng-model="objRoleHome.events"
                    id="calendar{{objRoleHome.id}}"
                    class="calendar calendar{{objRoleHome.id}}" ></div>
                </div>
            </td>
        </tr>
    </tbody>
</table>