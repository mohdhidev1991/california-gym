<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.Statistics"></h1>
    </div>
    <div class="wrapper-md">
        <div class="form-group">
            <div class="row">
                <div class="col-sm-4 form-group">
                    <div class="btn-group-dropdown" uib-dropdown>
                        <button id="club-button" type="button" class="btn btn-block" ng-class="{'btn-primary': stats_type, 'btn-default': !stats_type}" uib-dropdown-toggle ng-disabled="disabled">
                            <span>{{stats_type_list[stats_type]|translate}}</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="club-dropdown">
                            <li ng-repeat="(key, value) in stats_type_list" ng-class="{active: stats_type===key}">
                                <a ng-click="select_stats_type(key)">{{value|translate}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--
                <div class="col-sm-4 form-group">
                    <div class="btn-group-dropdown" uib-dropdown>
                        <button id="club-button" type="button" class="btn btn-block" ng-class="{'btn-primary': stats_by, 'btn-default': !stats_by}" uib-dropdown-toggle ng-disabled="disabled">
                            <span translate="Statistics.StatisticsPer"></span><span>: </span><span>{{stats_by_list[stats_by]|translate}}</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="club-dropdown">
                            <li ng-repeat="(key, value) in stats_by_list" ng-class="{active: stats_by===key}">
                                <a ng-click="select_stats_by(key)">{{value|translate}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                -->
                <div class="col-sm-4 form-group">

                    <div class="input-group input-group-datepicker-no-clear-button">
                        <input ui-jq="daterangepicker" ui-options="daterangepickerOptions" is-open="popup_datepicker.opened" ng-change="get_stats()" ng-model="dateRange.date" class="form-control w-md" id="daterangepicker" />


                        <!--<input type="text" class="form-control" uib-datepicker-popup="{{date_format}}" date-disabled="disabledDates(date,mode)" ng-click="open_datepicker()" ng-change="get_stats()" ng-model="popup_datepicker.date" is-open="popup_datepicker.opened" datepicker-options="dateOptions" current-text="{{'Global.Today'|translate}}" close-text="{{'Global.Close'|translate}}" ng-required="true" alt-input-formats="altInputFormats" />-->
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open_datepicker();"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                    </div>
                </div>

                <div class="col-sm-4 form-group" ng-hide="['cost_per_club','number_of_classes_per_club', 'number_of_substitutes_per_club' , 'price_per_course_by_coach' , 'nmber_cour_per_coach_by_club'].indexOf(stats_type)!=-1" >
                    <div class="btn-group-dropdown" uib-dropdown >
                        <button id="club-button" type="button" class="btn btn-block" ng-class="{'btn-primary': club, 'btn-default': !club}" uib-dropdown-toggle ng-disabled="disabled">
                            <span translate="Club.AllClubs" ng-show="!club" ></span>
                            <span ng-show="club" translate="Field.Club" ></span><span ng-show="club" >: </span><span ng-show="club" >{{club.club_name}}</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="club-dropdown">
                            <li ng-class="{active: !club}">
                                <a ng-click="club = null;get_stats();" ><span translate="Global.All" ></span></a>
                            </li>
                            <li ng-repeat="obj in clubs" ng-class="{active: club.id===obj.id}">
                                <a ng-click="select_club(obj);" >{{obj.club_name}}</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-4 form-group" ng-hide="['cost_per_coach','number_of_classes_per_coach', 'number_of_substitutes_per_coach' , 'price_paid_by_course' , 'cost_per_club' , 'number_of_courses_per_coach' , 'number_of_courses_per_concept' , 'total_cost_per_coach' , 'number_of_courses_by_coach' , 'number_of_classes_per_club' , 'number_of_hours_worked_per_course' , 'replacement_number_per_coach' , 'number_of_replaced_by_coach' , 'number_of_substitutes_per_club' ].indexOf(stats_type)!=-1" >
                    <div class="btn-group-dropdown" uib-dropdown >
                        <button id="coach-button" type="button" class="btn btn-block" ng-class="{'btn-primary': coach, 'btn-default': !coach}" uib-dropdown-toggle ng-disabled="disabled">
                            <span  ng-show="!coach" >Séléctionner un coach</span>
                            <span ng-show="coach" >{{coach.firstname}} {{coach.lastname}}</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="coach-dropdown">
                            <li ng-repeat="obj in coachs" ng-class="{active: coach.id===obj.id}">
                                <a ng-click="select_coach(obj);get_stats();" >{{obj.firstname}} {{obj.lastname}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            
            </div>

        </div>
        <div class="margin-bottom-30">


            <div class="alert alert-warning" ng-show="empty_stats"><span translate="Statistics.NoStatisticsToDisplayForThisDate"></span></div>
            
            <div class="alert alert-warning" ng-hide="['cost_per_coach','number_of_classes_per_coach', 'number_of_substitutes_per_coach' , 'price_paid_by_course' , 'cost_per_club' , 'number_of_courses_per_coach' , 'number_of_courses_per_concept' , 'total_cost_per_coach' , 'number_of_courses_by_coach' , 'number_of_classes_per_club' , 'number_of_hours_worked_per_course' , 'replacement_number_per_coach' , 'number_of_replaced_by_coach' , 'number_of_substitutes_per_club' ].indexOf(stats_type)!=-1" ng-show="!coach">Veuillez choisir un coach</div>

            <canvas ng-show="price_paid_by_course" id="price_paid_by_course" class="chart-horizontal-bar" chart-data="price_paid_by_course.data" chart-labels="price_paid_by_course.labels" chart-series="price_paid_by_course.series"></canvas>
            
            <canvas ng-show="price_per_course_by_coach" id="price_per_course_by_coach" class="chart-horizontal-bar" chart-data="price_per_course_by_coach.data" chart-labels="price_per_course_by_coach.labels" chart-series="price_per_course_by_coach.series"></canvas>
            
            <canvas ng-show="nmber_cour_per_coach_by_club" id="nmber_cour_per_coach_by_club" class="chart-horizontal-bar" chart-data="nmber_cour_per_coach_by_club.data" chart-labels="nmber_cour_per_coach_by_club.labels" chart-series="nmber_cour_per_coach_by_club.series"></canvas>
            
            <canvas ng-show="cost_per_club" id="cost_per_club" class="chart-horizontal-bar" chart-data="cost_per_club.data" chart-labels="cost_per_club.labels" chart-series="cost_per_club.series"></canvas>

            <canvas ng-show="total_cost_per_coach" id="total_cost_per_coach" class="chart-horizontal-bar" chart-data="total_cost_per_coach.data" chart-labels="total_cost_per_coach.labels" chart-series="total_cost_per_coach.series" chart-options="{maintainAspectRatio: false}" style="height:1200px"></canvas>

            <canvas ng-show="number_of_courses_per_coach" id="number_of_courses_per_coach" class="chart-horizontal-bar" chart-data="number_of_courses_per_coach.data" chart-labels="number_of_courses_per_coach.labels" chart-series="number_of_courses_per_coach.series" chart-options="{maintainAspectRatio: false}" style="height:1200px"></canvas>
            
            <canvas ng-show="number_of_courses_by_coach" id="number_of_courses_by_coach" class="chart-horizontal-bar" chart-data="number_of_courses_by_coach.data" chart-labels="number_of_courses_by_coach.labels" chart-series="number_of_courses_by_coach.series" chart-options="{maintainAspectRatio: false}" style="height:1200px"></canvas>
        
            <canvas ng-show="replacement_number_per_coach" id="replacement_number_per_coach" class="chart-horizontal-bar" chart-data="replacement_number_per_coach.data" chart-labels="replacement_number_per_coach.labels" chart-series="replacement_number_per_coach.series" style="height:1200px" chart-options="{maintainAspectRatio: false}" ></canvas>

            <canvas ng-show="number_of_classes_per_club" id="number_of_classes_per_club" class="chart-horizontal-bar" chart-data="number_of_classes_per_club.data" chart-labels="number_of_classes_per_club.labels" chart-series="number_of_classes_per_club.series" ></canvas>

            <canvas ng-show="number_of_hours_worked_per_course" id="number_of_hours_worked_per_course" class="chart-horizontal-bar" chart-data="number_of_hours_worked_per_course.data" chart-labels="number_of_hours_worked_per_course.labels" chart-series="number_of_hours_worked_per_course.series"></canvas>

            <canvas ng-show="number_of_replaced_by_coach" id="number_of_replaced_by_coach" class="chart-horizontal-bar" chart-data="number_of_replaced_by_coach.data" chart-labels="number_of_replaced_by_coach.labels" chart-series="number_of_replaced_by_coach.series" style="height:1200px" chart-options="{maintainAspectRatio: false}" ></canvas>

            <canvas ng-show="number_of_substitutes_per_club" id="number_of_substitutes_per_club" class="chart-horizontal-bar" chart-data="number_of_substitutes_per_club.data" chart-labels="number_of_substitutes_per_club.labels" chart-series="number_of_substitutes_per_club.series"></canvas>
            
            <canvas ng-show="number_of_courses_per_concept" id="number_of_courses_per_concept" class="chart-horizontal-bar" chart-data="number_of_courses_per_concept.data" chart-labels="number_of_courses_per_concept.labels" chart-series="number_of_courses_per_concept.series"></canvas>
        </div>
        
        <div class="footer-container-stats" ng-show="url_export_stats">
            <div class="row" >
                <div class="col-sm-12 text-right" >
                    <a class="btn btn-sm btn-default" ng-href="{{url_export_stats}}" target="_blank" ><i class="fa fa-file-excel-o" ></i> <span translate="Statistics.ExportStatistics" ></span></a>
                </div>
            </div>
        </div>

    </div>
</div>