<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Rating.EditRating" translate-values="{rating_id: rating.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Rating.AddNewRating" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Rating.CantGetRating" ng-show="!rating.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="rating.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Rating.RatingInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !rating.id}" >
                      <label for="rating_id" ><span translate="Rating.RatingID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="rating_id" ng-model="rating.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !rating.lookup_code}" >
                  <label for="lookup_code" ><span translate="Rating.RatingLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="rating.lookup_code" class="form-control" placeholder="{{ 'Rating.PleaseEnterTheRatingLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !rating.rating_name_ar}" >
                  <label for="rating_name_ar" ><span translate="Rating.RatingNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="rating_name_ar" ng-model="rating.rating_name_ar" class="form-control" placeholder="{{ 'Rating.PleaseEnterTheRatingNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !rating.rating_name_en}" >
                  <label for="rating_name_en" ><span translate="Rating.RatingNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="rating_name_en" ng-model="rating.rating_name_en" class="form-control" placeholder="{{ 'Rating.PleaseEnterTheRatingNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="max_pct" ><span translate="Rating.RatingMaxPct" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" id="max_pct" ng-model="rating.max_pct" class="form-control" placeholder="{{ 'Rating.PleaseEnterTheRatingMaxPct' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="min_pct" ><span translate="Rating.RatingMinPct" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" id="min_pct" ng-model="rating.min_pct" class="form-control" placeholder="{{ 'Rating.PleaseEnterTheRatingMinPct' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                
                <div class="form-group" >
                    <label for="school_id" ><span translate="Rating.LevelsTemplate" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-model="rating.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "School.SelectSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="Rating.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Rating.AddNewRating" ></span>
                    </button>
                </span>

                <a ui-sref="rating.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>