
<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="User.EditUser" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="User.AddNewUser" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default" ng-show="failed_get_data" >
          <div class="panel-body">
              <div class="alert alert-danger" translate="Global.InvalidData" ></div>
          </div>
          <div class="panel-footer" >
                <a ui-sref="rea_user.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
        </div>
        <div class="panel panel-default" ng-show="rea_user.id || action==='add'">

        <div class="panel-heading font-bold" translate="User.UserInformations" ></div>
            
            <div class="panel-body">
              <form role="form" name="form" class="" >
                <div ng-show="action==='edit'">
                    <div class="form-group" >
                      <label for="rea_user_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="rea_user_id" ng-model="rea_user.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                  <label for="firstname" ><span translate="Field.FirstName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="true" id="firstname" ng-model="rea_user.firstname" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="lastname" ><span translate="Field.LastName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lastname" ng-model="rea_user.lastname" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="email" ><span translate="Field.Email" ></span><sup class="required" >*</sup></label>
                  <input type="email" ng-required="true" required="" id="email" ng-model="rea_user.email" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="username" ><span translate="Field.Username" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="username" ng-model="rea_user.username" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="newpwd" ng-show="action!=='add'" ><span translate="Field.NewPassword" ></span></label>
                  <label for="pwd" ng-show="action==='add'" ><span translate="Field.Password" ></span><sup class="required" >*</sup></label>
                  <input type="password" id="newpwd" ng-required="action==='add'" ng-model="rea_user.newpwd" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="newpwd2" ng-show="action!=='add'" ><span translate="Field.RepeatNewPassword" ></span></label>
                  <label for="pwd2" ng-show="action==='add'" ><span translate="Field.RepeatPassword" ></span><sup class="required" >*</sup></label>
                  <input type="password" ng-required="newpwd" id="newpwd2" ng-model="rea_user.newpwd2" class="form-control" />
                </div>
                <div class="alert alert-danger" ng-show="rea_user.newpwd && rea_user.newpwd2 && rea_user.newpwd!==rea_user.newpwd2" ><span translate="Form.PasswordsNotTheSame" ></span></div>
                <div class="alert alert-warning" ng-show="rea_user.newpwd && !rea_user.newpwd2 && rea_user.newpwd!==rea_user.newpwd2" ><span translate="Form.PleaseRepeatThePassword" ></span></div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                
                <div class="form-group">
                    <label for="role"><span translate="Field.Role"></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in roles">
                        <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, rea_user.roles)" ng-click="toggle_checkbox_option(obj.id, rea_user.roles)" ><i></i>{{obj.role_name}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" >
                    <label for="club" ><span translate="Field.Club" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" id="select-club" ng-required="true" ng-model="rea_user.club" ng-change="selectclubs()" >
                        <option value="{{rea_user.club}}">{{ rea_user.club_name }}</option>
                        <optgroup>
                        <option value="0" >Tous Les Clubs</option>
                        <option ng-repeat="obj in clubs" value="{{obj.id}}" >{{obj.club_name}}</option>
                        </optgroup>
                    </select>
                </div>

                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
              
              </form>


              <div class="alert alert-danger" ng-show="errors_form" >
                  <ul>
                      <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                  </ul>
              </div>

              <div class="alert alert-success" ng-show="success_form" >
                  <span translate="Form.DataSavedWithSuccess" ></span>
              </div>

            </div>

            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid || !pwCheck()" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid || !pwCheck()"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>
                <span ng-show="action==='edit'" uib-tooltip="{{'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid || !pwCheck()" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid || !pwCheck()"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveAndBack" ></span>
                    </button>
                </span>
                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid || !pwCheck()" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled=" (!form.$valid) || !pwCheck()"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Add" ></span>
                    </button>
                </span>

                <a ui-sref="rea_user.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
      </div>
    </div>
</div>