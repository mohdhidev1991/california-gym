<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="User.Profile" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="User.CantGetUserInformation" ng-show="error_get" ></div>


        <div class="panel panel-default" ng-show="rea_user.id" >
            <div class="panel-heading font-bold" translate="User.MyInformations" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>


                <div class="panel panel-default">
                  <div class="panel-heading font-bold" translate="User.PersonalInformations" ></div>

                  <div class="panel-body">
                    <div class="row" >
                      
                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.genre_id}" >
                          <label for="genre_id" ><span translate="Field.Genre" ></span><sup class="required" >*</sup></label>
                          <select class="select-form-control" ng-required="true" ng-model="rea_user.genre_id" ng-options="obj.id as obj.genre_name_ar for obj in genres" >
                              <option value="" >-- {{ "Field.NotSelected" | translate }} --</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.firstname}" >
                          <label for="firstname" ><span translate="Field.FirstName" ></span><sup class="required" >*</sup></label>
                          <input type="text" ng-required="true" id="firstname" ng-model="rea_user.firstname" class="form-control" placeholder="{{ 'User.PleaseEnterTheUserFirstName' | translate }}" />
                        </div>
                      </div>

                      <div class="col-md-12" >
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.f_firstname}" >
                          <label for="f_firstname" ><span translate="Field.FamilyName" ></span><sup class="required" >*</sup></label>
                          <input type="text" ng-required="true" id="f_firstname" ng-model="rea_user.f_firstname" class="form-control" placeholder="*" />
                        </div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.lastname}" >
                          <label for="lastname" ><span translate="Field.LastName" ></span><sup class="required" >*</sup></label>
                          <input type="text" ng-required="true" id="lastname" ng-model="rea_user.lastname" class="form-control" placeholder="*" />
                        </div>
                      </div>


                      <div class="col-md-12" >
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.idn_type_id}" >
                            <label for="idn_type_id" ><span translate="Field.IdnType" ></span><sup class="required" >*</sup></label>
                            <select class="select-form-control" ng-required="true" ng-model="rea_user.idn_type_id" ng-options="obj.id as obj.idn_type_name_ar for obj in idn_types" >
                                <option value="" >-- {{ "Field.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" >
                          <label for="idn" ><span translate="Field.IDN" ></span></label>
                          <input type="text" id="idn" ng-required="true" ng-model="rea_user.idn" class="form-control" placeholder="{{ 'User.PleaseEnterTheUserIDN' | translate }}" />
                        </div>
                      </div>


                      <div class="col-md-12" >
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.country_id}" >
                            <label for="country_id" ><span translate="Field.Country" ></span><sup class="required" >*</sup></label>
                            <select class="select-form-control" ng-required="true" ng-model="rea_user.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                                <option value="" >-- {{ "Field.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.lang_id}" >
                            <label for="lang_id" ><span translate="Field.Language" ></span><sup class="required" >*</sup></label>
                            <select class="select-form-control" ng-required="true" ng-model="rea_user.lang_id" ng-options="obj.id as obj.lang_name_ar for obj in languages" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                      </div>

                    </div>
                  </div><!-- #panel-body -->
                </div>




                <div class="panel panel-default">
                  <div class="panel-heading font-bold" translate="User.ConnexionInformations" ></div>

                  <div class="panel-body">
                    <div class="row" >

                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.email}" >
                          <label for="email" ><span translate="Field.Email" ></span><sup class="required" >*</sup></label>
                          <input type="email" ng-required="true" required="" id="email" ng-model="rea_user.email" class="form-control" placeholder="*" />
                        </div>
                      </div>


                      <div class="col-md-6" >
                        <div class="form-group" ng-class="{'has-error': !rea_user.mobile}" >
                          <label for="mobile" ><span translate="Field.Mobile" ></span></label>
                          <input type="text" id="mobile" ng-required="true" ng-model="rea_user.mobile" class="form-control" placeholder="*" />
                        </div>
                      </div>


                      <div class="col-md-12" >
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                      </div>


                      <div class="col-md-12" ng-show="rea_user.valide_email!=='Y' || rea_user.valide_mobile!=='Y'" >
                        
                        <div class="alert alert-warning" ng-show="rea_user.valide_email!=='Y' && !success_send_active_email" >
                          <span translate="User.PleaseActivateYourEmailAddress" ></span>
                          <a class="btn btn-primary btn-xs" ng-click="send_active_email()" translate="User.SendActivationCode" ></a>
                        </div>
                        <div class="alert alert-success" ng-show="success_send_active_email" >
                          <span translate="User.EmailActivationSendedPleaseCheckYourEmail" ></span>
                        </div>


                        <div class="alert alert-warning" ng-show="rea_user.valide_mobile!=='Y'" ng-hide="success_send_active_mobile" >
                          <span translate="User.PleaseActivateYourMobile" ></span>
                          <a class="btn btn-primary btn-xs" ng-click="send_active_mobile()" translate="User.SendActivationCode" ></a>
                        </div>

                        <div class="alert alert-danger" ng-show="errors_send_active_mobile" >
                            <ul>
                                <li ng-repeat="error in errors_send_active_mobile" ><span translate="{{error}}" ></span></li>
                            </ul>
                        </div>

                        <div class="form-group" ng-show="success_send_active_mobile" >
                          
                          <div class="alert alert-success" ng-hide="success_submit_activation_mobile_code" >
                            <span translate="User.MobileCodeActivationSendedPleaseCheckYourMobileAndEnterTheCode" ></span>
                          </div>

                          <div class="input-group m-b" ng-hide="success_submit_activation_mobile_code" >
                            <input type="text" id="mobile_activation_code" ng-model="mobile_activation_code" name="mobile_activation_code" class="form-control" />
                            <span class="input-group-btn">
                              <span uib-tooltip="{{ 'User.PleaseEnterRecivedActivationCode' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!mobile_activation_code" tooltip-class="tooltip-danger zindex9999" >
                                <button class="btn btn-default" ng-click="submit_activation_mobile_code()" ng-disabled="!mobile_activation_code" type="button" translate="User.EnterActivationCode" ></button>
                              </span>
                            </span>
                          </div>

                          <div class="alert alert-success" ng-show="success_submit_activation_mobile_code" >
                            <span translate="User.YourMobileIsActiveNow" ></span>
                          </div>

                          <div class="alert alert-danger" ng-show="error_submit_activation_mobile_code" >
                            <span translate="User.InvalideActivationCode" ></span>
                          </div>
                          
                        </div>

                      </div>


                      <div class="col-md-12" ng-show="rea_user.valide_email!=='Y' || rea_user.valide_mobile!=='Y'" >
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                      </div>


                      <div class="col-md-6" >
                        <div class="form-group" >
                          <label for="newpwd" ><span translate="Field.NewPassword" ></span></label>
                          <input type="password" ui-validate=" '!$value || $value.length==0 || $value.length>5' " id="newpwd" ng-model="rea_user.newpwd" class="form-control" />
                        </div>
                      </div>

                      <div class="col-md-6" >
                        <div class="form-group" >
                          <label for="newpwd2" ><span translate="Field.RepeatPassword" ></span></label>
                          <input type="password" ng-required="rea_user.newpwd" ui-validate=" ' ($value==rea_user.newpwd) || (!rea_user.newpwd && !rea_user.newpwd2)' " id="newpwd2" name="newpwd2" ng-model="rea_user.newpwd2" class="form-control" />
                        </div>
                      </div>

                      <div class="col-md-12" >
                        <div class="alert alert-warning" ng-show="(rea_user.newpwd && !rea_user.newpwd2) || (rea_user.newpwd && rea_user.newpwd.length<6)" ><span translate="Form.PleaseEnterStrongPasswordMin6AndRepeatTheNewPassword" ></span></div>
                        <div class="alert alert-danger" ng-show="form.newpwd2.$error.validator" ><span translate="Form.PasswordsNotTheSame" ></span></div>
                      </div>


                      

                    </div>
                  </div>
                </div>
                
 



                <div class="panel panel-default">
                  <div class="panel-heading font-bold" translate="User.NationalAddress" ></div>

                  <div class="panel-body">
                      <div class="row" >

                          <div class="col-md-6" >
                              <div class="form-group" >
                                <label for="address" ><span translate="Field.Address" ></span><sup class="required" >*</sup></label>
                                <input type="text" id="address" ng-required="true" ng-model="rea_user.address" class="form-control" placeholder="*" />
                              </div>
                          </div>

                          <div class="col-md-6" >
                            <div class="alert alert-info" ><span translate="User.SubmitYourAdressCode252" ></span></div>
                          </div>

                          <div class="col-md-12" >
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>


                          <div class="col-md-6" >
                            <div class="form-group" >
                              <label for="quarter" ><span translate="Field.Quarter" ></span><sup class="required" >*</sup></label>
                              <input type="text" id="quarter" ng-required="true" ng-model="rea_user.quarter" class="form-control" placeholder="*" />
                            </div>
                          </div>

                          <div class="col-md-6" >
                            <div class="form-group" ng-class="{'has-error': !rea_user.city_id}" >
                                <label for="city_id" ><span translate="Field.City" ></span><sup class="required" >*</sup></label>
                                <select class="select-form-control" ng-required="true" ng-model="rea_user.city_id" ng-options="obj.id as obj.city_name_ar for obj in cities" >
                                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                </select>
                            </div>
                          </div>


                          <div class="col-md-12" >
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>

                          <div class="col-md-6" >
                            <div class="form-group" >
                              <label for="cp" ><span translate="Field.PostalCode" ></span><sup class="required" >*</sup></label>
                              <input type="text" id="cp" ng-model="rea_user.cp" ng-required="true" class="form-control" placeholder="*" />
                            </div>
                          </div>

                      </div>
                  </div>
                </div>



                <div class="panel panel-default" ng-show="rea_user.email && (rea_user.fb_id || rea_user.google_id || rea_user.twitter_id)" >
                  <div class="panel-heading font-bold" translate="User.SocialAccounts" ></div>

                  <div class="panel-body" >
                      <div class="row" >
                          <div class="col-md-12" ng-show="rea_user.fb_id" >
                            <div class="form-group" >
                              <label>
                                <a href="https://facebook.com/{{rea_user.fb_id}}" target="_blank" class="btn btn-rounded btn btn-icon btn-icon-no-margin btn-default" ><i class="fa fa-facebook" ></i></a>
                                <span translate="User.FacebookAccountID" ></span>: <a target="_blank" href="https://facebook.com/{{rea_user.fb_id}}"  >{{rea_user.fb_id}}</a>
                              </label>
                              <div class="clear" ><button class="btn btn-xs btn-danger" ng-click="remove_connexion('facebook')" ><i class="fa fa-times"></i><span translate="User.RemoveConnexion" ></span></button></div>
                            </div>
                          </div>
                          <div class="col-md-12" ng-show="rea_user.fb_id" >
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>

                          <div class="col-md-12" ng-show="rea_user.twitter_id" >
                            <div class="form-group" >
                              <label>
                                <a href="https://twitter.com/intent/user?user_id={{rea_user.twitter_id}}" target="_blank" class="btn btn-rounded btn btn-icon btn-icon-no-margin btn-default" ><i class="fa fa-twitter" ></i></a>
                                <span translate="User.TwitterAccountID" ></span>: <a target="_blank" href="https://twitter.com/intent/user?user_id={{rea_user.twitter_id}}"  >{{rea_user.twitter_id}}</a>
                              </label>
                              <div class="clear" ><button class="btn btn-xs btn-danger" ng-click="remove_connexion('twitter')" ><i class="fa fa-times"></i><span translate="User.RemoveConnexion" ></span></button></div>
                            </div>
                          </div>
                          <div class="col-md-12" ng-show="rea_user.twitter_id" >
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>

                          <div class="col-md-12" ng-show="rea_user.google_id" >
                            <div class="form-group" >
                              <label>
                                <a href="https://plus.google.com/{{rea_user.google_id}}" target="_blank" class="btn btn-rounded btn btn-icon btn-icon-no-margin btn-default" ><i class="fa fa-google-plus" ></i></a>
                                <span translate="User.GoogleAccountID" ></span>: <a target="_blank" href="https://plus.google.com/{{rea_user.google_id}}"  >{{rea_user.google_id}}</a>
                              </label>
                              <div class="clear" ><button class="btn btn-xs btn-danger" ng-click="remove_connexion('google')" ><i class="fa fa-times"></i><span translate="User.RemoveConnexion" ></span></button></div>
                            </div>
                          </div>
                      </div>
                  </div>
                </div>

                
              </form>
            </div>
            <div class="panel-footer" >
                <div class="alert alert-success" ng-show="success_update" ><span translate="Form.SuccessUpdate" ></span></div>
                <div class="alert alert-danger" ng-show="error_update" >
                  <ul>
                    <li ng-repeat="error in error_update_message" ><span translate="{{error}}" ></span></li>
                  </ul>
                  
                </div>


                <span uib-tooltip="Form.PleaseFillAllRequiredFields" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" aria-hidden="false" class="">
                    <button ng-disabled="!form.$valid"
                    type="submit" class="btn btn-sm btn-primary" ng-click="save()"
                    >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveData" ></span>
                    </button>
                </span>
                

                <a ui-sref="admin.dashbord" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Form.Cancel" ></span>
                </a>
            </div>


      </div>
    </div>
</div>