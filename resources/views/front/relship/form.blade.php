<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Relship.EditRelship" translate-values="{relship_id: relship.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Relship.AddNewRelship" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Relship.CantGetRelship" ng-show="!relship.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="relship.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Relship.RelshipInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !relship.id}" >
                      <label for="relship_id" ><span translate="Relship.RelshipID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="relship_id" ng-model="relship.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !relship.lookup_code}" >
                  <label for="lookup_code" ><span translate="Relship.RelshipLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="relship.lookup_code" class="form-control" placeholder="{{ 'Relship.PleaseEnterTheRelshipLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !relship.relship_name_ar}" >
                  <label for="relship_name_ar" ><span translate="Relship.RelshipNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="relship_name_ar" ng-model="relship.relship_name_ar" class="form-control" placeholder="{{ 'Relship.PleaseEnterTheRelshipNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !relship.relship_name_en}" >
                  <label for="relship_name_en" ><span translate="Relship.RelshipNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="relship_name_en" ng-model="relship.relship_name_en" class="form-control" placeholder="{{ 'Relship.PleaseEnterTheRelshipNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="Relship.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Relship.AddNewRelship" ></span>
                    </button>
                </span>

                <a ui-sref="relship.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>

      </div>
    </div>
</div>