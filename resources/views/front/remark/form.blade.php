<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Remark.EditRemark" translate-values="{remark_id: remark.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Remark.AddNewRemark" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Remark.CantGetRemark" ng-show="!remark.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="remark.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Remark.RemarkInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !remark.id}" >
                      <label for="remark_id" ><span translate="Remark.RemarkID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="remark_id" ng-model="remark.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !remark.lookup_code}" >
                  <label for="lookup_code" ><span translate="Remark.RemarkLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="remark.lookup_code" class="form-control" placeholder="{{ 'Remark.PleaseEnterTheRemarkLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !remark.remark_name_ar}" >
                  <label for="remark_name_ar" ><span translate="Remark.RemarkNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="remark_name_ar" ng-model="remark.remark_name_ar" class="form-control" placeholder="{{ 'Remark.PleaseEnterTheRemarkNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !remark.remark_name_en}" >
                  <label for="remark_name_en" ><span translate="Remark.RemarkNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="remark_name_en" ng-model="remark.remark_name_en" class="form-control" placeholder="{{ 'Remark.PleaseEnterTheRemarkNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group" ng-class="{'has-error': !remark.gremark_id}" >
                    <label for="gremark_id" ><span translate="Remark.LevelsTemplate" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="remark.gremark_id" ng-options="obj.id as obj.gremark_name_ar for obj in gremarks" >
                        <option value="" >-- {{ "School.SelectGRemark" | translate }} --</option>
                    </select>
                </div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="Remark.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Remark.AddNewRemark" ></span>
                    </button>
                </span>

                <a ui-sref="remark.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>