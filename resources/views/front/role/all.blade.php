<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.ManageRoles" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="Role.AllRoles" ></div>
            <div class="panel-header panel-header with-padding">
                <div class="header-tools header-btns" >
                    <a class="btn btn-default" ui-sref="role.add({add: true})" ><i class="fa fa-plus" ></i><span translate="Role.AddNewRole" ></span></a>
                </div>
                
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            
                            <tr>
                                <th style="width:20px;" >
                                    <label translate="Field.Active" ></label>
                                </th>
                                <th style="width:20px;">
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th>
                                    <label translate="Field.RoleName" ></label>
                                </th>
                                <th>
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                            
                        </thead>
                        <tbody>
                            <tr ng-repeat="role in roles" >
                                <td>
                                    <span >
                                        <i ng-show="role.active==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="role.active==='N'" class="fa fa-times text-danger text"></i>
                                    </span>
                                </td>
                                <td>
                                    <span>{{role.id}}</span>
                                </td>
                                <td>
                                    <span>{{role.role_name}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" ui-sref="role.edit({role_id: role.id})" ><i class="fa fa-edit"></i><span translate="Global.Edit" ></span></a>
                                    <a class="btn btn-xs btn-danger" ng-click="delete(role.id)" ><i class="fa fa-remove"></i><span translate="Global.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center"></div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetRoles()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Role.AreYouSureToDeleteThisRole' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>