<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Room.EditRoom" translate-values="{room_id: room.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Room.AddNewRoom" ></h1>
    </div>

    <div class="wrapper-md">

         <div class="panel panel-default" ng-show="failed_get_data" >
          <div class="panel-body">
              <div class="alert alert-danger" translate="Global.InvalidData" ></div>
          </div>
          <div class="panel-footer" >
                <a ui-sref="setting.club.rooms({club_id: club_id})" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>
        </div>


        <div class="panel panel-default" ng-show="room.id || action==='add'" ng-hide="!club" >
            <div class="panel-heading font-bold" translate="Room.RoomInformations" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                <div class="form-group" >
                   <div class="row">
                   <div class="col-md-12">
                    <label for="club_id" ><span translate="Field.Club" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-disabled="true" ng-model="room.club_id" ng-options="obj.id as obj.club_name for obj in clubs" >
                        <option value="" >-- {{ "Global.NotSelected" | translate }} --</option>
                    </select>
                    </div>

                    </div>
                </div>

                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div ng-show="action==='edit'">
                    <div class="form-group" ng-class="{'has-error': !room.id}" >
                      <label for="room_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="room_id" ng-model="room.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" >
                  <label for="room_name" >Code Salle<sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="code_room" ng-model="room.code_room" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                
                
                <div class="form-group" >
                  <label for="room_name" ><span translate="Field.RoomName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="room_name" ng-model="room.room_name" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="capacity" ><span translate="Field.Capacity" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="" ng-pattern="/^[0-9]{1,7}$/" id="capacity" ng-model="room.capacity" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="wdays" ><span translate="Field.Disponibility" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in wdays" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, room.disponibilities)" ng-click="toggle_checkbox_option(obj.id, room.disponibilities)" ><i></i>{{obj.wday_name}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>

                <div class="alert alert-danger" ng-show="errors_form" >
                  <ul>
                      <li ng-repeat="error in errors_form" ><span translate="{{error}}" ></span></li>
                  </ul>
                </div>

                <div class="alert alert-success" ng-show="success_form" >
                  <span translate="Form.DataSavedWithSuccess" ></span>
                </div>

              </form>
            </div>


            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.Save" ></span>
                    </button>
                </span>

                <a ui-sref="setting.club.rooms({club_id: club_id})" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>