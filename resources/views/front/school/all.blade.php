<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="School.ManageSchools" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="School.AllSchools" ></div>
            <div class="panel-header panel-header with-padding">
                
                <div class="header-tools header-btns margin-bottom-15" >
                    <a class="btn btn-default" ui-sref="school.add({add: true})" ><i class="fa fa-plus" ></i><span translate="School.AddNewSchool" ></span></a>
                </div>

                <div class="filter filter-table" >
                    <form name="filter" ng-class="{}">
                        <div class="header-filter" >
                        </div>
                        <div class="body-filter" >
                            <div class="form-group" >
                                <label for="filter_name_or_id" ><span translate="Field.NameOrId" ></span></label>
                                <input type="text" id="filter_name_or_id" ng-model="filter.name_or_id" class="form-control" />
                            </div>

                            <div class="form-group" >
                                <label for="filter_status" ><span translate="Field.Active" ></span></label>
                                <select id="filter_status" class="select-form-control" ng-model="filter.status" ng-options="obj.id as obj.label | translate for obj in active_status" >
                                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                </select>
                            </div>

                            <div class="form-group" >
                                <label for="filter_school_type_id" ><span translate="Field.SchoolType" ></span></label>
                                <select id="filter_school_type_id" class="select-form-control" ng-model="filter.school_type_id" ng-options="obj.id as obj.school_type_name_ar for obj in school_types" >
                                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                </select>
                            </div>

                            <div ng-show="show_all_filters" >
                                <div class="form-group" >
                                    <label for="filter_group_num" ><span translate="Field.GroupNum" ></span></label>
                                    <input type="number" id="filter_group_num" ng-model="filter.group_num" class="form-control" />
                                </div>

                                <div class="form-group" >
                                    <label for="filter_genre_id" ><span translate="Field.Genre" ></span></label>
                                    <select id="filter_genre_id" class="select-form-control" ng-model="filter.genre_id" ng-options="obj.id as obj.genre_name_ar for obj in genres" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>

                                <div class="form-group" >
                                    <label for="filter_lang_id" ><span translate="Field.Language" ></span></label>
                                    <select id="filter_lang_id" class="select-form-control" ng-model="filter.lang_id" ng-options="obj.id as obj.lang_name_ar for obj in languages" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="levels_template_id" ><span translate="Field.LevelsTemplate" ></span></label>
                                    <select id="filter_levels_template_id" class="select-form-control" ng-model="filter.levels_template_id" ng-options="obj.id as obj.levels_template_name_ar for obj in levels_templates" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="filter_country_id" ><span translate="Field.Country" ></span></label>
                                    <select id="filter_country_id" class="select-form-control" ng-change="GetCities();" ng-model="filter.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="filter_city_id" ><span translate="Field.City" ></span></label>
                                    <select id="filter_city_id" class="select-form-control" ng-disabled="!filter.country_id" ng-model="filter.city_id" ng-options="obj.id as obj.city_name_ar for obj in cities" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="filter_courses_template_id" ><span translate="Field.CoursesTemplate" ></span></label>
                                    <select id="filter_courses_template_id"  class="select-form-control" ng-model="filter.courses_template_id" ng-options="obj.id as obj.courses_template_name_ar for obj in courses_templates" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="filter_period_id" ><span translate="Field.Period" ></span></label>
                                    <select id="filter_period_id"  class="select-form-control" ng-model="filter.period_id" ng-options="obj.id as obj.period_name_ar for obj in periods" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="filter_school_level_id" ><span translate="Field.SchoolLevel" ></span></label>
                                    <select id="filter_school_level_id"  class="select-form-control" ng-model="filter.school_level_id" ng-options="obj.id as obj.school_level_name_ar for obj in school_levels" >
                                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="footer-filtrer" >
                            <span class="checkbox">
                                <label class="i-checks" >
                                    <input type="checkbox" ng-model="show_all_filters" ng-change="filter.status = filter.school_type_id = filter.name_or_id = filter.group_num = filter.country_id = filter.city_id = filter.genre_id = filter.lang_id = filter.levels_template_id = filter.courses_template_id = filter.school_level_id = filter.period_id = null" ><i></i> <span translate="Form.ShowAllFilters" ></span>
                                </label>
                            </span>
                            <button class="btn btn-primary" ng-click="GetSchools();" ><i class="fa fa-filter"></i><span translate="Form.Filter" ></span></button>
                            <button class="btn btn-danger" ng-show="filter.status || filter.school_type_id || filter.name_or_id || filter.group_num || filter.country_id || filter.city_id || filter.genre_id || filter.lang_id || filter.levels_template_id || filter.courses_template_id || filter.school_level_id || filter.period_id" ng-click="filter={};GetSchools();" ><i class="fa fa-times" ></i><span translate="Form.Reset" ></span></button>
                        </div>
                    </form>
                    
                </div>
                <div class="line line-dashed b-b line-lg"></div>
            </div>



            <div class="panel-body">
                <div class="alert alert-danger" ng-show="empty_results" >
                    <span translate="Global.NoResultsFound" ></span>
                </div>

                <div class="table-responsive" ng-show="schools" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='active', th_sorting_asc: order_by==='active' && order==='DESC', th_sorting_desc: order_by==='active' && order==='ASC' }" ng-click="set_order('active', null )" >
                                    <label translate="Field.Active" ></label>
                                </th>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='id', th_sorting_asc: order_by==='id' && order==='DESC', th_sorting_desc: order_by==='id' && order==='ASC' }" ng-click="set_order('id', null )" >
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='school_name_ar', th_sorting_asc: order_by==='school_name_ar' && order==='DESC', th_sorting_desc: order_by==='school_name_ar' && order==='ASC' }" ng-click="set_order('school_name_ar', null )" >
                                    <label translate="Field.ArabicName" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='school_name_en', th_sorting_asc: order_by==='school_name_en' && order==='DESC', th_sorting_desc: order_by==='school_name_en' && order==='ASC' }" ng-click="set_order('school_name_en', null )" >
                                    <label translate="Field.EnglishName" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="school in schools" >
                                <td>
                                    <span >
                                        <i ng-show="school.active==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="school.active==='N'" class="fa fa-times text-danger text"></i>
                                    </span>
                                </td>
                                <td>
                                    <span>{{school.id}}</span>
                                </td>
                                <td>
                                    <span>{{school.school_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{school.school_name_en}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-primary btn-xs" ui-sref="school.infos({school_id: school.id})" ><i class="fa fa-search"></i><span translate="School.SchoolInfos" ></span></a>
                                    <a class="btn btn-sm btn-primary btn-xs" ui-sref="school.edit({school_id: school.id})" ><i class="fa fa-edit"></i><span translate="Form.Edit" ></span></a>
                                    <a class="btn btn-sm btn-danger btn-xs" ng-click="delete_school(school.id)" ><i class="fa fa-remove"></i><span translate="Form.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer" ng-hide="empty_results" >
                <div class="text-right form-group" >
                    <button class="btn btn-default" ng-click="export_xls();" ><i class="fa fa-file-excel-o"></i><span translate="Form.Export" ></span></button>
                </div>
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetSchools()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Form.AreYouSureToDeleteThisData' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>