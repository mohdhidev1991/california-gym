<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="School.CourseSchedGenerator"></h1>
    </div>

    <div class="wrapper-md">

        <div class="panel panel-default">
            <div class="panel-heading" translate="School.CourseSchedGenerator" ></div>

            <header class="panel-header with-padding">
            	<form name="form_find" >
	            	<div class="form-container" >
		            	
		            	<div class="form-group">
		            		<div class="row">
			                    <label class="col-sm-3 control-label text-left">
			                    	<span translate="School.Name" ></span>
			                	</label>
			                	<label class="col-sm-9">
			                    	<select class="select-form-control" ng-change="changed_selected_school_id()" ng-required="true" ng-model="selected_school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" ng-disabled="!schools" >
			                    		<option value="" selected="selected" >-- {{ "School.SelectTheSchool" | translate }} --</option>
			                    	</select>
			                	</label>
		                 	</div>
		                </div>


		                <div class="form-group">
		            		<div class="row">
			                    <label class="col-sm-3 control-label text-left">
			                    	<span translate="School.SchoolYear" ></span>
			                	</label>
			                	<label class="col-sm-9">
			                    	<select class="select-form-control" ng-required="true" ng-change="changed_selected_school_year_id()" ng-model="selected_school_year_id" ng-options="obj.id as obj.school_year_name_ar for obj in school_years" ng-disabled="!selected_school_id || !school_years" >
			                    		<option value="" selected="selected" >-- -- {{ "School.SelectTheSchoolYear" | translate }} --</option>
			                    	</select>
			                	</label>
		                 	</div>
		                </div>



		                <div class="form-group">
		            		<div class="row">
			                    <label class="col-sm-3 control-label text-left">
			                    	<span translate="School.LevelClass" ></span>
			                	</label>
			                	<label class="col-sm-9">
			                    	<select class="select-form-control" ng-required="true" ng-change="changed_selected_level_class_id()" ng-model="selected_level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" ng-disabled="!selected_school_year_id || !level_classes" >
			                    		<option value="" selected="selected" >-- {{ "School.SelectTheLevelClass" | translate }} --</option>
			                    	</select>
			                	</label>
		                 	</div>
		                </div>


		                <div class="form-group">
		            		<div class="row">
			                    <label class="col-sm-3 control-label text-left">
			                    	<span translate="School.SchoolClass" ></span>
			                	</label>
			                	<label class="col-sm-9">
			                    	<select class="select-form-control" ng-required="true" ng-change="changed_selected_school_class_id()" ng-model="selected_school_class_id" ng-options="obj.id as obj.symbol for obj in school_classes" ng-disabled="!selected_level_class_id || !school_classes" >
			                    		<option value="" selected="selected" >-- {{ "School.SelectTheSchoolClass" | translate }} --</option>
			                    	</select>
			                	</label>
		                 	</div>
		                </div>


		                <div class="form-group">
		            		<div class="row">
			                    <div class="col-md-12">
			                    	<span uib-tooltip="Form.PleaseFillAllRequiredFields" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_find.$valid" tooltip-class="tooltip-danger zindex9999" aria-hidden="false" class="">
			                            <button ng-click="find_course_sched_items()" ng-disabled="!form_find.$valid" class="btn btn-sm btn-primary btn-default" ><i class="fa fa-search" ></i><span translate="Global.Find" ></span></button>
			                        </span>
			                	</div>
		                 	</div>
		                </div>

	                </div>
            	</form>
            </header>

            <div class="panel-body" ng-show="submited_find_course_sched_items" >
            	<div class="form-container" >
	                <div class="form-group">
	                	<div class="alert alert-danger" translate="Global.EmptyQ0010" ng-show="new_template" ></div>

	                    <table class="table table-bordered table-striped" >
	                        <thead ng-show="new_template" >
	                            <tr  >
	                                <th colspan="6" >
	                                    <select class="select-form-control" ng-required="true" ng-model="selected_week_template_id" ng-options="obj.id as obj.week_template_name_ar for obj in week_templates" >
			                    			<option value="" selected="selected" >{{ "School.SelectTheSchoolClass" | translate }}</option>
			                    		</select>
	                                </th>
	                                <th colspan="2" ><button class="btn btn-primary" ng-disabled="!selected_week_template_id" ng-click="creat_from_template()" translate="CreatFromTemplate" ></button></th>
	                            </tr>
	                        </thead>
	                        <tbody ng-show="course_sched_items" >
	                            <tr>
	                                <td class="td-session" >Session</td>
	                                <td class="td-day td-day-1" ng-show="days[1]" translate="Date.Sunday" ></td>
	                                <td class="td-day td-day-2" ng-show="days[2]" translate="Date.Monday" ></td>
	                                <td class="td-day td-day-3" ng-show="days[3]" translate="Date.Tuesday" ></td>
	                                <td class="td-day td-day-4" ng-show="days[4]" translate="Date.Wednesday" ></td>
	                                <td class="td-day td-day-5" ng-show="days[5]" translate="Date.Thursday" ></td>
	                                <td class="td-day td-day-6" ng-show="days[6]" translate="Date.Friday" ></td>
	                                <td class="td-day td-day-7" ng-show="days[7]" translate="Date.Saturday" ></td>
	                            </tr>
	                            <tr ng-repeat="session in sessions" ng-show="$index" >
	                                <td class="td-session" ><span>Session {{$index}}</span></td>
	                                <td class="td-day td-day-{{fday}}" ng-repeat="fday in [1,2,3,4,5,6,7]" ng-show="days[fday]" >
	                                	<!--<input  class="form-control" />-->
	                                	<!--{{fday}}-{{session[fday].id}}- {{session[fday].course_id}}-->
	                                	<select class="select-form-control" ng-model="session[fday].course_id" ng-options="obj.id as obj.course_name_ar for obj in courses" >
			                    			<option value="" selected="selected" >-- {{ "School.SelectCourse" | translate }} --</option>
			                    		</select>
	                            	</td>
                            	</tr>
	                        </tbody>
	                    </table>
	                </div>
            	</div>
            </div>

            <div class="panel-footer" ng-show="course_sched_items" >
            	<div class="alert alert-success" ng-show="success_update" translate="Form.DataSaved" ></div>
	            <div class="text-right" ><button class="btn btn-primary" ng-click="save()" translate="Global.SaveData" ></button></div>
            </div>


        </div>

    </div>

</div>