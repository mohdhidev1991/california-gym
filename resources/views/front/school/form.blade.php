<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="School.EditSchoolInfos" translate-values="{school_id: school.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="School.AddNewSchool" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="School.CantGetSchool" ng-show="!school.id && action==='edit'" ></div>




        <div class="panel panel-default" ng-show="school.id || action==='add'" >
            <div class="panel-heading font-bold" translate="School.SchoolInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school.id}" >
                      <label for="school_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="number" ng-disabled="true" ng-required="action==='edit'" required="" id="school_id" ng-model="school.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !school.group_num}" >
                  <label for="group_num" ><span translate="Field.GroupNum" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="" id="group_num" ng-model="school.group_num" class="form-control" placeholder="{{ 'Form.PleaseEnterThisRequiredField' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" ng-class="{'has-error': !school.school_name_ar}" >
                  <label for="school_name_ar" ><span translate="Field.ArabicName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_name_ar" ng-model="school.school_name_ar" class="form-control" placeholder="{{ 'Form.PleaseEnterThisRequiredField' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school.school_name_en}" >
                  <label for="school_name_en" ><span translate="Field.EnglishName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" id="school_name_en" ng-model="school.school_name_en" class="form-control" placeholder="{{ 'Form.PleaseEnterThisRequiredField' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="address" ><span translate="Field.Address" ></span></label>
                  <input type="text" id="address" ng-model="school.address" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="pc" ><span translate="Field.PostalCode" ></span></label>
                  <input type="text" id="pc" ng-model="school.pc" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="expiring_date" ><span translate="Field.ExpiringDate" ></span></label>
                  <islamic-datepicker id="expiring_date" ng-model="school.expiring_date" ng-model-hdate="school.expiring_hdate" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="quarter" ><span translate="Field.Quarter" ></span></label>
                  <input type="text" id="quarter" ng-model="school.quarter" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school.genre_id}" >
                    <label for="genre_id" ><span translate="Field.Genre" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school.genre_id" ng-options="obj.id as obj.genre_name_ar for obj in genres" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school.city_id}" >
                    <label for="city_id" ><span translate="Field.City" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school.city_id" ng-options="obj.id as obj.city_name_ar for obj in cities" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school.school_type_id}" >
                    <label for="school_type_id" ><span translate="Field.SchoolType" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school.school_type_id" ng-options="obj.id as obj.school_type_name_ar for obj in school_types" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="group_school_id" ><span translate="Field.Group" ></span></label>
                    <select class="select-form-control" ng-model="school.group_school_id" ng-options="obj.id as obj.school_name_ar for obj in group_schools" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>




                <div class="form-group" >
                  <label for="sp1" ><span translate="Field.SP1" ></span></label>
                  <input type="text" id="sp1" ng-model="school.sp1" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="sp2" ><span translate="Field.SP2" ></span></label>
                  <input type="text" id="sp1" ng-model="school.sp2" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="scapacity" ><span translate="Field.SchoolCapacity" ></span></label>
                  <input type="text" id="sp1" ng-model="school.scapacity" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="maps_location_url" ><span translate="Field.LocationMap" ></span></label>
                  <input type="text" id="maps_location_url" ng-model="school.maps_location_url" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="sp2" ><span translate="Field.Levels" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in school_levels" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, school.school_levels)" ng-click="toggle_checkbox_option(obj.id, school.school_levels)" ><i></i>{{obj.school_level_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                    <label for="period" ><span translate="Field.Periods" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in periods" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, school.periods)" ng-click="toggle_checkbox_option(obj.id, school.periods)" ><i></i>{{obj.period_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="lang_id" ><span translate="Field.Language" ></span></label>
                    <select class="select-form-control" ng-model="school.lang_id" ng-options="obj.id as obj.lang_name_ar for obj in languages" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="group_school_id" ><span translate="Field.DateSystem" ></span></label>
                    <select class="select-form-control" ng-model="school.date_system_id" ng-options="obj.id as obj.date_system_name_ar for obj in dates_system" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="levels_template_id" ><span translate="Field.LevelsTemplate" ></span></label>
                    <select class="select-form-control" ng-model="school.levels_template_id" ng-options="obj.id as obj.levels_template_name_ar for obj in levels_templates" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="courses_template_id" ><span translate="Field.CoursesTemplate" ></span></label>
                    <select class="select-form-control" ng-model="school.courses_template_id" ng-options="obj.id as obj.courses_template_name_ar for obj in courses_templates" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="courses_config_template_id" ><span translate="Field.CoursesConfigTemplate" ></span></label>
                    <select class="select-form-control" ng-model="school.courses_config_template_id" ng-options="obj.id as obj.courses_config_template_name_ar for obj in courses_config_templates" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                
                <div class="form-group" >
                    <label for="active" ><span translate="Field.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveDataAndBack" ></span>
                    </button>
                </span>


                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveData" ></span>
                    </button>
                </span>

                <a ui-sref="school.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>