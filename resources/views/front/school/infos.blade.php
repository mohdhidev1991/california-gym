<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="School.SchoolIdentityWithName" translate-values="{school_name: school.school_name_ar}" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="School.CantGetSchool" ng-show="can_get_data" ></div>


        <div class="panel panel-default" >
            <div class="panel-heading font-bold" translate="School.SchoolIdentityWithName" translate-values="{school_name: school.school_name_ar}" ></div>

            <div class="panel-body">
                <table class="table table-striped m-b-none">
                    <tbody>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.ID" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.id}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.GroupNum" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.group_num}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.SchoolType" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.school_type_name_ar}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Periods" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value multi_label_values" >
                                    <span ng-repeat="period in school.periods" >{{period.period_name_ar}}</span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.ArabicName" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.school_name_ar}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.EnglishName" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.school_name_en}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.GroupName" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.group_school_name_ar}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Genre" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.genre_name_ar}}</span>
                            </td>
                        </tr>


                        <tr class="tr_title_separator" >
                            <td class="col-md-12" colspan="4" >
                                <h4 translate="Field.Address" class="text-center" ></h4>
                            </td>
                        </tr>


                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Address" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.address}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.City" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.city_name_ar}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Quarter" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.quarter}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.PostalCode" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.pc}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.LocationMap" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" ><a ng-show="school.maps_location_url" href="{{school.maps_location_url}}" target="_blank" translate="Form.LocationLink" ></a></span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Country" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.country_name_ar}}</span>
                            </td>
                        </tr>



                        <tr class="tr_title_separator" >
                            <td class="col-md-12" colspan="4" >
                                <h4 translate="Field.Configuration" class="text-center" ></h4>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="School.AccountValidity" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.expiring_hdate}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.SchoolCapacity" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.scapacity}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.LevelsTemplate" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value multi_label_values" >
                                    <span ng-repeat="level in school.school_levels" >{{level.levels_template_name_ar}}</span>
                                </span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.CoursesTemplate" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.courses_template_name_ar}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.CoursesConfigTemplate" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.courses_config_template_name_ar}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Levels" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value multi_label_values" >
                                    <span ng-repeat="level in school.school_levels" >{{level.school_level_name_ar}}</span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Holidays" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.holiday_name_ar}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.Language" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.lang_name_ar}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.SP1" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.sp1}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="Field.SP2" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school.sp2}} %</span>
                            </td>
                        </tr>

                    </tbody>
                </table>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <h4 translate="School.OpenedSchoolYears" class="text-center" ></h4>
                <table class="table table-striped m-b-none">
                    <thead>
                        <tr>
                            <th class="col-md-2" >
                                <label class="label_info" translate="Field.SchoolYear" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="School.SchoolYearStartDate" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="School.SchoolYearEndDate" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="Field.SubscribeStartDate" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="Field.SubscribeEndDate" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="Form.See" ></label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="school_year in school.school_years" >
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_year.school_year_name_ar}}</span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_year.school_year_start_hdate}}</span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_year.school_year_end_hdate}}</span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_year.subscribe_start_hdate}}</span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_year.subscribe_end_hdate}}</span>
                            </td>
                            <td class="col-md-2" >
                                <a class="btn btn-icon-no-margin btn-icon btn-primary" ui-sref="school_year.edit({school_year_id: school_year.id})" ><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="line line-dashed b-b line-lg pull-in"></div>






                <h4 translate="Field.SchoolDepartments" class="text-center" ></h4>
                <table class="table table-striped m-b-none">
                    <thead>
                        <tr>
                            <th class="col-md-4" >
                                <label class="label_info" translate="Sdepartment.ShoolDepartmentName" ></label>
                            </th>
                            <th class="col-md-4" >
                                <label class="label_info" translate="Field.WeekTemplate" ></label>
                            </th>
                            <th class="col-md-4" >
                                <label class="label_info" translate="Form.See" ></label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="sdepartment in school.sdepartments" >
                            <td class="col-md-4" >
                                <span class="label_value" >{{sdepartment.sdepartment_name_ar}}</span>
                            </td>
                            <td class="col-md-4" >
                                <span class="label_value" >{{sdepartment.week_template_name_ar}}</span>
                            </td>
                            <td class="col-md-4" >
                                <a class="btn btn-icon-no-margin btn-icon btn-primary" ui-sref="sdepartment.edit({sdepartment_id: sdepartment.id})" ><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <h4 translate="Field.Employees" class="text-center" ></h4>
                <table class="table table-striped m-b-none">
                    <thead>
                        <tr>
                            <th class="col-md-3" >
                                <label class="label_info" translate="Field.JobDescription" ></label>
                            </th>
                            <th class="col-md-3" >
                                <label class="label_info" translate="Field.User" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="SchoolEmployee.JobTitle" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="Field.SchoolDepartment" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="Form.See" ></label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="school_employee in school.school_employees" >
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_employee.job_description}}</span>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_employee.firstname+' '+school_employee.lastname+' '+school_employee.f_firstname}}</span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value multi_label_values" >
                                    <span ng-repeat="school_job in school_employee.school_jobs" >{{school_job.school_job_name_ar}}</span>
                                </span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_employee.sdepartment_name_ar}}</span>
                            </td>
                            <td class="col-md-2" >
                                <a class="btn btn-icon-no-margin btn-icon btn-primary" ui-sref="school_emplyee.edit({school_employee_id: school_employee.id})" ><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>


            </div>

            <div class="panel-footer" >
                <a ui-sref="school.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>