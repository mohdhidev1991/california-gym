<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolClass.EditSchoolClass" translate-values="{school_class_id: school_class.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolClass.AddNewSchoolClass" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolClass.CantGetSchoolClass" ng-show="!school_class.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_class.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolClass.SchoolClassInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_class.id}" >
                      <label for="school_class_id" ><span translate="SchoolClass.SchoolClassID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_class_id" ng-model="school_class.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !school_class.symbol}" >
                  <label for="symbol" ><span translate="SchoolClass.SchoolClassSymbol" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="symbol" ng-model="school_class.symbol" class="form-control" placeholder="{{ 'SchoolClass.PleaseEnterTheSchoolClassSymbol' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group" ng-class="{'has-error': !school_class.level_class_id}" >
                    <label for="level_class_id" ><span translate="SchoolClass.SchoolLevelClass" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_class.level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" >
                        <option value="" >-- {{ "SchoolClass.SelectLevelClass" | translate }} --</option>
                    </select>
                </div>



                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group" ng-class="{'has-error': !school_class.school_year_id}" >
                    <label for="school_year_id" ><span translate="SchoolClass.SchoolYear" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_class.school_year_id" ng-options="obj.id as obj.school_year_name_ar for obj in school_years" >
                        <option value="" >-- {{ "SchoolClass.SelectSchoolYear" | translate }} --</option>
                    </select>
                </div>



                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group" ng-class="{'has-error': !school_class.room_id}" >
                    <label for="room_id" ><span translate="SchoolClass.Room" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_class.room_id" ng-options="obj.id as obj.room_name_ar for obj in rooms" >
                        <option value="" >-- {{ "SchoolClass.SelectRoom" | translate }} --</option>
                    </select>
                </div>



                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolClass.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolClass.AddNewSchoolClass" ></span>
                    </button>
                </span>

                <a ui-sref="school_class.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>