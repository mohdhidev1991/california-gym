<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="SchoolClassCourse.CourseMaterialsSettings"></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default">
            <div class="panel-heading" translate="SchoolClassCourse.CourseMaterialsSettings" ></div>

            <header class="panel-header with-padding">
                <div class="row">
                    <div class="col-md-1">
                        <label translate="Field.Template" ></label>
                    </div>
                    <div class="col-md-2">
                        <select class="select-form-control" ng-model="school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" ng-change="GetSchoolYears(); GetSchoolEmployees(); reset_datas(); " >
                            <option value="" >-- {{ "Form.SelectSchool" | translate }} --</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <label translate="School.SchoolYears" ></label>
                    </div>
                    <div class="col-md-2">
                        <select class="select-form-control" ng-model="school_year_id" ng-options="obj.id as obj.school_year_name_ar for obj in school_years" ng-change="GetSchoolLevels(); reset_datas();" ng-disabled="!school_id || !school_years" >
                            <option value="" >-- {{ "Form.SelectSchoolYear" | translate }} --</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <label translate="Field.SchoolLevel" ></label>
                    </div>
                    <div class="col-md-2">
                        <select class="select-form-control" ng-model="school_level_id" ng-options="obj.id as obj.school_level_name_ar for obj in school_levels" ng-change="GetLevelClasses(); reset_datas();" ng-disabled="!school_id || !school_levels" >
                            <option value="" >-- {{ "Form.SelectSchoolLevel" | translate }} --</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <label translate="Field.LevelClass" ></label>
                    </div>
                    <div class="col-md-2">
                        <select class="select-form-control" ng-model="level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" ng-changes="reset_datas();" ng-disabled="!school_level_id || !level_classes" >
                            <option value="" >-- {{ "Form.SelectLevelClass" | translate }} --</option>
                        </select>
                    </div>
                    <div class="col-md-12 text-center">
                        <button ng-click="get_school_classes_courses()" ng-disabled="!school_id || !school_year_id || !school_level_id || !level_class_id" ng-changed="changed_level_class_id()" class="btn btn-sm btn-primary btn-default" translate="Global.Manage" ></button>
                    </div>
                </div>
            </header>

            <form class="" name="form" >
                <div class="table-responsive" ng-show="school_classes_courses" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th>
                                    <label translate="Course.Name" ></label>
                                </th>
                                <th ng-repeat="symbol in symboles_keys" >
                                    <span>{{symboles[symbol].level_class_name_ar + ' - ' + symboles[symbol].symbol }}</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="course in courses_keys" >
                                <td>
                                    <span>{{courses[course].course_name_ar}}</span>
                                </td>
                                <td ng-repeat="symbol in symboles_keys" >
                                    <select class="select-form-control" ng-model="school_classes_courses[symbol][course].prof_id" ng-options="obj.id as obj.firstname+' '+obj.lastname+' '+obj.f_firstname for obj in school_classes_courses[symbol][course].profs" >
                                        <option value="" >-- {{ "School.SelectProf" | translate }} --</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </form>


            <footer class="panel-footer" ng-show= "school_classes_courses" >
                <div class="row">
                    <div class="col-md-12 text-center">

                        <div class="alert alert-success" ng-show="success_save" translate="Form.SuccessSaveData" aria-hidden="false" style="">Form.SuccessSaveData</div>
                        <div class="alert alert-danger" ng-show="error_save" translate="Form.ErrorSaveData" aria-hidden="true">Form.ErrorSaveData</div>
                        
                        <div>
                            <button ng-click="save()" class="btn btn-sm btn-primary btn-default" ><i class="fa fa-floppy-o" ></i><span translate="Global.SaveChanges" ></span></button>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>



