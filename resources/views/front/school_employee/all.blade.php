<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Administration.ManageSchoolEmployees" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="SchoolEmployee.AllSchoolEmployees" ></div>
            <div class="panel-header panel-header with-padding">
                <div class="header-tools header-btns" >
                    <a class="btn btn-default" ui-sref="school_employee.add({add: true})" ><i class="fa fa-plus" ></i><span translate="SchoolEmployee.AddNewSchoolEmployee" ></span></a>
                    <a class="btn btn-default" ui-sref="school_employee.replace()" ><i class="fa fa-exchange" ></i><span translate="SchoolEmployee.ReplaceEmployee" ></span></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:20px;" >
                                    <label translate="Field.Active" ></label>
                                </th>
                                <th style="width:20px;" >
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th>
                                    <label translate="SchoolEmployee.SchoolEmployeeName" ></label>
                                </th>
                                <th>
                                    <label translate="SchoolEmployee.JobDescription" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="school_employee in school_employees" >
                                <td>
                                    <span >
                                        <i ng-show="school_employee.active==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="school_employee.active==='N'" class="fa fa-times text-danger text"></i>
                                    </span>
                                </td>
                                <td>
                                    <span>{{school_employee.id}}</span>
                                </td>
                                <td>
                                    <span>{{school_employee.firstname}} {{school_employee.f_firstname}} {{school_employee.lastname}}</span>
                                </td>
                                <td>
                                    <span>{{school_employee.job_description}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" ui-sref="school_employee.edit({school_employee_id: school_employee.id})" ><i class="fa fa-edit"></i><span translate="Form.Edit" ></span></a>
                                    <a class="btn btn-xs btn-primary" ui-sref="school_employee.schedule({school_employee_id: school_employee.id})" ><i class="fa fa-calendar"></i><span translate="SchoolEmployee.ScheduleOfTheWeek" ></span></a>
                                    <!--<a class="btn btn-xs btn-danger" ng-click="delete_school_employee(school_employee.id)" ><i class="fa fa-remove"></i><span translate="Form.Delete" ></span></a>-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetSchoolEmployees()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'SchoolEmployee.AreYouSureToDeleteThisSchoolEmployee' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>