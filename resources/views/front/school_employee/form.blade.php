<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolEmployee.EditSchoolEmployee" translate-values="{school_employee_id: school_employee.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolEmployee.AddNewSchoolEmployee" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolEmployee.CantGetSchoolEmployee" ng-show="error_get" ></div>


        <div class="panel panel-default" ng-show="school_employee.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolEmployee.EditSchoolEmployee" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_employee.id}" >
                      <label for="school_employee_id" ><span translate="Field.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_employee_id" ng-model="school_employee.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div ng-show="action==='add'" >
                    <div class="form-group" >
                        <label for="sdepartment_id" ><span translate="SchoolEmployee.Schooldepartment" ></span></label>
                        <select class="select-form-control" ng-required="true" ng-model="school_employee.sdepartment_id" ng-options="obj.id as obj.sdepartment_name_ar for obj in sdepartments" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !school_employee.firstname}" >
                  <label for="firstname" ><span translate="Field.FirstName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="firstname" ng-model="school_employee.firstname" class="form-control" placeholder="*" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_employee.lastname}" >
                  <label for="lastname" ><span translate="Field.LastName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lastname" ng-model="school_employee.lastname" class="form-control" placeholder="*" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="f_firstname" ><span translate="Field.FamilyName" ></span></label>
                  <input type="text" id="f_firstname" ng-model="school_employee.f_firstname" class="form-control" placeholder="*" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group" >
                  <label for="g_f_firstname" ><span translate="Field.GrandfatherFirstName" ></span></label>
                  <input type="text" id="g_f_firstname" ng-model="school_employee.g_f_firstname" class="form-control" placeholder="*" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="birth_date" ><span translate="Field.Birthday" ></span></label>
                  <islamic-datepicker id="expiring_date" ng-model="school_employee.birth_date" ng-model-hdate="school_employee.birth_date" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="gender_id" ><span translate="Field.Genre" ></span></label>
                    <select class="select-form-control" ng-model="school_employee.gender_id" ng-options="obj.id as obj.genre_name_ar for obj in genres" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="address" ><span translate="Field.Address" ></span></label>
                  <input type="text" id="address" ng-model="school_employee.address" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                    <label for="country_id" ><span translate="Field.Country" ></span></label>
                    <select class="select-form-control"  ng-change="GetCities()" ng-model="school_employee.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="city_id" ><span translate="Field.City" ></span></label>
                    <select class="select-form-control" ng-disabled="!school_employee.country_id || school_employee.country_id===''" ng-model="school_employee.city_id" ng-options="obj.id as obj.city_name_ar for obj in cities" >
                        <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="mobile" ><span translate="Field.Mobile" ></span></label>
                  <input type="text" id="mobile" ng-model="school_employee.mobile" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                


                <div class="form-group" >
                  <label for="phone" ><span translate="Field.Phone" ></span></label>
                  <input type="text" id="phone" ng-model="school_employee.phone" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="email" ><span translate="Field.Email" ></span></label>
                  <input type="email" id="email" ng-model="school_employee.email" class="form-control" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>





                <div class="form-group" >
                  <label for="job_description" ><span translate="Field.JobDescription" ></span></label>
                  <textarea type="text" id="job_description" ng-model="school_employee.job_description" class="form-control"></textarea>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="jobs" ><span translate="SchoolEmployee.JobType" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in school_jobs" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, school_employee.school_jobs)" ng-click="toggle_checkbox_option(obj.id, school_employee.school_jobs)" ><i></i>{{obj.school_job_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="courses" ><span translate="Field.Courses" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in courses" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, school_employee.courses)" ng-click="toggle_checkbox_option(obj.id, school_employee.courses)" ><i></i>{{obj.course_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="wdays" ><span translate="Field.WeekDays" ></span></label>
                    <div class="checkbox">
                        <label class="i-checks" ng-repeat="obj in wdays" >
                            <input type="checkbox" value="{{obj.id}}" ng-checked="check_selected_option(obj.id, school_employee.wdays)" ng-click="toggle_checkbox_option(obj.id, school_employee.wdays)" ><i></i>{{obj.wday_name_ar}}
                        </label>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
              </form>

              <div class="alert" ng-class="{'alert-danger': alerts.type==='error', 'alert-success': alerts.type==='success'}" ng-show="alerts" >
                <span ng-show="alerts.messages.length===1" >{{alerts.messages[0] | translate}}</span>
                <ul ng-show="alerts.messages.length > 1" >
                    <li ng-repeat="alert in alerts.messages track by $index" ><span>{{alert | translate}}</span></li>
                </ul>
              </div>

            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFilInlAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFilInlAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Form.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFilInlAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolEmployee.AddNewSchoolEmployee" ></span>
                    </button>
                </span>

                <a ui-sref="school_employee.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>