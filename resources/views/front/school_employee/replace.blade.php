<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="SchoolEmployee.ReplaceEmployee" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="alert alert-success" ng-show="success_replace_employee" >{{ 'SchoolEmployee.HandingOverTasksToAnotherSchoolEmployeeWithSuccess' | translate }}</div>

        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="SchoolEmployee.AllSchoolEmployees" ></div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:20px;" >
                                    <label translate="Field.ID" ></label>
                                </th>
                                <th>
                                    <label translate="Field.EmployeeName" ></label>
                                </th>
                                <th>
                                    <label translate="Field.JobDescription" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="school_employee in school_employees" >
                                <td>
                                    <span>{{school_employee.id}}</span>
                                </td>
                                <td>
                                    <span>{{school_employee.firstname}} {{school_employee.f_firstname}} {{school_employee.lastname}}</span>
                                </td>
                                <td>
                                    <span>{{school_employee.job_description}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" ng-click="replace_employee(school_employee)" ><i class="fa fa-exchange"></i><span translate="SchoolEmployee.HandingOverTasksToAnotherSchoolEmployee" ></span></a>
                                    <a class="btn btn-xs btn-primary" ui-sref="school_employee.edit({school_employee_id: school_employee.id})" ><i class="fa fa-edit"></i><span translate="Form.Edit" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetSchoolEmployees()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" max-size="5" ></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalReplaceEmployee.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="SchoolEmployee.HandingOverTasksToAnotherSchoolEmployee" ></h3>
    </div>
    <div class="modal-body">
        <div class="alert alert-info"><span translate="SchoolEmployee.EmployeeWillBeReplacedWithAnotherStaffMemberHaveTheSameDaysWorkAndTheSameCoursesIfTheFirstWasATeacher" ></span></div>
        <div class="modal-body-form form-group" >
                <label for="school_employee" ><span translate="SchoolEmployee.CurrentSchoolEmployee" ></span></label>
                <input class="form-control" ng-disabled="true" value="{{school_employee.firstname + ' ' +school_employee.f_firstname+' '+school_employee.lastname}}" />
        </div>
        <div class="form-group"  >
            <label for="school_employee_to_replace_name_or_idn" ><span translate="SchoolEmployee.SubstituteSchoolEmployee" ></span></label>
            <input type="text" ng-model="school_employee_to_replace" uib-typeahead="obj as obj.fullname for obj in GetSchoolEmployees($viewValue)" typeahead-loading="loadingShoolEmployee" typeahead-no-results="noShoolEmployee" class="form-control" placeholder="{{ 'Form.EnterTeacherIdnOrNameOrMobileOrSchoolJob' | translate }}" >
            <i ng-show="loadingShoolEmployee" class="glyphicon glyphicon-refresh"></i>
            <div ng-show="noShoolEmployee">
              <i class="glyphicon glyphicon-remove"></i> <span translate="Global.NoResultsFound" ></span>
            </div>
            <div class="form-group" ng-show="school_employee_to_replace.id" >
                <pre>{{school_employee_to_replace.fullname}} <span ng-show="school_employee_to_replace.job_description">[{{school_employee_to_replace.job_description}}]</span></pre>
            </div>
        </div>

        <div class="form-group form-group-checks" >
            <label class="i-checks m-t-xs m-r">
              <input type="checkbox" id="cancel_employee_account" ng-change="" ng-model="cancel_employee_account" >
              <i></i>
            </label>
            <label for="cancel_employee_account" ><span translate="SchoolEmployee.CancelCurrentEmployeeAccount" ></span></label>
        </div>

        <div class="alert alert-danger" ng-show="errors" >
            <ul>
                <li ng-repeat="error in errors" ><span translate="{{error}}" ></span></li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-disabled="!school_employee_to_replace.id || !school_employee" ng-click="submit()" translate="SchoolEmployee.ReplaceEmployee" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Form.Cancel" ></button>
    </div>
</script>