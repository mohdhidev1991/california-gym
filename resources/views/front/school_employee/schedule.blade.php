<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Global.Dashboard" ng-show="!school_employee_id" ></h1>
        <h1 class="m-n font-bold h3" translate="SchoolEmployee.ScheduleOfTheWeek" ng-show="school_employee_id" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="alert alert-danger" translate="SchoolEmployee.CantGetSchoolEmployee" ng-show="error_get"></div>
        
        <div class="panel panel-default" ng-show="school_employee">
            <div class="panel-heading font-bold" translate="SchoolEmployee.EmployeeInformations"></div>
            <div class="panel-body">
                <div class="wrapper-md">
                    <table class="table table-striped m-b-none">
                        <tbody>
                            <tr>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="Field.ID"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value">{{school_employee.id}}</span>
                                </td>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="SchoolEmployee.JobDescription"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value">{{school_employee.job_description}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="SchoolEmployee.SchoolEmployeeFullName"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value">{{school_employee.fullname}}</span>
                                </td>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="SchoolEmployee.JobsAssigned"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value multi_label_values">
                                        <span ng-repeat="id_ in school_employee.school_jobs" >{{id_ | filter_name_by_id:school_jobs:'school_job_name_ar'}}</span>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="Field.School"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value">{{school_employee.school_name_ar}}</span>
                                </td>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="Field.Department"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value multi_label_values">
                                        <span ng-repeat="obj in school_employee.sdepartments" >{{obj.sdepartment_name_ar}}</span>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="SchoolEmployee.CoursesBeingStudiedBy"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value multi_label_values">
                                        <span ng-repeat="id_ in school_employee.courses" >{{id_ | filter_name_by_id:courses:'course_name_ar'}}</span>
                                    </span>
                                </td>
                                <td class="col-md-3">
                                    <label class="label_info font-bold" translate="Field.WorkingDays"></label>
                                </td>
                                <td class="col-md-3">
                                    <span class="label_value multi_label_values">
                                        <span ng-repeat="id_ in school_employee.wdays" >{{id_ | filter_name_by_id:wdays:'wday_name_ar'}}</span>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div><!-- END panel-body -->
                <div class="panel-footer" ng-show="school_employee_id">
                    <a ui-sref="school_employee.all()" class="btn btn-sm btn-default">
                        <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back"></span>
                    </a>
                </div>
        </div><!-- END panel -->





        <div class="panel panel-default" ng-show="school_employee">
            <div class="panel-heading font-bold" translate="SchoolEmployee.ClassesAndCourses"></div>
            <div class="panel-body">
                <div class="wrapper-md">
                    <table class="table table-striped m-b-none">
                        <tbody>
                            <thead>
                                <!--<th class="col-md-3">
                                    <label class="label_info" translate="Field.SchoolYear"></label>
                                </th>-->
                                <th class="col-md-3">
                                    <label class="label_info font-bold" translate="Field.LevelClass"></label>
                                </th>
                                <th class="col-md-3">
                                    <label class="label_info font-bold" translate="Field.Symbol"></label>
                                </th>
                                <th class="col-md-3">
                                    <label class="label_info font-bold" translate="Field.Course"></label>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="school_class_course in school_employee.school_class_courses">
                                    <!--<td class="col-md-3">
                                        <span class="label_value">{{school_class_course.school_year_name_ar}}</span>
                                    </td>-->
                                    <td class="col-md-3">
                                        <span class="label_value">{{school_class_course.level_class_name_ar}}</span>
                                    </td>
                                    <td class="col-md-3">
                                        <span class="label_value">{{school_class_course.symbol | translate_symbol}}</span>
                                    </td>
                                    <td class="col-md-3">
                                        <span class="label_value">{{school_class_course.course_name_ar}}</span>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
            </div>
            <!-- END panel-body -->
        </div><!-- END panel -->



        <div class="panel panel-default" ng-show="school_employee">
            <div class="panel-heading font-bold" translate="SchoolEmployee.ScheduleOfTheWeek"></div>
            <div class="panel-body">
                <div class="wrapper-md">
                    <div class="alert alert-danger" ng-show="school_employee.schedules_errors" >
                        <ul class="" >
                            <li ng-repeat="error in school_employee.schedules_errors" >
                                <span>{{error | translate}}</span>
                            </li>
                        </ul>
                    </div>
                    <table class="table table-striped m-b-none table-bordered" ng-show="school_employee.schedules" >
                        <tbody>
                            <thead>
                                <th>
                                    <span class="label_value" translate="CourseSession.Order"></span>
                                </th>
                                <th>
                                    <span class="label_value" translate="CourseSession.Start"></span>
                                </th>
                                <th>
                                    <span class="label_value" translate="CourseSession.End"></span>
                                </th>
                                <th ng-repeat="wday in wdays">
                                    <span class="label_value" >{{wday.wday_name_ar}}</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="schedule in school_employee.schedules">
                                    <td>
                                        <span class="label_value">{{schedule.session_order}}</span>
                                    </td>
                                    <td>
                                        <span class="label_value">{{schedule.session_start_time}}</span>
                                    </td>
                                    <td>
                                        <span class="label_value">{{schedule.session_end_time}}</span>
                                    </td>
                                    <td ng-repeat="wday in wdays" class="td-level-class-{{schedule['level_class_id_'+wday.id]}}" >
                                        <div ng-show="schedule['psi_'+wday.id]">
                                            <div><span class="label_value">{{schedule['course_id_'+wday.id] | filter_name_by_id:courses:'course_name_ar'}}</span></div>
                                            <div>
                                                <span class="label_value level-class-{{schedule['level_class_id_'+wday.id]}}">{{schedule['level_class_id_'+wday.id] | filter_name_by_id:level_classes:'level_class_name_ar'}}</span>
                                                <span class="label_value symbol-{{schedule['symbol_'+wday.id]}}">{{schedule['symbol_'+wday.id] | translate_symbol}}</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
            </div>
        </div><!-- END panel -->


    </div>
</div>