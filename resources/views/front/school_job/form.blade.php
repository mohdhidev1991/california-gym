<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolJob.EditSchoolJob" translate-values="{school_job_id: school_job.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolJob.AddNewSchoolJob" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolJob.CantGetSchoolJob" ng-show="!school_job.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_job.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolJob.SchoolJobInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_job.id}" >
                      <label for="school_job_id" ><span translate="SchoolJob.SchoolJobID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_job_id" ng-model="school_job.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !school_job.school_job_name_ar}" >
                  <label for="school_job_name_ar" ><span translate="SchoolJob.SchoolJobNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_job_name_ar" ng-model="school_job.school_job_name_ar" class="form-control" placeholder="{{ 'SchoolJob.PleaseEnterTheSchoolJobNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_job.school_job_name_en}" >
                  <label for="school_job_name_en" ><span translate="SchoolJob.SchoolJobNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_job_name_en" ng-model="school_job.school_job_name_en" class="form-control" placeholder="{{ 'SchoolJob.PleaseEnterTheSchoolJobNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !school_job.id_domain}" >
                  <label for="id_domain" ><span translate="SchoolJob.SchoolJobIdDomain" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="id_domain" ng-model="school_job.id_domain" class="form-control" placeholder="{{ 'SchoolJob.PleaseEnterTheSchoolJobIdDomain' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolJob.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolJob.AddNewSchoolJob" ></span>
                    </button>
                </span>

                <a ui-sref="school_job.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>