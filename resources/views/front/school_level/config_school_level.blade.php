<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="SchoolLevel.ManagementSchoolWorkDetails" ></h1>
    </div>
    <div class="wrapper-md">

        <div class="panel panel-default">
            <div class="panel-heading" translate="SchoolLevel.ManagementSchoolWorkDetails" ></div>
            <header class="panel-header with-padding">
                <div class="row">
                    <div class="col-md-2">
                        <label translate="SchoolLevel.School" ></label>
                    </div>
                    <div class="col-md-3">
                        <select class="select-form-control" ng-model="selected_school" ng-change="GetSchoolYears(); GetSchoolSdepartment();" ng-disabled="config_mode" >
                            <option value="" >-- {{ "SchoolLevel.SelectSchool" | translate }} --</option>
                            <option ng-repeat="school in schools" value="{{school.id}}" >{{school.school_name_ar}}</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label translate="SchoolLevel.SchoolYear" ></label>
                    </div>
                    <div class="col-md-3">
                        <select class="select-form-control" ng-model="selected_school_year" ng-disabled="!selected_school || !school_years || config_mode" >
                            <option value="" >-- {{ "SchoolLevel.SelectSchoolYear" | translate }} --</option>
                            <option ng-repeat="year in school_years" value="{{year.id}}" >{{year.school_year_name_ar}}</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button ng-click="config_school_level()" ng-disabled="!selected_school || !selected_school_year || config_mode" class="btn btn-sm btn-primary btn-block" translate="Global.Modify" ></button>
                    </div>
                </div>
            </header>
            <form name="form" ng-show="config_mode" >
                <div class="panel-body" >
                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                            <thead>
                                <tr>
                                    <th style="width:20px;">
                                        <label translate="Global.ID" ></label>
                                    </th>
                                    <th>
                                        <label translate="SchoolLevel.Name" ></label>
                                    </th>
                                    <th>
                                        <label translate="LevelClass.Name" ></label>
                                    </th>
                                    <th>
                                        <label translate="Sdepartment.Name" ></label>
                                    </th>
                                    <th>
                                        <label translate="LevelClass.ClassNb" ></label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item_config in school_levels_config" >
                                    <td>
                                        <input class="form-control" ng-required="true" ng-model="item_config.id" ng-disabled="true" />
                                    </td>
                                    <td>
                                        <input class="form-control" ng-model="item_config.school_level_name_ar" ng-disabled="true" />
                                    </td>
                                    <td>
                                        <input class="form-control" ng-model="item_config.level_class_name_ar" ng-disabled="true" />
                                    </td>
                                    <td>
                                        <select class="select-form-control" ng-model="item_config.sdepartment_id" ng-options="obj.id as obj.sdepartment_name_ar for obj in sdepartments" >
                                            <option value="" >-- {{ "School.SelectSchoolSdepartment" | translate }} --</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" ng-pattern="/^[0-9]{1,7}$/" class="form-control" ng-required="true" ng-model="item_config.class_nb" />
                                    </td>   
                            </tbody>
                        </table>
                    </div>
                </div>
                <footer class="panel-footer" >
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="alert alert-success" ng-show="success_save" translate="Form.SuccessSaveData" ></div>
                            <div class="alert alert-success" ng-show="error_save" translate="Form.ErrorSaveData" ></div>
                            
                            <span uib-tooltip="Form.PleaseFillAllRequiredFields" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" aria-hidden="false" class="">
                                <button ng-click="save()" ng-disabled="!form.$valid" class="btn btn-sm btn-primary" translate="Global.Save" >
                                    <i class="fa fa-floppy-o"></i><span translate="Global.SaveData" ></span>
                                </button>
                            </span>

                            <button ng-click="cancel_config_school_level()" class="btn btn-sm btn-default" translate="Global.Cancel" ></button>

                        </div>
                    </div>
                </footer>
            </form>
        </div>

    </div>
</div>