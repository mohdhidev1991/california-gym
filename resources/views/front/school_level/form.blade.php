<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolLevel.EditSchoolLevel" translate-values="{school_level_id: school_level.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolLevel.AddNewSchoolLevel" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolLevel.CantGetSchoolLevel" ng-show="!school_level.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_level.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolLevel.SchoolLevelInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_level.id}" >
                      <label for="school_level_id" ><span translate="SchoolLevel.SchoolLevelID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_level_id" ng-model="school_level.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !school_level.school_level_name_ar}" >
                  <label for="school_level_name_ar" ><span translate="SchoolLevel.SchoolLevelNameAR" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_level_name_ar" ng-model="school_level.school_level_name_ar" class="form-control" placeholder="{{ 'SchoolLevel.PleaseEnterTheSchoolLevelNameAR' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_level.school_level_name_en}" >
                  <label for="school_level_name_en" ><span translate="SchoolLevel.SchoolLevelNameEN" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_level_name_en" ng-model="school_level.school_level_name_en" class="form-control" placeholder="{{ 'SchoolLevel.PleaseEnterTheSchoolLevelNameEN' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group" ng-class="{'has-error': !school_level.levels_template_id}" >
                    <label for="levels_template_id" ><span translate="SchoolLevel.LevelsTemplate" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_level.levels_template_id" ng-options="obj.id as obj.levels_template_name_ar for obj in levels_templates" >
                        <option value="" >-- {{ "School.SelectLevelsTemplate" | translate }} --</option>
                    </select>
                </div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolLevel.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolLevel.AddNewSchoolLevel" ></span>
                    </button>
                </span>

                <a ui-sref="school_level.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>