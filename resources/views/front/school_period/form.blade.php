<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolPeriod.EditSchoolPeriod" translate-values="{school_period_id: school_period.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolPeriod.AddNewSchoolPeriod" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolPeriod.CantGetSchoolPeriod" ng-show="!school_period.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_period.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolPeriod.SchoolPeriodInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_period.id}" >
                      <label for="school_period_id" ><span translate="SchoolPeriod.SchoolPeriodID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_period_id" ng-model="school_period.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !school_period.period_name_ar}" >
                  <label for="period_name_ar" ><span translate="SchoolPeriod.SchoolPeriodNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="period_name_ar" ng-model="school_period.period_name_ar" class="form-control" placeholder="{{ 'SchoolPeriod.PleaseEnterTheSchoolPeriodNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_period.period_name_en}" >
                  <label for="period_name_en" ><span translate="SchoolPeriod.SchoolPeriodNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="period_name_en" ng-model="school_period.period_name_en" class="form-control" placeholder="{{ 'SchoolPeriod.PleaseEnterTheSchoolPeriodNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_period.capacity}" >
                  <label for="capacity" ><span translate="SchoolPeriod.SchoolPeriodCapacity" ></span><sup class="required" >*</sup></label>
                  <input type="number" ng-required="true" required="" id="capacity" ng-pattern="/^[0-9]{1,7}$/" ng-model="school_period.capacity" class="form-control" placeholder="{{ 'SchoolPeriod.PleaseEnterTheSchoolPeriodCapacity' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" ng-class="{'has-error': !school_period.school_id}" >
                    <label for="school_id" ><span translate="SchoolPeriod.School" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_period.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "School.SelectSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_period.period_id}" >
                    <label for="period_id" ><span translate="SchoolPeriod.Period" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_period.period_id" ng-options="obj.id as obj.period_name_ar for obj in periods" >
                        <option value="" >-- {{ "School.SelectPeriod" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                    <label for="timer_door_open_time" ><span translate="SchoolPeriod.DoorOpenTime" ></span></label>
                    <uib-timepicker ng-model="timer_door_open_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="timer_door_close_time" ><span translate="SchoolPeriod.DoorCloseTime" ></span></label>
                    <uib-timepicker ng-model="timer_door_close_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="timer_end_coming_time" ><span translate="SchoolPeriod.DoorComingTime" ></span></label>
                    <uib-timepicker ng-model="timer_end_coming_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="timer_start_exit_time" ><span translate="SchoolPeriod.DoorExitTime" ></span></label>
                    <uib-timepicker ng-model="timer_start_exit_time" minute-step="1" hour-step="1" show-meridian="false"></uib-timepicker>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolPeriod.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolPeriod.AddNewSchoolPeriod" ></span>
                    </button>
                </span>

                <a ui-sref="school_period.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>