<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolRegistration.EditSchoolRegistration" translate-values="{school_registration_id: school_registration.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolRegistration.AddNewSchoolRegistration" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolRegistration.CantGetSchoolRegistration" ng-show="!school_registration.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_registration.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolRegistration.Infos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_registration.id}" >
                      <label for="school_registration_id" ><span translate="SchoolRegistration.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_registration_id" ng-model="school_registration.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                  <label for="school_id" ><span translate="SchoolRegistration.SchoolID" ></span></label>
                  <input type="text" id="school_id" ng-model="school_registration.school_id" class="form-control" placeholder="{{ 'SchoolRegistration.PleaseEnterTheSchoolId' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-show="school_registration.school_name_ar" >
                  <label for="school_name" ><span translate="SchoolRegistration.School" ></span></label>
                  <input type="text" id="school_name" ng-disabled="true" ng-model="school_registration.school_name_ar" class="form-control" placeholder="{{ 'SchoolRegistration.PleaseEnterTheSchoolName' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                  <label for="firstname" ><span translate="SchoolRegistration.FirstName" ></span></label>
                  <input type="text" id="firstname" ng-model="school_registration.firstname" class="form-control" placeholder="{{ 'SchoolRegistration.PleaseEnterTheFirstName' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                  <label for="lastname" ><span translate="SchoolRegistration.LastName" ></span></label>
                  <input type="text" id="lastname" ng-model="school_registration.lastname" class="form-control" placeholder="{{ 'SchoolRegistration.PleaseEnterTheLastName' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="email" ><span translate="SchoolRegistration.Email" ></span></label>
                  <input type="email" id="email" ng-model="school_registration.email" class="form-control" placeholder="{{ 'SchoolRegistration.PleaseEnterTheEmail' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="mobile" ><span translate="SchoolRegistration.Mobile" ></span></label>
                  <input type="text" id="mobile" ng-model="school_registration.mobile" class="form-control" placeholder="{{ 'SchoolRegistration.PleaseEnterTheMobile' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="city_id" ><span translate="SchoolRegistration.City" ></span></label>
                    <select class="select-form-control" ng-model="school_registration.city_id" ng-options="obj.id as obj.city_name_ar for obj in cities" >
                        <option value="" >-- {{ "School.SelectCity" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolRegistration.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
              </form>
            </div>


            <div class="panel-footer" >

                <div ng-show="success_convert_to_school" class="alert alert-success" translate="SchoolRegistration.SuccessConvert" ></div>
                <div ng-show="errors_convert_to_school" class="alert alert-danger" >
                    <ul>
                        <li ng-repeat="error in errors_convert_to_school" >{{ error | translate }}</li>
                    </ul>
                </div>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled=" (!form.$valid)"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolRegistration.AddNewSchoolRegistration" ></span>
                    </button>
                </span>


                <span ng-show="action==='edit'" uib-tooltip="{{ 'SchoolRegistration.ThisSubmitAlreadyConverted' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="school_registration.school_id" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="school_registration.school_id"
                        type="submit" class="btn btn-sm btn-default" ng-click="convert_to_school()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolRegistration.AddNewSchoolRegistration" ></span>
                    </button>
                </span>

                <a ui-sref="school_registration.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>