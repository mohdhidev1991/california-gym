@extends('layouts.app_no_container_js')

@section('js_footer')
    @parent
    <script src="{!! URL::asset("themes/admin/assets/js/controllers/school/register_school.js") !!}"></script>
@endsection

@section('content')
<div ng-controller="RegisterSchoolFormController" >

    <div class="wrapper text-center">
      <strong translate="SchoolRegistration.RegisterYourSchoolNow" ></strong>
    </div>


    <form class="form-school-registration" name="form" role="form" ng-hide="success_school_registration" >
        {!! csrf_field() !!}

        <div class="list-group list-group-sm">
            
            <div class="list-group-item" ng-class="{'has-error': !school_registration.school_name}" >
                <input ng-model="school_registration.school_name" ng-required="true" type="text" class="form-control no-border" placeholder="{{ 'Field.SchoolName' | translate }} *" >
            </div>

            <div class="list-group-item" ng-class="{'has-error': !school_registration.firstname}" >
                <input ng-model="school_registration.firstname" ng-required="true" type="text" class="form-control no-border" placeholder="{{ 'SchoolRegistration.AdministratorOrDirectorFirstName' | translate }} *" >
            </div>

            <div class="list-group-item" ng-class="{'has-error': !school_registration.lastname}" >
                <input ng-model="school_registration.lastname" ng-required="true" type="text" class="form-control no-border" placeholder="{{ 'SchoolRegistration.AdministratorOrDirectorLastName' | translate }} *" >
            </div>

            <div class="list-group-item" ng-class="{'has-error': !school_registration.email}" >
                <input ng-model="school_registration.email" ng-required="true" type="email" class="form-control no-border" placeholder="{{ 'Field.Email' | translate }} *" >
            </div>

            <div class="list-group-item" ng-class="{'has-error': !school_registration.mobile}" >
                <input ng-model="school_registration.mobile" ng-required="true" type="text" class="form-control no-border" placeholder="{{ 'Field.Mobile' | translate }} *" >
            </div>

            <div class="list-group-item" ng-class="{'has-error': !school_registration.city}" >
                <input ng-model="school_registration.city" ng-required="true" type="text" class="form-control no-border" placeholder="{{ 'Field.City' | translate }} *" >
            </div>


        </div>

        <div class="list-group" >
            <div
                vc-recaptcha
                theme="'light'"
                key="model.key"
                on-create="setWidgetId(widgetId)"
                on-success="setResponse(response)"
                on-expire="cbExpiration()"
            ></div>
        </div>


        <div class="alert alert-danger" ng-show="failed_school_registration || failed_recaptcha" >
            <span translate="User.PleaseValideYourAreNotARobot" ng-show="failed_recaptcha" ></span>
            <ul ng-show="errors" >
                <li ng-repeat="error in errors" >
                    <span ng-repeat="item_error in error" >
                        {{item_error}}<br>
                    </span>
                </li>
            </ul>
        </div>

        <div class="list-group" >
            <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="register_school()" ng-disabled="!form.$valid" translate="SchoolRegistration.RequestSchoolRegistration" ></button>
            </span>
        </div>
        <div class="line line-dashed"></div>


        
    </form>

    <div class="alert alert-success" ng-show="success_school_registration" >
        <span translate="SchoolRegistration.SuccessSchoolRegistrationWeWillContactYouASAP" ></span>
    </div>

    <div class="form-group">
        <a href="{!! url('/') !!}" class="btn btn-lg btn-default btn-block" >
            <i class="fa fa-btn fa-rea_user"></i>
            <span translate="Global.BackToHome" ></span>
        </a>
    </div>
</div>
@endsection
