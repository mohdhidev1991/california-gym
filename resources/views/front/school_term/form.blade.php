<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolTerm.EditSchoolTerm" translate-values="{school_term_id: school_term.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolTerm.AddNewSchoolTerm" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolTerm.CantGetSchoolTerm" ng-show="!school_term.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_term.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolTerm.SchoolTermInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_term.id}" >
                      <label for="school_term_id" ><span translate="SchoolTerm.SchoolTermID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_term_id" ng-model="school_term.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !school_term.school_term_name_ar}" >
                  <label for="school_term_name_ar" ><span translate="SchoolTerm.SchoolTermNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_term_name_ar" ng-model="school_term.school_term_name_ar" class="form-control" placeholder="{{ 'SchoolTerm.PleaseEnterTheSchoolTermNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_term.school_term_name_en}" >
                  <label for="school_term_name_en" ><span translate="SchoolTerm.SchoolTermNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_term_name_en" ng-model="school_term.school_term_name_en" class="form-control" placeholder="{{ 'SchoolTerm.PleaseEnterTheSchoolTermNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !school_term.start_study_hdate}" >
                  <label for="start_study_hdate" ><span translate="SchoolTerm.StartStudyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="start_study_hdate" ng-model="school_term.start_study_hdate" class="form-control" placeholder="{{ 'SchoolTerm.PleaseEnterTheStartStudyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_term.end_study_hdate}" >
                  <label for="end_study_hdate" ><span translate="SchoolTerm.EndStudyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="end_study_hdate" ng-model="school_term.end_study_hdate" class="form-control" placeholder="{{ 'SchoolTerm.PleaseEnterTheEndStudyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !school_term.start_vacancy_hdate}" >
                  <label for="start_vacancy_hdate" ><span translate="SchoolTerm.StartVacancyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="start_vacancy_hdate" ng-model="school_term.start_vacancy_hdate" class="form-control" placeholder="{{ 'SchoolTerm.PleaseEnterTheStartVacancyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_term.end_vacancy_hdate}" >
                  <label for="end_vacancy_hdate" ><span translate="SchoolTerm.EndVacancyHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="end_vacancy_hdate" ng-model="school_term.end_vacancy_hdate" class="form-control" placeholder="{{ 'SchoolTerm.PleaseEnterTheEndVacancyHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group" ng-class="{'has-error': !school_term.school_id}" >
                    <label for="school_id" ><span translate="SchoolTerm.School" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_term.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "School.SelectSchool" | translate }} --</option>
                    </select>
                </div>


                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolTerm.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolTerm.AddNewSchoolTerm" ></span>
                    </button>
                </span>

                <a ui-sref="school_term.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>