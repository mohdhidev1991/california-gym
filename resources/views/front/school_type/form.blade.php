<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolType.EditSchoolType" translate-values="{school_type_id: school_type.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolType.AddNewSchoolType" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolType.CantGetSchoolType" ng-show="!school_type.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_type.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolType.SchoolTypeInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_type.id}" >
                      <label for="school_type_id" ><span translate="SchoolType.SchoolTypeID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_type_id" ng-model="school_type.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !school_type.lookup_code}" >
                  <label for="lookup_code" ><span translate="SchoolType.SchoolTypeLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="school_type.lookup_code" class="form-control" placeholder="{{ 'SchoolType.PleaseEnterTheSchoolTypeLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_type.school_type_name_ar}" >
                  <label for="school_type_name_ar" ><span translate="SchoolType.SchoolTypeNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_type_name_ar" ng-model="school_type.school_type_name_ar" class="form-control" placeholder="{{ 'SchoolType.PleaseEnterTheSchoolTypeNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_type.school_type_name_en}" >
                  <label for="school_type_name_en" ><span translate="SchoolType.SchoolTypeNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_type_name_en" ng-model="school_type.school_type_name_en" class="form-control" placeholder="{{ 'SchoolType.PleaseEnterTheSchoolTypeNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !school_type.tservice_id}" >
                    <label for="date_system_id" ><span translate="SchoolType.SchoolTypeTservice" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_type.tservice_id" ng-options="obj.id as obj.tservice_name_ar for obj in tservices" >
                        <option value="" >-- {{ "School.SelectTheTservice" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolType.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolType.AddNewSchoolType" ></span>
                    </button>
                </span>

                <a ui-sref="school_type.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>