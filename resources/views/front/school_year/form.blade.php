<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SchoolYear.EditSchoolYear" translate-values="{school_year_id: school_year.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SchoolYear.AddNewSchoolYear" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolYear.CantGetSchoolYear" ng-show="!school_year.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="school_year.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SchoolYear.SchoolYearInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !school_year.id}" >
                      <label for="school_year_id" ><span translate="SchoolYear.SchoolYearID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="school_year_id" ng-model="school_year.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !school_year.school_year_name_ar}" >
                  <label for="school_year_name_ar" ><span translate="SchoolYear.SchoolYearNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_year_name_ar" ng-model="school_year.school_year_name_ar" class="form-control" placeholder="{{ 'SchoolYear.PleaseEnterTheSchoolYearNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_year.school_year_name_en}" >
                  <label for="school_year_name_en" ><span translate="SchoolYear.SchoolYearNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_year_name_en" ng-model="school_year.school_year_name_en" class="form-control" placeholder="{{ 'SchoolYear.PleaseEnterTheSchoolYearNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !school_year.school_year_start_hdate}" >
                  <label for="school_year_start_hdate" ><span translate="SchoolYear.SchoolStartHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_year_start_hdate" ng-model="school_year.school_year_start_hdate" class="form-control" placeholder="{{ 'SchoolYear.PleaseEnterTheSchoolStartHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !school_year.school_year_end_hdate}" >
                  <label for="school_year_end_hdate" ><span translate="SchoolYear.SchoolEndHDate" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="school_year_end_hdate" ng-model="school_year.school_year_end_hdate" class="form-control" placeholder="{{ 'SchoolYear.PleaseEnterTheSchoolEndHDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !school_year.year}" >
                  <label for="year" ><span translate="SchoolYear.SchoolYear" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="year" ng-model="school_year.year" class="form-control" placeholder="{{ 'SchoolYear.PleaseEnterTheSchoolYear' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !school_year.school_id}" >
                    <label for="school_id" ><span translate="SchoolYear.School" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="school_year.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "SchoolYear.SelectSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="SchoolYear.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SchoolYear.AddNewSchoolYear" ></span>
                    </button>
                </span>

                <a ui-sref="school_year.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>