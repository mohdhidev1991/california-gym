<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="SchoolYear.Identity" translate-values="{school_year_name: school_year.school_year_name_ar}" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SchoolYear.CantGetSchoolYear" ng-show="can_get_data" ></div>


        <div class="panel panel-default" ng-show="school_year" >
            <div class="panel-heading font-bold" translate="SchoolYear.Identity" ></div>

            <div class="panel-body">
                <table class="table table-striped m-b-none">
                    <tbody>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.NameAr" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_year.school_year_name_ar}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.SchoolName" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_year.school_name_ar}}</span>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.YearStartDate" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_year.school_year_start_hdate}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.YearEndDate" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_year.school_year_end_hdate}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.SubscribeStartDate" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_year.subscribe_start_hdate}}</span>
                            </td>
                            <td class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.SubscribeEndDate" ></label>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_year.subscribe_end_hdate}}</span>
                            </td>
                        </tr>

                    </tbody>
                </table>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <h4 translate="SchoolYear.SchoolScopes" class="text-center" ></h4>
                <table class="table table-striped m-b-none">
                    <thead>
                        <tr>
                            <th class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.SchoolLevel" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="SchoolYear.LevelClass" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="SchoolYear.ClassNb" ></label>
                            </th>
                            <th class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.SDepartment" ></label>
                            </th>
                            <th class="col-md-2" >
                                <label class="label_info" translate="SchoolYear.Actions" ></label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="school_scope in school_year.school_scopes" >
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_scope.school_level_name_ar}}</span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_scope.level_class_name_ar}}</span>
                            </td>
                            <td class="col-md-2" >
                                <span class="label_value" >{{school_scope.class_nb}}</span>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_scope.sdepartment_name}}</span>
                            </td>
                            <td class="col-md-2" >
                                <a class="btn btn-icon btn-primary" ui-sref="school_scope.edit({school_scope_id: school_scope.id})" ><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <h4 translate="SchoolYear.SchoolClasses" class="text-center" ></h4>
                <table class="table table-striped m-b-none">
                    <thead>
                        <tr>
                            <th class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.LevelClass" ></label>
                            </th>
                            <th class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.Symbol" ></label>
                            </th>
                            <th class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.Room" ></label>
                            </th>
                            <th class="col-md-3" >
                                <label class="label_info" translate="SchoolYear.Actions" ></label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="school_class in school_year.school_classes" >
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_class.level_class_name_ar}}</span>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_class.symbol}}</span>
                            </td>
                            <td class="col-md-3" >
                                <span class="label_value" >{{school_class.room_name}} <i class="visible-lg-inline-block visible-md-inline-block visible-sm-inline-block visible-sm-inline-block" >( {{school_class.capacity}} )</i></span>
                            </td>
                            <td class="col-md-3" >
                                <a class="btn btn-icon btn-primary" ui-sref="school_scope.edit({school_scope_id: school_scope.id})" ><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="line line-dashed b-b line-lg pull-in"></div>


            </div>

            <div class="panel-footer" >
                <a ui-sref="school_year.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>