<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Sdepartment.ManageSdepartments" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="Sdepartment.AllSdepartments" ></div>
            <div class="panel-header panel-header with-padding">
                <div class="header-tools header-btns margin-bottom-15" >
                    <a class="btn btn-default" ui-sref="sdepartment.add({add: true})" ><i class="fa fa-plus" ></i><span translate="Sdepartment.AddNewSdepartment" ></span></a>
                </div>

                <div class="filter filter-table" >
                    <form name="filter" ng-class="{}">
                        <div class="header-filter" >
                        </div>
                        <div class="body-filter" >
                            <div class="form-group" >
                                <label for="filter_name_or_id" ><span translate="Field.NameOrId" ></span></label>
                                <input type="text" id="filter_name_or_id" ng-model="filter.name_or_id" class="form-control" placeholder="{{ 'Field.NameOrId' | translate }}" />
                            </div>

                            <div class="form-group" >
                                <label for="filter_status" ><span translate="Global.Active" ></span></label>
                                <select id="filter_status" class="select-form-control" ng-model="filter.status" ng-options="obj.id as obj.label | translate for obj in active_status" >
                                    <option value="" >-- {{ "Form.SelectStatus" | translate }} --</option>
                                </select>
                            </div>

                            

                            <div ng-show="show_all_filters" >
                                <div class="form-group" >
                                    <label for="filter_school_id" ><span translate="Field.School" ></span></label>
                                    <select id="filter_school_id"  class="select-form-control" ng-model="filter.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                                        <option value="" >-- {{ "Form.SelectSchool" | translate }} --</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label for="filter_week_template_id" ><span translate="Field.WeekTemplate" ></span></label>
                                    <select id="filter_week_template_id"  class="select-form-control" ng-model="filter.week_template_id" ng-options="obj.id as obj.week_template_name_ar for obj in week_templates" >
                                        <option value="" >-- {{ "Form.SelectWeekTemplate" | translate }} --</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="footer-filtrer" >
                            <span class="checkbox">
                                <label class="i-checks" >
                                    <input type="checkbox" ng-model="show_all_filters" ng-change="filter.status = filter.name_or_id = filter.school_id = filter.week_template_id = null" ><i></i> <span translate="Form.ShowAllFilters" ></span>
                                </label>
                            </span>
                            <button class="btn btn-primary" ng-click="GetSdepartments();" ><i class="fa fa-filter"></i><span translate="Form.Filter" ></span></button>
                            <button class="btn btn-danger" ng-show=" filter.status || filter.name_or_id || filter.week_template_id || filter.school_id " ng-click="filter={};GetSdepartments();" ><i class="fa fa-times" ></i><span translate="Form.Reset" ></span></button>
                        </div>
                    </form>
                    
                </div><!-- End filter-table -->
                <div class="line line-dashed b-b line-lg"></div>
            </div>

            <div class="panel-body">
                <div class="alert alert-danger" ng-show="empty_results" >
                    <span translate="Global.NoResultsFound" ></span>
                </div>

                <div class="table-responsive" ng-show="sdepartments" >
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='active', th_sorting_asc: order_by==='active' && order==='DESC', th_sorting_desc: order_by==='active' && order==='ASC' }" ng-click="set_order('active', null )" >
                                    <label translate="Global.Active" ></label>
                                </th>
                                <th role="button" style="width:10%;" ng-class="{th_sorting: order_by!=='id', th_sorting_asc: order_by==='id' && order==='DESC', th_sorting_desc: order_by==='id' && order==='ASC' }" ng-click="set_order('id', null )" >
                                    <label translate="Global.ID" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='sdepartment_name_ar', th_sorting_asc: order_by==='sdepartment_name_ar' && order==='DESC', th_sorting_desc: order_by==='sdepartment_name_ar' && order==='ASC' }" ng-click="set_order('sdepartment_name_ar', null )" >
                                    <label translate="Global.NameAr" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='sdepartment_name_en', th_sorting_asc: order_by==='sdepartment_name_en' && order==='DESC', th_sorting_desc: order_by==='sdepartment_name_en' && order==='ASC' }" ng-click="set_order('sdepartment_name_en', null )" >
                                    <label translate="Global.NameEn" ></label>
                                </th>
                                <th role="button" ng-class="{th_sorting: order_by!=='school_id', th_sorting_asc: order_by==='school_id' && order==='DESC', th_sorting_desc: order_by==='school_id' && order==='ASC' }" ng-click="set_order('school_id', null )" >
                                    <label translate="Field.School" ></label>
                                </th>
                                <th >
                                    <label translate="Global.Actions" ></label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="sdepartment in sdepartments" >
                                <td>
                                    <span >
                                        <i ng-show="sdepartment.active==='Y'" class="fa fa-check text-success "></i>
                                        <i ng-show="sdepartment.active==='N'" class="fa fa-times text-danger text"></i>
                                    </span>
                                </td>
                                <td>
                                    <span>{{sdepartment.id}}</span>
                                </td>
                                <td>
                                    <span>{{sdepartment.sdepartment_name_ar}}</span>
                                </td>
                                <td>
                                    <span>{{sdepartment.sdepartment_name_en}}</span>
                                </td>
                                <td>
                                    <span>{{sdepartment.school_name_ar}}</span>
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" ui-sref="sdepartment.edit({sdepartment_id: sdepartment.id})" ><i class="fa fa-edit"></i><span translate="Global.Edit" ></span></a>
                                    <a class="btn btn-xs btn-danger" ng-click="delete_sdepartment(sdepartment.id)" ><i class="fa fa-remove"></i><span translate="Global.Delete" ></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer" ng-hide="empty_results" >
                <div class="text-right form-group" >
                    <button class="btn btn-default" ng-click="export_xls();" ><i class="fa fa-file-excel-o"></i><span translate="Form.Export" ></span></button>
                </div>
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">
                        <uib-pagination boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetSdepartments()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Global.AreYouSure" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Sdepartment.AreYouSureToDeleteThisSdepartment' | translate}}</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel_confirm_delete()" translate="Global.No" ></button>
    </div>
</script>