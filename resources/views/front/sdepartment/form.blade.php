<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Sdepartment.EditSdepartment" translate-values="{sdepartment_id: sdepartment.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Sdepartment.AddNewSdepartment" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Sdepartment.CantGetSdepartment" ng-show="!sdepartment.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="sdepartment.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Sdepartment.SdepartmentInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !sdepartment.id}" >
                      <label for="sdepartment_id" ><span translate="Sdepartment.SdepartmentID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="sdepartment_id" ng-model="sdepartment.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !sdepartment.sdepartment_name_ar}" >
                  <label for="sdepartment_name_ar" ><span translate="Sdepartment.SdepartmentNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="sdepartment_name_ar" ng-model="sdepartment.sdepartment_name_ar" class="form-control" placeholder="{{ 'Sdepartment.PleaseEnterTheSdepartmentNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !sdepartment.sdepartment_name_en}" >
                  <label for="sdepartment_name_en" ><span translate="Sdepartment.SdepartmentNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="sdepartment_name_en" ng-model="sdepartment.sdepartment_name_en" class="form-control" placeholder="{{ 'Sdepartment.PleaseEnterTheSdepartmentNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !sdepartment.school_id}" >
                    <label for="school_id" ><span translate="Sdepartment.SdepartmentSchool" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="sdepartment.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "Sdepartment.SelectSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !sdepartment.week_template_id}" >
                    <label for="week_template_id" ><span translate="Sdepartment.WeekTemplate" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-required="true" ng-model="sdepartment.week_template_id" ng-options="obj.id as obj.week_template_name_ar for obj in week_templates" >
                        <option value="" >-- {{ "Sdepartment.SelectWeekTemplate" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                
                <div class="form-group" >
                    <label for="active" ><span translate="Sdepartment.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Sdepartment.AddNewSdepartment" ></span>
                    </button>
                </span>

                <a ui-sref="sdepartment.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>