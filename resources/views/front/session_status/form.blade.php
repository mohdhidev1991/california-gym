<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="SessionStatus.EditSessionStatus" translate-values="{session_status_id: session_status.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="SessionStatus.AddNewSessionStatus" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="SessionStatus.CantGetSessionStatus" ng-show="!session_status.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="session_status.id || action==='add'" >
            <div class="panel-heading font-bold" translate="SessionStatus.SessionStatusInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !session_status.id}" >
                      <label for="session_status_id" ><span translate="SessionStatus.SessionStatusID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="session_status_id" ng-model="session_status.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>

                <div class="form-group" ng-class="{'has-error': !session_status.lookup_code}" >
                  <label for="lookup_code" ><span translate="SessionStatus.SessionStatusLookupCode" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lookup_code" ng-model="session_status.lookup_code" class="form-control" placeholder="{{ 'SessionStatus.PleaseEnterTheSessionStatusLookupCode' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !session_status.session_status_name_ar}" >
                  <label for="session_status_name_ar" ><span translate="SessionStatus.SessionStatusNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="session_status_name_ar" ng-model="session_status.session_status_name_ar" class="form-control" placeholder="{{ 'SessionStatus.PleaseEnterTheSessionStatusNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !session_status.session_status_name_en}" >
                  <label for="session_status_name_en" ><span translate="SessionStatus.SessionStatusNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="session_status_name_en" ng-model="session_status.session_status_name_en" class="form-control" placeholder="{{ 'SessionStatus.PleaseEnterTheSessionStatusNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
                <div class="form-group" >
                    <label for="active" ><span translate="SessionStatus.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="SessionStatus.AddNewSessionStatus" ></span>
                    </button>
                </span>

                <a ui-sref="session_status.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>