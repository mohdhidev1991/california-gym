<div>
	<div class="bg-header-page b-b wrapper-md">
		<h1 class="m-n font-bold h3" ><span translate="Planning.SendByEmail" ></span></h1>
	</div>
	<div class="wrapper-md">
		<div class="panel panel-default" >
			<div class="panel-body">

				<div class="form-group" >
					<label translate="Planning.PlanningDate" class="font-bold" ></label>
					<div class="input-group input-group-datepicker-no-clear-button">
					  <input type="text" class="form-control" uib-datepicker-popup="{{format}}" date-disabled="disabledDates(date,mode)" ng-click="open_datepicker()" ng-change="change_datepicker()" ng-model="popup_datepicker.date" is-open="popup_datepicker.opened" datepicker-options="dateOptions" current-text="{{'Global.Today'|translate}}" close-text="{{'Global.Close'|translate}}" ng-required="true" alt-input-formats="altInputFormats" />
					  <span class="input-group-btn">
					    <button type="button" class="btn btn-default" ng-click="open_datepicker()"><i class="glyphicon glyphicon-calendar"></i></button>
					  </span>
					</div>
				</div>


				<div class="form-group" ng-show="processing" >
					<div class="infos-processor-feed-import" >
                        <div class="progress progress-sm progress-striped active m-t-xs m-b-none" >
                          <div class="progress-bar bar bg-success" role="progressbar" aria-valuenow="{{pourcent}}" aria-valuemin="0" aria-valuemax="100" style="width:{{pourcent}}%"><span>{{pourcent}}%</span></div>
                        </div>
                    </div>
				</div>
				<div class="alert alert-success" ng-show="finish" ><span translate="Planning.EmailsSendedWithSuccess"></span></div>
				<div class="form-group">
					<button class="btn btn-sm btn-primary" type="button" ng-click="send_by_email()">
						<i class="fa fa-paper-plane"></i> <span translate="Planning.SendByEmail" ></span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>