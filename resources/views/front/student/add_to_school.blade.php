<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="School.AddNewStudentToSchool" ></h1>
    </div>
    <div class="wrapper-md">


        <!--<div class="panel panel-default" >
            <div class="panel-heading font-bold" translate="School.AddNewStudentToSchool" ></div>

            <div class="panel-body">-->

              <div class="panel panel-default" ng-show="step==='find_parent' || step==='parent'" >
                <div class="panel-heading font-bold" translate="Panel.FindParent" ></div>

                <div class="panel-body">
                    <form role="form" name="form_search_parent" >
                      <div class="row" >
                        <div class="col-md-4" >
                            <div class="form-group" >
                                <label for="parent_idn_type_id" ><span translate="Field.IdnType" ></span><sup class="required">*</sup></label>
                                <select ng-disabled="step!=='find_parent'" ng-required="true" class="select-form-control" id="parent_idn_type_id"  ng-model="parent_user.idn_type_id" ng-options="obj.id as obj.idn_type_name_ar for obj in idn_types" >
                                    <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" >
                            <div class="form-group" >
                                <label for="parent_idn" ><span translate="Field.IDN" ></span><sup class="required">*</sup></label>
                                <input ng-disabled="step!=='find_parent'" type="text" ng-required="true" required="" id="parent_idn" name="parent_idn" ng-model="parent_user.idn" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4" >
                            <label for="parent_idn" >&nbsp;</label>
                            <div class="clear"></div>
                            <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_search_parent.$valid" tooltip-class="tooltip-danger zindex9999" >
                              <button ng-disabled="!form_search_parent.$valid || step!=='find_parent'" type="submit" class="btn btn-sm btn-primary" ng-click="find_parent()" >
                                  <i class="fa fa-search" aria-hidden="true"></i>
                                  <span translate="Form.Find" ></span>
                              </button>
                            </span>
                            <button type="reset" ng-show="step!=='find_parent'" class="btn btn-sm btn-default" ng-click="reset_all()" >
                                <span translate="Form.Cancel" ></span>
                            </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div><!-- END Panel Parent Search -->


              
              
                <div class="panel panel-default" ng-show="step==='parent'" >
                  <div class="panel-heading font-bold" translate="Panel.ParentInformations" ></div>

                  <div class="panel-body">
                    <form name="form_parent" role="form" class="ng-pristine ng-valid" novalidate >

                        <div class="form-group" >
                            <label for="parent_country_id" ><span translate="Field.Nationality" ></span><sup class="required">*</sup></label>
                            <select ng-required="true" class="select-form-control" id="parent_country_id"  ng-model="parent_user.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>

                        <div class="form-group" >
                            <label for="parent_genre_id" ><span translate="Field.Genre" ></span><sup class="required">*</sup></label>
                            <select class="select-form-control" id="parent_genre_id"  ng-model="parent_user.genre_id" ng-options="obj.id as obj.genre_name_ar for obj in genres" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>

                        <div class="form-group" >
                            <label for="parent_firstname" ><span translate="Field.FirstName" ></span><sup class="required">*</sup></label>
                            <input type="text" ng-required="true" id="parent_firstname" ng-model="parent_user.firstname" class="form-control" />
                        </div>
                        <div class="form-group" >
                            <label for="parent_lastname" ><span translate="Field.LastName" ></span><sup class="required">*</sup></label>
                            <input type="text" ng-required="true" id="parent_lastname" ng-model="parent_user.lastname" class="form-control" />
                        </div>
                        <div class="form-group" >
                            <label for="parent_f_firstname" ><span translate="Field.FatherName" ></span><sup class="required">*</sup></label>
                            <input type="text" ng-required="true" id="parent_f_firstname" ng-model="parent_user.f_firstname" class="form-control" />
                        </div>
                        <div class="form-group" >
                            <label for="parent_mobile" ><span translate="Field.Mobile" ></span><sup class="required">*</sup></label>
                            <input type="text" ng-required="true" id="parent_mobile" ng-model="parent_user.mobile" class="form-control" />
                        </div>
                        <div class="form-group" >
                            <label for="parent_email" ><span translate="Field.Email" ></span></label>
                            <input type="email" id="parent_email" ng-model="parent_user.email" class="form-control" />
                        </div>

                        <div class="alert alert-danger" ng-show="errors_save_parent" >
                          <ul>
                            <li ng-repeat="error in errors_save_parent" >
                              <span >{{ error | translate}}</span>
                            </li>
                          </ul>
                        </div>
                    </form><!-- END Form -->
                  </div>
                  <div class="panel-footer" >
                      <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_parent.$valid" tooltip-class="tooltip-danger zindex9999" >
                          <button ng-disabled="!form_parent.$valid"
                              type="submit" class="btn btn-sm btn-primary" ng-click="save_parent()"
                              >
                              <i class="fa fa-floppy-o" ></i><span translate="Form.SaveData" ></span>
                          </button>
                      </span>
                  </div>
                </div><!-- END Panel Parent infos -->




                <div class="panel panel-default" ng-show="step==='students'" >
                    <div class="panel-heading font-bold" translate="Panel.ListeOfStudentsRelated" ></div>

                    <div class="panel-body">
                      <div class="form-group text-right" >
                        <a class="btn btn-sm btn-default" ng-click="step='parent'" ><i class="fa fa-angle-right" ></i><span translate="Form.Back" ></span></a>
                        <a class="btn btn-sm btn-default" ng-click="add_student()" ><i class="fa fa-plus"></i><span translate="Student.AddNewStudent" ></span></a>
                      </div>

                      <div class="alert alert-warning" ng-show="empty_related_students" >
                        <span translate="User.ThisUserDoesNotHaveAnyFamilyRelationshipCurrently" ></span>
                      </div>

                      <div class="table-responsive" ng-show="related_students" >
                        <table class="table table-striped b-t b-light">
                            <thead>
                                <tr>
                                    <th>
                                        <label translate="Global.ReaayaService" ></label>
                                    </th>
                                    <th>
                                        <label translate="Student.StudentNumber" ></label>
                                    </th>
                                    <th>
                                        <label translate="Student.StudentName" ></label>
                                    </th>
                                    <th >
                                        <label translate="Global.Actions" ></label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="r_student in related_students" >
                                    <td>
                                        <span >
                                            <i ng-show="r_student.active_account==='Y'" class="fa fa-check text-success "></i>
                                            <i ng-show="r_student.active_account!=='Y'" class="fa fa-times text-danger text"></i>
                                        </span>
                                    </td>
                                    <td>
                                        <span ng-show="r_student.student_file_id" >{{r_student.student_num}}</span>
                                        <span ng-show="!r_student.student_file_id" class="text-danger" ><span translate="Student.NotRegistredInLevelClass" ></span></span>
                                    </td>
                                    <td>
                                        <span>{{r_student.student_firstname+' '+r_student.student_f_firstname+' '+r_student.student_lastname}}</span>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-primary" ng-click="edit_student(r_student.student_id)" ><i class="fa fa-edit"></i><span translate="Student.EditStudentInformations" ></span></a>
                                        <a ng-show="r_student.student_file_id" class="btn btn-xs btn-primary" ng-click="edit_student_file(r_student.student_id)" ><i class="fa fa-edit"></i><span translate="Student.EditStudentLevelClass" ></span></a>
                                        <a ng-show="!r_student.student_file_id" class="btn btn-xs btn-primary" ng-click="edit_student_file(r_student.student_id)" ><i class="fa fa-plus"></i><span translate="Student.RegisterStudentInLevelClass" ></span></a>
                                        <a class="btn btn-xs btn-danger" ng-click="remove_student()" ><i class="fa fa-times"></i><span translate="Form.Delete" ></span></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                </div><!-- END Panel Liste of students related -->


                <div class="panel panel-default" ng-show="step==='edit_student' || step==='add_student'" >
                  <form name="form_student" role="form" class="ng-pristine ng-valid" novalidate >
                    <div class="panel-heading font-bold" translate="Panel.StudentInformations" ></div>

                    <div class="panel-body">
                      <div class="form-group" >
                          <label for="student_genre_id" ><span translate="Field.Genre" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" id="student_genre_id"  ng-model="student.genre_id" ng-options="obj.id as obj.genre_name_ar for obj in genres" >
                              <option value="" >-- {{ "Select.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="family_relation_relship_id" ><span translate="Field.TypeOfRelationship" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" id="family_relation_relship_id"  ng-model="family_relation.relship_id" ng-options="obj.id as obj.relship_name_ar for obj in relships" >
                              <option value="" >-- {{ "Select.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="family_relation_resp_relationship" ><span translate="Field.RelationshipDescription" ></span><sup class="required">*</sup></label>
                          <input type="text" ng-required="true" id="family_relation_resp_relationship" ng-model="family_relation.resp_relationship" class="form-control" />
                      </div>
                      <div class="form-group" >
                          <label for="student_firstname" ><span translate="Field.FirstName" ></span><sup class="required">*</sup></label>
                          <input type="text" ng-required="true" id="student_firstname" ng-model="student.firstname" class="form-control" />
                      </div>
                      <div class="form-group" >
                          <label for="student_lastname" ><span translate="Field.LastName" ></span><sup class="required">*</sup></label>
                          <input type="text" ng-required="true" id="student_lastname" ng-model="student.lastname" class="form-control" />
                      </div>
                      <div class="form-group" >
                          <label for="parent_f_firstname" ><span translate="Field.FatherName" ></span><sup class="required">*</sup></label>
                          <input type="text" ng-required="true" id="parent_f_firstname" ng-model="student.f_firstname" class="form-control" />
                      </div>
                      <div class="form-group" >
                          <label for="student_country_id" ><span translate="Field.Nationality" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" id="student_country_id"  ng-model="student.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                              <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="student_idn_type" ><span translate="Field.IdnType" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" id="student_idn_type"  ng-model="student.idn_type_id" ng-options="obj.id as obj.idn_type_name_ar for obj in idn_types" >
                              <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="student_idn" ><span translate="Field.IDN" ></span><sup class="required">*</sup></label>
                          <input type="text" ng-required="true" required="" id="student_idn" name="student_idn" ng-model="student.idn" class="form-control" />
                      </div>
                      <div class="form-group" >
                        <label for="student_birth_date" ><span translate="Field.Birthday" ></span><sup class="required">*</sup></label>
                        <islamic-datepicker ng-required="true" id="birth_date" ng-model="student.birth_date" ng-model-hdate="student.birth_date" />
                      </div>
                      <div class="form-group" >
                          <span class="checkbox">
                              <label class="i-checks" >
                                  <input type="checkbox" ng-model="family_relation.active_account_checkbox" ><i></i> <span translate="Field.DidHeEnjoinReaayaServices" ></span>
                              </label>
                          </span>
                      </div>


                      <div class="form-group" >
                          <label for="student_current_school_level_id" ><span translate="Field.SchoolLevel" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" ng-required="true" ng-change="GetLevelClasses(student.current_school_level_id)" id="student_current_school_level_id"  ng-model="student.current_school_level_id" ng-options="obj.id as obj.school_level_name_ar for obj in school_levels" >
                              <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="student_current_level_class_id" ><span translate="Field.LevelClass" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" ng-required="true" ng-disabled="!student.current_school_level_id" id="student_current_level_class_id"  ng-model="student.current_level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" >
                              <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                          </select>
                      </div>

                      <div class="alert alert-danger" ng-show="errors_save_student" >
                        <ul>
                          <li ng-repeat="error in errors_save_student" >
                            <span >{{ error | translate}}</span>
                          </li>
                        </ul>
                      </div>

                      <div class="alert alert-success" ng-show="success_save_student" >
                        <span translate="Form.SuccessSaveData" ></span>
                      </div>
                    </div>
                    <div class="panel-footer" >
                        <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_student.$valid" tooltip-class="tooltip-danger zindex9999" >
                            <button ng-disabled="!form_student.$valid"
                                type="submit" class="btn btn-sm btn-primary" ng-click="save_student()"
                                >
                                <i class="fa fa-floppy-o" ></i><span translate="Form.SaveData" ></span>
                            </button>
                        </span>
                        <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_student.$valid" tooltip-class="tooltip-danger zindex9999" >
                            <button ng-disabled="!form_student.$valid"
                                type="submit" class="btn btn-sm btn-primary" ng-click="save_student(true)"
                                >
                                <i class="fa fa-floppy-o" ></i><span translate="Form.SaveDataAndBack" ></span>
                            </button>
                        </span>

                        <button ng-show="student.id && student.student_file_id"
                            type="submit" class="btn btn-sm btn-primary" ng-click="edit_student_file(student.id)"
                            >
                            <i class="fa fa-edit"></i><span translate="Student.EditStudentLevelClass" ></span>
                        </button>

                        <button ng-show="student.id && !student.student_file_id"
                            type="submit" class="btn btn-sm btn-primary" ng-click="edit_student_file(student.id)"
                            >
                            <i class="fa fa-plus"></i><span translate="Student.RegisterStudentInLevelClass" ></span>
                        </button>


                        <button type="back" class="btn btn-sm btn-default" ng-click="step='students'" aria-hidden="false" style="">
                            <span translate="Form.Back" ></span>
                        </button>
                    </div>
                  </form>
                </div><!-- END Panel Student infos -->





                <div class="panel panel-default" ng-show="step==='edit_student_file'" >
                  <form name="form_student_file" role="form" class="ng-pristine ng-valid" novalidate >
                    <div class="panel-heading font-bold" translate="Panel.LevelClassInformations" ></div>

                    <div class="panel-body">
                      <div class="form-group" >
                          <label for="student_file_school_level_id" ><span translate="Field.SchoolLevel" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" id="student_file_school_level_id"  ng-model="student_file.school_level_id" ng-options="obj.id as obj.school_level_name_ar for obj in school_levels" >
                              <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="student_file_level_class_id" ><span translate="Field.LevelClass" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" ng-disabled="!student_file.school_level_id" id="student_file_level_class_id"  ng-model="student_file.level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" >
                              <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="student_file_school_class_id" ><span translate="Field.SchoolClass" ></span><sup class="required">*</sup></label>
                          <select class="select-form-control" ng-disabled="!student_file.level_class_id" id="student_file_school_class_id"  ng-model="student_file.school_class_id" ng-options="obj.id as obj.symbol for obj in school_classes" >
                              <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                          </select>
                      </div>
                      <div class="form-group" >
                          <label for="student_file_student_num" ><span translate="Field.StudentNumber" ></span></label>
                          <input type="text" id="student_file_student_num" ng-model="student_file.student_num" class="form-control" />
                      </div>
                      <div class="form-group" >
                          <label for="student_file_student_place" ><span translate="Field.StudentPlace" ></span></label>
                          <input type="text" id="student_file_student_place" ng-model="student_file.student_place" class="form-control" />
                      </div>
                      <div class="form-group" >
                          <label for="student_file_paid_amount" ><span translate="Field.PaidAmount" ></span></label>
                          <input type="text" id="student_file_paid_amount" ng-model="student_file.paid_amount" class="form-control" />
                      </div>
                      <div class="form-group" >
                          <label for="student_file_paid_hdate" ><span translate="Field.PaidAmount" ></span></label>
                          <islamic-datepicker id="student_file_paid_hdate" ng-model="student_file.paid_hdate" ng-model-hdate="student_file.paid_hdate" />
                      </div>
                      <div class="form-group" >
                          <label for="student_file_paid_comment" ><span translate="Field.PaidComment" ></span></label>
                          <input type="text" id="student_file_paid_comment" ng-model="student_file.paid_comment" class="form-control" />
                      </div>

                      <div class="alert alert-danger" ng-show="errors_save_student_file" >
                        <ul>
                          <li ng-repeat="error in errors_save_student_file" >
                            <span >{{ error | translate}}</span>
                          </li>
                        </ul>
                      </div>

                      <div class="alert alert-success" ng-show="success_save_student_file" >
                        <span translate="Form.SuccessSaveData" ></span>
                      </div>

                    </div>
                    <div class="panel-footer" >
                        <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_student_file.$valid" tooltip-class="tooltip-danger zindex9999" >
                            <button ng-disabled="!form_student_file.$valid"
                                type="submit" class="btn btn-sm btn-primary" ng-click="save_student_file()"
                                >
                                <i class="fa fa-floppy-o" ></i><span translate="Form.SaveData" ></span>
                            </button>
                        </span>

                        <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_student_file.$valid" tooltip-class="tooltip-danger zindex9999" >
                            <button ng-disabled="!form_student_file.$valid"
                                type="submit" class="btn btn-sm btn-primary" ng-click="save_student_file(true)"
                                >
                                <i class="fa fa-floppy-o" ></i><span translate="Form.SaveDataAndBack" ></span>
                            </button>
                        </span>

                        <button type="back" class="btn btn-sm btn-default" ng-click="step='students'" aria-hidden="false" style="">
                            <span translate="Form.Back" ></span>
                        </button>
                    </div>
                  </form>
                </div><!-- END Panel Level Classs -->


            <!--</div>--><!-- END Panel Global -->
      <!--</div>-->
    </div>
</div>