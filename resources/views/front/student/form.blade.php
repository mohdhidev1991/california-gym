<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="Student.EditStudent" translate-values="{student_id: student.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="Student.AddNewStudent" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="Student.CantGetStudent" ng-show="!student.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="student.id || action==='add'" >
            <div class="panel-heading font-bold" translate="Student.Infos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !student.id}" >
                      <label for="student_id" ><span translate="Student.ID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="student_id" ng-model="student.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" >
                  <label for="rea_user_id" ><span translate="Student.ReaUserID" ></span></label>
                  <input type="text" id="rea_user_id" ng-model="student.rea_user_id" class="form-control" placeholder="{{ 'Student.PleaseEnterTheReaUserId' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="genre_id" ><span translate="Student.Genre" ></span></label>
                    <select class="select-form-control" id="genre_id"  ng-model="student.genre_id" ng-options="obj.id as obj.genre_name_ar for obj in genres" >
                        <option value="" >-- {{ "School.SelectGenre" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" ng-class="{'has-error': !student.firstname}" >
                  <label for="firstname" ><span translate="Student.FirstName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="firstname" ng-model="student.firstname" class="form-control" placeholder="{{ 'Student.PleaseEnterTheFirstName' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !student.lastname}" >
                  <label for="lastname" ><span translate="Student.LastName" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="lastname" ng-model="student.lastname" class="form-control" placeholder="{{ 'Student.PleaseEnterThelastName' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="f_firstname" ><span translate="Student.FamilyName" ></span></label>
                  <input type="text" id="f_firstname" ng-model="student.f_firstname" class="form-control" placeholder="{{ 'Student.PleaseEnterTheFamilyName' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="birth_date" ><span translate="Student.BirthDate" ></span></label>
                  <input type="text" id="birth_date" ng-model="student.birth_date" class="form-control" placeholder="{{ 'Student.PleaseEnterTheBirthDate' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="address" ><span translate="Student.Address" ></span></label>
                  <input type="text" id="address" ng-model="student.address" class="form-control" placeholder="{{ 'Student.PleaseEnterTheAddress' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="quarter" ><span translate="Student.Quarter" ></span></label>
                  <input type="text" id="quarter" ng-model="student.quarter" class="form-control" placeholder="{{ 'Student.PleaseEnterTheQuarter' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="father_rea_user_id" ><span translate="Student.FatherReaUser" ></span></label>
                  <input type="text" id="father_rea_user_id" ng-model="student.father_rea_user_id" class="form-control" placeholder="{{ 'Student.PleaseEnterTheFatherReaUser' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="mother_rea_user_id" ><span translate="Student.MotherReaUser" ></span></label>
                  <input type="text" id="mother_rea_user_id" ng-model="student.mother_rea_user_id" class="form-control" placeholder="{{ 'Student.PleaseEnterTheMotherReaUser' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="resp1_rea_user_id" ><span translate="Student.Responsable1" ></span></label>
                  <input type="text" id="resp1_rea_user_id" ng-model="student.resp1_rea_user_id" class="form-control" placeholder="{{ 'Student.PleaseEnterTheResponsable1' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="resp2_rea_user_id" ><span translate="Student.Responsable2" ></span></label>
                  <input type="text" id="resp2_rea_user_id" ng-model="student.resp2_rea_user_id" class="form-control" placeholder="{{ 'Student.PleaseEnterTheResponsable2' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                  <label for="cp" ><span translate="Student.CP" ></span></label>
                  <input type="text" id="cp" ng-model="student.cp" class="form-control" placeholder="{{ 'Student.PleaseEnterTheCP' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="idn_types" ><span translate="Student.IdnTypes" ></span></label>
                    <select class="select-form-control" id="idn_types"  ng-model="student.idn_type_id" ng-options="obj.id as obj.idn_type_name_ar for obj in idn_types" >
                        <option value="" >-- {{ "School.SelectIdnType" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="idn" ><span translate="Student.Idn" ></span></label>
                  <input type="text" id="idn" ng-model="student.idn" class="form-control" placeholder="{{ 'Student.PleaseEnterTheIdn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                    <label for="country_id" ><span translate="Student.Country" ></span></label>
                    <select class="select-form-control"  ng-change="GetCities()" ng-model="student.country_id" ng-options="obj.id as obj.country_name_ar for obj in countries" >
                        <option value="" >-- {{ "School.SelectCountry" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" >
                    <label for="city_id" ><span translate="Student.City" ></span></label>
                    <select class="select-form-control" ng-disabled="!student.country_id || student.country_id===''" ng-model="student.city_id" ng-options="obj.id as obj.city_name_ar for obj in cities" >
                        <option value="" >-- {{ "School.SelectCity" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                  <label for="mobile" ><span translate="Student.Mobile" ></span></label>
                  <input type="text" id="mobile" ng-model="student.mobile" class="form-control" placeholder="{{ 'Student.PleaseEnterTheMobile' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                  <label for="email" ><span translate="Student.Email" ></span></label>
                  <input type="email" id="email" ng-model="student.email" class="form-control" placeholder="{{ 'Student.PleaseEnterTheEmail' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                <div class="form-group" >
                    <label for="active" ><span translate="Student.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled=" !form.$valid "
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Student.AddNewStudent" ></span>
                    </button>
                </span>

                <a ui-sref="student.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>