<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Student.TransferStudentToAnotherSeat" ></h1>
    </div>
    <div class="wrapper-md">

              <div class="panel panel-default"  >
                <form name="form" role="form" class="ng-pristine ng-valid" novalidate >
                  <div class="panel-heading font-bold" translate="Panel.SearchLevelClass" ></div>

                  <div class="panel-body">
                    <div class="form-group" >
                        <label for="school_level_id" ><span translate="Field.SchoolLevel" ></span><sup class="required">*</sup></label>
                        <select class="select-form-control" id="school_level_id" ng-change="GetLevelClasses()"  ng-model="school_level_id" ng-options="obj.id as obj.school_level_name_ar for obj in school_levels" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>
                    <div class="form-group" >
                        <label for="level_class_id" ><span translate="Field.LevelClass" ></span><sup class="required">*</sup></label>
                        <select class="select-form-control" ng-disabled="!school_level_id" id="level_class_id" ng-change="GetSchoolClasses()" ng-model="level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>

                    <div class="form-group" >
                        <label for="school_class_id" ><span translate="Field.SchoolClass" ></span><sup class="required">*</sup></label>
                        <select class="select-form-control" ng-disabled="!level_class_id" ng-change="reset_bf_transfer()" id="school_class_id"  ng-model="school_class_id" ng-options="obj.id as obj.symbol | translate_symbol for obj in school_classes" >
                            <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                        </select>
                    </div>

                    <div class="alert alert-danger" ng-show="errors_get_students" >
                      <ul>
                        <li ng-repeat="error in errors_get_students" >
                          <span >{{ error | translate}}</span>
                        </li>
                      </ul>
                    </div>

                  </div>
                  <div class="panel-footer" >
                      <span uib-tooltip="{{ 'Form.PleaseFillInAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                          <button ng-disabled="!form.$valid"
                              type="submit" class="btn btn-sm btn-primary" ng-click="get_students()"
                              >
                              <i class="fa fa-search" ></i><span translate="Form.Search" ></span>
                          </button>
                      </span>
                      <button ng-show="bad_order" ng-click="reorder_school_class()" class="btn btn-sm btn-primary" >
                          <i class="fa fa-exchange" ></i><span translate="SchoolClass.StudentsAutoReorder" ></span>
                      </button>
                  </div>
                </form>
              </div><!-- END Panel Level Classs -->


              <div class="panel panel-default" ng-show="students_place" >
                  <div class="panel-heading font-bold" translate="SchoolClass.SchoolClassStudents" ></div>
                  <div class="panel-body">


                    <div class="table-responsive">
                      <table class="table table-striped b-t b-light">
                          <thead>
                              <tr>
                                  <th style="width:20px;" >
                                      <label translate="Room.Place" ></label>
                                  </th>
                                  <th >
                                      <label translate="Field.Student" ></label>
                                  </th>
                                  <th>
                                      <label translate="Student.TransferToAnotherSeat" ></label>
                                  </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr ng-repeat="student_place in students_place track by $index" >
                                  <td>
                                      <span>{{$index+1}}</span>
                                  </td>
                                  <td>
                                      <span ng-show="student_place.student_id" >
                                        <label class="font-bold" >
                                          <span>{{student_place.student_name}}</span>
                                        </label>
                                      </span>
                                      <span ng-show="!student_place.student_id" >
                                        <label class="font-bold" >
                                          <span translate="Global.Empty" ></span>
                                        </label>
                                      </span>
                                  </td>
                                  <td>
                                      <span ng-show="student_place.student_id" >
                                        <a class="btn btn-icon-no-margin btn-xs btn-primary" title="{{ 'Student.TransferStudentToAnotherSeat' | translate }}" ng-click="change_student_place(student_place)" >
                                          <i class="fa fa-exchange"></i>
                                        </a>
                                      </span>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
                  <!--

                    <ol class="students-list-ul" >
                      <li ng-repeat="student_place in students_place track by $index" >
                        <span ng-show="student_place.student_id" >
                          <a class="btn btn-icon-no-margin btn-xs btn-primary" title="{{ 'Student.TransferStudentToAnotherSeat' | translate }}" ng-click="change_student_place(student_place)" >
                            <i class="fa fa-exchange"></i>
                          </a>
                          <label class="font-bold" >
                            <span>{{student_place.student_name}}</span>
                          </label>
                        </span>
                        <span ng-show="!student_place.student_id" >
                          <label class="font-bold" >
                            <span translate="Global.Empty" ></span>
                          </label>
                        </span>
                      </li>
                    </ul>-->
                  </div>
              </div><!-- END Panel students_place -->

    </div>
</div>
<script type="text/ng-template" id="ModalChangeStudentPlace.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Student.TransferStudentToAnotherSeat" ></h3>
    </div>
    <div class="modal-body">

        <div class="form-group" >
            <label for="new_school_class_id" ><span translate="Field.SchoolClass" ></span><sup class="required">*</sup></label>
            <select class="select-form-control" ng-change="get_students()" id="new_school_class_id"  ng-model="new_school_class_id" ng-options="obj.id as obj.symbol | translate_symbol for obj in school_classes" >
                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
            </select>
        </div>



        <div class="panel panel-default" ng-show="students_place" >
            <div class="panel-heading font-bold" translate="SchoolClass.SchoolClassStudents" ></div>
            <div class="panel-body">
              <ol class="students-list-ul" >
                <li ng-repeat="student_place in students_place track by $index" >
                  <label class="i-checks" ng-class="{'current-student': student_place.student_id===student.student_id}" >
                    <input ng-disabled="student_place.student_id===student.student_id" type="radio" ng-model="new_student_place.place_number" ng-value="$index+1" />
                    <i></i>
                    <span ng-show="student_place.student_id" >
                      <span class="font-bold" >{{student_place.student_name}}</span></span>
                    </span>
                    <span ng-show="!student_place.student_id" >
                      <span class="font-bold" translate="Global.Empty" ></span>
                    </span>
                  </label>
                </li>
              </ol>
            </div>
        </div><!-- END Panel students_place -->


        <div class="alert alert-danger" ng-show="errors_get_students" >
          <ul>
            <li ng-repeat="error in errors_get_students" >
              <span >{{ error | translate}}</span>
            </li>
          </ul>
        </div>

    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" ng-disabled="!new_student_place.place_number" type="button" ng-click="confirm_replace()" translate="Student.TransferStudentToTheNewPlace" ></button>
        <button class="btn btn-danger" type="button" ng-click="cancel()" translate="Form.Cancel" ></button>
    </div>
</script>