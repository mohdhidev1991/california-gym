<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" translate="Administration.ManageTranslates" ></h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading font-bold" translate="Translate.AllTranslates" ></div>
            <div class="panel-header panel-header with-padding">
                <div class="row" >
                    <div class="col-md-4" >
                        <div class="form-group" >
                            <label for="filter_translate_module_id" ><span translate="Translate.FilterByTranslateModule" ></span></label>
                            <select id="filter_translate_module_id" ng-change="GetTranslates()" class="select-form-control" ng-model="filter_translate_module_id" ng-options="obj.id as obj.lookup_code for obj in translate_modules" >
                                <option value="" >-- {{ "Form.NotSelected" | translate }} --</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group" >
                            <label for="key_search" ><span translate="Form.Search" ></span></label>
                            <input id="key_search" ng-change="GetTranslates()" ng-model-options="{ debounce: 1000 }"  class="form-control" ng-model="key_search" place-holder="Translate.YouCanSearchByKey" />
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="header-tools header-btns" >
                            <a class="btn btn-default" ng-click="add_translate_module();" ><i class="fa fa-plus" ></i><span translate="Translate.AddNewModule" ></span></a>
                            <a class="btn btn-default" ng-click="add_translate();" ><i class="fa fa-plus" ></i><span translate="Translate.AddNewTranslate" ></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <div class="alert alert-danger" ng-show="no_results" ><span translate="Global.EmptyResults" ></span></div>

                    <table class="table table-striped b-t b-light" ng-hide="no_results" >
                        <thead>
                            <tr>
                                <thng-repeat="language in languages" class="col-md-4" >
                                    <label >{{language.lookup_code}}</label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat-start="translate in translates" >
                                <td colspan="{{languages.length}}" >
                                    <a class="btn btn-sm btn-danger" ng-click="delete_translate(translate.translate_module_id, translate.meta_key)" ><i class="fa fa-remove" style="margin: 0;"></i></a> <span>{{translate.tcode}}</span>
                                </td>
                            </tr>
                            <tr ng-repeat-end>
                                <td ng-repeat="language in languages" >
                                    <textarea class="form-control" ng-model="translate['lang_'+language.id]" ></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="panel-footer" ng-hide="no_results" >

                <div class="alert" ng-class="{'alert-danger': alerts.type==='error', 'alert-success': alerts.type==='success'}" ng-show="alerts" >
                    <span ng-show="alerts.messages.length===1" >{{alerts.messages[0] | translate}}</span>
                    <ul ng-show="alerts.messages.length > 1" >
                        <li ng-repeat="alert in alerts.messages track by $index" ><span>{{alert | translate}}</span></li>
                    </ul>
                </div>

                <div class="form-group text-center" ng-show="translates" >
                    <button class="btn btn-sm btn-primary" ng-click="multisave()" ><i class="fa fa-edit"></i><span translate="Global.SaveData" ></span></button>
                </div>
                <div class="row">
                    <div class="col-sm-6 hidden-xs">
                        <div class="input-group m-b">
                          <span class="input-group-addon" translate="Global.ItemsPerPage" ></span>
                          <select class="select-form-control" ng-model="itemsPerPage" ng-change="change_itemsPerPage()" ng-options="obj as obj for obj in optionsItemsPerPage" ></select>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right text-center-xs">
                        <uib-pagination max-size="5" boundary-links="true" items-per-page="itemsPerPage" total-items="totalItems" ng-change="GetTranslates()" ng-model="currentPage" class="pagination-md pagination-footer" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>


<script type="text/ng-template" id="ModalAddTranslateModule.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="TranslateModule.AddTranslateModule" ></h3>
    </div>
    <form name="form_add_translate_module" >
        <div class="modal-body">
            <div class="form-group" ng-class="{'has-error': !translate_module.lookup_code}" >
              <label for="lookup_code" ><span translate="TranslateModule.LookupCode" ></span><sup class="required" >*</sup></label>
              <input type="text" ng-required="true" required="" id="lookup_code" ng-model="translate_module.lookup_code" class="form-control" placeholder="{{ 'TransaleModule.PleaseEnterTheLookupCode' | translate }}" />
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
        </div>
        <div class="modal-footer">
            <div class="alert alert-danger" ng-show="errors" >
                <span ng-repeat="error in errors" >{{error | translate}}</span>
            </div>
            <span uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_add_translate_module.$valid" tooltip-class="tooltip-danger zindex9999" >
                <button class="btn btn-primary" ng-disabled="!form_add_translate_module.$valid" type="button" ng-click="add()" translate="Global.Add" ></button>
            </span>
            <button class="btn btn-danger" type="button" ng-click="close()" translate="Global.Close" ></button>
        </div>
    </form>
</script>



<script type="text/ng-template" id="ModalAddTranslate.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Translate.AddTranslate" ></h3>
    </div>
    <form name="form_add_translate" >
        <div class="modal-body">
            <div class="form-group" ng-class="{'has-error': !translate.translate_module_id}" >
                <label for="translate_module_id" ><span translate="Translate.MetaKey" ></span><sup class="required" >*</sup></label>
                <select id="translate_module_id" class="select-form-control" ng-required="true" ng-model="translate.translate_module_id" ng-options="obj.id as obj.lookup_code for obj in translate_modules" >
                    <option value="" >-- {{ "Translate.SelectTheTranslateModule" | translate }} --</option>
                </select>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group" ng-class="{'has-error': !translate.meta_key}" >
              <label for="meta_key" ><span translate="Translate.MetaKey" ></span><sup class="required" >*</sup></label>
              <input type="text" ng-required="true" required="" id="meta_key" ng-model="translate.meta_key" class="form-control" placeholder="{{ 'Transale.PleaseEnterTheMetaKey' | translate }}" />
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group" ng-repeat="lang in languages" >
              <label for="meta_value" ><span translate="Translate.MetaValue" ></span>: {{lang.lookup_code}}</label>
              <input type="text" id="meta_value" ng-model="translate['lang_'+lang.id]" class="form-control" placeholder="{{ 'Transale.PleaseEnterTheMetaValue' | translate }}" />
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
        </div>    
        <div class="modal-footer">
            <div class="alert" ng-class="{'alert-danger': alerts.type==='error', 'alert-success': alerts.type==='success'}" ng-show="alerts" >
                <span ng-show="alerts.messages.length===1" >{{alerts.messages[0] | translate}}</span>
                <ul ng-show="alerts.messages.length > 1" >
                    <li ng-repeat="alert in alerts.messages track by $index" ><span>{{alert | translate}}</span></li>
                </ul>
            </div>
            <span uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form_add_translate.$valid" tooltip-class="tooltip-danger zindex9999" >
                <button class="btn btn-primary" ng-disabled="!form_add_translate.$valid" type="button" ng-click="add(false)" translate="Global.Add" ></button>
                <button class="btn btn-primary" ng-disabled="!form_add_translate.$valid" type="button" ng-click="add(true)" translate="Translate.AddAndClose" ></button>
            </span>
            <button class="btn btn-danger" type="button" ng-click="close()" translate="Global.Close" ></button>
        </div>
    </form>
</script>



<script type="text/ng-template" id="ModalConfirmDelete.html">
    <div class="modal-header">
        <h3 class="modal-title" translate="Form.Confirmation" ></h3>
    </div>
    <div class="modal-body">
        <p>{{ 'Form.AreYouSureToDeleteThisData' | translate}}</p>
    </div>
    <div class="modal-footer">
        <div class="alert" ng-class="{'alert-danger': alerts.type==='error', 'alert-success': alerts.type==='success'}" ng-show="alerts" >
            <span ng-show="alerts.messages.length===1" >{{alerts.messages[0] | translate}}</span>
            <ul ng-show="alerts.messages.length > 1" >
                <li ng-repeat="alert in alerts.messages track by $index" ><span>{{alert | translate}}</span></li>
            </ul>
        </div>
        <button class="btn btn-primary" type="button" ng-click="confirm_delete()" translate="Global.Yes" ></button>
        <button class="btn btn-danger" type="button" ng-click="close()" translate="Global.No" ></button>
    </div>
</script>


