<div>
    <div class="bg-header-page b-b wrapper-md">
        <h1 class="m-n font-bold h3" ng-show="action==='edit'" translate="WeekTemplate.EditWeekTemplate" translate-values="{week_template_id: week_template.id}" ></h1>
        <h1 class="m-n font-bold h3" ng-show="action==='add'" translate="WeekTemplate.AddNewWeekTemplate" ></h1>
    </div>
    <div class="wrapper-md">
        
        <div class="alert alert-danger" translate="WeekTemplate.CantGetWeekTemplate" ng-show="!week_template.id && action==='edit'" ></div>


        <div class="panel panel-default" ng-show="week_template.id || action==='add'" >
            <div class="panel-heading font-bold" translate="WeekTemplate.WeekTemplateInfos" ></div>

            <div class="panel-body">
              <form role="form" name="form" class="ng-pristine ng-valid" novalidate>
                
                <div ng-show="action==='edit'" >
                    <div class="form-group" ng-class="{'has-error': !week_template.id}" >
                      <label for="week_template_id" ><span translate="WeekTemplate.WeekTemplateID" ></span><sup class="required" >*</sup></label>
                      <input type="text" ng-disabled="true" ng-required="action==='edit'" required="" id="week_template_id" ng-model="week_template.id" class="form-control" />
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>


                <div class="form-group" ng-class="{'has-error': !week_template.week_template_name_ar}" >
                  <label for="week_template_name_ar" ><span translate="WeekTemplate.WeekTemplateNameAr" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="week_template_name_ar" ng-model="week_template.week_template_name_ar" class="form-control" placeholder="{{ 'WeekTemplate.PleaseEnterTheWeekTemplateNameAr' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                <div class="form-group" ng-class="{'has-error': !week_template.week_template_name_en}" >
                  <label for="week_template_name_en" ><span translate="WeekTemplate.WeekTemplateNameEn" ></span><sup class="required" >*</sup></label>
                  <input type="text" ng-required="true" required="" id="week_template_name_en" ng-model="week_template.week_template_name_en" class="form-control" placeholder="{{ 'WeekTemplate.PleaseEnterTheWeekTemplateNameEn' | translate }}" />
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>


                
                <div class="form-group" ng-class="{'has-error': !week_template.school_id}" >
                    <label for="school_id" ><span translate="WeekTemplate.School" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-model="week_template.school_id" ng-options="obj.id as obj.school_name_ar for obj in schools" >
                        <option value="" >-- {{ "School.SelectSchool" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" >
                    <label for="level_class_id" ><span translate="WeekTemplate.LevelClass" ></span><sup class="required" >*</sup></label>
                    <select class="select-form-control" ng-model="week_template.level_class_id" ng-options="obj.id as obj.level_class_name_ar for obj in level_classes" >
                        <option value="" >-- {{ "School.SelectLevelClass" | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>




                <div class="form-group" ng-repeat="day_n in [1,2,3,4,5,6,7]" >
                    <label for="day{{day_n}}_template_id" ><span translate="WeekTemplate.TemplateDay{{day_n}}" ></span></label>
                    <select class="select-form-control" ng-model="week_template.days_template_id[day_n]" ng-options="obj.id as obj.day_template_name for obj in day_templates" >
                        <option value="" >-- {{ "School.SelectTemplateDay"+day_n | translate }} --</option>
                    </select>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>



                
                <div class="form-group" >
                    <label for="active" ><span translate="WeekTemplate.Active" ></span></label>
                    <label class="i-switch m-t-xs m-r">
                      <input type="checkbox" id="active" ng-model="active_switch" >
                      <i></i>
                    </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                
              </form>
            </div>
            <div class="panel-footer" >

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(false)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveData" ></span>
                    </button>
                </span>

                <span ng-show="action==='edit'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="save(true)"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="Global.SaveDataAndBack" ></span>
                    </button>
                </span>

                <span ng-show="action==='add'" uib-tooltip="{{ 'Form.PleaseFillAllRequiredFields' | translate }}" tooltip-placement="top" tooltip-trigger="mouseenter" tooltip-enable="!form.$valid" tooltip-class="tooltip-danger zindex9999" >
                    <button ng-disabled="!form.$valid"
                        type="submit" class="btn btn-sm btn-primary" ng-click="add()"
                        >
                        <i class="fa fa-floppy-o" ></i><span translate="WeekTemplate.AddNewWeekTemplate" ></span>
                    </button>
                </span>

                <a ui-sref="week_template.all()" class="btn btn-sm btn-default" >
                    <i class="fa fa-times" aria-hidden="true"></i><span translate="Global.Back" ></span>
                </a>
            </div>


      </div>
    </div>
</div>