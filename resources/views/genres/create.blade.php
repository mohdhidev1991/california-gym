@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'genres.store']) !!}

        @include('genres.fields')

    {!! Form::close() !!}
</div>
@endsection
