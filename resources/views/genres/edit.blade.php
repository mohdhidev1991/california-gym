@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($genre, ['route' => ['genres.update', $genre->id], 'method' => 'patch']) !!}

        @include('genres.fields')

    {!! Form::close() !!}
</div>
@endsection
