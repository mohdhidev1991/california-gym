@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Genres</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('genres.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($genres->isEmpty())
                <div class="well text-center">No Genres found.</div>
            @else
                @include('genres.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $genres])


    </div>
@endsection
