<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Genre Name</th>
			<th>Genre Name2</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($genres as $genre)
        <tr>
            <td>{!! $genre->id !!}</td>
			<td>{!! $genre->creation_user_id !!}</td>
			<td>{!! $genre->creation_date !!}</td>
			<td>{!! $genre->update_user_id !!}</td>
			<td>{!! $genre->update_date !!}</td>
			<td>{!! $genre->validation_user_id !!}</td>
			<td>{!! $genre->validation_date !!}</td>
			<td>{!! $genre->active !!}</td>
			<td>{!! $genre->version !!}</td>
			<td>{!! $genre->update_groups_mfk !!}</td>
			<td>{!! $genre->delete_groups_mfk !!}</td>
			<td>{!! $genre->display_groups_mfk !!}</td>
			<td>{!! $genre->sci_id !!}</td>
			<td>{!! $genre->genre_name !!}</td>
			<td>{!! $genre->genre_name2 !!}</td>
            <td>
                <a href="{!! route('genres.edit', [$genre->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('genres.delete', [$genre->id]) !!}" onclick="return confirm('Are you sure wants to delete this Genre?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
