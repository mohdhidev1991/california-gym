@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($hijraDateBase, ['route' => ['hijraDateBases.update', $hijraDateBase->id], 'method' => 'patch']) !!}

        @include('hijraDateBases.fields')

    {!! Form::close() !!}
</div>
@endsection
