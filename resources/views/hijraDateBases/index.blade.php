@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">HijraDateBases</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('hijraDateBases.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($hijraDateBases->isEmpty())
                <div class="well text-center">No HijraDateBases found.</div>
            @else
                @include('hijraDateBases.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $hijraDateBases])


    </div>
@endsection
