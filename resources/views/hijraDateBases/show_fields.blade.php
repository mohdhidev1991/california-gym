<!-- Hijri Year Field -->
<div class="form-group">
    {!! Form::label('HIJRI_YEAR', 'Hijri Year:') !!}
    <p>{!! $hijraDateBase->HIJRI_YEAR !!}</p>
</div>

<!-- Hijri Month Field -->
<div class="form-group">
    {!! Form::label('HIJRI_MONTH', 'Hijri Month:') !!}
    <p>{!! $hijraDateBase->HIJRI_MONTH !!}</p>
</div>

<!-- Hijri Month Days Field -->
<div class="form-group">
    {!! Form::label('HIJRI_MONTH_DAYS', 'Hijri Month Days:') !!}
    <p>{!! $hijraDateBase->HIJRI_MONTH_DAYS !!}</p>
</div>

<!-- Greg Date Field -->
<div class="form-group">
    {!! Form::label('GREG_DATE', 'Greg Date:') !!}
    <p>{!! $hijraDateBase->GREG_DATE !!}</p>
</div>

<!-- Holiday Days Field -->
<div class="form-group">
    {!! Form::label('HOLIDAY_DAYS', 'Holiday Days:') !!}
    <p>{!! $hijraDateBase->HOLIDAY_DAYS !!}</p>
</div>

