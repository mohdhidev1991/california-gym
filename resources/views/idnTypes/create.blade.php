@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'idnTypes.store']) !!}

        @include('idnTypes.fields')

    {!! Form::close() !!}
</div>
@endsection
