@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($idnType, ['route' => ['idnTypes.update', $idnType->id], 'method' => 'patch']) !!}

        @include('idnTypes.fields')

    {!! Form::close() !!}
</div>
@endsection
