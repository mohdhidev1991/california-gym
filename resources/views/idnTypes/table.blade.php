<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Idn  Type Name</th>
			<th>Idn Validation Function</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($idnTypes as $idnType)
        <tr>
            <td>{!! $idnType->id !!}</td>
			<td>{!! $idnType->creation_user_id !!}</td>
			<td>{!! $idnType->creation_date !!}</td>
			<td>{!! $idnType->update_user_id !!}</td>
			<td>{!! $idnType->update_date !!}</td>
			<td>{!! $idnType->validation_user_id !!}</td>
			<td>{!! $idnType->validation_date !!}</td>
			<td>{!! $idnType->active !!}</td>
			<td>{!! $idnType->version !!}</td>
			<td>{!! $idnType->update_groups_mfk !!}</td>
			<td>{!! $idnType->delete_groups_mfk !!}</td>
			<td>{!! $idnType->display_groups_mfk !!}</td>
			<td>{!! $idnType->sci_id !!}</td>
			<td>{!! $idnType->idn _type_name !!}</td>
			<td>{!! $idnType->idn_validation_function !!}</td>
            <td>
                <a href="{!! route('idnTypes.edit', [$idnType->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('idnTypes.delete', [$idnType->id]) !!}" onclick="return confirm('Are you sure wants to delete this IdnType?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
