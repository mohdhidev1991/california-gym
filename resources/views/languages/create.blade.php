@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'languages.store']) !!}

        @include('languages.fields')

    {!! Form::close() !!}
</div>
@endsection
