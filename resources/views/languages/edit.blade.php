@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($language, ['route' => ['languages.update', $language->id], 'method' => 'patch']) !!}

        @include('languages.fields')

    {!! Form::close() !!}
</div>
@endsection
