<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Update User Id</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Creation Date</th>
			<th>Update Date</th>
			<th>Short Name</th>
			<th>Folder Name</th>
			<th>Long Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($languages as $language)
        <tr>
            <td>{!! $language->id !!}</td>
			<td>{!! $language->creation_user_id !!}</td>
			<td>{!! $language->update_user_id !!}</td>
			<td>{!! $language->validation_user_id !!}</td>
			<td>{!! $language->validation_date !!}</td>
			<td>{!! $language->active !!}</td>
			<td>{!! $language->version !!}</td>
			<td>{!! $language->update_groups_mfk !!}</td>
			<td>{!! $language->delete_groups_mfk !!}</td>
			<td>{!! $language->display_groups_mfk !!}</td>
			<td>{!! $language->sci_id !!}</td>
			<td>{!! $language->creation_date !!}</td>
			<td>{!! $language->update_date !!}</td>
			<td>{!! $language->short_name !!}</td>
			<td>{!! $language->folder_name !!}</td>
			<td>{!! $language->long_name !!}</td>
            <td>
                <a href="{!! route('languages.edit', [$language->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('languages.delete', [$language->id]) !!}" onclick="return confirm('Are you sure wants to delete this Language?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
