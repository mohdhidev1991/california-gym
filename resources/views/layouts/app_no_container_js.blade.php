<!DOCTYPE html>
<html lang="fr" data-ng-app="app" >
<head>
  <meta charset="utf-8" />
  <title>{{'Global.ManagementPlanning' | translate}} | {{'Global.CaliforniaGym' | translate}}</title>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/assets/animate.css/animate.css") !!}" type="text/css" />
  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/assets/simple-line-icons/css/simple-line-icons.css") !!}" type="text/css" />
  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/jquery/bootstrap/dist/css/bootstrap.css") !!}" type="text/css" />
  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/libs/angular/angular-ui-notification/angular-ui-notification.min.css") !!}" type="text/css" />
  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/js/textAngular/textAngular.css") !!}" type="text/css" />
	
  <link href='https://fonts.googleapis.com/css?family=Signika:400,300,600,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/multiple-select.min.css") !!}">

  <!-- build:css css/app.min.css -->
  <!--<link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/font.css") !!}" type="text/css" />-->
  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/app.css") !!}" type="text/css" />
  <!-- endbuild -->


  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/admin.reaaya.css") !!}" type="text/css" />
  <link rel="stylesheet" href="{!! URL::asset("themes/admin/assets/css/admin.reaaya.responsive.css") !!}" type="text/css" />

  
</head>
<body ng-controller="AppCtrl" >
    <div class="app app-header-fixed ">
        <div class="container w-xxl w-auto-xs" ng-init="app.settings.container = false;">
            <div class="text-center" >
                <a href="{!! url('/') !!}" title="{{ 'Global.CaliforniaGym' | translate }}" class="link-logo">
                    <img src="{!! URL::asset("themes/admin/assets/img/logo.png") !!}" />
                </a>
            </div>
            @yield('content')
        </div>
        @if(0)
        <div class="text-center" >
            <ul class="menu-footer-languages">
              <li ng-repeat="(langKey, label) in langs" ng-class="{active: langKey===selectLangKey}" >
                <a ng-click="setLang(langKey, $event)" href>{{label}}</a>
              </li>
            </ul>
        </div>
        @endif
        <div class="text-center">
            <p>
                <small class="text-muted"><span translate="Global.CaliforniaGym" ></span> &copy; 2017</small>
            </p>
        </div>
    </div>
    

    <!-- jQuery -->
  <script src="{!! URL::asset("themes/admin/assets/libs/jquery/jquery/dist/jquery.js") !!}"></script>
  <!-- Bootstrap -->
  <script src="{!! URL::asset("themes/admin/assets/libs/jquery/bootstrap/dist/js/bootstrap.js") !!}"></script>
  <!-- Angular -->
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular/angular.js") !!}"></script>
  
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-animate/angular-animate.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-aria/angular-aria.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-cookies/angular-cookies.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-messages/angular-messages.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-resource/angular-resource.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-sanitize/angular-sanitize.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-touch/angular-touch.js") !!}"></script>
  
  <!-- bootstrap -->
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-ui-router/release/angular-ui-router.js") !!}"></script> 
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/ngstorage/ngStorage.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-ui-utils/ui-utils.js") !!}"></script>

  <!-- bootstrap -->
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-bootstrap/ui-bootstrap-tpls.js") !!}"></script>

  <!-- lazyload -->
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/oclazyload/dist/ocLazyLoad.js") !!}"></script>
  
  <!-- translate -->
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-translate/angular-translate.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-translate-loader-static-files/angular-translate-loader-static-files.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-translate-storage-cookie/angular-translate-storage-cookie.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-translate-storage-local/angular-translate-storage-local.js") !!}"></script>

  <!-- ui-bootstrap-tpls -->
  <script src="{!! URL::asset("themes/admin/assets/libs/assets/ui-bootstrap-tpls/ui-bootstrap-tpls-1.2.5.min.js") !!}"></script>

  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-css/angular-css.min.js") !!}"></script>

  
  <!-- App -->
  <script src="{!! URL::asset("themes/admin/assets/js/app.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/filters/reaaya_filters.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/config.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/config.lazyload.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/services/ui-load.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/filters/fromNow.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/setnganimate.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-butterbar.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-focus.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-fullscreen.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-jq.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-module.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-nav.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-scroll.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-shift.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/directives/ui-toggleclass.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/controllers/bootstrap.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/controllers/bootstrap.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-recaptcha-master/release/angular-recaptcha.min.js") !!}"></script>
  <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit"async defer></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-ui-notification/angular-ui-notification.min.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/angular/angular-truncate/src/truncate.js") !!}"></script>

  <script src="{!! URL::asset("themes/admin/assets/libs/assets/calendar/jquery.plugin.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/assets/calendar/jquery.calendars.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/assets/calendar/jquery.calendars.plus.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/libs/assets/calendar/jquery.calendars.picker.js") !!}"></script>
  
  <script src="{!! URL::asset("themes/admin/assets/js/multiple-select.min.js") !!}"></script>
  
  <!-- textAngular -->
  <script src="{!! URL::asset("themes/admin/assets/js/textAngular/textAngular-rangy.min.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/textAngular/textAngular-sanitize.min.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/textAngular/textAngular.min.js") !!}"></script>
  <!-- textAngular -->
  
  <script src="{!! URL::asset("themes/admin/assets/js/main.js") !!}"></script>
  <script src="{!! URL::asset("themes/admin/assets/js/services/permissions.js") !!}"></script>
  @yield('js_footer')
</body>
</html>