@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'levelClasses.store']) !!}

        @include('levelClasses.fields')

    {!! Form::close() !!}
</div>
@endsection
