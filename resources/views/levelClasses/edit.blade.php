@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($levelClass, ['route' => ['levelClasses.update', $levelClass->id], 'method' => 'patch']) !!}

        @include('levelClasses.fields')

    {!! Form::close() !!}
</div>
@endsection
