@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">LevelClasses</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('levelClasses.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($levelClasses->isEmpty())
                <div class="well text-center">No LevelClasses found.</div>
            @else
                @include('levelClasses.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $levelClasses])


    </div>
@endsection
