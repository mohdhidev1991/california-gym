<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation loiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>School Level Id</th>
			<th>Level Class Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($levelClasses as $levelClass)
        <tr>
            <td>{!! $levelClass->id !!}</td>
			<td>{!! $levelClass->creation_user_id !!}</td>
			<td>{!! $levelClass->creation_date !!}</td>
			<td>{!! $levelClass->update_user_id !!}</td>
			<td>{!! $levelClass->update_date !!}</td>
			<td>{!! $levelClass->validation_user_id !!}</td>
			<td>{!! $levelClass->validation_date !!}</td>
			<td>{!! $levelClass->active !!}</td>
			<td>{!! $levelClass->version !!}</td>
			<td>{!! $levelClass->update_groups_mfk !!}</td>
			<td>{!! $levelClass->delete_groups_mfk !!}</td>
			<td>{!! $levelClass->display_groups_mfk !!}</td>
			<td>{!! $levelClass->sci_id !!}</td>
			<td>{!! $levelClass->school_level_id !!}</td>
			<td>{!! $levelClass->level_class_name !!}</td>
            <td>
                <a href="{!! route('levelClasses.edit', [$levelClass->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('levelClasses.delete', [$levelClass->id]) !!}" onclick="return confirm('Are you sure wants to delete this LevelClass?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
