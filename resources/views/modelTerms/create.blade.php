@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'modelTerms.store']) !!}

        @include('modelTerms.fields')

    {!! Form::close() !!}
</div>
@endsection
