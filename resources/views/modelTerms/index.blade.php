@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">ModelTerms</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('modelTerms.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($modelTerms->isEmpty())
                <div class="well text-center">No ModelTerms found.</div>
            @else
                @include('modelTerms.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $modelTerms])


    </div>
@endsection
