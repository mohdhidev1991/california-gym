<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $modelTerm->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $modelTerm->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $modelTerm->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $modelTerm->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $modelTerm->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $modelTerm->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $modelTerm->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $modelTerm->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $modelTerm->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $modelTerm->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $modelTerm->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $modelTerm->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $modelTerm->sci_id !!}</p>
</div>

<!-- End Study Hdate Field -->
<div class="form-group">
    {!! Form::label('end_study_hdate', 'End Study Hdate:') !!}
    <p>{!! $modelTerm->end_study_hdate !!}</p>
</div>

<!-- End Vacancy Hdate Field -->
<div class="form-group">
    {!! Form::label('end_vacancy_hdate', 'End Vacancy Hdate:') !!}
    <p>{!! $modelTerm->end_vacancy_hdate !!}</p>
</div>

<!-- Start Study Hdate Field -->
<div class="form-group">
    {!! Form::label('start_study_hdate', 'Start Study Hdate:') !!}
    <p>{!! $modelTerm->start_study_hdate !!}</p>
</div>

<!-- Start Vacancy Hdate Field -->
<div class="form-group">
    {!! Form::label('start_vacancy_hdate', 'Start Vacancy Hdate:') !!}
    <p>{!! $modelTerm->start_vacancy_hdate !!}</p>
</div>

<!-- School Id Field -->
<div class="form-group">
    {!! Form::label('school_id', 'School Id:') !!}
    <p>{!! $modelTerm->school_id !!}</p>
</div>

