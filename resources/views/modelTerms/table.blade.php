<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>End Study Hdate</th>
			<th>End Vacancy Hdate</th>
			<th>Start Study Hdate</th>
			<th>Start Vacancy Hdate</th>
			<th>School Id</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($modelTerms as $modelTerm)
        <tr>
            <td>{!! $modelTerm->id !!}</td>
			<td>{!! $modelTerm->creation_user_id !!}</td>
			<td>{!! $modelTerm->creation_date !!}</td>
			<td>{!! $modelTerm->update_user_id !!}</td>
			<td>{!! $modelTerm->update_date !!}</td>
			<td>{!! $modelTerm->validation_user_id !!}</td>
			<td>{!! $modelTerm->validation_date !!}</td>
			<td>{!! $modelTerm->active !!}</td>
			<td>{!! $modelTerm->version !!}</td>
			<td>{!! $modelTerm->update_groups_mfk !!}</td>
			<td>{!! $modelTerm->delete_groups_mfk !!}</td>
			<td>{!! $modelTerm->display_groups_mfk !!}</td>
			<td>{!! $modelTerm->sci_id !!}</td>
			<td>{!! $modelTerm->end_study_hdate !!}</td>
			<td>{!! $modelTerm->end_vacancy_hdate !!}</td>
			<td>{!! $modelTerm->start_study_hdate !!}</td>
			<td>{!! $modelTerm->start_vacancy_hdate !!}</td>
			<td>{!! $modelTerm->school_id !!}</td>
            <td>
                <a href="{!! route('modelTerms.edit', [$modelTerm->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('modelTerms.delete', [$modelTerm->id]) !!}" onclick="return confirm('Are you sure wants to delete this ModelTerm?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
