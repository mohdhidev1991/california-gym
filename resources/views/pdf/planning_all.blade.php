<style>
* {
	    -webkit-box-sizing: border-box;
	    -moz-box-sizing: border-box;
	    box-sizing: border-box;
	}


	body {
	    margin: 0;
	}

	

	/* --- timetable --- */

	table.tt_timetable {

	    width: 100%;
	    color: #666;
	    margin-top: 30px;
	    border: none;
	    font-family: arial;
	    letter-spacing: normal;
	    line-height: normal;
	    font-family: "Lato";
	    font-family: "Helvetica";
	    border-collapse: separate !important;
	    border-spacing: 0 !important;
	    background: #FFF !important;
	
	}

	.event_container_wrapper {
	    position: relative;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	}

	.tt_timetable th,
	.tt_timetable td {
	    font-size: @php echo (isset($_GET['font-size']))? $_GET['font-size'] : '9px' ; @endphp;
	    font-weight: normal;
	    font-style: normal;
	    line-height: normal;
	    color: #34495E;
	    text-transform: none;
	    /*border: 1px solid #34495E !important;*/
	    position: relative;
	}

	.tt_timetable th,
	.tt_timetable td.tt_hours_column {
	    text-align: center;
	    vertical-align: middle;
	}

	.tt_timetable th {
	    padding: 10px 15px 12px;
	    letter-spacing: normal;
	}

	.tt_timetable .event_container {
	     padding: 3px 1px;
	    position: absolute;
	    top: 0;
	    left: 0;
	    height: 100%;
	    width: 100%;
	    z-index: 9;
	    font-size: 0.9em;
	    color: #000;
	    background: #FFF;
	    border: 1px solid #7d7d7d;
	}

	.tt_timetable td {
	    vertical-align: top;
	    height: 27px;
	    border: none;
	    padding: 0;
	    /*height: 100px;*/
	}

	.tt_timetable .hours {
	    color: #FFF;
	    font-weight: bold;
	}

	.tt_timetable .event {
	    background-color: #f5f5f5;
	    color: #000;
	    text-align: center;
	    padding: 0;
	    vertical-align: middle;
	    height: 62px;
	    position: relative;
	}

	.tt_timetable .event a,
	.tt_timetable .event .event_header {
	    font-weight: bold;
	    margin-bottom: 0px;
	    text-decoration: none;
	    outline: none;
	    border: none;
	}

	.tt_timetable .event a:hover,
	.tt_timetable .event a.event_header:hover {
	    text-decoration: underline;
	}

	.tt_timetable .event .before_hour_text,
	.tt_timetable .event .after_hour_text {
	    font-size: 9px;
	}

	.tt_timetable tr {
	    
	}

	.tt_timetable tr.row_bleu {
	    background: #23b7e5;
	    color: #FFF;
	}

	.tt_timetable tr.row_bleu th {
	    color: #FFF;
	    font-size: 16px;
	}

	.tt_timetable tr td {
	    vertical-align: middle;
	    text-align: center;
	    padding: 6px;
	}

	.tt_timetable tr td:first-child {
	    background: #f0f0f0;
	}

	.tt_timetable .row_gray {
	    background-color: #F0F0F0 !important;
	}

	.tt_timetable .event:hover,
	.tt_timetable .event .event_container.tt_tooltip:hover {}

	.tt_timetable .event.tt_tooltip:hover .hours,
	.tt_timetable .event .event_container.tt_tooltip:hover .hours {
	    color: #FFF;
	}

	.tt_timetable .event .hours_container {
	    margin: 2px 0;
	}

	.tt_timetable .event .top_hour {
	    margin-top: 0px;
	}

	.tt_timetable .event .bottom_hour,
	.event_layout_4 .tt_timetable .event .top_hour {
	    margin-bottom: 15px;
	}

	.tt_timetable .event hr {
	    background: #FFFFFF;
	    border: none;
	    height: 1px;
	    margin: 0;
	    opacity: 0.4;
	}

	.tt_timetable.small {
	    display: none;
	    font-size: 1.2em;
	}
.tt_timetable tr th.th-new-enter-day,
.tt_timetable tr td.td-new-enter-day {
    border-left: 3px solid #23b7e5 !important;
}

.tt_timetable tr th:last-child,
.tt_timetable tr td:last-child {
    border-right: 4px solid #23b7e5 !important;
}

.logo_pdf{
   padding :10px;
}

</style>

<div class="logo_pdf">
  <img src="http://gestion-planning.california-gym.com/themes/admin/assets/img/logo.png">
</div>

<table class="tt_timetable">
    <thead>
        <tr class="row_bleu" >
            <th colspan="{!! ($count_rooms*7) + 1 !!}" ><strong>{!! $club->club_name !!}</strong> - <span>Du {!! $date_from !!} au {!! $date_to !!}</span></th>
        </tr>
        <tr class="row_gray" style="background-color: #F0F0F0 !important;">
            <th style="width: 20px;" ></th>
            @if (in_array(1, $days_to_show))<th colspan="{!! $count_rooms !!}" class="th-new-enter-day" style="font-weight:bold;font-size:10px;border:3px solid #23b7e5 !important">{!! $week_day1 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(0)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(2, $days_to_show))<th colspan="{!! $count_rooms !!}" class="th-new-enter-day" style="font-weight:bold;font-size:10px;border:3px solid #23b7e5">{!! $week_day2 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(1)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(3, $days_to_show))<th colspan="{!! $count_rooms !!}" class="th-new-enter-day" style="font-weight:bold;font-size:10px;border:3px solid #23b7e5">{!! $week_day3 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(2)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(4, $days_to_show))<th colspan="{!! $count_rooms !!}" class="th-new-enter-day" style="font-weight:bold;font-size:10px;border:3px solid #23b7e5">{!! $week_day4 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(3)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(5, $days_to_show))<th colspan="{!! $count_rooms !!}" class="th-new-enter-day" style="font-weight:bold;font-size:10px;border:3px solid #23b7e5">{!! $week_day5 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(4)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(6, $days_to_show))<th colspan="{!! $count_rooms !!}" class="th-new-enter-day" style="font-weight:bold;font-size:10px;border:3px solid #23b7e5">{!! $week_day6 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(5)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(7, $days_to_show))<th colspan="{!! $count_rooms !!}" class="th-new-enter-day" style="font-weight:bold;font-size:10px;border:3px solid #23b7e5">{!! $week_day7 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(6)->format('d-m'); @endphp</span></th>@endif
        </tr>
        <tr class="row_gray" style="background-color: #F0F0F0 !important;">
            <th></th>
            @for ($kd = 1; $kd <= 7; $kd++)
            	@php $new_enter_day = true; @endphp
            	@foreach ($rooms as $room)
            		@if (in_array($kd, $days_to_show))
            			<th class="@php if($new_enter_day) echo 'th-new-enter-day'; @endphp" style="border:1px solid #333 !important" >{!! $room->room_name !!}</th>
            			@php $new_enter_day = false; @endphp
            		@endif
            	@endforeach
            @endfor
			
        </tr>
    </thead>
    <tbody>
    	@for ($i = 1; $i <= $diff_hours; $i++)
	    	<tr class="row_1">
	    		<td style="font-weight:bold;font-size:10px;border:1px solid #333">@php
	    			$table_hour = \Carbon\Carbon::createFromFormat('H:i',$min_hour)->addHours($i-1)->format('H:i');
	    			$nex_table_hour = \Carbon\Carbon::createFromFormat('H:i',$min_hour)->addHours($i)->format('H:i');
	    			echo $table_hour;
	    		@endphp</td>
	        	@for ($num_day = 1; $num_day <= 7; $num_day++)
	        		@php $new_enter_day = true; @endphp
	        			@foreach ($rooms as $room)
		        			@if (in_array($num_day, $days_to_show))
							    <td class="event tt_single_event @php if($new_enter_day) echo 'td-new-enter-day'; @endphp" style="border:1px solid #cbc5c5">
							    	<div class="event_container_wrapper" >
						                @foreach ($course_sessions_days[$num_day] as $course_session)
						                	@if( ($course_session->room_id==$room->id) && ($course_session->course_start_time>=$table_hour) && ($course_session->course_start_time<$nex_table_hour) )
											    
											    @if($course_session->course_id != NULL) 
												
												@if($course_session->course->logo_file_id)
												<div @if($course_session->course)
											    style="height: @php echo ($course_session->during_hours*100).'%' @endphp; top: @php echo ((\Carbon\Carbon::createFromFormat('H:i',$course_session->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$table_hour))/60)*100).'%'; @endphp"
											    @endif
											    class="event_container id-{!! $course_session->id !!}"
											    >@if($course_session->course)
											    	<span class="event_header" title="{!! $course_session->course->course_name !!}"
											    > {!! $course_session->course->course_name !!}<br><img  src="http://gestion-planning.california-gym.com/file/public/{!! $course_session->course->logo_file_id !!}" width="30px;" height="10px;"></span>
											    @endif
							                    	<div class="top_hour">
							                    		@if($course_session->coach)<span>{!! $course_session->coach->firstname !!}</span>@endif
							                    		<!-- | <span>{!! $course_session->course_start_time !!} - {!! $course_session->course_end_time !!}</span>-->
							                    	</div>
							                	</div>
												@endif


												@if($course_session->course->logo_file_id == NULL) 
												<div @if($course_session->course)
											    style="background: @php echo ($course_session->course->color) @endphp;height: @php echo ($course_session->during_hours*100).'%' @endphp; top: @php echo ((\Carbon\Carbon::createFromFormat('H:i',$course_session->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$table_hour))/60)*100).'%'; @endphp"
											    @endif
											    class="event_container id-{!! $course_session->id !!}"
											    >@if($course_session->course)
											    	<span class="event_header" title="{!! $course_session->course->course_name !!}"
											    > {!! $course_session->course->course_name !!}</span>
											    @endif
							                    	<div class="top_hour">
							                    		@if($course_session->coach)<span>{!! $course_session->coach->firstname !!}</span>@endif
							                    		<!-- | <span>{!! $course_session->course_start_time !!} - {!! $course_session->course_end_time !!}</span>-->
							                    	</div>
							                	</div>
												@endif


												@endif


												@if($course_session->course_id == NULL) 
                                                <div @if($course_session->description_course)
											    style="height: @php echo ($course_session->during_hours*100).'%' @endphp; top: @php echo ((\Carbon\Carbon::createFromFormat('H:i',$course_session->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$table_hour))/60)*100).'%'; @endphp"
											    @endif
											    class="event_container id-{!! $course_session->id !!}"
											    >@if($course_session->description_course)
											    	<span class="event_header" title="{!! $course_session->description_course !!}"
											    > {!! $course_session->description_course !!}</span>
											    @endif
							                    	<div class="top_hour">
							                    		@if($course_session->coach)<span>{!! $course_session->coach->firstname !!}</span>@endif
							                    		<!-- | <span>{!! $course_session->course_start_time !!} - {!! $course_session->course_end_time !!}</span>-->
							                    	</div>
							                	</div>
                                                @endif


						                	@endif
										@endforeach
									</div>
				            	</td>
				            @endif
		            	@php $new_enter_day = false; @endphp
	            	@endforeach
				@endfor
	        </tr>
        @endfor
    </tbody>
</table>
