<style>


* {
	    -webkit-box-sizing: border-box;
	    -moz-box-sizing: border-box;
	    box-sizing: border-box;
	}


	body {
	    margin: 0;
	}
    

	.cube {
  
    position:relative;
    margin-top:-20px;
    
    }
 
	.cube:hover .cube-titre{
		opacity:1;
		min-height:35px;
	}
	
	.cube-titre {
		opacity:0;
		position:relative;
		bottom: 0px;
		text-align: center;
	    background: #fff;
		border:1px solid #333;
		color: #000;
	    margin-left:10px;
		margin-top:15px;
		border-radius:3px;
		-webkit-transition: all 0.2s ease;
		-moz-transition: all 0.2s ease;
		-ms-transition: all 0.2s ease;
		-o-transition: all 0.2s ease;
			transition: all 0.2s ease;
	}
	

	/* --- timetable --- */

	table.tt_timetable {
	    width: 100%;
	    color: #666;
	    margin-top: 30px;
	    border: none;
	    font-family: arial;
	    letter-spacing: normal;
	    line-height: normal;
	    font-family: "Lato";
	    border-collapse: separate !important;
	    border-spacing: 2px !important;
	    background: #FFF !important;
	}

	.event_container_wrapper {
	    position: relative;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	}

	.tt_timetable th,
	.tt_timetable td {
	    font-size: @php echo (isset($_GET['font-size']))? $_GET['font-size'] : '11px' ; @endphp;
	    font-weight: normal;
	    font-style: normal;
	    line-height: normal;
	    color: #34495E;
	    text-transform: none;
	    border: none !important;
	    position: relative;
	}

	.tt_timetable th,
	.tt_timetable td.tt_hours_column {
	    text-align: center;
	    vertical-align: middle;
	}

	.tt_timetable th {
	    width: 12.5%;
	    padding: 10px 15px 12px;
	    letter-spacing: normal;
	}

	.tt_timetable .event_container {
	    padding: 2px 1px;
	    position: absolute;
	    top: 0;
	    left: 0;
	    height: 100%;
	    width: 100%;
	    z-index: 9;
	    font-size: 0.9em;
	    color: #FFF;
	    background: #CCC;
	}

	.tt_timetable td {
	    width: 12.5%;
	    vertical-align: top;
	    height: 27px;
	    border: none;
	    padding: 0;
	    /*height: 100px;*/
	}

	.tt_timetable .hours {
	    color: #FFF;
	    font-weight: bold;
	}

	.tt_timetable .event {
	    background-color: #f5f5f5;
	    color: #000;
	    text-align: center;
	    padding: 0;
	    vertical-align: middle;
	    height: 68px;
	    position: relative;
	}

	.tt_timetable .event a,
	.tt_timetable .event .event_header {
	    color: #FFF;
	    font-weight: bold;
	    margin-bottom: 0px;
	    text-decoration: none;
	    outline: none;
	    border: none;
	}

	.tt_timetable .event a:hover,
	.tt_timetable .event a.event_header:hover {
	    text-decoration: underline;
	}

	.tt_timetable .event .before_hour_text,
	.tt_timetable .event .after_hour_text {
	    font-size: 1.1em;
	}

	.tt_timetable tr {
	    
	}

	.tt_timetable tr.row_bleu {
	    background: #23b7e5;
	    color: #FFF;
	}

	.tt_timetable tr.row_bleu th {
	    color: #FFF;
	    font-size: 16px;
	}

	.tt_timetable tr td {
	    vertical-align: middle;
	    text-align: center;
	    padding: 6px;
	}

	.tt_timetable tr td:first-child {
	    background: #f0f0f0;
	}

	.tt_timetable .row_gray {
	    background-color: #F0F0F0 !important;
	}

	.tt_timetable .event:hover,
	.tt_timetable .event .event_container.tt_tooltip:hover {}

	.tt_timetable .event.tt_tooltip:hover .hours,
	.tt_timetable .event .event_container.tt_tooltip:hover .hours {
	    color: #FFF;
	}

	.tt_timetable .event .hours_container {
	    margin: 2px 0;
	}

	.tt_timetable .event .top_hour {
	    margin-top: 0px;
	}

	.tt_timetable .event .bottom_hour,
	.event_layout_4 .tt_timetable .event .top_hour {
	    margin-bottom: 15px;
	}

	.tt_timetable .event hr {
	    background: #FFFFFF;
	    border: none;
	    height: 1px;
	    margin: 0;
	    opacity: 0.4;
	}

	.tt_timetable.small {
	    display: none;
	    font-size: 1.2em;
	}
</style>





<table class="tt_timetable">
    <thead>
        <tr class="row_bleu" >
		
            <th colspan="8" ><span>Du {!! $date_from !!} au {!! $date_to !!}</span></th>
        </tr>
        <tr class="row_gray" style="background-color: #F0F0F0 !important;">
            
			<th style="width: 20px;" ></th>
            @if (in_array(1, $days_to_show))<th>{!! $week_day1 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(0)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(2, $days_to_show))<th>{!! $week_day2 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(1)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(3, $days_to_show))<th>{!! $week_day3 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(2)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(4, $days_to_show))<th>{!! $week_day4 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(3)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(5, $days_to_show))<th>{!! $week_day5 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(4)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(6, $days_to_show))<th>{!! $week_day6 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(5)->format('d-m'); @endphp</span></th>@endif
            @if (in_array(7, $days_to_show))<th>{!! $week_day7 !!}<br><span>@php echo \Carbon\Carbon::parse($date_from)->addDays(6)->format('d-m'); @endphp</span></th>@endif
        
		</tr>

    </thead>
    <tbody data-diff_hours="{!! $diff_hours !!}">
    	@for ($i = 1; $i <= $diff_hours; $i++)
	    	<tr class="row_1">
	    		
				
				<td>
				@php
	    			$table_hour = \Carbon\Carbon::createFromFormat('H:i',$min_hour)->addHours($i-1)->format('H:i');
	    			$nex_table_hour = \Carbon\Carbon::createFromFormat('H:i',$min_hour)->addHours($i)->format('H:i');
	    			echo $table_hour;
	    		@endphp
				</td>

	        	@for ($num_day = 1; $num_day <= 7; $num_day++)
				    @if (in_array($num_day, $days_to_show))
				    	<td class="event tt_single_event" >
							<div class="event_container_wrapper" >
								@foreach ($course_sessions_days[$num_day] as $course_session)
									@if( ($course_session->course_start_time>=$table_hour) && ($course_session->course_start_time<$nex_table_hour) )
										
									   
									     
                                        
                                        <div   style="height: @php echo ($course_session->during_hours*100).'%' @endphp; top: @php echo ((\Carbon\Carbon::createFromFormat('H:i',$course_session->course_start_time)->diffInMinutes(\Carbon\Carbon::createFromFormat('H:i',$table_hour))/60)*100).'%' @endphp; background:@php if($course_session->type_course == 'Autre'){echo '#00ff00';}else{ echo $course_session->course->color; } @endphp;" class="event_container id-{!! $course_session->id !!}"><span class="event_header" title="@php if($course_session->type_course == 'Autre'){ echo($course_session->description_course); }else{ echo($course_session->course->course_name);  } @endphp;">@php if($course_session->type_course == 'Autre'){ echo($course_session->description_course); }else{ echo($course_session->course->course_name);} @endphp;</span>
											
										    @if($course_session->coach)
											<div class="top_hour"><span>{!! $course_session->coach->firstname !!}</span>|<span>{!! $course_session->course_start_time !!} - {!! $course_session->course_end_time !!}</span>
											
                                                    

													@if($course_session->active == "I")
													<div style="background:red;padding:2px;margin-top:5px;">
													Cours non validée
													</div>
													@endif

													<div class="cube">
														<div class="cube-titre">
														{!! $course_session->coach->descenrichi !!}
														</div>      
													</div>
											
											</div>
											@endif
                                            
											

										</div>
									@endif
								@endforeach
							</div>
	            		</td>
	            	@endif
				@endfor
	        </tr>
        @endfor
    </tbody>

</table>

