<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Period Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($periods as $period)
        <tr>
            <td>{!! $period->id !!}</td>
			<td>{!! $period->creation_user_id !!}</td>
			<td>{!! $period->creation_date !!}</td>
			<td>{!! $period->update_user_id !!}</td>
			<td>{!! $period->update_date !!}</td>
			<td>{!! $period->validation_user_id !!}</td>
			<td>{!! $period->validation_date !!}</td>
			<td>{!! $period->active !!}</td>
			<td>{!! $period->version !!}</td>
			<td>{!! $period->update_groups_mfk !!}</td>
			<td>{!! $period->delete_groups_mfk !!}</td>
			<td>{!! $period->display_groups_mfk !!}</td>
			<td>{!! $period->sci_id !!}</td>
			<td>{!! $period->period_name !!}</td>
            <td>
                <a href="{!! route('periods.edit', [$period->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('periods.delete', [$period->id]) !!}" onclick="return confirm('Are you sure wants to delete this Period?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
