@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'ratings.store']) !!}

        @include('ratings.fields')

    {!! Form::close() !!}
</div>
@endsection
