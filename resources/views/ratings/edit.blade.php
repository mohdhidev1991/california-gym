@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($rating, ['route' => ['ratings.update', $rating->id], 'method' => 'patch']) !!}

        @include('ratings.fields')

    {!! Form::close() !!}
</div>
@endsection
