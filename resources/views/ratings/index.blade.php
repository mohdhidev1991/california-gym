@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Ratings</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('ratings.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($ratings->isEmpty())
                <div class="well text-center">No Ratings found.</div>
            @else
                @include('ratings.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $ratings])


    </div>
@endsection
