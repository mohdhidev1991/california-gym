<!-- Lookup Code Field -->
<div class="form-group">
    {!! Form::label('lookup_code', 'Lookup Code:') !!}
    <p>{!! $rating->lookup_code !!}</p>
</div>

<!-- Id School Field -->
<div class="form-group">
    {!! Form::label('id_school', 'Id School:') !!}
    <p>{!! $rating->id_school !!}</p>
</div>

<!-- Max Pct Field -->
<div class="form-group">
    {!! Form::label('max_pct', 'Max Pct:') !!}
    <p>{!! $rating->max_pct !!}</p>
</div>

<!-- Min Pct Field -->
<div class="form-group">
    {!! Form::label('min_pct', 'Min Pct:') !!}
    <p>{!! $rating->min_pct !!}</p>
</div>

<!-- Rating Name Field -->
<div class="form-group">
    {!! Form::label('rating_name', 'Rating Name:') !!}
    <p>{!! $rating->rating_name !!}</p>
</div>

