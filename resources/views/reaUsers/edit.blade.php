@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($reaUser, ['route' => ['reaUsers.update', $reaUser->id], 'method' => 'patch']) !!}

        @include('reaUsers.fields')

    {!! Form::close() !!}
</div>
@endsection
