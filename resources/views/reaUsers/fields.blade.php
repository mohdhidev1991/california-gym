<!-- Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creation User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
	{!! Form::number('creation_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creation Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('creation_date', 'Creation Date:') !!}
	{!! Form::date('creation_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Update User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
	{!! Form::number('update_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Update Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_date', 'Update Date:') !!}
	{!! Form::date('update_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Validation User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
	{!! Form::number('validation_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Validation Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('validation_date', 'Validation Date:') !!}
	{!! Form::date('validation_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('active', 'Active:') !!}
	{!! Form::text('active', null, ['class' => 'form-control']) !!}
</div>

<!-- Version Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('version', 'Version:') !!}
	{!! Form::number('version', null, ['class' => 'form-control']) !!}
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
	{!! Form::text('update_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
	{!! Form::text('delete_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
	{!! Form::text('display_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Sci Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sci_id', 'Sci Id:') !!}
	{!! Form::number('sci_id', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('city_id', 'City Id:') !!}
	{!! Form::number('city_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('country_id', 'Country Id:') !!}
	{!! Form::number('country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Current School Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('current_school_id', 'Current School Id:') !!}
	{!! Form::number('current_school_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Idn Type Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('idn_type_id', 'Idn Type Id:') !!}
	{!! Form::number('idn_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('address', 'Address:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('cp', 'Cp:') !!}
	{!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('firstname', 'Firstname:') !!}
	{!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- F Firstname Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('f_firstname', 'F Firstname:') !!}
	{!! Form::text('f_firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Idn Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('idn', 'Idn:') !!}
	{!! Form::text('idn', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('lastname', 'Lastname:') !!}
	{!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('mobile', 'Mobile:') !!}
	{!! Form::text('mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Pwd Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('pwd', 'Pwd:') !!}
	{!! Form::text('pwd', null, ['class' => 'form-control']) !!}
</div>

<!-- Quarter Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('quarter', 'Quarter:') !!}
	{!! Form::text('quarter', null, ['class' => 'form-control']) !!}
</div>

<!-- Sim Card Sn Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sim_card_sn', 'Sim Card Sn:') !!}
	{!! Form::text('sim_card_sn', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
