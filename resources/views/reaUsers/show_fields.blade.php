<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reaUser->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $reaUser->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $reaUser->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $reaUser->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $reaUser->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $reaUser->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $reaUser->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $reaUser->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $reaUser->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $reaUser->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $reaUser->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $reaUser->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $reaUser->sci_id !!}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', 'City Id:') !!}
    <p>{!! $reaUser->city_id !!}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{!! $reaUser->country_id !!}</p>
</div>

<!-- Current School Id Field -->
<div class="form-group">
    {!! Form::label('current_school_id', 'Current School Id:') !!}
    <p>{!! $reaUser->current_school_id !!}</p>
</div>

<!-- Idn Type Id Field -->
<div class="form-group">
    {!! Form::label('idn_type_id', 'Idn Type Id:') !!}
    <p>{!! $reaUser->idn_type_id !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $reaUser->address !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $reaUser->cp !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $reaUser->firstname !!}</p>
</div>

<!-- F Firstname Field -->
<div class="form-group">
    {!! Form::label('f_firstname', 'F Firstname:') !!}
    <p>{!! $reaUser->f_firstname !!}</p>
</div>

<!-- Idn Field -->
<div class="form-group">
    {!! Form::label('idn', 'Idn:') !!}
    <p>{!! $reaUser->idn !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $reaUser->lastname !!}</p>
</div>

<!-- Mobile Field -->
<div class="form-group">
    {!! Form::label('mobile', 'Mobile:') !!}
    <p>{!! $reaUser->mobile !!}</p>
</div>

<!-- Pwd Field -->
<div class="form-group">
    {!! Form::label('pwd', 'Pwd:') !!}
    <p>{!! $reaUser->pwd !!}</p>
</div>

<!-- Quarter Field -->
<div class="form-group">
    {!! Form::label('quarter', 'Quarter:') !!}
    <p>{!! $reaUser->quarter !!}</p>
</div>

<!-- Sim Card Sn Field -->
<div class="form-group">
    {!! Form::label('sim_card_sn', 'Sim Card Sn:') !!}
    <p>{!! $reaUser->sim_card_sn !!}</p>
</div>

