<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>City Id</th>
			<th>Country Id</th>
			<th>Current School Id</th>
			<th>Idn Type Id</th>
			<th>Address</th>
			<th>Cp</th>
			<th>Firstname</th>
			<th>F Firstname</th>
			<th>Idn</th>
			<th>Lastname</th>
			<th>Mobile</th>
			<th>Pwd</th>
			<th>Quarter</th>
			<th>Sim Card Sn</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($reaUsers as $reaUser)
        <tr>
            <td>{!! $reaUser->id !!}</td>
			<td>{!! $reaUser->creation_user_id !!}</td>
			<td>{!! $reaUser->creation_date !!}</td>
			<td>{!! $reaUser->update_user_id !!}</td>
			<td>{!! $reaUser->update_date !!}</td>
			<td>{!! $reaUser->validation_user_id !!}</td>
			<td>{!! $reaUser->validation_date !!}</td>
			<td>{!! $reaUser->active !!}</td>
			<td>{!! $reaUser->version !!}</td>
			<td>{!! $reaUser->update_groups_mfk !!}</td>
			<td>{!! $reaUser->delete_groups_mfk !!}</td>
			<td>{!! $reaUser->display_groups_mfk !!}</td>
			<td>{!! $reaUser->sci_id !!}</td>
			<td>{!! $reaUser->city_id !!}</td>
			<td>{!! $reaUser->country_id !!}</td>
			<td>{!! $reaUser->current_school_id !!}</td>
			<td>{!! $reaUser->idn_type_id !!}</td>
			<td>{!! $reaUser->address !!}</td>
			<td>{!! $reaUser->cp !!}</td>
			<td>{!! $reaUser->firstname !!}</td>
			<td>{!! $reaUser->f_firstname !!}</td>
			<td>{!! $reaUser->idn !!}</td>
			<td>{!! $reaUser->lastname !!}</td>
			<td>{!! $reaUser->mobile !!}</td>
			<td>{!! $reaUser->pwd !!}</td>
			<td>{!! $reaUser->quarter !!}</td>
			<td>{!! $reaUser->sim_card_sn !!}</td>
            <td>
                <a href="{!! route('reaUsers.edit', [$reaUser->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('reaUsers.delete', [$reaUser->id]) !!}" onclick="return confirm('Are you sure wants to delete this ReaUser?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
