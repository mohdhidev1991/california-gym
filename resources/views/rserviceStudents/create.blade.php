@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'rserviceStudents.store']) !!}

        @include('rserviceStudents.fields')

    {!! Form::close() !!}
</div>
@endsection
