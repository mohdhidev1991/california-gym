@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($rserviceStudent, ['route' => ['rserviceStudents.update', $rserviceStudent->id], 'method' => 'patch']) !!}

        @include('rserviceStudents.fields')

    {!! Form::close() !!}
</div>
@endsection
