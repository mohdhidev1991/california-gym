<!-- School Year Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_year_id', 'School Year Id:') !!}
	{!! Form::number('school_year_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rservice Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rservice_id', 'Rservice Id:') !!}
	{!! Form::number('rservice_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Student Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('student_id', 'Student Id:') !!}
	{!! Form::number('student_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Parent User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('parent_user_id', 'Parent User Id:') !!}
	{!! Form::number('parent_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Period Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('payment_period_id', 'Payment Period Id:') !!}
	{!! Form::number('payment_period_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Hdate Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('start_hdate', 'Start Hdate:') !!}
	{!! Form::text('start_hdate', null, ['class' => 'form-control']) !!}
</div>

<!-- Expire Hdate Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('expire_hdate', 'Expire Hdate:') !!}
	{!! Form::text('expire_hdate', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('amount', 'Amount:') !!}
	{!! Form::number('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('currency_id', 'Currency Id:') !!}
	{!! Form::number('currency_id', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
