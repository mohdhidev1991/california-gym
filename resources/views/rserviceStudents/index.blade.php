@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">RserviceStudents</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('rserviceStudents.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($rserviceStudents->isEmpty())
                <div class="well text-center">No RserviceStudents found.</div>
            @else
                @include('rserviceStudents.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $rserviceStudents])


    </div>
@endsection
