<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $rserviceStudent->id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $rserviceStudent->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $rserviceStudent->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $rserviceStudent->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $rserviceStudent->updated_at !!}</p>
</div>

<!-- Validated By Field -->
<div class="form-group">
    {!! Form::label('validated_by', 'Validated By:') !!}
    <p>{!! $rserviceStudent->validated_by !!}</p>
</div>

<!-- Validated At Field -->
<div class="form-group">
    {!! Form::label('validated_at', 'Validated At:') !!}
    <p>{!! $rserviceStudent->validated_at !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $rserviceStudent->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $rserviceStudent->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $rserviceStudent->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $rserviceStudent->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $rserviceStudent->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $rserviceStudent->sci_id !!}</p>
</div>

<!-- School Year Id Field -->
<div class="form-group">
    {!! Form::label('school_year_id', 'School Year Id:') !!}
    <p>{!! $rserviceStudent->school_year_id !!}</p>
</div>

<!-- Rservice Id Field -->
<div class="form-group">
    {!! Form::label('rservice_id', 'Rservice Id:') !!}
    <p>{!! $rserviceStudent->rservice_id !!}</p>
</div>

<!-- Student Id Field -->
<div class="form-group">
    {!! Form::label('student_id', 'Student Id:') !!}
    <p>{!! $rserviceStudent->student_id !!}</p>
</div>

<!-- Parent User Id Field -->
<div class="form-group">
    {!! Form::label('parent_user_id', 'Parent User Id:') !!}
    <p>{!! $rserviceStudent->parent_user_id !!}</p>
</div>

<!-- Payment Period Id Field -->
<div class="form-group">
    {!! Form::label('payment_period_id', 'Payment Period Id:') !!}
    <p>{!! $rserviceStudent->payment_period_id !!}</p>
</div>

<!-- Start Hdate Field -->
<div class="form-group">
    {!! Form::label('start_hdate', 'Start Hdate:') !!}
    <p>{!! $rserviceStudent->start_hdate !!}</p>
</div>

<!-- Expire Hdate Field -->
<div class="form-group">
    {!! Form::label('expire_hdate', 'Expire Hdate:') !!}
    <p>{!! $rserviceStudent->expire_hdate !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $rserviceStudent->amount !!}</p>
</div>

<!-- Currency Id Field -->
<div class="form-group">
    {!! Form::label('currency_id', 'Currency Id:') !!}
    <p>{!! $rserviceStudent->currency_id !!}</p>
</div>

