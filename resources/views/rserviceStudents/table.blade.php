<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>School Year Id</th>
			<th>Rservice Id</th>
			<th>Student Id</th>
			<th>Parent User Id</th>
			<th>Payment Period Id</th>
			<th>Start Hdate</th>
			<th>Expire Hdate</th>
			<th>Amount</th>
			<th>Currency Id</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($rserviceStudents as $rserviceStudent)
        <tr>
            <td>{!! $rserviceStudent->id !!}</td>
			<td>{!! $rserviceStudent->created_by !!}</td>
			<td>{!! $rserviceStudent->created_at !!}</td>
			<td>{!! $rserviceStudent->updated_by !!}</td>
			<td>{!! $rserviceStudent->updated_at !!}</td>
			<td>{!! $rserviceStudent->validated_by !!}</td>
			<td>{!! $rserviceStudent->validated_at !!}</td>
			<td>{!! $rserviceStudent->active !!}</td>
			<td>{!! $rserviceStudent->version !!}</td>
			<td>{!! $rserviceStudent->update_groups_mfk !!}</td>
			<td>{!! $rserviceStudent->delete_groups_mfk !!}</td>
			<td>{!! $rserviceStudent->display_groups_mfk !!}</td>
			<td>{!! $rserviceStudent->sci_id !!}</td>
			<td>{!! $rserviceStudent->school_year_id !!}</td>
			<td>{!! $rserviceStudent->rservice_id !!}</td>
			<td>{!! $rserviceStudent->student_id !!}</td>
			<td>{!! $rserviceStudent->parent_user_id !!}</td>
			<td>{!! $rserviceStudent->payment_period_id !!}</td>
			<td>{!! $rserviceStudent->start_hdate !!}</td>
			<td>{!! $rserviceStudent->expire_hdate !!}</td>
			<td>{!! $rserviceStudent->amount !!}</td>
			<td>{!! $rserviceStudent->currency_id !!}</td>
            <td>
                <a href="{!! route('rserviceStudents.edit', [$rserviceStudent->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('rserviceStudents.delete', [$rserviceStudent->id]) !!}" onclick="return confirm('Are you sure wants to delete this RserviceStudent?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
