@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'rservices.store']) !!}

        @include('rservices.fields')

    {!! Form::close() !!}
</div>
@endsection
