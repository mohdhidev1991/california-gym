@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($rservice, ['route' => ['rservices.update', $rservice->id], 'method' => 'patch']) !!}

        @include('rservices.fields')

    {!! Form::close() !!}
</div>
@endsection
