<!-- Lookup Code Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('lookup_code', 'Lookup Code:') !!}
	{!! Form::text('lookup_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Rservice Name Ar Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rservice_name_ar', 'Rservice Name Ar:') !!}
	{!! Form::text('rservice_name_ar', null, ['class' => 'form-control']) !!}
</div>

<!-- Rservice Name En Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rservice_name_en', 'Rservice Name En:') !!}
	{!! Form::text('rservice_name_en', null, ['class' => 'form-control']) !!}
</div>

<!-- For Parent Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('for_parent', 'For Parent:') !!}
	{!! Form::text('for_parent', null, ['class' => 'form-control']) !!}
</div>

<!-- For School Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('for_school', 'For School:') !!}
	{!! Form::text('for_school', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('price', 'Price:') !!}
	{!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('currency_id', 'Currency Id:') !!}
	{!! Form::number('currency_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Period Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('payment_period_mfk', 'Payment Period Mfk:') !!}
	{!! Form::text('payment_period_mfk', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
