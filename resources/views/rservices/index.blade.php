@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Rservices</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('rservices.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($rservices->isEmpty())
                <div class="well text-center">No Rservices found.</div>
            @else
                @include('rservices.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $rservices])


    </div>
@endsection
