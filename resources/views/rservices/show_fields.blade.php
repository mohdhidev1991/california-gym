<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $rservice->id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $rservice->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $rservice->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $rservice->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $rservice->updated_at !!}</p>
</div>

<!-- Validated By Field -->
<div class="form-group">
    {!! Form::label('validated_by', 'Validated By:') !!}
    <p>{!! $rservice->validated_by !!}</p>
</div>

<!-- Validated At Field -->
<div class="form-group">
    {!! Form::label('validated_at', 'Validated At:') !!}
    <p>{!! $rservice->validated_at !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $rservice->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $rservice->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $rservice->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $rservice->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $rservice->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $rservice->sci_id !!}</p>
</div>

<!-- Lookup Code Field -->
<div class="form-group">
    {!! Form::label('lookup_code', 'Lookup Code:') !!}
    <p>{!! $rservice->lookup_code !!}</p>
</div>

<!-- Rservice Name Ar Field -->
<div class="form-group">
    {!! Form::label('rservice_name_ar', 'Rservice Name Ar:') !!}
    <p>{!! $rservice->rservice_name_ar !!}</p>
</div>

<!-- Rservice Name En Field -->
<div class="form-group">
    {!! Form::label('rservice_name_en', 'Rservice Name En:') !!}
    <p>{!! $rservice->rservice_name_en !!}</p>
</div>

<!-- For Parent Field -->
<div class="form-group">
    {!! Form::label('for_parent', 'For Parent:') !!}
    <p>{!! $rservice->for_parent !!}</p>
</div>

<!-- For School Field -->
<div class="form-group">
    {!! Form::label('for_school', 'For School:') !!}
    <p>{!! $rservice->for_school !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $rservice->price !!}</p>
</div>

<!-- Currency Id Field -->
<div class="form-group">
    {!! Form::label('currency_id', 'Currency Id:') !!}
    <p>{!! $rservice->currency_id !!}</p>
</div>

<!-- Payment Period Mfk Field -->
<div class="form-group">
    {!! Form::label('payment_period_mfk', 'Payment Period Mfk:') !!}
    <p>{!! $rservice->payment_period_mfk !!}</p>
</div>

