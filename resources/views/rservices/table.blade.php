<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Lookup Code</th>
			<th>Rservice Name Ar</th>
			<th>Rservice Name En</th>
			<th>For Parent</th>
			<th>For School</th>
			<th>Price</th>
			<th>Currency Id</th>
			<th>Payment Period Mfk</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($rservices as $rservice)
        <tr>
            <td>{!! $rservice->id !!}</td>
			<td>{!! $rservice->created_by !!}</td>
			<td>{!! $rservice->created_at !!}</td>
			<td>{!! $rservice->updated_by !!}</td>
			<td>{!! $rservice->updated_at !!}</td>
			<td>{!! $rservice->validated_by !!}</td>
			<td>{!! $rservice->validated_at !!}</td>
			<td>{!! $rservice->active !!}</td>
			<td>{!! $rservice->version !!}</td>
			<td>{!! $rservice->update_groups_mfk !!}</td>
			<td>{!! $rservice->delete_groups_mfk !!}</td>
			<td>{!! $rservice->display_groups_mfk !!}</td>
			<td>{!! $rservice->sci_id !!}</td>
			<td>{!! $rservice->lookup_code !!}</td>
			<td>{!! $rservice->rservice_name_ar !!}</td>
			<td>{!! $rservice->rservice_name_en !!}</td>
			<td>{!! $rservice->for_parent !!}</td>
			<td>{!! $rservice->for_school !!}</td>
			<td>{!! $rservice->price !!}</td>
			<td>{!! $rservice->currency_id !!}</td>
			<td>{!! $rservice->payment_period_mfk !!}</td>
            <td>
                <a href="{!! route('rservices.edit', [$rservice->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('rservices.delete', [$rservice->id]) !!}" onclick="return confirm('Are you sure wants to delete this Rservice?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
