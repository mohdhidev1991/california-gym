@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'schoolClasses.store']) !!}

        @include('schoolClasses.fields')

    {!! Form::close() !!}
</div>
@endsection
