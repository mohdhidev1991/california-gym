@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolClasses</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolClasses.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolClasses->isEmpty())
                <div class="well text-center">No SchoolClasses found.</div>
            @else
                @include('schoolClasses.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolClasses])


    </div>
@endsection
