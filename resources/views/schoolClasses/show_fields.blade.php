<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $schoolClass->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $schoolClass->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $schoolClass->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $schoolClass->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $schoolClass->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $schoolClass->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $schoolClass->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $schoolClass->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $schoolClass->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $schoolClass->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $schoolClass->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $schoolClass->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $schoolClass->sci_id !!}</p>
</div>

<!-- Level Class Id Field -->
<div class="form-group">
    {!! Form::label('level_class_id', 'Level Class Id:') !!}
    <p>{!! $schoolClass->level_class_id !!}</p>
</div>

<!-- School Year Id Field -->
<div class="form-group">
    {!! Form::label('school_year_id', 'School Year Id:') !!}
    <p>{!! $schoolClass->school_year_id !!}</p>
</div>

<!-- School Class Name Field -->
<div class="form-group">
    {!! Form::label('school_class_name', 'School Class Name:') !!}
    <p>{!! $schoolClass->school_class_name !!}</p>
</div>

<!-- Symbol Field -->
<div class="form-group">
    {!! Form::label('symbol', 'Symbol:') !!}
    <p>{!! $schoolClass->symbol !!}</p>
</div>

