@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolEmployee, ['route' => ['schoolEmployees.update', $schoolEmployee->id], 'method' => 'patch']) !!}

        @include('schoolEmployees.fields')

    {!! Form::close() !!}
</div>
@endsection
