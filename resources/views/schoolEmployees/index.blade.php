@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolEmployees</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolEmployees.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolEmployees->isEmpty())
                <div class="well text-center">No SchoolEmployees found.</div>
            @else
                @include('schoolEmployees.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolEmployees])


    </div>
@endsection
