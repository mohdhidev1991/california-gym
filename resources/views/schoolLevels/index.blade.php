@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolLevels</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolLevels.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolLevels->isEmpty())
                <div class="well text-center">No SchoolLevels found.</div>
            @else
                @include('schoolLevels.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolLevels])


    </div>
@endsection
