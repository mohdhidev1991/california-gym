<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Update User Id</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>School Id</th>
			<th>School Level Name</th>
			<th>Creation Date</th>
			<th>Update Date</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolLevels as $schoolLevel)
        <tr>
            <td>{!! $schoolLevel->id !!}</td>
			<td>{!! $schoolLevel->creation_user_id !!}</td>
			<td>{!! $schoolLevel->update_user_id !!}</td>
			<td>{!! $schoolLevel->validation_user_id !!}</td>
			<td>{!! $schoolLevel->validation_date !!}</td>
			<td>{!! $schoolLevel->active !!}</td>
			<td>{!! $schoolLevel->version !!}</td>
			<td>{!! $schoolLevel->update_groups_mfk !!}</td>
			<td>{!! $schoolLevel->delete_groups_mfk !!}</td>
			<td>{!! $schoolLevel->display_groups_mfk !!}</td>
			<td>{!! $schoolLevel->sci_id !!}</td>
			<td>{!! $schoolLevel->school_id !!}</td>
			<td>{!! $schoolLevel->school_level_name !!}</td>
			<td>{!! $schoolLevel->creation_date !!}</td>
			<td>{!! $schoolLevel->update_date !!}</td>
            <td>
                <a href="{!! route('schoolLevels.edit', [$schoolLevel->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolLevels.delete', [$schoolLevel->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolLevel?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
