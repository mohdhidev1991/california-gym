@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolMemberDay, ['route' => ['schoolMemberDays.update', $schoolMemberDay->id], 'method' => 'patch']) !!}

        @include('schoolMemberDays.fields')

    {!! Form::close() !!}
</div>
@endsection
