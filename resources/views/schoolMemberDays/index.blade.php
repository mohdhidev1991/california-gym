@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolMemberDays</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolMemberDays.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolMemberDays->isEmpty())
                <div class="well text-center">No SchoolMemberDays found.</div>
            @else
                @include('schoolMemberDays.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolMemberDays])


    </div>
@endsection
