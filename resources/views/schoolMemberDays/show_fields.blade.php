<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $schoolMemberDay->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $schoolMemberDay->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $schoolMemberDay->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $schoolMemberDay->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $schoolMemberDay->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $schoolMemberDay->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $schoolMemberDay->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $schoolMemberDay->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $schoolMemberDay->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $schoolMemberDay->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $schoolMemberDay->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $schoolMemberDay->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $schoolMemberDay->sci_id !!}</p>
</div>

<!-- Day Hdate Field -->
<div class="form-group">
    {!! Form::label('day_hdate', 'Day Hdate:') !!}
    <p>{!! $schoolMemberDay->day_hdate !!}</p>
</div>

<!-- Coming Time Field -->
<div class="form-group">
    {!! Form::label('coming_time', 'Coming Time:') !!}
    <p>{!! $schoolMemberDay->coming_time !!}</p>
</div>

<!-- Exit Time Field -->
<div class="form-group">
    {!! Form::label('exit_time', 'Exit Time:') !!}
    <p>{!! $schoolMemberDay->exit_time !!}</p>
</div>

<!-- Coming Status Id Field -->
<div class="form-group">
    {!! Form::label('coming_status_id', 'Coming Status Id:') !!}
    <p>{!! $schoolMemberDay->coming_status_id !!}</p>
</div>

<!-- Exit Status Id Field -->
<div class="form-group">
    {!! Form::label('exit_status_id', 'Exit Status Id:') !!}
    <p>{!! $schoolMemberDay->exit_status_id !!}</p>
</div>

<!-- Rea User Id Field -->
<div class="form-group">
    {!! Form::label('rea_user_id', 'Rea User Id:') !!}
    <p>{!! $schoolMemberDay->rea_user_id !!}</p>
</div>

<!-- School Id Field -->
<div class="form-group">
    {!! Form::label('school_id', 'School Id:') !!}
    <p>{!! $schoolMemberDay->school_id !!}</p>
</div>

