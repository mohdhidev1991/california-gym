@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'schoolPeriods.store']) !!}

        @include('schoolPeriods.fields')

    {!! Form::close() !!}
</div>
@endsection
