<!-- Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creation User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
	{!! Form::number('creation_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creation Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('creation_date', 'Creation Date:') !!}
	{!! Form::date('creation_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Update User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
	{!! Form::number('update_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Update Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_date', 'Update Date:') !!}
	{!! Form::date('update_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Validation User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
	{!! Form::number('validation_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Validation Date Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('validation_date', 'Validation Date:') !!}
	{!! Form::date('validation_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('active', 'Active:') !!}
	{!! Form::text('active', null, ['class' => 'form-control']) !!}
</div>

<!-- Version Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('version', 'Version:') !!}
	{!! Form::number('version', null, ['class' => 'form-control']) !!}
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
	{!! Form::text('update_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
	{!! Form::text('delete_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
	{!! Form::text('display_groups_mfk', null, ['class' => 'form-control']) !!}
</div>

<!-- Sci Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sci_id', 'Sci Id:') !!}
	{!! Form::number('sci_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Capacity Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('capacity', 'Capacity:') !!}
	{!! Form::number('capacity', null, ['class' => 'form-control']) !!}
</div>

<!-- Door Close Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('door_close_time', 'Door Close Time:') !!}
	{!! Form::text('door_close_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Door Open Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('door_open_time', 'Door Open Time:') !!}
	{!! Form::text('door_open_time', null, ['class' => 'form-control']) !!}
</div>

<!-- End Coming Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('end_coming_time', 'End Coming Time:') !!}
	{!! Form::text('end_coming_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Exit Time Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('start_exit_time', 'Start Exit Time:') !!}
	{!! Form::text('start_exit_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Period Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('period_id', 'Period Id:') !!}
	{!! Form::number('period_id', null, ['class' => 'form-control']) !!}
</div>

<!-- School Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_id', 'School Id:') !!}
	{!! Form::number('school_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Period Name Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('period_name', 'Period Name:') !!}
	{!! Form::text('period_name', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
