@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolPeriods</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolPeriods.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolPeriods->isEmpty())
                <div class="well text-center">No SchoolPeriods found.</div>
            @else
                @include('schoolPeriods.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolPeriods])


    </div>
@endsection
