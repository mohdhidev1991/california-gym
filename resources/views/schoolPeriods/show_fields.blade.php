<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $schoolPeriod->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $schoolPeriod->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $schoolPeriod->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $schoolPeriod->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $schoolPeriod->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $schoolPeriod->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $schoolPeriod->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $schoolPeriod->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $schoolPeriod->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $schoolPeriod->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $schoolPeriod->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $schoolPeriod->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $schoolPeriod->sci_id !!}</p>
</div>

<!-- Capacity Field -->
<div class="form-group">
    {!! Form::label('capacity', 'Capacity:') !!}
    <p>{!! $schoolPeriod->capacity !!}</p>
</div>

<!-- Door Close Time Field -->
<div class="form-group">
    {!! Form::label('door_close_time', 'Door Close Time:') !!}
    <p>{!! $schoolPeriod->door_close_time !!}</p>
</div>

<!-- Door Open Time Field -->
<div class="form-group">
    {!! Form::label('door_open_time', 'Door Open Time:') !!}
    <p>{!! $schoolPeriod->door_open_time !!}</p>
</div>

<!-- End Coming Time Field -->
<div class="form-group">
    {!! Form::label('end_coming_time', 'End Coming Time:') !!}
    <p>{!! $schoolPeriod->end_coming_time !!}</p>
</div>

<!-- Start Exit Time Field -->
<div class="form-group">
    {!! Form::label('start_exit_time', 'Start Exit Time:') !!}
    <p>{!! $schoolPeriod->start_exit_time !!}</p>
</div>

<!-- Period Id Field -->
<div class="form-group">
    {!! Form::label('period_id', 'Period Id:') !!}
    <p>{!! $schoolPeriod->period_id !!}</p>
</div>

<!-- School Id Field -->
<div class="form-group">
    {!! Form::label('school_id', 'School Id:') !!}
    <p>{!! $schoolPeriod->school_id !!}</p>
</div>

<!-- Period Name Field -->
<div class="form-group">
    {!! Form::label('period_name', 'Period Name:') !!}
    <p>{!! $schoolPeriod->period_name !!}</p>
</div>

