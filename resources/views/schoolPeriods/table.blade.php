<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Capacity</th>
			<th>Door Close Time</th>
			<th>Door Open Time</th>
			<th>End Coming Time</th>
			<th>Start Exit Time</th>
			<th>Period Id</th>
			<th>School Id</th>
			<th>Period Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolPeriods as $schoolPeriod)
        <tr>
            <td>{!! $schoolPeriod->id !!}</td>
			<td>{!! $schoolPeriod->creation_user_id !!}</td>
			<td>{!! $schoolPeriod->creation_date !!}</td>
			<td>{!! $schoolPeriod->update_user_id !!}</td>
			<td>{!! $schoolPeriod->update_date !!}</td>
			<td>{!! $schoolPeriod->validation_user_id !!}</td>
			<td>{!! $schoolPeriod->validation_date !!}</td>
			<td>{!! $schoolPeriod->active !!}</td>
			<td>{!! $schoolPeriod->version !!}</td>
			<td>{!! $schoolPeriod->update_groups_mfk !!}</td>
			<td>{!! $schoolPeriod->delete_groups_mfk !!}</td>
			<td>{!! $schoolPeriod->display_groups_mfk !!}</td>
			<td>{!! $schoolPeriod->sci_id !!}</td>
			<td>{!! $schoolPeriod->capacity !!}</td>
			<td>{!! $schoolPeriod->door_close_time !!}</td>
			<td>{!! $schoolPeriod->door_open_time !!}</td>
			<td>{!! $schoolPeriod->end_coming_time !!}</td>
			<td>{!! $schoolPeriod->start_exit_time !!}</td>
			<td>{!! $schoolPeriod->period_id !!}</td>
			<td>{!! $schoolPeriod->school_id !!}</td>
			<td>{!! $schoolPeriod->period_name !!}</td>
            <td>
                <a href="{!! route('schoolPeriods.edit', [$schoolPeriod->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolPeriods.delete', [$schoolPeriod->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolPeriod?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
