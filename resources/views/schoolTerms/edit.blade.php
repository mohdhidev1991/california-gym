@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolTerm, ['route' => ['schoolTerms.update', $schoolTerm->id], 'method' => 'patch']) !!}

        @include('schoolTerms.fields')

    {!! Form::close() !!}
</div>
@endsection
