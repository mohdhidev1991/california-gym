@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolTerms</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolTerms.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolTerms->isEmpty())
                <div class="well text-center">No SchoolTerms found.</div>
            @else
                @include('schoolTerms.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolTerms])


    </div>
@endsection
