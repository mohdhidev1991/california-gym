<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>School Term Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolTerms as $schoolTerm)
        <tr>
            <td>{!! $schoolTerm->id !!}</td>
			<td>{!! $schoolTerm->creation_user_id !!}</td>
			<td>{!! $schoolTerm->creation_date !!}</td>
			<td>{!! $schoolTerm->update_user_id !!}</td>
			<td>{!! $schoolTerm->update_date !!}</td>
			<td>{!! $schoolTerm->validation_user_id !!}</td>
			<td>{!! $schoolTerm->validation_date !!}</td>
			<td>{!! $schoolTerm->active !!}</td>
			<td>{!! $schoolTerm->version !!}</td>
			<td>{!! $schoolTerm->update_groups_mfk !!}</td>
			<td>{!! $schoolTerm->delete_groups_mfk !!}</td>
			<td>{!! $schoolTerm->display_groups_mfk !!}</td>
			<td>{!! $schoolTerm->sci_id !!}</td>
			<td>{!! $schoolTerm->school_term_name !!}</td>
            <td>
                <a href="{!! route('schoolTerms.edit', [$schoolTerm->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolTerms.delete', [$schoolTerm->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolTerm?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
