@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'schoolTypes.store']) !!}

        @include('schoolTypes.fields')

    {!! Form::close() !!}
</div>
@endsection
