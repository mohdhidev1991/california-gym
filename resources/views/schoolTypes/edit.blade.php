@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolType, ['route' => ['schoolTypes.update', $schoolType->id], 'method' => 'patch']) !!}

        @include('schoolTypes.fields')

    {!! Form::close() !!}
</div>
@endsection
