@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolTypes</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolTypes.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolTypes->isEmpty())
                <div class="well text-center">No SchoolTypes found.</div>
            @else
                @include('schoolTypes.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolTypes])


    </div>
@endsection
