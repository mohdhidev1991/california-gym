<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Tservice Id</th>
			<th>School Type Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolTypes as $schoolType)
        <tr>
            <td>{!! $schoolType->id !!}</td>
			<td>{!! $schoolType->creation_user_id !!}</td>
			<td>{!! $schoolType->creation_date !!}</td>
			<td>{!! $schoolType->update_user_id !!}</td>
			<td>{!! $schoolType->update_date !!}</td>
			<td>{!! $schoolType->validation_user_id !!}</td>
			<td>{!! $schoolType->validation_date !!}</td>
			<td>{!! $schoolType->active !!}</td>
			<td>{!! $schoolType->version !!}</td>
			<td>{!! $schoolType->update_groups_mfk !!}</td>
			<td>{!! $schoolType->delete_groups_mfk !!}</td>
			<td>{!! $schoolType->display_groups_mfk !!}</td>
			<td>{!! $schoolType->sci_id !!}</td>
			<td>{!! $schoolType->tservice_id !!}</td>
			<td>{!! $schoolType->school_type_name !!}</td>
            <td>
                <a href="{!! route('schoolTypes.edit', [$schoolType->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolTypes.delete', [$schoolType->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolType?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
