@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($schoolYear, ['route' => ['schoolYears.update', $schoolYear->id], 'method' => 'patch']) !!}

        @include('schoolYears.fields')

    {!! Form::close() !!}
</div>
@endsection
