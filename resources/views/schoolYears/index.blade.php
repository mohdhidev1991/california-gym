@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SchoolYears</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('schoolYears.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($schoolYears->isEmpty())
                <div class="well text-center">No SchoolYears found.</div>
            @else
                @include('schoolYears.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $schoolYears])


    </div>
@endsection
