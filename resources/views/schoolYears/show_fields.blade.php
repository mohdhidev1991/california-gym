<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $schoolYear->id !!}</p>
</div>

<!-- Creation User Id Field -->
<div class="form-group">
    {!! Form::label('creation_user_id', 'Creation User Id:') !!}
    <p>{!! $schoolYear->creation_user_id !!}</p>
</div>

<!-- Creation Date Field -->
<div class="form-group">
    {!! Form::label('creation_date', 'Creation Date:') !!}
    <p>{!! $schoolYear->creation_date !!}</p>
</div>

<!-- Update User Id Field -->
<div class="form-group">
    {!! Form::label('update_user_id', 'Update User Id:') !!}
    <p>{!! $schoolYear->update_user_id !!}</p>
</div>

<!-- Update Date Field -->
<div class="form-group">
    {!! Form::label('update_date', 'Update Date:') !!}
    <p>{!! $schoolYear->update_date !!}</p>
</div>

<!-- Validation User Id Field -->
<div class="form-group">
    {!! Form::label('validation_user_id', 'Validation User Id:') !!}
    <p>{!! $schoolYear->validation_user_id !!}</p>
</div>

<!-- Validation Date Field -->
<div class="form-group">
    {!! Form::label('validation_date', 'Validation Date:') !!}
    <p>{!! $schoolYear->validation_date !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $schoolYear->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $schoolYear->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $schoolYear->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $schoolYear->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $schoolYear->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $schoolYear->sci_id !!}</p>
</div>

<!-- School Year End Hdate Field -->
<div class="form-group">
    {!! Form::label('school_year_end_hdate', 'School Year End Hdate:') !!}
    <p>{!! $schoolYear->school_year_end_hdate !!}</p>
</div>

<!-- School Year Start Hdate Field -->
<div class="form-group">
    {!! Form::label('school_year_start_hdate', 'School Year Start Hdate:') !!}
    <p>{!! $schoolYear->school_year_start_hdate !!}</p>
</div>

<!-- School Id Field -->
<div class="form-group">
    {!! Form::label('school_id', 'School Id:') !!}
    <p>{!! $schoolYear->school_id !!}</p>
</div>

<!-- School Year Name Field -->
<div class="form-group">
    {!! Form::label('school_year_name', 'School Year Name:') !!}
    <p>{!! $schoolYear->school_year_name !!}</p>
</div>

