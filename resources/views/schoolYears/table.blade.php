<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>School Year End Hdate</th>
			<th>School Year Start Hdate</th>
			<th>School Id</th>
			<th>School Year Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($schoolYears as $schoolYear)
        <tr>
            <td>{!! $schoolYear->id !!}</td>
			<td>{!! $schoolYear->creation_user_id !!}</td>
			<td>{!! $schoolYear->creation_date !!}</td>
			<td>{!! $schoolYear->update_user_id !!}</td>
			<td>{!! $schoolYear->update_date !!}</td>
			<td>{!! $schoolYear->validation_user_id !!}</td>
			<td>{!! $schoolYear->validation_date !!}</td>
			<td>{!! $schoolYear->active !!}</td>
			<td>{!! $schoolYear->version !!}</td>
			<td>{!! $schoolYear->update_groups_mfk !!}</td>
			<td>{!! $schoolYear->delete_groups_mfk !!}</td>
			<td>{!! $schoolYear->display_groups_mfk !!}</td>
			<td>{!! $schoolYear->sci_id !!}</td>
			<td>{!! $schoolYear->school_year_end_hdate !!}</td>
			<td>{!! $schoolYear->school_year_start_hdate !!}</td>
			<td>{!! $schoolYear->school_id !!}</td>
			<td>{!! $schoolYear->school_year_name !!}</td>
            <td>
                <a href="{!! route('schoolYears.edit', [$schoolYear->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('schoolYears.delete', [$schoolYear->id]) !!}" onclick="return confirm('Are you sure wants to delete this SchoolYear?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
