<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $school->id !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $school->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $school->created_at !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $school->updated_by !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $school->updated_at !!}</p>
</div>

<!-- Validated By Field -->
<div class="form-group">
    {!! Form::label('validated_by', 'Validated By:') !!}
    <p>{!! $school->validated_by !!}</p>
</div>

<!-- Validated At Field -->
<div class="form-group">
    {!! Form::label('validated_at', 'Validated At:') !!}
    <p>{!! $school->validated_at !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $school->active !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $school->version !!}</p>
</div>

<!-- Update Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('update_groups_mfk', 'Update Groups Mfk:') !!}
    <p>{!! $school->update_groups_mfk !!}</p>
</div>

<!-- Delete Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('delete_groups_mfk', 'Delete Groups Mfk:') !!}
    <p>{!! $school->delete_groups_mfk !!}</p>
</div>

<!-- Display Groups Mfk Field -->
<div class="form-group">
    {!! Form::label('display_groups_mfk', 'Display Groups Mfk:') !!}
    <p>{!! $school->display_groups_mfk !!}</p>
</div>

<!-- Sci Id Field -->
<div class="form-group">
    {!! Form::label('sci_id', 'Sci Id:') !!}
    <p>{!! $school->sci_id !!}</p>
</div>

<!-- Scapacity Field -->
<div class="form-group">
    {!! Form::label('scapacity', 'Scapacity:') !!}
    <p>{!! $school->scapacity !!}</p>
</div>

<!-- Expiring Hdate Field -->
<div class="form-group">
    {!! Form::label('expiring_hdate', 'Expiring Hdate:') !!}
    <p>{!! $school->expiring_hdate !!}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', 'City Id:') !!}
    <p>{!! $school->city_id !!}</p>
</div>

<!-- Genre Id Field -->
<div class="form-group">
    {!! Form::label('genre_id', 'Genre Id:') !!}
    <p>{!! $school->genre_id !!}</p>
</div>

<!-- Group School Id Field -->
<div class="form-group">
    {!! Form::label('group_school_id', 'Group School Id:') !!}
    <p>{!! $school->group_school_id !!}</p>
</div>

<!-- Lang Id Field -->
<div class="form-group">
    {!! Form::label('lang_id', 'Lang Id:') !!}
    <p>{!! $school->lang_id !!}</p>
</div>

<!-- School Type Id Field -->
<div class="form-group">
    {!! Form::label('school_type_id', 'School Type Id:') !!}
    <p>{!! $school->school_type_id !!}</p>
</div>

<!-- Period Mfk Field -->
<div class="form-group">
    {!! Form::label('period_mfk', 'Period Mfk:') !!}
    <p>{!! $school->period_mfk !!}</p>
</div>

<!-- Sp2 Field -->
<div class="form-group">
    {!! Form::label('sp2', 'Sp2:') !!}
    <p>{!! $school->sp2 !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $school->address !!}</p>
</div>

<!-- Maps Location Url Field -->
<div class="form-group">
    {!! Form::label('maps_location_url', 'Maps Location Url:') !!}
    <p>{!! $school->maps_location_url !!}</p>
</div>

<!-- Pc Field -->
<div class="form-group">
    {!! Form::label('pc', 'Pc:') !!}
    <p>{!! $school->pc !!}</p>
</div>

<!-- Quarter Field -->
<div class="form-group">
    {!! Form::label('quarter', 'Quarter:') !!}
    <p>{!! $school->quarter !!}</p>
</div>

<!-- School Name Ar Field -->
<div class="form-group">
    {!! Form::label('school_name_ar', 'School Name Ar:') !!}
    <p>{!! $school->school_name_ar !!}</p>
</div>

<!-- School Name En Field -->
<div class="form-group">
    {!! Form::label('school_name_en', 'School Name En:') !!}
    <p>{!! $school->school_name_en !!}</p>
</div>

<!-- Sp1 Field -->
<div class="form-group">
    {!! Form::label('sp1', 'Sp1:') !!}
    <p>{!! $school->sp1 !!}</p>
</div>

