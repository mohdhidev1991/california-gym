@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'sdepartments.store']) !!}

        @include('sdepartments.fields')

    {!! Form::close() !!}
</div>
@endsection
