@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($sdepartment, ['route' => ['sdepartments.update', $sdepartment->id], 'method' => 'patch']) !!}

        @include('sdepartments.fields')

    {!! Form::close() !!}
</div>
@endsection
