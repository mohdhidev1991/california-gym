<!-- Sdepartment Name Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sdepartment_name', 'Sdepartment Name:') !!}
	{!! Form::text('sdepartment_name', null, ['class' => 'form-control']) !!}
</div>

<!-- School Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('school_id', 'School Id:') !!}
	{!! Form::number('school_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Week Template Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('week_template_id', 'Week Template Id:') !!}
	{!! Form::number('week_template_id', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
