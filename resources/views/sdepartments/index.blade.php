@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Sdepartments</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('sdepartments.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($sdepartments->isEmpty())
                <div class="well text-center">No Sdepartments found.</div>
            @else
                @include('sdepartments.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $sdepartments])


    </div>
@endsection
