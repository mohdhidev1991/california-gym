<table class="table">
    <thead>
    <th>Id</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Updated By</th>
			<th>Updated At</th>
			<th>Validated By</th>
			<th>Validated At</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Sdepartment Name</th>
			<th>School Id</th>
			<th>Week Template Id</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($sdepartments as $sdepartment)
        <tr>
            <td>{!! $sdepartment->id !!}</td>
			<td>{!! $sdepartment->created_by !!}</td>
			<td>{!! $sdepartment->created_at !!}</td>
			<td>{!! $sdepartment->updated_by !!}</td>
			<td>{!! $sdepartment->updated_at !!}</td>
			<td>{!! $sdepartment->validated_by !!}</td>
			<td>{!! $sdepartment->validated_at !!}</td>
			<td>{!! $sdepartment->active !!}</td>
			<td>{!! $sdepartment->version !!}</td>
			<td>{!! $sdepartment->update_groups_mfk !!}</td>
			<td>{!! $sdepartment->delete_groups_mfk !!}</td>
			<td>{!! $sdepartment->display_groups_mfk !!}</td>
			<td>{!! $sdepartment->sci_id !!}</td>
			<td>{!! $sdepartment->sdepartment_name !!}</td>
			<td>{!! $sdepartment->school_id !!}</td>
			<td>{!! $sdepartment->week_template_id !!}</td>
            <td>
                <a href="{!! route('sdepartments.edit', [$sdepartment->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('sdepartments.delete', [$sdepartment->id]) !!}" onclick="return confirm('Are you sure wants to delete this Sdepartment?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
