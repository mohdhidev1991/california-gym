@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'sessionStatuses.store']) !!}

        @include('sessionStatuses.fields')

    {!! Form::close() !!}
</div>
@endsection
