@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($sessionStatus, ['route' => ['sessionStatuses.update', $sessionStatus->id], 'method' => 'patch']) !!}

        @include('sessionStatuses.fields')

    {!! Form::close() !!}
</div>
@endsection
