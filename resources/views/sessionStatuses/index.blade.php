@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SessionStatuses</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('sessionStatuses.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($sessionStatuses->isEmpty())
                <div class="well text-center">No SessionStatuses found.</div>
            @else
                @include('sessionStatuses.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $sessionStatuses])


    </div>
@endsection
