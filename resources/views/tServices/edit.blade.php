@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($tService, ['route' => ['tServices.update', $tService->id], 'method' => 'patch']) !!}

        @include('tServices.fields')

    {!! Form::close() !!}
</div>
@endsection
