<table class="table">
    <thead>
    <th>Id</th>
			<th>Creation User Id</th>
			<th>Creation Date</th>
			<th>Update User Id</th>
			<th>Update Date</th>
			<th>Validation User Id</th>
			<th>Validation Date</th>
			<th>Active</th>
			<th>Version</th>
			<th>Update Groups Mfk</th>
			<th>Delete Groups Mfk</th>
			<th>Display Groups Mfk</th>
			<th>Sci Id</th>
			<th>Tservice Name</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($tServices as $tService)
        <tr>
            <td>{!! $tService->id !!}</td>
			<td>{!! $tService->creation_user_id !!}</td>
			<td>{!! $tService->creation_date !!}</td>
			<td>{!! $tService->update_user_id !!}</td>
			<td>{!! $tService->update_date !!}</td>
			<td>{!! $tService->validation_user_id !!}</td>
			<td>{!! $tService->validation_date !!}</td>
			<td>{!! $tService->active !!}</td>
			<td>{!! $tService->version !!}</td>
			<td>{!! $tService->update_groups_mfk !!}</td>
			<td>{!! $tService->delete_groups_mfk !!}</td>
			<td>{!! $tService->display_groups_mfk !!}</td>
			<td>{!! $tService->sci_id !!}</td>
			<td>{!! $tService->tservice_name !!}</td>
            <td>
                <a href="{!! route('tServices.edit', [$tService->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('tServices.delete', [$tService->id]) !!}" onclick="return confirm('Are you sure wants to delete this TService?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
