<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'ZendService\\Apple\\Exception\\' => array($vendorDir . '/zendframework/zendservice-apple-apns/library'),
    'ZendService\\Apple\\Apns\\' => array($vendorDir . '/zendframework/zendservice-apple-apns/library'),
    'Svg\\' => array($vendorDir . '/phenx/php-svg-lib/src'),
    'Sly' => array($vendorDir . '/sly/notification-pusher/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'PHPExcel' => array($vendorDir . '/phpoffice/phpexcel/Classes'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'Mailgun\\Tests' => array($vendorDir . '/mailgun/mailgun-php/tests'),
    'Mailgun' => array($vendorDir . '/mailgun/mailgun-php/src'),
    'Maatwebsite\\Excel\\' => array($vendorDir . '/maatwebsite/excel/src'),
    'Laracasts\\Flash' => array($vendorDir . '/laracasts/flash/src'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'JakubOnderka\\PhpConsoleColor' => array($vendorDir . '/jakub-onderka/php-console-color/src'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
    'FontLib\\' => array($vendorDir . '/phenx/php-font-lib/src'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Davibennun\\LaravelPushNotification' => array($vendorDir . '/davibennun/laravel-push-notification/src'),
    'Curl' => array($vendorDir . '/php-mod/curl/src'),
    'Asm89\\Stack' => array($vendorDir . '/asm89/stack-cors/src'),
);
